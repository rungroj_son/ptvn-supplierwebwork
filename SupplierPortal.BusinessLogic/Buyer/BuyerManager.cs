﻿using SupplierPortal.Data.CustomModels.BuyerSuppliers;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.Organization;
using SupplierPortal.DataAccess.Buyer;
using SupplierPortal.DataAccess.BuyerConfig;
using SupplierPortal.DataAccess.ContactPerson;
using SupplierPortal.DataAccess.OrgContactPerson;
using SupplierPortal.DataAccess.User;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;
using SupplierPortal.BusinessLogic.API;
using SupplierPortal.Framework.AccountManage;
using SupplierPortal.Services.AccountManage;
using System.Linq;
using SupplierPortal.Data.CustomModels.Buyer;

namespace SupplierPortal.BusinessLogic.Buyer
{
    public class BuyerManager : IBuyerManager
    {
        private readonly IContactPersonRepository _contactPersonRepository;
        private readonly IUserRepository _userRepository;
        private readonly IOrgContactPersonRepository _orgContactPersonRepository;
        private readonly IBuyerRepository _buyerRepository;
        private readonly IBuyerConfigRepository _buyerConfigRepository;
        private readonly IOrganizationRepository _organizationRepository;
        private readonly IAPILog _apiLog;

        public BuyerManager()
        {
            _contactPersonRepository = new ContactPersonRepository();
            _userRepository = new UserRepository();
            _orgContactPersonRepository = new OrgContactPersonRepository();
            _buyerRepository = new BuyerRepository();
            _buyerConfigRepository = new BuyerConfigRepository();
            _organizationRepository = new OrganizationRepository();
            _apiLog = new APILog();
        }

        #region SupplierUser
        #region INSERT
        public int insertSupplierUser(SupplierUserModel supplierUser)
        {
            try
            {
                int contactPersonID = 0;
                int logContactPersonID = 0;
                int logOrgContactPersonID = 0;
                int userID = 0;
                int logID = 0;
                int logBuyerID = 0;
                string newUsername = string.Empty;

                // Get BuyerConfig and Get Username
                Tbl_BuyerConfig buyerConfig = _buyerConfigRepository.GetBuyerConfig("Supplier_Buyer_Portal", "Prefix", supplierUser.EID);

                if (buyerConfig != null)
                {

                    newUsername = buyerConfig.Value + DateTime.Now.ToString().GetHashCode().ToString("x"); ;
                }
                else
                {
                    newUsername = DateTime.Now.ToString().GetHashCode().ToString("x");
                }

                // Check User Duplicate
                if (!this.CheckUserDuplicate(newUsername))
                {
                    //บันทึกข้อมูลลง Tbl_ContactPerson 
                    Tbl_ContactPerson contactPerson = new Tbl_ContactPerson()
                    {
                        TitleID = supplierUser.TitleID,
                        FirstName_Local = supplierUser.FirstName_Local,
                        LastName_Local = supplierUser.LastName_Local,
                        FirstName_Inter = supplierUser.FirstName_Inter,
                        LastName_Inter = supplierUser.LastName_Inter,
                        JobTitleID = supplierUser.JobTitleID == null || supplierUser.JobTitleID == 0 ? -1 : supplierUser.JobTitleID.Value,
                        PhoneNo = supplierUser.PhoneNo,
                        PhoneExt = supplierUser.PhoneExt,
                        MobileCountryCode = supplierUser.MobileCountryCode,
                        MobileNo = supplierUser.MobileNo,
                        Email = supplierUser.Email,
                        isDeleted = 0
                    };
                    contactPersonID = _contactPersonRepository.Insert(contactPerson);
                    supplierUser.ContactID = contactPersonID;
                    //บันทึกข้อมูลลง Tbl_LogContactPerson 
                    Tbl_LogContactPerson logContactPerson = new Tbl_LogContactPerson()
                    {
                        ContactID = supplierUser.ContactID,
                        Title = supplierUser.TitleID.ToString(),
                        FirstName_Local = supplierUser.FirstName_Local,
                        LastName_Local = supplierUser.LastName_Local,
                        FirstName_Inter = supplierUser.FirstName_Inter,
                        LastName_Inter = supplierUser.LastName_Inter,
                        JobTitleID = supplierUser.JobTitleID == null || supplierUser.JobTitleID == 0 ? -1 : supplierUser.JobTitleID.Value,
                        PhoneNo = supplierUser.PhoneNo,
                        PhoneExt = supplierUser.PhoneExt,
                        Email = supplierUser.Email,
                        isDeleted = 0,
                        UpdateBy = 1,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = "Add",
                    };
                    logContactPersonID = _contactPersonRepository.InsertLogContactPerson(logContactPerson);


                    //บันทึกข้อมูลลง Tbl_OrgContactPerson 
                    if (supplierUser.isPrimaryContact == 1)
                    {
                        _orgContactPersonRepository.UpdatePrimaryOrgContactPersonToDefault(supplierUser.SupplierID);
                    }

                    Tbl_OrgContactPerson orgContactPerson = new Tbl_OrgContactPerson()
                    {
                        SupplierID = supplierUser.SupplierID,
                        ContactID = supplierUser.ContactID,
                        ContactTypeID = supplierUser.ContactTypeID == null || supplierUser.ContactTypeID == 0 ? 0 : supplierUser.ContactTypeID.Value,
                        isPrimaryContact = supplierUser.isPrimaryContact,
                        NewContactID = 0
                    };

                    _orgContactPersonRepository.InsertOrgContactPerson(orgContactPerson);


                    // บันทึกข้อมูลลง Table Tbl_OrgContactPerson 
                    Tbl_LogOrgContactPerson logOrgContactPerson = new Tbl_LogOrgContactPerson()
                    {
                        SupplierID = orgContactPerson.SupplierID,
                        ContactID = orgContactPerson.ContactID,
                        ContactTypeID = orgContactPerson.ContactTypeID,
                        isPrimaryContact = orgContactPerson.isPrimaryContact,
                        NewContactID = orgContactPerson.NewContactID,
                        UpdateByPIS = 0,
                        UpdateBy = 1,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = "Add",
                    };
                    logOrgContactPersonID = _orgContactPersonRepository.InsertLogOrgContactPerson(logOrgContactPerson);

                    // Get Salt and Temp Password
                    Gen_Portal_UserPassword_Result tempPassword = _userRepository.GetRandomSaltAndPassword();
                    string fullPwdStr = tempPassword.Password + tempPassword.RandomSalt;
                    byte[] passwordHash = ClsHashManagement.ComputeHash(fullPwdStr);
                    string newPasswordHashing = Convert.ToBase64String(passwordHash);

                    // บันทึกข้อมูลลง Table Tbl_User
                    Tbl_Organization org = _organizationRepository.GetOrganization(supplierUser.SupplierID);


                    Tbl_User user = new Tbl_User
                    {
                        Username = newUsername,
                        SupplierID = supplierUser.SupplierID,
                        OrgID = org == null ? "0" : org.OrgID,
                        ContactID = supplierUser.ContactID,
                        Password = newPasswordHashing,
                        Salt = tempPassword.RandomSalt,
                        IsActivated = 1,
                        TimeZone = "Asia/Bangkok",
                        Locale = "th_TH",
                        LanguageCode = "en",
                        Currency = "THB",
                        DaysToExpire = 0,
                        PwdRecoveryToken = "",
                        MigrateFlag = 0,
                        IsDisabled = 0,
                        IsPublic = 0,
                        IsDeleted = 0,
                        MergeToUserID = 0,
                        isAcceptTermOfUse = 0,
                        SendMailFlag = 1
                    };
                    userID = _userRepository.InsertUser(user);
                    user.UserID = userID;

                    // บันทึกข้อมูลลง Table Tbl_LogUser
                    Tbl_LogUser logUser = new Tbl_LogUser()
                    {
                        UserID = userID,
                        Username = user.Username,
                        OrgID = user.OrgID,
                        SupplierID = user.SupplierID,
                        ContactID = user.ContactID,
                        Password = user.Password,
                        Salt = user.Salt,
                        IsActivated = user.IsActivated,
                        TimeZone = user.TimeZone,
                        Locale = user.Locale,
                        LanguageCode = user.LanguageCode,
                        Currency = user.Currency,
                        DaysToExpire = user.DaysToExpire,
                        PwdRecoveryToken = user.PwdRecoveryToken,
                        MigrateFlag = user.MigrateFlag,
                        IsDisabled = user.IsDisabled,
                        IsPublic = user.IsPublic,
                        IsDeleted = user.IsDeleted,
                        MergeToUserID = user.MergeToUserID,
                        isAcceptTermOfUse = user.isAcceptTermOfUse,
                        SendMailFlag = user.SendMailFlag,
                        UpdateByPIS = null,
                        UpdateBy = 1,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = "Add",
                    };

                    logID = _userRepository.InsertLogUser(logUser);

                    // บันทึกข้อมูลลง Table Tbl_LogUserByBuyer
                    Tbl_LogUserByBuyer logUserByBuyer = new Tbl_LogUserByBuyer()
                    {
                        UserID = user.UserID,
                        Username = user.Username,
                        OrgID = user.OrgID,
                        SupplierID = user.SupplierID,
                        ContactID = user.ContactID,
                        UpdateDate = DateTime.UtcNow,
                        UpdateBy = Convert.ToInt32(supplierUser.CurrentID),
                        LogAction = "Add",
                        TimeZone = user.TimeZone
                    };
                    logBuyerID = _buyerRepository.InsertLogUserByBuyer(logUserByBuyer);
                }

                return userID;


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GET
        public bool resetPasswordOfSupplierUser(string visitorsIPAddr)
        {
            try
            {
                bool status = false;
                int haveError = 0;
                bool haveData = false;
                List<GetUser_Notify_ChangePassword_Result> results =  _userRepository.GetUserNotifyChangePassword();


                if (results.Any())
                {
                    foreach (GetUser_Notify_ChangePassword_Result item in results)
                    {
                        string erfx_id = item.erfx_id == null ? "" : item.erfx_id.ToString();
                        if (!string.IsNullOrEmpty(item.SysUserID) &&
                            item.BuyerEID != null &&
                            item.erfx_id != null)
                        {
                            haveData = true;
                            bool successStatus = RecoveryPassword.RecoveryPasswordToEmailForBuyerSupplierUser(item, visitorsIPAddr);
                            if (successStatus) // send email is success
                            {
                                _userRepository.UpdateUserNotifyChangePassword(item.SysUserID, item.BuyerEID.Value.ToString(), item.erfx_id.Value.ToString());
                            }
                            else // send email is faile
                            {
                                haveError++;
                            }
                        }
                    }

                }

                if(haveError == 0 && haveData)
                {
                    status = true;
                }

                return status;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #endregion

        #region BuyerSupplier

        #region INSERT
        public int InsertBuyerUser(BuyerUserModel buyerUserModel)
        {
            try
            {
                int buyerUserId = 0;
                // Get Salt and Temp Password
                Gen_Portal_UserPassword_Result tempPassword = _userRepository.GetRandomSaltAndPassword();
                string fullPwdStr = tempPassword.Password + tempPassword.RandomSalt;
                byte[] passwordHash = ClsHashManagement.ComputeHash(fullPwdStr);
                string newPasswordHashing = Convert.ToBase64String(passwordHash);

                Tbl_BuyerUser buyerUser = new Tbl_BuyerUser()
                {
                    EID = buyerUserModel.EID,
                    EPSysUserID = buyerUserModel.EPSysUserID,
                    ContactID = 1,
                    Login = buyerUserModel.Username,
                    Password = newPasswordHashing,
                    Salt = tempPassword.RandomSalt,
                    isAcceptTermOfUse = 1,
                    Createdated = DateTime.UtcNow
                };

                buyerUserId = _buyerRepository.InsertBuyerUser(buyerUser);

                return buyerUserId;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region GET
        public Tuple<List<Buyer_Suppliers_Result>, int, int> GetSuppliers(GetBuyerSuppliersRequest request)
        {
            try
            {
                IBuyerRepository repo = new BuyerRepository();
                Tuple<List<Buyer_Suppliers_Result>, int, int> results = repo.GetSuppliers(request);
                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Tbl_Organization GetOrganization(int id)
        {
            IOrganizationRepository repo = new OrganizationRepository();
            Tbl_Organization result = repo.GetOrganization(id);

            Tbl_Organization obj = new Tbl_Organization
            {
                SupplierID = result.SupplierID,
                OrgID = result.OrgID,
                PursiteID = result.PursiteID,
                CountryCode = result.CountryCode,
                TaxID = result.TaxID,
                DUNSNumber = result.DUNSNumber,
                CompanyTypeID = result.CompanyTypeID,
                BusinessEntityID = result.BusinessEntityID,
                OtherBusinessEntity = result.OtherBusinessEntity,
                CompanyName_Local = result.CompanyName_Local,
                CompanyName_Inter = result.CompanyName_Inter,
                InvName_Local = result.InvName_Local,
                InvName_Inter = result.InvName_Inter,
                MainBusinessCode = result.MainBusinessCode,
                PhoneNo = result.PhoneNo,
                PhoneExt = result.PhoneExt,
                MobileNo = result.MobileNo,
                FaxNo = result.FaxNo,
                WebSite = result.WebSite,
                Latitude = result.Latitude,
                Longtitude = result.Longtitude,
                YearEstablished = result.YearEstablished,
                RegisteredCapital = result.RegisteredCapital,
                CurrencyCode = result.CurrencyCode,
                NumberOfEmp = result.NumberOfEmp,
                isPTVNVerified = result.isPTVNVerified,
                isAffiliateCP = result.isAffiliateCP,
                LastUpdate = result.LastUpdate,
                isDeleted = result.isDeleted,
                isPublic = result.isPublic

            };
            return obj;
        }
        #endregion
        #endregion

        #region BuyerSupplierContact
        #region GET
        public ViewBuyerSupplier GetBuyerSupplierContactDetail(int userId)
        {
            IBuyerRepository repo = new BuyerRepository();
            ViewBuyerSupplier results = null;
            try
            {
                results = repo.GetBuyerSupplierContactDetail(userId);
            }
            catch (Exception ex)
            {
                throw;
            }
            return results;
        }
        public Boolean CheckContactPersonDataChange(Tbl_ContactPerson oldBuyerSupplier, ViewBuyerSupplier newBuyerSupplier)
        {
            bool result = true;
            try
            {
                if (oldBuyerSupplier.FirstName_Local.Trim().ToLower().Equals(newBuyerSupplier.FirstName_Local.Trim().ToLower())
                    && oldBuyerSupplier.LastName_Local.Trim().ToLower().Equals(newBuyerSupplier.LastName_Local.Trim().ToLower())
                    && oldBuyerSupplier.FirstName_Inter.Trim().ToLower().Equals(newBuyerSupplier.FirstName_Inter.Trim().ToLower())
                    && oldBuyerSupplier.LastName_Inter.Trim().ToLower().Equals(newBuyerSupplier.LastName_Inter.Trim().ToLower())                
                    && oldBuyerSupplier.TitleID == newBuyerSupplier.TitleID)
                {

                    string strOldPhoneNo = string.IsNullOrEmpty(oldBuyerSupplier.PhoneNo) ? "" : oldBuyerSupplier.PhoneNo.Trim().ToLower();
                    string strNewPhoneNo = string.IsNullOrEmpty(newBuyerSupplier.PhoneNo) ? "" : newBuyerSupplier.PhoneNo.Trim().ToLower();

                    string strOldEmail = string.IsNullOrEmpty(oldBuyerSupplier.Email) ? "" : oldBuyerSupplier.Email.Trim().ToLower();
                    string strNewEmail = string.IsNullOrEmpty(newBuyerSupplier.Email) ? "" : newBuyerSupplier.Email.Trim().ToLower();

                    string strOldPhoneExt = string.IsNullOrEmpty(oldBuyerSupplier.PhoneExt) ? "" : oldBuyerSupplier.PhoneExt.Trim().ToLower();
                    string strNewPhoneExt = string.IsNullOrEmpty(newBuyerSupplier.PhoneExt) ? "" : newBuyerSupplier.PhoneExt.Trim().ToLower();

                    string strOldMobileNo = string.IsNullOrEmpty(oldBuyerSupplier.MobileNo) ? "" : oldBuyerSupplier.MobileNo.Trim().ToLower();
                    string strNewMobileNo = string.IsNullOrEmpty(newBuyerSupplier.MobileNo) ? "" : newBuyerSupplier.MobileNo.Trim().ToLower();

                    string strOldMobileCountryCode = string.IsNullOrEmpty(oldBuyerSupplier.MobileCountryCode) ? "" : oldBuyerSupplier.MobileCountryCode.Trim().ToLower();
                    string strNewMobileCountryCode = string.IsNullOrEmpty(newBuyerSupplier.MobileCountryCode) ? "" : newBuyerSupplier.MobileCountryCode.Trim().ToLower();

                    if (strOldPhoneNo.Equals(strNewPhoneNo) && strOldEmail.Equals(strNewEmail) && strOldPhoneExt.Equals(strNewPhoneExt) && strOldMobileNo.Equals(strNewMobileNo) && strOldMobileCountryCode.Equals(strNewMobileCountryCode))
                        result = false;

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        #endregion

        #region UPDATE
        public Boolean UpdateSupplierContactPerson(BuyerSuppliersModel buyerSupplier)
        {
            bool result = false;
            Tbl_ContactPerson oldContactPerson = null;
            Tbl_OrgContactPerson orgContactPerson = null;
            int contactID = 0;

            try
            {
                if (buyerSupplier != null)
                {
                    //หา  ข้อมูลจากตาราง Tbl_ContactPerson โดย ContactID
                    oldContactPerson = new Tbl_ContactPerson();
                    oldContactPerson = _contactPersonRepository.GetContactPersonByContactId(buyerSupplier.ContactID);

                    //หา  ข้อมูลจากตาราง Tbl_OrgContactPerson โดย SupplierID, ContactID
                    orgContactPerson = new Tbl_OrgContactPerson();
                    orgContactPerson = _orgContactPersonRepository.GetOrgContactPersonBySupplierContactID(Convert.ToInt32(buyerSupplier.SupplierID), Convert.ToInt32(buyerSupplier.ContactID));

                    if (buyerSupplier.isPrimaryContact != orgContactPerson.isPrimaryContact)
                    {
                        result = this.UpdateOrgContactPerson(orgContactPerson, Convert.ToInt32(buyerSupplier.isPrimaryContact), 0);
                    }

                    //ตรวจสอบการเปลี่ยนแปลงของข้อมูล
                    if (this.CheckContactPersonDataChange(oldContactPerson, buyerSupplier))
                    {
                        contactID = this.UpdateContactPerson(buyerSupplier, oldContactPerson);

                        if (contactID > 0)
                        {
                            result = this.UpdateUser(buyerSupplier, contactID);

                            if (result)
                            {
                                result = this.UpdateOrgContactPerson(orgContactPerson, Convert.ToInt32(buyerSupplier.isPrimaryContact), contactID);

                                if (result)
                                    result = _apiLog.GetSystemListByUsernameForUpdateAPI(buyerSupplier.Username);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                oldContactPerson = null;
                orgContactPerson = null;
            }
            return result;
        }


        public Boolean UpdateOrgContactPerson(Tbl_OrgContactPerson orgContactPerson, int isPrimaryContact, int contactID = 0)
        {
            bool result = false;
            Tbl_LogOrgContactPerson logOrgContactPerson = null;
            int logOrgId = 0;
            int orgId = 0;
            try
            {
                if (orgContactPerson != null)
                {

                    if (isPrimaryContact == 1)
                    {
                        //Update Primary Contact เป็นของ Supplier ให้เป็นศูนย์ทุกตัว
                        result = _orgContactPersonRepository.UpdatePrimaryOrgContactPersonToDefault(Convert.ToInt32(orgContactPerson.SupplierID));
                    }

                    logOrgContactPerson = new Tbl_LogOrgContactPerson()
                    {
                        SupplierID = orgContactPerson.SupplierID,
                        ContactID = orgContactPerson.ContactID,
                        ContactTypeID = orgContactPerson.ContactTypeID,
                        isPrimaryContact = orgContactPerson.isPrimaryContact,
                        NewContactID = orgContactPerson.NewContactID,
                        UpdateByPIS = 0,
                        UpdateBy = 1,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = "Delete",
                    };

                    // เก็บข้อมูลลง Table Tbl_LogOrgContactPerson 
                    logOrgId = _orgContactPersonRepository.InsertLogOrgContactPerson(logOrgContactPerson);

                    if (logOrgId > 0)
                    {
                        // ลบข้อมูลใน Table Tbl_OrgContactPerson 

                        result = _orgContactPersonRepository.RemoveOrgContactPerson(orgContactPerson);

                        // เก็บข้อมูลลง Table Tbl_OrgContactPerson 
                        if (contactID == 0)
                        {
                            orgContactPerson.ContactID = Convert.ToInt32(orgContactPerson.ContactID);
                            orgContactPerson.ContactTypeID = Convert.ToInt32(orgContactPerson.ContactTypeID);
                            orgContactPerson.isPrimaryContact = isPrimaryContact;
                        }
                        else
                        {
                            orgContactPerson.ContactID = Convert.ToInt32(contactID);
                            orgContactPerson.ContactTypeID = Convert.ToInt32(orgContactPerson.ContactTypeID);
                            orgContactPerson.isPrimaryContact = isPrimaryContact;
                        }


                        orgId = _orgContactPersonRepository.InsertOrgContactPerson(orgContactPerson);

                        result = true;

                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
            return result;
        }

        public int UpdateContactPerson(BuyerSuppliersModel buyerSupplier, Tbl_ContactPerson oldContactPerson)
        {
            bool result = false;
            Tbl_ContactPerson contactPerson = null;
            Tbl_LogContactPerson logContactPerson = null;
            int contactPersonID = 0;
            int logContactPersonID = 0;
            try
            {
                //บันทึกข้อมูล ContactPerson ใหม่ ลง Tbl_ContactPerson ถ้าข้อมูลมีการแก้ไข
                contactPerson = new Tbl_ContactPerson()
                {
                    TitleID = buyerSupplier.TitleID,
                    FirstName_Local = buyerSupplier.FirstName_Local,
                    LastName_Local = buyerSupplier.LastName_Local,
                    FirstName_Inter = buyerSupplier.FirstName_Inter,
                    LastName_Inter = buyerSupplier.LastName_Inter,
                    PhoneNo = buyerSupplier.PhoneNo,
                    PhoneExt = buyerSupplier.PhoneExt,
                    Email = buyerSupplier.Email,
                    MobileNo = buyerSupplier.MobileNo,
                    MobileCountryCode = buyerSupplier.MobileCountryCode,
                    isDeleted = 0
                };
                contactPersonID = _contactPersonRepository.Insert(contactPerson);

                //บันทึกข้อมูล ContactPerson ใหม่ Tbl_LogContactPerson หลังจาก Insert ContactPerson ใหม่
                if (contactPersonID > 0)
                {
                    logContactPerson = new Tbl_LogContactPerson()
                    {
                        ContactID = contactPersonID,
                        Title = buyerSupplier.TitleID.ToString(),
                        FirstName_Local = buyerSupplier.FirstName_Local,
                        LastName_Local = buyerSupplier.LastName_Local,
                        FirstName_Inter = buyerSupplier.FirstName_Inter,
                        LastName_Inter = buyerSupplier.LastName_Inter,
                        PhoneNo = buyerSupplier.PhoneNo,
                        PhoneExt = buyerSupplier.PhoneExt,
                        Email = buyerSupplier.Email,
                        MobileNo = buyerSupplier.MobileNo,
                        MobileCountryCode = buyerSupplier.MobileCountryCode,
                        isDeleted = 0,
                        UpdateBy = 1,
                        UpdateDate = DateTime.Now,
                        LogAction = "Add",
                    };

                    logContactPersonID = _contactPersonRepository.InsertLogContactPerson(logContactPerson);

                    //update ข้อมูล Tbl_ContactPerson เก่าให้ isDeleted = 1
                    if (logContactPersonID > 0)
                    {
                        logContactPersonID = 0;
                        oldContactPerson.isDeleted = 1;
                        result = _contactPersonRepository.Update(oldContactPerson);

                        //บันทึกข้อมูลลง Tbl_LogContactPerson หลังจาก Update ContactPerson
                        if (result)
                        {
                            logContactPerson = new Tbl_LogContactPerson()
                            {
                                ContactID = oldContactPerson.ContactID,
                                Title = oldContactPerson.TitleID.ToString(),
                                FirstName_Local = oldContactPerson.FirstName_Local,
                                LastName_Local = oldContactPerson.LastName_Local,
                                FirstName_Inter = oldContactPerson.FirstName_Inter,
                                LastName_Inter = oldContactPerson.LastName_Inter,
                                PhoneNo = oldContactPerson.PhoneNo,
                                PhoneExt = oldContactPerson.PhoneExt,
                                Email = oldContactPerson.Email,
                                MobileNo = oldContactPerson.MobileNo,
                                MobileCountryCode = oldContactPerson.MobileCountryCode,
                                isDeleted = 0,
                                UpdateBy = 1,
                                UpdateDate = DateTime.Now,
                                LogAction = "Modify",
                            };
                            logContactPersonID = _contactPersonRepository.InsertLogContactPerson(logContactPerson);

                            if (logContactPersonID > 0)
                                result = true;
                            else
                                result = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result = false;
            }
            return contactPersonID;
        }

        public Boolean UpdateUser(BuyerSuppliersModel buyerSupplier, int contactPersonID)
        {
            bool result = false;
            Tbl_User user = null;
            Tbl_LogUser logUser = null;
            Tbl_LogUserByBuyer logUserByBuyer = null;
            int logID = 0;
            int logBuyerID = 0;
            try
            {
                user = new Tbl_User();
                user = _userRepository.GetUserByUserId(buyerSupplier.UserID);

                //Insert log ลง Tbl_LogUser
                if (user != null)
                {
                    logUser = new Tbl_LogUser()
                    {
                        UserID = user.UserID,
                        Username = user.Username,
                        OrgID = user.OrgID,
                        SupplierID = user.SupplierID,
                        ContactID = user.ContactID,
                        Password = user.Password,
                        Salt = user.Salt,
                        IsActivated = user.IsActivated,
                        TimeZone = user.TimeZone,
                        Locale = user.Locale,
                        LanguageCode = user.LanguageCode,
                        Currency = user.Currency,
                        DaysToExpire = user.DaysToExpire,
                        PwdRecoveryToken = user.PwdRecoveryToken,
                        MigrateFlag = user.MigrateFlag,
                        IsDisabled = user.IsDisabled,
                        IsPublic = user.IsPublic,
                        IsDeleted = user.IsDeleted,
                        MergeToUserID = user.MergeToUserID,
                        isAcceptTermOfUse = user.isAcceptTermOfUse,
                        SendMailFlag = user.SendMailFlag,
                        UpdateByPIS = null,
                        UpdateBy = 1,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = "Modify",
                    };

                    logID = _userRepository.InsertLogUser(logUser);

                    //Insert Log ใน Table Tbl_LogUserByBuyer
                    if (logID > 0)
                    {
                        logUserByBuyer = new Tbl_LogUserByBuyer()
                        {
                            UserID = user.UserID,
                            Username = user.Username,
                            OrgID = user.OrgID,
                            SupplierID = user.SupplierID,
                            ContactID = user.ContactID,
                            UpdateDate = DateTime.UtcNow,
                            UpdateBy = Convert.ToInt32(buyerSupplier.CurrentID),
                            LogAction = "Modify",
                            TimeZone = user.TimeZone
                        };
                        logBuyerID = _buyerRepository.InsertLogUserByBuyer(logUserByBuyer);
                        // Update ContactID ใหม่ให้กับ User
                        if (logBuyerID > 0)
                        {
                            user.ContactID = contactPersonID;
                            result = _userRepository.UpdateUser(user);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                result = false;
            }
            return result;
        }
        #endregion

        #region GET
        public List<BuyerHistoryContactPersonModel> GetListHistorySupplierContactPersonByUserId(int userId)
        {
            List<BuyerHistoryContactPersonModel> result = null;
            List<BuyerHistoryContactPersonModel> buyerHistoryTracking = null;
            int count = 0;
            try
            {
                buyerHistoryTracking = new List<BuyerHistoryContactPersonModel>();
                buyerHistoryTracking = _buyerRepository.GetListHistorySupplierContactPersonByUserId(userId);

                if (buyerHistoryTracking != null)
                {
                    foreach (var data in buyerHistoryTracking)
                    {
                        BuyerHistoryContactPersonModel model = new BuyerHistoryContactPersonModel();
                        if (data.UpdateDate != null)
                            model.UpdateDate = TimeZoneConvert.ConvertDateTimeToTimeZone(data.TimeZone, data.UpdateDate);

                        model = data;

                        if (result == null)
                        {
                            result = new List<BuyerHistoryContactPersonModel>();

                        }
                        else
                        {
                            if (string.IsNullOrEmpty(result[count].Title_Local))
                                model.Title_Local = "##" + model.Title_Local;
                            else if (string.IsNullOrEmpty(data.Title_Local))
                                model.Title_Local = "##" + model.Title_Local;
                            else if (!data.Title_Local.Trim().ToLower().Equals(result[count].Title_Local.Trim().Replace("##", "").ToLower()))
                                model.Title_Local = "##" + model.Title_Local;

                            if (string.IsNullOrEmpty(result[count].Title_Inter))
                                model.Title_Inter = "##" + model.Title_Inter;
                            else if (string.IsNullOrEmpty(data.Title_Inter))
                                model.Title_Inter = "##" + model.Title_Inter;
                            else if (!data.Title_Inter.Trim().ToLower().Equals(result[count].Title_Inter.Trim().Replace("##", "").ToLower()))
                                model.Title_Inter = "##" + model.Title_Inter;

                            if (string.IsNullOrEmpty(result[count].FirstName_Local))
                                model.FirstName_Local = "##" + model.FirstName_Local;
                            else if (string.IsNullOrEmpty(data.FirstName_Local))
                                model.FirstName_Local = "##" + model.FirstName_Local;
                            else if (!data.FirstName_Local.Trim().ToLower().Equals(result[count].FirstName_Local.Trim().Replace("##", "").ToLower()))
                                model.FirstName_Local = "##" + model.FirstName_Local;

                            if (string.IsNullOrEmpty(result[count].LastName_Local))
                                model.LastName_Local = "##" + model.LastName_Local;
                            else if (string.IsNullOrEmpty(data.LastName_Local))
                                model.LastName_Local = "##" + model.LastName_Local;
                            else if (!data.LastName_Local.Trim().ToLower().Equals(result[count].LastName_Local.Trim().Replace("##", "").ToLower()))
                                model.LastName_Local = "##" + model.LastName_Local;

                            if (string.IsNullOrEmpty(result[count].FirstName_Inter))
                                model.FirstName_Inter = "##" + model.FirstName_Inter;
                            else if (string.IsNullOrEmpty(data.FirstName_Inter))
                                model.FirstName_Inter = "##" + model.FirstName_Inter;
                            else if (!data.FirstName_Inter.Trim().ToLower().Equals(result[count].FirstName_Inter.Trim().Replace("##", "").ToLower()))
                                model.FirstName_Inter = "##" + model.FirstName_Inter;

                            if (string.IsNullOrEmpty(result[count].LastName_Inter))
                                model.LastName_Inter = "##" + model.LastName_Inter;
                            else if (string.IsNullOrEmpty(data.LastName_Inter))
                                model.LastName_Inter = "##" + model.LastName_Inter;
                            else if (!data.LastName_Inter.Trim().ToLower().Equals(result[count].LastName_Inter.Trim().Replace("##", "").ToLower()))
                                model.LastName_Inter = "##" + model.LastName_Inter;

                            if (string.IsNullOrEmpty(result[count].PhoneNo))
                                model.PhoneNo = "##" + model.PhoneNo;
                            else if (string.IsNullOrEmpty(data.PhoneNo))
                                model.PhoneNo = "##" + model.PhoneNo;
                            else if (!data.PhoneNo.Trim().ToLower().Equals(result[count].PhoneNo.Trim().Replace("##", "").Replace("&&", "").ToLower()))
                                model.PhoneNo = "##" + model.PhoneNo;

                            if (string.IsNullOrEmpty(result[count].PhoneExt))
                                model.PhoneExt = "##" + model.PhoneExt;
                            else if (string.IsNullOrEmpty(data.PhoneExt))
                                model.PhoneExt = "##" + model.PhoneExt;
                            else if (!data.PhoneExt.Trim().ToLower().Equals(result[count].PhoneExt.Trim().Replace("##", "").Replace("&&", "").ToLower()))
                                model.PhoneExt = "##" + model.PhoneExt;

                            if (string.IsNullOrEmpty(result[count].Email))
                                model.Email = "##" + model.Email;
                            else if (string.IsNullOrEmpty(data.Email))
                                model.Email = "##" + model.Email;
                            else if (!data.Email.Trim().ToLower().Equals(result[count].Email.Trim().Replace("##", "").ToLower()))
                                model.Email = "##" + model.Email;

                            if (string.IsNullOrEmpty(result[count].MobileNo))
                                model.MobileNo = "##" + model.MobileNo;
                            else if (string.IsNullOrEmpty(data.MobileNo))
                                model.MobileNo = "##" + model.MobileNo;
                            else if (!data.MobileNo.Trim().ToLower().Equals(result[count].MobileNo.Trim().Replace("##", "").ToLower()))
                                model.MobileNo = "##" + model.MobileNo;

                            if (string.IsNullOrEmpty(result[count].MobileCountryCode))
                                model.MobileCountryCode = "##" + model.MobileCountryCode;
                            else if (string.IsNullOrEmpty(data.MobileCountryCode))
                                model.MobileCountryCode = "##" + model.MobileCountryCode;
                            else if (!data.MobileCountryCode.Trim().ToLower().Equals(result[count].MobileCountryCode.Trim().Replace("##", "").Replace("&&", "").ToLower()))
                                model.MobileCountryCode = "##" + model.MobileCountryCode;

                            count++;
                        }
                        result.Add(model);
                    };
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        #endregion
        #endregion

        #region GetBuyerUserWithConfigBySysUserID
        #region GET
        public virtual Tbl_BuyerConfig GetBuyerConfigByEID(int eid)
        {
            Tbl_BuyerConfig result = null;
            try
            {
                result = new Tbl_BuyerConfig();
                result = _buyerRepository.GetBuyerConfigByEID(eid);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        #endregion
        #endregion

        #region Check User Duplicate
        #region GET
        public bool CheckUserDuplicate(string username)
        {
            bool result = false;
            Tbl_User user = null;
            try
            {
                user = new Tbl_User();
                user = _userRepository.GetUserByUsername(username);

                if(user != null)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        #endregion
        #endregion

    }
}
