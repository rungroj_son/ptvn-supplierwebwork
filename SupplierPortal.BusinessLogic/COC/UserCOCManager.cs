﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.DataAccess.COC;
using SupplierPortal.DataAccess.User;
using SupplierPortal.Models.Dto.COC;
using SupplierPortal.BusinessLogic.ExternalService;
using SupplierPortal.Data.Models.SupportModel.Consent;
using SupplierPortal.Data.CustomModels.Consent;
using SupplierPortal.BusinessLogic.ExternalService.Factory;
using System.Web.Configuration;
using System.Web;
using Newtonsoft.Json;

namespace SupplierPortal.BusinessLogic.COC
{
    public partial class UserCOCManager : IUserCOCManager
    {
        private readonly IUserCOCRepository _userCOCRepository;
        private readonly IUserSessionRepository _userSessionRepository;
        private readonly IUserRepository _userRepository;
        private readonly IConsentServiceManager _consentServiceManager;

        public string accessToken
        {
            get
            {
                if (HttpContext.Current.Application != null && HttpContext.Current.Application["AccessToken"] != null)
                {
                    return HttpContext.Current.Application["AccessToken"].ToString();
                }
                else
                {
                    return null;
                }
            }
            set
            {
                HttpContext.Current.Application["AccessToken"] = value;
            }
        }

        public DateTime accessTokenExpire
        {
            get
            {
                if (HttpContext.Current.Application != null && HttpContext.Current.Application["AccessTokenExpire"] != null)
                {
                    return Convert.ToDateTime(HttpContext.Current.Application["AccessTokenExpire"]);
                }
                else
                {
                    return DateTime.Now.AddMinutes(-1);
                }
            }

            set
            {
                HttpContext.Current.Application["AccessTokenExpire"] = value;
            }
        }

        public UserCOCManager()
        {
            _userCOCRepository = new UserCOCRepository();
            _userSessionRepository = new UserSessionRepository();
            _userRepository = new UserRepository();
            _consentServiceManager = new PTVNConsentServiceManager();
        }

        public ViewOrgVendorConsent ExistsInCodeOfConduct(string username)
        {
            ViewOrgVendorConsent consent = null;
            try
            {
                consent = _userCOCRepository.GetConsent(username);
            }
            catch (Exception ex) { }

            return consent;
        }

        public async Task<ConsentRequestAcceptModel> GetCodeOfConduct(ViewOrgVendorConsent consent, string username)
        {
            ConsentRequestAcceptModel response = null;

            try
            {
                username = username.ToUpper();
                _consentServiceManager.setConfigConsent(consent);
                ConsentRequestAcceptModel result = await _consentServiceManager.GetRequestConcentAccept(username);
                if (result != null && result.payload != null && result.payload.Count > 0)
                {
                    response = result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

        public async Task<ConsentAcceptModel> AcceptConsent(ViewOrgVendorConsent consent, string username, ConsentAcceptRequest request)
        {
            ConsentAcceptModel response = null;

            try
            {
                request.dataSubject = request.dataSubject.ToUpper();
                _consentServiceManager.setConfigConsent(consent);
                ConsentAcceptModel result = await _consentServiceManager.AcceptConsent(request);
                if (result != null && result.payload != null && result.result.FirstOrDefault().message == "Succeed")
                {
                    response = result;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }
    }
}
