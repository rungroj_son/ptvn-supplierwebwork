﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.APILogs;
using SupplierPortal.Data.Models.Repository.SystemConfigureAPI;
using SupplierPortal.Data.Models.SupportModel.LinkSystem;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Services.AccountManage;
using SupplierPortal.Services.ProfileManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.API
{
    public class APILog : IAPILog
    {
        private readonly IAPILogsRepository _apiLogsRepository;
        private readonly ISystemConfigureAPIRepository _systemConfigureAPIRepository;
        private readonly IProfileDataService _profileDataService;
        public APILog()
        {
            _apiLogsRepository = new APILogsRepository();
            _systemConfigureAPIRepository = new SystemConfigureAPIRepository();
            _profileDataService = new ProfileDataService();
        }
        public void insertAPILog(string ReqID, int? SystemID, string Events, DateTime? Events_Time, string IPAddress_Client, string APIKey, string APIName, string Return_Code, string Return_Status, string Return_Message, string Data, string UserGUID)
        {
            var APILog = new Tbl_APILogs()
            {
                ReqID = ReqID,
                SystemID = SystemID,
                Events = Events,
                Events_Time = Events_Time,
                IPAddress_Client = IPAddress_Client,
                APIKey = APIKey,
                APIName = APIName,
                Return_Code = Return_Code,
                Return_Status = Return_Status,
                Return_Message = Return_Message,
                Data = Data,
                UserGUID = UserGUID
            };

            _apiLogsRepository.Insert(APILog);
        }

        public Boolean GetSystemListByUsernameForUpdateAPI(string userName)
        {
            bool result = false;
            string systemUnSuccessCall = string.Empty;
            HttpClient client = null;
            Tbl_APILogs _apiLogs = null;
            try
            {
                #region Call API Data

                string guid = "mockGUID";

                //if (System.Web.HttpContext.Current.Session["BuyerGUID"] != null)
                //{
                //    guid = System.Web.HttpContext.Current.Session["BuyerGUID"].ToString();
                //}

                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                var listSystem = _systemConfigureAPIRepository.GetSystemListByUsernameForUpdateAPI(userName);

                foreach (var sysMapping in listSystem)
                {
                    string reqID = GeneratorGuid.GetRandomGuid();

                    string Jsonstr = string.Empty;

                    string _APIKey = string.Empty;

                    string _APIName = string.Empty;

                    var apiUpdateModels = _profileDataService.GetDataCallUpdateUserAPI(sysMapping, reqID, sysMapping.Username);
                    Jsonstr = JsonConvert.SerializeObject(apiUpdateModels);
                    _APIKey = apiUpdateModels.Request.APIKey;
                    _APIName = apiUpdateModels.Request.APIName;

                    client = new HttpClient();
                    client.BaseAddress = new Uri(sysMapping.API_URL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpContent content = new StringContent(Jsonstr);
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    #region Insert API Log Before

                    _apiLogs = new Tbl_APILogs()
                    {
                        ReqID = reqID,
                        SystemID = sysMapping.SystemID,
                        Events = "Request_Before",
                        Events_Time = DateTime.UtcNow,
                        IPAddress_Client = visitorsIPAddr,
                        APIKey = _APIKey,
                        APIName = _APIName,
                        Return_Code = "",
                        Return_Status = "",
                        Return_Message = "",
                        Data = Jsonstr,
                        //Data = responseBody,
                        UserGUID = guid,
                    };

                    _apiLogsRepository.Insert(_apiLogs);
                    #endregion

                    #region Call API
                    //HttpResponseMessage response = client.PostAsync(sysMapping.API_URL, content).Result;

                    //var responseBody = response.Content.ReadAsStringAsync().Result;

                    //if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    //{
                    //    try
                    //    {
                    //        if (!string.IsNullOrEmpty(responseBody))
                    //        {
                    //            JObject _response = JObject.Parse(responseBody);

                    //            string return_Code = _response["Response"]["Result"]["Code"].ToString();
                    //            string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                    //            string return_Message = _response["Response"]["Result"]["Message"].ToString();


                    //            #region Insert API Log

                    //            var _APILogs = new Tbl_APILogs()
                    //            {
                    //                ReqID = reqID,
                    //                SystemID = sysMapping.SystemID,
                    //                Events = "Request",
                    //                Events_Time = DateTime.UtcNow,
                    //                IPAddress_Client = visitorsIPAddr,
                    //                APIKey = _APIKey,
                    //                APIName = _APIName,
                    //                Return_Code = return_Code,
                    //                Return_Status = return_Status,
                    //                Return_Message = return_Message,
                    //                Data = Jsonstr,
                    //                //Data = responseBody,
                    //                UserGUID = guid,
                    //            };

                    //            _aPILogsRepository.Insert(_APILogs);
                    //            #endregion

                    //            if (!Convert.ToBoolean(return_Status))
                    //            {
                    //                systemUnSuccessCall += sysMapping.SystemName + ",";
                    //            }
                    //        }
                    //        else
                    //        {
                    //            #region Insert API Log

                    //            var _APILogs = new Tbl_APILogs()
                    //            {
                    //                ReqID = reqID,
                    //                SystemID = sysMapping.SystemID,
                    //                Events = "Request",
                    //                Events_Time = DateTime.UtcNow,
                    //                IPAddress_Client = visitorsIPAddr,
                    //                APIKey = _APIKey,
                    //                APIName = _APIName,
                    //                Return_Code = "",
                    //                Return_Status = "",
                    //                Return_Message = "ResponseBody Empty",
                    //                Data = Jsonstr,
                    //                //Data = responseBody,
                    //                UserGUID = guid,
                    //            };

                    //            _aPILogsRepository.Insert(_APILogs);
                    //            #endregion

                    //            systemUnSuccessCall += sysMapping.SystemName + ",";
                    //        }

                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        #region Insert API Log
                    //        string guidCall = "mockGUID";

                    //        if (System.Web.HttpContext.Current.Session["GUID"] != null)
                    //        {
                    //            guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                    //        }

                    //        string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


                    //        var _APILogsCheck = new Tbl_APILogs()
                    //        {
                    //            ReqID = reqID,
                    //            SystemID = sysMapping.SystemID,
                    //            Events = "Request_Check",
                    //            Events_Time = DateTime.UtcNow,
                    //            IPAddress_Client = visitorsIPAddrCall,
                    //            APIKey = _APIKey,
                    //            APIName = _APIName,
                    //            Return_Code = "",
                    //            Return_Status = response.StatusCode.ToString(),
                    //            Return_Message = responseBody,
                    //            Data = Jsonstr,
                    //            //Data = responseBody,
                    //            UserGUID = guidCall,
                    //        };

                    //        _aPILogsRepository.Insert(_APILogsCheck);


                    //        #endregion

                    //        throw;
                    //    }

                    //}
                    //else
                    //{

                    //    #region Insert API Log

                    //    var _APILogs = new Tbl_APILogs()
                    //    {
                    //        ReqID = reqID,
                    //        Events = "Request",
                    //        Events_Time = DateTime.UtcNow,
                    //        IPAddress_Client = visitorsIPAddr,
                    //        APIKey = _APIKey,
                    //        APIName = _APIName,
                    //        Return_Code = "",
                    //        Return_Status = "",
                    //        Return_Message = "",
                    //        Data = Jsonstr,
                    //        //Data = responseBody,
                    //        UserGUID = guid,
                    //    };

                    //    _aPILogsRepository.Insert(_APILogs);

                    //    systemUnSuccessCall += sysMapping.SystemName + ",";

                    //    #endregion
                    //}
                    #endregion

                    #region  API Post Task
                    string responseBody = string.Empty;
                    var continuationTask = client.PostAsync(sysMapping.API_URL, content).ContinueWith(
                                            (postTask) =>
                                            {
                                                bool isCompleted = postTask.IsCompleted;
                                                bool isFaulted = postTask.IsFaulted;
                                                if (isCompleted && !isFaulted)
                                                {
                                                    var response = postTask.Result;
                                                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                                    {
                                                        try
                                                        {
                                                            if (!string.IsNullOrEmpty(response.Content.ReadAsStringAsync().Result))
                                                            {
                                                                responseBody = response.Content.ReadAsStringAsync().Result;
                                                                JObject _response = JObject.Parse(responseBody);

                                                                string return_Code = _response["Response"]["Result"]["Code"].ToString();
                                                                string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                                                                string return_Message = _response["Response"]["Result"]["Message"].ToString();


                                                                #region Insert API Log

                                                                _apiLogs = new Tbl_APILogs()
                                                                {
                                                                    ReqID = reqID,
                                                                    SystemID = sysMapping.SystemID,
                                                                    Events = "Response_Success",
                                                                    Events_Time = DateTime.UtcNow,
                                                                    IPAddress_Client = visitorsIPAddr,
                                                                    APIKey = _APIKey,
                                                                    APIName = _APIName,
                                                                    Return_Code = return_Code,
                                                                    Return_Status = return_Status,
                                                                    Return_Message = return_Message,
                                                                    Data = Jsonstr,
                                                                    //Data = responseBody,
                                                                    UserGUID = guid,
                                                                };

                                                                _apiLogsRepository.Insert(_apiLogs);
                                                                #endregion

                                                                if (!Convert.ToBoolean(return_Status))
                                                                {
                                                                    systemUnSuccessCall += sysMapping.SystemName + ",";
                                                                }
                                                            }
                                                            //else
                                                            //{
                                                            //    #region Insert API Log

                                                            //    var _APILogs = new Tbl_APILogs()
                                                            //    {
                                                            //        ReqID = reqID,
                                                            //        SystemID = sysMapping.SystemID,
                                                            //        Events = "Request",
                                                            //        Events_Time = DateTime.UtcNow,
                                                            //        IPAddress_Client = visitorsIPAddr,
                                                            //        APIKey = _APIKey,
                                                            //        APIName = _APIName,
                                                            //        Return_Code = "",
                                                            //        Return_Status = "",
                                                            //        Return_Message = "ResponseBody Empty UpdateGeneralData",
                                                            //        Data = Jsonstr,
                                                            //        //Data = responseBody,
                                                            //        UserGUID = guid,
                                                            //    };

                                                            //    _aPILogsRepository.Insert(_APILogs);
                                                            //    #endregion

                                                            //    systemUnSuccessCall += sysMapping.SystemName + ",";
                                                            //}

                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            #region Insert API Log
                                                            string guidCall = "mockGUID";

                                                            if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                                            {
                                                                guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                                            }

                                                            string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


                                                            _apiLogs = new Tbl_APILogs()
                                                            {
                                                                ReqID = reqID,
                                                                SystemID = sysMapping.SystemID,
                                                                Events = "Response_Check",
                                                                Events_Time = DateTime.UtcNow,
                                                                IPAddress_Client = visitorsIPAddrCall,
                                                                APIKey = _APIKey,
                                                                APIName = _APIName,
                                                                Return_Code = "",
                                                                Return_Status = response.StatusCode.ToString(),
                                                                Return_Message = ex.InnerException.Message,
                                                                Data = Jsonstr,
                                                                //Data = responseBody,
                                                                UserGUID = guidCall,
                                                            };

                                                            _apiLogsRepository.Insert(_apiLogs);


                                                            #endregion

                                                            throw;
                                                        }
                                                    }
                                                    else
                                                    {

                                                        #region Insert API Log

                                                        var _APILogs = new Tbl_APILogs()
                                                        {
                                                            ReqID = reqID,
                                                            SystemID = sysMapping.SystemID,
                                                            Events = "Response_Fail",
                                                            Events_Time = DateTime.UtcNow,
                                                            IPAddress_Client = visitorsIPAddr,
                                                            APIKey = _APIKey,
                                                            APIName = _APIName,
                                                            Return_Code = "",
                                                            Return_Status = response.StatusCode.ToString(),
                                                            Return_Message = "",
                                                            Data = Jsonstr,
                                                            //Data = responseBody,
                                                            UserGUID = guid,
                                                        };

                                                        _apiLogsRepository.Insert(_APILogs);

                                                        systemUnSuccessCall += sysMapping.SystemName + ",";

                                                        #endregion
                                                    }
                                                }

                                            });
                    continuationTask.Wait(20000);
                    //continuationTask.Wait(
                    #endregion

                }

                #endregion
                result = true;
            }
            catch (Exception)
            {
                result = false;
                throw;
            }
            finally
            {
                if (client != null)
                    client.Dispose();
            }
            return result;
        }

    }
}
