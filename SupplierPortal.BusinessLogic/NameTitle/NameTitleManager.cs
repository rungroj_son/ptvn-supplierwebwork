﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.NameTitle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.NameTitle
{
    public partial class NameTitleManager : INameTitleManager
    {
        protected readonly INameTitleRepository _nameTitleRepository;

        public NameTitleManager()
        {
            _nameTitleRepository = new NameTitleRepository();
        }

        public List<Tbl_NameTitle> GetNameTitleList(int languageID)
        {
            List<Tbl_NameTitle> result = null;
            try
            {
                result = _nameTitleRepository.GetNameTitleList(languageID);
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
    }
}
