﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.Language;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.Language
{
    public partial class LanguageManager : ILanguageManager
    {

        protected readonly ILanguageRepository _languageRepository;

        public LanguageManager()
        {
            _languageRepository = new LanguageRepository();
        }

        public string GetLanguageIdByCode(string languageCode)
        {
            string languageID = string.Empty;

            var result = _languageRepository.GetLanguageIdByCode(languageCode).FirstOrDefault();

            if (result != null)
            {
                languageID = result.LanguageID.ToString();
            }
            else
            {
                var result2 = _languageRepository.GetLanguage().Where(b => b.isInter == 1).FirstOrDefault();

                languageID = result2.LanguageID.ToString();
            }

            return languageID;
        }

        public bool IsLocalLanguage(int languageId)
        {
            try
            {
                return _languageRepository.IsLocalLanguage(languageId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Tbl_Language> GetLanguage()
        {
            List<Tbl_Language> language = null;
            try
            {
                language = _languageRepository.GetLanguage();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return language.ToList();
        }
    }
}
