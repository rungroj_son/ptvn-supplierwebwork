﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.Language
{
    public partial interface ILanguageManager
    {
        string GetLanguageIdByCode(string languageCode);
        bool IsLocalLanguage(int languageId);

        List<Tbl_Language> GetLanguage();
    }
}
