﻿using Newtonsoft.Json;
using SupplierPortal.Data.CustomModels.Consent;
using SupplierPortal.Data.CustomModels.SCF;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Consent;
using SupplierPortal.Services.APIService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace SupplierPortal.BusinessLogic.ExternalService.Factory
{
    public class PTVNConsentServiceManager : APIConnectionService, IConsentServiceManager
    {
        public string oauthURL;
        public string clientId;
        public string clientSecret;
        public string grantType;
        public string name;
        public string api;
        public string requestAcceptEndpoint;
        public string acceptConsentEndpoint;

        public PTVNConsentServiceManager()
        {
            this.api = WebConfigurationManager.AppSettings["ptvn_consent_api"];
            this.requestAcceptEndpoint = WebConfigurationManager.AppSettings["ptvn_consent_request_accept"];
            this.acceptConsentEndpoint = WebConfigurationManager.AppSettings["ptvn_consent_accept_consent"];
        }

        public void setConfigConsent(ViewOrgVendorConsent consent)
        {
            this.name = consent.ApplicationName;
        }

        public async Task<ConsentRequestAcceptModel> GetRequestConcentAccept(string dataSubject)
        {
            ConsentRequestAcceptModel response = null;
            try
            {
                using (HttpResponseMessage httpResponse = new HttpResponseMessage())
                {
                    return await this.PTVNRequestConcentAccept(response, dataSubject);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ConsentAcceptModel> AcceptConsent(ConsentAcceptRequest request)
        {
            ConsentAcceptModel response = null;
            try
            {
                using (HttpResponseMessage httpResponse = new HttpResponseMessage())
                {
                    return await this.PTVNAcceptConsent(response, request);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region With Application Name
        public async Task<ConsentRequestAcceptModel> GetRequestAcceptWithApplicationName(string dataSubject, string applicationName)
        {
            ConsentRequestAcceptModel response = null;
            try
            {
                using (HttpResponseMessage httpResponse = new HttpResponseMessage())
                {
                    return await this.PTVNRequestConcentAccept(response, dataSubject, applicationName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<ConsentAcceptModel> AcceptWithApplicationName(ConsentAcceptRequest request, string applicationName)
        {
            ConsentAcceptModel response = null;
            try
            {
                using (HttpResponseMessage httpResponse = new HttpResponseMessage())
                {
                    return await this.PTVNAcceptConsent(response, request, applicationName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method
        private async Task<ConsentRequestAcceptModel> PTVNRequestConcentAccept(ConsentRequestAcceptModel response, string dataSubject, string applicationName = "")
        {
            string url = this.setURLHelper(this.requestAcceptEndpoint, this.api);
            string application = String.IsNullOrWhiteSpace(applicationName) ? this.name : applicationName;

            url = this.AddParameterHelper(url, new List<KeyValuePair<string, string>>() {
                    new KeyValuePair<string, string>("data.application",application),
                    new KeyValuePair<string, string>("data.dataSubject",dataSubject),
                });

            List<APIKey> keys = new List<APIKey>() { };

            HttpResponseMessage httpResponse = await this.GetApiConnectionWithHeader(url, "{0}", keys);

            if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return this.streamReaderToObject<ConsentRequestAcceptModel>(httpResponse);
            }

            return response;
        }


        private async Task<ConsentAcceptModel> PTVNAcceptConsent(ConsentAcceptModel response, ConsentAcceptRequest request, string applicationName = "")
        {
            string url = this.setURLHelper(this.acceptConsentEndpoint, this.api);
            string application = String.IsNullOrWhiteSpace(applicationName) ? this.name : applicationName;
            List<APIKey> keys = new List<APIKey>() { };
            request.app = application;
            request.deviceFingerprintId = "-";
            request.note = string.Empty;

            HttpResponseMessage httpResponse = await this.PostApiConnectionWithHeader(url, "{0}", request, keys);

            if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                response = this.streamReaderToObject<ConsentAcceptModel>(httpResponse);
            }
            return response;
        }
        #endregion
    }
}
