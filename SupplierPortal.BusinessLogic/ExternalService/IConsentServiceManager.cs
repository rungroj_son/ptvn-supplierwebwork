﻿using SupplierPortal.Data.CustomModels.Consent;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Consent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.ExternalService
{
    public interface IConsentServiceManager
    {
        void setConfigConsent(ViewOrgVendorConsent consent);
        Task<ConsentRequestAcceptModel> GetRequestConcentAccept(string dataSubject);
        Task<ConsentAcceptModel> AcceptConsent(ConsentAcceptRequest request);
        Task<ConsentRequestAcceptModel> GetRequestAcceptWithApplicationName(string dataSubject, string applicationName);
        Task<ConsentAcceptModel> AcceptWithApplicationName(ConsentAcceptRequest request, string applicationName);
    }
}
