﻿using SupplierPortal.Data.CustomModels.Organization;
using SupplierPortal.Data.CustomModels.SearchResult;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Models.Dto;
using System.Collections.Generic;

namespace SupplierPortal.BusinessLogic.Organization
{
    public interface IOrganizationManager
    {
        OrganizationShortDetailDto GetOrganizationShortDetail(string taxid, string branchNo);

        List<OrganizationShortDetailDto> GetOrganizationList(string taxId, string countryCode);

        SearchResultResponse<List<SearchOrgResponse>> GetOrganizationWithPagination(SearchResultRequest request);
    }
}
