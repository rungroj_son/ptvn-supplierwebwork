﻿using SupplierPortal.Data.CustomModels.User;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Data.Models.SupportModel.UAA;
using System;

namespace SupplierPortal.BusinessLogic.User
{
    public partial interface IUserManager
    {
        Boolean UpdateUserActiveInActive(int userId);

        UserContactPersonDataModels GetUserInfo(string username);

        UserContactResponse GetUserInfoFromProcurementBoard(string username);

        UAAOuthResponse GetUserForUAAByUsername(UserRequest request);
    }
}
