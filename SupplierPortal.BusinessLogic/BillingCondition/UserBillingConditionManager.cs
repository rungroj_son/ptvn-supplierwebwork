﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.DataAccess.BillingCondition;

namespace SupplierPortal.BusinessLogic.BillingCondition
{
    public partial class UserBillingConditionManager : IUserBillingConditionManager
    {
        private readonly IUserBillingConditionRepository _userBillingConditionRepository;
        private readonly IUserSessionRepository _userSessionRepository;

        public UserBillingConditionManager()
        {
            _userBillingConditionRepository = new UserBillingConditionRepository();
            _userSessionRepository = new UserSessionRepository();
        }

        public bool CheckAcceeptBillingCondition(string username)
        {
            bool result = false;
            Tbl_UserBillingCondition users;
            try
            {
                users = new Tbl_UserBillingCondition();
                users = this._userBillingConditionRepository.GetUserBillingCondition(username);

                if (users == null)
                {
                    return true;
                }

                if (users != null && users.IsAcceptBillingCondition != null && (bool)users.IsAcceptBillingCondition)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public bool UpdateBillingCondition(string userGUID)
        {
            bool reuslt = true;
            Tbl_UserBillingCondition users = null;
            Tbl_UserSession userSession;
            try
            {

                userSession = new Tbl_UserSession();
                userSession = this._userSessionRepository.GetUserSession(userGUID);

                if (userSession != null)
                {
                    users = this._userBillingConditionRepository.GetUserBillingCondition(userSession.Username);
                }

                if (users != null && users.IsAcceptBillingCondition == null)
                {
                    users.IsAcceptBillingCondition = true;
                    users.BillingPeriod = DateTime.Now;
                    reuslt = this._userBillingConditionRepository.UpdateUserBillingCondition(users);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return reuslt;
        }
    }
}
