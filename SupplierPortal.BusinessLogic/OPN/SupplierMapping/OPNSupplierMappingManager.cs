﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.OPN.Buyer;
using SupplierPortal.DataAccess.OPN.SupplierMapping;
using SupplierPortal.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.OPN.SupplierMapping
{
    public class OPNSupplierMappingManager : IOPNSupplierMappingManager
    {
        public readonly IOPNSupplierMappingRepository _opnSupplierMappingRepository;
        public readonly IOPNBuyerRepository _opnBuyer;

        public OPNSupplierMappingManager()
        {
            _opnSupplierMappingRepository = new OPNSupplierMappingRepository();
            _opnBuyer = new OPNBuyerRepository();
        }

        #region INSERT
        public int InsertOPNSupplierMapping(RegisterOPNSupplierMapping request)
        {
            try
            {
                int Id = 0;
                Tbl_OPNSupplierMapping tbl_OPNSupplierMapping = new Tbl_OPNSupplierMapping();
                tbl_OPNSupplierMapping.RegID = request.regId;
                tbl_OPNSupplierMapping.OPNSupplierID = request.buyerSupplierId;
                tbl_OPNSupplierMapping.ContactID = request.buyerSupplierContactId;
                tbl_OPNSupplierMapping.SupplierUserID = request.supplierUserId;

                if (!string.IsNullOrEmpty(request.buyerShortName)) // buyerShortName = invitation code
                {
                    Tbl_OPNBuyer result = _opnBuyer.getOPNBuyer(request.buyerShortName);
                    if (result != null)
                    {
                        tbl_OPNSupplierMapping.BuyerID = result.BuyerID;
                        Id = _opnSupplierMappingRepository.InsertOPNSupplierMapping(tbl_OPNSupplierMapping);
                    }

                }

                return Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
