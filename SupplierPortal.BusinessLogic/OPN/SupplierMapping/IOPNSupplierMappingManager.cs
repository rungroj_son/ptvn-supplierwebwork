﻿using SupplierPortal.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.OPN.SupplierMapping
{
    public interface IOPNSupplierMappingManager
    {
        #region INSERT
        int InsertOPNSupplierMapping(RegisterOPNSupplierMapping request);
        #endregion
    }
}
