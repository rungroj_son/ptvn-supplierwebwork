﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.Country;
using SupplierPortal.DataAccess.BusinessType;
using SupplierPortal.DataAccess.Organization;
using SupplierPortal.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.OPN.OPNOrganization 
{
    public class OPNOrganizationManager : IOPNOrganizationManager
    { 

        public readonly IOrganizationRepository _organizationRepository;
        public readonly IBusinessTypeRepository _businessTypeRepository;
        public readonly ICountryRepository _countryRepository;
        public OPNOrganizationManager()
        {
            _organizationRepository = new OrganizationRepository();
            _businessTypeRepository = new BusinessTypeRepository();
            _countryRepository = new CountryRepository();
        }

        #region GET
        public OPN_OrganizationDto getOPNOrganizationShortDetail(string taxid, string branchNo)
        {
            OPN_OrganizationDto organizationDto = null;
            try
            {
                Tbl_Organization organization = _organizationRepository.GetOrganization(taxid, branchNo);
                if (organization != null)
                {
                    organizationDto = new OPN_OrganizationDto();
                    organizationDto.taxId = organization.TaxID;
                    //organizationDto.branchNo = organization.BranchNo;
                    //organizationDto.companyActive = organization.isDeleted == 0 ? false : true;
                    organizationDto.registeredCapital = organization.RegisteredCapital;
                    organizationDto.currency = organization.CurrencyCode;
                    organizationDto.yearEstablished = organization.YearEstablished;
                    organizationDto.supplierDirectory = true; // defualt is true
                    organizationDto.deliveryDate = null;

                    // Set Organization Address
                    Tbl_OrgAddress orgAddress = organization.Tbl_OrgAddress.Where(i => i.AddressTypeID == 1).FirstOrDefault();

                    if (orgAddress != null)
                    {
                        if (orgAddress.Tbl_Address != null)
                        {
                            Tbl_Address address = orgAddress.Tbl_Address;
                            organizationDto.address = "ที่อยู่ " + (string.IsNullOrEmpty(address.HouseNo_Local.Trim()) ? "-" : address.HouseNo_Local.Trim())
                                                    + " หมู่ที่ " + (string.IsNullOrEmpty(address.VillageNo_Local.Trim()) ? "-" : address.VillageNo_Local.Trim())
                                                    + " ซอย " + (string.IsNullOrEmpty(address.Lane_Local.Trim()) ? "-" : address.Lane_Local.Trim())
                                                    + " ถนน " + (string.IsNullOrEmpty(address.Road_Local.Trim()) ? "-" : address.Road_Local.Trim())
                                                    + " ตำบล " + (string.IsNullOrEmpty(address.SubDistrict_Local.Trim()) ? "-" : address.SubDistrict_Local.Trim())
                                                    + " อำเภอ " + (string.IsNullOrEmpty(address.City_Local.Trim()) ? "-" : address.City_Local.Trim());
                            organizationDto.province = address.State_Local;
                            organizationDto.postcode = address.PostalCode;
                            if (!string.IsNullOrEmpty(address.CountryCode))
                            {
                                Tbl_Country country = _countryRepository.GetCountry(address.CountryCode);
                                if(country!= null)
                                {
                                    organizationDto.country = country.CountryName;
                                } 
                            }
                        }
                    }

                    //Set BusinessType
                    List<Tbl_OrgBusinessType> orgBusinessTypes = organization.Tbl_OrgBusinessType == null ? null : organization.Tbl_OrgBusinessType.ToList();
                    if (orgBusinessTypes != null)
                    {
                        if(orgBusinessTypes.Where(i => i.BusinessTypeID == 1).FirstOrDefault() != null)// Manufacturer
                        {
                            organizationDto.manufacturer = true;
                        }

                        if (orgBusinessTypes.Where(i => i.BusinessTypeID == 2).FirstOrDefault() != null)// distributor
                        {
                            organizationDto.distributor = true;
                        }

                        if (orgBusinessTypes.Where(i => i.BusinessTypeID == 3).FirstOrDefault() != null)// dealer
                        {
                            organizationDto.dealer = true;
                        }

                        if (orgBusinessTypes.Where(i => i.BusinessTypeID == 4).FirstOrDefault() != null)// serviceProvider
                        {
                            organizationDto.serviceProvider = true;
                        }


                    }

                    // Set Contact
                    Tbl_OrgContactPerson orgContactPerson = organization.Tbl_OrgContactPerson.Where(i => i.isPrimaryContact == 1).FirstOrDefault();
                    if (orgContactPerson != null)
                    {
                        if (orgContactPerson.Tbl_ContactPerson != null)
                        {
                            Tbl_ContactPerson contactPerson = orgContactPerson.Tbl_ContactPerson;
                            organizationDto.contactName = contactPerson.Tbl_NameTitle.TitleName + contactPerson.FirstName_Local.Trim() + " " + contactPerson.LastName_Local.Trim();
                            organizationDto.phone = contactPerson.PhoneNo == "NULL" ? null : contactPerson.PhoneNo + (string.IsNullOrEmpty(organization.PhoneExt.Trim()) ? "" : "" + organization.PhoneExt.Trim());
                            organizationDto.mobile = contactPerson.MobileNo == "NULL" ? null : contactPerson.MobileNo;
                            organizationDto.department = contactPerson.Department;
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return organizationDto;
        }
        #endregion

    }
}
