﻿using SupplierPortal.Data.CustomModels.SCF;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.UserSession
{
    public partial interface IUserSessionManager
    {
        Boolean Update(Tbl_UserSession tbl_UserSession);
        Tbl_UserSession GetUserSession(string userGuid);
        UserSessionSCF GetUserSessionForSCF(string userGuid);
    }
}
