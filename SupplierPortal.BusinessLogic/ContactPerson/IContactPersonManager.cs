﻿using SupplierPortal.Data.CustomModels.SearchResult;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Models.Dto;
using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.ContactPerson
{
    public partial interface IContactPersonManager
    {
        Tuple<List<Tbl_ContactPersonDto>, int, int> GetUserContactPersonsOfOrganization(int supplierID, DatatableRequest request);

        bool GetContactPersionByEmail(string email);

        SearchResultResponse<List<ContactWithPagination_Sel_Result>> GetContactWithPagination(SearchResultRequest request);
    }
}
