﻿using SupplierPortal.BusinessLogic.ExternalService;
using SupplierPortal.Data.CustomModels.RegisterSupplier;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.RegContactRequest;
using SupplierPortal.Data.Models.Repository.RegInfo;
using SupplierPortal.DataAccess.RegFromConnect;
using SupplierPortal.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.RegFromConnect
{
    public class RegFromConnectManager : IRegFromConnectManager
    {
        private IRegFromConnectRepository _regFromConnectRepository;
        private IRegInfoRepository _regInfoRepository;
        private IRegContactRequestRepository _regContactRequestRepository;

        private IExternalServiceManager _externalServiceManager;

        public RegFromConnectManager()
        {
            _regFromConnectRepository = new RegFromConnectRepository();
            _regInfoRepository = new RegInfoRepository();
            _regContactRequestRepository = new RegContactRequestRepository();

            _externalServiceManager = new ExternalServiceManager();
        }

        public async Task<List<ConnectJobResponse>> RunningJobRegisterSupplierAndRequestTojoin()
        {
            List<ConnectJobResponse> response;
            try
            {
                response = new List<ConnectJobResponse>();

                List<ConnectJobRequest> request = new List<ConnectJobRequest>();

                List<int> listRegId = new List<int>();
                List<int> listUserRegId = new List<int>();
                List<int> listReqStatus = new List<int>();

                List<Tbl_RegInfo> listSupplier = new List<Tbl_RegInfo>();
                List<Tbl_RegContactRequest> listContactRequest = new List<Tbl_RegContactRequest>();

                var listRegister = _regFromConnectRepository.GetAllByIsJobCompleted(false);

                foreach (Tbl_RegFromConnect data in listRegister)
                {
                    if ((bool)data.IsSupplierRegister)
                    {
                        listRegId.Add((int)data.RegId);
                    }
                    else
                    {
                        listUserRegId.Add((int)data.RegId);
                    }
                }

                listReqStatus.Add(RegStatusEnum.REJECTED.GetAttributeId());
                listReqStatus.Add(RegStatusEnum.SUCCESS.GetAttributeId());

                listSupplier = _regInfoRepository.GetRegInfoByRegListAndStatusList(listRegId, listReqStatus);
                listContactRequest = _regContactRequestRepository.GetRegContactRequestByRegListAndStatusList(listUserRegId, listReqStatus);

                foreach (Tbl_RegInfo data in listSupplier)
                {
                    ConnectJobRequest job = new ConnectJobRequest()
                    {
                        regId = data.RegID,
                        status = (int)data.RegStatusID == RegStatusEnum.SUCCESS.GetAttributeId() ? UserBranchStatusEnum.APPROVED.GetAttributeCode() : UserBranchStatusEnum.REJECTED.GetAttributeCode(),
                        type = RegisterTypeEnum.REGISTER.GetAttributeCode()
                    };

                    request.Add(job);
                }

                foreach (Tbl_RegContactRequest data in listContactRequest)
                {
                    ConnectJobRequest job = new ConnectJobRequest()
                    {
                        regId = data.UserReqId,
                        status = (int)data.UserReqStatusID == RegStatusEnum.SUCCESS.GetAttributeId() ? UserBranchStatusEnum.APPROVED.GetAttributeCode() : UserBranchStatusEnum.REJECTED.GetAttributeCode(),
                        type = RegisterTypeEnum.REGISTER.GetAttributeCode()
                    };

                    request.Add(job);
                }

                if (request.Count > 0)
                {

                    int limitRequest = 20;
                    if (request.Count >= limitRequest)
                    {
                        int countRequest = request.Count;
                        int loopRequest = (countRequest / limitRequest) + ((countRequest % limitRequest) > 0 ? 1 : 0);
                        int indexStart = 0;
                        for (int num = 0; num < loopRequest; num++)
                        {
                            limitRequest = (countRequest - indexStart < limitRequest) ? (countRequest - indexStart) : limitRequest;
                            var filterRequest = request.GetRange(indexStart, limitRequest);
                            indexStart += limitRequest;

                            var result = await _externalServiceManager.UpdateStatusSupplierConnect(filterRequest);
                            response.AddRange(result);
                        }
                    }
                    else
                    {
                        response = await _externalServiceManager.UpdateStatusSupplierConnect(request);
                    }
                }

                if (response.Count > 0)
                {
                    foreach (ConnectJobResponse data in response)
                    {
                        if (!data.isError)
                        {
                            var regConectData = listRegister.Find(f => f.RegId == data.regId);
                            regConectData.IsJobCompleted = (bool)regConectData.IsSupplierRegister ? true : false;
                            _regFromConnectRepository.Update(regConectData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public bool UpdateRegFromConnect(Tbl_RegFromConnect data)
        {
            bool result = false;
            try
            {
                result = _regFromConnectRepository.Update(data);
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }

    }
}
