﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.RolePrivilege
{
    public interface IRolePrivilegeManager
    {
        List<Tbl_BuyerRolePrivilege> GetBuyerRolePrivileges(int sysUserId);

        List<Tbl_BuyerRolePrivilege> GetBuyerRolePrivilgeByEPUserId(string sessionUserId, int epUserId, int eid);
        
    }
}
