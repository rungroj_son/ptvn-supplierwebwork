﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.RolePrivilege;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.RolePrivilege
{
    public class RolePrivilegeManager : IRolePrivilegeManager
    {
        private readonly IRolePrivilegeRepository _rolePrivilegeRepository;

        public RolePrivilegeManager()
        {
            _rolePrivilegeRepository = new RolePrivilegeRepository();
        }


        public List<Tbl_BuyerRolePrivilege> GetBuyerRolePrivileges(int sysUserId)
        {
            try
            {
                List<Tbl_BuyerRolePrivilege> results = _rolePrivilegeRepository.GetBuyerRolePrivileges(sysUserId);

                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Tbl_BuyerRolePrivilege> GetBuyerRolePrivilgeByEPUserId(string sessionUserId, int epUserId, int eid)
        {
            try
            {
                List<Tbl_BuyerRolePrivilege> results = _rolePrivilegeRepository.GetBuyerRolePrivilgeByEPUserId(sessionUserId, epUserId, eid);

                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
