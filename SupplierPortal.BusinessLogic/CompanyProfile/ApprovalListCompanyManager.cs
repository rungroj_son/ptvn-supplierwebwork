﻿
using SupplierPortal.Data.Models.Repository.TrackingOrgAttachment;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SupplierPortal.Data.CustomModels.CompanyProfile;
using SupplierPortal.Data.Models.Repository.TrackingOrgAddress;

namespace SupplierPortal.BusinessLogic.CompanyProfile
{

    public partial class ApprovalListCompanyManager : IApprovalListCompanyManager
    {
        private readonly ITrackingOrgAttachmentRepository _trackingOrgAttachmentRepository;
        private readonly ITrackingOrgAddressRepository _trackingOrgAddressRepository;
        public ApprovalListCompanyManager()
        {
            _trackingOrgAttachmentRepository = new TrackingOrgAttachmentRepository();
            _trackingOrgAddressRepository = new TrackingOrgAddressRepository();
        }
        public async Task<ApprovalResponseObject> GetTrackingApprovalList(CompanyProfileApprovalRequest request)
        {
            List<ApprovalResonseModel> listAttachment = new List<ApprovalResonseModel>();
            List<ApprovalResonseModel> listAddress = new List<ApprovalResonseModel>();

            var dataAttachment = _trackingOrgAttachmentRepository.GetViewAttachmentByItemId(request.trackingAttachmentItemIdList);
            foreach (var item in dataAttachment)
            {
                ApprovalResonseModel trackingApprovalModel = new ApprovalResonseModel
                {

                    ItemId = item.ItemId,
                    TrackingStatusId = item.TrackingStatusId,
                    TrackingStatusDate = item.TrackingStatusDate,
                    TrackingStatusName = item.TrackingStatusName,
                    Remark = item.Remark,
                    AddressTypeId = null,
                    AddressTypeName = null
                    
                };
                
                listAttachment.Add(trackingApprovalModel);
            }

            var dataAddress = _trackingOrgAddressRepository.GetTrackingContactAddressByTrackingItemIDList(request.trackingAddressItemIdList);
            foreach (var item in dataAddress)
            {
                ApprovalResonseModel trackingApprovalModel = new ApprovalResonseModel
                {

                    ItemId = item.ItemId,
                    TrackingStatusId = (int)item.TrackingStatusId,
                    TrackingStatusDate = item.TrackingStatusDate,
                    TrackingStatusName = item.TrackingStatusName,
                    Remark = item.Remark,
                    AddressTypeId = item.AddressTypeId,
                    AddressTypeName = item.AddressTypeName

                };

                listAddress.Add(trackingApprovalModel);
            }
            ApprovalResponseObject approval = new ApprovalResponseObject
            {
                TrackingAddress = listAddress,
                TrackingAttachment = listAttachment
            };

            return approval;
        }
    }

    
}
