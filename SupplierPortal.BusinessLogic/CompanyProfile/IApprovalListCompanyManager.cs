﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SupplierPortal.Data.CustomModels.CompanyProfile;


namespace SupplierPortal.BusinessLogic.CompanyProfile
{
    public partial interface IApprovalListCompanyManager
    {
        Task<ApprovalResponseObject> GetTrackingApprovalList(CompanyProfileApprovalRequest request);

    }
}
