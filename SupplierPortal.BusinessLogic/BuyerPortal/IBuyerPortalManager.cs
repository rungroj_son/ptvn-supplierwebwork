﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.BuyerPortal
{
    public partial interface IBuyerPortalManager
    {
        HttpResponseMessage LoginValidate(string publicKey, string authInfo, string encodeUserpassword, string languageId, string browserType, string clientIPAddress);
    }
}
