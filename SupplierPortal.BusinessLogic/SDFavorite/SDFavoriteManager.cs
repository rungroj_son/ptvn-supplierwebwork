﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.LogSDFavoriteRepository;
using SupplierPortal.DataAccess.SDFavorite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.SDFavorite
{
    public class SDFavoriteManager : ISDFavoriteManager
    {
        private readonly ISDFavoriteRepository _SDFavoriteRepository;
        private readonly ILogSDFavoriteRepository _LogSDFavoriteRepository;

        public SDFavoriteManager()
        {
            _SDFavoriteRepository = new SDFavoriteRepository();
            _LogSDFavoriteRepository = new LogSDFavoriteRepository();
        }

        #region GET
        public  List<Tbl_SDFavorite> GetSDFavoriteByEID(int EID, string sysUserId)
        {

            List<Tbl_SDFavorite> resutls = _SDFavoriteRepository.GetSDFavoriteByEID(EID, sysUserId).ToList();

            return resutls;
        }
        public List<Core_SupplierContactFavorite_Result> GetSDFavorites(int eid, int supplierId, int sysUserId)
        {

            List<Core_SupplierContactFavorite_Result> resutls = _SDFavoriteRepository.GetSDFavorites(eid,supplierId,sysUserId);

            return resutls;
        }

        public Tbl_SDFavorite GetSDFavorite(string sysUserID, int EID, int userID)
        {
            try
            {
                Tbl_SDFavorite result = _SDFavoriteRepository.GetSDFavorite(sysUserID, EID, userID);

                return result;
            }
            catch (Exception e)
            {

                throw;
            }
        }
        #endregion

        #region INSERT
        public int Insert(Tbl_SDFavorite tbl_SDFavorite, int updateBy)
        {
            try
            {
                int resultId = _SDFavoriteRepository.Insert(tbl_SDFavorite, updateBy);
                _LogSDFavoriteRepository.Insert(resultId, updateBy, "Add");

                return resultId;
            }
            catch (Exception e)
            {

                throw;
            }

        }
        #endregion

        #region Delete
        public int Delete(int id, int updateBy)
        {

            try
            {
                int resultId = _SDFavoriteRepository.Delete(id, updateBy);
                if(resultId > 0)
                {
                    _LogSDFavoriteRepository.Insert(resultId, updateBy, "Remove");
                }

                return resultId;


            }
            catch (Exception e)
            {

                throw;
            }
        }
        #endregion
    }
}
