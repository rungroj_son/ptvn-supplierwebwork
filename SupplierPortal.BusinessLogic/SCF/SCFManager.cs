﻿using SupplierPortal.BusinessLogic.ExternalService;
using SupplierPortal.Core.Extension;
using SupplierPortal.Data.CustomModels.SCF;
using SupplierPortal.Data.CustomModels.SendEmail;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.ACL_OrgMenuMapping;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.MenuPortal;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.DataAccess.SCF;
using SupplierPortal.DataAccess.TPDEntry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace SupplierPortal.BusinessLogic.SCF
{
    public class SCFManager : ISCFManager
    {
        public readonly IExternalServiceManager _externalServiceManager;
        public readonly ISCFRequestRepository _sCFRequestRepository;
        public readonly IUserRepository _userRepository;
        public readonly IUserSessionRepository _userSessionRepository;
        public readonly IOrganizationRepository _organizationRepository;
        public readonly IContactPersonRepository _contactPersonRepository;
        public readonly IAddressRepository _addressRepository;
        public readonly IACL_OrgMenuMappingRepository _orgMenuMappingRepository;
        public readonly IMenuPortalRepository _menuPortalRepository;
        public readonly IViewTPDEntryRepository _viewTPDEntryRepository;

        private string email_scf_to;
        private string email_scf_from;
        private string template_email_scf_id;
        private string email_scf_subject;
        private string email_scf_cc;

        private string eInvoice_service_name;
        public SCFManager()
        {
            _externalServiceManager = new ExternalServiceManager();
            _sCFRequestRepository = new SCFRequestRepository();
            _userRepository = new UserRepository();
            _userSessionRepository = new UserSessionRepository();
            _organizationRepository = new OrganizationRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _addressRepository = new AddressRepository();
            _orgMenuMappingRepository = new ACL_OrgMenuMappingRepository();
            _menuPortalRepository = new MenuPortalRepository();
            _viewTPDEntryRepository = new ViewTPDEntryRepository();

            this.email_scf_from = WebConfigurationManager.AppSettings["email_scf_from"];
            this.email_scf_to = WebConfigurationManager.AppSettings["email_scf_to"];
            this.email_scf_cc = WebConfigurationManager.AppSettings["email_scf_cc"];
            this.template_email_scf_id = WebConfigurationManager.AppSettings["template_email_scf_id"];
            this.email_scf_subject = WebConfigurationManager.AppSettings["email_scf_subject"];
            this.eInvoice_service_name = WebConfigurationManager.AppSettings["eInvoice_service_name"];
        }


        #region Check Access E-Invoice
        public async Task<bool> CheckAccessEInvoice(string orgID, string userName)
        {
            bool result = false;
            try
            {
                Tbl_MenuPortal portal = this._menuPortalRepository.GetMenuByMenuName(this.eInvoice_service_name);
                Tbl_Organization organization = this._organizationRepository.GetOrganizationByOrgID(orgID);

                if (portal != null && organization != null)
                {
                    result = this._orgMenuMappingRepository.CheckAllowAccessBySupplierIDAndMenuID(organization.SupplierID, portal.MenuID);
                }

                if (result)
                {
                    ViewTPDEntry viewTPD = this._viewTPDEntryRepository.GetTPDEntryBytpOrgId(organization.OrgID);
                    if (viewTPD != null)
                    {
                        string buyerMpId = WebConfigurationManager.AppSettings["opn_supplier_invoice_buyer_mp_id"];

                        result = await this._externalServiceManager.CheckInvoice(viewTPD.MktParticipantID, "6", buyerMpId);

                        if(!result)
                        {
                            int supplierId = await this._externalServiceManager.GetSupplierId(organization.OrgID);
                            result = await this._externalServiceManager.CheckGR(viewTPD.MktParticipantID, supplierId, userName, 6, true);
                        }
                    }
                }


            }
            catch (Exception e)
            {
                throw e;
            }
            return result;
        }
        #endregion

        #region SCF Save
        public async Task<SCFResponse> SaveSCFRequest(SCFRequest request)
        {
            SCFResponse response = null;
            try
            {
                response = new SCFResponse();
                response.msgError = "";
                response.isError = true;

                Tbl_UserSession userSession = this._userSessionRepository.GetUserSession(request.userGUID);
                if (userSession == null)
                {
                    response.msgError = "Common.ErrMsg.UserSessionNotFound";
                    return response;
                }

                if (userSession.isTimeout == 1 || userSession.LogoutTime != null)
                {
                    response.msgError = "Common.ErrMsg.UserSessionTimeout";
                    return response;
                }

                Tbl_User user = this._userRepository.FindByUsername(userSession.Username);
                if (user == null)
                {
                    response.msgError = "Common.ErrMsg.UserNotFound";
                    return response;
                }

                if (this.ValidateSCFInprogress(user.OrgID))
                {
                    response.msgError = "Common.ErrMsg.UserInprogress";
                    return response;
                }

                Tbl_Organization organization = this._organizationRepository.GetOrganizationByOrgID(user.OrgID);
                if (organization == null)
                {
                    response.msgError = "Common.ErrMsg.SupplierNotFound";
                    return response;
                }

                Tbl_ContactPerson contactPerson = this._contactPersonRepository.GetContactPersonByContectID((int)user.ContactID);
                if (contactPerson == null)
                {
                    response.msgError = "Common.ErrMsg.ContactNotFound";
                    return response;
                }

                Tbl_Address address = this._addressRepository.GetAddressBySupplierID(organization.SupplierID);
                string companyAddress = "ที่อยู่ " + (string.IsNullOrEmpty(address.HouseNo_Local) ? "-" : address.HouseNo_Local)
                                                    + " หมู่ที่ " + (string.IsNullOrEmpty(address.VillageNo_Local) ? "-" : address.VillageNo_Local)
                                                    + " ซอย " + (string.IsNullOrEmpty(address.Lane_Local) ? "-" : address.Lane_Local)
                                                    + " ถนน " + (string.IsNullOrEmpty(address.Road_Local) ? "-" : address.Road_Local)
                                                    + " ตำบล " + (string.IsNullOrEmpty(address.SubDistrict_Local) ? "-" : address.SubDistrict_Local)
                                                    + " อำเภอ " + (string.IsNullOrEmpty(address.City_Local) ? "-" : address.City_Local);

                Tbl_SCFRequest sCFRequest = new Tbl_SCFRequest();
                sCFRequest.Username = user.Username;
                sCFRequest.SupplierId = user.SupplierID;
                sCFRequest.OrgId = user.OrgID;
                sCFRequest.OrganizationName = organization.InvName_Inter;
                sCFRequest.InProgress = false;
                sCFRequest.Approved = false;
                sCFRequest.Rejected = false;
                sCFRequest.ErrMsg = "";
                sCFRequest.ActionFrom = request.actionFrom;
                sCFRequest.AcceptedDate = DateTime.Now;

                int resultSave = 0;
                resultSave = this._sCFRequestRepository.InsertSCFRequest(sCFRequest);


                bool sendEmailStatus = false;
                if (resultSave > 0)
                {
                    sCFRequest.Id = resultSave;
                    response.scfRequestId = resultSave;

                    SCFEmailMapping sCFEmailMapping = new SCFEmailMapping();
                    sCFEmailMapping.NAME = contactPerson.FirstName_Local + " " + contactPerson.LastName_Local;
                    sCFEmailMapping.PHONE_NUMBER = contactPerson.PhoneNo + (String.IsNullOrEmpty(contactPerson.PhoneExt) ? "" : "Ext : " + contactPerson.PhoneExt);
                    sCFEmailMapping.EMAIL = contactPerson.Email;
                    sCFEmailMapping.TAX_ID = organization.TaxID;
                    sCFEmailMapping.COMPANY_NAME = organization.InvName_Inter;
                    sCFEmailMapping.BRANCH_NUMBER = organization.BranchNo;
                    sCFEmailMapping.COMPANY_ADDRESS = companyAddress;
                    sCFEmailMapping.REQUEST_FROM = request.actionFrom;

                    JavaScriptSerializer jsonConvert = new JavaScriptSerializer();
                    string scfMappingData = jsonConvert.Serialize(sCFEmailMapping);

                    string[] emailTo = null;
                    if (!String.IsNullOrEmpty(this.email_scf_to))
                        emailTo = this.email_scf_to.Split(',');

                    string[] emailCC = null;
                    if (!String.IsNullOrEmpty(this.email_scf_cc))
                        emailCC = this.email_scf_cc.Split(',');

                    SendEmailRequest emailRequest = new SendEmailRequest();
                    emailRequest.mailFrom = this.email_scf_from;
                    emailRequest.mailTo = emailTo;
                    emailRequest.mailCC = emailCC;
                    emailRequest.mailSubject = String.Format(this.email_scf_subject, resultSave, organization.InvName_Inter);
                    emailRequest.templateId = int.Parse(this.template_email_scf_id);
                    emailRequest.mappingData = scfMappingData;

                    sendEmailStatus = await this._externalServiceManager.SCFSendEmail(emailRequest);
                }

                if (!sendEmailStatus)
                {
                    sCFRequest.ErrMsg = "Common.ErrMsg.SendEmailFail";
                    sendEmailStatus = this._sCFRequestRepository.UpdateSCFRequest(sCFRequest);

                    response.msgError = sCFRequest.ErrMsg;
                    return response;
                }
                else
                {
                    sCFRequest.InProgress = true;
                    sCFRequest.ErrMsg = "";
                    sendEmailStatus = this._sCFRequestRepository.UpdateSCFRequest(sCFRequest);

                    response.isError = false;
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SCF Save
        public async Task<SCFResponse> CheckSCFRequestStatusInProgress(SCFRequest request)
        {
            SCFResponse response = null;
            try
            {
                response = new SCFResponse();
                response.msgError = "";
                response.isError = true;

                Tbl_UserSession userSession = this._userSessionRepository.GetUserSession(request.userGUID);
                if (userSession == null)
                {
                    response.msgError = "Common.ErrMsg.UserSessionNotFound";
                    return response;
                }

                if (userSession.isTimeout == 1 || userSession.LogoutTime != null)
                {
                    response.msgError = "Common.ErrMsg.UserSessionTimeout";
                    return response;
                }

                Tbl_User user = this._userRepository.FindByUsername(userSession.Username);
                if (user == null)
                {
                    response.msgError = "Common.ErrMsg.UserNotFound";
                    return response;
                }

                if (this.ValidateSCFInprogress(user.OrgID))
                {
                    response.msgError = "Common.ErrMsg.UserInprogress";
                    return response;
                }

                response.isError = false;
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region call vault service
        public async Task<VaultEncryptResponse> GetVaultEncrypt(VaultEncryptRequest request)
        {
            VaultEncryptResponse response = null;
            try
            {
                request.plaintext = CryptoExtension.Base64Encode(request.plaintext);
                response = await this._externalServiceManager.VaultEncryptSerivce(request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        public async Task<VaultDecryptResponse> GetVaultDecrypt(VaultDecryptRequest request)
        {
            VaultDecryptResponse response = null;
            try
            {
                response = await this._externalServiceManager.VaultDecryptSerivce(request);
                response.data.plaintext = CryptoExtension.Base64Decode(response.data.plaintext);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }
        #endregion

        #region Function Validate
        public bool ValidateSCFInprogress(string orgID)
        {
            try
            {
                List<Tbl_SCFRequest> listSCF = this._sCFRequestRepository.GetSCFRequestByOrgID(orgID);
                if (!listSCF.Any())
                {
                    return false;
                }

                if (listSCF.Where(f => f.InProgress == true).Any())
                {
                    return true;
                }

                return false;
            }
            catch (Exception e)
            {
                return true;
            }
        }
        #endregion
    }
}
