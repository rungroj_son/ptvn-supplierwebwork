﻿using SupplierPortal.Data.CustomModels.SCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.SCF
{
    public interface ISCFManager
    {
        Task<bool> CheckAccessEInvoice(string orgID, string userName);

        Task<SCFResponse> SaveSCFRequest(SCFRequest request);

        Task<SCFResponse> CheckSCFRequestStatusInProgress(SCFRequest request);

        Task<VaultEncryptResponse> GetVaultEncrypt(VaultEncryptRequest request);

        Task<VaultDecryptResponse> GetVaultDecrypt(VaultDecryptRequest request);
    }
}
