﻿using Minio;
using SupplierPortal.Data.CustomModels.RegisterSupplier;
using System.Threading.Tasks;

namespace SupplierPortal.BusinessLogic.RegisterSupplier
{
    public interface IRegisterSupplierManager
    {
        Task<RegisterSupplierResponse> SupplierRegister(RegisterSupplierRequest model);

        ContactResponse AddContactRequest(ContactRequest contactModel);

        Task<bool> TestConnectMinioServer();

        Task<bool> TestMinioDownloadAndUploadAttachment(RegAttachment attachment);
    }
}
