﻿using Minio;
using SupplierPortal.Data.CustomModels.RegisterSupplier;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.RegAttachment;
using SupplierPortal.DataAccess.MappingDataSWWAndSC;
using SupplierPortal.DataAccess.RegMigrateAttachmentToSC;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace SupplierPortal.BusinessLogic.RegMigrateAttachmentToSC
{
    public class RegMigrateAttachmentToSCManager : IRegMigrateAttachmentToSCManager
    {
        private IRegMigrateAttachmentToSCRepository _regMigrateAttachmentToSCRepository;
        private IMappingDataSWWAndSCRepository _mappingDataSWWAndSCRepository;
        private IAppConfigRepository _appConfigRepository;
        private IRegAttachmentRepository _regAttachmentRepository;

        private string minio_url;
        private string minio_access_key;
        private string minio_secret_key;
        private string minio_bucket_name;
        private string minio_object_name;
        private string attachmentURL;

        public RegMigrateAttachmentToSCManager()
        {
            _regMigrateAttachmentToSCRepository = new RegMigrateAttachmentToSCRepository();
            _mappingDataSWWAndSCRepository = new MappingDataSWWAndSCRepository();
            _appConfigRepository = new AppConfigRepository();
            _regAttachmentRepository = new RegAttachmentRepository();

            this.minio_url = WebConfigurationManager.AppSettings["minio_url"];
            this.minio_access_key = WebConfigurationManager.AppSettings["minio_access_key"];
            this.minio_secret_key = WebConfigurationManager.AppSettings["minio_secret_key"];
            this.minio_bucket_name = WebConfigurationManager.AppSettings["minio_bucket_name"];
            this.minio_object_name = WebConfigurationManager.AppSettings["minio_object_name"];
            this.attachmentURL = WebConfigurationManager.AppSettings["attachmentURL"];
        }

        public async Task<bool> MigrateAttachmentToSC()
        {
            bool result = false;
            try
            {
                List<Temp_RegMigrateAttachmentToSC> listRegMigrate = new List<Temp_RegMigrateAttachmentToSC>();
                List<Temp_MappingDataSWWAndSC> mappingData = new List<Temp_MappingDataSWWAndSC>();
                List<Tbl_RegAttachment> attachments = new List<Tbl_RegAttachment>();
                List<int> listRegID = new List<int>();

                listRegMigrate = _regMigrateAttachmentToSCRepository.FindByIsMigrateComplete(false);
                mappingData = _mappingDataSWWAndSCRepository.FindAll();

                if (listRegMigrate.Count > 0)
                {
                    foreach (Temp_RegMigrateAttachmentToSC data in listRegMigrate)
                    {
                        listRegID.Add((int)data.RegId);
                    }
                }

                if (listRegMigrate.Count == 0 && mappingData.Count > 0)
                {
                    foreach (Temp_MappingDataSWWAndSC data in mappingData)
                    {
                        listRegID.Add((int)data.SWW_RegId);
                    }
                }

                attachments = _regAttachmentRepository.GetRegAttachmentByManyRegID(listRegID);

                foreach (Tbl_RegAttachment data in attachments)
                {
                    Temp_RegMigrateAttachmentToSC temp = new Temp_RegMigrateAttachmentToSC();
                    temp = await UploadAttachmentToMINIO(data);
                    if (temp != null)
                    {
                        int tempID = _regMigrateAttachmentToSCRepository.Insert(temp);
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public async Task<Temp_RegMigrateAttachmentToSC> UploadAttachmentToMINIO(Tbl_RegAttachment attachment)
        {
            Temp_RegMigrateAttachmentToSC result = null;
            MinioClient minio = null;
            try
            {
                minio = new MinioClient(this.minio_url, this.minio_access_key, this.minio_secret_key);

                bool found = await minio.BucketExistsAsync(this.minio_bucket_name);
                if (!found)
                {
                    return null;
                }
                string pathSave = _appConfigRepository.GetAppConfigByName("Register_AttachmentPath").Value;
                string pathForSave = this.attachmentURL + pathSave + attachment.RegID;
                string filePathSave = pathForSave + "\\" + attachment.AttachmentNameUnique;

                TimeSpan time = DateTime.UtcNow - new DateTime(1970, 1, 1);
                long fileNameEPOCH = (long)time.TotalMilliseconds;

                bool uploadSuccess = await GetAttachmentAndUploadToMINIO(minio, filePathSave, fileNameEPOCH.ToString());

                result = new Temp_RegMigrateAttachmentToSC();

                result.RegId = attachment.RegID;
                result.AttachmentID = attachment.AttachmentID;
                result.AttachmentName = attachment.AttachmentName;
                result.AttachmentNameUnique = attachment.AttachmentNameUnique;
                result.DocumentNameID = attachment.DocumentNameID;
                result.OtherDocument = attachment.OtherDocument;
                result.SC_Name = attachment.AttachmentName;
                result.SC_Extension = attachment.AttachmentNameUnique.Split('.')[1];
                result.SC_Size = Int64.Parse(attachment.SizeAttach);
                result.SC_UniqueName = fileNameEPOCH;
                result.SC_DocumentTypeLv1 = MappingDocumentTypeLV1((int)attachment.DocumentNameID);
                result.SC_DocumentTypeLv2 = MappingDocumentTypeLV2((int)attachment.DocumentNameID);
                result.SC_BranchId = 0;
                result.IsMigrateComplete = uploadSuccess;
            }
            catch (Exception ex)
            {
                return null;
            }

            return result;
        }

        public async Task<bool> GetAttachmentAndUploadToMINIO(MinioClient minio, string pathSave, string fileName)
        {
            bool result = false;

            try
            {
                byte[] pathSabeByte = File.ReadAllBytes(pathSave);
                using (MemoryStream filestream = new MemoryStream(pathSabeByte))
                {
                    string objectName = this.minio_object_name + "/" + fileName;
                    await minio.PutObjectAsync(this.minio_bucket_name, objectName, filestream, filestream.Length);
                    result = true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return result;
        }

        public static string MappingDocumentTypeLV1(int documentID)
        {
            switch (documentID)
            {

                case 1:
                case 14: return "SupplierRegistration";
                case 2: return "MemorandumOfAssociation";
                case 3:
                case 4:
                case 5: return "VATRegistration";
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                    return "Others";
                default: return string.Empty;
            }
        }

        public static string MappingDocumentTypeLV2(int documentID)
        {
            switch (documentID)
            {

                case 1: return "SupplierRegistration";
                case 2: return "MemorandumOfAssociation";
                case 3: return "VATRegistration";
                case 4: return "VATRegistrationPorPor01";
                case 5: return "VATRegistrationPorPor09";
                case 9: return "PowerOfAttorney";
                case 10: return "CompanyProfile";
                case 11: return "CopyOfIdCard";
                case 12: return "CopyOfPassport";
                case 13: return "CopyOfHouseBook";
                case 14: return "SupplierRegistration";
                default: return string.Empty;
            }
        }
    }
}
