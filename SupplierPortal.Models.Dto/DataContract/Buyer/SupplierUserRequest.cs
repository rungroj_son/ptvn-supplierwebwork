﻿using SupplierPortal.Data.CustomModels.BuyerSuppliers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto.DataContract
{
    public class SupplierUserRequest
    {
        public SupplierUserModel SupplierUser { get; set; }
    }
}
