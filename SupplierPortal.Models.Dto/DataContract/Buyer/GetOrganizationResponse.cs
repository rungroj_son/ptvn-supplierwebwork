﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto.DataContract
{
    public class GetOrganizationResponse : BaseReponse
    {
        public Tbl_Organization data { get; set; }
    }
}
