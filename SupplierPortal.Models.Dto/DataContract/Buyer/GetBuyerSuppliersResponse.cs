﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto.DataContract
{
    public class GetBuyerSuppliersResponse : DatatableResponse<Buyer_Suppliers_Result>
    {
    }
}
