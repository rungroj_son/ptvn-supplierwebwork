﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto.DataContract
{
    public class CreateResponse
    {
        public bool Success { get; set; }
        public int Id { get; set; }
        public string Message { get; set; }
    }
}
