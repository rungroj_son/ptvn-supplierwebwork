﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto
{
    public class OPN_OrganizationDto
    {
        public int? buyerSupplierId { get; set; }
        public int? buyerSupplierContactId { get; set; }
        public int? supplierUserId { get; set; }
        public string password { get; set; }
        public string taxId { get; set; }
        public Nullable<decimal> registeredCapital { get; set; }
        public string currency { get; set; }
        public string yearEstablished { get; set; }
        public bool manufacturer { get; set; }
        public bool distributor { get; set; }
        public bool dealer { get; set; }
        public bool serviceProvider { get; set; }
        public string address { get; set; }
        public string province { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public string contactName { get; set; }
        public string department { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string paymentMethod { get; set; }
        public string paymentTerm { get; set; }
        public DateTime? deliveryDate { get; set; }
        public bool supplierDirectory { get; set; }

    }
}
