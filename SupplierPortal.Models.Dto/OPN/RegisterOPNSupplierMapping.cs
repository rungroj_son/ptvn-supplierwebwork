﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto
{
    public class RegisterOPNSupplierMapping
    {
        public int regId { get; set; }
        public string buyerShortName { get; set; }
        public string email { get; set; }
        public int? buyerSupplierId { get; set; }
        public int? buyerSupplierContactId { get; set; }
        public int? supplierUserId { get; set; }
    }
}
