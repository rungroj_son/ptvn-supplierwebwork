﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto
{
    public class Tbl_JobTitleDto
    {
        public int JobTitleID { get; set; }
        public string JobTitleName { get; set; }
        public Nullable<int> SeqNo { get; set; }
    }
}
