﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto
{
    public class BusinessTypeShortDetailDto
    {
        public int BusinessTypeID { get; set; }
        public string businessTypeDesc { get; set; }
    }
}
