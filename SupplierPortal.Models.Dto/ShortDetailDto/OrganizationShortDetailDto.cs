﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Dto
{
    public class OrganizationShortDetailDto
    {
        public string taxId { get; set; }
        public string branchNo { get; set; }
        public bool companyActive { get; set; }
        public Nullable<decimal> registeredCapital { get; set; }
        public string currencyCode { get; set; }
        public string yearEstablished { get; set; }
        public List<BusinessTypeShortDetailDto> businessTypes { get; set; }
        public string address { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public string contactName { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string department { get; set; }

    }
}
