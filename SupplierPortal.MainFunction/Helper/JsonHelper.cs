﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.MainFunction.Helper
{
    public static class JsonHelper
    {

        public static HttpResponseMessage CompleteJson<T>(IEnumerable<T> objData, params HttpResponseMessage[] listError) where T : class
        {
            int code = System.Runtime.InteropServices.Marshal.GetExceptionCode();
            var responsemessage = listError.FirstOrDefault();
            //var responsemessage = listError.Where(x => !x.IsSuccessStatusCode).FirstOrDefault();
            //var i = listError.f(x => x.StatusCode).FirstOrDefault();

            var result = new
            {
                result = new
                {
                    Code = ((Int32?)responsemessage.StatusCode).ToString(),
                    SuccessStatus = responsemessage.IsSuccessStatusCode ? true : false,
                    Message = responsemessage.ReasonPhrase.ToString()
                },
                data = objData.ToList()
            };

            string json = JsonConvert.SerializeObject(result);

            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent(json);
            //response.ReasonPhrase = listError.
            //response.StatusCode = HttpStatusCode.NotFound;
            //{
            //    Content = new StringContent(json)
            //};
            return response;
        }

        public static string CompleteJsonString<T>(IEnumerable<T> objData, params HttpResponseMessage[] listError) where T : class
        {
            int code = System.Runtime.InteropServices.Marshal.GetExceptionCode();
            var responsemessage = listError.FirstOrDefault();
            //var responsemessage = listError.Where(x => !x.IsSuccessStatusCode).FirstOrDefault();
            //var i = listError.f(x => x.StatusCode).FirstOrDefault();

            var result = new
            {
                result = new
                {
                    Code = ((Int32?)responsemessage.StatusCode).ToString(),
                    SuccessStatus = responsemessage.IsSuccessStatusCode ? true : false,
                    Message = responsemessage.ReasonPhrase.ToString()
                },
                data = objData.ToList()
            };

            string json = JsonConvert.SerializeObject(result);

            //HttpResponseMessage response = new HttpResponseMessage();
            //response.Content = new StringContent(json);
            //response.ReasonPhrase = listError.
            //response.StatusCode = HttpStatusCode.NotFound;
            //{
            //    Content = new StringContent(json)
            //};
            return json;
        }
    }
}
