﻿using SupplierPortal.Data.CustomModels.SearchResult;
using SupplierPortal.Models.Enums;
using System.Collections.Generic;
using System.Text;

namespace SupplierPortal.MainFunction.Helper
{
    public static class SearchConditionHelper
    {
        public static string CreateCondition(List<AdvanceSearch> search)
        {
            StringBuilder result = new StringBuilder();

            foreach (AdvanceSearch data in search)
            {
                if (!string.IsNullOrWhiteSpace(data.column))
                    result.Append(MappingCondition(data));
            }

            return result.ToString();
        }

        public static string MappingCondition(AdvanceSearch search)
        {
            string result = string.Empty;
            SearchConditionTypeEnum conditionEnum = MappingConditionTypeEnum(search.conditionType);
            SearchOperatorTypeEnum operatorEnum = MappingOperatorTypeEnum(search.operatorType);

            switch (operatorEnum)
            {
                case SearchOperatorTypeEnum.Like:
                    result = conditionEnum.GetAttributeCode() + MappingColumnTableEntity.MappingColumnAndTable(search.column) + operatorEnum.GetAttributeCode() + " '%" + search.value + "%' ";
                    break;

                case SearchOperatorTypeEnum.Equal:
                case SearchOperatorTypeEnum.NotEqual:
                case SearchOperatorTypeEnum.GreaterEqual:
                case SearchOperatorTypeEnum.LessEqual:
                case SearchOperatorTypeEnum.LessOrEqualWithOwnTime:
                case SearchOperatorTypeEnum.Is:
                case SearchOperatorTypeEnum.GreaterThan:
                case SearchOperatorTypeEnum.LessThan:
                    result = conditionEnum.GetAttributeCode() + MappingColumnTableEntity.MappingColumnAndTable(search.column) + operatorEnum.GetAttributeCode() + " '" + search.value + "' ";
                    break;

                case SearchOperatorTypeEnum.In:
                case SearchOperatorTypeEnum.NotIn:
                    result = conditionEnum.GetAttributeCode() + MappingColumnTableEntity.MappingColumnAndTable(search.column) + operatorEnum.GetAttributeCode() + "('" + string.Join("','", search.listValue.ToArray()) + "')";
                    break;

                default: result = string.Empty; break;
            }

            return result;
        }

        public static SearchConditionTypeEnum MappingConditionTypeEnum(string value)
        {
            if (SearchConditionTypeEnum.And.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchConditionTypeEnum.And;
            }

            if (SearchConditionTypeEnum.Not.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchConditionTypeEnum.Not;
            }

            if (SearchConditionTypeEnum.Or.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchConditionTypeEnum.Or;
            }

            return SearchConditionTypeEnum.And;
        }

        public static SearchOperatorTypeEnum MappingOperatorTypeEnum(string value)
        {
            if (SearchOperatorTypeEnum.Like.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchOperatorTypeEnum.Like;
            }

            if (SearchOperatorTypeEnum.Equal.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchOperatorTypeEnum.Equal;
            }

            if (SearchOperatorTypeEnum.NotEqual.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchOperatorTypeEnum.NotEqual;
            }

            if (SearchOperatorTypeEnum.GreaterEqual.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchOperatorTypeEnum.GreaterEqual;
            }

            if (SearchOperatorTypeEnum.LessEqual.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchOperatorTypeEnum.LessEqual;
            }

            if (SearchOperatorTypeEnum.LessOrEqualWithOwnTime.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchOperatorTypeEnum.LessOrEqualWithOwnTime;
            }

            if (SearchOperatorTypeEnum.Is.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchOperatorTypeEnum.Is;
            }

            if (SearchOperatorTypeEnum.GreaterThan.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchOperatorTypeEnum.GreaterThan;
            }

            if (SearchOperatorTypeEnum.LessThan.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchOperatorTypeEnum.LessThan;
            }

            if (SearchOperatorTypeEnum.In.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchOperatorTypeEnum.In;
            }

            if (SearchOperatorTypeEnum.NotIn.GetAttributeTypeCode().ToLower().Equals(value.ToLower()))
            {
                return SearchOperatorTypeEnum.NotIn;
            }

            return SearchOperatorTypeEnum.Equal;
        }
    }
}
