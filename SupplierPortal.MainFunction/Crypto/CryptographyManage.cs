﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.MainFunction.Helper;

namespace SupplierPortal.MainFunction.Crypto
{
    public static class CryptographyManage
    {

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static string UserPasswordEncode(string username,string password)
        {
            string publicKey = "63d02ea3cd2c48dfbab6e2b07c74654c";

            string result = "";
            //encode password
            result = Base64Encode(password);
            //PublicKey + result password
            result = result + publicKey;
            // encode rusult
            result = Base64Encode(result);
            //strUserName : result 
            result = username + ":" + result;
            //encode result
            result = Base64Encode(result);

            return result;
        }

        public static string UserPasswordDecode(string userPasswordEncode, out string password)
        {
            string publicKey = "63d02ea3cd2c48dfbab6e2b07c74654c";

            string decodeUserPassword = Base64Decode(userPasswordEncode);

            //split Userpassword
            var splitUserpassword = decodeUserPassword.Split(':');

            string userName = "";
            password = "";

            if (splitUserpassword.Length >= 2)
            {
                userName = splitUserpassword[0];
                password = splitUserpassword[1];
            }

            password = Base64Decode(password);

            //Split publicKey
            password = password.Trim().Replace(publicKey, "");

            //Decode Password
            password = Base64Decode(password);

            //คืนค่า Username ตั้งต้น และ คืนค่าแบบ out password
            return userName;
        }

        public static string StringEncode(string plainText)
        {
            string publicKey = "63d02ea3cd2c48dfbab6e2b07c74654c";

            string result = "";

            result = Base64Encode(plainText);

            result = result + publicKey;

            result = Base64Encode(result);


            return result;
        }

        public static string StringDecode(string plainText)
        {
            string result = "";
            string publicKey = "63d02ea3cd2c48dfbab6e2b07c74654c";

            string decodePlainText = Base64Decode(plainText);

            result = decodePlainText.Trim().Replace(publicKey, "");

            result = Base64Decode(result);

            return result;
        }

        public static string EncryptAESBase32(string text, string key, bool padding)
        {
            byte[] encrypt = Aes128Helper.EncryptStringToBytes(
                                text,
                                Encoding.UTF8.GetBytes(key),
                                new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
                             );

            return Base32.Encode(encrypt, padding);
        }

        public static String EncryptAESBase64(String plainText, String key)
        {
            var plainBytes = Encoding.UTF8.GetBytes(plainText);
            return Uri.EscapeDataString(Convert.ToBase64String(Aes128Helper.Encrypt(plainBytes, Aes128Helper.getRijndaelManaged(key))));
        }

        public static String DecryptAESBase64(String encryptedText, String key)
        {
            var encryptedBytes = Convert.FromBase64String(encryptedText);
            return Encoding.UTF8.GetString(Aes128Helper.Decrypt(encryptedBytes, Aes128Helper.getRijndaelManaged(key)));
        }
    }
}
