﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.MainFunction.SupportModel
{
    public partial class CompoundAddressModel
    {
        public string TrackingKey { get; set; }
        public string OldKeyValue { get; set; }
        public string NewKeyValue { get; set; }
    }
}
