﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.BillingDetail;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using SupplierPortal.Data.Models.SupportModel.BillingDetail;
namespace SupplierPortal.Services.BillingManage
{
    public static class BillingManage
    {


        public static IQueryable<Tbl_BillingDetail> BillingDetail
        {
            
            get
            {
                IBillingDetailRepository _repo = null;
                _repo = new BillingDetailRepository();
                return _repo.GetAll().OrderBy(m => m.DocNo);
            }

        }

        public static IQueryable<BillingAndPaymentModels> BillingAndPayment
        {

            get
            {
                IBillingDetailRepository _repo = null;
                _repo = new BillingDetailRepository();
                return _repo.GetBillingAndPaymentAll().OrderBy(m => m.DocNo);
            }

        }


        public static IQueryable<BillingAndPaymentModels> BillingAndPaymentPerform
        {

            get
            {
                IBillingDetailRepository _repo = null;
                _repo = new BillingDetailRepository();
                return _repo.BillingAndPaymentPerform().OrderBy(m => m.DocNo);
            }

        }

        public static IQueryable<BillingAndPaymentModels> BillingAndPaymentPerformBySupplierID(int supplierID)
        {

                IBillingDetailRepository _repo =  new BillingDetailRepository();

                return _repo.BillingAndPaymentPerformBySupplierID(supplierID).OrderBy(m => m.DocNo);


        }
      

        public static IQueryable<Tbl_LocalizedProperty> LocalizedProperty
        {

            get
            {
                ILocalizedPropertyRepository _repo = null;
                _repo = new LocalizedPropertyRepository();
                return _repo.GetLocalizedPropLoadAll();
                //return _repo.GetLocalizedPropLoadAll().OrderBy(m => m.DocNo);
            }

        }
    }
}
