﻿using SupplierPortal.Data.Models.SupportModel.AutoMerge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Services.AutoMergeManage
{
    public partial interface IAutoMergeTransactions
    {
        IEnumerable<MergeUserGroupReadyModel> GetMergUserReady(int limit);

        void MergeUser(MergeUserGroupReadyModel model);

        void UpdateMergeUserNonReady(MergeUserGroupReadyModel model);

        void UpdateUser(string username,int id);
    }
}
