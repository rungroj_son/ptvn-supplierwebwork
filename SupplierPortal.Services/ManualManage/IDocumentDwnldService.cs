﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.SupportModel.Manual;

namespace SupplierPortal.Services.ManualManage
{
    public partial interface IDocumentDwnldService
    {

        ViewDataDocumentManual DocumentManualList();

    }
}
