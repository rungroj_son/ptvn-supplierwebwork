﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.RegInfo;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.OrgContactPerson;
using SupplierPortal.Data.Models.Repository.OrgSystemMapping;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.MessageTemplate;
using SupplierPortal.Data.Models.Repository.EmailAccount;
using SupplierPortal.Data.Models.Repository.RegContact;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.RegAddress;
using SupplierPortal.Data.Models.Repository.RegServiceTypeMapping;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using SupplierPortal.Data.Models.Repository.NameTitle;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.RegContactRequest;
using SupplierPortal.Data.Models.Repository.OPAdoptionNotify;
using SupplierPortal.Data.Models.Repository.OrgAddress;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Data.Models.Repository.BusinessEntity;
using SupplierPortal.Services.AccountManage;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.SupportModel.Register;
using SupplierPortal.Data.Models.Repository.ADSBuyer;
using SupplierPortal.Data.Models.Repository.ADSBuyerSupplierMapping;

namespace SupplierPortal.Services.RegisterPortalManage
{
    public partial class RegisterDataService : IRegisterDataService
    {
        private IRegInfoRepository _regInfoRepository;
        private IOrganizationRepository _organizationRepository;
        private IOrgContactPerson _orgContactPersonRepo;
        private IOrgSystemMappingRepository _orgSystemMappingRepository;
        private IContactPersonRepository _contactPersonRepository;
        private IMessageTemplateRepository _messageTemplateRepository;
        private IEmailAccountRepository _emailAccountRepository;
        private IRegContactRepository _regContactRepository;
        private ICountryRepository _countryRepository;
        private IRegAddressRepository _regAddressRepository;
        private IRegServiceTypeMappingRepository _regServiceTypeMappingRepository;
        private IUserSystemMappingRepository _userSystemMappingRepository;
        private INameTitleRepository _nameTitleRepository;
        private ILanguageRepository _languageRepository;
        private IRegContactRequestRepository _regContactRequestRepository;
        private IOPAdoptionNotifyRepository _oPAdoptionNotifyRepository;
        private IOrgAddressRepository _orgAddressRepository;
        private IAddressRepository _addressRepository;
        private IBusinessEntityRepository _businessEntityRepository;
        private IADSBuyerRepository _aDSBuyerRepository;
        private IADSBuyerSupplierMappingRepository _aDSBuyerSupplierMappingRepository;

        public RegisterDataService()
        {
            _regInfoRepository = new RegInfoRepository();
            _organizationRepository = new OrganizationRepository();
            _orgContactPersonRepo = new OrgContactPerson();
            _orgSystemMappingRepository = new OrgSystemMappingRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _messageTemplateRepository = new MessageTemplateRepository();
            _emailAccountRepository = new EmailAccountRepository();
            _regContactRepository = new RegContactRepository();
            _countryRepository = new CountryRepository();
            _regAddressRepository = new RegAddressRepository();
            _regServiceTypeMappingRepository = new RegServiceTypeMappingRepository();
            _userSystemMappingRepository = new UserSystemMappingRepository();
            _nameTitleRepository = new NameTitleRepository();
            _languageRepository = new LanguageRepository();
            _regContactRequestRepository = new RegContactRequestRepository();
            _oPAdoptionNotifyRepository = new OPAdoptionNotifyRepository();
            _orgAddressRepository = new OrgAddressRepository();
            _addressRepository = new AddressRepository();
            _businessEntityRepository = new BusinessEntityRepository();
            _aDSBuyerRepository = new ADSBuyerRepository();
            _aDSBuyerSupplierMappingRepository = new ADSBuyerSupplierMappingRepository();
        }

        public RegisterDataService(
            RegInfoRepository regInfoRepository,
            OrganizationRepository organizationRepository,
            OrgContactPerson orgContactPersonRepo,
            OrgSystemMappingRepository orgSystemMappingRepository,
            ContactPersonRepository contactPersonRepository,
            MessageTemplateRepository messageTemplateRepository,
            EmailAccountRepository emailAccountRepository,
            RegContactRepository regContactRepository,
            CountryRepository countryRepository,
            RegAddressRepository regAddressRepository,
            RegServiceTypeMappingRepository regServiceTypeMappingRepository,
            UserSystemMappingRepository userSystemMappingRepository,
            NameTitleRepository nameTitleRepository,
            LanguageRepository languageRepository,
            RegContactRequestRepository regContactRequestRepository,
            OPAdoptionNotifyRepository oPAdoptionNotifyRepository,
            OrgAddressRepository orgAddressRepository,
            AddressRepository addressRepository,
            BusinessEntityRepository businessEntityRepository,
            ADSBuyerRepository aDSBuyerRepository,
            ADSBuyerSupplierMappingRepository aDSBuyerSupplierMappingRepository

            )
        {
            _regInfoRepository = regInfoRepository;
            _organizationRepository = organizationRepository;
            _orgContactPersonRepo = orgContactPersonRepo;
            _orgSystemMappingRepository = orgSystemMappingRepository;
            _contactPersonRepository = contactPersonRepository;
            _messageTemplateRepository = messageTemplateRepository;
            _emailAccountRepository = emailAccountRepository;
            _regContactRepository = regContactRepository;
            _countryRepository = countryRepository;
            _regAddressRepository = regAddressRepository;
            _regServiceTypeMappingRepository = regServiceTypeMappingRepository;
            _userSystemMappingRepository = userSystemMappingRepository;
            _nameTitleRepository = nameTitleRepository;
            _languageRepository = languageRepository;
            _regContactRequestRepository = regContactRequestRepository;
            _oPAdoptionNotifyRepository = oPAdoptionNotifyRepository;
            _orgAddressRepository = orgAddressRepository;
            _addressRepository = addressRepository;
            _businessEntityRepository = businessEntityRepository;
            _aDSBuyerRepository = aDSBuyerRepository;
            _aDSBuyerSupplierMappingRepository = aDSBuyerSupplierMappingRepository;
        }

        public virtual bool CheckDuplicateOrganization(string infosconcat)
        {
            return _organizationRepository.CheckDuplicateOrganizationVerifyString(infosconcat);
        }

        public virtual bool CheckDuplicateRegInfo(string infosconcat, string oldRegID)
        {
            return _regInfoRepository.CheckDuplicateRegInfoVerifyString(infosconcat, oldRegID);
        }

        public virtual string GetSupplierIDOrganizationByConcatRegis(string infosconcat)
        {
            string supplierID = "";

            var model = _organizationRepository.GetOrganizationByInfosConcatRegis(infosconcat);

            if (model != null)
            {
                supplierID = model.SupplierID.ToString();
            }
            return supplierID;
        }

        public virtual string GetSupplierIDOrganizationByTaxIDAndBranchNo(string taxID, string branchNo)
        {
            string supplierID = "";

            var model = _organizationRepository.GetOrganizationByTaxIDAndBranchNo(taxID, branchNo);

            if (model != null)
            {
                supplierID = model.SupplierID.ToString();
            }
            return supplierID;
        }

        //public virtual bool CheckOrgContactPersonDuplicate(string FirstName_Contact, string LastName_Contact, int SupplierID, string ServiceOPChecked,out string emailAdminBSP)
        //{
        //    bool chk = false;

        //    emailAdminBSP = "";

        //    if (string.IsNullOrEmpty(ServiceOPChecked))
        //    {
        //        if (_orgContactPersonRepo.CheckOrgContactPersonDuplicate(FirstName_Contact, LastName_Contact, SupplierID))
        //        {
        //            chk = true;
        //        }
        //    }
        //    else
        //    {
        //        var orgMappingModel = _orgSystemMappingRepository.GetOrgSystemMappingBySupplierIdAndSystemId(SupplierID,1);

        //        if (orgMappingModel.Count() > 0)
        //        {
        //            if (_orgContactPersonRepo.CheckOrgContactPersonDuplicate(FirstName_Contact, LastName_Contact, SupplierID))
        //            {
        //                chk = true;
        //            }
        //            else
        //            {
        //                var contactPersonModel = _contactPersonRepository.ContactPersonAdminBSPBySupplierID(SupplierID);

        //                if (contactPersonModel != null)
        //                {
        //                    emailAdminBSP = contactPersonModel.Email;
        //                }
        //            }
        //        }
        //    }

        //    return chk;
        //}

        public virtual bool CheckFullnameContactDuplicateOP(string FirstName_Contact, string LastName_Contact, int SupplierID, string invitationCode, out string emailReturn, out string stringResourceReturn, out string duplicateType)
        {
            bool chk = false;

            emailReturn = "";
            stringResourceReturn = "";
            duplicateType = "";// ประเภทที่ซ้ำ 0 = Message only, 1 = Send mail .Register.ContactBSPAdmin.ReqUser,2 = Send mail . Register.AdoptionNotify , 3 = Send mail .ForgotPassword ,4 = Show Alert InvitationCodeNotFound

            var userModel = _orgContactPersonRepo.GetUserDuplicate(FirstName_Contact, LastName_Contact, SupplierID);

            string supplierOrgID = "";
            var orgModel = _organizationRepository.GetDataBySupplierID(SupplierID);
            if (orgModel != null)
            {
                supplierOrgID = orgModel.OrgID;
            }

            if (userModel.Count() > 0) //ตรวจสอบ ชื่อ-นามสกุล ซ้ำในระบบ (Y)
            {
                foreach (var item in userModel)
                {
                    if (_userSystemMappingRepository.CheckExistsUserSystemMappingByUserIDAndSystemID(item.UserID, 1)) //ตรวจสอบ Service OP ของ User ที่พบ (Y)
                    {
                        #region-------- ตรวจสอบ Service OP ของ User ที่พบ (Y) -----------------
                        if (!string.IsNullOrEmpty(invitationCode))//มีการร้องขอด้วย InvitationCode (Y)
                        {
                            string buyerOrgID = _aDSBuyerRepository.GetBuyerOrgIDByInvitationCode(invitationCode);

                            if (!string.IsNullOrEmpty(buyerOrgID)) //ตรวจสอบความถูกต้องของ InvitationCode (Y)
                            {
                                var adsBuyerSupplierMappingModel = _aDSBuyerSupplierMappingRepository.GetADSBuyerSupplierMappingByBuyerOrgIDAndSupplierOrgID(buyerOrgID, supplierOrgID);
                                if (adsBuyerSupplierMappingModel != null)//ตรวจสอบความสัมพันธ์ระหว่างบริษัทที่ร้องขอกับผู้ซื้อ (Y)
                                {
                                    //Send Email to Admin
                                    stringResourceReturn = "_Regis.RequestUser.ConfirmMsg.NotifyBSPAdmin";
                                    duplicateType = "1";
                                    var contactPersonModel = _contactPersonRepository.ContactPersonAdminBSPBySupplierID(SupplierID);

                                    if (contactPersonModel != null)
                                    {
                                        emailReturn = contactPersonModel.Email;
                                    }
                                    return true;
                                }
                                else
                                {
                                    //ตรวจสอบความสัมพันธ์ระหว่างบริษัทที่ร้องขอกับผู้ซื้อ (N)
                                    //Create Request Adoption ที่ Table Tbl_OPAdoptionNotify แบบ ADS
                                    stringResourceReturn = "_Regis.RequestUser.ConfirmMsg.NotifyOPAdoption";
                                    duplicateType = "2";
                                    //var contactPersonModel = _contactPersonRepository.ContactPersonAdminBSPBySupplierID(SupplierID);
                                    return true;
                                }
                            }
                            else
                            {
                                //ตรวจสอบความถูกต้องของ InvitationCode (N) แจ้ง Message ผ่านหน้าจอ ว่า ไม่พบ InvitationCode ที่กรอกมา
                                stringResourceReturn = "_Regis.RequestUser.InfoMsg.InvitationCodeNotFound";
                                duplicateType = "4";
                                return true;
                            }
                        }
                        else
                        {
                            //มีการร้องขอด้วย InvitationCode (N) **แจ้ง Message ผ่านหน้าจอ ว่า เป็น User ใน Suppier Portal อยู่แล้วและสามารถใช้บริการ OP ได้อยู่แล้ว

                            stringResourceReturn = "_Regis.RequestUser.InfoMsg.DuplicateUserOP";
                            duplicateType = "0";
                            return true;
                        }
                        #endregion
                    }
                    else  //ตรวจสอบ Service OP ของ User ที่พบ (N)
                    {
                        #region-------- ตรวจสอบ Service OP ของ User ที่พบ (N) -----------------

                        if (_orgSystemMappingRepository.CheckExistsOrgSystemMappingBySupplierIdAndSystemId(SupplierID, 1))//ตรวจสอบ Service OP ของบริษัทของ User ที่พบ (Y)
                        {
                            if (!string.IsNullOrEmpty(invitationCode))//มีการร้องขอด้วย InvitationCode (Y)
                            {
                                string buyerOrgID = _aDSBuyerRepository.GetBuyerOrgIDByInvitationCode(invitationCode);
                                if (!string.IsNullOrEmpty(buyerOrgID)) //ตรวจสอบความถูกต้องของ InvitationCode (Y)
                                {
                                    var adsBuyerSupplierMappingModel = _aDSBuyerSupplierMappingRepository.GetADSBuyerSupplierMappingByBuyerOrgIDAndSupplierOrgID(buyerOrgID, supplierOrgID);
                                    if (adsBuyerSupplierMappingModel != null)//ตรวจสอบความสัมพันธ์ระหว่างบริษัทที่ร้องขอกับผู้ซื้อ (Y)
                                    {
                                        //Send Email to Admin
                                        stringResourceReturn = "_Regis.RequestUser.ConfirmMsg.NotifyBSPAdmin";
                                        duplicateType = "1";
                                        var contactPersonModel = _contactPersonRepository.ContactPersonAdminBSPBySupplierID(SupplierID);

                                        if (contactPersonModel != null)
                                        {
                                            emailReturn = contactPersonModel.Email;
                                        }
                                        return true;
                                    }
                                    else //ตรวจสอบความสัมพันธ์ระหว่างบริษัทที่ร้องขอกับผู้ซื้อ (N)
                                    {
                                        //Create Request Adoption ที่ Table Tbl_OPAdoptionNotify แบบ ADS
                                        stringResourceReturn = "_Regis.RequestUser.ConfirmMsg.NotifyOPAdoption";
                                        duplicateType = "2";
                                        //var contactPersonModel = _contactPersonRepository.ContactPersonAdminBSPBySupplierID(SupplierID);
                                        return true;
                                    }
                                }
                                else //ตรวจสอบความถูกต้องของ InvitationCode (N) แจ้ง Message ผ่านหน้าจอ ว่า ไม่พบ InvitationCode ที่กรอกมา
                                {
                                    stringResourceReturn = "_Regis.RequestUser.InfoMsg.InvitationCodeNotFound";
                                    duplicateType = "4";
                                    return true;
                                }
                            }
                            else //มีการร้องขอด้วย InvitationCode (N)
                            {
                                //Send Email to Admin
                                stringResourceReturn = "_Regis.RequestUser.ConfirmMsg.NotifyBSPAdmin";
                                duplicateType = "1";
                                var contactPersonModel = _contactPersonRepository.ContactPersonAdminBSPBySupplierID(SupplierID);

                                if (contactPersonModel != null)
                                {
                                    emailReturn = contactPersonModel.Email;
                                }
                                return true;
                            }
                        }
                        else //ตรวจสอบ Service OP ของบริษัทของ User ที่พบ (N)
                        {
                            if (!string.IsNullOrEmpty(invitationCode))//มีการร้องขอด้วย InvitationCode (Y)
                            {
                                string buyerOrgID = _aDSBuyerRepository.GetBuyerOrgIDByInvitationCode(invitationCode);
                                if (!string.IsNullOrEmpty(buyerOrgID)) //ตรวจสอบความถูกต้องของ InvitationCode (Y)
                                {
                                    //Create Request Adoption ที่ Table Tbl_OPAdoptionNotify แบบ ADS

                                    stringResourceReturn = "_Regis.RequestUser.ConfirmMsg.NotifyOPAdoption";
                                    duplicateType = "2";
                                    return true;
                                }
                                else //ตรวจสอบความถูกต้องของ InvitationCode (N) แจ้ง Message ผ่านหน้าจอ ว่า ไม่พบ InvitationCode ที่กรอกมา
                                {
                                    stringResourceReturn = "_Regis.RequestUser.InfoMsg.InvitationCodeNotFound";
                                    duplicateType = "4";
                                    return true;
                                }
                            }
                            else //มีการร้องขอด้วย InvitationCode (N)
                            {
                                //Create Request Adoption ที่ Table Tbl_OPAdoptionNotify

                                stringResourceReturn = "_Regis.RequestUser.ConfirmMsg.NotifyOPAdoption";
                                duplicateType = "2";
                                //var contactPersonModel = _contactPersonRepository.ContactPersonAdminBSPBySupplierID(SupplierID);
                                return true;
                            }
                        }
                        #endregion
                    }
                }
            }
            else //ตรวจสอบ ชื่อ-นามสกุล ซ้ำในระบบ (N)
            {
                if (!string.IsNullOrEmpty(invitationCode))//มีการร้องขอด้วย InvitationCode (Y)
                {
                    string buyerOrgID = _aDSBuyerRepository.GetBuyerOrgIDByInvitationCode(invitationCode);
                    if (string.IsNullOrEmpty(buyerOrgID)) //ตรวจสอบความถูกต้องของ InvitationCode (N) ** buyerOrgID เป็น ค่าว่าง แสดงว่าไม่ถูกต้อง
                    {
                        //ตรวจสอบความถูกต้องของ InvitationCode (N) แจ้ง Message ผ่านหน้าจอ ว่า ไม่พบ InvitationCode ที่กรอกมา
                        stringResourceReturn = "_Regis.RequestUser.InfoMsg.InvitationCodeNotFound";
                        duplicateType = "4";
                        return true;
                    }
                }
            }

            return chk;
        }

        public virtual bool CheckFullnameContactDuplicateNonOP(string FirstName_Contact, string LastName_Contact, int SupplierID, out string stringResourceReturn, out string usernameDuplicate, out string duplicateType)
        {
            bool chk = false;

            stringResourceReturn = "";
            duplicateType = "";
            usernameDuplicate = "";

            var userModel = _orgContactPersonRepo.GetUserDuplicate(FirstName_Contact, LastName_Contact, SupplierID).FirstOrDefault();

            if (userModel != null)
            {
                usernameDuplicate = userModel.Username;
                duplicateType = "3";
                stringResourceReturn = "_Regis.RequestUser.ConfirmMsg.NotifyForgotPasword";
                return true;
            }

            return chk;
        }

        public virtual bool SendingEmailBSPAdminRequestContact(SendEmailDuplicateContactModels model, string languageId, string emailCC = "")
        {
            try
            {
                var _objmail = GetContentMailForContactBSPAdminReqUser(model, languageId, emailCC);

                EmailService.SendMailToAdmin(_objmail);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual bool SendingEmailAdoptionNotifyExistingUser(int adoptionReqID, string languageId, string emailCC = "")
        {
            try
            {
                var _objmail = GetContentMailForAdoptionNotifyExistingUser(adoptionReqID, languageId, emailCC);

                EmailService.SendMailToAdmin(_objmail);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual bool SendingEmailNewRequestUser(int userReqId, string languageId, string emailCC = "")
        {
            try
            {
                var _objmail = GetContentMailForNewRequestUser(userReqId, languageId, emailCC);

                EmailService.SendMailToAdmin(_objmail);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual bool SendingEmailNewRequestUserPIS(int userReqId, string languageId)
        {
            try
            {
                var _objmail = GetContentMailForNewRequestUserPIS(userReqId, languageId);

                EmailService.SendMailToAdmin(_objmail);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual bool SendingEmailRegisterSubmit(int regID, string languageId)
        {
            try
            {
                var _objmail = GetContentMailForRegisterSubmit(regID, languageId);

                EmailService.SendMailToAdmin(_objmail);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual bool SendingEmailRegisterSubmitPIS(int regID, string languageId)
        {
            try
            {
                var _objmail = GetContentMailForRegisterSubmitPIS(regID, languageId);

                EmailService.SendMailToAdmin(_objmail);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual RegisterModels GetRegisterRegInfoDataByTicketCode(string ticketCode)
        {
            RegisterModels model = new RegisterModels();

            var regInfoModel = _regInfoRepository.GetRegInfoByTicketcode(ticketCode);

            if (regInfoModel != null)
            {
                var compAddress = _regAddressRepository.GetRegAddressMappingByContactID(regInfoModel.MainContactID ?? 0);
                //string countryCode = "";
                //string RegCompanyAddress_HouseNo = "";
                //string RegCompanyAddress_VillageNo = "";
                //string RegCompanyAddress_Lane = "";
                //string RegCompanyAddress_Road = "";
                //string RegCompanyAddress_SubDistrict = "";
                //string RegCompanyAddress_City = "";
                //string RegCompanyAddress_State = "";
                //string RegCompanyAddress_PostalCode = "";
                //string RegCompanyAddress_CountryCode = "";

                //if (compAddress!=null)
                //{
                //    countryCode = compAddress.CountryCode;
                //    RegCompanyAddress_HouseNo = compAddress.HouseNo_Local;
                //}
                string compCountryName = _countryRepository.GetCountryNameByCountryCode(compAddress.CountryCode);

                string regInfoCountryName = _countryRepository.GetCountryNameByCountryCode(regInfoModel.CountryCode);

                var mainContact = _regContactRepository.GetRegContactByContactid(regInfoModel.MainContactID ?? 0).FirstOrDefault();

                List<int> list = new List<int>();
                var serviceMapping = _regServiceTypeMappingRepository.GetRegServiceListByRegID(regInfoModel.RegID);

                foreach (var item in serviceMapping)
                {
                    list.Add(item.ServiceTypeID ?? 0);
                }

                int[] serviceTypeList = list.ToArray();

                var dataInvitationCode = serviceMapping.Where(a => a.ServiceTypeID == 2).FirstOrDefault();
                string invitationNo = "";
                if (dataInvitationCode != null)
                {
                    invitationNo = dataInvitationCode.InvitationCode;
                }

                RegisterModels regisModel = new RegisterModels
                {
                    Proc = "Edit",
                    RegID = regInfoModel.RegID,
                    RegStatusID = regInfoModel.RegStatusID,
                    TicketCode = regInfoModel.TicketCode,
                    CompanyTypeID = regInfoModel.CompanyTypeID,
                    BusinessEntityID = regInfoModel.BusinessEntityID ?? 0,
                    OtherBusinessEntity = regInfoModel.OtherBusinessEntity,
                    CompanyName_Local = regInfoModel.CompanyName_Local,
                    CompanyName_Inter = regInfoModel.CompanyName_Inter,
                    CountryCode = regInfoModel.CountryCode,
                    CountryName = regInfoCountryName,
                    TaxID = regInfoModel.TaxID,
                    BranchNo = regInfoModel.BranchNo,
                    MainBusiness = regInfoModel.MainBusiness,
                    CompanyAddrID = regInfoModel.CompanyAddrID,
                    RegCompanyAddress_HouseNo = compAddress.HouseNo_Local,
                    RegCompanyAddress_VillageNo = compAddress.VillageNo_Local,
                    RegCompanyAddress_Lane = compAddress.Lane_Local,
                    RegCompanyAddress_Road = compAddress.Road_Local,
                    RegCompanyAddress_SubDistrict = compAddress.SubDistrict_Local,
                    RegCompanyAddress_City = compAddress.City_Local,
                    RegCompanyAddress_State = compAddress.State_Local,
                    RegCompanyAddress_PostalCode = compAddress.PostalCode,
                    RegCompanyAddress_CountryCode = compAddress.CountryCode,
                    RegCompanyAddress_CountryName = compCountryName,
                    MainContactID = regInfoModel.MainContactID,
                    NameTitleID = mainContact.TitleID ?? 0,
                    FirstName = mainContact.FirstName_Local,
                    LastName = mainContact.LastName_Local,
                    JobTitleID = mainContact.JobTitleID ?? 0,
                    OtherJobTitle = mainContact.OtherJobTitle,
                    PhoneNoCountryCode = mainContact.PhoneCountryCode,
                    PhoneNo = mainContact.PhoneNo,
                    PhoneExt = mainContact.PhoneExt,
                    MobileNoCountryCode = mainContact.MobileCountryCode,
                    MobileNo = mainContact.MobileNo,
                    Email = mainContact.Email,
                    ConfirmEmail = mainContact.Email,
                    ServiceTypeList = serviceTypeList,
                    RegisteredCapital = regInfoModel.RegisteredCapital,
                    InvetationCode = invitationNo,
                    CurrencyCode = regInfoModel.CurrencyCode
                };
                model = regisModel;
            }

            return model;
        }

        #region------------------------------------------------Function Helper------------------------------------------------------

        private MailModel GetContentMailForContactBSPAdminReqUser(SendEmailDuplicateContactModels model, string languageId, string emailCC)
        {
            try
            {
                string from = "";
                string toAddress = "";

                var messageTemplate = _messageTemplateRepository.GetMessageTemplateByName("Register.ContactBSPAdmin.ReqUser");
                string pkTableMain = messageTemplate.MessageId.ToString();
                string ToEmailAddress = messageTemplate.ToEmailAddress;
                string CcEmailAddress = messageTemplate.CcEmailAddress;
                string BccEmailAddress = messageTemplate.BccEmailAddress;

                if (string.IsNullOrEmpty(CcEmailAddress))
                {
                    CcEmailAddress = emailCC;
                }
                else
                {
                    if (!string.IsNullOrEmpty(emailCC))
                    {
                        CcEmailAddress = CcEmailAddress + "," + emailCC;
                    }
                }

                if (!string.IsNullOrEmpty(model.EmailAdminBSP))
                {
                    toAddress = model.EmailAdminBSP;
                }

                if (!string.IsNullOrEmpty(ToEmailAddress))
                {
                    if (!string.IsNullOrEmpty(toAddress))
                    {
                        toAddress += "," + ToEmailAddress;
                    }
                    else
                    {
                        toAddress += ToEmailAddress;
                    }
                }

                var EmlaccResult = _emailAccountRepository.GetEmailAccountByEmailAccountID(messageTemplate.EmailAccountID ?? 0);

                var EmlaccList = EmlaccResult.FirstOrDefault();
                from = EmlaccList.Email;
                string subject = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Subject", pkTableMain);
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                string body = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Body", pkTableMain);

                var adminBSP = _contactPersonRepository.ContactPersonAdminBSPBySupplierID(Convert.ToInt32(model.SupplierID));

                int localLanguageID = _languageRepository.GetLocalLanguage().LanguageID;

                int interLanguageID = _languageRepository.GetInterLanguage().LanguageID;

                //int languageID = Convert.ToInt32(languageId);

                string ext_Local = SupplierPortal.Framework.Localization.GetLocaleStringResource.GetStringResourceByResourecNameAndLanguageId("_Regis.RequestUser.Popup.ColCaption.PhoneNo.Ext", localLanguageID.ToString());
                string ext_Inter = SupplierPortal.Framework.Localization.GetLocaleStringResource.GetStringResourceByResourecNameAndLanguageId("_Regis.RequestUser.Popup.ColCaption.PhoneNo.Ext", interLanguageID.ToString());

                string nameTitleAdmin_Local = "";
                string nameTitleAdmin_Inter = "";
                string contactNameAdmin_Local = "";
                string contactNameAdmin_Inter = "";
                string telNumberAdmin = "";
                string emailAdmin = "";

                if (adminBSP != null)
                {
                    nameTitleAdmin_Local = _nameTitleRepository.GetTitleNameByIdAndLanguageID(adminBSP.TitleID ?? 0, localLanguageID);
                    nameTitleAdmin_Inter = _nameTitleRepository.GetTitleNameByIdAndLanguageID(adminBSP.TitleID ?? 0, interLanguageID);
                    contactNameAdmin_Local = adminBSP.FirstName_Local ?? "" + "  " + adminBSP.LastName_Local ?? "";
                    contactNameAdmin_Inter = adminBSP.FirstName_Inter ?? "" + "  " + adminBSP.LastName_Inter ?? "";
                    telNumberAdmin = adminBSP.PhoneNo;
                    if (!string.IsNullOrEmpty(adminBSP.PhoneExt))
                    {
                        telNumberAdmin += ext_Local + adminBSP.PhoneExt;
                    }
                    emailAdmin = adminBSP.Email;
                }

                string nameTitleUser_Local = "";
                string nameTitleUser_Inter = "";
                string contactNameUser_Local = "";
                string contactNameUser_Inter = "";
                string telNumberUser = "";
                string emailUser = "";

                nameTitleUser_Local = _nameTitleRepository.GetTitleNameByIdAndLanguageID(model.ContactTitleName, localLanguageID);
                nameTitleUser_Inter = _nameTitleRepository.GetTitleNameByIdAndLanguageID(model.ContactTitleName, interLanguageID);
                contactNameUser_Local = model.FirstName_Contact + "  " + model.LastName_Contact;
                contactNameUser_Inter = model.FirstName_Contact + "  " + model.LastName_Contact;
                telNumberUser = model.ContactPhoneNo;
                if (!string.IsNullOrEmpty(model.ContactPhoneExt))
                {
                    telNumberUser += ext_Local + model.ContactPhoneExt;
                }
                emailUser = model.ContactEmail;

                body = @body.Replace("<%NameTitleAdmin_Local%>", nameTitleAdmin_Local);
                body = @body.Replace("<%ContactNameAdmin_Local%>", contactNameAdmin_Local);
                body = @body.Replace("<%NameTitleUser_Local%>", nameTitleUser_Local);
                body = @body.Replace("<%ContactNameUser_Local%>", contactNameUser_Local);
                body = @body.Replace("<%TelNumberUser%>", telNumberUser);
                body = @body.Replace("<%emailUser%>", emailUser);
                body = @body.Replace("<%TelNumberAdmin%>", telNumberAdmin);
                body = @body.Replace("<%emailAdmin%>", emailAdmin);

                body = @body.Replace("<%NameTitleAdmin_Inter%>", nameTitleAdmin_Inter);
                body = @body.Replace("<%ContactNameAdmin_Inter%>", contactNameAdmin_Inter);
                body = @body.Replace("<%NameTitleUser_Inter%>", nameTitleUser_Inter);
                body = @body.Replace("<%ContactNameUser_Inter%>", contactNameUser_Inter);

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = toAddress,
                    CcEmailAddress = CcEmailAddress,
                    BccEmailAddress = BccEmailAddress,
                    Subject = subject,
                    Body = body,
                    Host = host,
                };

                return _objmail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private MailModel GetContentMailForAdoptionNotifyExistingUser(int adoptionReqID, string languageId, string emailCC)
        {
            try
            {
                string from = "";

                var messageTemplate = _messageTemplateRepository.GetMessageTemplateByName("Register.AdoptionNotify.ExistingUser");
                string pkTableMain = messageTemplate.MessageId.ToString();
                string toEmail = messageTemplate.ToEmailAddress;
                string CcEmailAddress = messageTemplate.CcEmailAddress;
                string BccEmailAddress = messageTemplate.BccEmailAddress;

                if (string.IsNullOrEmpty(CcEmailAddress))
                {
                    CcEmailAddress = emailCC;
                }
                else
                {
                    if (!string.IsNullOrEmpty(emailCC))
                    {
                        CcEmailAddress = CcEmailAddress + "," + emailCC;
                    }
                }

                var EmlaccResult = _emailAccountRepository.GetEmailAccountByEmailAccountID(messageTemplate.EmailAccountID ?? 0);

                var EmlaccList = EmlaccResult.FirstOrDefault();
                from = EmlaccList.Email;
                string subject = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Subject", pkTableMain);
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                string body = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Body", pkTableMain);

                var opNotifyModel = _oPAdoptionNotifyRepository.GetOPAdoptionNotifyByAdoptionReqID(adoptionReqID);

                int localLanguageID = _languageRepository.GetLocalLanguage().LanguageID;

                int interLanguageID = _languageRepository.GetInterLanguage().LanguageID;

                string localCompanyName = "";
                string branchNo = "";
                string taxID = "";
                string address = "";
                string contactName_Local = "";
                string tel = "";
                string fax = "";
                //
                string mobile = "";
                //
                string email = "";

                string businessEntityDisplay = "";

                if (opNotifyModel != null)
                {
                    var orgModel = _organizationRepository.GetDataBySupplierID(opNotifyModel.SupplierID ?? 0);

                    var businessEntityDisplayLocalModel = _businessEntityRepository.GetLocalizedBusinessEntityDisplayByIdAndLanguageID(orgModel.BusinessEntityID ?? 0, localLanguageID);

                    if (orgModel != null)
                    {
                        localCompanyName = orgModel.CompanyName_Local;

                        if (businessEntityDisplayLocalModel != null)
                        {
                            businessEntityDisplay = businessEntityDisplayLocalModel.BusinessEntityDisplay;

                            if (businessEntityDisplayLocalModel.ConcatStrLocal.Trim() == "P")
                            {
                                localCompanyName = businessEntityDisplay + localCompanyName;
                            }
                            else
                            {
                                localCompanyName = localCompanyName + businessEntityDisplay;
                            }
                        }

                        branchNo = orgModel.BranchNo;
                        taxID = orgModel.TaxID;

                        var orgAddress = _orgAddressRepository.GetAddresType(orgModel.SupplierID, 1);
                        if (orgAddress != null)
                        {
                            var addressModel = _addressRepository.GetAddressByAddressID(orgAddress.AddressID);
                            if (addressModel != null)
                            {
                                if (!string.IsNullOrEmpty(addressModel.HouseNo_Local))
                                {
                                    address += " " + addressModel.HouseNo_Local;
                                }

                                if (!string.IsNullOrEmpty(addressModel.VillageNo_Local))
                                {
                                    address += " " + addressModel.VillageNo_Local;
                                }

                                if (!string.IsNullOrEmpty(addressModel.Lane_Local))
                                {
                                    address += " " + addressModel.Lane_Local;
                                }

                                if (!string.IsNullOrEmpty(addressModel.Road_Local))
                                {
                                    address += " " + addressModel.Road_Local;
                                }

                                if (!string.IsNullOrEmpty(addressModel.SubDistrict_Local))
                                {
                                    address += " " + addressModel.SubDistrict_Local;
                                }

                                if (!string.IsNullOrEmpty(addressModel.City_Local))
                                {
                                    address += " " + addressModel.City_Local;
                                }

                                if (!string.IsNullOrEmpty(addressModel.State_Local))
                                {
                                    address += " " + addressModel.State_Local;
                                }

                                if (!string.IsNullOrEmpty(addressModel.PostalCode))
                                {
                                    address += " " + addressModel.PostalCode;
                                }

                                if (!string.IsNullOrEmpty(addressModel.CountryCode))
                                {
                                    address += " " + _countryRepository.GetCountryNameByCountryCode(addressModel.CountryCode);
                                }
                            }
                        }
                    }

                    string titleName_Local = _nameTitleRepository.GetTitleNameByIdAndLanguageID(opNotifyModel.TitleID ?? 0, localLanguageID);
                    contactName_Local = titleName_Local + opNotifyModel.FirstName_Local + "  " + opNotifyModel.LastName_Local;
                    tel = opNotifyModel.PhoneNo;
                    if (!string.IsNullOrEmpty(opNotifyModel.PhoneExt))
                    {
                        tel += " ต่อ " + opNotifyModel.PhoneExt;
                    }
                    fax = opNotifyModel.FaxExt;
                    if (!string.IsNullOrEmpty(opNotifyModel.FaxExt))
                    {
                        fax += " ต่อ " + opNotifyModel.FaxExt;
                    }
                    mobile = opNotifyModel.MobileNo;
                    email = opNotifyModel.Email;
                }

                subject = subject.Replace("<%LocalCompanyName%>", localCompanyName);
                subject = subject.Replace("<%BranchNo%>", branchNo);

                body = @body.Replace("<%TaxID%>", taxID);
                body = @body.Replace("<%LocalCompanyName%>", localCompanyName);
                body = @body.Replace("<%BranchNo%>", branchNo);
                body = @body.Replace("<%Address%>", address);
                body = @body.Replace("<%ContactName_Local%>", contactName_Local);
                body = @body.Replace("<%Tel%>", tel);
                body = @body.Replace("<%Fax%>", fax);
                body = @body.Replace("<%Mobile%>", mobile);
                body = @body.Replace("<%Email%>", email);

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = toEmail,
                    CcEmailAddress = CcEmailAddress,
                    BccEmailAddress = BccEmailAddress,
                    Subject = subject,
                    Body = body,
                    Host = host,
                };

                return _objmail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private MailModel GetContentMailForNewRequestUser(int userReqId, string languageId, string emailCC)
        {
            try
            {
                string from = "";

                var messageTemplate = _messageTemplateRepository.GetMessageTemplateByName("Register.NewRequestUser");
                string pkTableMain = messageTemplate.MessageId.ToString();
                string CcEmailAddress = messageTemplate.CcEmailAddress;
                string BccEmailAddress = messageTemplate.BccEmailAddress;
                string toAddress = messageTemplate.ToEmailAddress;

                if (string.IsNullOrEmpty(CcEmailAddress))
                {
                    CcEmailAddress = emailCC;
                }
                else
                {
                    if (!string.IsNullOrEmpty(emailCC))
                    {
                        CcEmailAddress = CcEmailAddress + "," + emailCC;
                    }
                }

                var EmlaccResult = _emailAccountRepository.GetEmailAccountByEmailAccountID(messageTemplate.EmailAccountID ?? 0);

                var EmlaccList = EmlaccResult.FirstOrDefault();
                from = EmlaccList.Email;
                string subject = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Subject", pkTableMain);
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                string body = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Body", pkTableMain);

                var regContactRequest = _regContactRequestRepository.GetRegContactRequestByUserReqId(userReqId);

                int localLanguageID = _languageRepository.GetLocalLanguage().LanguageID;

                int interLanguageID = _languageRepository.GetInterLanguage().LanguageID;

                string fullContactName_Local = "";
                string fullContactName_Inter = "";

                if (regContactRequest != null)
                {
                    string titleName_Local = _nameTitleRepository.GetTitleNameByIdAndLanguageID(regContactRequest.TitleID ?? 0, localLanguageID);
                    fullContactName_Local = titleName_Local + regContactRequest.FirstName_Local + "  " + regContactRequest.LastName_Local;
                    string titleName_Inter = _nameTitleRepository.GetTitleNameByIdAndLanguageID(regContactRequest.TitleID ?? 0, interLanguageID);
                    fullContactName_Inter = titleName_Inter + regContactRequest.FirstName_Inter + "  " + regContactRequest.LastName_Inter;
                }

                body = @body.Replace("<%ContactName_Local%>", fullContactName_Local);
                body = @body.Replace("<%ContactName_Inter%>", fullContactName_Inter);

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = toAddress,
                    CcEmailAddress = CcEmailAddress,
                    BccEmailAddress = BccEmailAddress,
                    Subject = subject,
                    Body = body,
                    Host = host,
                };

                return _objmail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private MailModel GetContentMailForNewRequestUserPIS(int userReqId, string languageId)
        {
            try
            {
                string from = "";

                var messageTemplate = _messageTemplateRepository.GetMessageTemplateByName("Register.NewRequestUser.PIS");
                string pkTableMain = messageTemplate.MessageId.ToString();
                string ToEmailAddress = messageTemplate.ToEmailAddress;
                string CcEmailAddress = messageTemplate.CcEmailAddress;
                string BccEmailAddress = messageTemplate.BccEmailAddress;
                string toAddress = messageTemplate.ToEmailAddress;

                var EmlaccResult = _emailAccountRepository.GetEmailAccountByEmailAccountID(messageTemplate.EmailAccountID ?? 0);

                var EmlaccList = EmlaccResult.FirstOrDefault();
                from = EmlaccList.Email;
                string subject = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Subject", pkTableMain);
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                string body = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Body", pkTableMain);

                var regContactRequest = _regContactRequestRepository.GetRegContactRequestByUserReqId(userReqId);

                int localLanguageID = _languageRepository.GetLocalLanguage().LanguageID;

                int interLanguageID = _languageRepository.GetInterLanguage().LanguageID;

                string localCompanyName = "";
                string branchNo = "";
                string contactName_Local = "";

                string businessEntityDisplay = "";

                if (regContactRequest != null)
                {
                    var orgModel = _organizationRepository.GetDataBySupplierID(regContactRequest.SupplierID ?? 0);

                    var businessEntityDisplayLocalModel = _businessEntityRepository.GetLocalizedBusinessEntityDisplayByIdAndLanguageID(orgModel.BusinessEntityID ?? 0, localLanguageID);

                    if (orgModel != null)
                    {
                        localCompanyName = orgModel.CompanyName_Local;
                        branchNo = orgModel.BranchNo;

                        if (businessEntityDisplayLocalModel != null)
                        {
                            businessEntityDisplay = businessEntityDisplayLocalModel.BusinessEntityDisplay;

                            if (businessEntityDisplayLocalModel.ConcatStrLocal.Trim() == "P")
                            {
                                localCompanyName = businessEntityDisplay + localCompanyName;
                            }
                            else
                            {
                                localCompanyName = localCompanyName + businessEntityDisplay;
                            }
                        }
                    }
                    string titleName_Local = _nameTitleRepository.GetTitleNameByIdAndLanguageID(regContactRequest.TitleID ?? 0, localLanguageID);
                    contactName_Local = titleName_Local + regContactRequest.FirstName_Local + "  " + regContactRequest.LastName_Local;
                }

                DateTime thisDateNow = DateTime.Now;
                string today = thisDateNow.ToString("dd/MM/yyyy H:mm:ss");

                subject = subject.Replace("<%LocalCompanyName%>", localCompanyName);
                subject = subject.Replace("<%UserReqId%>", userReqId.ToString());

                body = @body.Replace("<%ExportedDate%>", today);

                body = @body.Replace("<%UserReqId%>", userReqId.ToString());
                body = @body.Replace("<%LocalCompanyName%>", localCompanyName);
                body = @body.Replace("<%BranchNo%>", branchNo);
                body = @body.Replace("<%ContactName_Local%>", contactName_Local);

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = ToEmailAddress,
                    CcEmailAddress = CcEmailAddress,
                    BccEmailAddress = BccEmailAddress,
                    Subject = subject,
                    Body = body,
                    Host = host,
                };

                return _objmail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private MailModel GetContentMailForRegisterSubmit(int regID, string languageId)
        {
            try
            {
                string from = "";

                string toEmail = "";

                var messageTemplate = _messageTemplateRepository.GetMessageTemplateByName("Register.Submit");
                string pkTableMain = messageTemplate.MessageId.ToString();
                string ToEmailAddress = messageTemplate.ToEmailAddress;
                string CcEmailAddress = messageTemplate.CcEmailAddress;
                string BccEmailAddress = messageTemplate.BccEmailAddress;

                if (!string.IsNullOrEmpty(ToEmailAddress))
                {
                    toEmail = ToEmailAddress;
                }

                var EmlaccResult = _emailAccountRepository.GetEmailAccountByEmailAccountID(messageTemplate.EmailAccountID ?? 0);

                var EmlaccList = EmlaccResult.FirstOrDefault();
                from = EmlaccList.Email;
                string subject = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Subject", pkTableMain);
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                string body = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Body", pkTableMain);

                var regInfoModel = _regInfoRepository.GetRegInfoByRegID(regID).FirstOrDefault();

                int localLanguageID = _languageRepository.GetLocalLanguage().LanguageID;

                int interLanguageID = _languageRepository.GetInterLanguage().LanguageID;

                string fullContact_Local = "";
                string fullContact_Inter = "";
                string ticketCode = "";

                if (regInfoModel != null)
                {
                    ticketCode = regInfoModel.TicketCode;

                    var mainContactModel = _regContactRepository.GetRegContactByContactid(regInfoModel.MainContactID ?? 0).FirstOrDefault();

                    if (mainContactModel != null)
                    {
                        if (!string.IsNullOrEmpty(toEmail))
                        {
                            toEmail += "," + mainContactModel.Email;
                        }
                        else
                        {
                            toEmail += mainContactModel.Email;
                        }

                        string titleName_Local = _nameTitleRepository.GetTitleNameByIdAndLanguageID(mainContactModel.TitleID ?? 0, localLanguageID);
                        fullContact_Local = titleName_Local + mainContactModel.FirstName_Local + "  " + mainContactModel.LastName_Local;
                        string titleName_Inter = _nameTitleRepository.GetTitleNameByIdAndLanguageID(mainContactModel.TitleID ?? 0, interLanguageID);
                        fullContact_Inter = titleName_Inter + mainContactModel.FirstName_Inter + "  " + mainContactModel.LastName_Inter;
                    }
                }

                body = @body.Replace("<%ContactName_Local%>", fullContact_Local);
                body = @body.Replace("<%ContactName_Inter%>", fullContact_Inter);
                body = @body.Replace("<%TicketCode%>", ticketCode);

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = toEmail,
                    CcEmailAddress = CcEmailAddress,
                    BccEmailAddress = BccEmailAddress,
                    Subject = subject,
                    Body = body,
                    Host = host,
                };

                return _objmail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private MailModel GetContentMailForRegisterSubmitPIS(int regID, string languageId)
        {
            try
            {
                string from = "";

                var messageTemplate = _messageTemplateRepository.GetMessageTemplateByName("Register.Submit.PIS");
                string pkTableMain = messageTemplate.MessageId.ToString();
                string ToEmailAddress = messageTemplate.ToEmailAddress;
                string CcEmailAddress = messageTemplate.CcEmailAddress;
                string BccEmailAddress = messageTemplate.BccEmailAddress;

                var EmlaccResult = _emailAccountRepository.GetEmailAccountByEmailAccountID(messageTemplate.EmailAccountID ?? 0);

                var EmlaccList = EmlaccResult.FirstOrDefault();
                from = EmlaccList.Email;

                string subject = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Subject", pkTableMain);
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                string body = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Body", pkTableMain);

                var regInfoModel = _regInfoRepository.GetRegInfoByRegID(regID).FirstOrDefault();

                int localLanguageID = _languageRepository.GetLocalLanguage().LanguageID;

                int interLanguageID = _languageRepository.GetInterLanguage().LanguageID;

                string localCompanyName = "";
                string businessEntityDisplay = "";
                string branchNo = "";
                string contactName_Local = "";
                string ticketCode = "";

                if (regInfoModel != null)
                {
                    var businessEntityDisplayLocalModel = _businessEntityRepository.GetLocalizedBusinessEntityDisplayByIdAndLanguageID(regInfoModel.BusinessEntityID ?? 0, localLanguageID);

                    localCompanyName = regInfoModel.CompanyName_Local;

                    if (businessEntityDisplayLocalModel != null)
                    {
                        businessEntityDisplay = businessEntityDisplayLocalModel.BusinessEntityDisplay;

                        if (businessEntityDisplayLocalModel.ConcatStrLocal.Trim() == "P")
                        {
                            localCompanyName = businessEntityDisplay + localCompanyName;
                        }
                        else
                        {
                            localCompanyName = localCompanyName + businessEntityDisplay;
                        }
                    }

                    branchNo = regInfoModel.BranchNo;
                    ticketCode = regInfoModel.TicketCode;

                    var mainContactModel = _regContactRepository.GetRegContactByContactid(regInfoModel.MainContactID ?? 0).FirstOrDefault();

                    if (mainContactModel != null)
                    {
                        string titleName_Local = _nameTitleRepository.GetTitleNameByIdAndLanguageID(mainContactModel.TitleID ?? 0, localLanguageID);
                        contactName_Local = titleName_Local + mainContactModel.FirstName_Local + "  " + mainContactModel.LastName_Local;
                    }
                }

                DateTime thisDateNow = DateTime.Now;
                string today = thisDateNow.ToString("dd/MM/yyyy H:mm:ss");

                subject = @subject.Replace("<%LocalCompanyName%>", localCompanyName);
                subject = @subject.Replace("<%RegID%>", regID.ToString());

                body = @body.Replace("<%ExportedDate%>", today);

                body = @body.Replace("<%RegID%>", regID.ToString());
                body = @body.Replace("<%LocalCompanyName%>", localCompanyName);
                body = @body.Replace("<%BranchNo%>", branchNo);
                body = @body.Replace("<%TicketCode%>", ticketCode);

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = ToEmailAddress,
                    CcEmailAddress = CcEmailAddress,
                    BccEmailAddress = BccEmailAddress,
                    Subject = subject,
                    Body = body,
                    Host = host,
                };

                return _objmail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}