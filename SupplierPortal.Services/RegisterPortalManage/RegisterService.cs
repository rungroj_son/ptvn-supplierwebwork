﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.DocumentNoFormat;
using SupplierPortal.Data.Models.Repository.RegInfo;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.MessageTemplate;
using SupplierPortal.Data.Models.Repository.RegEmailTicket;
using SupplierPortal.Data.Models.Repository.EmailAccount;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using SupplierPortal.Services.AccountManage;
using SupplierPortal.Data.Models.Repository.Product;
using SupplierPortal.Data.Models.Repository.OrgContactPerson;
using SupplierPortal.Data.Models.Repository.RegAttachment;
using SupplierPortal.Data.Models.Repository.BusinessType;
using SupplierPortal.Data.Models.Repository.ProductType;
using SupplierPortal.Data.Models.Repository.RegProduct;
using SupplierPortal.Data.Models.Repository.CustomerDocNameMapping;
using SupplierPortal.Data.Models.Repository.CustomerDocType;
using SupplierPortal.Data.Models.Repository.NameTitle;
using SupplierPortal.Data.Models.Repository.JobTitle;
using System.Web;

namespace SupplierPortal.Services.RegisterPortalManage
{
    public static class RegisterService
    {
        public static IEnumerable<Tbl_DocumentNoFormat> GetPatternByDocName(this string docName)
        {
            IDocumentNoFormatRepository _repo = new DocumentNoFormatRepository();
            var result = _repo.GetDocumentNoFormatListBy(docName);
            _repo.Dispose();
            return result;
        }

        public static string GetLanguageCodeById(this string languageId)
        {
            ILanguageRepository _repo = new LanguageRepository();
            var result = _repo.GetLanguageCodeById(Convert.ToInt32(languageId));
            _repo.Dispose();
            return result;
        }

        public static bool IsTaxIDDuplicate(this string taxid)
        {
            IOrganizationRepository _repo = new OrganizationRepository();
            var result = _repo.GetDataByTaxID(taxid);
            _repo.Dispose();
            return result.Count() == 0 ? false : true; 
        }
        
        public static bool Insert(this RegisterPortalModel model, string ticketcode, string languageCode)
        {
            IRegInfoRepository _repo = new RegInfoRepository();
            var result = _repo.Insert(model, ticketcode, languageCode);
            _repo.Dispose();
            return result;
        }

        //public static bool InsertPart2(this RegisContinuePortalModel model, string languageCode)
        //{
        //    IRegInfoRepository _repo = new RegInfoRepository();
        //    var result = _repo.InsertPart2(model, languageCode);
        //    _repo.Dispose();
        //    return result;
        //}

        public static string RegInfoTaxIDDuplicateStatus(this string taxid)
        {
            IRegInfoRepository _repo = new RegInfoRepository();
            var result = _repo.GetRegInfoListByTaxID(taxid);
            string status = "";
            _repo.Dispose();
            if (result.Count() > 0)
            {
                var list = result.FirstOrDefault();
                status = list.RegStatusID.ToString();
                return status == "1" ? "RegInfo Warnning Duplicate" : "RegInfo Duplicate";
            }

            return "Isn't duplicate";
        }

        public static bool OrganizationIsDuplicateStatus(this string infosconcat)
        {
            IOrganizationRepository _repo = new OrganizationRepository();
            if (_repo.GetOrganizationByInfosConcat(infosconcat).Count() > 0)
            {
                return true;
            }

            return false;
        }

        public static string GetSupplierIDOrganizationByInfosconcat(this string lable, string infosconcat)
        {
            IOrganizationRepository _repo = new OrganizationRepository();

            string supplierID = "";

            var model = _repo.GetOrganizationByInfosConcat(infosconcat).FirstOrDefault();


            if (model != null)
            {
                supplierID = model.SupplierID.ToString();
            }

            return supplierID;
        }

        public static string RegInfoDuplicateStatus(this string infosconcat)
        {
            IRegInfoRepository _repo = new RegInfoRepository();
            var result = _repo.GetRegInfoByInfosConcat(infosconcat);
            string status = "";
            _repo.Dispose();
            if (result.Count() > 0)
            {
                var list = result.FirstOrDefault();
                status = list.RegStatusID.ToString();
                return status == "1" ? "RegInfo Warnning Duplicate" : "RegInfo Duplicate";
            }

            return "Isn't duplicate";
        }

        public static void TicketSendingToEmail(string ticketcode)
        {
            try
            {
                string email = "", from = "";
                string contactName = "";

                IRegEmailTicketRepository _repoEmailTicket = new RegEmailTicketRepository();
                var result = _repoEmailTicket.GetEmailTicketDataListByTicketcode(ticketcode);
                _repoEmailTicket.Dispose();

                

                var emltList = result.FirstOrDefault();
                email = emltList.RegisteredEmail;

                ILanguageRepository _repoLanguage = new LanguageRepository();
                string languageID = _repoLanguage.GetLanguageIdByCode(emltList.LanguageCode);
                _repoLanguage.Dispose();

                IMessageTemplateRepository _repoMessageTemplate = new MessageTemplateRepository();
                var messageTemplate = _repoMessageTemplate.GetMessageTemplateByName("Register.TicketSending");
                _repoMessageTemplate.Dispose();
                string pkTableMain = messageTemplate.MessageId.ToString();

                IEmailAccountRepository _repoEmlacc = new EmailAccountRepository();
                var EmlaccResult = _repoEmlacc.GetEmailAccountByEmailAccountID(Convert.ToInt32(messageTemplate.EmailAccountID));
                _repoEmlacc.Dispose();

                var EmlaccList = EmlaccResult.FirstOrDefault();
                from = EmlaccList.Email;

                IRegInfoRepository _repoRegInfo = new RegInfoRepository();
                var regModel = _repoRegInfo.GetRegInfoListByTicketcode(ticketcode).FirstOrDefault();

                if (regModel != null)
                {
                    //contactName = regModel.FirstName + "  " + regModel.LastName;
                }
                string baseURLWeb = System.Configuration.ConfigurationManager.AppSettings.Get("baseURLWeb");

                string subject = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_MessageTemplate", "Subject", pkTableMain);
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                string body = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_MessageTemplate", "Body", pkTableMain);
                body = @body.Replace("<%TicketCode%>", ticketcode);
                body = @body.Replace("<%ContactName%>", contactName);
                body = @body.Replace("<%UrlLink%>", baseURLWeb);

                //"192.168.2.11"

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = email,
                    Subject = subject,
                    Body = body,
                    Host = host,

                };

                EmailService.SendMail(_objmail);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static bool SendingEmailToSupplierAdmin(string NameTitle, string FirstName, string LastName, string Email, string MailContent, string languageId, string EmailTo)
        {
            try
            {
                string from = "";

                IMessageTemplateRepository _repoMessageTemplate = new MessageTemplateRepository();
                var messageTemplate = _repoMessageTemplate.GetMessageTemplateByName("Register.ContactSupplierAdmin");
                _repoMessageTemplate.Dispose();
                string pkTableMain = messageTemplate.MessageId.ToString();
                string CcEmailAddress = messageTemplate.CcEmailAddress;
                string BccEmailAddress = messageTemplate.BccEmailAddress;

                IEmailAccountRepository _repoEmlacc = new EmailAccountRepository();
                var EmlaccResult = _repoEmlacc.GetEmailAccountByEmailAccountID(Convert.ToInt32(messageTemplate.EmailAccountID));
                _repoEmlacc.Dispose();

                var EmlaccList = EmlaccResult.FirstOrDefault();
                from = EmlaccList.Email;

                string subject = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Subject", pkTableMain);
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                string body = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Body", pkTableMain);
                body = @body.Replace("<%NameTitle%>", NameTitle);
                body = @body.Replace("<%FirstName%>", FirstName);
                body = @body.Replace("<%LastName%>", LastName);
                body = @body.Replace("<%Email%>", Email);
                body = @body.Replace("<%MailContent%>", MailContent);

                //"192.168.2.11"

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = EmailTo,
                    CcEmailAddress = CcEmailAddress,
                    BccEmailAddress = BccEmailAddress,
                    Subject = subject,
                    Body = body,
                    Host = host,
                };

                EmailService.SendMailToAdmin(_objmail);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public static bool SendingEmailContactSupplierAdmin(SendingMailModel sendingmail,string languageId)
        {
            try
            {
                if (sendingmail.ChkAddContactOrg)
                {
                    var _objmail = GetContentMailForContactSupplierAdminReqUser(sendingmail, languageId);

                    EmailService.SendMailToAdmin(_objmail);

                }else
                {
                    var _objmail = GetContentMailForContactSupplierAdmin(sendingmail, languageId);

                    EmailService.SendMailToAdmin(_objmail);
                }
                

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static MailModel GetContentMailForContactSupplierAdmin(SendingMailModel sendingmail, string languageId)
        {
            try
            {
                string from = "";

                IMessageTemplateRepository _repoMessageTemplate = new MessageTemplateRepository();
                var messageTemplate = _repoMessageTemplate.GetMessageTemplateByName("Register.ContactSupplierAdmin");
                _repoMessageTemplate.Dispose();
                string pkTableMain = messageTemplate.MessageId.ToString();
                string CcEmailAddress = messageTemplate.CcEmailAddress;
                string BccEmailAddress = messageTemplate.BccEmailAddress;

                IEmailAccountRepository _repoEmlacc = new EmailAccountRepository();
                var EmlaccResult = _repoEmlacc.GetEmailAccountByEmailAccountID(Convert.ToInt32(messageTemplate.EmailAccountID));
                _repoEmlacc.Dispose();

                var EmlaccList = EmlaccResult.FirstOrDefault();
                from = EmlaccList.Email;

                string subject = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Subject", pkTableMain);
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                string body = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Body", pkTableMain);
                body = @body.Replace("<%CompanyAdmin%>", sendingmail.AdminName);
                body = @body.Replace("<%TextContent%>", sendingmail.MailContent);

                //"192.168.2.11"

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = sendingmail.EmailTo,
                    CcEmailAddress = CcEmailAddress,
                    BccEmailAddress = BccEmailAddress,
                    Subject = subject,
                    Body = body,
                    Host = host,
                };

                return _objmail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static MailModel GetContentMailForContactSupplierAdminReqUser(SendingMailModel sendingmail, string languageId)
        {
            try
            {
                string from = "";

                IMessageTemplateRepository _repoMessageTemplate = new MessageTemplateRepository();
                var messageTemplate = _repoMessageTemplate.GetMessageTemplateByName("Register.ContactSupplierAdmin.ReqUser");
                _repoMessageTemplate.Dispose();
                string pkTableMain = messageTemplate.MessageId.ToString();
                string CcEmailAddress = messageTemplate.CcEmailAddress;
                string BccEmailAddress = messageTemplate.BccEmailAddress;

                IEmailAccountRepository _repoEmlacc = new EmailAccountRepository();
                var EmlaccResult = _repoEmlacc.GetEmailAccountByEmailAccountID(Convert.ToInt32(messageTemplate.EmailAccountID));
                _repoEmlacc.Dispose();

                var EmlaccList = EmlaccResult.FirstOrDefault();
                from = EmlaccList.Email;

                
                string titleName = "";
                if(sendingmail.ContactTitleName != null)
                {
                    INameTitleRepository _repTitleName = new NameTitleRepository();
                    titleName = _repTitleName.GetTitleNameByIdAndLanguageID(sendingmail.ContactTitleName, Convert.ToInt32(languageId));
                }

                string jobTitleName = "";
                if (sendingmail.ContactJobTitle != null)
                {
                    if (sendingmail.ContactJobTitle== -1)
                    {
                        jobTitleName = sendingmail.ContactOtherJobTitle;
                    }else
                    {
                        IJobTitleRepository _repoJobTitle = new JobTitleRepository();
                        jobTitleName = _repoJobTitle.GetJobTitleNameByIdAndLanguageID(sendingmail.ContactJobTitle, Convert.ToInt32(languageId));
                    }
                  
                }
                

                string subject = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Subject", pkTableMain);
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                string body = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Body", pkTableMain);
                body = @body.Replace("<%CompanyAdmin%>", sendingmail.AdminName);
                body = @body.Replace("<%TextContent%>", sendingmail.MailContent);
                body = @body.Replace("<%Email%>", sendingmail.ContactEmail??"");
                body = @body.Replace("<%TitleName%>", titleName);
                body = @body.Replace("<%FirstName_Local%>", sendingmail.FirstName_Local);
                body = @body.Replace("<%LastName_Local%>", sendingmail.LastName_Local);
                body = @body.Replace("<%FirstName_Inter%>", sendingmail.FirstName_Inter);
                body = @body.Replace("<%LastName_Inter%>", sendingmail.LastName_Inter);
                body = @body.Replace("<%JobTitle%>", jobTitleName);
                body = @body.Replace("<%Department%>", sendingmail.ContactDepartment);
                body = @body.Replace("<%PhoneNo%>", sendingmail.ContactPhoneNo);
                body = @body.Replace("<%PhoneExt%>", sendingmail.ContactPhoneExt);
                body = @body.Replace("<%Mobile%>", sendingmail.ContactMobile);
                body = @body.Replace("<%FaxNo%>", sendingmail.ContactFaxNo);
                body = @body.Replace("<%FaxExt%>", sendingmail.ContactFaxExt);

                //"192.168.2.11"

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = sendingmail.EmailTo,
                    CcEmailAddress = CcEmailAddress,
                    BccEmailAddress = BccEmailAddress,
                    Subject = subject,
                    Body = body,
                    Host = host,
                };

                return _objmail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static IQueryable<TempProductModel> GetQrbMultiLangProductByLangid(int _langid)
        //{
        //    IProductRepository _repo = new ProductRepository();
        //    return _repo.GetQrbMultiLangProductByLangid(_langid);
        //}


        public static IEnumerable<Tbl_RegAttachment> GetRegAttachmentByRegIDAndDocumentTypeID(int RegID, int DocumentTypeID)
        {
            IRegAttachmentRepository _repo = new RegAttachmentRepository();
            var models = _repo.GetRegAttachmentByRegIDAndDocumentTypeID(RegID, DocumentTypeID);

            return models;
        }

        public static RegAttachmentListModels GetRegAttachmentListByRegIDAndDocumentTypeID(int RegID, int DocumentTypeID, int CompanyTypeID)
        {
            IRegAttachmentRepository _repo = new RegAttachmentRepository();
            var models = _repo.GetRegAttachmentListByRegIDAndDocumentTypeID(RegID, DocumentTypeID, CompanyTypeID);

            return models;
        }

        public static bool CheckAttachmentIsExists(int RegID, int documentNameID)
        {
  
            IRegAttachmentRepository _regAttachmentRepository = new RegAttachmentRepository();
            bool chk = _regAttachmentRepository.RegAttachmentExistsByDocumentNameID(documentNameID, RegID);



            return chk;
        }

        public static bool CheckAttachmentIsExistsRequire(int RegID, int documentNameID, int companyTypeID, string countryCode)
        {

            IRegAttachmentRepository _regAttachmentRepository = new RegAttachmentRepository();
            ICustomerDocNameMappingRepository _customerDocNameMappingRepository = new CustomerDocNameMappingRepository();

            bool chk = false;

            var customerDocNameRequire = _customerDocNameMappingRepository.GetCustomerDocNameMappingRequireByCompTypeAndCountryCode(companyTypeID, countryCode);

            if (customerDocNameRequire != null)
            {
                bool checkRequire = false;

                foreach (var itemlist in customerDocNameRequire.Where(m => m.DocumentNameID == documentNameID))
                {
                    checkRequire = _regAttachmentRepository.RegAttachmentExistsByDocumentNameID(itemlist.DocumentNameID, RegID);

                    if (!checkRequire)
                    {
                        if (!string.IsNullOrEmpty(itemlist.ListDocumentNameID))
                        {
                            string[] strTemp = itemlist.ListDocumentNameID.Split(',');

                            foreach (var a in strTemp)
                            {
                                if (!string.IsNullOrEmpty(a))
                                {
                                    bool checkRequireCompensate = _regAttachmentRepository.RegAttachmentExistsByDocumentNameID(Convert.ToInt32(a), RegID);

                                    if (checkRequireCompensate)
                                    {
                                        checkRequire = true;
                                        return checkRequire;
                                        
                                    }
                                }
                            }
                        }
                    }else
                    {
                        return true;
                    }
                }
            }
            



            return chk;
        }


        public static bool CheckDocumentTypeIsRequire(int documentTypeID, int companyTypeID, string countryCode)
        {

            ICustomerDocTypeRepository _customerDocTypeRepository = new CustomerDocTypeRepository();

            bool chk = _customerDocTypeRepository.CheckDocumentTypeIsRequire(documentTypeID, companyTypeID, countryCode);

            return chk;
        }

        public static bool CheckCustomerDocNameIsRequire(int companyTypeID, int documentNameID, string countryCode)
        {
            ICustomerDocNameMappingRepository _customerDocNameMappingRepository = new CustomerDocNameMappingRepository();
            bool chk = _customerDocNameMappingRepository.CheckCustomerDocNameIsRequire(documentNameID, companyTypeID, countryCode);


            return chk;
        }

      

        public static IEnumerable<TempForMultiLang> GetBusinessTypeGroup(int languageID)
        {

            IBusinessTypeRepository _repo = new BusinessTypeRepository();

            var models = _repo.GetMultiLangBusinessTypeByLangid(languageID);

            return models;

        }

        public static IEnumerable<TempForMultiLang> GetProductTypeGroup(int languageID)
        {

            IProductTypeRepository _repo = new ProductTypeRepository();

            var models = _repo.GetMultiLangProductTypeByLangid(languageID);

            return models;

        }

        public static List<ProductTypeAddModel> GetProductTypeByRegIDAndProductCode(int regID, string productCode)
        {

            IRegProductRepository _repo = new RegProductRepository();

            var models = _repo.GetProductTypeByRegIDAndProductCode(regID,productCode);

            return models.ToList();

        }
      
        public static string GetFileSize(int contentLength)
        {

            string[] sizes = { "B", "KB", "MB", "GB" };
            int len = contentLength;
            int order = 0;
            while (len >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                len = len / 1024;
            }

            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            string result = String.Format("{0:0.##} {1}", len, sizes[order]);
            return result;
        }

        public static int GetRegAttachmentCountByRegIDAndDocumentTypeID(int regID, int documentTypeID)
        {
            IRegAttachmentRepository _regAttachmentRepository = new RegAttachmentRepository();

            int countAttachment = _regAttachmentRepository.GetRegAttachmentCountByRegIDAndDocumentTypeID(regID, documentTypeID);

            return countAttachment;
        }
        
    }
}
