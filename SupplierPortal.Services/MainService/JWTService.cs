﻿using JWT;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace SupplierPortal.Services
{
    public static class JWTService
    {
        public static byte[] StringToByeArray(string str)
        {
            return Encoding.ASCII.GetBytes(str);
            //return Enumerable.Range(0, hex.Length)
            //                 .Where(x => x % 2 == 0)
            //                 .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
            //                 .ToArray();
        }

        public static string JWTEncrypt(string strEncryptKey, Dictionary<string, string> dic)
        {
            return JWTEncrypt(strEncryptKey, dic, null);
        }
        public static string JWTEncrypt(string strEncryptKey, Dictionary<string, string> dic, DateTime? expiredUtc)
        {
            var payload = new Dictionary<string, object>();

            // Convert key string to byte array (default : password)
            strEncryptKey = !string.IsNullOrEmpty(strEncryptKey) ? strEncryptKey : "password";
            byte[] byteArrayKey = StringToByeArray(strEncryptKey);

            // Expired date token utc format (default expire token 1 hour)
            var expire = (expiredUtc.HasValue) ? expiredUtc.Value : DateTime.UtcNow.Add(new TimeSpan(1, 0, 0));
            Int32 unixTimestamp = (Int32)(expire.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            //var strUnixTimestamp = unixTimestamp.ToString();

            // Add range dictionary value
            foreach (var item in dic)
            {
                payload.Add(item.Key, item.Value);
            }

            // Expire time token
            if (payload.ContainsKey("exp"))
                payload.Remove("exp");
            payload.Add("exp", unixTimestamp);

            return JsonWebToken.Encode(payload, byteArrayKey, JwtHashAlgorithm.HS512);
        }

        public static Dictionary<string, string> JWTDecrypt(string strDecryptKey, string token)
        {
            // Convert key string to byte array (default : password)
            strDecryptKey = !string.IsNullOrEmpty(strDecryptKey) ? strDecryptKey : "password";
            byte[] byteArrayKey = StringToByeArray(strDecryptKey);

            var jsonResult = JsonWebToken.Decode(token, byteArrayKey, true);
            var xmlDocument = JsonConvert.DeserializeXmlNode(jsonResult, "root");
            Dictionary<string, string> dic = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(jsonResult);
            return dic;
        }
    }
}
