﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Services.MainService
{
    public static class MainService
    {
        public static string GetFileSize(int contentLength)
        {
            string[] sizes = { "B", "KB", "MB", "GB" };
            int len = contentLength;
            int order = 0;
            while (len >= 1024 && order + 1 < sizes.Length)
            {
                order++;
                len = len / 1024;
            }

            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            string result = String.Format("{0:0.##} {1}", len, sizes[order]);
            return result;

        }

        public static void InsertLogs(string logMessage)
        {
            using (StreamWriter w = File.AppendText(@"D:\Work\Pantavanij\Logs\log.txt"))
            {
                Log(logMessage, w);
            }

            using (StreamReader r = File.OpenText("log.txt"))
            {
                DumpLog(r);
            }
        }


        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\rLog Entry : ");
            w.WriteLine("{0} {1}", DateTime.UtcNow.ToLongTimeString(), logMessage);
        }

        public static void DumpLog(StreamReader r)
        {
            string line;
            while ((line = r.ReadLine()) != null)
            {
                Console.WriteLine(line);
            }
        }
    }
}
