﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.MessageTemplate;
using System.Web.Mvc;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using System.Web;
using SupplierPortal.Framework.AccountManage;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.EmailAccount;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.NameTitle;
using SupplierPortal.Data.Models.Repository.Organization;
using System.Configuration;
using System.Net.Mail;
using log4net.Repository.Hierarchy;
using SupplierPortal.Data.Models.Repository.APILogs;
using System.Web.Script.Serialization;
using SupplierPortal.DataAccess.BuyerConfig;

namespace SupplierPortal.Services.AccountManage
{
    public static class RecoveryPassword
    {
        readonly static log4net.ILog _logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void ResetPassword(string userName,string newPassword)
        {
            try
            {
                
                IUserRepository _repoUser = new UserRepository();

                var tbl_User = _repoUser.FindByUsername(userName);

                string newPasswordHashing = PasswordHashing.HashResetPassword(newPassword, tbl_User.Username);

                tbl_User.Password = newPasswordHashing;
                tbl_User.PwdRecoveryToken = "";
                tbl_User.MigrateFlag = 0;

                _repoUser.Update(tbl_User);

                if (tbl_User.IsActivated == 0)
                {
                    _repoUser.UpdateIsActivated(tbl_User.UserID);
                }
                

            }
            catch (Exception ex)
            {
                throw ex;
            }    

        }

        public static void RecoveryPasswordToEmail(string username)
        {
            try
            {
                IUserRepository _repoUser = new UserRepository();

                IMessageTemplateRepository _repoMessageTemplate = new MessageTemplateRepository();

                ILanguageRepository _repoLanguage = new LanguageRepository();

                IContactPersonRepository _repoContact = new ContactPersonRepository();

                ILanguageRepository _languageRepository = new LanguageRepository();

                INameTitleRepository _nameTitleRepository = new NameTitleRepository();

                string guid = "";
                string email = "";
                string token = "";
                string contactNameLocal = "";
                string contactNameInter = "";
                string callbackUrlLocal = "";
                string callbackUrlInter = "";

                var tbl_User = _repoUser.FindByUsername(username);

                var localLanguage = _languageRepository.GetLocalLanguage();
                string localLanguageCode = localLanguage.LanguageCode;
                int localLanguageID = localLanguage.LanguageID;

                var interLanguage = _languageRepository.GetInterLanguage();
                string interLanguageCode = interLanguage.LanguageCode;
                int interLanguageID = interLanguage.LanguageID;

                //email = tbl_User.Email;

                if (tbl_User!= null)
                {
                    int contactID = Convert.ToInt32(tbl_User.ContactID);
                    var resultContact = _repoContact.GetContactPersonByContectID(contactID);

                    if (resultContact != null)
                    {
                        string titleName_Local = _nameTitleRepository.GetTitleNameByIdAndLanguageID(resultContact.TitleID ?? 0, localLanguageID);
                        string titleName_Inter = _nameTitleRepository.GetTitleNameByIdAndLanguageID(resultContact.TitleID ?? 0, interLanguageID);

                        contactNameLocal = titleName_Local + resultContact.FirstName_Local + "  " + resultContact.LastName_Local;
                        contactNameInter = titleName_Inter + resultContact.FirstName_Inter + "  " + resultContact.LastName_Inter;

                        email = resultContact.Email;
                    }
 
                }
              

                guid = GeneratorGuid.GetRandomGuid();

                token = guid + ":" + tbl_User.Username;

                tbl_User.PwdRecoveryToken = token;

                _repoUser.Update(tbl_User);

                #region JWT Encrypt Token

                var encryptKey = ConfigurationManager.AppSettings["Jwt_ForgotPasswordKey"];
                if (string.IsNullOrEmpty(encryptKey))
                    throw new Exception("Jwt_ForgotPasswordKey is null or empty.");

                var minutesExpiredDate = DateTime.UtcNow;
                var minutesExpiredString = ConfigurationManager.AppSettings["Jwt_ForgotPasswordExpiredMinutes"];
                if (string.IsNullOrEmpty(minutesExpiredString))
                    throw new Exception("Jwt_ForgotPasswordExpiredMinutes is null or empty.");

                try
                {
                    int tmpMinutes = 1;
                    int.TryParse(minutesExpiredString, out tmpMinutes);
                    minutesExpiredDate = DateTime.UtcNow.Add(new TimeSpan(0, tmpMinutes, 0));
                }
                catch (Exception ex)
                {
                }

                Dictionary<string, string> payload = new Dictionary<string, string>();
                payload.Add("temp_token", token);

                var jwtToken = SupplierPortal.Services.JWTService.JWTEncrypt(encryptKey, payload, minutesExpiredDate);

                #endregion

                string baseURLWeb = System.Configuration.ConfigurationManager.AppSettings.Get("baseURLWeb");

                callbackUrlLocal = baseURLWeb + "AccountPortal/ResetPassword?code=" + jwtToken + "&language=" + localLanguageCode;
                callbackUrlLocal = HttpUtility.HtmlEncode(callbackUrlLocal);

                callbackUrlInter = baseURLWeb + "AccountPortal/ResetPassword?code=" + jwtToken + "&language=" + interLanguageCode;
                callbackUrlInter = HttpUtility.HtmlEncode(callbackUrlInter);

                var messageTemplate = _repoMessageTemplate.GetMessageTemplateByName("Login.ForgotPassword");

                string languageID = _repoLanguage.GetLanguageIdByCode(tbl_User.LanguageCode);


                string pkTableMain = messageTemplate.MessageId.ToString();

                IEmailAccountRepository _repoEmlacc = new EmailAccountRepository();
                var EmlaccResult = _repoEmlacc.GetEmailAccountByEmailAccountID(Convert.ToInt32(messageTemplate.EmailAccountID));

                var EmlaccList = EmlaccResult.FirstOrDefault();
                string from = EmlaccList.Email;
                //string from = messageTemplate.Tbl_EmailAccount.Email;

                string subject = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_MessageTemplate", "Subject", pkTableMain);

                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");

                string body = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_MessageTemplate", "Body", pkTableMain);


                body = @body.Replace("<%UrlLinkLocal%>", callbackUrlLocal);
                body = @body.Replace("<%ContactName_Local%>", contactNameLocal);
                body = @body.Replace("<%UrlLinkInter%>", callbackUrlInter);
                body = @body.Replace("<%ContactName_Inter%>", contactNameInter);
                
                //"192.168.2.11"

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = email,
                    Subject = subject,
                    Body = body,
                    Host = host,

                };

                EmailService.SendMail(_objmail);

            }
            catch (Exception ex)
            {
                throw ex;
            }          

        }

        public static bool RecoveryPasswordToEmailForBuyerSupplierUser(GetUser_Notify_ChangePassword_Result itemInfo, string visitorsIPAddr)
        {
            string mailBodyJson = "";
            try
            {
                IUserRepository _repoUser = new UserRepository();

                IMessageTemplateRepository _repoMessageTemplate = new MessageTemplateRepository();

                ILanguageRepository _repoLanguage = new LanguageRepository();

                IContactPersonRepository _repoContact = new ContactPersonRepository();

                IOrganizationRepository _repoOrganization = new OrganizationRepository();

                ILanguageRepository _languageRepository = new LanguageRepository();

                INameTitleRepository _nameTitleRepository = new NameTitleRepository();

                IBuyerConfigRepository _buyerConfigRepository = new BuyerConfigRepository();

                string guid = "";
                string email = "";
                string token = "";
                string contactNameLocal = "";
                string contactNameInter = "";
                string callbackUrlLocal = "";
                string callbackUrlInter = "";
                Tbl_Organization org = new Tbl_Organization() ;
                Tbl_User tbl_User = _repoUser.FindByUsername(itemInfo.Username);

                var localLanguage = _languageRepository.GetLocalLanguage();
                string localLanguageCode = localLanguage.LanguageCode;
                int localLanguageID = localLanguage.LanguageID;

                var interLanguage = _languageRepository.GetInterLanguage();
                string interLanguageCode = interLanguage.LanguageCode;
                int interLanguageID = interLanguage.LanguageID;

                //email = tbl_User.Email;

                if (tbl_User != null)
                {
                    org = _repoOrganization.GetDataBySupplierID(tbl_User.SupplierID.Value);
                    int contactID = Convert.ToInt32(tbl_User.ContactID);
                    var resultContact = _repoContact.GetContactPersonByContectID(contactID);

                    if (resultContact != null)
                    {
                        string titleName_Local = _nameTitleRepository.GetTitleNameByIdAndLanguageID(resultContact.TitleID ?? 0, localLanguageID);
                        string titleName_Inter = _nameTitleRepository.GetTitleNameByIdAndLanguageID(resultContact.TitleID ?? 0, interLanguageID);

                        contactNameLocal = titleName_Local + resultContact.FirstName_Local + "  " + resultContact.LastName_Local;
                        contactNameInter = titleName_Inter + resultContact.FirstName_Inter + "  " + resultContact.LastName_Inter;

                        email = resultContact.Email;
                    }

                }


                guid = GeneratorGuid.GetRandomGuid();

                token = guid + ":" + tbl_User.Username;

                tbl_User.PwdRecoveryToken = token;

                _repoUser.Update(tbl_User);

                #region JWT Encrypt Token

                var encryptKey = ConfigurationManager.AppSettings["Jwt_ForgotPasswordKey"];
                if (string.IsNullOrEmpty(encryptKey))
                    throw new Exception("Jwt_ForgotPasswordKey is null or empty.");

                var dateTimeNow = DateTime.UtcNow;
                var minutesExpiredDate = dateTimeNow;
                string minutesExpiredString = "";

                //--- GET minutesExpiredString ---
                //var minutesExpiredString = ConfigurationManager.AppSettings["Jwt_ForgotPasswordExpiredMinutes"];
                Tbl_BuyerConfig buyerConfig = _buyerConfigRepository.GetBuyerConfig("Supplier_Buyer_Portal", "Jwt_ForgotPasswordExpiredMinutes", itemInfo.BuyerEID.Value);
                if(buyerConfig != null)
                {
                    minutesExpiredString = buyerConfig.Value;
                }
                else
                {
                    minutesExpiredString = ConfigurationManager.AppSettings["Jwt_ForgotPasswordExpiredMinutes"];
                }

                if (string.IsNullOrEmpty(minutesExpiredString))
                    throw new Exception("Jwt_ForgotPasswordExpiredMinutes is null or empty.");

                try
                {
                    int tmpMinutes = 1;
                    int.TryParse(minutesExpiredString, out tmpMinutes);
                    minutesExpiredDate = DateTime.UtcNow.Add(new TimeSpan(0, tmpMinutes, 0));
                }
                catch (Exception ex)
                {
                }

                Dictionary<string, string> payload = new Dictionary<string, string>();
                payload.Add("temp_token", token);

                var jwtToken = SupplierPortal.Services.JWTService.JWTEncrypt(encryptKey, payload, minutesExpiredDate);

                #endregion

                string baseURLWeb = System.Configuration.ConfigurationManager.AppSettings.Get("baseURLWeb");

                callbackUrlLocal = baseURLWeb + "AccountPortal/ResetPassword?code=" + jwtToken + "&language=" + localLanguageCode;
                callbackUrlLocal = HttpUtility.HtmlEncode(callbackUrlLocal);

                callbackUrlInter = baseURLWeb + "AccountPortal/ResetPassword?code=" + jwtToken + "&language=" + interLanguageCode;
                callbackUrlInter = HttpUtility.HtmlEncode(callbackUrlInter);

                var messageTemplate = _repoMessageTemplate.GetMessageTemplateByName("Buyer.NewSupplierUser");

                string languageID = _repoLanguage.GetLanguageIdByCode(tbl_User.LanguageCode);


                string pkTableMain = messageTemplate.MessageId.ToString();

                IEmailAccountRepository _repoEmlacc = new EmailAccountRepository();
                var EmlaccResult = _repoEmlacc.GetEmailAccountByEmailAccountID(Convert.ToInt32(messageTemplate.EmailAccountID));

                var EmlaccList = EmlaccResult.FirstOrDefault();
                string from = EmlaccList.Email;
                //string from = messageTemplate.Tbl_EmailAccount.Email;

                string subject = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_MessageTemplate", "Subject", pkTableMain);

                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");

                string body = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_MessageTemplate", "Body", pkTableMain);

                body = @body.Replace("<%Username%>", itemInfo.Username);
                body = @body.Replace("<%Buyer_Company_Name%>", itemInfo.bc_com_name);
                body = @body.Replace("<%CompanyName_Local%>", (string.IsNullOrEmpty(org.CompanyName_Local) ? org.InvName_Local : org.CompanyName_Local));
                body = @body.Replace("<%CompanyName_Inter%>", (string.IsNullOrEmpty(org.CompanyName_Inter) ? org.InvName_Inter : org.CompanyName_Inter));
                body = @body.Replace("<%BranchNo.%>", (string.IsNullOrEmpty(org.BranchNo) ? "None" : org.BranchNo));
                body = @body.Replace("<%erfxname%>", (string.IsNullOrEmpty(itemInfo.erfx_name) ? "None" : itemInfo.erfx_name));
                body = @body.Replace("<%erfxno%>", (string.IsNullOrEmpty(itemInfo.erfx_id == null ? "" : itemInfo.erfx_id.ToString()) ? "None" : itemInfo.erfx_id.ToString()));
                
                body = @body.Replace("<%buyerName%>", (string.IsNullOrEmpty(itemInfo.bc_purchaser == null ? "" : itemInfo.bc_purchaser.ToString()) ? "None" : itemInfo.bc_purchaser.ToString()));
                body = @body.Replace("<%buyerName_Inter%>", (string.IsNullOrEmpty(itemInfo.bc_purchaser == null ? "" : itemInfo.bc_purchaser.ToString()) ? "None" : itemInfo.bc_purchaser.ToString()));
                body = @body.Replace("<%buyerContactEmail%>", (string.IsNullOrEmpty(itemInfo.bc_email == null ? "" : itemInfo.bc_email.ToString()) ? "None" : itemInfo.bc_email.ToString()));
                body = @body.Replace("<%buyerContactTel%>", (string.IsNullOrEmpty(itemInfo.bc_phone == null ? "" : itemInfo.bc_phone.ToString()) ? "None" : itemInfo.bc_phone.ToString()));


                body = @body.Replace("<%UrlLink_Local%>", callbackUrlLocal);
                body = @body.Replace("<%ContactName_Local%>", contactNameLocal);
                body = @body.Replace("<%UrlLink_Inter%>", callbackUrlInter);
                body = @body.Replace("<%ContactName_Inter%>", contactNameInter);

                //"192.168.2.11"

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = email,
                    Subject = subject,
                    Body = body,
                    Host = host,

                };

                // Create mailBodyJson for save in log
                string jsonResult = new JavaScriptSerializer().Serialize(_objmail);
                mailBodyJson = jsonResult;

                // send email
                EmailService.SendMail(_objmail);

                #region Insert Log

                string returnCode = "200";
                string returnStatus = "true";
                string returnMessage = "send email is success " + "--- Username: " + itemInfo.Username ;

                IAPILogsRepository _aPILogsRepository = new APILogsRepository();
                Tbl_APILogs apilog = new Tbl_APILogs
                {
                    Data = jsonResult,
                    APIName = "InviteResetPassword",
                    SystemID = 0,
                    Events = "Request",
                    Events_Time = dateTimeNow,
                    IPAddress_Client = visitorsIPAddr,
                    Return_Code = returnCode,
                    Return_Status = returnStatus,
                    Return_Message = returnMessage

                };

                _aPILogsRepository.Insert(apilog);
                #endregion

                // call store to update
                _logger.Error("RecoveryPasswordToEmailForBuyerSupplierUser (sent mail status) : success " + "--- Username: " + itemInfo.Username);

                return true;
            }
            catch (Exception ex)
            {
                #region Insert Log

                string returnCode = "500";
                string returnStatus = "false";
                string returnMessage = "send email is unsuccessful : " + "--- Username: " + itemInfo.Username +" --- Message: "+ ex.Message;

                IAPILogsRepository _aPILogsRepository = new APILogsRepository();
                Tbl_APILogs apilog = new Tbl_APILogs
                {
                    Data = mailBodyJson,
                    APIName = "InviteResetPassword",
                    SystemID = 0,
                    Events = "Request",
                    Events_Time = DateTime.UtcNow,
                    IPAddress_Client = visitorsIPAddr,
                    Return_Code = returnCode,
                    Return_Status = returnStatus,
                    Return_Message = returnMessage

                };

                _aPILogsRepository.Insert(apilog);
                #endregion

                #region Log
                _logger.Error("RecoveryPasswordToEmailForBuyerSupplierUser (Method)" + "--- Username: " + itemInfo.Username + " --- Message: " + ex);
                #endregion

                return false;

            }

        }

        public static void RecoveryPasswordNonEmail(string username)
        {
            try
            {
                IUserRepository _repoUser = new UserRepository();

                string guid = "";
                string token = "";

                var tbl_User = _repoUser.FindByUsername(username);

                if (tbl_User != null)
                {
                    guid = GeneratorGuid.GetRandomGuid();

                    token = guid + ":" + tbl_User.Username;

                    tbl_User.PwdRecoveryToken = token;

                    _repoUser.Update(tbl_User);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }    
        }

        public static void RecoveryPasswordToEmail(string username,string templateName)
        {
            try
            {
                IUserRepository _repoUser = new UserRepository();

                IMessageTemplateRepository _repoMessageTemplate = new MessageTemplateRepository();

                ILanguageRepository _repoLanguage = new LanguageRepository();

                IContactPersonRepository _repoContact = new ContactPersonRepository();

                string guid = "";
                string email = "";
                string token = "";
                string contactNameLocal = "";
                string contactNameInter = "";

                var tbl_User = _repoUser.FindByUsername(username);

                //email = tbl_User.Email;

                if (tbl_User != null)
                {
                    int contactID = Convert.ToInt32(tbl_User.ContactID);
                    var resultContact = _repoContact.GetContactPersonByContectID(contactID);

                    contactNameLocal = resultContact.FirstName_Local + "  " + resultContact.LastName_Local;
                    contactNameInter = resultContact.FirstName_Inter + "  " + resultContact.LastName_Inter;

                    email = resultContact.Email;
                }


                guid = GeneratorGuid.GetRandomGuid();

                token = guid + ":" + tbl_User.Username;

                tbl_User.PwdRecoveryToken = token;

                _repoUser.Update(tbl_User);

                string baseURLWeb = System.Configuration.ConfigurationManager.AppSettings.Get("baseURLWeb");

                var callbackUrl = baseURLWeb + "AccountPortal/ResetPassword?code=" + token;
                callbackUrl = HttpUtility.HtmlEncode(@callbackUrl);

                var messageTemplate = _repoMessageTemplate.GetMessageTemplateByName(templateName);

                string languageCode = tbl_User.LanguageCode;
                string languageID = "";

                if (string.IsNullOrEmpty(languageCode))
                {
                    languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
                }
                else 
                {
                    languageID = _repoLanguage.GetLanguageIdByCode(tbl_User.LanguageCode);
                }

                

                string pkTableMain = "-1";

                if (messageTemplate!=null)
                {
                    pkTableMain = messageTemplate.MessageId.ToString();
                }

                IEmailAccountRepository _repoEmlacc = new EmailAccountRepository();
                var EmlaccResult = _repoEmlacc.GetEmailAccountByEmailAccountID(Convert.ToInt32(messageTemplate.EmailAccountID));
                _repoEmlacc.Dispose();

                var EmlaccList = EmlaccResult.FirstOrDefault();
                string from = EmlaccList.Email;
                //string from = messageTemplate.Tbl_EmailAccount.Email;

                string subject = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_MessageTemplate", "Subject", pkTableMain);

                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");

                string body = GetLocalizedProperty.GetLocalizedValue(languageID, "Tbl_MessageTemplate", "Body", pkTableMain);

                var languageTbl = _repoLanguage.GetLanguageById(Convert.ToInt32(languageID));

                int isInter = languageTbl.isInter ?? 0;
                string contactName = (isInter == 1 ? contactNameInter : contactNameLocal);

                body = @body.Replace("<%UrlLink%>", callbackUrl);
                body = @body.Replace("<%Username%>", username);
                body = @body.Replace("<%ContactName%>", contactName);

                //"192.168.2.11"

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = email,
                    Subject = subject,
                    Body = body,
                    Host = host,

                };

                EmailService.SendMail(_objmail);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
