﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Framework.MethodHelper;

namespace SupplierPortal.Services.AccountManage
{
    public static class GeneratorGuid
    {
        public static string GetRandomGuid()
        {
            string guid = "";
            Guid g;
            // Create and display the value of two GUIDs.
            g = Guid.NewGuid();

            guid = g.ToString();

            guid = guid.Replace("-", "");

            return guid;
           
        }

        public static void InsertGuid(string guid, string username)
        {
            IUserSessionRepository _repo = null;
            _repo = new UserSessionRepository();

            try
            {


                DateTime? now = DateTime.Now;

                //var utc = TimeZoneConvert.ConvertDateTimeToGMT(timeZone, now);

                DateTime utcNow = DateTime.UtcNow;

                Tbl_UserSession userSession = new Tbl_UserSession()
                {
                    UserGuid = guid,
                    Username = username,
                    //IPAddress = ipAddress,
                    //BrowserType = browserType,
                    //LoginTime = utcNow,
                    LastAccessed = utcNow,
                    isTimeout = 0
                };

                _repo.Insert(userSession);


            }
            catch (TimeZoneNotFoundException)
            {
                //Console.WriteLine("The registry does not define the Hawaiian Standard Time zone.");
            }
            catch (InvalidTimeZoneException)
            {
                //Console.WriteLine("Registry data on the Hawaiian STandard Time zone has been corrupted.");
            }



        }



    }
}
