﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Framework.Localization;

namespace SupplierPortal.Services.AccountManage
{
    public static class UserSessionManage
    {


        public static bool CheckUserSession(string guid, string checkSessionOnly)
        {
            IUserSessionRepository _repo = new UserSessionRepository();

            bool userTimeout = false;

            try
            {
                var userSession = _repo.GetUserSession(guid);
                if (userSession != null)
                {
                    DateTime lastAccessed = userSession.LastAccessed ?? DateTime.Now;

                    DateTime utcNow = DateTime.UtcNow;

                    long elapsedTicks = utcNow.Ticks - lastAccessed.Ticks;
                    TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);

                    double totalDateDiff = elapsedSpan.TotalMilliseconds;

                    string strTimeout = System.Configuration.ConfigurationManager.AppSettings.Get("TimeOut");

                    long timeOut = Convert.ToInt64(strTimeout);

                    if (timeOut > totalDateDiff && userSession.isTimeout == 0)
                    {
                        userSession.LastAccessed = utcNow;
                        userSession.isTimeout = 0;
                        if (string.IsNullOrEmpty(checkSessionOnly))
                            _repo.Update(userSession);
                        userTimeout = true;
                    }
                    else
                    {
                        userSession.isTimeout = 1;
                        if (string.IsNullOrEmpty(checkSessionOnly))
                            _repo.Update(userSession);
                        userTimeout = false;
                    }
                }
                else
                {
                    string languageId = "";
                    string messageReturn = "";
                    if (string.IsNullOrEmpty(languageId))
                    {
                        languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
                    }
                    throw new NotImplementedException(messageReturn.GetStringResource("_ApiMsg.ActiveUserSession.UserGuidNotFound", languageId));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return userTimeout;
        }


        public static bool CheckUserSessionTimeout(string guid)
        {
            IUserSessionRepository _repo = new UserSessionRepository();

            bool userTimeout = false;

            try
            {
                var userSession = _repo.GetUserSession(guid);

                if (userSession != null)
                {
                    DateTime lastAccessed = userSession.LastAccessed ?? DateTime.Now;

                    DateTime utcNow = DateTime.UtcNow;
                    
                    long elapsedTicks = utcNow.Ticks - lastAccessed.Ticks;
                    TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);

                    double totalDateDiff = elapsedSpan.TotalMilliseconds;

                    string strTimeout = System.Configuration.ConfigurationManager.AppSettings.Get("TimeOut");

                    long timeOut = Convert.ToInt64(strTimeout);

                    if (timeOut > totalDateDiff && userSession.isTimeout == 0)
                    {
                        userTimeout = true;
                    }
                    else
                    {
                        userTimeout = false;
                    }
                }
                else
                {
                    userTimeout = false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return userTimeout;
        }


    }
}
