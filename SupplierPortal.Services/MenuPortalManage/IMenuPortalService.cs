﻿using SupplierPortal.Data.Models.SupportModel.MenuPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Services.MenuPortalManage
{
    public partial interface IMenuPortalService
    {

        IEnumerable<SubMenuPortal> GetSubMenuPortalNonNotification();

        int GetNotificationByUserNameAndMenuID(string username, int menuID);

        int GetNotificationBoxByUserNameAndMenuID(string username, int menuID);

        NotificationBoxModels GetNotificationBoxAllByUserNameAndMenuID(string username, int menuID);
    }
}
