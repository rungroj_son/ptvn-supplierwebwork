﻿using Newtonsoft.Json;
using SupplierPortal.Core.Extension;
using SupplierPortal.Data.CustomModels.SCF;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using RestSharp;

namespace SupplierPortal.Services.APIService
{
    public class APIConnectionService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string formatLog = "APIConnectionService\r\nDescription: {0}\r\nURL: {1}\r\nMethod: {2}\r\nRequest: {3}\r\nResponse StatusCode: {4}";
        public async Task<HttpResponseMessage> PostApiConnection(string baseURL, string url, Object model, List<APIKey> keys = null)
        {
            HttpClient client = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            try
            {
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string Jsonstr = JsonConvert.SerializeObject(model);
                HttpContent content = new StringContent(Jsonstr, Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                if (keys != null)
                {
                    foreach (APIKey item in keys)
                    {
                        content.Headers.Add(item.key, item.value);
                    }
                }

                string urlAll = string.Format(url, baseURL);

                HttpResponseMessage response = await client.PostAsync(urlAll, content);

                return response;
            }
            catch (Exception ex)
            {
                logger.Error("APIConnectionService Method Post :" + ex);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }

        }

        public async Task<HttpResponseMessage> PostApiConnectionWithHeader(string baseURL, string url, Object model, List<APIKey> header = null)
        {
            HttpClient client = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            try
            {
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string Jsonstr = JsonConvert.SerializeObject(model);
                HttpContent content = new StringContent(Jsonstr, Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                if (header != null)
                {
                    foreach (APIKey item in header)
                    {
                        client.DefaultRequestHeaders.Add(item.key, item.value);
                    }
                }

                string urlAll = string.Format(url, baseURL);

                HttpResponseMessage response = await client.PostAsync(urlAll, content);

                return response;
            }
            catch (Exception ex)
            {
                logger.Error("APIConnectionService Method Post :" + ex);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }

        }

        public async Task<HttpResponseMessage> PostApiConnectionWithFormUrlEncoded(string baseURL, string url, Object model, List<APIKey> keys = null)
        {
            HttpClient client = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            try
            {
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));

                string Jsonstr = JsonConvert.SerializeObject(model);
                HttpContent content = new FormUrlEncodedContent((List<KeyValuePair<string, string>>)model);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                if (keys != null)
                {
                    foreach (APIKey item in keys)
                    {
                        client.DefaultRequestHeaders.Add(item.key, item.value);
                    }
                }

                string urlAll = string.Format(url, baseURL);

                HttpResponseMessage response = await client.PostAsync(urlAll, content);

                return response;
            }
            catch (Exception ex)
            {
                logger.Error("APIConnectionService Method Post :" + ex);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        public async Task<HttpResponseMessage> PostApiConnectionWithFormData(string baseURL, string url, List<KeyValuePair<string, string>> model, List<APIKey> keys = null)
        {
            try
            {
                string urlAll = string.Format(url, baseURL);
                var client = new RestClient(urlAll);
                client.Timeout = -1;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

                var request = new RestRequest(Method.POST);
                request.AlwaysMultipartFormData = true;

                if (keys != null)
                {
                    foreach (APIKey item in keys)
                    {
                        request.AddHeader(item.key, item.value);
                    }
                }

                foreach (var eachModel in model)
                {
                    request.AddParameter(eachModel.Key, eachModel.Value);
                }

                IRestResponse response = await client.ExecuteAsync(request);
                HttpResponseMessage returnResponse = new HttpResponseMessage();
                returnResponse.Content = new StringContent(response.Content);
                returnResponse.StatusCode = response.StatusCode;
                return returnResponse;
            }
            catch (Exception ex)
            {
                logger.Error("APIConnectionService Method Post :" + ex);
                throw ex;
            }
        }

        public async Task<HttpResponseMessage> GetApiConnection(string baseURL, string url, string strUsername = "", string strPassword = "")
        {
            HttpClient client = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            try
            {

                if (!string.IsNullOrEmpty(strUsername) && !string.IsNullOrEmpty(strPassword))
                {
                    client = SetHeaderLoginPortal(strUsername, strPassword);
                }

                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(url);

                return response;
            }
            catch (Exception ex)
            {
                logger.Error("APIConnectionService Method Get :" + ex);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        public async Task<HttpResponseMessage> GetApiConnectionWithOutSecurity(string baseURL, string url, string strUsername = "", string strPassword = "")
        {
            HttpClient client = new HttpClient();

            try
            {

                if (!string.IsNullOrEmpty(strUsername) && !string.IsNullOrEmpty(strPassword))
                {
                    client = SetHeaderLoginPortal(strUsername, strPassword);
                }

                client.BaseAddress = new Uri(baseURL);
                //client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("User-Agent", "WebApiForPortal");
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                client.DefaultRequestHeaders.Add("Accept-Charset", "UTF-8");
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(url);

                return response;
            }
            catch (Exception ex)
            {
                logger.Error("APIConnectionService Method Get :" + ex);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        public async Task<HttpResponseMessage> GetApiConnectionWithHeader(string baseURL, string url, List<APIKey> keys = null)
        {
            HttpClient client = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            try
            {
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (keys != null)
                {
                    foreach (APIKey item in keys)
                    {
                        client.DefaultRequestHeaders.Add(item.key, item.value);
                    }
                }

                string urlAll = string.Format(url, baseURL);

                HttpResponseMessage response = await client.GetAsync(urlAll);

                return response;
            }
            catch (Exception ex)
            {
                logger.Error("APIConnectionService Method Get :" + ex);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        public async Task<HttpResponseMessage> PutApiConnection(string baseURL, string url, Object model, List<APIKey> keys = null)
        {
            HttpClient client = new HttpClient();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            try
            {
                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string Jsonstr = JsonConvert.SerializeObject(model);
                HttpContent content = new StringContent(Jsonstr, Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                if (keys != null)
                {
                    foreach (APIKey item in keys)
                    {
                        content.Headers.Add(item.key, item.value);
                    }
                }

                string urlAll = string.Format(url, baseURL);

                HttpResponseMessage response = await client.PutAsync(urlAll, content);

                return response;
            }
            catch (Exception ex)
            {
                logger.Error("APIConnectionService Method Put :" + ex);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }

        }

        private HttpClient SetHeaderLoginPortal(string strUserName, string userPassword)
        {
            //string publicKey = "89844013dc5f506455e8dd3a48e5b4a9";
            string publicKey = WebConfigurationManager.AppSettings["publicKey"];
            //string strUserName = "portal@pantavanij.com";
            //string secretKey = "769fa783018bf7ff350697f5d7a218be";
            string secretKey = WebConfigurationManager.AppSettings["secretKey"];
            //string baseURL = "http://localhost:3867/";
            string baseURL = WebConfigurationManager.AppSettings["baseURL"];
            string signature = CryptoExtension.Signature(publicKey, secretKey);

            string authInfo = strUserName + ":" + signature;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
            client.DefaultRequestHeaders.Add("User-Agent", "WebApiForPortal");
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            client.DefaultRequestHeaders.Add("Accept-Charset", "UTF-8");
            client.DefaultRequestHeaders.Add("AuthInfo", authInfo);
            client.DefaultRequestHeaders.Add("PublicKey", publicKey);
            //client.DefaultRequestHeaders.Add("baseURL", baseURL);
            client.DefaultRequestHeaders.Add("Userpassword", userPassword);

            return client;
        }

        #region Helper
        public T streamReaderToObject<T>(HttpResponseMessage httpResponse)
        {
            string jsonResult = "";
            using (Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result)
            {
                using (StreamReader r = new StreamReader(responseString))
                {
                    jsonResult = r.ReadToEnd();
                }
            }

            JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
            return JsonConvert.Deserialize<T>(jsonResult);
        }
        public string setURLHelper(string endpoint, string baseURL = "")
        {
            return string.Format("{0}{1}", baseURL, endpoint);
        }
        public APIKey setTokenHeaderHelper(string tokenType, string accessToken)
        {
            return new APIKey() { key = "Authorization", value = string.Format("{0} {1}", tokenType, accessToken) };
        }
        public string AddParameterHelper(string baseURL, List<KeyValuePair<string, string>> key = null)
        {
            var uriBuilder = new UriBuilder(baseURL);

            if (key != null)
            {
                var paramValues = HttpUtility.ParseQueryString(uriBuilder.Query);

                key.ForEach(i =>
                {
                    paramValues.Add(i.Key, i.Value);
                });

                uriBuilder.Query = paramValues.ToString();
            }

            return uriBuilder.Uri.ToString();
        }

        public void LogRequestInfo(string description, string url, string method, dynamic objectRequest = null, string responseStatusCode = null)
        {
            string deObjectRequest = objectRequest == null ? "-" : JsonConvert.SerializeObject(objectRequest);

            logger.Info(string.Format(this.formatLog, description, url, method, deObjectRequest, responseStatusCode));
        }
        #endregion
    }
}