﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using System.Transactions;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.SystemRole;
using SupplierPortal.Data.Models.Repository.APILogs;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.OrgContactPerson;
using SupplierPortal.Data.Models.Repository.LogUser;
using SupplierPortal.Data.Models.Repository.LogContactPerson;
using SupplierPortal.Data.Models.Repository.LogOrgContactPerson;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.UserServiceMapping;
using SupplierPortal.Data.Models.Repository.UserReportMapping;
using SupplierPortal.Data.Models.Repository.SupplierPortalReports;
using SupplierPortal.Services.AccountManage;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.SystemConfigureAPI;

namespace SupplierPortal.Services.ProfileManage
{
    public partial class ProfileTransactions : IProfileTransactions
    {
        IUserSystemMappingRepository _userSystemMappingRepository;
        IUserRepository _userRepository;
        ISystemRepository _systemRepository;
        IProfileDataService _profileDataService;
        IAPILogsRepository _aPILogsRepository;
        IContactPersonRepository _contactPersonRepository;
        ILogUserRepository _logUserRepository;
        ILogContactPersonRepository _logContactPersonRepository;
        ILogOrgContactPersonRepository _logOrgContactPersonRepository;
        IOrgContactPerson _orgContactPersonRepository;
        IOrganizationRepository _organizationRepository;
        IUserServiceMappingRepository _userServiceMappingRepository;
        IUserReportMappingRepository _userReportMappingRepository;
        ISystemConfigureAPIRepository _systemConfigureAPIRepository;

        public ProfileTransactions()
        {
            _userRepository = new UserRepository();
            _userSystemMappingRepository = new UserSystemMappingRepository();
            _systemRepository = new SystemRepository();
            _profileDataService = new ProfileDataService();
            _aPILogsRepository = new APILogsRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _logUserRepository = new LogUserRepository();
            _logContactPersonRepository = new LogContactPersonRepository();
            _logOrgContactPersonRepository = new LogOrgContactPersonRepository();
            _orgContactPersonRepository = new OrgContactPerson();
            _organizationRepository = new OrganizationRepository();
            _userServiceMappingRepository = new UserServiceMappingRepository();
            _userReportMappingRepository = new UserReportMappingRepository();
            _systemConfigureAPIRepository = new SystemConfigureAPIRepository();
        }

        public ProfileTransactions(
            UserRepository userRepository,
            UserSystemMappingRepository userSystemMappingRepository,
            SystemRepository systemRepository,
            ProfileDataService profileDataService,
            APILogsRepository aPILogsRepository,
            ContactPersonRepository contactPersonRepository,
            LogUserRepository logUserRepository,
            LogContactPersonRepository logContactPersonRepository,
            LogOrgContactPersonRepository logOrgContactPersonRepository,
            OrgContactPerson orgContactPersonRepository,
            OrganizationRepository organizationRepository,
            UserServiceMappingRepository userServiceMappingRepository,
            UserReportMappingRepository userReportMappingRepository,
            SystemConfigureAPIRepository systemConfigureAPIRepository
         )
         {
             _userRepository = userRepository;
             _userSystemMappingRepository = userSystemMappingRepository;
             _systemRepository = systemRepository;
             _profileDataService = profileDataService;
             _aPILogsRepository = aPILogsRepository;
             _contactPersonRepository = contactPersonRepository;
             _logUserRepository = logUserRepository;
             _logContactPersonRepository = logContactPersonRepository;
             _logOrgContactPersonRepository = logOrgContactPersonRepository;
             _orgContactPersonRepository = orgContactPersonRepository;
             _organizationRepository = organizationRepository;
             _userServiceMappingRepository = userServiceMappingRepository;
             _userReportMappingRepository = userReportMappingRepository;
         }


        public virtual bool MergUserAccount(string masterUsername, List<UserMergModels> mergUsername, string usernameUpdateBy, out string returnMessage)
        {
            bool chk = true;
            string messageReturn = "";
            string languageId = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
            returnMessage = "";
            string tempMessage = "";
            var listAPILogs = new List<Tbl_APILogs>();
            try
            {
                //var _APILogs = new Tbl_APILogs();
                

                var listsysMappingSuccess = new List<SystemRoleListModels>();

                using (TransactionScope ts = new TransactionScope())
                {
                    var masterUser = _userRepository.FindByUsername(masterUsername);

                    var updateByUser = _userRepository.FindByUsername(usernameUpdateBy);
                    int userID = updateByUser.UserID;

                    foreach (var userlist in mergUsername)
                    {
                        
                        #region Update Tbl_UserSystemMapping

                        var mergUser = _userRepository.FindByUsername(userlist.UserMergList);

                        var mergUserMapping = _userSystemMappingRepository.GetUserSystemMappingByUserID(mergUser.UserID);

                        foreach (var list in mergUserMapping)
                        {
                            var userMapping = list;
                            _userSystemMappingRepository.UpdateByMergAccount(userMapping, masterUser.UserID);
                        }                   

                        #endregion

                        #region Update Tbl_UserServiceMapping

                        var serviceMappingMasterUser = _userServiceMappingRepository.GetUserServiceMappingByUserID(masterUser.UserID);

                        var serviceMappingMergUser = _userServiceMappingRepository.GetUserServiceMappingByUserID(mergUser.UserID);

                        var query = from a in serviceMappingMergUser
                                    where !(serviceMappingMasterUser.Any(m => m.ServiceID == a.ServiceID))
                                    select a;

                        var listServiceMapping = query.ToList();

                        foreach (var listService in listServiceMapping)
                        {
                            if (!_userServiceMappingRepository.CheckExistsUserServiceMapping(masterUser.UserID, listService.ServiceID))
                            {
                                var userServiceMappingModel = new Tbl_UserServiceMapping()
                                {
                                    UserID = masterUser.UserID,
                                    ServiceID = listService.ServiceID,
                                    isActive = listService.isActive
                                };

                                _userServiceMappingRepository.Insert(userServiceMappingModel, updateByUser.UserID);
                            } 
                        }
                        
                        #endregion

                        #region Update Tbl_User
                        mergUser.MergeToUserID = masterUser.UserID;
                        mergUser.IsDeleted = 1;
                        _userRepository.Update(mergUser);
                        _logUserRepository.Insert(mergUser.UserID, "Modify", usernameUpdateBy);
                        #endregion

                        #region Update Tbl_ContactPerson
                        var contactPerson = _contactPersonRepository.GetContactPersonByContectID(mergUser.ContactID??0);
                        if (contactPerson != null)
                        {
                            contactPerson.isDeleted = 1;
                            _contactPersonRepository.Update(contactPerson);
                            _logContactPersonRepository.Insert(contactPerson.ContactID, "Modify", usernameUpdateBy);
                        }
                        #endregion

                        #region Update Tbl_OrgContactPerson
                        int supplierID = _organizationRepository.GetSupplierIDByUsername(mergUser.Username);
                        var orgContactPerson = _orgContactPersonRepository.GetOrgContactPersonBySupplierIDAndContactID(supplierID, mergUser.ContactID??0);
                        if (orgContactPerson != null)
                        {
                            orgContactPerson.NewContactID = masterUser.ContactID;
                            _orgContactPersonRepository.Update(orgContactPerson);
                            _logOrgContactPersonRepository.Insert(orgContactPerson.SupplierID, orgContactPerson.ContactID, "Modify", usernameUpdateBy);
                        }
                        #endregion

                        #region Update Tbl_Organization
                        _organizationRepository.UpdateLastUpdate(supplierID, userID);
                        #endregion

                        #region Mapping Reports

                        var reportMappingMasterUser = _userReportMappingRepository.GetUserReportMappingByUserID(masterUser.UserID);

                        var reportMappingMergUser = _userReportMappingRepository.GetUserReportMappingByUserID(mergUser.UserID);

                        var queryReport = from a in reportMappingMergUser
                                    where !(reportMappingMasterUser.Any(m => m.ReportID == a.ReportID))
                                    select a;

                        var listReportMapping = queryReport.ToList();

                        foreach (var listReport in listReportMapping)
                        {
                            if (!_userReportMappingRepository.CheckExistsUserReportMapping(masterUser.UserID, listReport.ReportID))
                            {
                                var userReportMappingModel = new Tbl_UserReportMapping()
                                {
                                    UserID = masterUser.UserID,
                                    ReportID = listReport.ReportID,
                                    isEnable = listReport.isEnable
                                };

                                _userReportMappingRepository.Insert(userReportMappingModel);
                            } 
                        }

                        #endregion
                    }

                    #region Update System Role OP
                    var userMappingOP = _userSystemMappingRepository.GetUserSystemMappingOrderSysRoleIDASC(masterUser.UserID, 1);
                    int i = 1;
                    foreach (var list in userMappingOP)
                    {
                        if (i == 1)
                        {
                            if (list.MergeFromUserID != 0)
                            {
                                list.MergeFromUserID = 0;
                                _userSystemMappingRepository.Update(list);
                            }

                        }
                        else
                        {
                            if (list.MergeFromUserID == 0)
                            {
                                list.MergeFromUserID = list.UserID;
                                _userSystemMappingRepository.Update(list);
                            }
                        }

                        i++;
                    }
                    #endregion

                    #region Call API Data

                    //var listMapping = _systemRepository.GetSystemListByUsernameForUpdate(masterUser.Username);

                    var listMapping = _systemConfigureAPIRepository.GetSystemListByUsernameForUpdateAPI(masterUser.Username);

                    foreach (var sysMapping in listMapping)
                    {
                        

                        string reqID = GeneratorGuid.GetRandomGuid();

                        string Jsonstr = "";

                        string _APIKey = "";

                        string _APIName = "";

                        string guid = "mockGUID";

                        if (System.Web.HttpContext.Current.Session["GUID"] != null)
                        {
                            guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                        }

                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                        var apiUpdateModels = _profileDataService.GetDataCallUpdateUserAPI(sysMapping, reqID, usernameUpdateBy, "MergeUser");
                        Jsonstr = JsonConvert.SerializeObject(apiUpdateModels);
                        _APIKey = apiUpdateModels.Request.APIKey;
                        _APIName = apiUpdateModels.Request.APIName;

                        HttpClient client = new HttpClient();
                        client.BaseAddress = new Uri(sysMapping.API_URL);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        HttpContent content = new StringContent(Jsonstr);
                        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                        #region Insert API Log Before

                        var _APILogsBefore = new Tbl_APILogs()
                        {
                            ReqID = reqID,
                            SystemID = sysMapping.SystemID,
                            Events = "Request_Before",
                            Events_Time = DateTime.UtcNow,
                            IPAddress_Client = visitorsIPAddr,
                            APIKey = _APIKey,
                            APIName = _APIName,
                            Return_Code = "",
                            Return_Status = "",
                            Return_Message = "NO RESPONSE",
                            Data = Jsonstr,
                            UserGUID = guid,
                        };

                        listAPILogs.Add(_APILogsBefore);
                        #endregion

                        #region Call API
                        //bool isFaulted = client.PostAsync(sysMapping.API_URL, content).IsFaulted;
                        //if (isFaulted)
                        //{
                        //    #region Insert API Log

                        //    var _APILogs = new Tbl_APILogs()
                        //    {
                        //        ReqID = reqID,
                        //        SystemID = sysMapping.SystemID,
                        //        Events = "Request",
                        //        Events_Time = DateTime.UtcNow,
                        //        IPAddress_Client = visitorsIPAddr,
                        //        APIKey = _APIKey,
                        //        APIName = _APIName,
                        //        Return_Code = "",
                        //        Return_Status = "",
                        //        Return_Message = "NO RESPONSE",
                        //        Data = Jsonstr,
                        //        UserGUID = guid,
                        //    };

                        //    listAPILogs.Add(_APILogs);
                        //    #endregion

                        //    returnMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);

                        //    chk = false;
                        //    break;
                        //}

                        //HttpResponseMessage response = client.PostAsync(sysMapping.API_URL, content).Result;

                        //var responseBody = response.Content.ReadAsStringAsync().Result;

                        //if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        //{
                        //    try
                        //    {
                        //        if (!string.IsNullOrEmpty(responseBody))
                        //        {
                        //            JObject _response = JObject.Parse(responseBody);

                        //            string return_Code = _response["Response"]["Result"]["Code"].ToString();
                        //            string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                        //            string return_Message = _response["Response"]["Result"]["Message"].ToString();


                        //            #region Insert API Log

                        //            var _APILogs = new Tbl_APILogs()
                        //            {
                        //                ReqID = reqID,
                        //                SystemID = sysMapping.SystemID,
                        //                Events = "Request",
                        //                Events_Time = DateTime.UtcNow,
                        //                IPAddress_Client = visitorsIPAddr,
                        //                APIKey = _APIKey,
                        //                APIName = _APIName,
                        //                Return_Code = return_Code,
                        //                Return_Status = return_Status,
                        //                Return_Message = return_Message,
                        //                Data = Jsonstr,
                        //                UserGUID = guid,
                        //            };

                        //            listAPILogs.Add(_APILogs);

                        //            #endregion

                        //            if (!Convert.ToBoolean(return_Status))
                        //            {
                        //                if (sysMapping.SystemGrpID == 2)
                        //                {
                        //                    returnMessage = return_Message;
                        //                }
                        //                chk = false;
                        //                break;
                        //            }
                        //            else
                        //            {
                        //                listsysMappingSuccess.Add(sysMapping);
                        //            }
                        //        }
                        //        else
                        //        {
                        //            #region Insert API Log

                        //            var _APILogs = new Tbl_APILogs()
                        //            {
                        //                ReqID = reqID,
                        //                SystemID = sysMapping.SystemID,
                        //                Events = "Request",
                        //                Events_Time = DateTime.UtcNow,
                        //                IPAddress_Client = visitorsIPAddr,
                        //                APIKey = _APIKey,
                        //                APIName = _APIName,
                        //                Return_Code = "",
                        //                Return_Status = "",
                        //                Return_Message = "ResponseBody Empty",
                        //                Data = Jsonstr,
                        //                UserGUID = guid,
                        //            };

                        //            listAPILogs.Add(_APILogs);
                        //            #endregion

                        //            returnMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);

                        //            chk = false;
                        //            break;
                        //        }

                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        #region Insert API Log
                        //        string guidCall = "mockGUID";

                        //        if (System.Web.HttpContext.Current.Session["GUID"] != null)
                        //        {
                        //            guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                        //        }

                        //        string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


                        //        var _APILogsCheck = new Tbl_APILogs()
                        //        {
                        //            ReqID = reqID,
                        //            SystemID = sysMapping.SystemID,
                        //            Events = "Request_Check",
                        //            Events_Time = DateTime.UtcNow,
                        //            IPAddress_Client = visitorsIPAddrCall,
                        //            APIKey = _APIKey,
                        //            APIName = _APIName,
                        //            Return_Code = "",
                        //            Return_Status = response.StatusCode.ToString(),
                        //            Return_Message = responseBody,
                        //            Data = Jsonstr,
                        //            //Data = responseBody,
                        //            UserGUID = guidCall,
                        //        };

                        //        listAPILogs.Add(_APILogsCheck);


                        //        #endregion

                        //        throw;
                        //    }

                        //}
                        //else
                        //{

                        //    #region Insert API Log

                        //    var _APILogs = new Tbl_APILogs()
                        //    {
                        //        ReqID = reqID,
                        //        SystemID = sysMapping.SystemID,
                        //        Events = "Request",
                        //        Events_Time = DateTime.UtcNow,
                        //        IPAddress_Client = visitorsIPAddr,
                        //        APIKey = _APIKey,
                        //        APIName = _APIName,
                        //        Return_Code = "",
                        //        Return_Status = "",
                        //        Return_Message = "",
                        //        Data = Jsonstr,
                        //        UserGUID = guid,
                        //    };

                        //    listAPILogs.Add(_APILogs);
                        //    #endregion

                        //    returnMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);

                        //    chk = false;
                        //    break;
                        //}
                        #endregion

                        #region  API Post Task
                        string responseBody = "";
                        
                        var continuationTask = client.PostAsync(sysMapping.API_URL, content).ContinueWith(
                                                (postTask) =>
                                                {
                                                    bool isCompleted = postTask.IsCompleted;
                                                    bool isFaulted = postTask.IsFaulted;
                                                    if (isCompleted && !isFaulted)
                                                    {
                                                        var response = postTask.Result;
                                                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                                        {
                                                            if (!string.IsNullOrEmpty(response.Content.ReadAsStringAsync().Result))
                                                            {
                                                                responseBody = response.Content.ReadAsStringAsync().Result;
                                                                try
                                                                {

                                                                    JObject _response = JObject.Parse(responseBody);

                                                                    string return_Code = _response["Response"]["Result"]["Code"].ToString();
                                                                    string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                                                                    string return_Message = _response["Response"]["Result"]["Message"].ToString();


                                                                    #region Insert API Log

                                                                    var _APILogs = new Tbl_APILogs()
                                                                    {
                                                                        ReqID = reqID,
                                                                        SystemID = sysMapping.SystemID,
                                                                        Events = "Request",
                                                                        Events_Time = DateTime.UtcNow,
                                                                        IPAddress_Client = visitorsIPAddr,
                                                                        APIKey = _APIKey,
                                                                        APIName = _APIName,
                                                                        Return_Code = return_Code,
                                                                        Return_Status = return_Status,
                                                                        Return_Message = return_Message,
                                                                        Data = Jsonstr,
                                                                        UserGUID = guid,
                                                                    };

                                                                    listAPILogs.Add(_APILogs);

                                                                    #endregion

                                                                    if (!Convert.ToBoolean(return_Status))
                                                                    {
                                                                        if (sysMapping.SystemGrpID == 2)
                                                                        {
                                                                            //returnMessage = return_Message;
                                                                            tempMessage = return_Message;
                                                                        }
                                                                        chk = false;
                                                                        //break;
                                                                    }
                                                                    else
                                                                    {
                                                                        listsysMappingSuccess.Add(sysMapping);
                                                                    }
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    #region Insert API Log
                                                                    string guidCall = "mockGUID";

                                                                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                                                    {
                                                                        guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                                                    }

                                                                    string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


                                                                    var _APILogsCheck = new Tbl_APILogs()
                                                                    {
                                                                        ReqID = reqID,
                                                                        SystemID = sysMapping.SystemID,
                                                                        Events = "Request_Check",
                                                                        Events_Time = DateTime.UtcNow,
                                                                        IPAddress_Client = visitorsIPAddrCall,
                                                                        APIKey = _APIKey,
                                                                        APIName = _APIName,
                                                                        Return_Code = "",
                                                                        Return_Status = response.StatusCode.ToString(),
                                                                        Return_Message = responseBody,
                                                                        Data = Jsonstr,
                                                                        //Data = responseBody,
                                                                        UserGUID = guidCall,
                                                                    };

                                                                    listAPILogs.Add(_APILogsCheck);


                                                                    #endregion

                                                                    throw;
                                                                }
                                                            }


                                                        }
                                                        else
                                                        {

                                                            #region Insert API Log

                                                            var _APILogs = new Tbl_APILogs()
                                                            {
                                                                ReqID = reqID,
                                                                SystemID = sysMapping.SystemID,
                                                                Events = "Request",
                                                                Events_Time = DateTime.UtcNow,
                                                                IPAddress_Client = visitorsIPAddr,
                                                                APIKey = _APIKey,
                                                                APIName = _APIName,
                                                                Return_Code = "",
                                                                Return_Status = "",
                                                                Return_Message = "",
                                                                Data = Jsonstr,
                                                                UserGUID = guid,
                                                            };

                                                            listAPILogs.Add(_APILogs);
                                                            #endregion

                                                            //returnMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);
                                                            tempMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);
                                                            chk = false;
                                                            //break;
                                                        }
                                                    }
                                                });
                        continuationTask.Wait(20000);
                        #endregion
                        if(!chk)
                        { break; }
                    }

                    #endregion


                    if (chk)
                    {
                        ts.Complete();
                    }
                    
                    
                }


                foreach (var listLog in listAPILogs)
                {
                    _aPILogsRepository.Insert(listLog);
                }
               

                if (!chk)
                {
                    #region Call API Rollback

                    foreach (var userlist in mergUsername)
                    {

                        foreach (var sysMapping in listsysMappingSuccess)
                        {
                            //var listMapping = _systemRepository.GetSystemListByUsernameAndSystemIDForUpdate(userlist.UserMergList, sysMapping.SystemID);

                            var listMapping = _systemConfigureAPIRepository.GetSystemListByUsernameAndSystemIDForUpdateAPI(userlist.UserMergList, sysMapping.SystemID);

                            foreach (var list in listMapping)
                            {
                                string reqID = GeneratorGuid.GetRandomGuid();

                                string Jsonstr = "";

                                string _APIKey = "";

                                string _APIName = "";

                                var apiUpdateModels = _profileDataService.GetDataCallUpdateUserAPI(list, reqID, usernameUpdateBy);
                                Jsonstr = JsonConvert.SerializeObject(apiUpdateModels);
                                _APIKey = apiUpdateModels.Request.APIKey;
                                _APIName = apiUpdateModels.Request.APIName;

                                HttpClient client = new HttpClient();
                                client.BaseAddress = new Uri(sysMapping.API_URL);
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                                HttpContent content = new StringContent(Jsonstr);
                                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                                HttpResponseMessage response = client.PostAsync(sysMapping.API_URL, content).Result;

                                var responseBody = response.Content.ReadAsStringAsync().Result;
                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    JObject _response = JObject.Parse(responseBody);

                                    string return_Code = _response["Response"]["Result"]["Code"].ToString();
                                    string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
                                    string return_Message = _response["Response"]["Result"]["Message"].ToString();

                                    #region Insert API Log
                                    string guid = "mockGUID";

                                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                    {
                                        guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                    }

                                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();


                                    var _APILogRollback = new Tbl_APILogs()
                                    {
                                        ReqID = reqID,
                                        SystemID = sysMapping.SystemID,
                                        Events = "Request",
                                        Events_Time = DateTime.UtcNow,
                                        IPAddress_Client = visitorsIPAddr,
                                        APIKey = _APIKey,
                                        APIName = _APIName,
                                        Return_Code = return_Code,
                                        Return_Status = return_Status,
                                        Return_Message = return_Message,
                                        Data = Jsonstr,
                                        //Data = responseBody,
                                        UserGUID = guid,
                                    };

                                    _aPILogsRepository.Insert(_APILogRollback);
                                    #endregion
                                }else
                                {
                                    #region Insert API Log
                                    string guid = "mockGUID";

                                    if (System.Web.HttpContext.Current.Session["GUID"] != null)
                                    {
                                        guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
                                    }

                                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();


                                    var _APILogRollback = new Tbl_APILogs()
                                    {
                                        ReqID = reqID,
                                        SystemID = sysMapping.SystemID,
                                        Events = "Request",
                                        Events_Time = DateTime.UtcNow,
                                        IPAddress_Client = visitorsIPAddr,
                                        APIKey = _APIKey,
                                        APIName = _APIName,
                                        Return_Code = "",
                                        Return_Status = "",
                                        Return_Message = "",
                                        Data = Jsonstr,
                                        //Data = responseBody,
                                        UserGUID = guid,
                                    };

                                    _aPILogsRepository.Insert(_APILogRollback);

                                    #endregion
                                }
                            }
                        
                        }
                    }

                    #endregion
                }
                
            }
            catch (Exception ex)
            {
                #region Insert API Log
                string guidCall = "mockGUID";

                if (System.Web.HttpContext.Current.Session["GUID"] != null)
                {
                    guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
                }

                string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();

                var _APILogs = new Tbl_APILogs()
                {
                    ReqID = "",
                    SystemID = 0,
                    Events = "RequestException",
                    Events_Time = DateTime.UtcNow,
                    IPAddress_Client = visitorsIPAddrCall,
                    APIKey = "Pantavanij",
                    APIName = "MergUserAccount",
                    Return_Code = "",
                    Return_Status = "",
                    Return_Message = "",
                    Data = ex.Message,
                    UserGUID = guidCall,
                };

                listAPILogs.Add(_APILogs);
                #endregion

                foreach (var listLog in listAPILogs)
                {
                    _aPILogsRepository.Insert(listLog);
                }
                throw;
            }

            returnMessage = tempMessage;
            return chk;
        }

        //public virtual async Task<ResultMergeModel> MergUserAccount2(string masterUsername, List<UserMergModels> mergUsername, string usernameUpdateBy)
        //{
        //    bool chk = true;
        //    string messageReturn = "";
        //    string languageId = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
        //    string returnMessage = "";

        //    var listAPILogs = new List<Tbl_APILogs>();
        //    try
        //    {
        //        //var _APILogs = new Tbl_APILogs();


        //        var listsysMappingSuccess = new List<SystemRoleListModels>();

        //        using (TransactionScope ts = new TransactionScope())
        //        {
        //            var masterUser = _userRepository.FindByUsername(masterUsername);

        //            var updateByUser = _userRepository.FindByUsername(usernameUpdateBy);
        //            int userID = updateByUser.UserID;

        //            foreach (var userlist in mergUsername)
        //            {

        //                #region Update Tbl_UserSystemMapping

        //                var mergUser = _userRepository.FindByUsername(userlist.UserMergList);

        //                var mergUserMapping = _userSystemMappingRepository.GetUserSystemMappingByUserID(mergUser.UserID);

        //                foreach (var list in mergUserMapping)
        //                {
        //                    var userMapping = list;
        //                    _userSystemMappingRepository.UpdateByMergAccount(userMapping, masterUser.UserID);
        //                }

        //                #endregion

        //                #region Update Tbl_UserServiceMapping

        //                var serviceMappingMasterUser = _userServiceMappingRepository.GetUserServiceMappingByUserID(masterUser.UserID);

        //                var serviceMappingMergUser = _userServiceMappingRepository.GetUserServiceMappingByUserID(mergUser.UserID);

        //                var query = from a in serviceMappingMergUser
        //                            where !(serviceMappingMasterUser.Any(m => m.ServiceID == a.ServiceID))
        //                            select a;

        //                var listServiceMapping = query.ToList();

        //                foreach (var listService in listServiceMapping)
        //                {
        //                    if (!_userServiceMappingRepository.CheckExistsUserServiceMapping(masterUser.UserID, listService.ServiceID))
        //                    {
        //                        var userServiceMappingModel = new Tbl_UserServiceMapping()
        //                        {
        //                            UserID = masterUser.UserID,
        //                            ServiceID = listService.ServiceID,
        //                            isActive = listService.isActive
        //                        };

        //                        _userServiceMappingRepository.Insert(userServiceMappingModel, updateByUser.UserID);
        //                    }
        //                }

        //                #endregion

        //                #region Update Tbl_User
        //                mergUser.MergeToUserID = masterUser.UserID;
        //                mergUser.IsDeleted = 1;
        //                _userRepository.Update(mergUser);
        //                _logUserRepository.Insert(mergUser.UserID, "Modify", usernameUpdateBy);
        //                #endregion

        //                #region Update Tbl_ContactPerson
        //                var contactPerson = _contactPersonRepository.GetContactPersonByContectID(mergUser.ContactID ?? 0);
        //                if (contactPerson != null)
        //                {
        //                    contactPerson.isDeleted = 1;
        //                    _contactPersonRepository.Update(contactPerson);
        //                    _logContactPersonRepository.Insert(contactPerson.ContactID, "Modify", usernameUpdateBy);
        //                }
        //                #endregion

        //                #region Update Tbl_OrgContactPerson
        //                int supplierID = _organizationRepository.GetSupplierIDByUsername(mergUser.Username);
        //                var orgContactPerson = _orgContactPersonRepository.GetOrgContactPersonBySupplierIDAndContactID(supplierID, mergUser.ContactID ?? 0);
        //                if (orgContactPerson != null)
        //                {
        //                    orgContactPerson.NewContactID = masterUser.ContactID;
        //                    _orgContactPersonRepository.Update(orgContactPerson);
        //                    _logOrgContactPersonRepository.Insert(orgContactPerson.SupplierID, orgContactPerson.ContactID, "Modify", usernameUpdateBy);
        //                }
        //                #endregion

        //                #region Update Tbl_Organization
        //                _organizationRepository.UpdateLastUpdate(supplierID, userID);
        //                #endregion

        //                #region Mapping Reports

        //                var reportMappingMasterUser = _userReportMappingRepository.GetUserReportMappingByUserID(masterUser.UserID);

        //                var reportMappingMergUser = _userReportMappingRepository.GetUserReportMappingByUserID(mergUser.UserID);

        //                var queryReport = from a in reportMappingMergUser
        //                                  where !(reportMappingMasterUser.Any(m => m.ReportID == a.ReportID))
        //                                  select a;

        //                var listReportMapping = queryReport.ToList();

        //                foreach (var listReport in listReportMapping)
        //                {
        //                    if (!_userReportMappingRepository.CheckExistsUserReportMapping(masterUser.UserID, listReport.ReportID))
        //                    {
        //                        var userReportMappingModel = new Tbl_UserReportMapping()
        //                        {
        //                            UserID = masterUser.UserID,
        //                            ReportID = listReport.ReportID,
        //                            isEnable = listReport.isEnable
        //                        };

        //                        _userReportMappingRepository.Insert(userReportMappingModel);
        //                    }
        //                }

        //                #endregion
        //            }

        //            #region Update System Role OP
        //            var userMappingOP = _userSystemMappingRepository.GetUserSystemMappingOrderSysRoleIDASC(masterUser.UserID, 1);
        //            int i = 1;
        //            foreach (var list in userMappingOP)
        //            {
        //                if (i == 1)
        //                {
        //                    if (list.MergeFromUserID != 0)
        //                    {
        //                        list.MergeFromUserID = 0;
        //                        _userSystemMappingRepository.Update(list);
        //                    }

        //                }
        //                else
        //                {
        //                    if (list.MergeFromUserID == 0)
        //                    {
        //                        list.MergeFromUserID = list.UserID;
        //                        _userSystemMappingRepository.Update(list);
        //                    }
        //                }

        //                i++;
        //            }
        //            #endregion

        //            #region Call API Data

        //            //var listMapping = _systemRepository.GetSystemListByUsernameForUpdate(masterUser.Username);

        //            var listMapping = _systemConfigureAPIRepository.GetSystemListByUsernameForUpdateAPI(masterUser.Username);

        //            foreach (var sysMapping in listMapping)
        //            {


        //                string reqID = GeneratorGuid.GetRandomGuid();

        //                string Jsonstr = "";

        //                string _APIKey = "";

        //                string _APIName = "";

        //                string guid = "mockGUID";

        //                if (System.Web.HttpContext.Current.Session["GUID"] != null)
        //                {
        //                    guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
        //                }

        //                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

        //                var apiUpdateModels = _profileDataService.GetDataCallUpdateUserAPI(sysMapping, reqID, usernameUpdateBy, "MergeUser");
        //                Jsonstr = JsonConvert.SerializeObject(apiUpdateModels);
        //                _APIKey = apiUpdateModels.Request.APIKey;
        //                _APIName = apiUpdateModels.Request.APIName;

        //                HttpClient client = new HttpClient();
        //                client.BaseAddress = new Uri(sysMapping.API_URL);
        //                client.DefaultRequestHeaders.Accept.Clear();
        //                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //                HttpContent content = new StringContent(Jsonstr);
        //                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

        //                #region Insert API Log Before

        //                var _APILogsBefore = new Tbl_APILogs()
        //                {
        //                    ReqID = reqID,
        //                    SystemID = sysMapping.SystemID,
        //                    Events = "Request_Before",
        //                    Events_Time = DateTime.UtcNow,
        //                    IPAddress_Client = visitorsIPAddr,
        //                    APIKey = _APIKey,
        //                    APIName = _APIName,
        //                    Return_Code = "",
        //                    Return_Status = "",
        //                    Return_Message = "NO RESPONSE",
        //                    Data = Jsonstr,
        //                    UserGUID = guid,
        //                };

        //                listAPILogs.Add(_APILogsBefore);
        //                #endregion

        //                #region Call API
        //                bool isFaulted = client.PostAsync(sysMapping.API_URL, content).IsFaulted;
        //                if (isFaulted)
        //                {
        //                    #region Insert API Log

        //                    var _APILogs = new Tbl_APILogs()
        //                    {
        //                        ReqID = reqID,
        //                        SystemID = sysMapping.SystemID,
        //                        Events = "Request",
        //                        Events_Time = DateTime.UtcNow,
        //                        IPAddress_Client = visitorsIPAddr,
        //                        APIKey = _APIKey,
        //                        APIName = _APIName,
        //                        Return_Code = "",
        //                        Return_Status = "",
        //                        Return_Message = "NO RESPONSE",
        //                        Data = Jsonstr,
        //                        UserGUID = guid,
        //                    };

        //                    listAPILogs.Add(_APILogs);
        //                    #endregion

        //                    returnMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);

        //                    chk = false;
        //                    break;
        //                }

        //                HttpResponseMessage response = await client.PostAsync(sysMapping.API_URL, content);

        //                if (response.StatusCode == System.Net.HttpStatusCode.OK)
        //                {
        //                    var responseBody = response.Content.ReadAsStringAsync().Result;
        //                    try
        //                    {
        //                        if (!string.IsNullOrEmpty(responseBody))
        //                        {
        //                            JObject _response = JObject.Parse(responseBody);

        //                            string return_Code = _response["Response"]["Result"]["Code"].ToString();
        //                            string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
        //                            string return_Message = _response["Response"]["Result"]["Message"].ToString();


        //                            #region Insert API Log

        //                            var _APILogs = new Tbl_APILogs()
        //                            {
        //                                ReqID = reqID,
        //                                SystemID = sysMapping.SystemID,
        //                                Events = "Request",
        //                                Events_Time = DateTime.UtcNow,
        //                                IPAddress_Client = visitorsIPAddr,
        //                                APIKey = _APIKey,
        //                                APIName = _APIName,
        //                                Return_Code = return_Code,
        //                                Return_Status = return_Status,
        //                                Return_Message = return_Message,
        //                                Data = Jsonstr,
        //                                UserGUID = guid,
        //                            };

        //                            listAPILogs.Add(_APILogs);

        //                            #endregion

        //                            if (!Convert.ToBoolean(return_Status))
        //                            {
        //                                if (sysMapping.SystemGrpID == 2)
        //                                {
        //                                    returnMessage = return_Message;
        //                                }
        //                                chk = false;
        //                                break;
        //                            }
        //                            else
        //                            {
        //                                listsysMappingSuccess.Add(sysMapping);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            #region Insert API Log

        //                            var _APILogs = new Tbl_APILogs()
        //                            {
        //                                ReqID = reqID,
        //                                SystemID = sysMapping.SystemID,
        //                                Events = "Request",
        //                                Events_Time = DateTime.UtcNow,
        //                                IPAddress_Client = visitorsIPAddr,
        //                                APIKey = _APIKey,
        //                                APIName = _APIName,
        //                                Return_Code = "",
        //                                Return_Status = "",
        //                                Return_Message = "ResponseBody Empty",
        //                                Data = Jsonstr,
        //                                UserGUID = guid,
        //                            };

        //                            listAPILogs.Add(_APILogs);
        //                            #endregion

        //                            returnMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);

        //                            chk = false;
        //                            break;
        //                        }

        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        #region Insert API Log
        //                        string guidCall = "mockGUID";

        //                        if (System.Web.HttpContext.Current.Session["GUID"] != null)
        //                        {
        //                            guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
        //                        }

        //                        string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


        //                        var _APILogsCheck = new Tbl_APILogs()
        //                        {
        //                            ReqID = reqID,
        //                            SystemID = sysMapping.SystemID,
        //                            Events = "Request_Check",
        //                            Events_Time = DateTime.UtcNow,
        //                            IPAddress_Client = visitorsIPAddrCall,
        //                            APIKey = _APIKey,
        //                            APIName = _APIName,
        //                            Return_Code = "",
        //                            Return_Status = response.StatusCode.ToString(),
        //                            Return_Message = responseBody,
        //                            Data = Jsonstr,
        //                            //Data = responseBody,
        //                            UserGUID = guidCall,
        //                        };

        //                        listAPILogs.Add(_APILogsCheck);


        //                        #endregion

        //                        throw;
        //                    }

        //                }
        //                else
        //                {

        //                    #region Insert API Log

        //                    var _APILogs = new Tbl_APILogs()
        //                    {
        //                        ReqID = reqID,
        //                        SystemID = sysMapping.SystemID,
        //                        Events = "Request",
        //                        Events_Time = DateTime.UtcNow,
        //                        IPAddress_Client = visitorsIPAddr,
        //                        APIKey = _APIKey,
        //                        APIName = _APIName,
        //                        Return_Code = "",
        //                        Return_Status = "",
        //                        Return_Message = "",
        //                        Data = Jsonstr,
        //                        UserGUID = guid,
        //                    };

        //                    listAPILogs.Add(_APILogs);
        //                    #endregion

        //                    returnMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);

        //                    chk = false;
        //                    break;
        //                }
        //                #endregion

        //                #region  API Post Task
        //                //string responseBody = "";
        //                //string tempMessage = "";
        //                //var continuationTask = client.PostAsync(sysMapping.API_URL, content).ContinueWith(
        //                //                        (postTask) =>
        //                //                        {
        //                //                            bool isCompleted = postTask.IsCompleted;
        //                //                            bool isFaulted = postTask.IsFaulted;
        //                //                            if (isCompleted && !isFaulted)
        //                //                            {
        //                //                                var response = postTask.Result;
        //                //                                if (response.StatusCode == System.Net.HttpStatusCode.OK)
        //                //                                {
        //                //                                    if (!string.IsNullOrEmpty(response.Content.ReadAsStringAsync().Result))
        //                //                                    {
        //                //                                        responseBody = response.Content.ReadAsStringAsync().Result;
        //                //                                        try
        //                //                                        {

        //                //                                            JObject _response = JObject.Parse(responseBody);

        //                //                                            string return_Code = _response["Response"]["Result"]["Code"].ToString();
        //                //                                            string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
        //                //                                            string return_Message = _response["Response"]["Result"]["Message"].ToString();


        //                //                                            #region Insert API Log

        //                //                                            var _APILogs = new Tbl_APILogs()
        //                //                                            {
        //                //                                                ReqID = reqID,
        //                //                                                SystemID = sysMapping.SystemID,
        //                //                                                Events = "Request",
        //                //                                                Events_Time = DateTime.UtcNow,
        //                //                                                IPAddress_Client = visitorsIPAddr,
        //                //                                                APIKey = _APIKey,
        //                //                                                APIName = _APIName,
        //                //                                                Return_Code = return_Code,
        //                //                                                Return_Status = return_Status,
        //                //                                                Return_Message = return_Message,
        //                //                                                Data = Jsonstr,
        //                //                                                UserGUID = guid,
        //                //                                            };

        //                //                                            listAPILogs.Add(_APILogs);

        //                //                                            #endregion

        //                //                                            if (!Convert.ToBoolean(return_Status))
        //                //                                            {
        //                //                                                if (sysMapping.SystemGrpID == 2)
        //                //                                                {
        //                //                                                    //returnMessage = return_Message;
        //                //                                                    tempMessage = return_Message;
        //                //                                                }
        //                //                                                chk = false;
        //                //                                                //break;
        //                //                                            }
        //                //                                            else
        //                //                                            {
        //                //                                                listsysMappingSuccess.Add(sysMapping);
        //                //                                            }
        //                //                                        }
        //                //                                        catch (Exception ex)
        //                //                                        {
        //                //                                            #region Insert API Log
        //                //                                            string guidCall = "mockGUID";

        //                //                                            if (System.Web.HttpContext.Current.Session["GUID"] != null)
        //                //                                            {
        //                //                                                guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
        //                //                                            }

        //                //                                            string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();


        //                //                                            var _APILogsCheck = new Tbl_APILogs()
        //                //                                            {
        //                //                                                ReqID = reqID,
        //                //                                                SystemID = sysMapping.SystemID,
        //                //                                                Events = "Request_Check",
        //                //                                                Events_Time = DateTime.UtcNow,
        //                //                                                IPAddress_Client = visitorsIPAddrCall,
        //                //                                                APIKey = _APIKey,
        //                //                                                APIName = _APIName,
        //                //                                                Return_Code = "",
        //                //                                                Return_Status = response.StatusCode.ToString(),
        //                //                                                Return_Message = responseBody,
        //                //                                                Data = Jsonstr,
        //                //                                                //Data = responseBody,
        //                //                                                UserGUID = guidCall,
        //                //                                            };

        //                //                                            listAPILogs.Add(_APILogsCheck);


        //                //                                            #endregion

        //                //                                            throw;
        //                //                                        }
        //                //                                    }


        //                //                                }
        //                //                                else
        //                //                                {

        //                //                                    #region Insert API Log

        //                //                                    var _APILogs = new Tbl_APILogs()
        //                //                                    {
        //                //                                        ReqID = reqID,
        //                //                                        SystemID = sysMapping.SystemID,
        //                //                                        Events = "Request",
        //                //                                        Events_Time = DateTime.UtcNow,
        //                //                                        IPAddress_Client = visitorsIPAddr,
        //                //                                        APIKey = _APIKey,
        //                //                                        APIName = _APIName,
        //                //                                        Return_Code = "",
        //                //                                        Return_Status = "",
        //                //                                        Return_Message = "",
        //                //                                        Data = Jsonstr,
        //                //                                        UserGUID = guid,
        //                //                                    };

        //                //                                    listAPILogs.Add(_APILogs);
        //                //                                    #endregion

        //                //                                    //returnMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);
        //                //                                    tempMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);
        //                //                                    chk = false;
        //                //                                    //break;
        //                //                                }
        //                //                            }
        //                //                        });
        //                //continuationTask.Wait(20000);
        //                #endregion

        //                //if (!chk)
        //                //{ break; }
        //            }

        //            #endregion


        //            if (chk)
        //            {
        //                ts.Complete();
        //            }


        //        }


        //        foreach (var listLog in listAPILogs)
        //        {
        //            _aPILogsRepository.Insert(listLog);
        //        }


        //        if (!chk)
        //        {
        //            #region Call API Rollback

        //            foreach (var userlist in mergUsername)
        //            {

        //                foreach (var sysMapping in listsysMappingSuccess)
        //                {
        //                    //var listMapping = _systemRepository.GetSystemListByUsernameAndSystemIDForUpdate(userlist.UserMergList, sysMapping.SystemID);

        //                    var listMapping = _systemConfigureAPIRepository.GetSystemListByUsernameAndSystemIDForUpdateAPI(userlist.UserMergList, sysMapping.SystemID);

        //                    foreach (var list in listMapping)
        //                    {
        //                        string reqID = GeneratorGuid.GetRandomGuid();

        //                        string Jsonstr = "";

        //                        string _APIKey = "";

        //                        string _APIName = "";

        //                        var apiUpdateModels = _profileDataService.GetDataCallUpdateUserAPI(list, reqID, usernameUpdateBy);
        //                        Jsonstr = JsonConvert.SerializeObject(apiUpdateModels);
        //                        _APIKey = apiUpdateModels.Request.APIKey;
        //                        _APIName = apiUpdateModels.Request.APIName;

        //                        HttpClient client = new HttpClient();
        //                        client.BaseAddress = new Uri(sysMapping.API_URL);
        //                        client.DefaultRequestHeaders.Accept.Clear();
        //                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        //                        HttpContent content = new StringContent(Jsonstr);
        //                        content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

        //                        HttpResponseMessage response = client.PostAsync(sysMapping.API_URL, content).Result;

        //                        var responseBody = response.Content.ReadAsStringAsync().Result;
        //                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
        //                        {
        //                            JObject _response = JObject.Parse(responseBody);

        //                            string return_Code = _response["Response"]["Result"]["Code"].ToString();
        //                            string return_Status = _response["Response"]["Result"]["SuccessStatus"].ToString();
        //                            string return_Message = _response["Response"]["Result"]["Message"].ToString();

        //                            #region Insert API Log
        //                            string guid = "mockGUID";

        //                            if (System.Web.HttpContext.Current.Session["GUID"] != null)
        //                            {
        //                                guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
        //                            }

        //                            string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();


        //                            var _APILogRollback = new Tbl_APILogs()
        //                            {
        //                                ReqID = reqID,
        //                                SystemID = sysMapping.SystemID,
        //                                Events = "Request",
        //                                Events_Time = DateTime.UtcNow,
        //                                IPAddress_Client = visitorsIPAddr,
        //                                APIKey = _APIKey,
        //                                APIName = _APIName,
        //                                Return_Code = return_Code,
        //                                Return_Status = return_Status,
        //                                Return_Message = return_Message,
        //                                Data = Jsonstr,
        //                                //Data = responseBody,
        //                                UserGUID = guid,
        //                            };

        //                            _aPILogsRepository.Insert(_APILogRollback);
        //                            #endregion
        //                        }
        //                        else
        //                        {
        //                            #region Insert API Log
        //                            string guid = "mockGUID";

        //                            if (System.Web.HttpContext.Current.Session["GUID"] != null)
        //                            {
        //                                guid = System.Web.HttpContext.Current.Session["GUID"].ToString();
        //                            }

        //                            string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();


        //                            var _APILogRollback = new Tbl_APILogs()
        //                            {
        //                                ReqID = reqID,
        //                                SystemID = sysMapping.SystemID,
        //                                Events = "Request",
        //                                Events_Time = DateTime.UtcNow,
        //                                IPAddress_Client = visitorsIPAddr,
        //                                APIKey = _APIKey,
        //                                APIName = _APIName,
        //                                Return_Code = "",
        //                                Return_Status = "",
        //                                Return_Message = "",
        //                                Data = Jsonstr,
        //                                //Data = responseBody,
        //                                UserGUID = guid,
        //                            };

        //                            _aPILogsRepository.Insert(_APILogRollback);

        //                            #endregion
        //                        }
        //                    }

        //                }
        //            }

        //            #endregion
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        #region Insert API Log
        //        string guidCall = "mockGUID";

        //        if (System.Web.HttpContext.Current.Session["GUID"] != null)
        //        {
        //            guidCall = System.Web.HttpContext.Current.Session["GUID"].ToString();
        //        }

        //        string visitorsIPAddrCall = GetClientIPAddressHelper.GetClientIPAddres();

        //        var _APILogs = new Tbl_APILogs()
        //        {
        //            ReqID = "",
        //            SystemID = 0,
        //            Events = "RequestException",
        //            Events_Time = DateTime.UtcNow,
        //            IPAddress_Client = visitorsIPAddrCall,
        //            APIKey = "Pantavanij",
        //            APIName = "MergUserAccount",
        //            Return_Code = "",
        //            Return_Status = "",
        //            Return_Message = "",
        //            Data = ex.Message,
        //            UserGUID = guidCall,
        //        };

        //        listAPILogs.Add(_APILogs);
        //        #endregion

        //        foreach (var listLog in listAPILogs)
        //        {
        //            _aPILogsRepository.Insert(listLog);
        //        }
        //        throw;
        //    }

        //    var resultModel = new ResultMergeModel();
        //    resultModel.isSuccess = chk;
        //    resultModel.ReturnMessage = returnMessage;

        //    return resultModel;
        //}

    }
    
}
