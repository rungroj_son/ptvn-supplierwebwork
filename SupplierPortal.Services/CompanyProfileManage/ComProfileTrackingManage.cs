﻿using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.TrackingRequest;

namespace SupplierPortal.Services.CompanyProfileManage
{
    public static class ComProfileTrackingManage
    {

        public static IQueryable<ViewTrackingRequestModel> GetTrackingRequestWaitingApproveBySupplierID(int supplierID)
        {

            ITrackingRequestRepository _repo = new TrackingRequestRepository();

            return _repo.GetTrackingRequestWaitingApproveBySupplierID(supplierID);

        }

        public static IQueryable<ViewTrackingRequestModel> GetTrackingRequestAllBySupplierID(int supplierID)
        {

            ITrackingRequestRepository _repo = new TrackingRequestRepository();

            return _repo.GetTrackingRequestAllBySupplierID(supplierID);

        }

        public static string GetTrackingStatusNameWaitingApproveByTrackingStatusID(int trackingStatusID)
        {
            ITrackingRequestRepository _repo = new TrackingRequestRepository();

            string trackingStatusName = _repo.GetTrackingStatusNameWaitingApproveByTrackingStatusID(trackingStatusID);
            return trackingStatusName;
        }
    }
}
