﻿using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.BusinessMain;
using SupplierPortal.Data.Models.Repository.OrgBusinessType;
using SupplierPortal.Data.Models.Repository.OrgProduct;
using SupplierPortal.Data.Models.Repository.BusinessType;
using SupplierPortal.Data.Models.Repository.OrgProductKeyword;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.OrgContactPerson;
using SupplierPortal.Data.Models.Repository.OrgContactAddressMapping;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.OrgFacility;
using SupplierPortal.Data.Models.Repository.OrgCertifiedStd;
using SupplierPortal.Data.Models.Repository.CertifiedStd;
using SupplierPortal.Data.Models.Repository.OrgCertifiedStdAttachment;
using SupplierPortal.Data.Models.Repository.TrackingComprofile;
using SupplierPortal.Data.Models.Repository.Country;

namespace SupplierPortal.Services.CompanyProfileManage
{
    public partial class ComProfileDataService : IComProfileDataService
    {
        IOrganizationRepository _organizationRepository;
        IBusinessMainRepository _businessMainRepository;
        IOrgBusinessTypeRepository _orgBusinessTypeRepository;
        IBusinessTypeRepository _businessTypeRepository;
        IOrgProductRepository _orgProductRepository;
        IOrgProductKeywordRepository _orgProductKeywordRepository;
        IContactPersonRepository _contactPersonRepository;
        IOrgContactPerson _orgContactPerson;
        IOrgContactAddressMappingRepository _orgContactAddressMappingRepository;
        IAddressRepository _addressRepository;
        IOrgFacilityRepository _orgFacilityRepository;
        IOrgCertifiedStdRepository _orgCertifiedStdRepository;
        ICertifiedStdRepository _certifiedStdRepository;
        IOrgCertifiedStdAttachmentRepository _orgCertifiedStdAttachmentRepository;
        ITrackingComprofileRepository _trackingComprofileRepository;
        ICountryRepository _countryRepository;

        public ComProfileDataService()
        {
            _organizationRepository = new OrganizationRepository();
            _businessMainRepository = new BusinessMainRepository();
            _orgBusinessTypeRepository = new OrgBusinessTypeRepository();
            _businessTypeRepository = new BusinessTypeRepository();
            _orgProductRepository = new OrgProductRepository();
            _orgProductKeywordRepository = new OrgProductKeywordRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _orgContactPerson = new OrgContactPerson();
            _orgContactAddressMappingRepository = new OrgContactAddressMappingRepository();
            _addressRepository = new AddressRepository();
            _orgFacilityRepository = new OrgFacilityRepository();
            _orgCertifiedStdRepository = new OrgCertifiedStdRepository();
            _certifiedStdRepository = new CertifiedStdRepository();
            _orgCertifiedStdAttachmentRepository = new OrgCertifiedStdAttachmentRepository();
            _trackingComprofileRepository = new TrackingComprofileRepository();
            _countryRepository = new CountryRepository();
        }

        public ComProfileDataService(
            OrganizationRepository organizationRepository,
            BusinessMainRepository businessMainRepository,
            OrgBusinessTypeRepository orgBusinessTypeRepository,
            BusinessTypeRepository businessTypeRepository,
            OrgProductRepository orgProductRepository,
            OrgProductKeywordRepository orgProductKeywordRepository,
            ContactPersonRepository contactPersonRepository,
            OrgContactPerson orgContactPerson,
            OrgContactAddressMappingRepository orgContactAddressMappingRepository,
            AddressRepository addressRepository,
            OrgFacilityRepository orgFacilityRepository,
            OrgCertifiedStdRepository orgCertifiedStdRepository,
            CertifiedStdRepository certifiedStdRepository,
            OrgCertifiedStdAttachmentRepository orgCertifiedStdAttachmentRepository,
            TrackingComprofileRepository trackingComprofileRepository,
            CountryRepository countryRepository
            )
        {
            _organizationRepository = organizationRepository;
            _businessMainRepository = businessMainRepository;
            _orgBusinessTypeRepository = orgBusinessTypeRepository;
            _businessTypeRepository = businessTypeRepository;
            _orgProductRepository = orgProductRepository;
            _orgProductKeywordRepository = orgProductKeywordRepository;
            _contactPersonRepository = contactPersonRepository;
            _orgContactPerson = orgContactPerson;
            _orgContactAddressMappingRepository = orgContactAddressMappingRepository;
            _addressRepository = addressRepository;
            _orgFacilityRepository = orgFacilityRepository;
            _orgCertifiedStdRepository = orgCertifiedStdRepository;
            _certifiedStdRepository = certifiedStdRepository;
            _orgCertifiedStdAttachmentRepository = orgCertifiedStdAttachmentRepository;
            _trackingComprofileRepository = trackingComprofileRepository;
            _countryRepository = countryRepository;
        }



        public virtual CompanyGeneralModel GetGeneralComProfileBySupplierID(int supplierID)
        {
            var model = new CompanyGeneralModel();

            var orgModel = _organizationRepository.GetDataBySupplierID(supplierID);

            //var trackingModel = _trackingComprofileRepository.GetDataBySupplierID(supplierID);
            string countryName =  "";
            if (orgModel != null)
            {

                countryName = _countryRepository.GetCountryNameByCountryCode(orgModel.CountryCode);

                model.OrgID = orgModel.OrgID;
                model.SupplierID = orgModel.SupplierID;
                model.CompanyTypeID = orgModel.CompanyTypeID;
                model.BusinessEntityID = orgModel.BusinessEntityID;
                model.OtherBusinessEntity = orgModel.OtherBusinessEntity;
                model.CompanyName_Local = orgModel.CompanyName_Local;
                model.CompanyName_Inter = orgModel.CompanyName_Inter;
                model.CountryCode = orgModel.CountryCode;
                model.CountryName = countryName;
                model.TaxID = orgModel.TaxID;
                model.DUNSNumber = orgModel.DUNSNumber;
                model.BranchNo = orgModel.BranchNo;
                model.BranchName_Local = orgModel.BranchName_Local;
                model.BranchName_Inter = orgModel.BranchName_Inter;

                //model.TrackingStatusID = trackingModel.TrackingStatusID;
            }

            return model;
        }

        public virtual CompanyProductModel GetProductServiceBySupplierID(int supplierID)
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();
            int languageId = Convert.ToInt32(languageID);

            var model = new CompanyProductModel();
            var listBusinessType = new List<BusinessTypeModel>();

            var orgModel = _organizationRepository.GetDataBySupplierID(supplierID);
            if (orgModel != null)
            {
                model.SupplierID = orgModel.SupplierID;
                model.MainBusinessCode = orgModel.MainBusinessCode;
                model.BusinessLongDes = _businessMainRepository.GetBusinessLongDesByBusinessCodeAndLanguageID(orgModel.MainBusinessCode, languageId) ?? "";


                var orgBusinessType = _orgBusinessTypeRepository.GetOrgBusinessTypeBySupplierID(supplierID);
                if (orgBusinessType.Count() > 0)
                {
                    foreach (var item in orgBusinessType)
                    {
                        var itemList = new BusinessTypeModel()
                        {
                            BusinessTypeID = item.BusinessTypeID
                        };
                        listBusinessType.Add(itemList);
                    }
                }
                model.OrgBusinessType = listBusinessType;
            }

            return model;
        }

        public virtual CompanyProductKeywordModel GetProductKeywordBySupplierID(int supplierID)
        {
            var model = new CompanyProductKeywordModel();
            var listProductKeyword = new List<ProductKeywordModel>();
            model.SupplierID = supplierID;

            var productKeywordModel = _orgProductKeywordRepository.GetProductKeywordBySupplierID(supplierID);

            if (productKeywordModel.Count() > 0 )
            {
                foreach (var item in productKeywordModel)
                {
                    var itemList = new ProductKeywordModel()
                    {
                        ProductKeyword = item.Keyword
                    };
                    listProductKeyword.Add(itemList);
                }
            }

            model.OrgProductKeyword = listProductKeyword;

            return model;
        }

        public virtual void UpdateProductService(CompanyProductModel model,int updateByUserID)
        {

            #region-----------------------Manage OrgBusinessType------------------------

            var businessType = _businessTypeRepository.GetBusinessTypeAll();

            foreach (var item in businessType)
            {
                if (model.OrgBusinessType.Exists(m => m.BusinessTypeID == item.BusinessTypeID))
                {
                    if (!_orgBusinessTypeRepository.OrgBusinessTypeExists(model.SupplierID,item.BusinessTypeID))
                    {
                        var businessTypeModel = new Tbl_OrgBusinessType()
                        {
                            SupplierID = model.SupplierID,
                            BusinessTypeID = item.BusinessTypeID
                        };
                        _orgBusinessTypeRepository.Insert(businessTypeModel, updateByUserID);
                    }
                }else
                {
                    if (_orgBusinessTypeRepository.OrgBusinessTypeExists(model.SupplierID, item.BusinessTypeID))
                    {
                        _orgBusinessTypeRepository.Delete(model.SupplierID, item.BusinessTypeID, updateByUserID);
                    }
                }
            }
            #endregion

            #region-----------------------Manage OrgProduct------------------------
            foreach (var itemList in model.OrgProductList)
            {
                if (itemList.ActionLog == "Add")
                {
                    _orgProductRepository.Insert(model.SupplierID, itemList.ProdTypeID, itemList.ProductCode, updateByUserID);
                }
                else if (itemList.ActionLog == "Remove")
                {
                    _orgProductRepository.Delete(model.SupplierID, itemList.ProdTypeID, itemList.ProductCode, updateByUserID);
                }
                else if (itemList.ActionLog == "RemoveAll")
                {
                    _orgProductRepository.Delete(model.SupplierID, itemList.ProductCode, updateByUserID);
                }
            }
            #endregion
        }

        public virtual void InsertCompanyContactPerson(CompanyContactPersonModel model, int updateByUserID)
        {
            #region-----------------------Manage ContactPerson------------------------

            var contactPersonModel = new Tbl_ContactPerson()
            {
                TitleID = model.TitleID,
                FirstName_Local = model.FirstName_Local ?? "",
                FirstName_Inter = model.FirstName_Inter ?? "",
                LastName_Local = model.LastName_Local ?? "",
                LastName_Inter = model.LastName_Inter ?? "",
                JobTitleID = model.JobTitleID,
                OtherJobTitle = model.OtherJobTitle??"",
                Department = model.Department??"",
                PhoneCountryCode = "",
                PhoneNo = model.PhoneNo??"",
                PhoneExt = model.PhoneExt??"",
                MobileCountryCode = "",
                MobileNo = model.MobileNo??"",
                FaxNo = model.FaxNo??"",
                FaxExt = model.FaxExt??"",
                Email = model.Email ?? "",
                isDeleted = 0

            };

            int contactID = _contactPersonRepository.InsertContactPersonReturnContactID(contactPersonModel, updateByUserID);
            model.ContactID = contactID;
            #endregion

            #region-----------------------Manage OrgContactPerson------------------------
            var orgContactPersonModel = new Tbl_OrgContactPerson()
            {
                SupplierID = model.SupplierID,
                ContactID = contactID,
                ContactTypeID = 0,
                isPrimaryContact = 0,
                NewContactID = 0
            };
            _orgContactPerson.Insert(orgContactPersonModel, updateByUserID);
            #endregion

            #region-----------------------Manage Address------------------------

            var addressModel = new Tbl_Address()
            {
                HouseNo_Local = model.Address_HouseNo_Local??"",
                HouseNo_Inter = "",
                VillageNo_Local = model.Address_VillageNo_Local ?? "",
                VillageNo_Inter = "",
                Lane_Local = model.Address_Lane_Local ?? "",
                Lane_Inter = "",
                Road_Local = model.Address_Road_Local ?? "",
                Road_Inter = "",
                SubDistrict_Local = model.Address_SubDistrict_Local ?? "",
                SubDistrict_Inter = "",
                City_Local = model.Address_City_Local ?? "",
                City_Inter = "",
                State_Local = model.Address_State_Local ?? "",
                State_Inter = "",
                CountryCode = model.Address_CountryCode ?? "",
                PostalCode = model.Address_PostalCode ?? ""
            };

            int addressID = _addressRepository.InsertAddressReturnaddressID(addressModel, updateByUserID);

            var contactAddressMapping = new Tbl_OrgContactAddressMapping()
            {
                ContactID = contactID,
                AddressID = addressID
            };

            _orgContactAddressMappingRepository.Insert(contactAddressMapping, updateByUserID);

            #endregion

        }

        public virtual void DeleteCompanyContactPerson(int supplierID, int contactID, int updateByUserID)
        {
            _contactPersonRepository.DeleteByContactID(contactID, updateByUserID);
            _orgContactPerson.Delete(supplierID, contactID, updateByUserID);

            var contactAddressMapping = _orgContactAddressMappingRepository.GetOrgContactAddrMappingByContactID(contactID);

            if (contactAddressMapping != null)
            {
                int addressID = contactAddressMapping.AddressID??0;

                var addressModel = _addressRepository.GetAddressByAddressID(addressID);

                if (addressModel != null)
                {
                    _addressRepository.Delete(addressID, updateByUserID);
                }

                _orgContactAddressMappingRepository.Delete(contactAddressMapping.Id, updateByUserID);
            }

        }

        public virtual void EditCompanyContactPerson(CompanyContactPersonModel model, int updateByUserID)
        {
            #region-----------------------Manage ContactPerson------------------------
            var contactPersonModel = _contactPersonRepository.GetContactPersonByContectID(model.ContactID);
            if (contactPersonModel != null)
            {
                contactPersonModel.Email = model.Email;
                contactPersonModel.TitleID = model.TitleID;
                contactPersonModel.FirstName_Local = model.FirstName_Local ?? "";
                contactPersonModel.FirstName_Inter = model.FirstName_Inter ?? "";
                contactPersonModel.LastName_Local = model.LastName_Local ?? "";
                contactPersonModel.LastName_Inter = model.LastName_Inter ?? "";
                contactPersonModel.JobTitleID = model.JobTitleID;
                contactPersonModel.OtherJobTitle = model.OtherJobTitle ?? "";
                contactPersonModel.Department = model.Department ?? "";
                contactPersonModel.PhoneNo = model.PhoneNo ?? "";
                contactPersonModel.PhoneExt = model.PhoneExt ?? "";
                contactPersonModel.MobileNo = model.MobileNo ?? "";
                contactPersonModel.FaxNo = model.FaxNo ?? "";
                contactPersonModel.FaxExt = model.FaxExt ?? "";

                _contactPersonRepository.Update(contactPersonModel, updateByUserID);
            }
            #endregion

            #region-----------------------Manage Address------------------------

            var addressModel = _addressRepository.GetAddressByAddressID(model.AddressID??0);
            if (addressModel != null)
            {
                addressModel.HouseNo_Local = model.Address_HouseNo_Local;
                addressModel.VillageNo_Local = model.Address_VillageNo_Local ?? "";
                addressModel.Lane_Local = model.Address_Lane_Local ?? "";
                addressModel.Road_Local = model.Address_Road_Local ?? "";
                addressModel.SubDistrict_Local = model.Address_SubDistrict_Local;
                addressModel.City_Local = model.Address_City_Local;
                addressModel.State_Local = model.Address_State_Local;
                addressModel.CountryCode = model.Address_CountryCode;
                addressModel.PostalCode = model.Address_PostalCode;

                _addressRepository.UpdateAddress(addressModel, updateByUserID);
            }else
            {
                var addressNewModel = new Tbl_Address()
                {
                    HouseNo_Local = model.Address_HouseNo_Local ?? "",
                    HouseNo_Inter = "",
                    VillageNo_Local = model.Address_VillageNo_Local ?? "",
                    VillageNo_Inter = "",
                    Lane_Local = model.Address_Lane_Local ?? "",
                    Lane_Inter = "",
                    Road_Local = model.Address_Road_Local ?? "",
                    Road_Inter = "",
                    SubDistrict_Local = model.Address_SubDistrict_Local ?? "",
                    SubDistrict_Inter = "",
                    City_Local = model.Address_City_Local ?? "",
                    City_Inter = "",
                    State_Local = model.Address_State_Local ?? "",
                    State_Inter = "",
                    CountryCode = model.Address_CountryCode ?? "",
                    PostalCode = model.Address_PostalCode ?? ""
                };

                int addressID = _addressRepository.InsertAddressReturnaddressID(addressNewModel, updateByUserID);

                var contactAddressMapping = new Tbl_OrgContactAddressMapping()
                {
                    ContactID = model.ContactID,
                    AddressID = addressID
                };

                _orgContactAddressMappingRepository.Insert(contactAddressMapping, updateByUserID);
            }
            #endregion
        }

        public virtual CompanyContactPersonModel GetCompanyContactPersonByContactID(int contactID)
        {
            var model = new CompanyContactPersonModel();

            var contactPerson = _contactPersonRepository.GetContactPersonByContectID(contactID);
            if (contactPerson != null)
            {
                model.ContactID = contactPerson.ContactID;
                model.Email = contactPerson.Email;
                model.InitialEmail = contactPerson.Email;
                model.TitleID = contactPerson.TitleID;
                model.TitleID_Inter = contactPerson.TitleID;
                model.FirstName_Local = contactPerson.FirstName_Local;
                model.FirstName_Inter = contactPerson.FirstName_Inter;
                model.LastName_Local = contactPerson.LastName_Local;
                model.LastName_Inter = contactPerson.LastName_Inter;
                model.JobTitleID = contactPerson.JobTitleID;
                model.OtherJobTitle = contactPerson.OtherJobTitle;
                model.Department = contactPerson.Department;
                model.PhoneNo = contactPerson.PhoneNo;
                model.PhoneExt = contactPerson.PhoneExt;
                model.MobileNo = contactPerson.MobileNo;
                model.FaxNo = contactPerson.FaxNo;
                model.FaxExt = contactPerson.FaxExt;

            }

            var addressMapping = _orgContactAddressMappingRepository.GetOrgContactAddrMappingByContactID(contactID);
            if (addressMapping != null)
            {
                model.AddressID = addressMapping.AddressID;

                var address = _addressRepository.GetAddressByAddressID(addressMapping.AddressID??0);

                if (address != null)
                {
                    model.Address_HouseNo_Local = address.HouseNo_Local;
                    model.Address_VillageNo_Local = address.VillageNo_Local;
                    model.Address_Lane_Local = address.Lane_Local;
                    model.Address_Road_Local = address.Road_Local;
                    model.Address_SubDistrict_Local = address.SubDistrict_Local;
                    model.Address_City_Local = address.City_Local;
                    model.Address_State_Local = address.State_Local;
                    model.Address_CountryCode = address.CountryCode;
                    model.Address_PostalCode = address.PostalCode;
                }
            }

            return model;
        }

        public virtual CompanyFacilityModel GetDataFacAddressBySupplierID(int supplierID, int facilityID)
        {
            var ORGFacList = _orgFacilityRepository.GetDataFacAddressBySupplierID(supplierID);
            var item=ORGFacList.FirstOrDefault(m=>m.FacilityID==facilityID);
            //ModelState.Clear();
            var model = new CompanyFacilityModel();

            model.SupplierID = supplierID;
            model.FacilityID = item.FacilityID;
            model.FacilityTypeID = item.FacilityTypeID;
            model.FacilityType = item.FacilityType;
            model.FacilityName = item.FacilityName;
            model.PhoneNo = item.PhoneNo;
            model.PhoneExt = item.PhoneExt;
            model.MobileNo = item.MobileNo;
            model.FaxNo = item.FaxNo;
            model.FaxExt = item.FaxExt;
            model.WebSite = item.WebSite;
            model.SeqNo = item.SeqNo;

            model.AddressID = item.AddressID;
            model.HouseNo_Inter = item.HouseNo_Inter;
            model.VillageNo_Inter = item.VillageNo_Inter;
            model.Lane_Inter = item.Lane_Inter;
            model.Road_Inter = item.Road_Inter;
            model.SubDistrict_Inter = item.SubDistrict_Inter;
            model.City_Inter = item.City_Inter;
            model.State_Inter = item.State_Inter;

            model.HouseNo_Local = item.HouseNo_Local;
            model.VillageNo_Local = item.VillageNo_Local;
            model.Lane_Local = item.Lane_Local;
            model.Road_Local = item.Road_Local;
            model.SubDistrict_Local = item.SubDistrict_Local;
            model.City_Local = item.City_Local;
            model.State_Local = item.State_Local;

            model.CountryCode = item.CountryCode;
            model.PostalCode = item.PostalCode;

            return model;
        }

        public virtual IEnumerable<OrgCertifiedStdTableModel> GetOrgCertifiedStdTableBySupplierID(int supplierID)
        {
            var listOrgCertifiedStd = new List<OrgCertifiedStdTableModel>();

            var orgCertifiedStd = _orgCertifiedStdRepository.GetOrgCertifiedStdBySupplierID(supplierID).OrderBy(m=>m.SeqNo);

            foreach(var item in orgCertifiedStd)
            {
                var certifiedStd = _certifiedStdRepository.GetCertifiedStdByCertifiedID(item.CertifiedID);
                string standardName = certifiedStd.StandardName;
                
                string validFrom = item.ValidFrom.GetValueOrDefault().ToString("dd/MM/yyyy");
                string validTo = item.ValidTo.GetValueOrDefault().ToString("dd/MM/yyyy");
                var OrgCertifiedStdModel = new OrgCertifiedStdTableModel()
                {
                    CertId = item.CertId,
                    SupplierID = item.SupplierID,
                    SeqNo = item.SeqNo,
                    CertifiedID = item.CertifiedID,
                    StandardName = standardName,
                    OtherStandardName = item.OtherStandardName,
                    ValidFrom = validFrom,
                    ValidTo = validTo
                };

                var listAttachment = _orgCertifiedStdAttachmentRepository.GetOrgCertifiedStdAttachmentByCertId(item.CertId);

                OrgCertifiedStdModel.OrgCertifiedStdAttachment = listAttachment.ToList();

                listOrgCertifiedStd.Add(OrgCertifiedStdModel);
            }

            return listOrgCertifiedStd;
        }

        public virtual CompanyCertificationModel GetCompanyCertificationByCertId(int certId)
        {
            var model = new CompanyCertificationModel();

            var orgCertifiedStd = _orgCertifiedStdRepository.GetOrgCertifiedStdByCertId(certId);
            if (orgCertifiedStd != null)
            {
                string validFrom = orgCertifiedStd.ValidFrom.GetValueOrDefault().ToString("dd/MM/yyyy");
                string validTo = orgCertifiedStd.ValidTo.GetValueOrDefault().ToString("dd/MM/yyyy");

                model.CertId = orgCertifiedStd.CertId;
                model.SupplierID = orgCertifiedStd.SupplierID;
                model.CertifiedID = orgCertifiedStd.CertifiedID;
                model.SeqNo = orgCertifiedStd.SeqNo;
                model.OtherStandardName = orgCertifiedStd.OtherStandardName;
                model.ValidFrom = validFrom;
                model.ValidTo = validTo;
            }

            var listAttachment = _orgCertifiedStdAttachmentRepository.GetOrgCertifiedStdAttachmentByCertId(certId);

            model.OrgCertifiedStdAttachment = listAttachment.ToList();

            return model;
        }

        public virtual void DeleteOrgCertifiedStd(int certId, int updateByUserID)
        {
            _orgCertifiedStdRepository.Delete(certId, updateByUserID);

            var listAttachment = _orgCertifiedStdAttachmentRepository.GetOrgCertifiedStdAttachmentByCertId(certId);

            foreach (var item in listAttachment)
            {
                _orgCertifiedStdAttachmentRepository.DeleteByCertIdAndAttachmentID(item.CertId, item.AttachmentID, updateByUserID);
            }

        }
    }
}
