﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.OrgAttachment;
using SupplierPortal.Data.Models.Repository.CustomerDocName;
using SupplierPortal.Data.Models.Repository.TrackingOrgAttachment;
using SupplierPortal.Data.Models.Repository.TrackingOrgAddress;
using SupplierPortal.Data.Models.Repository.TrackingComprofile;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Data.Models.Repository.PortalFieldConfig;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.TrackingRequest;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.MessageTemplate;
using SupplierPortal.Data.Models.Repository.EmailAccount;
using SupplierPortal.Data.Models.Repository.BusinessEntity;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.NameTitle;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Models.MappingTable;
using System.Web;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.MainFunction.Address;
using SupplierPortal.MainFunction.SupportModel;
using SupplierPortal.MainFunction.Crypto;
using SupplierPortal.MainFunction.AddressManagementString;
using SupplierPortal.Services.AccountManage;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;

namespace SupplierPortal.Services.CompanyProfileManage
{
    public partial class ComProfileTrackingService : IComProfileTrackingService
    {
        IOrgAttachmentRepository _orgAttachmentRepository;
        ICustomerDocNameRepository _customerDocNameRepository;
        ITrackingOrgAttachmentRepository _trackingOrgAttachmentRepository;
        ITrackingOrgAddressRepository _trackingOrgAddressRepository;
        ITrackingComprofileRepository _trackingComprofileRepository;
        IAddressRepository _addressRepository;
        IPortalFieldConfigRepository _portalFieldConfigRepository;
        ICountryRepository _countryRepository;
        ITrackingRequestRepository _trackingRequestRepository;
        IOrganizationRepository _organizationRepository;
        IMessageTemplateRepository _messageTemplateRepository;
        IEmailAccountRepository _emailAccountRepository;
        IBusinessEntityRepository _businessEntityRepository;
        ILanguageRepository _languageRepository;
        IUserRepository _userRepository;
        IContactPersonRepository _contactPersonRepository;
        INameTitleRepository _nameTitleRepository; 

        public ComProfileTrackingService()
        {
            _orgAttachmentRepository = new OrgAttachmentRepository();
            _customerDocNameRepository = new CustomerDocNameRepository();
            _trackingOrgAttachmentRepository = new TrackingOrgAttachmentRepository();
            _trackingOrgAddressRepository = new TrackingOrgAddressRepository();
            _trackingComprofileRepository = new TrackingComprofileRepository();
            _addressRepository = new AddressRepository();
            _portalFieldConfigRepository = new PortalFieldConfigRepository();
            _countryRepository = new CountryRepository();
            _trackingRequestRepository = new TrackingRequestRepository();
            _organizationRepository = new OrganizationRepository();
            _messageTemplateRepository = new MessageTemplateRepository();
            _emailAccountRepository = new EmailAccountRepository();
            _businessEntityRepository = new BusinessEntityRepository();
            _languageRepository = new LanguageRepository();
            _userRepository = new UserRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _nameTitleRepository = new NameTitleRepository();
        }

        public ComProfileTrackingService(
            OrgAttachmentRepository  orgAttachmentRepository,
            CustomerDocNameRepository customerDocNameRepository,
            TrackingOrgAttachmentRepository trackingOrgAttachmentRepository,
            TrackingOrgAddressRepository trackingOrgAddressRepository,
            TrackingComprofileRepository trackingComprofileRepository,
            AddressRepository addressRepository,
            PortalFieldConfigRepository portalFieldConfigRepository,
            CountryRepository countryRepository,
            TrackingRequestRepository trackingRequestRepository,
            OrganizationRepository organizationRepository,
            MessageTemplateRepository messageTemplateRepository,
            EmailAccountRepository emailAccountRepository,
            BusinessEntityRepository businessEntityRepository,
            LanguageRepository languageRepository,
            UserRepository userRepository,
            ContactPersonRepository contactPersonRepository,
            NameTitleRepository nameTitleRepository
            )
        {
            _orgAttachmentRepository = orgAttachmentRepository;
            _customerDocNameRepository = customerDocNameRepository;
            _trackingOrgAttachmentRepository = trackingOrgAttachmentRepository;
            _trackingOrgAddressRepository = trackingOrgAddressRepository;
            _trackingComprofileRepository = trackingComprofileRepository;
            _addressRepository = addressRepository;
            _portalFieldConfigRepository = portalFieldConfigRepository;
            _countryRepository = countryRepository;
            _trackingRequestRepository = trackingRequestRepository;
            _organizationRepository = organizationRepository;
            _messageTemplateRepository = messageTemplateRepository;
            _emailAccountRepository = emailAccountRepository;
            _businessEntityRepository = businessEntityRepository;
            _languageRepository = languageRepository;
            _userRepository = userRepository;
            _contactPersonRepository = contactPersonRepository;
            _nameTitleRepository = nameTitleRepository;
        }

        public virtual OrgAttachmentListModels GetOrgAttachmentListBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID, int companyTypeID)
        {
            OrgAttachmentListModels model = new OrgAttachmentListModels();

            bool isTracking = false;
            bool isShow = false;
            int trackingGrpID = 0;

            var listTrackingAttachment = new List<TrackingAttachment>();

            var docNameModel = _customerDocNameRepository.GetCustomerDocNameByDocumentTypeID(documentTypeID);
            model.DocumentName = docNameModel;

            var orgAttachmentModel = _orgAttachmentRepository.GetOrgAttachmentBySupplierIDAndDocumentTypeID(supplierID, documentTypeID);
            foreach (var item in orgAttachmentModel)
            {
                var trackingAttachment = new TrackingAttachment();
                var trackingAttachmentModel = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentWaitingForApprove(item.SupplierID, item.DocumentNameID ?? 0, item.AttachmentID);

                //เชคว่า AttachmentID ของ DocumentNameID ของ Supplier อยู่ในระหว่งการแก้ไขรออนุมัติหรือไม่ (**เพิ่ม AttachmentID ไป where ด้วยเพราะบางเอกสารแนบซ้ำได้หลายไฟล์)
                if (trackingAttachmentModel != null) 
                {
                    //เกิดจากการเพิ่มไฟล์ใหม่
                    if (trackingAttachmentModel.OldAttachmentID == 0)
                    {
                        isShow = true;
                        trackingAttachment.OrgAttachment = item;
                    }else
                    {
                        //เกิดจากการลบไฟล์ที่มี
                        if (trackingAttachmentModel.NewAttachmentID == 0)
                        {
                            isShow = true;
                            trackingAttachment.OrgAttachment = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(trackingAttachmentModel.OldAttachmentID ?? 0);
                        }else
                        {
                            //เกิดจากการลบแล้วแนบไฟล์ใหม่
                            if (item.AttachmentID == trackingAttachmentModel.OldAttachmentID)
                            {
                                //เกิดจากการกด Restore กลับมาใช้ไฟล์เก่าที่เคยถูกลบแล้วไม่แนบใหม่ **(OldAttachmentID == NewAttachmentID)
                                if (trackingAttachmentModel.OldAttachmentID == trackingAttachmentModel.NewAttachmentID)
                                {
                                    isShow = true;
                                }else
                                {
                                    isShow = false;
                                }
                                // รอบที่ Loop เข้ามาเป็นไฟล์ที่ถูกลบ
                                trackingAttachment.OrgAttachment = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(trackingAttachmentModel.OldAttachmentID ?? 0);
                            }else
                            {
                                // รอบที่ Loop เข้ามาเป็นไฟล์ที่เพิ่มเข้ามาแทนที่ DocumentNameID เดิม
                                isShow = true;
                                trackingAttachment.OrgAttachment = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(trackingAttachmentModel.NewAttachmentID ?? 0);
                            }
                            
                        }
                    }

                    var trackingRequestModel = _trackingRequestRepository.GetTrackingRequestByTrackingReqID(trackingAttachmentModel.TrackingReqID);
                    if (trackingRequestModel != null)
                    {
                        trackingGrpID = trackingRequestModel.TrackingGrpID ?? 0;
                    }else
                    {
                        trackingGrpID = 0;
                    }

                    isTracking = true;
                    trackingAttachment.TrackingReqID = trackingAttachmentModel.TrackingReqID;
                    trackingAttachment.TrackingItemID = trackingAttachmentModel.TrackingItemID;
                    trackingAttachment.TrackingOrgAttachment = trackingAttachmentModel;

                }else
                {
                    //ไม่มีการแก้ใขใน DocumentNameID นั้นๆ
                    isShow = true;
                    isTracking = false;
                    trackingGrpID = 0;
                    trackingAttachment.OrgAttachment = item;
                    trackingAttachment.TrackingOrgAttachment = null;

                   
                }
                trackingAttachment.TrackingGrpID = trackingGrpID;
                trackingAttachment.isShow = isShow;
                trackingAttachment.isTracking = isTracking;

                listTrackingAttachment.Add(trackingAttachment);
            }

            model.TrackingAttachment = listTrackingAttachment;
            model.CompanyTypeID = companyTypeID;
            return model;
        }

        public virtual OrgAttachmentListModels GetOrgAttachmentListBySupplierIDAndDocumentTypeIDOther(int supplierID, int documentNameId, int companyTypeID)
        {
            OrgAttachmentListModels model = new OrgAttachmentListModels();

            bool isTracking = false;
            bool isShow = false;
            int trackingGrpID = 0;

            var listTrackingAttachment = new List<TrackingAttachment>();
            var orgAttachmentModel = _orgAttachmentRepository.GetOrgAttachmentBySupplierIDAndDocumentNameIDAndDocumentNameID(supplierID, 9, 10);

            foreach (var item in orgAttachmentModel)
            {
                var trackingAttachment = new TrackingAttachment();
                var trackingAttachmentModel = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentWaitingForApprove(item.SupplierID, item.DocumentNameID ?? 0, item.AttachmentID);

                //เชคว่า AttachmentID ของ DocumentNameID ของ Supplier อยู่ในระหว่งการแก้ไขรออนุมัติหรือไม่ (**เพิ่ม AttachmentID ไป where ด้วยเพราะบางเอกสารแนบซ้ำได้หลายไฟล์)
                if (trackingAttachmentModel != null)
                {
                    //เกิดจากการเพิ่มไฟล์ใหม่
                    if (trackingAttachmentModel.OldAttachmentID == 0)
                    {
                        isShow = true;
                        trackingAttachment.OrgAttachment = item;
                    }
                    else
                    {
                        //เกิดจากการลบไฟล์ที่มี
                        if (trackingAttachmentModel.NewAttachmentID == 0)
                        {
                            isShow = true;
                            trackingAttachment.OrgAttachment = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(trackingAttachmentModel.OldAttachmentID ?? 0);
                        }
                        else
                        {
                            //เกิดจากการลบแล้วแนบไฟล์ใหม่
                            if (item.AttachmentID == trackingAttachmentModel.OldAttachmentID)
                            {
                                //เกิดจากการกด Restore กลับมาใช้ไฟล์เก่าที่เคยถูกลบแล้วไม่แนบใหม่ **(OldAttachmentID == NewAttachmentID)
                                if (trackingAttachmentModel.OldAttachmentID == trackingAttachmentModel.NewAttachmentID)
                                {
                                    isShow = true;
                                }
                                else
                                {
                                    isShow = false;
                                }
                                // รอบที่ Loop เข้ามาเป็นไฟล์ที่ถูกลบ
                                trackingAttachment.OrgAttachment = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(trackingAttachmentModel.OldAttachmentID ?? 0);
                            }
                            else
                            {
                                // รอบที่ Loop เข้ามาเป็นไฟล์ที่เพิ่มเข้ามาแทนที่ DocumentNameID เดิม
                                isShow = true;
                                trackingAttachment.OrgAttachment = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(trackingAttachmentModel.NewAttachmentID ?? 0);
                            }

                        }
                    }

                    var trackingRequestModel = _trackingRequestRepository.GetTrackingRequestByTrackingReqID(trackingAttachmentModel.TrackingReqID);
                    if (trackingRequestModel != null)
                    {
                        trackingGrpID = trackingRequestModel.TrackingGrpID ?? 0;
                    }
                    else
                    {
                        trackingGrpID = 0;
                    }

                    isTracking = true;
                    trackingAttachment.TrackingReqID = trackingAttachmentModel.TrackingReqID;
                    trackingAttachment.TrackingItemID = trackingAttachmentModel.TrackingItemID;
                    trackingAttachment.TrackingOrgAttachment = trackingAttachmentModel;

                }
                else
                {
                    //ไม่มีการแก้ใขใน DocumentNameID นั้นๆ
                    isShow = true;
                    isTracking = false;
                    trackingGrpID = 0;
                    trackingAttachment.OrgAttachment = item;
                    trackingAttachment.TrackingOrgAttachment = null;


                }
                trackingAttachment.TrackingGrpID = trackingGrpID;
                trackingAttachment.isShow = isShow;
                trackingAttachment.isTracking = isTracking;

                listTrackingAttachment.Add(trackingAttachment);
            }

            model.TrackingAttachment = listTrackingAttachment;
            model.CompanyTypeID = companyTypeID;
            return model;
        }


        public virtual ViewDocumentTrackingModels GetDocumentTrackingByTrackingItemID(int trackingItemID)
        {

            ViewDocumentTrackingModels model = new ViewDocumentTrackingModels();
            OrgAttachmentModel oldOldOrgAttachmentModel = new OrgAttachmentModel();
            OrgAttachmentModel newOldOrgAttachmentModel = new OrgAttachmentModel();

            var trackingOrgAttachmentModel = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentByTrackingItemID(trackingItemID);

            if (trackingOrgAttachmentModel != null)
            {
                
                if (trackingOrgAttachmentModel.OldAttachmentID != 0)
                {
                    
                    var orgAttachment = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(trackingOrgAttachmentModel.OldAttachmentID??0);

                    if (orgAttachment != null)
                    {
                        string documentName = _customerDocNameRepository.GetCustomerDocNameByDocumentNameID(orgAttachment.DocumentNameID??0);

                        oldOldOrgAttachmentModel.SupplierID = orgAttachment.SupplierID;
                        oldOldOrgAttachmentModel.AttachmentID = orgAttachment.AttachmentID;
                        oldOldOrgAttachmentModel.AttachmentName = orgAttachment.AttachmentName;
                        oldOldOrgAttachmentModel.AttachmentNameUnique = orgAttachment.AttachmentNameUnique;
                        oldOldOrgAttachmentModel.SizeAttach = orgAttachment.SizeAttach;
                        oldOldOrgAttachmentModel.DocumentNameID = orgAttachment.DocumentNameID;
                        oldOldOrgAttachmentModel.DocumentNameID = orgAttachment.DocumentNameID;
                        oldOldOrgAttachmentModel.DocumentName = documentName;
                        oldOldOrgAttachmentModel.OtherDocument = orgAttachment.OtherDocument;

                    }
                }else
                {
                   
                    oldOldOrgAttachmentModel.AttachmentID = 0;
                    oldOldOrgAttachmentModel.AttachmentName = "";
                }

                if (trackingOrgAttachmentModel.NewAttachmentID != 0)
                {
                    
                    var orgAttachment = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(trackingOrgAttachmentModel.NewAttachmentID ?? 0);

                    if (orgAttachment != null)
                    {
                        string documentName = _customerDocNameRepository.GetCustomerDocNameByDocumentNameID(orgAttachment.DocumentNameID ?? 0);

                        newOldOrgAttachmentModel.SupplierID = orgAttachment.SupplierID;
                        newOldOrgAttachmentModel.AttachmentID = orgAttachment.AttachmentID;
                        newOldOrgAttachmentModel.AttachmentName = orgAttachment.AttachmentName;
                        newOldOrgAttachmentModel.AttachmentNameUnique = orgAttachment.AttachmentNameUnique;
                        newOldOrgAttachmentModel.SizeAttach = orgAttachment.SizeAttach;
                        newOldOrgAttachmentModel.DocumentNameID = orgAttachment.DocumentNameID;
                        newOldOrgAttachmentModel.DocumentNameID = orgAttachment.DocumentNameID;
                        newOldOrgAttachmentModel.DocumentName = documentName;
                        newOldOrgAttachmentModel.OtherDocument = orgAttachment.OtherDocument;
                    }
                }else
                {
                    
                    newOldOrgAttachmentModel.AttachmentID = 0;
                    newOldOrgAttachmentModel.AttachmentName = "";
                }

                model.OldOrgAttachment = oldOldOrgAttachmentModel;
                model.NewOrgAttachment = newOldOrgAttachmentModel;
            }

            return model;
        }

        public virtual IEnumerable<ViewTrackingContactAddressModel> GetOrgAddressTrackingByTrackingReqID(int trackingReqID)
        {

            var trackingContactAddressList = _trackingOrgAddressRepository.GetTrackingContactAddressByTrackingReqID(trackingReqID);

            foreach (var item in trackingContactAddressList)
            {
                
                if (item.OldAddressID == item.NewAddressID)
                {
                    var trackingField = _trackingComprofileRepository.GetTrackingFieldCompanyProfileByTrackingReqID(item.TrackingReqID);
                    foreach (var itemTracking in trackingField)
                    {
                        if (itemTracking.isForeignKey == 1)
                        {
                            if (itemTracking.ForeignKeyGroup == "Tbl_Country")
                            {

                                string oldCountryName = _countryRepository.GetCountryNameByCountryCode(itemTracking.OldKeyValue);
                                string newCountryName = _countryRepository.GetCountryNameByCountryCode(itemTracking.NewKeyValue);

                                itemTracking.OldKeyValue = oldCountryName;
                                itemTracking.NewKeyValue = newCountryName;

                            }
                        }
                    }
                    item.AddressTrackingField = trackingField.ToList();
                }else
                {
                    var oldAddress = _addressRepository.GetAddressByAddressID(item.OldAddressID);
                    var newAddress = _addressRepository.GetAddressByAddressID(item.NewAddressID);
                    var fieldAddressTracking = _portalFieldConfigRepository.GetPortalFieldConfigIsTrackingByFieldGroup("Address");

                    if(oldAddress != null && newAddress!= null)
                    {

                        var listtrackingField = new List<TrackingFieldCompanyProfileModel>();


                         #region----------------------------------Add Value Address Tracking-----------------------------------------

                         var trackingFieldHouseNo_Local = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.HouseNo_Local").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.HouseNo_Local,
                             NewKeyValue = newAddress.HouseNo_Local
                         };
                         listtrackingField.Add(trackingFieldHouseNo_Local);

                         var trackingFieldHouseNo_Inter = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.HouseNo_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.HouseNo_Inter,
                             NewKeyValue = newAddress.HouseNo_Inter
                         };
                         listtrackingField.Add(trackingFieldHouseNo_Inter);

                         var trackingFieldVillageNo_Local = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.VillageNo_Local").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.VillageNo_Local,
                             NewKeyValue = newAddress.VillageNo_Local
                         };
                         listtrackingField.Add(trackingFieldVillageNo_Local);

                         var trackingFieldVillageNo_Inter = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.VillageNo_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.VillageNo_Inter,
                             NewKeyValue = newAddress.VillageNo_Inter
                         };
                         listtrackingField.Add(trackingFieldVillageNo_Inter);

                         var trackingFieldLane_Local = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.Lane_Local").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.Lane_Local,
                             NewKeyValue = newAddress.Lane_Local
                         };
                         listtrackingField.Add(trackingFieldLane_Local);

                         var trackingFieldLane_Inter = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.Lane_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.Lane_Inter,
                             NewKeyValue = newAddress.Lane_Inter
                         };
                         listtrackingField.Add(trackingFieldLane_Inter);

                         var trackingFieldRoad_Local = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.Street_Local").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.Road_Local,
                             NewKeyValue = newAddress.Road_Local
                         };
                         listtrackingField.Add(trackingFieldRoad_Local);

                         var trackingFieldRoad_Inter = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.Street_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.Road_Inter,
                             NewKeyValue = newAddress.Road_Inter
                         };
                         listtrackingField.Add(trackingFieldRoad_Inter);

                         var trackingFieldSubDistrict_Local = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.SubDistrict_Local").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.SubDistrict_Local,
                             NewKeyValue = newAddress.SubDistrict_Local
                         };
                         listtrackingField.Add(trackingFieldSubDistrict_Local);

                         var trackingFieldSubDistrict_Inter = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.SubDistrict_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.SubDistrict_Inter,
                             NewKeyValue = newAddress.SubDistrict_Inter
                         };
                         listtrackingField.Add(trackingFieldSubDistrict_Inter);

                         var trackingFieldCity_Local = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.City_Local").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.City_Local,
                             NewKeyValue = newAddress.City_Local
                         };
                         listtrackingField.Add(trackingFieldCity_Local);

                         var trackingFieldCity_Inter = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.City_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.City_Inter,
                             NewKeyValue = newAddress.City_Inter
                         };
                         listtrackingField.Add(trackingFieldCity_Inter);

                         var trackingFieldState_Local = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.State_Local").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.State_Local,
                             NewKeyValue = newAddress.State_Local
                         };
                         listtrackingField.Add(trackingFieldState_Local);

                         var trackingFieldState_Inter = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.State_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.State_Inter,
                             NewKeyValue = newAddress.State_Inter
                         };
                         listtrackingField.Add(trackingFieldState_Inter);

                         var trackingFieldPostalCode = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.PostalCode").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldAddress.PostalCode,
                             NewKeyValue = newAddress.PostalCode
                         };
                         listtrackingField.Add(trackingFieldPostalCode);

                         string oldCountryName = _countryRepository.GetCountryNameByCountryCode(oldAddress.CountryCode);
                         string newCountryName = _countryRepository.GetCountryNameByCountryCode(newAddress.CountryCode);
                         var trackingFieldCountryName = new TrackingFieldCompanyProfileModel
                         {
                             TrackingReqID = item.TrackingReqID,
                             FieldID = 0,
                             TrackingStatusID = item.TrackingStatusID,
                             TrackingStatusName = item.TrackingStatusName,
                             ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.Country").Select(m => m.ResourceName).FirstOrDefault(),
                             OldKeyValue = oldCountryName,
                             NewKeyValue = newCountryName
                         };
                         listtrackingField.Add(trackingFieldCountryName);

                         item.AddressTrackingField = listtrackingField;

                         #endregion

                    }                                       
                }
            }

            return trackingContactAddressList;
        }

        public virtual IEnumerable<ViewTrackingContactAddressModel> GetOrgAddressTrackingViewByTrackingReqID(int trackingReqID)
        {
            string oldAddress = "";
            string newAddress = "";

            var trackingContactAddressList = _trackingOrgAddressRepository.GetTrackingContactAddressByTrackingReqID(trackingReqID);
            foreach (var item in trackingContactAddressList)
            {
                if (item.OldAddressID == item.NewAddressID)
                {
                    var trackingField = _trackingComprofileRepository.GetTrackingFieldCompanyProfileByTrackingReqID(item.TrackingReqID);

                    #region Compound Address Field

                    #region Address Local

                    #region HouseNo_Local
                    var houseNo_Local = trackingField.Where(m => m.TrackingKey == "HouseNo_Local").FirstOrDefault();
                    if (houseNo_Local != null)
                    {
                        oldAddress += houseNo_Local.OldKeyValue + " ";
                        if (houseNo_Local.OldKeyValue == houseNo_Local.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += houseNo_Local.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(houseNo_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(houseNo_Local.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + houseNo_Local.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + houseNo_Local.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region VillageNo_Local
                    var villageNo_Local = trackingField.Where(m => m.TrackingKey == "VillageNo_Local").FirstOrDefault();
                    if (villageNo_Local != null)
                    {
                        oldAddress += villageNo_Local.OldKeyValue + " ";
                        if (villageNo_Local.OldKeyValue == villageNo_Local.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += villageNo_Local.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(villageNo_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(villageNo_Local.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + villageNo_Local.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + villageNo_Local.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region Lane_Local
                    var lane_Local = trackingField.Where(m => m.TrackingKey == "Lane_Local").FirstOrDefault();
                    if (lane_Local != null)
                    {
                        oldAddress += lane_Local.OldKeyValue + " ";
                        if (lane_Local.OldKeyValue == lane_Local.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += lane_Local.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(lane_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(lane_Local.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + lane_Local.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + lane_Local.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region Road_Local
                    var road_Local = trackingField.Where(m => m.TrackingKey == "Road_Local").FirstOrDefault();
                    if (road_Local != null)
                    {
                        oldAddress += road_Local.OldKeyValue + " ";
                        if (road_Local.OldKeyValue == road_Local.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += road_Local.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(road_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(road_Local.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + road_Local.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + road_Local.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region SubDistrict_Local
                    var subDistrict_Local = trackingField.Where(m => m.TrackingKey == "SubDistrict_Local").FirstOrDefault();
                    if (subDistrict_Local != null)
                    {
                        oldAddress += subDistrict_Local.OldKeyValue + " ";
                        if (subDistrict_Local.OldKeyValue == subDistrict_Local.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += subDistrict_Local.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(subDistrict_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(subDistrict_Local.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + subDistrict_Local.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + subDistrict_Local.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region City_Local
                    var city_Local = trackingField.Where(m => m.TrackingKey == "City_Local").FirstOrDefault();
                    if (city_Local != null)
                    {
                        oldAddress += city_Local.OldKeyValue + " ";
                        if (city_Local.OldKeyValue == city_Local.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += city_Local.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(city_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(city_Local.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + city_Local.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + city_Local.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region State_Local
                    var state_Local = trackingField.Where(m => m.TrackingKey == "State_Local").FirstOrDefault();
                    if (state_Local != null)
                    {
                        oldAddress += state_Local.OldKeyValue + " ";
                        if (state_Local.OldKeyValue == state_Local.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += state_Local.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(state_Local.OldKeyValue.Trim()) && string.IsNullOrEmpty(state_Local.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + state_Local.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + state_Local.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region PostalCode
                    var postalCode = trackingField.Where(m => m.TrackingKey == "PostalCode").FirstOrDefault();
                    if (postalCode != null)
                    {
                        oldAddress += postalCode.OldKeyValue + " ";
                        if (postalCode.OldKeyValue == postalCode.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += postalCode.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(postalCode.OldKeyValue.Trim()) && string.IsNullOrEmpty(postalCode.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + postalCode.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + postalCode.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region CountryCode
                    var countryCode = trackingField.Where(m => m.TrackingKey == "CountryCode").FirstOrDefault();
                    if (countryCode != null)
                    {
                        string oldCountryName = _countryRepository.GetCountryNameByCountryCode(countryCode.OldKeyValue);
                        string newCountryName = _countryRepository.GetCountryNameByCountryCode(countryCode.NewKeyValue);

                        oldAddress += oldCountryName;
                        if (countryCode.OldKeyValue == countryCode.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += newCountryName + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(countryCode.OldKeyValue.Trim()) && string.IsNullOrEmpty(countryCode.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + oldCountryName + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + newCountryName + "|";
                            }

                        }
                    }
                    #endregion
                    
                    #endregion

                    //Add value to Address_Local
                    item.OldAddress_Local = oldAddress;
                    item.NewAddress_Local = newAddress;

                    //Reset string value
                    oldAddress = "";
                    newAddress = "";

                    #region Address Inter

                    #region HouseNo_Inter
                    var houseNo_Inter = trackingField.Where(m => m.TrackingKey == "HouseNo_Inter").FirstOrDefault();
                    if (houseNo_Inter != null)
                    {
                        oldAddress += houseNo_Inter.OldKeyValue + " ";
                        if (houseNo_Inter.OldKeyValue == houseNo_Inter.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += houseNo_Inter.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(houseNo_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(houseNo_Inter.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + houseNo_Inter.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + houseNo_Inter.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region VillageNo_Inter
                    var villageNo_Inter = trackingField.Where(m => m.TrackingKey == "VillageNo_Inter").FirstOrDefault();
                    if (villageNo_Inter != null)
                    {
                        oldAddress += villageNo_Inter.OldKeyValue + " ";
                        if (villageNo_Inter.OldKeyValue == villageNo_Inter.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += villageNo_Inter.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(villageNo_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(villageNo_Inter.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + villageNo_Inter.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + villageNo_Inter.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region Lane_Inter
                    var lane_Inter = trackingField.Where(m => m.TrackingKey == "Lane_Inter").FirstOrDefault();
                    if (lane_Inter != null)
                    {
                        oldAddress += lane_Inter.OldKeyValue + " ";
                        if (lane_Inter.OldKeyValue == lane_Inter.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += lane_Inter.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(lane_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(lane_Inter.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + lane_Inter.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + lane_Inter.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region Road_Inter
                    var road_Inter = trackingField.Where(m => m.TrackingKey == "Road_Inter").FirstOrDefault();
                    if (road_Inter != null)
                    {
                        oldAddress += road_Inter.OldKeyValue + " ";
                        if (road_Inter.OldKeyValue == road_Inter.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += road_Inter.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(road_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(road_Inter.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + road_Inter.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + road_Inter.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region SubDistrict_Inter
                    var subDistrict_Inter = trackingField.Where(m => m.TrackingKey == "SubDistrict_Inter").FirstOrDefault();
                    if (subDistrict_Inter != null)
                    {
                        oldAddress += subDistrict_Inter.OldKeyValue + " ";
                        if (subDistrict_Inter.OldKeyValue == subDistrict_Inter.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += subDistrict_Inter.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(subDistrict_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(subDistrict_Inter.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + subDistrict_Inter.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + subDistrict_Inter.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region City_Inter
                    var city_Inter = trackingField.Where(m => m.TrackingKey == "City_Inter").FirstOrDefault();
                    if (city_Inter != null)
                    {
                        oldAddress += city_Inter.OldKeyValue + " ";
                        if (city_Inter.OldKeyValue == city_Inter.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += city_Inter.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(city_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(city_Inter.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + city_Inter.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + city_Inter.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region State_Inter
                    var state_Inter = trackingField.Where(m => m.TrackingKey == "State_Inter").FirstOrDefault();
                    if (state_Inter != null)
                    {
                        oldAddress += state_Inter.OldKeyValue + " ";
                        if (state_Inter.OldKeyValue == state_Inter.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += state_Inter.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(state_Inter.OldKeyValue.Trim()) && string.IsNullOrEmpty(state_Inter.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + state_Inter.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + state_Inter.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region PostalCode
                    //var postalCode = trackingField.Where(m => m.TrackingKey == "PostalCode").FirstOrDefault();
                    if (postalCode != null)
                    {
                        oldAddress += postalCode.OldKeyValue + " ";
                        if (postalCode.OldKeyValue == postalCode.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += postalCode.NewKeyValue + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(postalCode.OldKeyValue.Trim()) && string.IsNullOrEmpty(postalCode.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + postalCode.OldKeyValue + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + postalCode.NewKeyValue + "|";
                            }

                        }
                    }
                    #endregion

                    #region CountryCode
                    //var countryCode = trackingField.Where(m => m.TrackingKey == "CountryCode").FirstOrDefault();
                    if (countryCode != null)
                    {
                        string oldCountryName = _countryRepository.GetCountryNameByCountryCode(countryCode.OldKeyValue);
                        string newCountryName = _countryRepository.GetCountryNameByCountryCode(countryCode.NewKeyValue);

                        oldAddress += oldCountryName;
                        if (countryCode.OldKeyValue == countryCode.NewKeyValue)
                        {
                            //ถ้าค่าเท่ากันแสดงว่าไม่มีการเปลี่ยนแปลง
                            newAddress += newCountryName + "|";
                        }
                        else
                        {
                            //แสดงว่ามีการเปลี่ยนแปลงค่า
                            if (!string.IsNullOrEmpty(countryCode.OldKeyValue.Trim()) && string.IsNullOrEmpty(countryCode.NewKeyValue.Trim()))
                            {
                                //กรณีที่ค่าเก่ามีค่า แล้วค่าใหม่ไม่มีค่า จะแสดงเป็น ขีดกลางคั่น(ใช้สัญลักษ์ & นำหน้า)
                                newAddress += "&" + oldCountryName + "|";
                            }
                            else
                            {
                                //กรณีที่มีการเปลี่ยนแปลงค่า จะแสดงเป็น ตัวหนังสือสีแดง(ใช้สัญลักษ์ # นำหน้า)
                                newAddress += "#" + newCountryName + "|";
                            }

                        }
                    }
                    #endregion
                    #endregion

                    //Add value to Address_Inter
                    item.OldAddress_Inter = oldAddress;
                    item.NewAddress_Inter = newAddress;

                    #endregion

                }else
                {
                    var oldAddressModel = _addressRepository.GetAddressByAddressID(item.OldAddressID);
                    var newAddressModel = _addressRepository.GetAddressByAddressID(item.NewAddressID);

                    string oldCountryName = _countryRepository.GetCountryNameByCountryCode(oldAddressModel.CountryCode);
                    string newCountryName = _countryRepository.GetCountryNameByCountryCode(newAddressModel.CountryCode);

                    if (oldAddressModel != null && newAddressModel != null)
                    {
                        #region OldAddress

                        item.OldAddress_Local += oldAddressModel.HouseNo_Local + " " + oldAddressModel.VillageNo_Local + " " + oldAddressModel.Lane_Local + " ";
                        item.OldAddress_Local += oldAddressModel.Road_Local + " " + oldAddressModel.SubDistrict_Local + " " + oldAddressModel.City_Local + " ";
                        item.OldAddress_Local += oldAddressModel.State_Local + " " + oldAddressModel.PostalCode + " " + oldCountryName;

                        item.OldAddress_Inter += oldAddressModel.HouseNo_Inter + " " + oldAddressModel.VillageNo_Inter + " " + oldAddressModel.Lane_Inter + " ";
                        item.OldAddress_Inter += oldAddressModel.Road_Inter + " " + oldAddressModel.SubDistrict_Inter + " " + oldAddressModel.City_Inter + " ";
                        item.OldAddress_Inter += oldAddressModel.State_Inter + " " + oldAddressModel.PostalCode + " " + oldCountryName;

                        #endregion

                        #region NewAddress

                        item.NewAddress_Local += newAddressModel.HouseNo_Local + " " + newAddressModel.VillageNo_Local + " " + newAddressModel.Lane_Local + " ";
                        item.NewAddress_Local += newAddressModel.Road_Local + " " + newAddressModel.SubDistrict_Local + " " + newAddressModel.City_Local + " ";
                        item.NewAddress_Local += newAddressModel.State_Local + " " + newAddressModel.PostalCode + " " + newCountryName;

                        item.NewAddress_Inter += newAddressModel.HouseNo_Inter + " " + newAddressModel.VillageNo_Inter + " " + newAddressModel.Lane_Inter + " ";
                        item.NewAddress_Inter += newAddressModel.Road_Inter + " " + newAddressModel.SubDistrict_Inter + " " + newAddressModel.City_Inter + " ";
                        item.NewAddress_Inter += newAddressModel.State_Inter + " " + newAddressModel.PostalCode + " " + newCountryName;
                       
                        #endregion                      
 
                    }
                }
            }

            return trackingContactAddressList;
        }

        public virtual IEnumerable<ViewTrackingContactAddressModel> GetOrgAddressTrackingViewDataByTrackingReqID(int trackingReqID)
        {
            string oldAddress = "";
            //string newAddress = "";

            //string userpassword = CryptographyManage.UserPasswordEncode("username", "password");
            
            int supplierID = 0;
            string orgCountryCode = "";
            var trackingReqModel = _trackingRequestRepository.GetTrackingRequestByTrackingReqID(trackingReqID);
            if (trackingReqModel != null)
            {
                supplierID = trackingReqModel.SupplierID??0;
            }
            var orgModel = _organizationRepository.GetDataBySupplierID(supplierID);
            if (orgModel != null)
            {
                orgCountryCode = orgModel.CountryCode;
            }


            var trackingContactAddressList = _trackingOrgAddressRepository.GetTrackingContactAddressByTrackingReqID(trackingReqID);
            foreach (var item in trackingContactAddressList)
            {
                if (item.OldAddressID == item.NewAddressID)
                {
                    var trackingField = _trackingComprofileRepository.GetTrackingFieldCompanyProfileByTrackingReqID(item.TrackingReqID);

                    #region Address Local
                    var oldAddressLocal = new AddressModel();
                    var newAddressLocal = new AddressModel();

                    #region HouseNo_Local
                    var houseNo_Local = trackingField.Where(m => m.TrackingKey == "HouseNo_Local").FirstOrDefault();
                    if (houseNo_Local != null)
                    {
                        oldAddressLocal.HouseNo = houseNo_Local.OldKeyValue ?? "";
                        newAddressLocal.HouseNo = houseNo_Local.NewKeyValue ?? "";
                    }
                    #endregion

                    #region VillageNo_Local
                    var villageNo_Local = trackingField.Where(m => m.TrackingKey == "VillageNo_Local").FirstOrDefault();
                    if (villageNo_Local != null)
                    {
                        oldAddressLocal.VillageNo = villageNo_Local.OldKeyValue ?? "";
                        newAddressLocal.VillageNo = villageNo_Local.NewKeyValue ?? "";
                    }
                    #endregion

                    #region Lane_Local
                    var lane_Local = trackingField.Where(m => m.TrackingKey == "Lane_Local").FirstOrDefault();
                    if (lane_Local != null)
                    {
                        oldAddressLocal.Lane = lane_Local.OldKeyValue ?? "";
                        newAddressLocal.Lane = lane_Local.NewKeyValue ?? "";
                    }
                    #endregion

                    #region Road_Local
                    var road_Local = trackingField.Where(m => m.TrackingKey == "Road_Local").FirstOrDefault();
                    if (road_Local != null)
                    {
                        oldAddressLocal.Road = road_Local.OldKeyValue ?? "";
                        newAddressLocal.Road = road_Local.NewKeyValue ?? "";
                    }
                    #endregion

                    #region SubDistrict_Local
                    var subDistrict_Local = trackingField.Where(m => m.TrackingKey == "SubDistrict_Local").FirstOrDefault();
                    if (subDistrict_Local != null)
                    {
                        oldAddressLocal.SubDistrict = subDistrict_Local.OldKeyValue ?? "";
                        newAddressLocal.SubDistrict = subDistrict_Local.NewKeyValue ?? "";
                    }
                    #endregion

                    #region City_Local
                    var city_Local = trackingField.Where(m => m.TrackingKey == "City_Local").FirstOrDefault();
                    if (city_Local != null)
                    {
                        oldAddressLocal.City = city_Local.OldKeyValue ?? "";
                        newAddressLocal.City = city_Local.NewKeyValue ?? "";
                    }
                    #endregion

                    #region State_Local
                    var state_Local = trackingField.Where(m => m.TrackingKey == "State_Local").FirstOrDefault();
                    if (state_Local != null)
                    {
                        oldAddressLocal.State = state_Local.OldKeyValue ?? "";
                        newAddressLocal.State = state_Local.NewKeyValue ?? "";
                    }
                    #endregion

                    #region PostalCode
                    var postalCode = trackingField.Where(m => m.TrackingKey == "PostalCode").FirstOrDefault();
                    if (postalCode != null)
                    {
                        oldAddressLocal.PostalCode = postalCode.OldKeyValue ?? "";
                        newAddressLocal.PostalCode = postalCode.NewKeyValue ?? "";
                    }
                    #endregion

                    #region CountryCode

                    var countryCode = trackingField.Where(m => m.TrackingKey == "CountryCode").FirstOrDefault();

                    var orgCountry = _countryRepository.GetCountryByCountryCode(orgCountryCode);
                   
                    if (countryCode != null)
                    {
                        string oldCountryName = _countryRepository.GetCountryNameByCountryCode(countryCode.OldKeyValue);
                        string newCountryName = _countryRepository.GetCountryNameByCountryCode(countryCode.NewKeyValue);

                        if (orgCountry != null)
                        {
                            var oldCountryLocal = _countryRepository.GetCountryByCountryCode(countryCode.OldKeyValue);
                            if (oldCountryLocal != null)
                            {
                                oldCountryName = GetLocalizedProperty.GetLocalizedValue(orgCountry.LocalLanguageID.ToString(), "Tbl_Country", "CountryName", oldCountryLocal.CountryID.ToString());
                            }
                            var newCountryLocal = _countryRepository.GetCountryByCountryCode(countryCode.NewKeyValue);
                            if (newCountryLocal != null)
                            {
                                newCountryName = GetLocalizedProperty.GetLocalizedValue(orgCountry.LocalLanguageID.ToString(), "Tbl_Country", "CountryName", newCountryLocal.CountryID.ToString());
                            }
                        }


                        oldAddressLocal.CountryName = oldCountryName;
                        newAddressLocal.CountryName = newCountryName;
                    }
                    #endregion

                    //Add value to Address_Local
                    oldAddressLocal = AddressManagementString.GetPrefix(oldAddressLocal);
                    newAddressLocal = AddressManagementString.GetPrefix(newAddressLocal);
                    item.NewAddress_Local = AddressManagementString.GetGenAddress(oldAddressLocal, newAddressLocal, out oldAddress);
                    item.OldAddress_Local = oldAddress;
                    //item.NewAddress_Local = CompoundStringManage.CompoundAddressTracking(oldAddressLocal, newAddressLocal, out oldAddress);
                    //item.OldAddress_Local = oldAddress;

                    //item.NewAddress_Local = AddressManagementString.GetGenAddress(oldAddressLocal, newAddressLocal, out oldAddress);
                    //item.OldAddress_Local = oldAddress;

                    #endregion

                    #region Address Inter
                    var oldAddressInter = new AddressModel();
                    var newAddressInter = new AddressModel();

                    #region HouseNo_Inter
                    var houseNo_Inter = trackingField.Where(m => m.TrackingKey == "HouseNo_Inter").FirstOrDefault();
                    if (houseNo_Inter != null)
                    {
                        oldAddressInter.HouseNo = houseNo_Inter.OldKeyValue ?? "";
                        newAddressInter.HouseNo = houseNo_Inter.NewKeyValue ?? "";
                    }
                    #endregion

                    #region VillageNo_Inter
                    var villageNo_Inter = trackingField.Where(m => m.TrackingKey == "VillageNo_Inter").FirstOrDefault();
                    if (villageNo_Inter != null)
                    {
                        oldAddressInter.VillageNo = villageNo_Inter.OldKeyValue ?? "";
                        newAddressInter.VillageNo = villageNo_Inter.NewKeyValue ?? "";
                    }
                    #endregion

                    #region Lane_Inter
                    var lane_Inter = trackingField.Where(m => m.TrackingKey == "Lane_Inter").FirstOrDefault();
                    if (lane_Inter != null)
                    {
                        oldAddressInter.Lane = lane_Inter.OldKeyValue ?? "";
                        newAddressInter.Lane = lane_Inter.NewKeyValue ?? "";
                    }
                    #endregion

                    #region Road_Inter
                    var road_Inter = trackingField.Where(m => m.TrackingKey == "Road_Inter").FirstOrDefault();
                    if (road_Inter != null)
                    {
                        oldAddressInter.Road = road_Inter.OldKeyValue ?? "";
                        newAddressInter.Road = road_Inter.NewKeyValue ?? "";
                    }
                    #endregion

                    #region SubDistrict_Inter
                    var subDistrict_Inter = trackingField.Where(m => m.TrackingKey == "SubDistrict_Inter").FirstOrDefault();
                    if (subDistrict_Inter != null)
                    {
                        oldAddressInter.SubDistrict = subDistrict_Inter.OldKeyValue ?? "";
                        newAddressInter.SubDistrict = subDistrict_Inter.NewKeyValue ?? "";
                    }
                    #endregion

                    #region City_Inter
                    var city_Inter = trackingField.Where(m => m.TrackingKey == "City_Inter").FirstOrDefault();
                    if (city_Inter != null)
                    {
                        oldAddressInter.City = city_Inter.OldKeyValue ?? "";
                        newAddressInter.City = city_Inter.NewKeyValue ?? "";
                    }
                    #endregion

                    #region State_Inter
                    var state_Inter = trackingField.Where(m => m.TrackingKey == "State_Inter").FirstOrDefault();
                    if (state_Inter != null)
                    {
                        oldAddressInter.State = state_Inter.OldKeyValue ?? "";
                        newAddressInter.State = state_Inter.NewKeyValue ?? "";
                    }
                    #endregion

                    #region PostalCode
                    //var postalCode = trackingField.Where(m => m.TrackingKey == "PostalCode").FirstOrDefault();
                    if (postalCode != null)
                    {
                        oldAddressInter.PostalCode = postalCode.OldKeyValue ?? "";
                        newAddressInter.PostalCode = postalCode.NewKeyValue ?? "";
                    }
                    #endregion

                    #region CountryCode
                    
                    if (countryCode != null)
                    {
                        string oldCountryName = _countryRepository.GetCountryNameByCountryCode(countryCode.OldKeyValue);
                        string newCountryName = _countryRepository.GetCountryNameByCountryCode(countryCode.NewKeyValue);

                        if (orgCountry != null)
                        {
                            var oldCountryInter = _countryRepository.GetCountryByCountryCode(countryCode.OldKeyValue);
                            if (oldCountryInter != null)
                            {
                                oldCountryName = GetLocalizedProperty.GetLocalizedValue(orgCountry.InterLanguageID.ToString(), "Tbl_Country", "CountryName", oldCountryInter.CountryID.ToString());
                            }
                            var newCountryInter = _countryRepository.GetCountryByCountryCode(countryCode.NewKeyValue);
                            if (newCountryInter != null)
                            {
                                newCountryName = GetLocalizedProperty.GetLocalizedValue(orgCountry.InterLanguageID.ToString(), "Tbl_Country", "CountryName", newCountryInter.CountryID.ToString());
                            }
                        }

                        oldAddressInter.CountryName = oldCountryName;
                        newAddressInter.CountryName = newCountryName;
                    }
                    #endregion

                    //Add value to Address_Inter
                    item.NewAddress_Inter = AddressManagementString.GetGenAddress(oldAddressInter, newAddressInter, out oldAddress);
                    item.OldAddress_Inter = oldAddress;
                    //item.NewAddress_Inter = CompoundStringManage.CompoundAddressTracking(oldAddressInter, newAddressInter, out oldAddress);
                    //item.OldAddress_Inter = oldAddress;

                    #endregion


                }
                else
                {
                    var oldAddressModel = _addressRepository.GetAddressByAddressID(item.OldAddressID);
                    var newAddressModel = _addressRepository.GetAddressByAddressID(item.NewAddressID);

                    string oldCountryName = _countryRepository.GetCountryNameByCountryCode(oldAddressModel.CountryCode);
                    string newCountryName = _countryRepository.GetCountryNameByCountryCode(newAddressModel.CountryCode);

                    var orgCountry = _countryRepository.GetCountryByCountryCode(orgCountryCode);

                    

                    if (oldAddressModel != null && newAddressModel != null)
                    {
                        

                        #region Address Local

                        if (orgCountry != null)
                        {
                            var oldCountryLocal = _countryRepository.GetCountryByCountryCode(oldAddressModel.CountryCode);
                            if (oldCountryLocal != null)
                            {
                                oldCountryName = GetLocalizedProperty.GetLocalizedValue(orgCountry.LocalLanguageID.ToString(), "Tbl_Country", "CountryName", oldCountryLocal.CountryID.ToString());
                            }
                            var newCountryLocal = _countryRepository.GetCountryByCountryCode(newAddressModel.CountryCode);
                            if (newCountryLocal != null)
                            {
                                newCountryName = GetLocalizedProperty.GetLocalizedValue(orgCountry.LocalLanguageID.ToString(), "Tbl_Country", "CountryName", newCountryLocal.CountryID.ToString());
                            }
                        }

                        var oldAddressLocal = new AddressModel();
                        var newAddressLocal = new AddressModel();

                        oldAddressLocal.HouseNo = oldAddressModel.HouseNo_Local ?? "";
                        oldAddressLocal.VillageNo = oldAddressModel.VillageNo_Local ?? "";
                        oldAddressLocal.Lane = oldAddressModel.Lane_Local ?? "";
                        oldAddressLocal.Road = oldAddressModel.Road_Local ?? "";
                        oldAddressLocal.SubDistrict = oldAddressModel.SubDistrict_Local ?? "";
                        oldAddressLocal.City = oldAddressModel.City_Local ?? "";
                        oldAddressLocal.State = oldAddressModel.State_Local ?? "";
                        oldAddressLocal.PostalCode = oldAddressModel.PostalCode ?? "";
                        oldAddressLocal.CountryName = oldCountryName ?? "";

                        newAddressLocal.HouseNo = newAddressModel.HouseNo_Local ?? "";
                        newAddressLocal.VillageNo = newAddressModel.VillageNo_Local ?? "";
                        newAddressLocal.Lane = newAddressModel.Lane_Local ?? "";
                        newAddressLocal.Road = newAddressModel.Road_Local ?? "";
                        newAddressLocal.SubDistrict = newAddressModel.SubDistrict_Local ?? "";
                        newAddressLocal.City = newAddressModel.City_Local ?? "";
                        newAddressLocal.State = newAddressModel.State_Local ?? "";
                        newAddressLocal.PostalCode = newAddressModel.PostalCode ?? "";
                        newAddressLocal.CountryName = newCountryName ?? "";


                        //Add value to Address_Local
                        oldAddressLocal = AddressManagementString.GetPrefix(oldAddressLocal);
                        newAddressLocal = AddressManagementString.GetPrefix(newAddressLocal);
                        item.NewAddress_Local = AddressManagementString.GetGenAddress(oldAddressLocal, newAddressLocal, out oldAddress);
                        item.OldAddress_Local = oldAddress;
                        //item.NewAddress_Local = CompoundStringManage.CompoundAddressTracking(oldAddressLocal, newAddressLocal, out oldAddress);
                        //item.OldAddress_Local = oldAddress;
                        #endregion

                        #region Address Inter

                        if (orgCountry != null)
                        {
                            var oldCountryInter = _countryRepository.GetCountryByCountryCode(oldAddressModel.CountryCode);
                            if (oldCountryInter != null)
                            {
                                oldCountryName = GetLocalizedProperty.GetLocalizedValue(orgCountry.InterLanguageID.ToString(), "Tbl_Country", "CountryName", oldCountryInter.CountryID.ToString());
                            }
                            var newCountryInter = _countryRepository.GetCountryByCountryCode(newAddressModel.CountryCode);
                            if (newCountryInter != null)
                            {
                                newCountryName = GetLocalizedProperty.GetLocalizedValue(orgCountry.InterLanguageID.ToString(), "Tbl_Country", "CountryName", newCountryInter.CountryID.ToString());
                            }
                        }

                        var oldAddressInter = new AddressModel();
                        var newAddressInter = new AddressModel();

                        oldAddressInter.HouseNo = oldAddressModel.HouseNo_Inter??"";
                        oldAddressInter.VillageNo = oldAddressModel.VillageNo_Inter ?? "";
                        oldAddressInter.Lane = oldAddressModel.Lane_Inter ?? "";
                        oldAddressInter.Road = oldAddressModel.Road_Inter ?? "";
                        oldAddressInter.SubDistrict = oldAddressModel.SubDistrict_Inter ?? "";
                        oldAddressInter.City = oldAddressModel.City_Inter ?? "";
                        oldAddressInter.State = oldAddressModel.State_Inter ?? "";
                        oldAddressInter.PostalCode = oldAddressModel.PostalCode ?? "";
                        oldAddressInter.CountryName = oldCountryName ?? "";

                        newAddressInter.HouseNo = newAddressModel.HouseNo_Inter ?? "";
                        newAddressInter.VillageNo = newAddressModel.VillageNo_Inter ?? "";
                        newAddressInter.Lane = newAddressModel.Lane_Inter ?? "";
                        newAddressInter.Road = newAddressModel.Road_Inter ?? "";
                        newAddressInter.SubDistrict = newAddressModel.SubDistrict_Inter ?? "";
                        newAddressInter.City = newAddressModel.City_Inter ?? "";
                        newAddressInter.State = newAddressModel.State_Inter ?? "";
                        newAddressInter.PostalCode = newAddressModel.PostalCode ?? "";
                        newAddressInter.CountryName = newCountryName ?? "";

                        //Add value to Address_Inter
                        item.NewAddress_Inter = AddressManagementString.GetGenAddress(oldAddressInter, newAddressInter, out oldAddress);
                        item.OldAddress_Inter = oldAddress;
                        //item.NewAddress_Inter = CompoundStringManage.CompoundAddressTracking(oldAddressInter, newAddressInter, out oldAddress);
                        //item.OldAddress_Inter = oldAddress;
                        #endregion

                    }
                }
            }
            return trackingContactAddressList;
        }

        public virtual IEnumerable<ViewTrackingContactAddressModel> GetOrgAddressTrackingByTrackingItemID(int trackingItemIDAddress, int trackingItemIDField)
        {

            var trackingContactAddressList = _trackingOrgAddressRepository.GetTrackingContactAddressByTrackingItemID(trackingItemIDAddress);

            foreach (var item in trackingContactAddressList)
            {

                if (item.OldAddressID == item.NewAddressID)
                {
                    var trackingField = _trackingComprofileRepository.GetTrackingFieldCompanyProfileByTrackingItemID(trackingItemIDField);
                    foreach (var itemTracking in trackingField)
                    {
                        if (itemTracking.isForeignKey == 1)
                        {
                            if (itemTracking.ForeignKeyGroup == "Tbl_Country")
                            {

                                string oldCountryName = _countryRepository.GetCountryNameByCountryCode(itemTracking.OldKeyValue);
                                string newCountryName = _countryRepository.GetCountryNameByCountryCode(itemTracking.NewKeyValue);

                                itemTracking.OldKeyValue = oldCountryName;
                                itemTracking.NewKeyValue = newCountryName;

                            }
                        }
                    }
                    item.AddressTrackingField = trackingField.ToList();
                }
                else
                {
                    var oldAddress = _addressRepository.GetAddressByAddressID(item.OldAddressID);
                    var newAddress = _addressRepository.GetAddressByAddressID(item.NewAddressID);
                    var fieldAddressTracking = _portalFieldConfigRepository.GetPortalFieldConfigIsTrackingByFieldGroup("Address");

                    if (oldAddress != null && newAddress != null)
                    {

                        var listtrackingField = new List<TrackingFieldCompanyProfileModel>();


                        #region----------------------------------Add Value Address Tracking-----------------------------------------

                        var trackingFieldHouseNo_Local = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.HouseNo_Local").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.HouseNo_Local,
                            NewKeyValue = newAddress.HouseNo_Local
                        };
                        listtrackingField.Add(trackingFieldHouseNo_Local);

                        var trackingFieldHouseNo_Inter = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.HouseNo_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.HouseNo_Inter,
                            NewKeyValue = newAddress.HouseNo_Inter
                        };
                        listtrackingField.Add(trackingFieldHouseNo_Inter);

                        var trackingFieldVillageNo_Local = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.VillageNo_Local").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.VillageNo_Local,
                            NewKeyValue = newAddress.VillageNo_Local
                        };
                        listtrackingField.Add(trackingFieldVillageNo_Local);

                        var trackingFieldVillageNo_Inter = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.VillageNo_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.VillageNo_Inter,
                            NewKeyValue = newAddress.VillageNo_Inter
                        };
                        listtrackingField.Add(trackingFieldVillageNo_Inter);

                        var trackingFieldLane_Local = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.Lane_Local").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.Lane_Local,
                            NewKeyValue = newAddress.Lane_Local
                        };
                        listtrackingField.Add(trackingFieldLane_Local);

                        var trackingFieldLane_Inter = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.Lane_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.Lane_Inter,
                            NewKeyValue = newAddress.Lane_Inter
                        };
                        listtrackingField.Add(trackingFieldLane_Inter);

                        var trackingFieldRoad_Local = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.Street_Local").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.Road_Local,
                            NewKeyValue = newAddress.Road_Local
                        };
                        listtrackingField.Add(trackingFieldRoad_Local);

                        var trackingFieldRoad_Inter = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.Street_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.Road_Inter,
                            NewKeyValue = newAddress.Road_Inter
                        };
                        listtrackingField.Add(trackingFieldRoad_Inter);

                        var trackingFieldSubDistrict_Local = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.SubDistrict_Local").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.SubDistrict_Local,
                            NewKeyValue = newAddress.SubDistrict_Local
                        };
                        listtrackingField.Add(trackingFieldSubDistrict_Local);

                        var trackingFieldSubDistrict_Inter = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.SubDistrict_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.SubDistrict_Inter,
                            NewKeyValue = newAddress.SubDistrict_Inter
                        };
                        listtrackingField.Add(trackingFieldSubDistrict_Inter);

                        var trackingFieldCity_Local = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.City_Local").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.City_Local,
                            NewKeyValue = newAddress.City_Local
                        };
                        listtrackingField.Add(trackingFieldCity_Local);

                        var trackingFieldCity_Inter = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.City_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.City_Inter,
                            NewKeyValue = newAddress.City_Inter
                        };
                        listtrackingField.Add(trackingFieldCity_Inter);

                        var trackingFieldState_Local = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.State_Local").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.State_Local,
                            NewKeyValue = newAddress.State_Local
                        };
                        listtrackingField.Add(trackingFieldState_Local);

                        var trackingFieldState_Inter = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.State_Inter").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.State_Inter,
                            NewKeyValue = newAddress.State_Inter
                        };
                        listtrackingField.Add(trackingFieldState_Inter);

                        var trackingFieldPostalCode = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.PostalCode").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldAddress.PostalCode,
                            NewKeyValue = newAddress.PostalCode
                        };
                        listtrackingField.Add(trackingFieldPostalCode);

                        string oldCountryName = _countryRepository.GetCountryNameByCountryCode(oldAddress.CountryCode);
                        string newCountryName = _countryRepository.GetCountryNameByCountryCode(newAddress.CountryCode);
                        var trackingFieldCountryName = new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = item.TrackingReqID,
                            FieldID = 0,
                            TrackingStatusID = item.TrackingStatusID,
                            TrackingStatusName = item.TrackingStatusName,
                            ResourceName = fieldAddressTracking.Where(m => m.FieldName == "Contact.DeliveredAddress.Country").Select(m => m.ResourceName).FirstOrDefault(),
                            OldKeyValue = oldCountryName,
                            NewKeyValue = newCountryName
                        };
                        listtrackingField.Add(trackingFieldCountryName);

                        item.AddressTrackingField = listtrackingField;

                        #endregion

                    }


                }
            }

            return trackingContactAddressList;
        }

        public virtual IEnumerable<TrackingFieldCompanyProfileModel> GetTrackingFieldCompanyProfileByTrackingReqID(int trackingReqID)
        {
            string languageId = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            var model = _trackingComprofileRepository.GetTrackingFieldCompanyProfileByTrackingReqID(trackingReqID);

            string oldValueForeign = "";
            string newValueForeign = "";
            string oldCountryName = "";
            string newCountryName = "";

            foreach (var item in model)
            {
                #region Check OtherField
                //รูปแบบการเก็บฟิวใน Tracking เช่น -1|อื่นๆ
                if (item.OtherFieldID != 0)
                {
                    int index1 = item.OldKeyValue.IndexOf('|');
                    if (index1 != -1)
                    {
                        //oldValueForeign = item.OldKeyValue.Remove(0, index1 + 1);
                        //item.OldKeyValue = oldValueForeign;
                        var splitOldKeyValue = item.OldKeyValue.Split('|');
                        oldValueForeign = GetLocalizedProperty.GetLocalizedValue(languageId, item.ForeignKeyGroup, item.ForeignKey, splitOldKeyValue[0]);
                        oldValueForeign += " (" + splitOldKeyValue[1] + ")";
                        item.OldKeyValue = oldValueForeign;
                    }
                    else
                    {
                        oldValueForeign = GetLocalizedProperty.GetLocalizedValue(languageId, item.ForeignKeyGroup, item.ForeignKey, item.OldKeyValue);
                        item.OldKeyValue = oldValueForeign;
                    }

                    int index2 = item.NewKeyValue.IndexOf('|');
                    if (index2 != -1)
                    {
                        //newValueForeign = item.NewKeyValue.Remove(0, index2 + 1);
                        //item.NewKeyValue = newValueForeign;
                        var splitNewKeyValue = item.NewKeyValue.Split('|');
                        newValueForeign = GetLocalizedProperty.GetLocalizedValue(languageId, item.ForeignKeyGroup, item.ForeignKey, splitNewKeyValue[0]);
                        newValueForeign += " (" + splitNewKeyValue[1] + ")";
                        item.NewKeyValue = newValueForeign;
                    }
                    else
                    {
                        newValueForeign = GetLocalizedProperty.GetLocalizedValue(languageId, item.ForeignKeyGroup, item.ForeignKey, item.NewKeyValue);
                        item.NewKeyValue = newValueForeign;
                    }
                }
                #endregion

                #region Check Foreign Field
                //ดึงค่าที่เก็บเป็น ID มาแสดง
                if (item.isForeignKey == 1 && item.OtherFieldID == 0)
                {
                    newValueForeign = "";
                    oldValueForeign = "";
                    oldCountryName = "";
                    newCountryName = "";

                    if (item.ForeignKeyGroup == "Tbl_Country")
                    {

                        var oldCountry = _countryRepository.GetCountryByCountryCode(item.OldKeyValue);
                        if (oldCountry != null)
                        {
                            oldCountryName = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_Country", "CountryName", oldCountry.CountryID.ToString());
                            item.OldKeyValue = oldCountryName;
                        }
                        var newCountry = _countryRepository.GetCountryByCountryCode(item.NewKeyValue);
                        if (newCountry != null)
                        {
                            newCountryName = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_Country", "CountryName", newCountry.CountryID.ToString());
                            item.NewKeyValue = newCountryName;
                        }

                    }
                    else
                    {

                        oldValueForeign = GetLocalizedProperty.GetLocalizedValue(languageId, item.ForeignKeyGroup, item.ForeignKey, item.OldKeyValue);
                        newValueForeign = GetLocalizedProperty.GetLocalizedValue(languageId, item.ForeignKeyGroup, item.ForeignKey, item.NewKeyValue);

                        item.OldKeyValue = oldValueForeign;
                        item.NewKeyValue = newValueForeign;
                    }
                }
                #endregion

                #region Check Branch Code Not Specify

                if (item.TrackingKey == "BranchNo")
                {
                    string notSpecify = "";

                    if (string.IsNullOrEmpty(item.OldKeyValue))
                    {
                        notSpecify = notSpecify.GetStringResource("_ComPro.CompanyInfo.LblBranchNotSpecified", languageId);

                        item.OldKeyValue = notSpecify;
                    }

                    if (string.IsNullOrEmpty(item.NewKeyValue))
                    {
                        notSpecify = notSpecify.GetStringResource("_ComPro.CompanyInfo.LblBranchNotSpecified", languageId);

                        item.NewKeyValue = notSpecify;
                    }
                }
                #endregion

            }

            return model;
        }

        public virtual IEnumerable<TrackingFieldCompanyProfileModel> GetTrackingFieldCompanyProfileByTrackingItemID(int trackingItemID)
        {
            string languageId = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            var model = _trackingComprofileRepository.GetTrackingFieldCompanyProfileByTrackingItemID(trackingItemID);

            string oldValueForeign = "";
            string newValueForeign = "";
            string oldCountryName = "";
            string newCountryName = "";

            foreach (var item in model)
            {
                #region Check OtherField
                if (item.OtherFieldID != 0)
                {
                    int index1 = item.OldKeyValue.IndexOf('|');
                    if (index1 != -1)
                    {
                        oldValueForeign = item.OldKeyValue.Remove(0, index1 + 1);
                        item.OldKeyValue = oldValueForeign;
                    }
                    else
                    {
                        oldValueForeign = GetLocalizedProperty.GetLocalizedValue(languageId, item.ForeignKeyGroup, item.ForeignKey, item.OldKeyValue);
                        item.OldKeyValue = oldValueForeign;
                    }

                    int index2 = item.NewKeyValue.IndexOf('|');
                    if (index2 != -1)
                    {
                        newValueForeign = item.NewKeyValue.Remove(0, index2 + 1);
                        item.NewKeyValue = newValueForeign;
                    }
                    else
                    {
                        newValueForeign = GetLocalizedProperty.GetLocalizedValue(languageId, item.ForeignKeyGroup, item.ForeignKey, item.NewKeyValue);
                        item.NewKeyValue = newValueForeign;
                    }
                }
                #endregion

                #region Check Foreign Field
                if (item.isForeignKey == 1 && item.OtherFieldID == 0)
                {
                    newValueForeign = "";
                    oldValueForeign = "";
                    oldCountryName = "";
                    newCountryName = "";

                    if (item.ForeignKeyGroup == "Tbl_Country")
                    {

                        var oldCountry = _countryRepository.GetCountryByCountryCode(item.OldKeyValue);
                        if (oldCountry != null)
                        {
                            oldCountryName = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_Country", "CountryName", oldCountry.CountryID.ToString());
                            item.OldKeyValue = oldCountryName;
                        }
                        var newCountry = _countryRepository.GetCountryByCountryCode(item.NewKeyValue);
                        if (newCountry != null)
                        {
                            newCountryName = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_Country", "CountryName", newCountry.CountryID.ToString());
                            item.NewKeyValue = newCountryName;
                        }

                    }
                    else
                    {
                        oldValueForeign = GetLocalizedProperty.GetLocalizedValue(languageId, item.ForeignKeyGroup, item.ForeignKey, item.OldKeyValue);
                        newValueForeign = GetLocalizedProperty.GetLocalizedValue(languageId, item.ForeignKeyGroup, item.ForeignKey, item.NewKeyValue);

                        item.OldKeyValue = oldValueForeign;
                        item.NewKeyValue = newValueForeign;
                    }
                }
                #endregion
                
            }

            return model;
        }

        public virtual void SendingEmailVerifyTracking(int trackingReqID, string languageId, string emailCC = "")
        {

            try
            {
                var _objmail = GetContentMailForVerifyTracking(trackingReqID, languageId, emailCC);

                EmailService.SendMailToAdmin(_objmail);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        #region------------------------------------------------Function Helper------------------------------------------------------

        private MailModel GetContentMailForVerifyTracking(int trackingReqID, string languageId, string emailCC)
        {
            try
            {
                string from = "";
                var messageTemplate = _messageTemplateRepository.GetMessageTemplateByName("CompanyProfile.OpenTracking");
                string pkTableMain = messageTemplate.MessageId.ToString();
                string toEmail = messageTemplate.ToEmailAddress;
                string CcEmailAddress = messageTemplate.CcEmailAddress;
                string BccEmailAddress = messageTemplate.BccEmailAddress;

                if (string.IsNullOrEmpty(CcEmailAddress))
                {
                    CcEmailAddress = emailCC;
                }
                else
                {
                    if (!string.IsNullOrEmpty(emailCC))
                    {
                        CcEmailAddress = CcEmailAddress + "," + emailCC;
                    }
                }

                var EmlaccResult = _emailAccountRepository.GetEmailAccountByEmailAccountID(messageTemplate.EmailAccountID ?? 0);


                var EmlaccList = EmlaccResult.FirstOrDefault();
                from = EmlaccList.Email;
                string subject = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Subject", pkTableMain);
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                string body = GetLocalizedProperty.GetLocalizedValue(languageId, "Tbl_MessageTemplate", "Body", pkTableMain);

                var trackingRequestModel = _trackingRequestRepository.GetTrackingRequestByTrackingReqID(trackingReqID);

                int localLanguageID = _languageRepository.GetLocalLanguage().LanguageID;

                int interLanguageID = _languageRepository.GetInterLanguage().LanguageID;

                string localCompanyName = "";
                string branchNo = "";
                string username = "";
                string titleName = "";
                string contactName = "";
                string tel = "";
                string email = "";
                string topic = "";

                string businessEntityDisplay = "";


                if (trackingRequestModel != null)
                {

                    #region-----------------Organization Data------------------

                    var orgModel = _organizationRepository.GetDataBySupplierID(trackingRequestModel.SupplierID ?? 0);

                    var businessEntityDisplayLocalModel = _businessEntityRepository.GetLocalizedBusinessEntityDisplayByIdAndLanguageID(orgModel.BusinessEntityID ?? 0, localLanguageID);

                    if (orgModel != null)
                    {
                        localCompanyName = orgModel.CompanyName_Local;

                        if (businessEntityDisplayLocalModel != null)
                        {
                            if (businessEntityDisplayLocalModel.Id == -1)
                            {
                                businessEntityDisplay = orgModel.OtherBusinessEntity;
                            }else
                            {
                                businessEntityDisplay = businessEntityDisplayLocalModel.BusinessEntityDisplay;
                            }
                            

                            if (businessEntityDisplayLocalModel.ConcatStrLocal.Trim() == "P")
                            {
                                localCompanyName = businessEntityDisplay + localCompanyName;
                            }
                            else
                            {
                                localCompanyName = localCompanyName + businessEntityDisplay;
                            }
                        }

                    }

                    branchNo = orgModel.BranchNo;
                    #endregion End Organization Data

                    #region-----------------ReqBy Data------------------

                    var userModel = _userRepository.FindUserByUserID(trackingRequestModel.ReqBy??0);

                    if (userModel != null)
                    {
                        username = userModel.Username;

                        var contactPersonModel = _contactPersonRepository.GetContactPersonByContectID(userModel.ContactID??0);

                        if (contactPersonModel != null)
                        {
                            titleName = _nameTitleRepository.GetTitleNameByIdAndLanguageID(contactPersonModel.TitleID ?? 0, localLanguageID);
                            contactName = contactPersonModel.FirstName_Local + "  " + contactPersonModel.LastName_Local;

                            tel = contactPersonModel.PhoneNo;
                            if (!string.IsNullOrEmpty(contactPersonModel.PhoneExt))
                            {
                                tel += " ต่อ " + contactPersonModel.PhoneExt;
                            }

                            email = contactPersonModel.Email;

                        }
                    }

                    #endregion End ReqBy Data

                    #region-----------------Get Topic Data------------------
                    string messageReturn = "";
                    string resourceNameTopic = "";
                    var trackingGroup = _trackingRequestRepository.GetTrackingGroupByTrackingGrpID(trackingRequestModel.TrackingGrpID??0);
                    if(trackingGroup!=null)
                    {
                        resourceNameTopic = trackingGroup.ResourceName;
                    }
                    
                    topic = messageReturn.GetStringResource(resourceNameTopic, localLanguageID.ToString());
                    #endregion End Get Topic Data


                    DateTime thisDateNow = DateTime.Now;
                    string today = thisDateNow.ToString("dd/MM/yyyy H:mm:ss");

                    subject = subject.Replace("<%LocalCompanyName%>", localCompanyName);
                    subject = subject.Replace("<%Reg.No%>", trackingReqID.ToString());

                    body = @body.Replace("<%ExportedDate%>", today);

                    body = @body.Replace("<%CompanyName%>", localCompanyName);
                    body = @body.Replace("<%BranchNo%>", branchNo);
                    body = @body.Replace("<%Username%>", username);
                    body = @body.Replace("<%THNameTitle%>", titleName);
                    body = @body.Replace("<%THContactName%>", contactName);
                    body = @body.Replace("<%ContactTel%>", tel);
                    body = @body.Replace("<%ContactEmail%>", email);
                    body = @body.Replace("<%Topic%>", topic);


                }

                MailModel _objmail = new MailModel
                {
                    From = from,
                    To = toEmail,
                    CcEmailAddress = CcEmailAddress,
                    BccEmailAddress = BccEmailAddress,
                    Subject = subject,
                    Body = body,
                    Host = host,
                };

                return _objmail;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

    }
}
