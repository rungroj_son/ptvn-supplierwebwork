﻿using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Services.CompanyProfileManage
{
    public partial interface IComProfileTrackingService
    {
        OrgAttachmentListModels GetOrgAttachmentListBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID, int companyTypeID);

        OrgAttachmentListModels GetOrgAttachmentListBySupplierIDAndDocumentTypeIDOther(int supplierID, int documentTypeID, int companyTypeID);

        ViewDocumentTrackingModels GetDocumentTrackingByTrackingItemID(int trackingItemID);

        IEnumerable<ViewTrackingContactAddressModel> GetOrgAddressTrackingByTrackingReqID(int trackingReqID);

        IEnumerable<ViewTrackingContactAddressModel> GetOrgAddressTrackingViewByTrackingReqID(int trackingReqID);

        IEnumerable<ViewTrackingContactAddressModel> GetOrgAddressTrackingViewDataByTrackingReqID(int trackingReqID);

        IEnumerable<ViewTrackingContactAddressModel> GetOrgAddressTrackingByTrackingItemID(int trackingItemIDAddress, int trackingItemIDField);

        IEnumerable<TrackingFieldCompanyProfileModel> GetTrackingFieldCompanyProfileByTrackingReqID(int trackingReqID);

        IEnumerable<TrackingFieldCompanyProfileModel> GetTrackingFieldCompanyProfileByTrackingItemID(int trackingItemID);

        void SendingEmailVerifyTracking(int trackingReqID, string languageId, string emailCC = "");

    }
}
