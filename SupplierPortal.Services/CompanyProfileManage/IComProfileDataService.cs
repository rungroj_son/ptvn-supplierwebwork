﻿using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Services.CompanyProfileManage
{
    public partial interface IComProfileDataService
    {
        CompanyGeneralModel GetGeneralComProfileBySupplierID(int supplierID);

        CompanyProductModel GetProductServiceBySupplierID(int supplierID);

        CompanyProductKeywordModel GetProductKeywordBySupplierID(int supplierID);

        void UpdateProductService(CompanyProductModel model, int updateByUserID);

        void InsertCompanyContactPerson(CompanyContactPersonModel model, int updateByUserID);

        void DeleteCompanyContactPerson(int supplierID, int contactID, int updateByUserID);

        void EditCompanyContactPerson(CompanyContactPersonModel model, int updateByUserID);

        CompanyContactPersonModel GetCompanyContactPersonByContactID(int contactID);

        CompanyFacilityModel GetDataFacAddressBySupplierID(int supplierID, int facilityID);

        IEnumerable<OrgCertifiedStdTableModel> GetOrgCertifiedStdTableBySupplierID(int supplierID);

        CompanyCertificationModel GetCompanyCertificationByCertId(int certId);

        void DeleteOrgCertifiedStd(int certId, int updateByUserID);

    }
}
