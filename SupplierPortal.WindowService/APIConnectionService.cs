﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.WindowService
{
    public class APIConnectionService
    {
        WriteLog writeLog;

        public APIConnectionService()
        {
            writeLog = new WriteLog();
        }

        public async Task<HttpResponseMessage> PostApiConnection(string url, Object model)
        {
            HttpClient client = new HttpClient();
            try
            {
                string languageURL = string.Empty;
                string baseURL = ConfigurationManager.AppSettings["baseURL"];

                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string Jsonstr = JsonConvert.SerializeObject(model);
                HttpContent content = new StringContent(Jsonstr, Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string urlAll = string.Format(url, baseURL);
                //string urlAll = string.Format("{0}api/AccountPortal/ResetPassword{1}", baseURL, languageURL);

                HttpResponseMessage response = await client.PostAsync(urlAll, content);
                writeLog.WriteToFile("APIConnectionService Post(Success) " + DateTime.Now);
                return response;
            }
            catch (Exception ex)
            {
                writeLog.WriteToFile("APIConnectionService Post(Error) " + DateTime.Now + " " + ex.Message);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }

        }

        public async Task<HttpResponseMessage> GetApiConnection(string url)
        {
            HttpClient client = new HttpClient();
            try
            {
                string languageURL = string.Empty;
                string baseURL = ConfigurationManager.AppSettings["baseURL"];

                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(baseURL + url);
                writeLog.WriteToFile("APIConnectionService Get(Success) " + DateTime.Now);
                return response;
            }
            catch (Exception ex)
            {
                writeLog.WriteToFile("APIConnectionService Get(Error) " + DateTime.Now + " " + ex.Message);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }
    }
}
