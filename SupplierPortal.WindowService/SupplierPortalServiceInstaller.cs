﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace SupplierPortal.WindowService
{
    [RunInstaller(true)]
    public partial class SupplierPortalServiceInstaller : System.Configuration.Install.Installer
    {
        public SupplierPortalServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
