﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums
{

    /// <summary>
    /// The enum attrribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class EnumAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnumAttribute"/> class.
        /// </summary>
        protected EnumAttribute()
        {
        }
    }

    /// <summary>
    /// The enum extension.
    /// </summary>
    public static class EnumExtension
    {
        /// <summary>
        /// The get attribute.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The <see cref="EnumAttribute"/>.
        /// </returns>
        public static EnumAttribute GetAttribute(this Enum value)
        {
            Type type = value.GetType();
            FieldInfo fieldInfo = type.GetField(value.ToString());
            var atts = (EnumAttribute[])fieldInfo.GetCustomAttributes(typeof(EnumAttribute), false);
            return atts.Length > 0 ? atts[0] : null;
        }

        public static string GetAttributeCode(this Enum value)
        {
            return ((SWWAttribute)value.GetAttribute()).Code;
        }

        public static string GetAttributeDescription(this Enum value)
        {
            return ((SWWAttribute)value.GetAttribute()).Description;
        }

        public static string GetAttributeTypeCode(this Enum value)
        {
            return ((SWWAttribute)value.GetAttribute()).TypeCode;
        }

        public static int GetAttributeId(this Enum value)
        {
            return ((SWWAttribute)value.GetAttribute()).Id;
        }
    }

    /// <summary>
    /// The enum extension.
    /// </summary>
    /// <typeparam name="T">
    /// </typeparam>
    public static class EnumExtension<T>
    {
        public static T GetEnumByName(string name)
        {
            var enumType = typeof(T);

            var items = enumType.GetMembers(BindingFlags.Public | BindingFlags.Static).Where(e => e.Name == name);

            return (T)Enum.Parse(typeof(T), items.FirstOrDefault().Name, true);
        }

        public static T GetEnumByCode(string code)
        {
            var enumType = typeof(T);
            MemberInfo result = null;
            var items = enumType.GetMembers(BindingFlags.Public | BindingFlags.Static);
            foreach (var item in items)
            {
                var check = item.GetCustomAttributes().FirstOrDefault(i => ((SWWAttribute)i).Code.ToUpper() == code.ToUpper());
                if (check != null)
                {
                    result = item;
                    break;
                }
            }
            if (result == null)
            {
                throw new ArgumentException("Argument incorect.");
            }
            return (T)Enum.Parse(typeof(T), result.Name, true);
        }

        public static T GetEnumById(int id)
        {
            var enumType = typeof(T);
            MemberInfo result = null;
            var items = enumType.GetMembers(BindingFlags.Public | BindingFlags.Static);
            foreach (var item in items)
            {
                var check = item.GetCustomAttributes().FirstOrDefault(i => ((SWWAttribute)i).Id == id);
                if (check != null)
                {
                    result = item;
                    break;
                }
            }
            if (result == null)
            {
                throw new ArgumentException("Argument incorect.");
            }
            return (T)Enum.Parse(typeof(T), result.Name, true);
        }

        public static T GetEnumByDescription(string description)
        {
            var enumType = typeof(T);
            MemberInfo result = null;
            var items = enumType.GetMembers(BindingFlags.Public | BindingFlags.Static);
            foreach (var item in items)
            {
                var check = item.GetCustomAttributes().FirstOrDefault(i => ((SWWAttribute)i).Description == description);
                if (check != null)
                {
                    result = item;
                    break;
                }
            }
            if (result == null)
            {
                return default(T); //ใช้ใน Audit(GetDisplayName) จะ Return ค่าแรกของ Enum ถ้าหาไม่เจอ
            }
            return (T)Enum.Parse(typeof(T), result.Name, true);
        }

        public static T GetEnumByTypeCode(string typecode)
        {
            var enumType = typeof(T);
            MemberInfo result = null;
            var items = enumType.GetMembers(BindingFlags.Public | BindingFlags.Static);
            foreach (var item in items)
            {
                var check = item.GetCustomAttributes().FirstOrDefault(i => ((SWWAttribute)i).TypeCode == typecode);
                if (check != null)
                {
                    result = item;
                    break;
                }
            }
            if (result == null)
            {
                return default(T); //ใช้ใน Audit(GetDisplayName) จะ Return ค่าแรกของ Enum ถ้าหาไม่เจอ
            }
            return (T)Enum.Parse(typeof(T), result.Name, true);
        }
    }

    /// <summary>
    /// The pvd attribute.
    /// </summary>
    public class SWWAttribute : EnumAttribute
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the typecode.
        /// </summary>
        public string TypeCode { get; set; }

        public SWWAttribute()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SWWAttribute"/> class.
        /// </summary>
        /// <param name="id">
        /// The Id.
        /// </param>
        public SWWAttribute(int id)
        {
            Id = id;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PISAttribute"/> class.
        /// </summary>
        /// <param name="code">
        /// The code.
        /// </param>
        public SWWAttribute(string code)
        {
            Code = code;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PISAttribute"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="code">
        /// The code.
        /// </param>
        public SWWAttribute(int id, string code)
        {
            Id = id;
            Code = code;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PISAttribute"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        public SWWAttribute(int id, string code, string description)
        {
            Id = id;
            Code = code;
            Description = description;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="PISAttribute"/> class.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <param name="typecode">
        /// The TypeCode.
        /// </param>
        /// <param name="code">
        /// The code.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        public SWWAttribute(int id, string typecode, string code, string description)
        {
            Id = id;
            TypeCode = typecode;
            Code = code;
            Description = description;
        }
    }
}
