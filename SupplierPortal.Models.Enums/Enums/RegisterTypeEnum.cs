﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums
{
    public enum RegisterTypeEnum
    {
        [SWW(Code = "REGISTER")]
        REGISTER,

        [SWW(Code = "REQUEST_JOIN")]
        REQUEST_JOIN
    }
}
