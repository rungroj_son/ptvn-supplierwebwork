﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums.Enums
{
    public enum RedirectActionNameEnum
    {
        [SWW(Code = "RedirectAVL", TypeCode = "AVL")]
        RedirectAVL,

        [SWW(Code = "RedirectAuction", TypeCode = "RedirectAuction")]
        RedirectAuction,

        [SWW(Code = "RedirectProcurementBoard", TypeCode = "Procurement Board")]
        RedirectProcurementBoard
    }
}           