﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Models.Enums
{
    public enum RegStatusEnum
    {
        [SWW(Id = 1)]
        NEW,

        [SWW(Id = 2)]
        WAITING,

        [SWW(Id = 3)]
        REJECTED,

        [SWW(Id = 4)]
        CONFIRM,

        [SWW(Id = 5)]
        SUCCESS
    }
}
