﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.SendEmail
{
    public partial class SendEmailRequest
    {
        public string mailFrom { get; set; }
        public string[] mailTo { get; set; }
        public string[] mailCC { get; set; }
        public string mailSubject { get; set; }
        public int templateId { get; set; }
        public string mappingData { get; set; }
    }
}
