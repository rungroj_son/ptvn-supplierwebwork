﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.BuyerSuppliers
{
    public class SupplierUserModel
    {
        public int SupplierID { get; set; }
        public int ContactID { get; set; }
        public Nullable<int> ContactTypeID { get; set; }
        public Nullable<int> TitleID { get; set; }
        public string FirstName_Local { get; set; }
        public string LastName_Local { get; set; }
        public string FirstName_Inter { get; set; }
        public string LastName_Inter { get; set; }
        public Nullable<int> JobTitleID { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string Email { get; set; }
        public int UserID { get; set; }
        public string Username { get; set; }
        public string OrgID { get; set; }
        public string CompanyName_Local { get; set; }
        public string CompanyName_Inter { get; set; }
        public int EID { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public string TaxID { get; set; }
        public string MobileCountryCode { get; set; }
        public string MobileNo { get; set; }
        public int CurrentID { get; set; }//Buyer Id

        // Tbl_OrgContactPerson
        public Nullable<int> isPrimaryContact { get; set; }

    }
}
