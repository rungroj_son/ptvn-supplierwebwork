﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.BuyerSuppliers
{
    public partial class BuyerSuppliersModel : ViewBuyerSupplier
    {
        public int CurrentID { get; set; }
        public string CurrentUser { get; set; }
    }
}
