﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.BuyerSuppliers
{
    public partial class BuyerHistoryContactPersonModel
    {
        public Nullable<int> UserID { get; set; }

        public string Username { get; set; }

        public Nullable<int> SupplierID { get; set; }

        public string OrgID { get; set; }

        public Nullable<int> ContactID { get; set; }

        public string Title_Local { get; set; }

        public string Title_Inter { get; set; }

        public string FirstName_Local { get; set; }

        public string LastName_Local { get; set; }

        public string FirstName_Inter { get; set; }

        public string LastName_Inter { get; set; }

        public string PhoneNo { get; set; }

        public string PhoneExt { get; set; }

        public string MobileNo { get; set; }

        public string MobileCountryCode { get; set; }

        public string PhoneCountryCode { get; set; }

        public string Email { get; set; }

        public Nullable<int> isDeleted { get; set; }

        public Nullable<int> UpdateById { get; set; }

        public string UpdateByName { get; set; }

        public Nullable<DateTime> UpdateDate { get; set; }

        public string TimeZone { get; set; }

        public Nullable<int> IsPrimaryContact { get; set; }

    }
}
