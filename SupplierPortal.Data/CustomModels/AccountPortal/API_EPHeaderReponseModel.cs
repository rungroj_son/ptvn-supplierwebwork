﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.AccountPortal
{
    public partial class API_EPHeaderReponseModel
    {
        public string APIName { get; set; }
        public string APIKey { get; set; }
        public string ReqID { get; set; }
        public string SID { get; set; }
        public string EID { get; set; }
        public string SysUserID { get; set; }
        public string LanguageCode { get; set; }

        /// <summary>
        /// EPURL is link post ERFX for EP and OP
        /// </summary>
        public string EPURL { get; set; }
        public string Name { get; set; }
        public string Organization { get; set; }
        public string ActiveSessionURL { get; set; }
        public string SessionTimeoutURL { get; set; }
        public string Reports { get; set; }
        public string HeaderType { get; set; }
        public HeaderInfo HeaderInfo { get; set; }
        public string result { get; set; }

        // New fields
        public string CreateERFX { get; set; }
        public string CreateSupplierGroup { get; set; }
    }

    public class HeaderInfo
    {
        public string CustomerLogoURL { get; set; }
        public List<TopMenu> TopMenu { get; set; }
        public List<ControlPanel> ControlPanel { get; set; }
        public List<Help> Help { get; set; }
    }
    public class TopMenu
    {
        public string Visible { get; set; }
        public string DisplayName { get; set; }
        public string DisplayNameTH { get; set; }
        public string Icon { get; set; }
        public string URL { get; set; }
        public string PopupURL { get; set; }
        public List<DisplayContent> DisplayContent { get; set; }
    }
    public class ControlPanel
    {
        public string DisplayName { get; set; }
        public string DisplayNameTH { get; set; }
        public string Icon { get; set; }
        public string URL { get; set; }
        public string Visible { get; set; }
    }
    public class Help
    {
        public List<Content_list> content_list { get; set; }
    }

    public class DisplayContent
    {
        public string topic { get; set; }
        public List<Content_list> content_list { get; set; }
    }

    public class Content_list
    {
        public string content { get; set; }
        public string display_url { get; set; }
    }

}
