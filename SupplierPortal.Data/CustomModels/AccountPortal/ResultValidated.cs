﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.AccountPortal
{
    public class ResultValidated
    {
        // Auto-implemented properties. 
        public string Username { get; set; }
        public string SysUserID { get; set; }
        public bool Result { get; set; }
        public string GUID { get; set; }
        public string EID { get; set; }
        public string Message { get; set; }
        public string User { get; set; }
        public string Organization { get; set; }
        public string LanguageId { get; set; }
        public string isAcceptTermOfUse { get; set; }
        public bool isCheckConsent { get; set; }
        public bool isUserEInvoice { get; set; }
        public bool isAcceptBillingCondition { get; set; }
    }
}
