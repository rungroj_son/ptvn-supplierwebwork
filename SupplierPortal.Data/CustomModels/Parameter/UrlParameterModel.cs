﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.Parameter
{
    public partial class UrlParameterModel
    {
        public string Url { get; set; }

        public string Parameter { get; set; }
    }
}
