﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.SearchResult
{
    public partial class SearchResultResponse<T>
    {
        public int totalRecord { get; set; }
        public int totalItem { get; set; }
        public int totalPage { get; set; }
        public int totalMainRecord { get; set; }
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public T dataResult { get; set; }
    }
}