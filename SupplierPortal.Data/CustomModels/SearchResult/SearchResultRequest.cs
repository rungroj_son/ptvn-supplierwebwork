﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.SearchResult
{
    public partial class SearchResultRequest
    {
        public int pageIndex { get; set; }
        public int pageSize { get; set; }
        public string sortColumn { get; set; }
        public bool sortAscending { get; set; }
        public List<AdvanceSearch> advanceSearch;
    }
}
