﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.SupportModel.Register;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SupplierPortal.Data.CustomModels.RegisterSupplier
{
    public partial class RegisterSupplierRequest
    {

        public string Proc { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.RegID_NotNull")]
        public int RegID { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.RegStatusID_NotNull")]
        public int RegStatusID { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Register.ErrorMsg.TicketCode_NotNull")]
        public string TicketCode { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.CompanyTypeID_NotNull")]
        public int CompanyTypeID { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.BusinessEntityID_NotNull")]
        public int BusinessEntityID { get; set; }

        public string OtherBusinessEntity { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.CompanyNameLocal_NotNullOrEmpty")]
        public string CompanyName_Local { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.CompanyNameInter_NotNullOrEmpty")]
        public string CompanyName_Inter { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.CountryCode_NotNullOrEmpty")]
        public string CountryCode { get; set; }

        public string CountryName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.TaxID_NotNullOrEmpty")]
        public string TaxID { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.BranchNo_NotNullOrEmpty")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Register.ErrorMsg.BranchNoCharactersOnly")]
        public string BranchNo { get; set; }

        public string MainBusiness { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.CompanyAddrIDNotNull")]
        [Range(0, 0, ErrorMessage = "Register.ErrorMsg.NumberEqualZero")]
        public int CompanyAddrID { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_HouseNo_Local_NotNull")]
        public string RegCompanyAddress_HouseNo_Local { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_HouseNo_Inter_NotNull")]
        public string RegCompanyAddress_HouseNo_Inter { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_VillageNo_Local_NotNull")]
        public string RegCompanyAddress_VillageNo_Local { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_VillageNo_Inter_NotNull")]
        public string RegCompanyAddress_VillageNo_Inter { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_Lane_Local_NotNull")]
        public string RegCompanyAddress_Lane_Local { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_Lane_Inter_NotNull")]
        public string RegCompanyAddress_Lane_Inter { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_Road_Local_NotNull")]
        public string RegCompanyAddress_Road_Local { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_Road_Inter_NotNull")]
        public string RegCompanyAddress_Road_Inter { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_SubDistrict_Local_NotNull")]
        public string RegCompanyAddress_SubDistrict_Local { get; set; }

        [Required(AllowEmptyStrings = true, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_SubDistrict_Inter_NotNull")]
        public string RegCompanyAddress_SubDistrict_Inter { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_City_Local_NotNullOrEmpty")]
        public string RegCompanyAddress_City_Local { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_City_Inter_NotNullOrEmpty")]
        public string RegCompanyAddress_City_Inter { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_State_Local_NotNullOrEmpty")]
        public string RegCompanyAddress_State_Local { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_State_Inter_NotNullOrEmpty")]
        public string RegCompanyAddress_State_Inter { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.RegCompanyAddress_PostalCode_NotNullOrEmpty")]
        public string RegCompanyAddress_PostalCode { get; set; }

        public string RegCompanyAddress_CountryCode { get; set; }

        public string RegCompanyAddress_CountryName { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.MainContactID_NotNull")]
        [Range(0, 0, ErrorMessage = "Register.ErrorMsg.NumberEqualZero")]
        public int MainContactID { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.NameTitleID_NotNull")]
        public int NameTitleID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.FirstName_Local_NotNullOrEmpty")]
        public string FirstName_Local { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.LastName_Local_NotNullOrEmpty")]
        public string LastName_Local { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.FirstName_Inter_NotNullOrEmpty")]
        public string FirstName_Inter { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.LastName_Inter_NotNullOrEmpty")]
        public string LastName_Inter { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.JobTitleID_NotNull")]
        public int JobTitleID { get; set; }

        public string OtherJobTitle { get; set; }

        public string PhoneNoCountryCode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.PhoneNo_NotNullOrEmpty")]
        public string PhoneNo { get; set; }

        public string PhoneExt { get; set; }

        public string MobileNoCountryCode { get; set; }

        public string MobileNo { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.Email_NotNullOrEmpty")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.ConfirmEmail_NotNull")]
        [CompareErrorMessage("Email", "Register.ErrorMsg.NotMatchEmail")]
        public string ConfirmEmail { get; set; }

        public bool ServiceCheck1 { get; set; }

        public bool ServiceCheck2 { get; set; }

        public int[] ServiceTypeList { get; set; }


        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.ContactEmail_NotNullOrEmpty")]
        public string ContactEmail { get; set; }

        [Required(ErrorMessage = "Register.ErrorMsg.ConfirmContactEmail_NotNull")]
        [CompareErrorMessage("ContactEmail", "Register.ErrorMsg.NotMatchEmail")]
        public string ConfirmContactEmail { get; set; }

        public List<BusinessTypeRegModel> OrgBusinessType { get; set; }

        public string ProductKeyword { get; set; }

        public decimal RegisteredCapital { get; set; }

        [StringLength(3, ErrorMessage = "Register.ErrorMsg.CurrencyCodeExceedLimitKeyword")]
        public string CurrencyCode { get; set; }

        public string CategoryID_Lev3 { get; set; }

        public string InvitationCode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Register.ErrorMsg.LanguageCode_NotNullOrEmpty")]
        public string LanguageCode { get; set; }

        public List<RegAttachment> RegAttachments;

        public OPNSupplierRegister OpnSupplierRegister { get; set; }
    }

    public partial class RegAttachment
    {
        public string AttachmentName;

        public string AttachmentNameUnique;

        public string Extension;

        public long SizeAttach;

        public int DocumentNameID;
    }

}