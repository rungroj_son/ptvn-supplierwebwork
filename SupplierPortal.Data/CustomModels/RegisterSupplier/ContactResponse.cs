﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.RegisterSupplier
{
    public partial class ContactResponse
    {
        public int ContactRegID { get; set; }
        public int SupplierID { get; set; }
        public string ContactEmail { get; set; }
        public string ErrorMsg { get; set; }
    }
}
