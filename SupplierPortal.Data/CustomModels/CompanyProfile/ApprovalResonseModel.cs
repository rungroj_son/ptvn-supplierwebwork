﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.CompanyProfile
{
    public partial class ApprovalResonseModel
    {
        public int ItemId { get; set; }
        [JsonProperty("AddressTypeId", NullValueHandling = NullValueHandling.Ignore)]
        public Nullable<int> AddressTypeId { get; set; }
        [JsonProperty("AddressTypeName", NullValueHandling = NullValueHandling.Ignore)]
        public string AddressTypeName { get; set; }
        public int TrackingStatusId { get; set; }
        public string TrackingStatusName { get; set; }
        public DateTime TrackingStatusDate { get; set; }
        public string Remark;

    }
}
