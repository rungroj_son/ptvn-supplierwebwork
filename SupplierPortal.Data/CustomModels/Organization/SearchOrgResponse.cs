﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.Organization
{
    public partial class SearchOrgResponse
    {
        public int RowNumber { get; set; }
        public string OrgID { get; set; }
        public int SupplierID { get; set; }
        public string CompanyName_Inter { get; set; }
        public string CompanyName_Local { get; set; }
        public List<SearchContactResponse> SearchContacts { get; set; }
    }

    public partial class SearchContactResponse
    {
        public int ContactID { get; set; }
        public string FirstName_Local { get; set; }
        public string LastName_Local { get; set; }
        public string FirstName_Inter { get; set; }
        public string LastName_Inter { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string PhoneCountryCode { get; set; }
        public string MobileNo { get; set; }
        public string MobileCountryCode { get; set; }
        public string Email { get; set; }
        public int UserID { get; set; }
        public string Username { get; set; }
    }


}
