﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.SCF
{
    [MetadataType(typeof(SCFRequest))]
    public partial class SCFRequest
    {
        [Required(ErrorMessage = "UserGUID")]
        public string userGUID { get; set; }
        [Required(AllowEmptyStrings = true, ErrorMessage = "ACtion")]
        public string actionFrom { get; set; }
    }
}
