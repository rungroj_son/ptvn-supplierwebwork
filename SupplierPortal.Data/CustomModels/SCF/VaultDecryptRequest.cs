﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.SCF
{
    public partial class VaultDecryptRequest
    {
        public string ciphertext { get; set; }
    }
}
