﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.CustomModels.SCF
{
    public class SupplierMpInfoResponse
    {
        public string accountCode { get; set; }
        public string buyerMpId { get; set; }
        public int supplierId { get; set; }
        public string supplierMpId { get; set; }
        public string supplierName { get; set; }
        public string supplierShortName { get; set; }
    }
}
