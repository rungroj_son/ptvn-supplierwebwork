﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.Language;
using System.Web;

namespace SupplierPortal.Data.Helper
{
    public static class LanguageHelper
    {

        public static int GetLanguageIsInter()
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            ILanguageRepository _repo = new LanguageRepository();
            var _tempLanguage = _repo.GetLanguageById(languageId);
            int isInter = 0;
            if (_tempLanguage != null)
            {
                isInter = _tempLanguage.isInter ?? 0;
            }

            return isInter;
        }

        public static string GetStringLanguageIDCurrent()
        {
            HttpCookie cultureCookie = System.Web.HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }

            return languageID;
        }

        public static int GetIntLanguageIDCurrent()
        {
            HttpCookie cultureCookie = System.Web.HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }

            int languageId = Convert.ToInt32(languageID);

            return languageId;
        }
    }
}
