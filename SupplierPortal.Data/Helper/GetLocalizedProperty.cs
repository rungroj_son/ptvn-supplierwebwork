﻿using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Helper
{
    public static class GetLocalizedProperty
    {      

        public static string GetLocalizedValue(string languageID, string localeKeyGroup, string localeKey, string pkTableMain)
        {
            string resultValue = "";
            string sqlQuery = "";
            string variabledynamic = "";

            variabledynamic = localeKey + "_" + languageID;

            try
            {

                ILocalizedPropertyRepository _repoLocalizedProperty = null;
                _repoLocalizedProperty = new LocalizedPropertyRepository();

                sqlQuery = "EXEC LocalizedProperty_find '" + languageID + "','" + localeKeyGroup + "','" + pkTableMain + "' ";

                var result = _repoLocalizedProperty.GetLocalizedByStore(sqlQuery);

                if (!string.IsNullOrEmpty(result[variabledynamic].ToString()))
                {
                    resultValue = result[variabledynamic];
                }
                else
                {
                    resultValue = result[localeKey].ToString();
                }

            }
            catch (Exception e)
            {

                throw;
            }

            return resultValue;
        }      
    }
}
