﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.RegisterPortal
{
    public partial class ContactPersonAdminModels
    {
        public int SupplierID { get; set; }
        public int ContactID { get; set; }
        public string Company { get; set; }
        public string TitleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string Email { get; set; }

    }

    public partial class ContactPersonSendEmailModels
    {
        public int SupplierID { get; set; }
        public int ContactID { get; set; }
        public string Company { get; set; }
        public string AdminName { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }

    }
}
