﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.RegisterPortal
{
    public partial class CustomerDocNameModels
    {

        public int DocumentNameID { get; set; }
        public string DocumentName { get; set; }
        public int SeqNo { get; set; }
        public int DocumentTypeID { get; set; }
    }
}
