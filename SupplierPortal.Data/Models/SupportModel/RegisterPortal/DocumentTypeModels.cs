﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.RegisterPortal
{
    public partial class DocumentTypeModels
    {
        public int DocumentTypeID { get; set; }
        public string DocumentType { get; set; }
        public int SeqNo { get; set; }
        public int CompanyTypeID { get; set; }
        public string CountryCode { get; set; }
    }
}
