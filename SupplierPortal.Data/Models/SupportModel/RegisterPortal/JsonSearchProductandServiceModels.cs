﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.RegisterPortal
{

    public partial class JsonSearchProductandServiceModels
    {
        public int current { get; set; }
        public int rowCount { get; set; }
        public List<ProductCategoryResultModels> rows { get; set; }
        //public List<ProductandServiceResultModels> rows { get; set; }
        public int total { get; set; }

    }

    //public class Rows
    //{
    //    public string ProductCode { get; set; }
    //    public string ProductName { get; set; }
    //}
}
