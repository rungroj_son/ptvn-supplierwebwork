﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.RegisterPortal
{
    public partial class DynamicLanguageModels
    {
        public IEnumerable<Tbl_Language> LanguageLocalInter { get; set; }
        public IEnumerable<Tbl_Language> LanguageLocalized { get; set; }
    }

   
}
