﻿using System;


namespace SupplierPortal.Data.Models.SupportModel.JsonAPI
{
    public partial class JsonCountryModel
    {
        public string id { get; set; }
        public string text { get; set; }
    }
}
