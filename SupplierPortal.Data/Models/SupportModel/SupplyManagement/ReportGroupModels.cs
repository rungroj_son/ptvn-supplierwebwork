﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.SupplyManagement
{
     public partial class ReportGroupModels
    {

        // public ReportGroupModels()
        //{
        //    this.ReportListModels = new HashSet<ReportListModels>();
        //}
        // [Key]
         public int ReportGroupID { get; set; }
         public string ReportGroupName { get; set; }


         //public virtual ICollection<ReportListModels> ReportListModels { get; set; }
    }
}
