﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.MenuPortal
{
    public partial class NotificationBoxModels
    {
        public bool GetNotificationShow { get; set; }
        public IList<NotificationResponse> NotificationResponse { get; set; }

    }

    public class NotificationResponse
    {
        public int NotificationID { get; set; }
        public int NotificationValue { get; set; }
    }
}
