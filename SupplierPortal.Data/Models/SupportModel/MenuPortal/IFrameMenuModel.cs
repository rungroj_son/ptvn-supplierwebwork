﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.MenuPortal
{
    public partial class IFrameMenuModel
    {
        public string UrliFrame { get; set; }
        public string Token { get; set; }
        public string DocGroup { get; set; }
        public int PageNo { get; set; }

    }
}
