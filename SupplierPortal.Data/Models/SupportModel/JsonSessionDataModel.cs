﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel
{
    public partial class JsonSessionDataModel
    {
        public DataResult Data { get; set; }
    }

    public class DataResult
    {
        public string SessionName { get; set; }
        public string UserGUID { get; set; }
        public string PortalLoginID { get; set; }
        public string ModifiedUserID { get; set; }
        public string UrlLink { get; set; }
        public ActionMethod ActionMethod { get; set; }
        public string UpdateDate { get; set; }

    }

    public class ActionMethod
    {
        public string MethodForm { get; set; } // POST ,GET
        public List<ParameterData> ParameterData { get; set; }
    }

    public class ParameterData
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}
