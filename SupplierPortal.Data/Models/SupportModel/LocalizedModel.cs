﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel
{
    public class LocalizedModel
    {
        public int EntityID { get; set; }
        public string EntityValue { get; set; }
    }
}
