﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class ChangePasswordModels
    {
        [RequiredErrorMessageBase("_Profile.ChangePassword.RequireOldPassword")]
        [Remote("IsOldPasswordMatch", "Profile")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [RequiredErrorMessageBase("_Profile.ChangePassword.RequireNewPassword")]
        [RegularErrorMessage(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$", "_Profile.ChangePassword.ValidateMsg.InvalidPasswordPolicy")]
        //[StringLengthErrorMessage(100, "_Profile.ResetPassword.ValidateMsg.PasswordLength", 8)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [RequiredErrorMessageBase("_Profile.ChangePassword.ValidateMsg.RequireRePassword")]
        [DataType(DataType.Password)]
        [CompareErrorMessage("Password", "_Profile.ChangePassword.ValidateMsg.NotMatchPassword")]
        public string ConfirmPassword { get; set; }
    }
}
