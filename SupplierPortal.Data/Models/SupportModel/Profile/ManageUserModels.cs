﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class ManageUserModels
    {
        public string Proc { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireContactPerson")]
        public int ContactID { get; set; }
        public string UserUpdate { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireUserLogin")]
        [Remote("IsUsernameAvailableConfig", "Profile", AdditionalFields = "OrgID")]
        public string UserLoginID { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireEmail")]
        [Remote("IsEmailAvailableConfig", "Profile", AdditionalFields = "InitialEmail")]
        //[RegularErrorMessage(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?", "_Regis.Msg_EmailInvalid")]
        public string Email { get; set; }
        public string OrgID { get; set; }
        public string InitialEmail { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireTitleName")]
        public Nullable<int> TitleID { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireFirstName")]
        public string FirstName { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireLastName")]
        public string LastName { get; set; }
        public Nullable<int> JobTitleID { get; set; }
        public string OtherJobTitle { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireDepartment")]
        public string Department { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequirePhone")]
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string FaxExt { get; set; }


        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireTimeZone")]
        public string TimeZoneIDString { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireLocale")]
        public string LocaleName { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireCurrency")]
        public string CurrencyCode { get; set; }
        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireLanguage")]
        public string LanguageCode { get; set; }
        public bool IsPublic { get; set; }

        [RequiredErrorMessageBase("_Profile.ValidateMsg.RequireRole")]
        public int RoleID { get; set; }

        public IEnumerable<SystemRoleListModels> SystemRoleList { get; set; }

    }
}
