﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class API_UpdateUserModels
    {
        public RequestUpdate Request { get; set; }
    }

    public class RequestUpdate
    {
        public string ReqID { get; set; }
        public string APIKey { get; set; }
        public string APIName { get; set; }
        public DataUpdate Data { get; set; }

    }

    public class DataUpdate
    {
        public string Action { get; set; }
        public List<PortalLoginID> PortalLoginID { get; set; }
        public List<LoginID> LoginID { get; set; }
        public SupplierInfo SupplierInfo { get; set; }
        public OrgInfo OrgInfo { get; set; }
        public UserInfoUpdate UserInfo { get; set; }
        public UserContact UserContact { get; set; }
        public Address Address { get; set; }
        public AddBy EditBy { get; set; }
    }

    public class LoginID
    {
        public string value { get; set; }
        public bool IsPrimary { get; set; }
    }

    public class UserInfoUpdate
    {
        public string Locale { get; set; }
        public int Timezone { get; set; }
        public string CurrencyCode { get; set; }
        public string LanguageCode { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsDeleted { get; set; }
    }
}
