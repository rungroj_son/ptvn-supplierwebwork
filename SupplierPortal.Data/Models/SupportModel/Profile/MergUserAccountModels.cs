﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class MergUserAccountModels
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int UserID { get; set; }

    }
}
