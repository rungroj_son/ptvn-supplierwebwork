﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public class Tbl_OPNBuyerViewModels
    {
        public int ID { get; set; }
        public string BuyerOrgID { get; set; }
        public string BuyerName { get; set; }
        public Nullable<int> BuyerID { get; set; }
        public string InvitationCode { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<System.DateTime> ExpireDate { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
    }
}
