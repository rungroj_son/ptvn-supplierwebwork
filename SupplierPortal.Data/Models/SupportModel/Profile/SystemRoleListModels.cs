﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class SystemRoleListModels
    {
        public string Username { get; set; }
        public int SystemID { get; set; }
        public string SystemName { get; set; }
        public string Description { get; set; }
        public Nullable<int> SystemGrpID { get; set; }
        public Nullable<int> IsShowOnList { get; set; }
        public Nullable<int> IsShowRoleList { get; set; }
        public string API_URL { get; set; }
        public string API_Key { get; set; }
        public string API_Name { get; set; }
        //public string APIName_UpdateUser { get; set; }
        //public string APIName_UpdateSupplier { get; set; }

        //public IEnumerable<SystemRoles_sel_Result> RoleSelectList { get; set; }

        public int IsChecked { get; set; }
        public string SysRoleID { get; set; }
    }
}
