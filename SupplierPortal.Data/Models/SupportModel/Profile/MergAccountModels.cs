﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class MergAccountModels
    {
        public string MasterUsernameMerg { get; set; }
        public string UserMergList { get; set; }
        public bool Status { get; set; }
        public string Message { get; set; }
    }
}
