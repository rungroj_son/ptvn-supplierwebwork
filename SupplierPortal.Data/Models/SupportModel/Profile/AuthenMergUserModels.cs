﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class AuthenMergUserModels
    {
        public int CurrentUserID { get; set; }
        public string CurrentUsername { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string UserMerg { get; set; }
        public bool Status { get; set; }
        public string Message { get; set; }
        public string HtmlText { get; set; }
    }

    public partial class UserMergModels
    {
        public string UserMergList { get; set; }
    }
}
