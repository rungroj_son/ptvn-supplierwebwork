﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class BSP_UserPreferenceModels
    {
        public string UsernameCurrent { get; set; }
        public int UserID { get; set; }
        public int CheckedOP { get; set; }
        public string SysRoleID { get; set; }
        public string UrlLink { get; set; }

        public SystemRoleListModels SystemRole { get; set; }

    }
}
