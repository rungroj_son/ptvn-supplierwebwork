﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class SystemRoleModels
    {

        public int SystemID { get; set; }
        public string SystemName { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public Nullable<int> SystemGrpID { get; set; }
        public Nullable<int> SeqNo { get; set; }
        public Nullable<int> IsShowOnList { get; set; }
        public Nullable<int> IsShowRoleList { get; set; }
        public Nullable<int> IsActive { get; set; }
        public bool IsChecked { get; set; }


    }
}
