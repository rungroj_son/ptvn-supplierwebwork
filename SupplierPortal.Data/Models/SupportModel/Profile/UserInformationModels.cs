﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Profile
{
    public partial class UserInformationModels
    {
        public int UserID { get; set; }
        public string Username { get; set; }
        public string TitleContactName { get; set; }
        public string Fullname { get; set; }
        public int JobTitleID { get; set; }
        public string JobTitleName { get; set; }
        public string OtherJobTitle { get; set; }
        public string Tel { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public Nullable<int> OPChecked { get; set; }
        public Nullable<int> OPRoleID { get; set; }
        public string OPRoleName { get; set; }
    }
}
