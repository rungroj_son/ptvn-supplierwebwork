﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class CompanyCertificationModel
    {
        public string Proc { get; set; }

        public int SupplierID { get; set; }
        public Nullable<int> CertId { get; set; }
        [ResourceDisplayName("_ComPro.Certification.LblCertificate")]
        public Nullable<int> CertifiedID { get; set; }
        public int SeqNo { get; set; }
        public string OtherStandardName { get; set; }
        [ResourceDisplayName("_ComPro.Certification.LblValidFrom")]
        [RequiredErrorMessageBase("_ComPro.Certification.ValidateMsg.RequireValidFrom")]
        public string ValidFrom { get; set; }
        [ResourceDisplayName("_ComPro.Certification.LblValidTo")]
        [RequiredErrorMessageBase("_ComPro.Certification.ValidateMsg.RequireValidTo")]
        public string ValidTo { get; set; }

        public IList<Tbl_OrgCertifiedStdAttachment> OrgCertifiedStdAttachment { get; set; }
    }
}
