﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class OrgAttachmentListModels
    {
        public IEnumerable<CustomerDocNameModels> DocumentName { get; set; }
        public IList<TrackingAttachment> TrackingAttachment { get; set; }
        public int CompanyTypeID { get; set; }
    }

    public class TrackingAttachment
    {
        public Tbl_OrgAttachment OrgAttachment { get; set; }
        public Tbl_TrackingOrgAttachment TrackingOrgAttachment { get; set; }
        public int TrackingGrpID { get; set; }
        public bool isTracking { get; set; }
        public bool isShow { get; set; }
        public Nullable<int> TrackingReqID { get; set; }
        public Nullable<int> TrackingItemID { get; set; }
    }
}
