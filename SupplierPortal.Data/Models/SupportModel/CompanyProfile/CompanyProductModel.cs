﻿using SupplierPortal.Data.CustomValidate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class CompanyProductModel
    {
        public int SupplierID { get; set; }
        public string MainBusinessCode { get; set; }
        public string BusinessLongDes { get; set; }
        public List<BusinessTypeModel> OrgBusinessType { get; set; }
        public List<OrgProductGridViewModel> OrgProductList { get; set; }
        [ResourceDisplayName("_ComPro.ProductInfo.LblKeyword")]
        public string ProductKeyword { get; set; }
       
        //public string CategoryID_Lev_3 { get; set; }
        //public List<int> ProductsTypeID { get; set; }

        public List<ProductCategory> ProductCategory { get; set; }
    }

    public partial class BusinessTypeModel
    {
        public int BusinessTypeID { get; set; }
    }

    public partial class ProductCategory 
    {
        public int CategoryID_Lev_3 { get; set; }
        public List<int> ProductsTypeID { get; set; }
    }
}
