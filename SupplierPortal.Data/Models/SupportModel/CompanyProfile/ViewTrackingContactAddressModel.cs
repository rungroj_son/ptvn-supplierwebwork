﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class ViewTrackingContactAddressModel
    {
        public int TrackingReqID { get; set; }
        public int TrackingItemID { get; set; }
        public int AdressTypeID { get; set; }
        public string AdressType { get; set; }
        public int OldAddressID { get; set; }
        public int NewAddressID { get; set; }
        public IList<TrackingFieldCompanyProfileModel> AddressTrackingField { get; set; }
        public string OldAddress_Local { get; set; }
        public string NewAddress_Local { get; set; }
        public string OldAddress_Inter { get; set; }
        public string NewAddress_Inter { get; set; }
        public int TrackingStatusID { get; set; }
        public string TrackingStatusName { get; set; }
        
    }

}
