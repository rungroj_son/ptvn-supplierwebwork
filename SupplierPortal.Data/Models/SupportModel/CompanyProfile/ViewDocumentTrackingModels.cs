﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class ViewDocumentTrackingModels
    {
        public OrgAttachmentModel OldOrgAttachment { get; set; }
        public OrgAttachmentModel NewOrgAttachment { get; set; }

    }

    public class OrgAttachmentModel
    {
        public int SupplierID { get; set; }
        public int AttachmentID { get; set; }
        public string AttachmentName { get; set; }
        public string AttachmentNameUnique { get; set; }
        public string SizeAttach { get; set; }
        public Nullable<int> DocumentNameID { get; set; }
        public string DocumentName { get; set; }       
        public string OtherDocument { get; set; }
    }


}
