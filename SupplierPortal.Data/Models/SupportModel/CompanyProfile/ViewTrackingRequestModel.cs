﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class ViewTrackingRequestModel
    {
        public int TrackingReqID { get; set; }
        public int TrackingGrpID { get; set; }
        public string ResourceName { get; set; }
        public string TrackingGrpName { get; set; }
        public int SupplierID { get; set; }
        public int ReqBy { get; set; }
        public string ReqByTitleName { get; set; }
        public string ReqByFullname { get; set; }
        public int TrackingStatusID { get; set; }
        public string TrackingStatusName { get; set; }
        public Nullable<System.DateTime> ReqDate { get; set; }
    }
}
