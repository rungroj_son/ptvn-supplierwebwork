﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class ViewTrackingCompInfoModel
    {
        public int TrackingReqID { get; set; }
        public string TrackingKey { get; set; }
        public string ResourceName { get; set; }
        public string OldKeyValue { get; set; }
        public string NewKeyValue { get; set; }
        public int TrackingStatusID { get; set; }
        public string TrackingStatusName { get; set; }
        public int FieldID { get; set; }
    }
}
