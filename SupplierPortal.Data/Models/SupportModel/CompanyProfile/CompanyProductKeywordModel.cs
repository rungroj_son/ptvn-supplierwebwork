﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class CompanyProductKeywordModel
    {
        public int SupplierID { get; set; }
        public List<ProductKeywordModel> OrgProductKeyword { get; set; }
    }

    public partial class ProductKeywordModel
    {
        public string ProductKeyword { get; set; }
    }
}
