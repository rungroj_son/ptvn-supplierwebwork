﻿using SupplierPortal.Data.Models.CustomValidate;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    [Serializable()]
    public class CompanyReferenceCustomer
    {
        public int Id { get; set; }
        public int SupplierID { get; set; }
        public int SeqNo { get; set; }
        [RequiredErrorMessageBase("_ComPro.RefCustomer.ValidateMsg.RequireCompanyNameLocal")]
        public string CompanyName_Local { get; set; }
        [RequiredErrorMessageBase("_ComPro.RefCustomer.ValidateMsg.RequireCompanyNameInter")]
        public string CompanyName_Inter { get; set; }
        public Nullable<int> BusinessEntityID { get; set; }
        [RequiredErrorMessageBase("_ComPro.RefCustomer.ValidateMsg.RequireOtherBusinessEntity")]
        public string OtherBusinessEntity { get; set; }


        //public virtual Tbl_Organization Tbl_Organization { get; set; }

        
        //BusinessEntity
        public string BusinessEntity { get; set; }
        public string BusinessEntityDisplay { get; set; }
        public string Description { get; set; }
        public Nullable<int> CompanyTypeID { get; set; }

    }
}
