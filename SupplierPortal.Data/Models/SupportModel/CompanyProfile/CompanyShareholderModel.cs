﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    [Serializable()]
    public class CompanyShareholderModel
    {
        //shareholder

        public int Id { get; set; }
        public int SupplierID { get; set; }
        public int SeqNo { get; set; }
        [Required]
        public Nullable<int> TitleID { get; set; }
        [RequiredErrorMessageBase("_ComPro.Shareholder.ValidateMsg.RequireFirstName")]
        public string FirstName_Local { get; set; }
        [RequiredErrorMessageBase("_ComPro.Shareholder.ValidateMsg.RequireFirstName")]
        public string FirstName_Inter { get; set; }
        [RequiredErrorMessageBase("_ComPro.Shareholder.ValidateMsg.RequireLastName")]
        public string LastName_Local { get; set; }
        [RequiredErrorMessageBase("_ComPro.Shareholder.ValidateMsg.RequireLastName")]
        public string LastName_Inter { get; set; }
        [Required]
        public string CountryCode { get; set; }
        //[RequiredErrorMessageBase("_ComPro.Shareholder.ValidateMsg.RequireIdentityNo")]
        //[Remote("IdentityNoValid", "AdditionalInformation", AdditionalFields = "CountryCode_VI,Id")]
        public string IdentityNo { get; set; }

        //public int TitleID_Local { get; set; }

        public int TitleID_Inter { get; set; }

        public int CompanyTypeID { get; set; }

        //public virtual Tbl_Organization Tbl_Organization { get; set; }



        //NameTitle

        public string TitleName { get; set; }

        public string TitleName_Inter { get; set; }

        public virtual ICollection<Tbl_ContactPerson> Tbl_ContactPerson { get; set; }

        //Country
        public string CountryName { get; set; }
    }
}
