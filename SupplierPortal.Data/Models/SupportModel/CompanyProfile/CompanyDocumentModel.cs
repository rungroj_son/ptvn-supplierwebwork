﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class CompanyDocumentModel
    {
        public int SupplierID { get; set; }
        public string CountryCode { get; set; }
        public Nullable<int> CompanyTypeID { get; set; }
        public string CheckModify { get; set; }
        public Nullable<int> TrackingReqID { get; set; }
        public Nullable<int> TrackingItemID { get; set; }
        public string TrackingType { get; set; }
    }
}
