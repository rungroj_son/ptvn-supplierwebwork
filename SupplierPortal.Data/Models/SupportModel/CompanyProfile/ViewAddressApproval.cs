﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class ViewAddressApproval
    {
        public int ItemId { get; set; }
        public int AddressTypeId { get; set; }
        public string AddressTypeName { get; set; }
        public int TrackingStatusId { get; set; }
        public string TrackingStatusName { get; set; }
        public DateTime TrackingStatusDate { get; set; }
        public String Remark;
    }
}
