﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class CompanyProfileModel
    {
        public GeneralInfosModel GeneralInfosData { get; set; }
    }

    public partial class GeneralInfosModel
    {
        public int SupplierID { get; set; }
        public string OrgID { get; set; }
        public string CountryCode { get; set; }
        public string TaxID { get; set; }
        public string DUNSNumber { get; set; }
        public Nullable<int> CompanyTypeID { get; set; }
        public Nullable<int> BusinessEntityID { get; set; }
        public string OtherBusinessEntity { get; set; }
        public string CompName { get; set; }
        public string InvName { get; set; }
        public Nullable<int> BranchNo { get; set; }
        public string BranchName { get; set; }
        public Nullable<int> BusinessTypeID { get; set; }
        public string MainBusinessCode { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string FaxExt { get; set; }
        //Required(ErrorMessage = "wrong type url")]
        [Remote("WebsiteValidate", "ContactInformation")]
        public string WebSite { get; set; }

        [Remote("LatValidate", "ContactInformation")]
        public Nullable<double> Latitude { get; set; }
        [Remote("LongVadilate", "ContactInformation")]
        public Nullable<double> Longtitude { get; set; }
        //[Remote("YearEstablishValidate", "AdditionalInformation")]
        public string YearEstablished { get; set; }
        //[Remote("RegisCapitalValidate", "AdditionalInformation")]
        public Nullable<decimal> RegisteredCapital { get; set; }
        public string CurrencyCode { get; set; }
        //[Remote("NumberOfEMPValidate", "AdditionalInformation")]
        public Nullable<int> NumberOfEmp { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }

        public Nullable<int> isPTVNVerified { get; set; }
    }
}
