﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class ProductModel
    {
        public Nullable<int> ID { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
    }
}
