﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.CompanyProfile
{
    public partial class OrgProductGridViewModel
    {
        public int SupplierID { get; set; }
        public int ProdTypeID { get; set; }
        public string OldProdTypeID { get; set; }
        public string ProductType { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ActionLog { get; set; }
    }
}
