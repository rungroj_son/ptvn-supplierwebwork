﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.UAA
{
    public class UAAOuthResponse
    {
        public string username;
        public string password;
        public string salt;
        public string tenantid;
        public string rolename;
    }
}
