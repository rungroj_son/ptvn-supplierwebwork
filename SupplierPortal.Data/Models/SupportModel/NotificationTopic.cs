﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel
{
    public class NotificationTopic
    {
        public int TopicID { get; set; }
        public string TopicName { get; set; }
        public string Description { get; set; }
        public Nullable<int> GroupID { get; set; }
        public Nullable<int> isAddStopTimeWhenRead { get; set; }
        public Nullable<int> StopTimeAdding { get; set; }
        public string TopicStringResource { get; set; }
        public string TopicStringResource_Header { get; set; }
        public string TopicURL { get; set; }
        public string TopicURLParameters { get; set; }
        public Nullable<int> HeaderLength { get; set; }
    }
}
