﻿using System;
using System.Collections.Generic;


namespace SupplierPortal.Data.Models.SupportModel.AccountPortal
{
    public class MailModel
    {

        public string From { get; set; }
        public string To { get; set; }
        public string CcEmailAddress { get; set; }
        public string BccEmailAddress { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Host { get; set; }
        //public int Port { get; set; }
        //public string MailAccount { get; set; }
        //public string MailPassword { get; set; }

        

    }
}
