﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.SupportModel.AccountPortal.Register
{
    //TRS @14-11-2014 Helper Class for Register
    public class RegisterPortalModel
    {
        [ResourceDisplayName("_Regis.LblEmail")]
        [RequiredErrorMessageBase("_Regis.Msg_RequireEmail")]
        //[RegularErrorMessage(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?", "_Regis.Msg_EmailInvalid")]
        [Remote("IsEmailAvailableConfig", "RegisterPortal")]
        public string Email { get; set; }

        [ResourceDisplayName("_Regis.LblConfirmEmail")]
        [RequiredErrorMessageBase("_Regis.Msg_RequireConfirmEmail")]
        //[RegularErrorMessage(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?", "_Regis.Msg_EmailInvalid")]
        [CompareErrorMessage("Email", "_Regis.Msg_EmailNotMatch")]
        public string Confirmemail { get; set; }

        [ResourceDisplayName("_Regis.LblName")]
        [RequiredErrorMessageBase("_Regis.Msg_RequireFirstName")]
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [ResourceDisplayName("_Regis.LblCountry")]
        [RequiredErrorMessageBase("_Regis.Msg_RequireCountry")]
        public string CountryCode { get; set; }
        
        [ResourceDisplayName("_Regis.LblTaxID")]
        [RequiredErrorMessageBase("_Regis.Msg_RequireTaxID")]
        //[TaxIDValidate("_Regis.Msg_TaxIDFormat")]
        //[TaxIDInfosValidate("_Regis.Msg_TaxIDDuplicate_Reg_Await")]
        [Remote("ValidateTaxID", "RegisterPortal", AdditionalFields = "CountryCode_VI")]
        public string TaxID { get; set; }

        [ResourceDisplayName("_Regis.LblCompanyName")]
        [RequiredErrorMessageBase("_Regis.Msg_RequireCompanyName")]
        public string CompanyName { get; set; }

        [ResourceDisplayName("_Regis.LblBranchName")]
        public string BranchName { get; set; }

        [ResourceDisplayName("_Regis.LblBranchNo")]
        [Remote("ValidateBranchNo", "RegisterPortal", AdditionalFields = "CountryCode_VI")]
        public string BranchNo { get; set; }

        public int maxlength { get; set; }

        //[ResourceDisplayName("_Regis.LblEmail")]
        //[RequiredErrorMessageBase("_Regis.Msg_RequireEmail")]
        //[RegularErrorMessage(@"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?", "_Regis.Msg_EmailInvalid")]
        //public string ContactEmail { get; set; }

    }
    //-------------------------------------END---------------------------------------    

    public class ValidTaxID
    {
        [Required]
        public string TaxID { get; set; }

        public string CompanyName { get; set; }

        public string BranchNo { get; set; }

        public string BranchName { get; set; }

        public string RegID { get; set; }

        public string Message { get; set; }

        public string Alert { get; set; }

        public string SupplierID { get; set; }
    }

    public class TicketcodeRegContinue
    {
        [Required]
        public string Ticketcode { get; set; }
    }

    public class Topic
    {
        [Required]
        public string Body { get; set; }
    }
    //-------------------------------------END---------------------------------------
}
