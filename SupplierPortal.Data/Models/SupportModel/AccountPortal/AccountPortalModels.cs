﻿using SupplierPortal.Data.CustomValidate;
using SupplierPortal.Data.Models.CustomValidate;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SupplierPortal.Data.Models.SupportModel.AccountPortal
{
    public class LoginPortalModel
    {

        [RequiredErrorMessageBase("_Login.ValidateMsg.RequireEmail")]
        [ResourceDisplayName("_Login.TxtUsername")]
        public string Username { get; set; }

        [RequiredErrorMessageBase("_Login.ValidateMsg.RequirePassword")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }

    public class ForgotPasswordModel
    {

        [Display(Name = "Username")]
        [RequiredErrorMessageBase("_Login.ValidateMsg.RequireEmail")]
        public string Username { get; set; }

    }

    public class TokenModel
    {

        public string Username { get; set; }
        public string Token { get; set; }

    }

    public class CheckTimeoutModel
    {
        public string UserGuid { get; set; }
    }

    public class ResetPasswordModel
    {

        //[Required]
        public string Token { get; set; }

        [ResourceDisplayName("_Login.ResetPassword.LblPassword")]
        [RequiredErrorMessageBase("_Login.ResetPassword.ValidateMsg.RequirePassword")]
        [RegularErrorMessage(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$", "_Login.ResetPassword.ValidateMsg.PasswordPolicy")]
        [StringLengthErrorMessage(100, "_Login.ResetPassword.ValidateMsg.PasswordLength", 8)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [ResourceDisplayName("_Login.ResetPassword.LblConfirmPassword")]
        [RequiredErrorMessageBase("_Login.ResetPassword.ValidateMsg.RequireRePassword")]
        [DataType(DataType.Password)]
        [CompareErrorMessage("Password", "_Login.ResetPassword.ValidateMsg.PasswordNotMatch")]
        public string ConfirmPassword { get; set; }

    }

    public class LoginTrackingModel
    {

        public string UserGuid { get; set; }
        public string TrackingType { get; set; }
        public int TrackingReqID { get; set; }
        public int TrackingItemID { get; set; }
        public int TrackingGrpID { get; set; }
        public string ReturnUrl { get; set; }
        public string LanguageCode { get; set; }

    }
}
