﻿using System;
using System.Collections.Generic;

namespace SupplierPortal.Data.Models.SupportModel.LinkSystem
{
    public class Menu
    {
        public string MenuCaption { get; set; }
        public string MenuLink { get; set; }
        public string Icon { get; set; }
    }
}
