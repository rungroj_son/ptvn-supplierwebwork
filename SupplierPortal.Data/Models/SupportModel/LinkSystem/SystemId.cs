﻿using System;
using System.Collections.Generic;

namespace SupplierPortal.Data.Models.SupportModel.LinkSystem
{
    public class SysUserID
    {
        public string value { get; set; }
        public string SystemID { get; set; }
        public bool IsPrimary { get; set; }
    }
}
