﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Register
{
    public class ValidRegisModels
    {
        public string Proc { get; set; }
        public string RegID { get; set; }
        public string OldConcat { get; set; }
        public string TaxID { get; set; }
        public string CompanyName_Local { get; set; }
        public string CompanyName_Inter { get; set; }
        public string BranchNo { get; set; }
        public string Message { get; set; }
        public string DuplicateType { get; set; }
        public string SupplierID { get; set; }
        public string DuplicateCompanyName { get; set; }
        public bool Status { get; set; }
        public string TelCountryCode { get; set; }
        public string CountryCode { get; set; }
    }
}
