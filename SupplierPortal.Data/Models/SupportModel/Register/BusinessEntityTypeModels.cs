﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Register
{
    public class BusinessEntityTypeModels
    {
        public int BusinessEntityID { get; set; }
        public Nullable<int> CompanyTypeID { get; set; }
        public string BusinessEntity { get; set; }
        public Nullable<int> SeqNo { get; set; }
    }
}
