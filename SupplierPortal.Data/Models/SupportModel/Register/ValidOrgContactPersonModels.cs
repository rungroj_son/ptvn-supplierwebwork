﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Register
{
    public class ValidOrgContactPersonModels
    {
        public string hidSupplierID { get; set; }
        public string FirstName_Contact { get; set; }
        public string LastName_Contact { get; set; }
        public string hidService1 { get; set; }
        public string hidService2 { get; set; }
        public string hidInvitationCode { get; set; }
        public string hidBuyerOrgID { get; set; }
        //public string emailAdminBSP { get; set; }
        public string emailReturn { get; set; }
        public string DuplicateType { get; set; } // ประเภทที่ซ้ำ 0 = Message only, 1 = Send mail .Register.ContactBSPAdmin.ReqUser,2 = Send mail . Register.AdoptionNotify , 3 = Send mail .ForgotPassword 
        public string UsernameDuplicate { get; set; }
        public bool Status { get; set; }
        public string Message { get; set; }
    }
}
