﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Manual
{
    public partial class DocumentGroupModels
    {
        public int DocGrpID { get; set; }
        public string DocumentGrp { get; set; }
    }
}
