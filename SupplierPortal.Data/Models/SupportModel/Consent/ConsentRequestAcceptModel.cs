﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Consent
{
    public class ConsentRequestAcceptModel : ConsentBaseModel
    {
        public List<ConsentRequestAcceptPayloadModel> payload { get; set; }
    }
}
