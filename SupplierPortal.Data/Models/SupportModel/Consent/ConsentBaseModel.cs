﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Consent
{
    public class ConsentBaseModel
    {
        public List<ResultModel> result;

        public class ResultModel
        {
            public string type { get; set; }
            public string message { get; set; }
        }
    }
}
