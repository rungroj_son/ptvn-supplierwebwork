﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.Consent
{
    public class ConsentRequestAcceptPayloadModel
    {
        public string id { get; set; }
        public string projectId { get; set; }
        public dynamic privacyNotice { get; set; }
        public dynamic termsAndCondition { get; set; }
        public string privacyNoticeRef { get; set; }
        public int revision { get; set; }
        public string name { get; set; }
    }
}
