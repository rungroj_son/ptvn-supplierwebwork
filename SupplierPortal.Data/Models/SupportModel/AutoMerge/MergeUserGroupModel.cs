﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.SupportModel.AutoMerge
{
    public partial class MergeUserGroupReadyModel
    {
        public int MergeID { get; set; }
        public List<MergUserReadyModel> MergUserReady { get; set; }
        public bool isMerge { get; set; }
    }

    public partial class MergeUserGroupModel
    {
        public int MergeID { get; set; }
    }

}
