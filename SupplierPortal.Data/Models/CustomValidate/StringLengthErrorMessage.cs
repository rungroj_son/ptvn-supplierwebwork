﻿using SupplierPortal.Data.Models.Repository.LocaleStringResource;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.Web.Mvc;

namespace SupplierPortal.Data.CustomValidate
{
    public class StringLengthErrorMessage : StringLengthAttribute, IClientValidatable
    {

        private string _resourceValue = string.Empty;

        public StringLengthErrorMessage(int maximumLength, string resourceKey, int minimumLength)
            : base(maximumLength)
        {

            ResourceKey = resourceKey;
            MaximumLength = maximumLength;
            MinimumLength = minimumLength;
        }

        public string ResourceKey { get; set; }
        public int MaximumLength { get; set; }
        public int MinimumLength { get; set; }

        IEnumerable<ModelClientValidationRule> IClientValidatable.GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = null;
            _repo = new LocaleStringRepository();

            _resourceValue = _repo.GetResource(ResourceKey);

            yield return new ModelClientValidationStringLengthRule(_resourceValue, MinimumLength, MaximumLength);

        }
    }
}
