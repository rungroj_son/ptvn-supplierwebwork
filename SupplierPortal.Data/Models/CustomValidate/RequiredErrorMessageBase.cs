﻿using SupplierPortal.Data.Models.Repository.LocaleStringResource;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SupplierPortal.Data.Models.CustomValidate
{
    public class RequiredErrorMessageBase : RequiredAttribute, IClientValidatable
    {

        private string _resourceValue = string.Empty;

        public RequiredErrorMessageBase(string resourceKey)
            : base()
        {
            ResourceKey = resourceKey;
        }

        public string ResourceKey { get; set; }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ILocaleStringRepository _repo = null;
            _repo = new LocaleStringRepository();

            _resourceValue = _repo.GetResource(ResourceKey);


            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = _resourceValue;
            rule.ValidationType = "required";
            yield return rule;
        }


    }
}
