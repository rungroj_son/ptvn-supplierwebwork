﻿using SupplierPortal.Data.Models.Repository.LocaleStringResource;

namespace SupplierPortal.Data.CustomValidate
{
    public class ResourceDisplayName : System.ComponentModel.DisplayNameAttribute
    {

        private string _resourceValue = string.Empty;

        public ResourceDisplayName(string resourceKey)
            : base(resourceKey)
        {
            ResourceKey = resourceKey;
        }

        public string ResourceKey { get; set; }

        public override string DisplayName
        {
            get
           {
                ILocaleStringRepository _repo = null;
                _repo = new LocaleStringRepository();

                _resourceValue = _repo.GetResource(ResourceKey);
                //    _resourceValueRetrived = true;
                //}
                return _resourceValue;
            }
        }

        public string Name
        {
            get { return "ResourceDisplayName"; }
        }

    }
}
