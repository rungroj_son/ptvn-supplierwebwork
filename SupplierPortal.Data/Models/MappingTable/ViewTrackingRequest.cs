//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SupplierPortal.Data.Models.MappingTable
{
    using System;
    using System.Collections.Generic;
    
    public partial class ViewTrackingRequest
    {
        public int TrackingReqID { get; set; }
        public Nullable<int> SupplierID { get; set; }
        public Nullable<int> ReqBy { get; set; }
        public Nullable<int> TitleID { get; set; }
        public string FirstName_Local { get; set; }
        public string LastName_Local { get; set; }
        public string FirstName_Inter { get; set; }
        public string LastName_Inter { get; set; }
        public Nullable<System.DateTime> ReqDate { get; set; }
        public Nullable<int> TrackingStatusID { get; set; }
        public Nullable<int> TrackingGrpID { get; set; }
        public string TrackingGrpName { get; set; }
        public string ResourceName { get; set; }
        public Nullable<int> isDeleted { get; set; }
    }
}
