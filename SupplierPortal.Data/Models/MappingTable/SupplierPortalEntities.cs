﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.MappingTable
{
    public class TrackingDictionary : Dictionary<string, string>
    {

    }

    public partial class SupplierPortalEntities
    {
        public TrackingDictionary GetTrackingStatus<T>() where T : class
        {
            var result = new TrackingDictionary();
            var reflector = typeof(T);
            var tabelName = reflector.Name;
            foreach (var attr in reflector.GetProperties())
            {
                var filedName = attr.Name;
                var combindField = string.Format("{0}.{1}", tabelName, filedName);

                //Logic for find in database
                var value = this.Tbl_PortalFieldConfig.FirstOrDefault(f => f.Variable == combindField);

                //add to Dictionary
                if (value != null)
                { 
                    //result.Add(combindField, value.isTrackModify);
                    if (value.isTrackModify == 1)
                    {
                        result.Add(filedName,combindField);
                    }
                }
                   
               
            }
            return result;
        }
    }
}
