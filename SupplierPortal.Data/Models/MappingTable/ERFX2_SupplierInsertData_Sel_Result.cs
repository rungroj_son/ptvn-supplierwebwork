//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SupplierPortal.Data.Models.MappingTable
{
    using System;
    
    public partial class ERFX2_SupplierInsertData_Sel_Result
    {
        public string com_type { get; set; }
        public string org_name { get; set; }
        public string short_name { get; set; }
        public string tax_id { get; set; }
        public string branch_no { get; set; }
        public string contact_name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string fax { get; set; }
        public int language_id { get; set; }
        public Nullable<int> time_zone_id { get; set; }
        public string address { get; set; }
    }
}
