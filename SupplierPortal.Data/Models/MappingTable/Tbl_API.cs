//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SupplierPortal.Data.Models.MappingTable
{
    using System;
    using System.Collections.Generic;
    
    public partial class Tbl_API
    {
        public int APIId { get; set; }
        public string Name { get; set; }
        public string APIGroup { get; set; }
        public string API_URL { get; set; }
        public string API_Key { get; set; }
        public string API_Name { get; set; }
        public string Description { get; set; }
        public Nullable<int> isActive { get; set; }
        public Nullable<int> isCheckAPISecurity { get; set; }
    }
}
