﻿using System.Collections.Generic;

namespace SupplierPortal.Data.Models.Repository.LocaleStringResource
{
    public partial interface ILocaleStringRepository
    {

        string GetResource(string resourceKey);

        string GetResource(string resourceKey, string languageId);
        string getStringResourceByResourceName(string resourceName);
        void Dispose();
    }
}
