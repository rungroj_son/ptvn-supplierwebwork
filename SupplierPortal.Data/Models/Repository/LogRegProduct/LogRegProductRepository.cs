﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.LogRegProduct
{
    public partial class LogRegProductRepository:ILogRegProductRepository
    {
        private SupplierPortalEntities context;

        public LogRegProductRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogRegProductRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual bool Insert(int regid, int producttypeid, string productcode, string logAction)
        {
            try
            {
                var _regProduct = context.Tbl_RegProduct.FirstOrDefault(m => ((m.RegID == regid) && (m.ProductTypeID == producttypeid) && (m.ProductCode == productcode)));

                var _logRegProduct = new Tbl_LogRegProduct()
                {
                    RegID = _regProduct.RegID,
                    ProductTypeID = _regProduct.ProductTypeID,
                    ProductCode = _regProduct.ProductCode,
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };
                if (_logRegProduct != null)
                {
                    context.Tbl_LogRegProduct.Add(_logRegProduct);
                    context.SaveChanges();
                }

                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
