﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.LogRegProduct
{
    public partial interface ILogRegProductRepository
    {
        bool Insert(int regid, int producttypeid, string productcode, string logAction);

        void Dispose();
    }
}
