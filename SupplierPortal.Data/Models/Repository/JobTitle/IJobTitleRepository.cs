﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.JobTitle
{
    public partial interface IJobTitleRepository
    {
        IEnumerable<TempForMultiLang> GetJobTitleList(int languageID);


        string GetJobTitleNameByIdAndLanguageID(int jobTitleID, int languageID);

        void Dispose();

    }
}
