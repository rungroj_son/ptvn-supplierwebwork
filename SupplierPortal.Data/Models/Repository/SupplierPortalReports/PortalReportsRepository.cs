﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.SupplierPortalReports
{
    public partial class PortalReportsRepository : IPortalReportsRepository
    {

         private SupplierPortalEntities context;


        public PortalReportsRepository()
        {
            context = new SupplierPortalEntities();
        }

        public PortalReportsRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<Tbl_SupplierPortalReports> List()
        {
            return context.Tbl_SupplierPortalReports.ToList();
        }

        public virtual IEnumerable<Tbl_SupplierPortalReports> GetReportsListByUser(string username)
        {

            var query = from db in context.Tbl_SupplierPortalReports
                        join m in context.Tbl_UserReportMapping on db.ReportID equals m.ReportID
                        join b in context.Tbl_User on m.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username == username
                        select db;

           var result =  query.ToList();


           return result;
        }

        public void Dispose()
        {
            context.Dispose();
        }

    }
}
