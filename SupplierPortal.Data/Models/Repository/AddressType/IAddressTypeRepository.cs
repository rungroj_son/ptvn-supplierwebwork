﻿using System;
using System.Collections.Generic;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.AddressType
{
    public partial interface IAddressTypeRepository
    {

        IEnumerable<Tbl_AddressType> GetAddressTypeAll();

        void Dispose();
    }
}
