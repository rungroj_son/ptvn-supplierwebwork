﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.AddressType
{
    public partial class AddressTypeRepository : IAddressTypeRepository
    {

        private SupplierPortalEntities context;

        public AddressTypeRepository()
        {
            context = new SupplierPortalEntities();
        }

        public AddressTypeRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<Tbl_AddressType> GetAddressTypeAll()
        {
            var query = from a in context.Tbl_AddressType
                        select a;

            return query.ToList();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
