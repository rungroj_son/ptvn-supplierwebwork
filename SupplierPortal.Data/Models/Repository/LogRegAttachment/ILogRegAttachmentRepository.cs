﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.LogRegAttachment
{
    public partial interface ILogRegAttachmentRepository
    {

        void InsertLogAdd(int regID, string attachmentNameUnique, string logAction);

        void InsertLogDelete(int regID, int attachmentID, string logAction);

        void Dispose();
    }
}
