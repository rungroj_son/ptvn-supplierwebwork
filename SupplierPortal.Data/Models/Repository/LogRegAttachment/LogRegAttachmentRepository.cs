﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogRegAttachment
{
    public partial class LogRegAttachmentRepository : ILogRegAttachmentRepository
    {

        private SupplierPortalEntities context;

        public LogRegAttachmentRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogRegAttachmentRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual void InsertLogAdd(int regID, string attachmentNameUnique, string logAction)
        {
            try
            {
                var _regRegAttachmentTmp = context.Tbl_RegAttachment.FirstOrDefault(m => m.RegID == regID && m.AttachmentNameUnique == attachmentNameUnique);

                var _logRegAttachment = new Tbl_LogRegAttachment()
                {
                    RegID = _regRegAttachmentTmp.RegID,
                    AttachmentID = _regRegAttachmentTmp.AttachmentID,
                    AttachmentName = _regRegAttachmentTmp.AttachmentName,
                    AttachmentNameUnique = _regRegAttachmentTmp.AttachmentNameUnique,
                    SizeAttach = _regRegAttachmentTmp.SizeAttach,
                    DocumentNameID = _regRegAttachmentTmp.DocumentNameID,
                    OtherDocument = _regRegAttachmentTmp.OtherDocument,
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };
                if (_logRegAttachment != null)
                {
                    context.Tbl_LogRegAttachment.Add(_logRegAttachment);
                    context.SaveChanges();
                }


            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void InsertLogDelete(int regID, int attachmentID, string logAction)
        {
            try
            {
                var _regRegAttachmentTmp = context.Tbl_RegAttachment.FirstOrDefault(m => m.RegID == regID && m.AttachmentID == attachmentID);

                var _logRegAttachment = new Tbl_LogRegAttachment()
                {
                    RegID = _regRegAttachmentTmp.RegID,
                    AttachmentID = _regRegAttachmentTmp.AttachmentID,
                    AttachmentName = _regRegAttachmentTmp.AttachmentName,
                    AttachmentNameUnique = _regRegAttachmentTmp.AttachmentNameUnique,
                    SizeAttach = _regRegAttachmentTmp.SizeAttach,
                    DocumentNameID = _regRegAttachmentTmp.DocumentNameID,
                    OtherDocument = _regRegAttachmentTmp.OtherDocument,
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };
                if (_logRegAttachment != null)
                {
                    context.Tbl_LogRegAttachment.Add(_logRegAttachment);
                    context.SaveChanges();
                }


            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
