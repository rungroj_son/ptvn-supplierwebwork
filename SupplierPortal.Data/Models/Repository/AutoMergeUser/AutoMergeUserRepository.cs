﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.AutoMergeUser
{
    public partial class AutoMergeUserRepository : IAutoMergeUserRepository
    {

        private SupplierPortalEntities context;

        public AutoMergeUserRepository()
        {
            context = new SupplierPortalEntities();
        }

        public AutoMergeUserRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<Tbl_AutoMergeUser> GetAutoMergeUserSuccess()
        {
            var query = from a in context.Tbl_AutoMergeUser
                        where a.isSuccess != 0
                        && a.isPrimary == 1
                        select a.MergeID;

            var result = context.Tbl_AutoMergeUser.Where(m => query.Contains(m.MergeID)).ToList().OrderBy(m => m.MergeID).ThenByDescending(n => n.isPrimary);


            return result;
        }

        public virtual IEnumerable<Tbl_AutoMergeUser> GetAutoMergeUserUnSuccess()
        {
            var query = from a in context.Tbl_AutoMergeUser
                        where a.isSuccess == 0
                        && a.isPrimary == 1
                        select a.MergeID;

            var result = context.Tbl_AutoMergeUser.Where(m => query.Contains(m.MergeID)).ToList().OrderBy(m => m.MergeID).ThenByDescending(n => n.isPrimary);

            return result;
        }

        public virtual Tbl_AutoMergeUser GetAutoMergeUserByMergeIDAndMergeFromUserName(int mergeID, string mergeFromUserName)
        {
            return context.Tbl_AutoMergeUser.Where(m => m.MergeID == mergeID && m.MergeFromUserName == mergeFromUserName).FirstOrDefault();
        }

        public virtual void Update(Tbl_AutoMergeUser model)
        {
            if (model != null)
            {
                var tempModel = context.Tbl_AutoMergeUser.Where(m => m.MergeID == model.MergeID && m.MergeFromUserName == model.MergeFromUserName).FirstOrDefault();

                if (tempModel != null)
                {
                    tempModel = model;

                    context.SaveChanges();
                }
            }
        }

        public virtual IEnumerable<Tbl_AutoUpdateUser> GeAutoUpdateUserSuccess()
        {
            var result = context.Tbl_AutoUpdateUser.Where(m => m.isSuccess != 0).ToList();

            return result;
        }

        public virtual IEnumerable<Tbl_AutoUpdateUser> GeAutoUpdateUserUnSuccess()
        {
            var result = context.Tbl_AutoUpdateUser.Where(m => m.isSuccess == 0).ToList();

            return result;
        }

        public virtual void UpdateAutoUpdateUser(Tbl_AutoUpdateUser model)
        {
            if (model != null)
            {
                var tempModel = context.Tbl_AutoUpdateUser.Where(m => m.ID == model.ID && m.Username == model.Username).FirstOrDefault();

                if (tempModel != null)
                {
                    tempModel = model;

                    context.SaveChanges();
                }
            }
        }

        public virtual Tbl_AutoUpdateUser GetAutoUpdateUserByID(int id)
        {
            return context.Tbl_AutoUpdateUser.Where(m => m.ID == id).FirstOrDefault();
        }
    }
}
