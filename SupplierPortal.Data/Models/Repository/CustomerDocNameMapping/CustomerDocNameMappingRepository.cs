﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.CustomerDocNameMapping
{
    public partial class CustomerDocNameMappingRepository : ICustomerDocNameMappingRepository
    {

        private SupplierPortalEntities context;

        public CustomerDocNameMappingRepository()
        {
            context = new SupplierPortalEntities();
        }

        public CustomerDocNameMappingRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual bool CheckCustomerDocNameIsRequire(int documentNameID, int companyTypeID ,string countryCode)
        {

            bool chk = false;

            if (countryCode == "TH")
            {
                var query = from a in context.Tbl_CustomerDocNameMapping
                            where a.DocumentNameID == documentNameID
                            && a.CompanyTypeID == companyTypeID
                            && a.isTHShowOnList == 1
                            && a.isTHRequire == 1
                            select a;

                var result = query.ToList();

                if (result.Count() > 0)
                {
                    chk = true;
                }
            }else
            {
                var query = from a in context.Tbl_CustomerDocNameMapping
                            where a.DocumentNameID == documentNameID
                            && a.CompanyTypeID == companyTypeID
                            && a.isNonTHShowOnList == 1
                            && a.isNonTHRequire == 1
                            select a;

                var result = query.ToList();

                if (result.Count() > 0)
                {
                    chk = true;
                }
            }

            return chk;
        }


        public virtual IEnumerable<Tbl_CustomerDocNameMapping> GetCustomerDocNameMappingRequireByCompTypeAndCountryCode(int companyTypeID, string countryCode)
        {

            if (countryCode == "TH")
            {
                var query = from a in context.Tbl_CustomerDocNameMapping
                            where a.CompanyTypeID == companyTypeID
                            && a.isTHShowOnList == 1
                            && a.isTHRequire == 1
                            select a;

                var result = query.ToList();
                return result;

            }
            else
            {
                var query = from a in context.Tbl_CustomerDocNameMapping
                            where a.CompanyTypeID == companyTypeID
                            && a.isNonTHShowOnList == 1
                            && a.isNonTHRequire == 1
                            select a;

                var result = query.ToList();
                return result;

            }
        }

        public virtual IEnumerable<Tbl_CustomerDocNameMapping> GetDocumentIsRequire(int documentTypeID, int companyTypeID, string countryCode)
        {

            if (countryCode == "TH")
            {
                var query = from a in context.Tbl_CustomerDocNameMapping
                            join b in context.Tbl_CustomerDocName on a.DocumentNameID equals b.DocumentNameID into c
                            from d in c.DefaultIfEmpty()
                            where d.DocumentTypeID == documentTypeID
                            && a.CompanyTypeID == companyTypeID
                            && a.isTHShowOnList == 1
                            && a.isTHRequire == 1
                            select a;

                var result = query.ToList();

                return result;
            }
            else
            {
                var query = from a in context.Tbl_CustomerDocNameMapping
                            join b in context.Tbl_CustomerDocName on a.DocumentNameID equals b.DocumentNameID into c
                            from d in c.DefaultIfEmpty()
                            where d.DocumentTypeID == documentTypeID
                            && a.CompanyTypeID == companyTypeID
                            && a.isNonTHShowOnList == 1
                            && a.isNonTHRequire == 1
                            select a;

                var result = query.ToList();

                return result;
            }

           
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
