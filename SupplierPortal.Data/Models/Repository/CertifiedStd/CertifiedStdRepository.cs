﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.CertifiedStd
{
    public partial class CertifiedStdRepository : ICertifiedStdRepository
    {

        private SupplierPortalEntities context;

        public CertifiedStdRepository()
        {
            context = new SupplierPortalEntities();
        }

        public CertifiedStdRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<Tbl_CertifiedStd> GetCertifiedStdAll()
        {
            var query = from a in context.Tbl_CertifiedStd
                        select a;

            return query.ToList();
        }

        public virtual Tbl_CertifiedStd GetCertifiedStdByCertifiedID(int certifiedID)
        {
            var query = context.Tbl_CertifiedStd.Where(m => m.CertifiedID == certifiedID).FirstOrDefault();

            return query;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
