﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.CertifiedStd
{
    public partial interface ICertifiedStdRepository
    {
        IEnumerable<Tbl_CertifiedStd> GetCertifiedStdAll();

        Tbl_CertifiedStd GetCertifiedStdByCertifiedID(int certifiedID);

        void Dispose();
    }
}
