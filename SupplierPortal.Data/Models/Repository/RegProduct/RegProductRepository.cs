﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LogRegProduct;

namespace SupplierPortal.Data.Models.Repository.RegProduct
{
    public partial class RegProductRepository:IRegProductRepository
    {
        private SupplierPortalEntities context;

        public RegProductRepository()
        {
            context = new SupplierPortalEntities();
        }

        public RegProductRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<Tbl_RegProduct> GetRegProductByRegID(int regid)
        {
            return (from list in context.Tbl_RegProduct
                    where list.RegID == regid
                    select list).ToList<Tbl_RegProduct>();
        }


        public virtual IEnumerable<ProductTypeAddModel> GetProductTypeByRegIDAndProductCode(int regID, string productCode)
        {
            var query = from db in context.Tbl_RegProduct
                        where db.RegID == regID
                        && db.ProductCode == productCode
                        select new ProductTypeAddModel
                        {
                            ProductTypeID = db.ProductTypeID
                        };
            var result = query.ToList();

            return result; 
        }

        public virtual IEnumerable<Tbl_RegProduct> GetRegProductByMany(int regid, int producttypeid, string productcode)
        {
            return (from list in context.Tbl_RegProduct
                    where list.RegID == regid
                    && list.ProductTypeID == producttypeid
                    && list.ProductCode == productcode
                    select list).ToList<Tbl_RegProduct>();
        }

        public virtual IEnumerable<RegProductGridViewModel> GetMultiLangRegProductByMany(int regid, int languageID)
        {
            var query = from RPD in context.Tbl_RegProduct
                        where RPD.RegID == regid
                        join PDT in context.Tbl_ProductType on RPD.ProductTypeID equals PDT.ProductTypeID into ps
                        from ds in ps.DefaultIfEmpty()
                        join PD in context.Tbl_Product on RPD.ProductCode equals PD.ProductCode into pg
                        from dt in pg.DefaultIfEmpty()

                        select new RegProductGridViewModel
                        {
                            ProdTypeID = RPD.ProductTypeID,
                            ProductCode = RPD.ProductCode,
                            ProductType = ((from PDTP in context.Tbl_LocalizedProperty
                                            where PDTP.EntityID == ds.ProductTypeID
                                            && PDTP.LanguageID == languageID
                                            && PDTP.LocaleKeyGroup == "Tbl_ProductType"
                                            && PDTP.LocaleKey == "ProductType"
                                            select PDTP.LocaleValue
                                       )).FirstOrDefault() ?? ds.ProductType,
                            ProductName = ((from PDP in context.Tbl_LocalizedProperty
                                            where PDP.EntityID == dt.Id
                                            && PDP.LanguageID == languageID
                                            && PDP.LocaleKeyGroup == "Tbl_Product"
                                            && PDP.LocaleKey == "ProductName"
                                            select PDP.LocaleValue
                                       )).FirstOrDefault() ?? dt.ProductName,
                        };
            return query.ToList<RegProductGridViewModel>();
        }

        public virtual bool Insert(int regid, int producttypeid, string productcode)
        {
            try
            {
                var _regProduct = new Tbl_RegProduct()
                {
                    RegID = regid,
                    ProductTypeID = producttypeid,
                    ProductCode = productcode
                };
                if (_regProduct != null)
                {
                    context.Tbl_RegProduct.Add(_regProduct);
                    context.SaveChanges();

                    ILogRegProductRepository _logRegProduct = new LogRegProductRepository();
                    _logRegProduct.Insert(_regProduct.RegID, _regProduct.ProductTypeID, _regProduct.ProductCode, "Add");
                    _logRegProduct.Dispose();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual bool Delete(int regid, int producttypeid, string productcode)
        {
            try
            {
                var _regProduct = context.Tbl_RegProduct.Where(m => m.RegID == regid && m.ProductCode == productcode && m.ProductTypeID == producttypeid).FirstOrDefault();
                if (_regProduct != null)
                {
                    context.Tbl_RegProduct.Remove(_regProduct);                   

                    ILogRegProductRepository _logRegProduct = new LogRegProductRepository();
                    _logRegProduct.Insert(_regProduct.RegID, _regProduct.ProductTypeID, _regProduct.ProductCode, "Delete");
                    _logRegProduct.Dispose();

                    context.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual bool Delete(int regid, string productcode)
        {
            try
            {
                var _regProduct = context.Tbl_RegProduct.Where(m => m.RegID == regid && m.ProductCode == productcode).ToList();
                if (_regProduct.Count > 0)
                {
                    foreach (var itemlist in _regProduct)
                    {
                        context.Tbl_RegProduct.Remove(itemlist);

                        ILogRegProductRepository _logRegProduct = new LogRegProductRepository();
                        _logRegProduct.Insert(itemlist.RegID, itemlist.ProductTypeID, itemlist.ProductCode, "Delete");
                    }
                    //context.Tbl_RegProduct.Remove(_regProduct);

                    //ILogRegProductRepository _logRegProduct = new LogRegProductRepository();
                    //_logRegProduct.Insert(_regProduct.RegID, _regProduct.ProductTypeID, _regProduct.ProductCode, "Delete");
                    //_logRegProduct.Dispose();

                    context.SaveChanges();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }



        public virtual bool Update(int regid,int oldProducttypeid, int producttypeid, string productcode)
        {
            try
            {
                var _regProduct = context.Tbl_RegProduct.Where(m => m.RegID == regid && m.ProductCode == productcode && m.ProductTypeID == oldProducttypeid).FirstOrDefault();
                if (_regProduct != null)
                {
                    var productModel = new Tbl_RegProduct
                    {
                        RegID = regid,
                        ProductTypeID = producttypeid,
                        ProductCode = productcode

                    };

                    context.Tbl_RegProduct.Add(productModel);

                    context.Tbl_RegProduct.Remove(_regProduct);

                    context.SaveChanges();

                    ILogRegProductRepository _logRegProduct = new LogRegProductRepository();
                    _logRegProduct.Insert(productModel.RegID, productModel.ProductTypeID, productModel.ProductCode, "Update");
                    _logRegProduct.Dispose();


                    
                }
                
                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual bool RegProductExists(int regid, string productCode)
        {
            var count = context.Tbl_RegProduct.Count(m => m.RegID == regid && m.ProductCode == productCode);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual bool RegProductExists(int regid, string productCode, int producttypeid)
        {
            var count = context.Tbl_RegProduct.Count(m => m.RegID == regid && m.ProductCode == productCode && m.ProductTypeID == producttypeid);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
