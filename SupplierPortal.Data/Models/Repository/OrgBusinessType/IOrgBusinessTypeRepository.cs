﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.OrgBusinessType
{
    public partial interface IOrgBusinessTypeRepository
    {
        IEnumerable<Tbl_OrgBusinessType> GetOrgBusinessTypeBySupplierID(int supplierID);

        void Insert(Tbl_OrgBusinessType tbl_OrgBusinessType,int updateBy);

        void Delete(int supplierID, int businessTypeID, int updateBy);

        bool OrgBusinessTypeExists(int supplierID, int businessTypeID);

        void Dispose();
    }
}
