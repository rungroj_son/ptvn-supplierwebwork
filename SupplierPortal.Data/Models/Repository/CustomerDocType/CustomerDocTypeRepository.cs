﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using System.Web;
using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.Repository.CustomerDocName;

namespace SupplierPortal.Data.Models.Repository.CustomerDocType
{
    public partial class CustomerDocTypeRepository : ICustomerDocTypeRepository
    {
        private SupplierPortalEntities context;

        public CustomerDocTypeRepository()
        {
            context = new SupplierPortalEntities();
        }

        public CustomerDocTypeRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<TempForMultiLang> GetMultiLangCustomerDocTypeByLangid(int languageID)
        {
            ILocalizedPropertyRepository _repo = new LocalizedPropertyRepository();
            var result = _repo.GetAllEntityidLocalizedPropByMany("Tbl_CustomerDocType", "DocumentName", languageID);
            _repo.Dispose();

            if (result.Count() > 0)
            {
                return (from list in result
                        select new TempForMultiLang
                        {
                            EntityID = list.EntityID,
                            EntityValue = list.LocaleValue,
                        }).ToList<TempForMultiLang>();
            }
            else
            {
                return (from list in context.Tbl_CustomerDocType
                        select new TempForMultiLang
                        {
                            EntityID = list.DocumentTypeID,
                            EntityValue = list.DocumentType,
                        }).ToList<TempForMultiLang>();
            }
        }

        public virtual string GetMultiLangCustomerDocTypeByDocumentTypeID(int documentTypeID, int languageID)
        {
            ILocalizedPropertyRepository _repo = new LocalizedPropertyRepository();
            var result = _repo.GetAllEntityidLocalizedPropByMany("Tbl_CustomerDocType", "DocumentName", languageID);
            _repo.Dispose();

            if (result.Count() > 0)
            {
                return (result.Where(i => i.EntityID == documentTypeID).Select(i => i.LocaleValue)).FirstOrDefault();
            }

            return (context.Tbl_CustomerDocType.Where(i => i.DocumentTypeID == documentTypeID).Select(i => i.DocumentType)).FirstOrDefault();
        }

        public virtual IEnumerable<DocumentTypeModels> GetDocumentTypeByCompanyTypeID(int companyTypeID)
        {

            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            //var query = from db in context.Tbl_CustomerDocType
            //            join ac in context.Tbl_CustomerDocTypeMapping on db.DocumentTypeID equals ac.DocumentTypeID
            //            where ac.CompanyTypeID == companyTypeID 
            //            orderby db.DocumentTypeID
            //            select new DocumentTypeModels
            //            {
            //                DocumentTypeID = db.DocumentTypeID,
            //                DocumentType = (from lp in context.Tbl_LocalizedProperty
            //                               where lp.EntityID == db.DocumentTypeID
            //                               && lp.LocaleKeyGroup == "Tbl_CustomerDocType"
            //                               && lp.LocaleKey == "DocumentName"
            //                               && lp.LanguageID == languageId
            //                               select lp.LocaleValue).FirstOrDefault() ?? db.DocumentType,
            //                isRequire = ac.isRequire??0
                                          

            //            };

            //var result = query.ToList();
            var query = (from a in context.Tbl_CustomerDocType
                        join b in context.Tbl_CustomerDocName on a.DocumentTypeID equals b.DocumentTypeID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_CustomerDocNameMapping on d.DocumentNameID equals e.DocumentNameID into f
                        from g in f.DefaultIfEmpty()
                        where g.CompanyTypeID == companyTypeID
                        select new DocumentTypeModels
                        {
                            DocumentTypeID = a.DocumentTypeID,
                            DocumentType = (from lp in context.Tbl_LocalizedProperty
                                            where lp.EntityID == a.DocumentTypeID
                                            && lp.LocaleKeyGroup == "Tbl_CustomerDocType"
                                            && lp.LocaleKey == "DocumentName"
                                            && lp.LanguageID == languageId
                                            select lp.LocaleValue).FirstOrDefault() ?? a.DocumentType,
                            SeqNo = a.SeqNo??0,
                            CompanyTypeID = companyTypeID

                        }).Distinct().OrderBy(x => x.SeqNo);
            var result = query.ToList();

            return result;

        }

        public virtual IEnumerable<DocumentTypeModels> GetDocumentTypeByCompanyTypeIDAndCountrycode(int companyTypeID, string countryCode)
        {

            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            if (countryCode == "TH")
            {
                var query = (from a in context.Tbl_CustomerDocType
                             join b in context.Tbl_CustomerDocName on a.DocumentTypeID equals b.DocumentTypeID into c
                             from d in c.DefaultIfEmpty()
                             join e in context.Tbl_CustomerDocNameMapping on d.DocumentNameID equals e.DocumentNameID into f
                             from g in f.DefaultIfEmpty()
                             where g.CompanyTypeID == companyTypeID
                             && g.isTHShowOnList == 1
                             select new DocumentTypeModels
                             {
                                 DocumentTypeID = a.DocumentTypeID,
                                 DocumentType = (from lp in context.Tbl_LocalizedProperty
                                                 where lp.EntityID == a.DocumentTypeID
                                                 && lp.LocaleKeyGroup == "Tbl_CustomerDocType"
                                                 && lp.LocaleKey == "DocumentName"
                                                 && lp.LanguageID == languageId
                                                 select lp.LocaleValue).FirstOrDefault() ?? a.DocumentType,
                                 SeqNo = a.SeqNo ?? 0,
                                 CompanyTypeID = companyTypeID,
                                 CountryCode = countryCode

                             }).Distinct().OrderBy(x => x.SeqNo);
                var result = query.ToList();

                return result;

            }else
            {

                var query = (from a in context.Tbl_CustomerDocType
                             join b in context.Tbl_CustomerDocName on a.DocumentTypeID equals b.DocumentTypeID into c
                             from d in c.DefaultIfEmpty()
                             join e in context.Tbl_CustomerDocNameMapping on d.DocumentNameID equals e.DocumentNameID into f
                             from g in f.DefaultIfEmpty()
                             where g.CompanyTypeID == companyTypeID
                             && g.isNonTHShowOnList == 1
                             select new DocumentTypeModels
                             {
                                 DocumentTypeID = a.DocumentTypeID,
                                 DocumentType = (from lp in context.Tbl_LocalizedProperty
                                                 where lp.EntityID == a.DocumentTypeID
                                                 && lp.LocaleKeyGroup == "Tbl_CustomerDocType"
                                                 && lp.LocaleKey == "DocumentName"
                                                 && lp.LanguageID == languageId
                                                 select lp.LocaleValue).FirstOrDefault() ?? a.DocumentType,
                                 SeqNo = a.SeqNo ?? 0,
                                 CompanyTypeID = companyTypeID,
                                 CountryCode = countryCode

                             }).Distinct().OrderBy(x => x.SeqNo);
                var result = query.ToList();

                return result;
            }
        }


        public virtual bool CheckDocumentTypeIsRequire(int documentTypeID,int companyTypeID, string countryCode)
        {
            bool chk = false;

            var docName = from a in context.Tbl_CustomerDocName
                              where a.DocumentTypeID == documentTypeID
                              select a.DocumentNameID;

            var listDocName = docName.ToList();

            if (countryCode == "TH")
            {
                var query = from a in context.Tbl_CustomerDocNameMapping
                            where a.CompanyTypeID == companyTypeID
                            && (listDocName).Contains(a.DocumentNameID)
                            && a.isTHShowOnList == 1
                            && a.isTHRequire == 1
                            select a;

                if (query.Count() > 0)
                {
                    chk = true;
                }

                return chk;
            }else
            {

                var query = from a in context.Tbl_CustomerDocNameMapping
                           where a.CompanyTypeID == companyTypeID
                           && (listDocName).Contains(a.DocumentNameID)
                           && a.isNonTHShowOnList == 1
                           && a.isNonTHRequire == 1
                           select a;

                if (query.Count() > 0)
                {
                    chk = true;
                }

                return chk;
            }

            //var query = from a in context.Tbl_CustomerDocNameMapping
            //            where a.CompanyTypeID == companyTypeID
            //            && (listDocName).Contains(a.DocumentNameID)
            //            && a.isRequire == 1
            //            select a;

            //if (query.Count() > 0)
            //{
            //    chk = true;
            //}

            //return chk;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
