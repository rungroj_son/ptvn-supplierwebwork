﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.CustomerDocType
{
    public partial interface ICustomerDocTypeRepository
    {
        IEnumerable<TempForMultiLang> GetMultiLangCustomerDocTypeByLangid(int languageID);

        string GetMultiLangCustomerDocTypeByDocumentTypeID(int documentTypeID, int languageID);

        IEnumerable<DocumentTypeModels> GetDocumentTypeByCompanyTypeID(int companyTypeID);

        IEnumerable<DocumentTypeModels> GetDocumentTypeByCompanyTypeIDAndCountrycode(int companyTypeID, string countryCode);

        //IEnumerable<Tbl_CustomerDocTypeMapping> GetDocumentTypeForRemove(int companyTypeID);

        bool CheckDocumentTypeIsRequire(int documentTypeID, int companyTypeID, string countryCode);

        void Dispose();
    }
}
