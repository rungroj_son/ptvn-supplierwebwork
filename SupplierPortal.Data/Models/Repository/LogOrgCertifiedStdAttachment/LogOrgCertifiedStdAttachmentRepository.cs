﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgCertifiedStdAttachment
{
    public partial class LogOrgCertifiedStdAttachmentRepository : ILogOrgCertifiedStdAttachmentRepository
    {

        private SupplierPortalEntities context;


        public LogOrgCertifiedStdAttachmentRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogOrgCertifiedStdAttachmentRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual void Insert(int certId, int attachmentID, string logAction, int updateBy)
        {
            try
            {
                var tempModel = context.Tbl_OrgCertifiedStdAttachment.Where(m => m.CertId == certId && m.AttachmentID == attachmentID).FirstOrDefault();

                if (tempModel != null)
                {
                    var _logModel = new Tbl_LogOrgCertifiedStdAttachment()
                    {
                        CertId = tempModel.CertId,
                        AttachmentID = tempModel.AttachmentID,
                        AttachmentName = tempModel.AttachmentName,
                        AttachmentNameUnique = tempModel.AttachmentNameUnique,
                        SizeAttach = tempModel.SizeAttach,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgCertifiedStdAttachment.Add(_logModel);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
