﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.APISecurity
{
    public partial interface IAPISecurityRepository
    {

        bool CheckAPISecurityByNameAndIpAddress(string name,string ip_Address);

        void Dispose();
    }
}
