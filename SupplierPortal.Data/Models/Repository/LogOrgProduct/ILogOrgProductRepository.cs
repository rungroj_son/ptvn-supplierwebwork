﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgProduct
{
    public partial interface ILogOrgProductRepository
    {
        void Insert(int supplierID, int productTypeID, string productCode, string logAction, int updateBy);

        void Dispose();
    }
}
