﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using SupplierPortal.Data.Models.Repository.RegEmailTicket;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LogRegInfo;
using SupplierPortal.Data.Models.Repository.RegBusinessType;
using SupplierPortal.Data.Models.Repository.LogRegBusinessType;
using SupplierPortal.Data.Models.Repository.RegContactAddressMapping;
using SupplierPortal.Data.Models.Repository.RegHistory;
using SupplierPortal.Data.Helper;

namespace SupplierPortal.Data.Models.Repository.RegInfo
{
    public partial class RegInfoRepository : IRegInfoRepository
    {
        private SupplierPortalEntities context;

        ILogRegBusinessTypeRepository _logRegBusinessTypeRepository;
        IRegBusinessTypeRepository _regBusinessTypeRepository;
        ILocalizedPropertyRepository _localizedPropertyRepository;
        IRegContactAddressMappingRepository _regContactAddressMappingRepository;
        IRegHistoryRepository _regHistoryRepository;
        ILogRegInfoRepository _logRegInfoRepository;

        public RegInfoRepository()
        {
            context = new SupplierPortalEntities();
            _logRegBusinessTypeRepository = new LogRegBusinessTypeRepository();
            _regBusinessTypeRepository = new RegBusinessTypeRepository();
            _localizedPropertyRepository = new LocalizedPropertyRepository();
            _regContactAddressMappingRepository = new RegContactAddressMappingRepository();
            _regHistoryRepository = new RegHistoryRepository();
            _logRegInfoRepository = new LogRegInfoRepository();


        }

        public RegInfoRepository(
            SupplierPortalEntities context,
            LogRegBusinessTypeRepository logRegBusinessTypeRepository,
            RegBusinessTypeRepository regBusinessTypeRepository,
            LocalizedPropertyRepository localizedPropertyRepository,
            RegContactAddressMappingRepository regContactAddressMappingRepository,
            RegHistoryRepository regHistoryRepository,
            LogRegInfoRepository logRegInfoRepository
            )
        {
            this.context = context;
            _logRegBusinessTypeRepository = logRegBusinessTypeRepository;
            _regBusinessTypeRepository = regBusinessTypeRepository;
            _localizedPropertyRepository = localizedPropertyRepository;
            _regContactAddressMappingRepository = regContactAddressMappingRepository;
            _regHistoryRepository = regHistoryRepository;
            _logRegInfoRepository = logRegInfoRepository;
        }

        public virtual bool Insert(RegisterPortalModel model, string ticketcode, string languageCode)
        {
            try
            {
                IRegEmailTicketRepository _regemlrepo = new RegEmailTicketRepository();
                _regemlrepo.Insert(model, ticketcode, languageCode);
                _regemlrepo.Dispose();

                var _regInfo = new Tbl_RegInfo()
                {
                    RegStatusID = 1,
                    TaxID = model.TaxID ?? "",
                    CountryCode = model.CountryCode ?? "",
                    //FirstName = model.FirstName ?? "",
                    //LastName = model.LastName??"",
                    TicketCode = ticketcode ?? "",
                    CompanyName_Local = model.CompanyName ?? "",
                    CompanyName_Inter = "",
                    BranchNo = model.BranchNo ?? "",
                    BranchName_Local = model.BranchName ?? "",
                    BranchName_Inter = "",
                    CompanyAddrID = 0,
                    DeliveredAddrID = 0,
                    DUNSNumber = "",
                    PhoneNo = "",
                    PhoneExt = "",
                    MobileNo = "",
                    FaxNo = "",
                    FaxExt = "",
                    WebSite = ""
                };
                if (_regInfo != null)
                {
                    context.Tbl_RegInfo.Add(_regInfo);
                    context.SaveChanges();

                    ILogRegInfoRepository _logRegInfoRepo = new LogRegInfoRepository();
                    _logRegInfoRepo.Insert(_regInfo.RegID, "Add");
                    _logRegInfoRepo.Dispose();
                }

                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public virtual int InsertReturnRegID(Tbl_RegInfo model)
        {
            try
            {
                int regID = 0;
                if (model != null)
                {
                    context.Tbl_RegInfo.Add(model);
                    context.SaveChanges();

                    regID = model.RegID;


                    _logRegInfoRepository.Insert(regID, "Add");
                    //_logRegInfoRepository.Dispose();

                    var regHistory = new Tbl_RegHistory()
                    {
                        RegID = regID,
                        RegStatusID = model.RegStatusID ?? 0,
                        Remark = "",
                        UpdateBy = 1,
                        UpdateDate = DateTime.UtcNow
                    };
                    _regHistoryRepository.Insert(regHistory);
                    //_regHistoryRepository.Dispose();
                }

                return regID;
            }
            catch (Exception e)
            {
                throw;
            }

        }

        public virtual void UpdateRegInfo(Tbl_RegInfo model)
        {
            try
            {

                if (model != null)
                {
                    bool isChangedStatus = false;

                    var regInfoTemp = context.Tbl_RegInfo.Find(model.RegID);

                    if (model.RegStatusID != regInfoTemp.RegStatusID)
                    {
                        isChangedStatus = true;
                    }

                    regInfoTemp = model;
                    context.SaveChanges();

                    _logRegInfoRepository.Insert(regInfoTemp.RegID, "Modify");
                    //_logRegInfoRepository.Dispose();

                    if (isChangedStatus)
                    {
                        var regHistory = new Tbl_RegHistory()
                        {
                            RegID = model.RegID,
                            RegStatusID = model.RegStatusID ?? 0,
                            Remark = "",
                            UpdateBy = 1,
                            UpdateDate = DateTime.UtcNow
                        };
                        _regHistoryRepository.Insert(regHistory);
                        //_regHistoryRepository.Dispose();
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        private void ManageDataLocalizedProperty(IList<LocalizedPropModel> localizedModel, int entityIDUpdate, int entityIDInsert)
        {
            try
            {
                foreach (var itemList in localizedModel)
                {
                    var localized = _localizedPropertyRepository.GetEntityidLocalizedProp(entityIDUpdate, itemList.LocaleKeyGroup, itemList.LocaleKey, itemList.LanguageID ?? 0);

                    if (localized != null)
                    {
                        if (localized.LocaleValue != itemList.LocaleValue)
                        {
                            _localizedPropertyRepository.UpdateLocalized(entityIDUpdate, itemList.LanguageID, itemList.LocaleKeyGroup, itemList.LocaleKey, itemList.LocaleValue);
                        }
                    }
                    else
                    {
                        _localizedPropertyRepository.InsertLocalized(entityIDInsert, itemList.LanguageID, itemList.LocaleKeyGroup, itemList.LocaleKey, itemList.LocaleValue);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual IEnumerable<Tbl_RegInfo> GetRegInfoAll()
        {
            return (from list in context.Tbl_RegInfo select list).ToList<Tbl_RegInfo>();
        }

        public virtual IEnumerable<Tbl_RegInfo> GetRegInfoListByTaxID(string taxid)
        {
            return (from list in context.Tbl_RegInfo
                    where list.TaxID == taxid
                    select list).ToList<Tbl_RegInfo>();
        }

        public virtual IEnumerable<Tbl_RegInfo> GetRegInfoListByTicketcode(string ticketcode)
        {
            return (from list in context.Tbl_RegInfo
                    where list.TicketCode == ticketcode
                    select list).ToList<Tbl_RegInfo>();
        }

        public virtual IEnumerable<Tbl_RegInfo> GetRegInfoByRegstatusid(int regstatusid)
        {
            return (from list in context.Tbl_RegInfo
                    where list.RegStatusID == regstatusid
                    select list).ToList<Tbl_RegInfo>();
        }

        public virtual IEnumerable<Tbl_RegInfo> GetRegInfoByRegID(int regid)
        {
            return (from list in context.Tbl_RegInfo
                    where list.RegID == regid
                    select list).ToList<Tbl_RegInfo>();
        }

        public virtual IEnumerable<Tbl_RegInfo> GetRegInfoByInfosConcat(string infosconcat)
        {
            var query1 = from list in context.Tbl_RegInfo
                         where (list.TaxID.ToString() + list.CompanyName_Local + list.BranchNo + list.BranchName_Local).ToUpper() == infosconcat
                         && list.isDeleted != 1
                         select list;

            var result1 = query1.ToList();

            if (result1.Count() > 0)
            {
                return result1;
            }

            var query2 = from list in context.Tbl_RegInfo
                         where (list.TaxID.ToString() + list.CompanyName_Inter + list.BranchNo + list.BranchName_Inter).ToUpper() == infosconcat
                         select list;

            var result2 = query2.ToList();

            return result2;
        }

        public virtual bool CheckDuplicateRegInfo(string infosconcat)
        {
            bool chk = false;

            var query1 = from a in context.Tbl_RegInfo
                         where (a.TaxID.ToString() + a.CompanyName_Local + a.BranchNo).ToUpper() == infosconcat
                         && a.RegStatusID != 1
                         && a.isDeleted != 1
                         select a;

            var result1 = query1.ToList();

            if (result1.Count() > 0)
            {
                return true;
            }

            var query2 = from a in context.Tbl_RegInfo
                         where (a.TaxID.ToString() + a.CompanyName_Inter + a.BranchNo).ToUpper() == infosconcat
                         && a.RegStatusID != 1
                         select a;

            var result2 = query2.ToList();

            if (result2.Count() > 0)
            {
                return true;
            }

            return chk;
        }

        public virtual bool CheckDuplicateRegInfoVerifyString(string infosconcat, string oldRegID)
        {
            bool chk = false;
            int countRegID = 0;


            var query = from a in context.Tbl_RegInfo
                        where a.RegStatusID != 1
                        && a.isDeleted != 1
                        select a;

            var result = query.ToList();

            var resultChk1 = from a in result
                             where (StringHelper.GetMakeVerifyString(a.TaxID.ToString() + a.CompanyName_Local + a.BranchNo).ToUpper()) == infosconcat
                             select a;

            if (!string.IsNullOrEmpty(oldRegID))
            {
                int regID = Convert.ToInt32(oldRegID);
                countRegID = resultChk1.Where(m => m.RegID != regID).Count();
            }
            else
            {
                countRegID = resultChk1.Count();
            }
            if (countRegID > 0)
            {
                return true;
            }

            var resultChk2 = from a in result
                             where (StringHelper.GetMakeVerifyString(a.TaxID.ToString() + a.CompanyName_Inter + a.BranchNo).ToUpper()) == infosconcat
                             select a;

            countRegID = 0;
            if (!string.IsNullOrEmpty(oldRegID))
            {
                int regID = Convert.ToInt32(oldRegID);
                countRegID = resultChk2.Where(m => m.RegID != regID).Count();
            }
            else
            {
                countRegID = resultChk2.Count();
            }
            if (countRegID > 0)
            {
                return true;
            }

            return chk;
        }

        public virtual bool CheckDuplicateRegisterFromConnect(string taxID, string branchNumber, string countryCode)
        {
            bool result = false;
            try
            {
                var query = from a in context.Tbl_RegInfo
                            where a.TaxID.Equals(taxID)
                            && a.BranchNo.Equals(branchNumber)
                            && a.CountryCode.Equals(countryCode)
                            select a;

                if (query.Any())
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public virtual Tbl_RegInfo GetRegInfoByTicketcode(string ticketcode)
        {
            var query = from a in context.Tbl_RegInfo
                            //where ticketcode.Equals(a.TicketCode, StringComparison.InvariantCulture)
                        where a.isDeleted != 1
                        select a;
            var qlist = query.ToList();
            var result = qlist.Where(x => x.TicketCode == ticketcode).FirstOrDefault();

            return result;
        }

        public virtual bool CheckDuplicateRegInfoByTaxIDAndBranchNo(string taxID, string branchNo, string oldRegID)
        {
            bool check = false;

            var query = from a in context.Tbl_RegInfo
                        where a.TaxID.Trim() == taxID.Trim()
                        && a.BranchNo.Trim() == branchNo.Trim()
                        && a.isDeleted != 1
                        select a;

            int countRegID = 0;
            if (!string.IsNullOrEmpty(oldRegID))
            {
                int regID = Convert.ToInt32(oldRegID);
                var result = query.Where(m => m.RegID != regID).ToList();
                countRegID = result.Count();
            }
            else
            {
                var result = query.ToList();
                countRegID = result.Count();
            }
            if (countRegID > 0)
            {
                return true;
            }

            return check;
        }

        public virtual Tbl_RegInfo GetRegInfoByTaxIDAndBranchNo(string taxID, string branchNo)
        {
            var query = from a in context.Tbl_RegInfo
                        where a.TaxID.Trim() == taxID.Trim()
                        && a.BranchNo.Trim() == branchNo.Trim()
                        && a.isDeleted != 1
                        select a;

            var result = query.FirstOrDefault();

            return result;
        }

        public virtual List<Tbl_RegInfo> GetRegInfoByRegListAndStatusList(List<int> listRegId, List<int> listStatusId)
        {
            var query = from a in context.Tbl_RegInfo
                        where listRegId.Contains(a.RegID)
                         && listStatusId.Contains((int)a.RegStatusID)
                        && a.isDeleted != 1
                        select a;

            var result = query.ToList();

            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
