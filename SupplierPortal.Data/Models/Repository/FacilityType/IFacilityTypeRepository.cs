﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.FacilityType
{
    public partial interface IFacilityTypeRepository
    {
        IEnumerable<Tbl_FacilityType> GetFacilityList();

        Tbl_FacilityType GetFacilityByFacilityID(int FacilityID);

        IEnumerable<Tbl_FacilityType> GetFacilityListByFacilityID(int FacilityID);

        string GetFacilityTypeByFacilityID(int FacilityID);

        IEnumerable<TempForMultiLang> GetMultiLFacilityList();

        IEnumerable<Tbl_FacilityType> GetLocalizeFacilityList();

        void Dispose();
    }
}
