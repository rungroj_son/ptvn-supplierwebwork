﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogNotification
{
    public partial class LogNotificationRepository : ILogNotificationRepository
    {
        private SupplierPortalEntities context;

        public LogNotificationRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogNotificationRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(int notificationID, string logAction, int updateBy)
        {
            try
            {
                var _notification = context.Tbl_Notification.Where(m => m.NotificationID == notificationID).FirstOrDefault();

                if (_notification != null)
                {
                    var _logNotification = new Tbl_LogNotification()
                    {
                        NotificationID = _notification.NotificationID,
                        NotificationKey = _notification.NotificationKey,
                        SystemID = _notification.SystemID,
                        TopicID = _notification.TopicID,
                        UserID = _notification.UserID,
                        RawData = _notification.RawData,
                        //FormattedData = _notification.FormattedData,
                        StopTime = _notification.StopTime,
                        isRead = _notification.isRead,
                        isDeleted = _notification.isDeleted,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction,
                    };

                    context.Tbl_LogNotification.Add(_logNotification);
                    context.SaveChanges();
                }

            }
            catch (Exception e)
            {
                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
