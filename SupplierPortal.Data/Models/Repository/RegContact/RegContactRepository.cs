﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LogRegContact;

namespace SupplierPortal.Data.Models.Repository.RegContact
{
    public partial class RegContactRepository : IRegContactRepository
    {
        private SupplierPortalEntities context;
        ILogRegContactRepository _logRegContactRepository; 

        public RegContactRepository()
        {
            context = new SupplierPortalEntities();
            _logRegContactRepository = new LogRegContactRepository();
        }

        public RegContactRepository(
            SupplierPortalEntities context,
            LogRegContactRepository logRegContactRepository
            )
        {
            this.context = context;
            _logRegContactRepository = logRegContactRepository;
        }

        public virtual IEnumerable<Tbl_RegContact> GetRegContactByContactid(int contactid)
        {
            return (from list in context.Tbl_RegContact
                    where list.ContactID == contactid
                    select list).ToList<Tbl_RegContact>();
        }

        public virtual int InsertReturnContactID(Tbl_RegContact model)
        {
            try
            {
                int contactID = 0;

                if (model != null)
                {
                    context.Tbl_RegContact.Add(model);
                    context.SaveChanges();

                    contactID = model.ContactID;

                    _logRegContactRepository.Insert(contactID, "Add");
                    //_logRegContactRepository.Dispose();
                }

                return contactID;
            }
            catch (Exception e)
            {

                throw;
            }
        }


        public virtual void UpdateRegContact(Tbl_RegContact model)
        {
            try
            {

                if (model != null)
                {

                    var regContactTemp = context.Tbl_RegContact.Find(model.ContactID);
                    regContactTemp = model;
                    context.SaveChanges();

                    _logRegContactRepository.Insert(regContactTemp.ContactID, "Modify");
                    _logRegContactRepository.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void InsertContact(RegisContinuePortalModel _model, out int _contactid)
        {
            int _Contactid = 0;
            try
            {
                var _regContact = new Tbl_RegContact()
                    {
                        TitleID = _model.RegContact_TitleID,
                        FirstName_Local = _model.RegContact_FirstName_Local,
                        FirstName_Inter = _model.RegContact_FirstName_Inter,
                        LastName_Local = _model.RegContact_LastName_Local,
                        LastName_Inter = _model.RegContact_LastName_Inter,
                        JobTitleID = _model.RegContact_JobTitleID,
                        Department = _model.RegContact_Department,
                        PhoneNo = _model.RegContact_PhoneNo,
                        PhoneExt = _model.RegContact_PhoneExt,
                        FaxNo = _model.RegContact_FaxNo,
                        FaxExt = _model.RegContact_FaxExt,
                        MobileNo = _model.RegContact_MobileNo,
                        Email = _model.RegContact_Email
                    };
                if (_regContact != null)
                {
                    context.Tbl_RegContact.Add(_regContact);
                    context.SaveChanges();

                    _Contactid = _regContact.ContactID;

                    ILogRegContactRepository _logRegContactRepo = new LogRegContactRepository();
                    _logRegContactRepo.Insert(_Contactid, "Add");
                    _logRegContactRepo.Dispose();
                }
                _contactid = _Contactid;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual bool UpdateContact(RegisContinuePortalModel _model, int _contactid)
        {
            try
            {
                var model = context.Tbl_RegContact.FirstOrDefault(m => m.ContactID == _contactid);
                model.TitleID = _model.RegContact_TitleID;
                model.FirstName_Local = _model.RegContact_FirstName_Local;
                model.FirstName_Inter = _model.RegContact_FirstName_Inter;
                model.LastName_Local = _model.RegContact_LastName_Local;
                model.LastName_Inter = _model.RegContact_LastName_Inter;
                model.JobTitleID = _model.RegContact_JobTitleID;
                model.Department = _model.RegContact_Department;
                model.PhoneNo = _model.RegContact_PhoneNo;
                model.PhoneExt = _model.RegContact_PhoneExt;
                model.FaxNo = _model.RegContact_FaxNo;
                model.FaxExt = _model.RegContact_FaxExt;
                model.MobileNo = _model.RegContact_MobileNo;
                model.Email = _model.RegContact_Email;
                if (model != null)
                {
                    context.Entry(model).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();

                    ILogRegContactRepository _logRegContactRepo = new LogRegContactRepository();
                    _logRegContactRepo.Insert(model.ContactID, "Modify");
                    _logRegContactRepo.Dispose();
                }

                return true;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
