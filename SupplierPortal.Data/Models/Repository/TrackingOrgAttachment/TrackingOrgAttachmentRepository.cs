﻿using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SupplierPortal.Data.Models.Repository.LogTrackingOrgAttachment;

namespace SupplierPortal.Data.Models.Repository.TrackingOrgAttachment
{
    public partial class TrackingOrgAttachmentRepository : ITrackingOrgAttachmentRepository
    {
        private SupplierPortalEntities context;
        ILogTrackingOrgAttachmentRepository _logTrackingOrgAttachmentRepository;

        public TrackingOrgAttachmentRepository()
        {
            context = new SupplierPortalEntities();
            _logTrackingOrgAttachmentRepository = new LogTrackingOrgAttachmentRepository();
        }

        public TrackingOrgAttachmentRepository(
            SupplierPortalEntities ctx,
            LogTrackingOrgAttachmentRepository logTrackingOrgAttachmentRepository
            )
        {
            this.context = ctx;
            _logTrackingOrgAttachmentRepository = logTrackingOrgAttachmentRepository;
        }

        public virtual Tbl_TrackingOrgAttachment GetTrackingOrgAttachmentWaitingBySupplierIDAndDocumentNameID(int supplierID, int documentNameID)
        {
            var query = from a in context.Tbl_TrackingOrgAttachment
                        join b in context.Tbl_TrackingRequest on a.TrackingReqID equals b.TrackingReqID 
                        where b.SupplierID == supplierID
                        && a.DocumentNameID == documentNameID
                        && a.TrackingStatusID == 2
                        select a;

            var result = query.FirstOrDefault();

            return result;
        }

        public virtual Tbl_TrackingOrgAttachment GetTrackingOrgAttachmentWaitingForApprove(int supplierID, int documentNameID, int attachmentID)
        {
            var query = from a in context.Tbl_TrackingOrgAttachment
                        join b in context.Tbl_TrackingRequest on a.TrackingReqID equals b.TrackingReqID
                        where b.SupplierID == supplierID
                        && a.DocumentNameID == documentNameID
                        && a.TrackingStatusID == 2
                        && (a.OldAttachmentID == attachmentID || a.NewAttachmentID == attachmentID)
                        select a;

            var result = query.FirstOrDefault();

            return result;
        }

        public virtual Tbl_TrackingOrgAttachment GetTrackingOrgAttachmentByTrackingItemID(int trackingItemID)
        {
            var result = context.Tbl_TrackingOrgAttachment.Where(m => m.TrackingItemID == trackingItemID).FirstOrDefault();

            return result;
        }

        public virtual void Insert(Tbl_TrackingOrgAttachment model, int updateBy)
        {
            try
            {
                if (model != null)
                {
                    context.Tbl_TrackingOrgAttachment.Add(model);
                    context.SaveChanges();
                }

                _logTrackingOrgAttachmentRepository.Insert(model.TrackingItemID, updateBy, "Add");
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual IEnumerable<Tbl_TrackingOrgAttachment> GetTrackingOrgAttachmentByTrackingReqID(int trackingReqID)
        {
            var query = from a in context.Tbl_TrackingOrgAttachment
                        where a.TrackingReqID == trackingReqID
                        select a;

            return query.ToList();
        }

        public virtual IEnumerable<ViewTrackingCompAffidavitModel> GetViewTrackingCompanyAffidavitByTrackingReqID(int trackingReqID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from a in context.Tbl_TrackingOrgAttachment
                        join b in context.Tbl_OrgAttachment on a.OldAttachmentID equals b.AttachmentID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_OrgAttachment on a.NewAttachmentID equals e.AttachmentID into f
                        from g in f.DefaultIfEmpty()
                        join h in context.Tbl_TrackingStatus on a.TrackingStatusID equals h.TrackingStatusID into i
                        from j in i.DefaultIfEmpty()
                        join k in context.Tbl_CustomerDocName on a.DocumentNameID equals k.DocumentNameID into l
                        from m in l.DefaultIfEmpty()
                        where a.TrackingReqID == trackingReqID
                        select new ViewTrackingCompAffidavitModel
                        {
                            TrackingReqID = a.TrackingReqID,
                            OldAttachmentID = a.OldAttachmentID ?? 0,
                            OldAttachment = d,
                            NewAttachmentID = a.NewAttachmentID ?? 0,
                            NewAttachment = g,
                            TrackingStatusID = a.TrackingStatusID ?? 0,
                            TrackingStatusName = (from lp in context.Tbl_LocalizedProperty
                                                  where lp.EntityID == a.TrackingStatusID
                                                  && lp.LocaleKeyGroup == "Tbl_TrackingStatus"
                                                  && lp.LocaleKey == "TrackingStatusName"
                                                  && lp.LanguageID == languageId
                                                  select lp.LocaleValue).FirstOrDefault() ?? j.TrackingStatusName,
                            DocumentNameID = a.DocumentNameID??0,
                            DocumentName = (from lp in context.Tbl_LocalizedProperty
                                            where lp.EntityID == a.DocumentNameID
                                           && lp.LocaleKeyGroup == "Tbl_CustomerDocName"
                                           && lp.LocaleKey == "DocumentName"
                                           && lp.LanguageID == languageId
                                            select lp.LocaleValue).FirstOrDefault() ?? m.DocumentName,
                        };

            var result = query.ToList();

            return result;
        }

        public virtual IEnumerable<ViewTrackingCompAffidavitModel> GetViewTrackingCompanyAffidavitByTrackingItemID(int trackingItemID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from a in context.Tbl_TrackingOrgAttachment
                        join b in context.Tbl_OrgAttachment on a.OldAttachmentID equals b.AttachmentID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_OrgAttachment on a.NewAttachmentID equals e.AttachmentID into f
                        from g in f.DefaultIfEmpty()
                        join h in context.Tbl_TrackingStatus on a.TrackingStatusID equals h.TrackingStatusID into i
                        from j in i.DefaultIfEmpty()
                        join k in context.Tbl_CustomerDocName on a.DocumentNameID equals k.DocumentNameID into l
                        from m in l.DefaultIfEmpty()
                        where a.TrackingItemID == trackingItemID
                        select new ViewTrackingCompAffidavitModel
                        {
                            TrackingReqID = a.TrackingReqID,
                            OldAttachmentID = a.OldAttachmentID ?? 0,
                            OldAttachment = d,
                            NewAttachmentID = a.NewAttachmentID ?? 0,
                            NewAttachment = g,
                            TrackingStatusID = a.TrackingStatusID ?? 0,
                            TrackingStatusName = (from lp in context.Tbl_LocalizedProperty
                                                  where lp.EntityID == a.TrackingStatusID
                                                  && lp.LocaleKeyGroup == "Tbl_TrackingStatus"
                                                  && lp.LocaleKey == "TrackingStatusName"
                                                  && lp.LanguageID == languageId
                                                  select lp.LocaleValue).FirstOrDefault() ?? j.TrackingStatusName,
                            DocumentNameID = a.DocumentNameID ?? 0,
                            DocumentName = (from lp in context.Tbl_LocalizedProperty
                                            where lp.EntityID == a.DocumentNameID
                                           && lp.LocaleKeyGroup == "Tbl_CustomerDocName"
                                           && lp.LocaleKey == "DocumentName"
                                           && lp.LanguageID == languageId
                                            select lp.LocaleValue).FirstOrDefault() ?? m.DocumentName,
                        };

            var result = query.ToList();

            return result;
        }

        public virtual Tbl_TrackingOrgAttachment GetTrackingOrgAttachmentWaitingByTrackingReqIDAndDocumentNameID(int trackingReqID, int documentNameID)
        {
            var query = from a in context.Tbl_TrackingOrgAttachment
                        where a.TrackingReqID == trackingReqID
                        && a.DocumentNameID == documentNameID
                        && a.TrackingStatusID == 2
                        select a;

            return query.FirstOrDefault();
        }

        public virtual void Update(Tbl_TrackingOrgAttachment model, int updateBy)
        {
            try
            {
                if (model != null)
                {
                    var tempModel = context.Tbl_TrackingOrgAttachment.Where(m => m.TrackingReqID == model.TrackingReqID && m.TrackingItemID == model.TrackingItemID).FirstOrDefault();
                    if (tempModel != null)
                    {
                        tempModel = model;
                        context.SaveChanges();

                        _logTrackingOrgAttachmentRepository.Insert(tempModel.TrackingItemID, updateBy, "Modify");
                    }
                }

            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Delete(Tbl_TrackingOrgAttachment model, int updateBy)
        {
            try
            {
                if (model != null)
                {
                    var tempModel = context.Tbl_TrackingOrgAttachment.Where(m => m.TrackingReqID == model.TrackingReqID && m.TrackingItemID == model.TrackingItemID).FirstOrDefault();
                    if (tempModel != null)
                    {
                        _logTrackingOrgAttachmentRepository.Insert(model.TrackingItemID, updateBy, "Delete");

                        context.Tbl_TrackingOrgAttachment.Remove(tempModel);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual Tbl_TrackingOrgAttachment GetTrackingOrgAttachmentWaitingByAttachmentID(int attachmentID, int trackingReqID, int documentNameID)
        {
            var query = from a in context.Tbl_TrackingOrgAttachment
                        where (a.OldAttachmentID == attachmentID || a.NewAttachmentID == attachmentID)
                        && a.TrackingReqID == trackingReqID
                        && a.TrackingStatusID == 2
                        select a;

            return query.FirstOrDefault();
        }

        public virtual IEnumerable<Tbl_TrackingOrgAttachment> GetTrackingOrgAttachmentWaitingBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID)
        {

            var query = from a in context.Tbl_TrackingOrgAttachment
                        join b in  context.Tbl_CustomerDocName on a.DocumentNameID equals b.DocumentNameID
                        join c in context.Tbl_TrackingRequest on a.TrackingReqID equals c.TrackingReqID
                        where c.SupplierID == supplierID
                        && b.DocumentTypeID == documentTypeID
                        && c.SupplierID == supplierID
                        && a.TrackingStatusID == 2
                        select a;


            var result = query.ToList();

            return result;

        }

        public virtual IEnumerable<Tbl_TrackingOrgAttachment> GetTrackingOrgAttachmentByItemId(List<int> itemIdList)
        {

            var query = from a in context.Tbl_TrackingOrgAttachment
                        join b in context.Tbl_TrackingStatus on a.TrackingStatusID equals b.TrackingStatusID 
                        where itemIdList.Contains(a.TrackingItemID)
                        select a;


            var result = query.ToList();

            return result;

        }

        public virtual IEnumerable<ViewAttachmentApproval> GetViewAttachmentByItemId(List<int> itemIdList)
        {

            var query = from a in context.Tbl_TrackingOrgAttachment
                        join b in context.Tbl_TrackingStatus on a.TrackingStatusID equals b.TrackingStatusID
                        where itemIdList.Contains(a.TrackingItemID)
                        select new ViewAttachmentApproval
                        {
                            ItemId = a.TrackingItemID,
                            TrackingStatusId = (int) a.TrackingStatusID,
                            TrackingStatusName = b.TrackingStatusName,
                            Remark = a.Remark,
                            TrackingStatusDate = (DateTime) a.TrackingStatusDate

                        };


            var result = query.ToList();

            return result;

        }

        public virtual IEnumerable<Tbl_TrackingOrgAttachment> GetTrackingOrgAttachmentNonCancelByTrackingReqID(int trackingReqID)
        {
            var query = from a in context.Tbl_TrackingOrgAttachment
                        where a.TrackingStatusID != 4
                        && a.TrackingReqID == trackingReqID
                        select a;

            return query.ToList();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
