﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;

namespace SupplierPortal.Data.Models.Repository.TrackingOrgAttachment
{
    public partial interface ITrackingOrgAttachmentRepository
    {
        Tbl_TrackingOrgAttachment GetTrackingOrgAttachmentWaitingBySupplierIDAndDocumentNameID(int supplierID, int documentNameID);

        Tbl_TrackingOrgAttachment GetTrackingOrgAttachmentWaitingForApprove(int supplierID, int documentNameID, int attachmentID);

        Tbl_TrackingOrgAttachment GetTrackingOrgAttachmentByTrackingItemID(int trackingItemID);

        void Insert(Tbl_TrackingOrgAttachment model, int updateBy);

        IEnumerable<Tbl_TrackingOrgAttachment> GetTrackingOrgAttachmentByTrackingReqID(int trackingReqID);

        IEnumerable<ViewTrackingCompAffidavitModel> GetViewTrackingCompanyAffidavitByTrackingReqID(int trackingReqID);

        IEnumerable<ViewTrackingCompAffidavitModel> GetViewTrackingCompanyAffidavitByTrackingItemID(int trackingItemID);

        Tbl_TrackingOrgAttachment GetTrackingOrgAttachmentWaitingByTrackingReqIDAndDocumentNameID(int trackingReqID, int documentNameID);

        void Update(Tbl_TrackingOrgAttachment model, int updateBy);

        void Delete(Tbl_TrackingOrgAttachment model, int updateBy);

        Tbl_TrackingOrgAttachment GetTrackingOrgAttachmentWaitingByAttachmentID(int attachmentID, int trackingReqID, int documentNameID);

        IEnumerable<Tbl_TrackingOrgAttachment> GetTrackingOrgAttachmentWaitingBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID);

        IEnumerable<Tbl_TrackingOrgAttachment> GetTrackingOrgAttachmentByItemId(List<int> itemIdList);

        IEnumerable<ViewAttachmentApproval> GetViewAttachmentByItemId(List<int> itemIdList);

        IEnumerable<Tbl_TrackingOrgAttachment> GetTrackingOrgAttachmentNonCancelByTrackingReqID(int trackingReqID);

        void Dispose();
    }
}
