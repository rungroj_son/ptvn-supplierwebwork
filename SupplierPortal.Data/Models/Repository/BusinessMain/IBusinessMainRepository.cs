﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.BusinessMain
{
    public partial interface IBusinessMainRepository
    {
        IEnumerable<TempForMultiLangStrID> GetMultiLangBusinessMainByLangid(int languageID);

        string GetBusinessLongDesByBusinessCodeAndLanguageID(string businessCode, int languageID);

        void Dispose();
    }
}
