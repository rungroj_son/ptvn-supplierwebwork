﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;

namespace SupplierPortal.Data.Models.Repository.BusinessMain
{
    public partial class BusinessMainRepository : IBusinessMainRepository
    {
        private SupplierPortalEntities context;

        public BusinessMainRepository()
        {
            context = new SupplierPortalEntities();
        }

        public BusinessMainRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<TempForMultiLangStrID> GetMultiLangBusinessMainByLangid(int languageID)
        {

            var query = from a in context.Tbl_BusinessMain
                        select new TempForMultiLangStrID
                        {
                            EntityID = a.BusinessCode,
                            EntityValue = (from ml in context.Tbl_LocalizedProperty
                                           where ml.EntityID.ToString() == a.BusinessCode
                                           && ml.LocaleKeyGroup == "Tbl_BusinessMain"
                                           && ml.LocaleKey == "BusinessLongDes"
                                           && ml.LanguageID == languageID
                                           select ml.LocaleValue).FirstOrDefault() ?? a.BusinessLongDes
                        };

            var result = query.ToList();

            return result;
            //ILocalizedPropertyRepository _repo = new LocalizedPropertyRepository();
            //var result = _repo.GetAllEntityidLocalizedPropByMany("Tbl_BusinessMain", "BusinessLongDes", languageID);
            //_repo.Dispose();

            //if (result.Count() > 0)
            //{
            //    return (from list in result
            //            select new TempForMultiLangStrID
            //            {
            //                EntityID = list.EntityID.ToString(),
            //                EntityValue = list.LocaleValue,
            //            }).ToList<TempForMultiLangStrID>();
            //}
            //else
            //{
            //    return (from list in context.Tbl_BusinessMain
            //            select new TempForMultiLangStrID
            //            {
            //                EntityID = list.BusinessCode,
            //                EntityValue = list.BusinessLongDes,
            //            }).ToList<TempForMultiLangStrID>();
            //}
            
        }

        public virtual string GetBusinessLongDesByBusinessCodeAndLanguageID(string businessCode, int languageID)
        {
            var query = from a in context.Tbl_BusinessMain
                        where a.BusinessCode.Trim() == businessCode.Trim()
                        select new TempForMultiLangStrID
                        {
                            EntityID = a.BusinessCode,
                            EntityValue = (from ml in context.Tbl_LocalizedProperty
                                           where ml.EntityID.ToString() == a.BusinessCode
                                           && ml.LocaleKeyGroup == "Tbl_BusinessMain"
                                           && ml.LocaleKey == "BusinessLongDes"
                                           && ml.LanguageID == languageID
                                           select ml.LocaleValue).FirstOrDefault() ?? a.BusinessLongDes
                        };

            var result = query.FirstOrDefault();

            return result.EntityValue;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
