﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogRegContactRequest
{
    public partial class LogRegContactRequestRepository : ILogRegContactRequestRepository
    {
        private SupplierPortalEntities context;

        public LogRegContactRequestRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogRegContactRequestRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(int userReqId, string logAction,int updateBy)
        {

            try
            {
                var modelTemp = context.Tbl_RegContactRequest.Find(userReqId);

                if (modelTemp != null)
                {
                    var _logRegContactReq = new Tbl_LogRegContactRequest()
                    {
                        UserReqId = modelTemp.UserReqId,
                        SupplierID = modelTemp.SupplierID,
                        UserReqStatusID = modelTemp.UserReqStatusID,
                        RequestDate = modelTemp.RequestDate,
                        TitleID = modelTemp.TitleID,
                        FirstName_Local = modelTemp.FirstName_Local,
                        FirstName_Inter = modelTemp.FirstName_Inter,
                        LastName_Local = modelTemp.LastName_Local,
                        LastName_Inter = modelTemp.LastName_Inter,
                        JobTitleID = modelTemp.JobTitleID,
                        OtherJobTitle = modelTemp.OtherJobTitle,
                        Department = modelTemp.Department,
                        PhoneCountryCode = modelTemp.PhoneCountryCode,
                        PhoneNo = modelTemp.PhoneNo,
                        PhoneExt = modelTemp.PhoneExt,
                        MobileCountryCode = modelTemp.MobileCountryCode,
                        MobileNo = modelTemp.MobileNo,
                        FaxNo = modelTemp.FaxNo,
                        FaxExt = modelTemp.FaxExt,
                        Email = modelTemp.Email,
                        UserID = modelTemp.UserID,
                        isDeleted = modelTemp.isDeleted,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogRegContactRequest.Add(_logRegContactReq);
                    context.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }


        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
