﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogRegContactRequest
{
    public partial interface ILogRegContactRequestRepository
    {

        void Insert(int userReqId, string logAction, int updateBy);

        void Dispose();
    }
}
