﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogRegBusinessType
{
    public partial class LogRegBusinessTypeRepository : ILogRegBusinessTypeRepository
    {

        private SupplierPortalEntities context;

        public LogRegBusinessTypeRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogRegBusinessTypeRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }


        public virtual void Insert(int id,int regid, int businessTypeID, string logAction)
        {
            try
            {
                var model = new Tbl_LogRegBusinessType
                {
                    Id = id,
                    RegID = regid,
                    BusinessTypeID = businessTypeID,
                    //OtherBusinessType = _info.OtherBusinessType,
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };

                if (model != null)
                {
                    context.Tbl_LogRegBusinessType.Add(model);
                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
