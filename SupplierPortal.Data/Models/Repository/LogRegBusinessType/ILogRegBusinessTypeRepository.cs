﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogRegBusinessType
{
    public partial interface ILogRegBusinessTypeRepository
    {

        void Insert(int id,int regid, int businessTypeID, string logAction);

        void Dispose();
    }
}
