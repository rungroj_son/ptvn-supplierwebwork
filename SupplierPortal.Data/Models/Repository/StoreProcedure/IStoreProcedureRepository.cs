﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.StoreProcedure
{
    public partial interface IStoreProcedureRepository
    {
        IEnumerable<ProductSearchResultModel> GetResultStoreprocByQuery(string sqlQuery);

        int GetResultVerifyPostalCodeByQuery(string sqlQuery);

        void Dispose();
    }
}
