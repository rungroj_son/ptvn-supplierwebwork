﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.UserReportMapping
{
    public partial interface IUserReportMappingRepository
    {
        void Insert(Tbl_UserReportMapping tbl_UserReportMapping);

        void Delete(Tbl_UserReportMapping tbl_UserReportMapping);

        void Update(Tbl_UserReportMapping tbl_UserReportMapping);

        Tbl_UserReportMapping GerUserReportMappingByUserIDAndReportID(int userID, int reportID);

        IEnumerable<Tbl_UserReportMapping> GetUserReportMappingByUserID(int userID);

        bool CheckExistsUserReportMapping(int userID, int reportID);

        int CheckdataForShowChart(int userID);

        void Dispose();
    }
}
