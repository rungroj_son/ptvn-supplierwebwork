﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.UserReportMapping
{
    public partial class UserReportMappingRepository : IUserReportMappingRepository
    {

        private SupplierPortalEntities context;


        public UserReportMappingRepository()
        {
            context = new SupplierPortalEntities();
        }

        public UserReportMappingRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual Tbl_UserReportMapping GerUserReportMappingByUserIDAndReportID(int userID, int reportID)
        {

            var result = context.Tbl_UserReportMapping
                   .Where(b => b.UserID == userID && b.ReportID == reportID)
                   .FirstOrDefault();

            return result;

        }

        public virtual void Insert(Tbl_UserReportMapping tbl_UserReportMapping)
        {
            try
            {

                if (tbl_UserReportMapping != null)
                {

                    context.Tbl_UserReportMapping.Add(tbl_UserReportMapping);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void Delete(Tbl_UserReportMapping tbl_UserReportMapping)
        {
            try
            {

                if (tbl_UserReportMapping != null)
                {
                    var usereportMappingTmp = context.Tbl_UserReportMapping.Find(tbl_UserReportMapping.UserID, tbl_UserReportMapping.ReportID);

                    if (usereportMappingTmp != null)
                    {
                        context.Tbl_UserReportMapping.Remove(usereportMappingTmp);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void Update(Tbl_UserReportMapping tbl_UserReportMapping)
        {
            try
            {

                if (tbl_UserReportMapping != null)
                {
                    var usereportMappingTmp = context.Tbl_UserReportMapping.Find(tbl_UserReportMapping.UserID, tbl_UserReportMapping.ReportID);

                    if (usereportMappingTmp != null)
                    {
                        usereportMappingTmp.isEnable = tbl_UserReportMapping.isEnable;
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual IEnumerable<Tbl_UserReportMapping> GetUserReportMappingByUserID(int userID)
        {
            var query = from a in context.Tbl_UserReportMapping
                        where a.UserID == userID
                        select a;

            var result = query.ToList();

            return result;
        }

        public virtual bool CheckExistsUserReportMapping(int userID, int reportID)
        {
            var query = context.Tbl_UserReportMapping.Where(m => m.UserID == userID && m.ReportID == reportID).ToList();

            if (query.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int CheckdataForShowChart(int userID) 
        {
            var query = (from a in context.Tbl_UserReportMapping
                        where a.UserID == userID && a.ReportID == 1 && a.isEnable == 1
                        select  a).FirstOrDefault();

            if (query != null)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }


    }
}
