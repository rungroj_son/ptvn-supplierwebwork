﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.SupportModel.Register;
using SupplierPortal.Data.Models.SupportModel;

namespace SupplierPortal.Data.Models.Repository.BusinessEntity
{
    public partial interface IBusinessEntityRepository
    {
        IEnumerable<TempBusinessEntityType> GetBusinessEntityByMany(bool isCorperate, string localeKeyGroup, int languageID);

        IEnumerable<BusinessEntityTypeModels> GetMultiLangBusinessEntity(int companyTypeID, int languageID);

        string GetBusinessEntityByIdAndLanguageID(int Id, int languageID);

        Tbl_BusinessEntity GetBusinessEntityById(int id);

        BusinessEntityDisplayModel GetLocalizedBusinessEntityDisplayByIdAndLanguageID(int id, int languageID);

        void Dispose();
    }
}
