﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.SupportModel.Register;
using SupplierPortal.Data.Models.SupportModel;

namespace SupplierPortal.Data.Models.Repository.BusinessEntity
{
    public partial class BusinessEntityRepository : IBusinessEntityRepository
    {
        private SupplierPortalEntities context;

        public BusinessEntityRepository()
        {
            context = new SupplierPortalEntities();
        }

        public BusinessEntityRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<TempBusinessEntityType> GetBusinessEntityByMany(bool isCorperate, string localeKeyGroup, int languageID)
        {
            var businessEntityList = (from businessEntity in context.Tbl_BusinessEntity
             from localizedProperty in context.Tbl_LocalizedProperty
                  .Where(mapping => mapping.EntityID == businessEntity.Id
                  && mapping.LocaleKeyGroup == localeKeyGroup
                  && mapping.LanguageID == languageID).DefaultIfEmpty()
             select new TempBusinessEntityType
             {
                 BusinessEntityID = businessEntity.Id,
                 CompanyTypeID = businessEntity.CompanyTypeID,
                 BusinessEntity = businessEntity.BusinessEntity,
                 LocaleValue = localizedProperty.LocaleValue,
                 SeqNo = businessEntity.SeqNo
             }).ToList<TempBusinessEntityType>();

            if (isCorperate)
            {
                var companytypeidList = new int?[] { 0, 1 };
                return (from list in businessEntityList
                        where companytypeidList.Contains(list.CompanyTypeID)
                        select new TempBusinessEntityType {
                            BusinessEntityID = list.BusinessEntityID,
                            CompanyTypeID = list.CompanyTypeID,
                            BusinessEntity = list.LocaleValue ?? list.BusinessEntity,
                            LocaleValue = list.LocaleValue,
                            SeqNo = list.SeqNo
                        }).ToList<TempBusinessEntityType>().OrderBy(m => m.SeqNo);
            }
            else
            {
                var companytypeidList = new int?[] { 0, 2 };
                return (from list in businessEntityList
                        where companytypeidList.Contains(list.CompanyTypeID)
                        select new TempBusinessEntityType
                        {
                            BusinessEntityID = list.BusinessEntityID,
                            CompanyTypeID = list.CompanyTypeID,
                            BusinessEntity = list.LocaleValue ?? list.BusinessEntity,
                            LocaleValue = list.LocaleValue,
                            SeqNo = list.SeqNo
                        }).ToList<TempBusinessEntityType>().OrderBy(m=>m.SeqNo);
            }
        }

        public virtual IEnumerable<BusinessEntityTypeModels> GetMultiLangBusinessEntity(int companyTypeID, int languageID)
        {
            var query = from a in context.Tbl_BusinessEntity
                        where a.CompanyTypeID == companyTypeID
                        || a.CompanyTypeID == 0
                        select new BusinessEntityTypeModels
                        {
                            BusinessEntityID = a.Id,
                            CompanyTypeID = a.CompanyTypeID,
                            BusinessEntity = (from ml in context.Tbl_LocalizedProperty
                                              where ml.EntityID == a.Id
                                           && ml.LocaleKeyGroup == "Tbl_BusinessEntity"
                                           && ml.LocaleKey == "BusinessEntity"
                                           && ml.LanguageID == languageID
                                              select ml.LocaleValue).FirstOrDefault() ?? a.BusinessEntity,
                           SeqNo= a.SeqNo
                        };
            var result = query.ToList().OrderBy(m=>m.SeqNo);

            return result;

        }

        public virtual string GetBusinessEntityByIdAndLanguageID(int Id, int languageID)
        {
            string businessEntity = "";
            var query = from a in context.Tbl_BusinessEntity
                        where a.Id == Id
                        select new
                        {
                            EntityValue = (from lp in context.Tbl_LocalizedProperty
                                           where lp.EntityID == a.Id
                                          && lp.LocaleKeyGroup == "Tbl_BusinessEntity"
                                          && lp.LocaleKey == "BusinessEntity"
                                          && lp.LanguageID == languageID
                                           select lp.LocaleValue).FirstOrDefault() ?? a.BusinessEntity
                        };

            var result = query.FirstOrDefault();

            if(result != null)
            {
                businessEntity = result.EntityValue.ToString();
            }

            return businessEntity;
        }

        public virtual Tbl_BusinessEntity GetBusinessEntityById(int id)
        {
            var result = context.Tbl_BusinessEntity.Where(m => m.Id == id).FirstOrDefault();

            return result;
        }

        public virtual BusinessEntityDisplayModel GetLocalizedBusinessEntityDisplayByIdAndLanguageID(int id, int languageID)
        {
            var query = from a in context.Tbl_BusinessEntity
                        where a.Id == id
                        select new BusinessEntityDisplayModel
                        {
                            Id = a.Id,
                            BusinessEntity = (from lp in context.Tbl_LocalizedProperty
                                           where lp.EntityID == a.Id
                                          && lp.LocaleKeyGroup == "Tbl_BusinessEntity"
                                          && lp.LocaleKey == "BusinessEntity"
                                          && lp.LanguageID == languageID
                                           select lp.LocaleValue).FirstOrDefault() ?? a.BusinessEntity,
                            BusinessEntityDisplay = (from lp in context.Tbl_LocalizedProperty
                                           where lp.EntityID == a.Id
                                          && lp.LocaleKeyGroup == "Tbl_BusinessEntity"
                                          && lp.LocaleKey == "BusinessEntityDisplay"
                                          && lp.LanguageID == languageID
                                                     select lp.LocaleValue).FirstOrDefault() ?? a.BusinessEntityDisplay,
                            Description = a.Description,
                            CompanyTypeID = a.CompanyTypeID,
                            SeqNo = a.SeqNo,
                            ConcatStrLocal = a.ConcatStrLocal,
                            ConcatStrInter = a.ConcatStrInter
                        };


            var result = query.FirstOrDefault();

            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
