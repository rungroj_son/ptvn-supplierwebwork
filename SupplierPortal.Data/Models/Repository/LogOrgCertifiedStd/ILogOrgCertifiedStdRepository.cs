﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgCertifiedStd
{
    public partial interface ILogOrgCertifiedStdRepository
    {
        void Insert(int certId, string logAction, int updateBy);

        void Dispose();
    }
}
