﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgCertifiedStd
{
    public partial class LogOrgCertifiedStdRepository : ILogOrgCertifiedStdRepository
    {
        private SupplierPortalEntities context;


        public LogOrgCertifiedStdRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogOrgCertifiedStdRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual void Insert(int certId, string logAction, int updateBy)
        {
            try
            {
                var _orgCertifiedStd = context.Tbl_OrgCertifiedStd.Where(m=>m.CertId == certId).FirstOrDefault();

                if (_orgCertifiedStd != null)
                {
                    var _logOrgCertifiedStd = new Tbl_LogOrgCertifiedStd()
                    {
                        CertId = _orgCertifiedStd.CertId,
                        SupplierID = _orgCertifiedStd.SupplierID,
                        SeqNo = _orgCertifiedStd.SeqNo,
                        CertifiedID = _orgCertifiedStd.CertifiedID,
                        OtherStandardName = _orgCertifiedStd.OtherStandardName,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgCertifiedStd.Add(_logOrgCertifiedStd);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
