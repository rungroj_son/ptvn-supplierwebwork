﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.Currency
{
    public partial class CurrencyRepository : ICurrencyRepository
    {

        private SupplierPortalEntities context;

        public CurrencyRepository()
        {
            context = new SupplierPortalEntities();
        }

        public CurrencyRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }


        public virtual IEnumerable<Tbl_Currency> CurrencyList()
        {
            return context.Tbl_Currency.OrderBy(m => m.CurrencyCode).ToList();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
