﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.Currency
{
    public partial interface ICurrencyRepository
    {

        IEnumerable<Tbl_Currency> CurrencyList();

        void Dispose();


    }
}
