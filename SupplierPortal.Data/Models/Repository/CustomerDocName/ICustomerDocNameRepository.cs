﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.CustomerDocName
{
    public partial interface ICustomerDocNameRepository
    {
        IEnumerable<CustomerDocNameModels> GetCustomerDocNameByDocumentTypeID(int documentTypeID);

        IEnumerable<CustomerDocNameModels> GetCustomerDocNameByDocumentTypeIDAndCountryCode(int documentTypeID, string countryCode, int companyTypeID);

        IEnumerable<Tbl_CustomerDocName> GetCustomerDocNameAll();      

        IEnumerable<Tbl_CustomerDocNameMapping> GetCustomerDocNameForRemove(int companyTypeID);

        IEnumerable<Tbl_CustomerDocNameMapping> GetCustomerDocNameForRemoveByCountryCode(int companyTypeID, string countryCode);

        string GetCustomerDocNameByDocumentNameID(int documentNameID);

        void Dispose();
    }
}
