﻿using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SupplierPortal.Data.Models.Repository.CustomerDocName
{
    public partial class CustomerDocNameRepository : ICustomerDocNameRepository
    {

        private SupplierPortalEntities context;

        public CustomerDocNameRepository()
        {
            context = new SupplierPortalEntities();
        }

        public CustomerDocNameRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<CustomerDocNameModels> GetCustomerDocNameByDocumentTypeID(int documentTypeID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from a in context.Tbl_CustomerDocName
                        where a.DocumentTypeID == documentTypeID
                        select new CustomerDocNameModels
                        {
                            DocumentNameID = a.DocumentNameID,
                            DocumentName = (from lp in context.Tbl_LocalizedProperty
                                            where lp.EntityID == a.DocumentNameID
                                           && lp.LocaleKeyGroup == "Tbl_CustomerDocName"
                                           && lp.LocaleKey == "DocumentName"
                                           && lp.LanguageID == languageId
                                            select lp.LocaleValue).FirstOrDefault() ?? a.DocumentName,
                            SeqNo = a.SeqNo??0,
                            DocumentTypeID = a.DocumentTypeID??0
                        };

            var result = query.ToList().OrderBy(m=>m.SeqNo);

            return result;

        }

        public virtual IEnumerable<CustomerDocNameModels> GetCustomerDocNameByDocumentTypeIDAndCountryCode(int documentTypeID, string countryCode, int companyTypeID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            if (countryCode == "TH")
            {
                var query = from a in context.Tbl_CustomerDocName
                            join b in context.Tbl_CustomerDocNameMapping on a.DocumentNameID equals b.DocumentNameID into c
                            from e in c.DefaultIfEmpty()
                            where a.DocumentTypeID == documentTypeID
                            && e.CompanyTypeID == companyTypeID
                            && e.isTHShowOnList == 1
                            select new CustomerDocNameModels
                            {
                                DocumentNameID = a.DocumentNameID,
                                DocumentName = (from lp in context.Tbl_LocalizedProperty
                                                where lp.EntityID == a.DocumentNameID
                                               && lp.LocaleKeyGroup == "Tbl_CustomerDocName"
                                               && lp.LocaleKey == "DocumentName"
                                               && lp.LanguageID == languageId
                                                select lp.LocaleValue).FirstOrDefault() ?? a.DocumentName,
                                SeqNo = a.SeqNo ?? 0,
                                DocumentTypeID = a.DocumentTypeID ?? 0
                            };
                var result = query.ToList().OrderBy(m => m.SeqNo);
                return result;

            }else
            {

                var query = from a in context.Tbl_CustomerDocName
                            join b in context.Tbl_CustomerDocNameMapping on a.DocumentNameID equals b.DocumentNameID into c
                            from e in c.DefaultIfEmpty()
                            where a.DocumentTypeID == documentTypeID
                            && e.CompanyTypeID == companyTypeID
                            && e.isNonTHShowOnList == 1
                            select new CustomerDocNameModels
                            {
                                DocumentNameID = a.DocumentNameID,
                                DocumentName = (from lp in context.Tbl_LocalizedProperty
                                                where lp.EntityID == a.DocumentNameID
                                               && lp.LocaleKeyGroup == "Tbl_CustomerDocName"
                                               && lp.LocaleKey == "DocumentName"
                                               && lp.LanguageID == languageId
                                                select lp.LocaleValue).FirstOrDefault() ?? a.DocumentName,
                                SeqNo = a.SeqNo ?? 0,
                                DocumentTypeID = a.DocumentTypeID ?? 0
                            };
                var result = query.ToList().OrderBy(m => m.SeqNo);
                return result;
            } 
        }

        public virtual IEnumerable<Tbl_CustomerDocName> GetCustomerDocNameAll()
        {
            var query = from a in context.Tbl_CustomerDocName
                        select a;

            var result = query.ToList();

            return result;
        }

        public virtual IEnumerable<Tbl_CustomerDocNameMapping> GetCustomerDocNameForRemove(int companyTypeID)
        {
            //companyTypeID = 1
            var listMapping = from a in context.Tbl_CustomerDocNameMapping
                              where a.CompanyTypeID == companyTypeID
                              select a.DocumentNameID;
            // result = -1 1 2 3

            var listDoc = listMapping.ToList();

            var query = from db in context.Tbl_CustomerDocNameMapping
                        where db.CompanyTypeID != companyTypeID && !(listMapping).Contains(db.DocumentNameID)
                        select db;

            var result = query.ToList();

            return result;            
        }

        public virtual IEnumerable<Tbl_CustomerDocNameMapping> GetCustomerDocNameForRemoveByCountryCode(int companyTypeID, string countryCode)
        {
            if (countryCode == "TH")
            {
                var listMapping = from a in context.Tbl_CustomerDocNameMapping
                                  where a.CompanyTypeID == companyTypeID
                                  && a.isTHShowOnList == 1
                                  select a.DocumentNameID;

                var query = from db in context.Tbl_CustomerDocNameMapping
                            where db.CompanyTypeID != companyTypeID && !(listMapping).Contains(db.DocumentNameID)
                            select db;

                var result = query.ToList();

                return result;
            }else
            {
                var listMapping = from a in context.Tbl_CustomerDocNameMapping
                                  where a.CompanyTypeID == companyTypeID
                                  && a.isNonTHShowOnList == 1
                                  select a.DocumentNameID;

                var query = from db in context.Tbl_CustomerDocNameMapping
                            where db.CompanyTypeID != companyTypeID && !(listMapping).Contains(db.DocumentNameID)
                            select db;

                var result = query.ToList();

                return result;
            }
            //companyTypeID = 1
            //var listMapping = from a in context.Tbl_CustomerDocNameMapping
            //                  where a.CompanyTypeID == companyTypeID
            //                  select a.DocumentNameID;
            // result = -1 1 2 3

            //var listDoc = listMapping.ToList();

            //var query = from db in context.Tbl_CustomerDocNameMapping
            //            where db.CompanyTypeID != companyTypeID && !(listMapping).Contains(db.DocumentNameID)
            //            select db;

            //var result = query.ToList();

            //return result;
        }

        public virtual string GetCustomerDocNameByDocumentNameID(int documentNameID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from a in context.Tbl_CustomerDocName
                        where a.DocumentNameID == documentNameID
                        select new
                        {
                            DocumentName = (from lp in context.Tbl_LocalizedProperty
                                              where lp.EntityID == a.DocumentNameID
                                             && lp.LocaleKeyGroup == "Tbl_CustomerDocName"
                                             && lp.LocaleKey == "DocumentName"
                                             && lp.LanguageID == languageId
                                              select lp.LocaleValue).FirstOrDefault() ?? a.DocumentName
                        };

            return query.FirstOrDefault().DocumentName;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
