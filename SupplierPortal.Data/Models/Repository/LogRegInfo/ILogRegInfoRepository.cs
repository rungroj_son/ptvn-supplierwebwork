﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.LogRegInfo
{
    public partial interface ILogRegInfoRepository
    {
        bool Insert(int regid, string logAction);

        void Dispose();
    }
}
