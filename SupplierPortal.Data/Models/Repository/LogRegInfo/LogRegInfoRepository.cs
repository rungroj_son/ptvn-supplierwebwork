﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using SupplierPortal.Data.Models.Repository.RegAddress;
using SupplierPortal.Data.Models.Repository.RegEmailTicket;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.LogRegInfo
{
    public partial class LogRegInfoRepository : ILogRegInfoRepository
    {
        private SupplierPortalEntities context;

        public LogRegInfoRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogRegInfoRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual bool Insert(int regid, string logAction)
        {
            try
            {
                var _info = context.Tbl_RegInfo.FirstOrDefault(m => m.RegID == regid);

                var _logregInfo = new Tbl_LogRegInfo()
                {
                    RegID = _info.RegID,
                    RegStatusID = _info.RegStatusID,
                    //FirstName = _info.FirstName,
                    //LastName = _info.LastName,
                    TaxID = _info.TaxID,
                    CountryCode = _info.CountryCode,
                    TicketCode = _info.TicketCode,
                    DUNSNumber = _info.DUNSNumber,
                    CompanyTypeID = _info.CompanyTypeID,
                    BusinessEntityID = _info.BusinessEntityID,
                    OtherBusinessEntity = _info.OtherBusinessEntity,
                    CompanyName_Local = _info.CompanyName_Local,
                    CompanyName_Inter = _info.CompanyName_Inter,
                    BranchNo = _info.BranchNo,
                    BranchName_Local = _info.BranchName_Local,
                    BranchName_Inter = _info.BranchName_Inter,
                    MainBusinessCode = _info.MainBusinessCode,
                    MainBusiness = _info.MainBusiness,
                    MainContactID = _info.MainContactID,
                    PhoneNo = _info.PhoneNo,
                    PhoneExt = _info.PhoneExt,
                    MobileNo = _info.MobileNo,
                    FaxNo = _info.FaxNo,
                    FaxExt = _info.FaxExt,
                    WebSite = _info.WebSite,
                    Latitude = _info.Latitude,
                    Longtitude = _info.Longtitude,
                    LastUpdateDate = _info.LastUpdateDate,
                    LastRejectDate = _info.LastRejectDate,
                    isDeleted = _info.isDeleted,
                    CompanyAddrID = _info.CompanyAddrID,
                    DeliveredAddrID = _info.DeliveredAddrID,
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction,
                    RegisteredCapital = _info.RegisteredCapital ?? 0,
                    CurrencyCode = _info.CurrencyCode,
                    RegisteredEmail = _info.RegisteredEmail,
                    LanguageCode = _info.LanguageCode,
                    RequestDate = _info.RequestDate,
                    ApprovedBy = _info.ApprovedBy,
                    ApprovedDate = _info.ApprovedDate
                };
                if (_logregInfo != null)
                {
                    context.Tbl_LogRegInfo.Add(_logregInfo);
                    context.SaveChanges();
                }

                return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
