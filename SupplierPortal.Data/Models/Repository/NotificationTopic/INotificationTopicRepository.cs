﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.SupportModel;

namespace SupplierPortal.Data.Models.Repository.NotificationTopic
{
    public partial interface INotificationTopicRepository
    {
        Tbl_NotificationTopic GetDataByTopicID(int TopicID);

        List<NotificationTopicType> GetComboboxTopicList(int supID, int languageID);

        List<NotificationModel> GetListStrJoinNotiTopic(int LanguageID);

        void Dispose();
    }
}
