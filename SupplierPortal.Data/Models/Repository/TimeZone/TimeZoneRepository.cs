﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.TimeZone
{
    public partial class TimeZoneRepository : ITimeZoneRepository
    {
        
        private SupplierPortalEntities context;

        public TimeZoneRepository()
        {
            context = new SupplierPortalEntities();
        }

        public TimeZoneRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }


        public virtual IEnumerable<Tbl_TimeZone> TimeZoneList()
        {
            return context.Tbl_TimeZone.Where(m=>m.IsActive ==1).OrderBy(m => m.DisplayName).ToList();
        }

        public virtual Tbl_TimeZone GetTimeZoneByTimeZoneIDString(string timeZoneIDString)
        {
            var query = from db in context.Tbl_TimeZone
                        where db.TimeZoneIDString == timeZoneIDString
                        select db;
            return query.FirstOrDefault();
        }

        public void Dispose()
        {
            context.Dispose();
        }

    }
}
