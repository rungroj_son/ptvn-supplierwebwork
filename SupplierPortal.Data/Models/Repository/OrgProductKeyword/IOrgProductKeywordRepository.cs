﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OrgProductKeyword
{
    public partial interface IOrgProductKeywordRepository
    {
        IEnumerable<Tbl_OrgProductKeyword> GetProductKeywordBySupplierID(int supplierID);

        void removeKeyWordList(IEnumerable<Tbl_OrgProductKeyword> Data);

        void Insert(int supplierID, string keyword, int updateBy);

        void Delete(int supplierID, string keyword, int updateBy);

        bool OrgProductKeywordExists(int supplierID, string keyword);

        void Dispose();
    }
}
