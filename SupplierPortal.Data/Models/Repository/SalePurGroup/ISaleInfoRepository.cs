﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.SalePurGroup
{
    public partial interface ISaleInfoRepository
    {
        IEnumerable<Tbl_SalePurGroup> GetBuyerList();

        Tbl_OrgSaleInfo_ProductList GetProduct(int ContactID);

        IEnumerable<Tbl_OrgSaleInfo_Purgroup> GetPurgroup(int ContactID);

        void insertProductList(int ContactID, string Product, int SupplierID);

        void updateProductList(int ContactID, string Product, int SupplierID);

        void insertBuyerList(int ContactID, int Purgroup, int SupplierID);

        void removeBuyerList(IEnumerable<Tbl_OrgSaleInfo_Purgroup> Data);

        void removeProductList(Tbl_OrgSaleInfo_ProductList Data);

        void updateBuyerList(int ContactID, int Purgroup);

        void Dispose();
    }
}
