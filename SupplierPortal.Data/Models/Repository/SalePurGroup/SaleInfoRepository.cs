﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogSalePurGroup;
using System.Globalization;

namespace SupplierPortal.Data.Models.Repository.SalePurGroup
{
    public partial class SaleInfoRepository : ISaleInfoRepository
    {
        private SupplierPortalEntities context;
        ILogSalePurGroupRepository _logSalePurGroupRepository;

        public SaleInfoRepository()
        {
            context = new SupplierPortalEntities();
            _logSalePurGroupRepository = new LogSalePurGroupRepository();
        }

        public SaleInfoRepository(
            SupplierPortalEntities context,
            LogSalePurGroupRepository logSalePurGroupRepository
            )
        {
            this.context = context;
            _logSalePurGroupRepository = logSalePurGroupRepository;
        }

        public virtual IEnumerable<Tbl_SalePurGroup> GetBuyerList()
        {
            return (from Buyer in context.Tbl_SalePurGroup
                    where Buyer.BuyerShortName == "CPFSAP"
                    select Buyer).ToList<Tbl_SalePurGroup>().OrderBy(x => x.PurGroupName, StringComparer.Create(new CultureInfo("th-TH"), true));
            //OrderBy(m => m.PurGroupName);

        }

        public virtual Tbl_OrgSaleInfo_ProductList GetProduct(int ContactID)
        {
            return (from product in context.Tbl_OrgSaleInfo_ProductList
                    where product.ContactID == ContactID
                    select product).FirstOrDefault();
        }

        public virtual IEnumerable<Tbl_OrgSaleInfo_Purgroup> GetPurgroup(int ContactID)
        {
            return (from Purgroup in context.Tbl_OrgSaleInfo_Purgroup
                    where Purgroup.ContactID == ContactID
                    select Purgroup).ToList<Tbl_OrgSaleInfo_Purgroup>().OrderBy(m => m.Id);
        }

        public void insertProductList(int ContactID, string Product, int SupplierID)
        {
            try
            {
                var data = new Tbl_OrgSaleInfo_ProductList()
                {
                    ContactID = ContactID,
                    ProductList = Product
                };

                context.Tbl_OrgSaleInfo_ProductList.Add(data);
                context.SaveChanges();

                _logSalePurGroupRepository.insertLogProductList(ContactID, SupplierID, "Add");
            }
            catch
            {
                throw;
            }
        }

        public void updateProductList(int ContactID, string Product, int SupplierID)
        {
            try
            {
                var tempModel = context.Tbl_OrgSaleInfo_ProductList.Find(ContactID);

                if (tempModel != null)
                {
                    var data = new Tbl_OrgSaleInfo_ProductList()
                    {
                        ContactID = ContactID,
                        ProductList = Product
                    };
                    context.Tbl_OrgSaleInfo_ProductList.Remove(tempModel);
                    context.Tbl_OrgSaleInfo_ProductList.Add(data);
                    context.SaveChanges();

                    _logSalePurGroupRepository.insertLogProductList(ContactID, SupplierID, "Modify");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void insertBuyerList(int ContactID, int Purgroup, int SupplierID)
        {
            try
            {
                var data = new Tbl_OrgSaleInfo_Purgroup()
                {
                    ContactID = ContactID,
                    PurGroupId = Purgroup
                };
                context.Tbl_OrgSaleInfo_Purgroup.Add(data);
                context.SaveChanges();

                _logSalePurGroupRepository.insertLogBuyerList(ContactID, Purgroup, SupplierID, "Add");
            }
            catch
            {
                throw;
            }
        }

        public void removeBuyerList(IEnumerable<Tbl_OrgSaleInfo_Purgroup> Data)
        {
            foreach (var item in Data)
            {
                context.Tbl_OrgSaleInfo_Purgroup.Remove(item);
                context.SaveChanges();
            }

        }

        public void removeProductList(Tbl_OrgSaleInfo_ProductList Data)
        {
            context.Tbl_OrgSaleInfo_ProductList.Remove(Data);
            context.SaveChanges();
        }

        public void updateBuyerList(int ContactID, int Purgroup)
        {
            try
            {
                var tempModel = context.Tbl_OrgSaleInfo_Purgroup.Find(ContactID);

                if (tempModel != null)
                {
                    tempModel.ContactID = ContactID;
                    tempModel.PurGroupId = Purgroup;

                    context.SaveChanges();
                    //InsertAddress(model);

                    //_logOrgAddressRepository.Insert(orgAdd.AddressID, orgAdd.SupplierID, "Modify");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
