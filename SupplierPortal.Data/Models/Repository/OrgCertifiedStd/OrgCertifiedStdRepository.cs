﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.LogOrgCertifiedStd;

namespace SupplierPortal.Data.Models.Repository.OrgCertifiedStd
{
    public partial class OrgCertifiedStdRepository : IOrgCertifiedStdRepository
    {

        private SupplierPortalEntities context;
        ILogOrgCertifiedStdRepository _logOrgCertifiedStdRepository;

        public OrgCertifiedStdRepository()
        {
            context = new SupplierPortalEntities();
            _logOrgCertifiedStdRepository = new LogOrgCertifiedStdRepository(); 
        }

        public OrgCertifiedStdRepository(
            SupplierPortalEntities context,
            LogOrgCertifiedStdRepository logOrgCertifiedStdRepository
            )
        {
            this.context = context;
            _logOrgCertifiedStdRepository = logOrgCertifiedStdRepository;
        }

        public virtual IEnumerable<Tbl_OrgCertifiedStd> GetOrgCertifiedStdBySupplierID(int supplierID)
        {
            var query = from a in context.Tbl_OrgCertifiedStd
                        where a.SupplierID == supplierID
                        select a;

            var result = query.ToList();

            return result;
        }

        public virtual int InsertOrgCertifiedStdReturnCertId(Tbl_OrgCertifiedStd model, int updateBy)
        {
            try
            {
                int certId = 0;

                if (model != null)
                {
                    context.Tbl_OrgCertifiedStd.Add(model);
                    context.SaveChanges();

                    certId = model.CertId;

                    _logOrgCertifiedStdRepository.Insert(certId, "Add", updateBy);
                }

                return certId;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual Tbl_OrgCertifiedStd GetOrgCertifiedStdByCertId(int certId)
        {
            var query = context.Tbl_OrgCertifiedStd.Where(m => m.CertId == certId).FirstOrDefault();

            return query;
        }

        public virtual void Delete(int certId, int updateBy)
        {
            try
            {
                var model = context.Tbl_OrgCertifiedStd.Where(m => m.CertId == certId).FirstOrDefault();

                if (model != null)
                {
                    _logOrgCertifiedStdRepository.Insert(certId, "Remove", updateBy);

                    context.Tbl_OrgCertifiedStd.Remove(model);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
            

        }

        public virtual void Update(Tbl_OrgCertifiedStd model, int updateBy)
        {
            try
            {
                var tempModel = context.Tbl_OrgCertifiedStd.Find(model.CertId);

                if (tempModel != null)
                {
                    tempModel = model;
                    context.SaveChanges();
                    _logOrgCertifiedStdRepository.Insert(model.CertId, "Modify", updateBy);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void UpdateNonLog(Tbl_OrgCertifiedStd model)
        {
            try
            {
                var tempModel = context.Tbl_OrgCertifiedStd.Find(model.CertId);

                if (tempModel != null)
                {
                    tempModel = model;
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
