﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgProductCategory
{
    public partial interface ILogOrgProductCategoryRepository
    {
        void Insert(int id, int supplierID,int categoryID_Lev3, int productTypeID, string logAction);

        void Dispose();
    }
}
