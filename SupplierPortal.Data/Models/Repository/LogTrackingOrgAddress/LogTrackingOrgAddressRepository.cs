﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogTrackingOrgAddress
{
    public partial class LogTrackingOrgAddressRepository : ILogTrackingOrgAddressRepository
    {
        private SupplierPortalEntities context;

        public LogTrackingOrgAddressRepository()
        {
            context = new SupplierPortalEntities();
        }
        public LogTrackingOrgAddressRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }
        public void Insert(int TrackingItemID, int UpdateBy, string logAction)
        {
            try
            {
                var LogTrackingOrgaddress = context.Tbl_TrackingOrgAddress.FirstOrDefault(m => m.TrackingItemID == TrackingItemID);

                if (LogTrackingOrgaddress != null)
                {
                    var _logOrg = new Tbl_LogTrackingOrgAddress()
                    {
                        TrackingReqID = LogTrackingOrgaddress.TrackingReqID,
                        TrackingItemID = LogTrackingOrgaddress.TrackingItemID,
                        AdressTypeID = LogTrackingOrgaddress.AdressTypeID,
                        OldAddressID = LogTrackingOrgaddress.OldAddressID,
                        NewAddressID = LogTrackingOrgaddress.NewAddressID,
                        TrackingStatusID = LogTrackingOrgaddress.TrackingStatusID,
                        UpdateBy = UpdateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction,
                        Remark = "",
                        TrackingStatusDate = LogTrackingOrgaddress.TrackingStatusDate
                    };

                    context.Tbl_LogTrackingOrgAddress.Add(_logOrg);
                    context.SaveChanges();
                }

            }
            catch
            {
                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
