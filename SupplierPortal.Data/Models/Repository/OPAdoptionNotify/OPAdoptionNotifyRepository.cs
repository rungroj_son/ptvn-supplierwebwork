﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.OPAdoptionNotify
{
    public partial class OPAdoptionNotifyRepository : IOPAdoptionNotifyRepository
    {
        private SupplierPortalEntities context;

        public OPAdoptionNotifyRepository()
        {
            context = new SupplierPortalEntities();
        }

        public OPAdoptionNotifyRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(Tbl_OPAdoptionNotify model)
        {
            try
            {

                if (model != null)
                {

                    context.Tbl_OPAdoptionNotify.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual int InsertReturnAdoptionReqID(Tbl_OPAdoptionNotify model)
        {
            try
            {
                int adoptionReqID = 0;

                if (model != null)
                {

                    context.Tbl_OPAdoptionNotify.Add(model);
                    context.SaveChanges();
                }


                adoptionReqID = model.AdoptionReqID;

                return adoptionReqID;

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual Tbl_OPAdoptionNotify GetOPAdoptionNotifyByAdoptionReqID(int adoptionReqID)
        {
            var query = context.Tbl_OPAdoptionNotify.Where(m => m.AdoptionReqID == adoptionReqID).FirstOrDefault();

            return query;
        }

        public virtual void UpdateFlagIsSendMailByAdoptionReqID(int adoptionReqID)
        {
            try
            {
                var model = context.Tbl_OPAdoptionNotify.Find(adoptionReqID);

                if (model != null)
                {
                    model.isSendMail = 1;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
