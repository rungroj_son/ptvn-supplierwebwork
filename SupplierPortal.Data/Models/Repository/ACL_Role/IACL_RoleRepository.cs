﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.ACL_Role
{
    public partial interface IACL_RoleRepository
    {

        IEnumerable<Tbl_ACL_Role> GetRoleList();

        IEnumerable<Tbl_ACL_Role> GetRoleListAll();

        string GetRoleNameByRoleID(int sysRoleID);


        void Dispose();
    }
}
