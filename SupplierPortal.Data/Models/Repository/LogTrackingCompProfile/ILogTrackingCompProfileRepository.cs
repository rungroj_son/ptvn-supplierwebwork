﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogTrackingCompProfile
{
    public partial interface ILogTrackingCompProfileRepository
    {
        void Insert(int TrackingItemID, int UpdateBy, string logAction);

        void Dispose();
    }
}
