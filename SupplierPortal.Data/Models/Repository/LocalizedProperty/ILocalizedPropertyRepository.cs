﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;

namespace SupplierPortal.Data.Models.Repository.LocalizedProperty
{
    public partial interface ILocalizedPropertyRepository
    {

        string GetLocalizedValue(string sqlQuery);

        //dynamic GetLocalizedDynamicByStore(string languageID, string localeKeyGroup, string pkTableMain);

        dynamic GetLocalizedByStore(string sqlQuery);
		
		dynamic GetLocalizedListByStore(string sqlQuery);

        IEnumerable<Tbl_LocalizedProperty> GetLocalizedPropAll();

        IQueryable<Tbl_LocalizedProperty> GetLocalizedPropLoadAll();

        IEnumerable<Tbl_LocalizedProperty> GetLocalizedPropByEntityid(int entityid);

        IEnumerable<Tbl_LocalizedProperty> GetAllLangLocalizedPropByMany(int _entityid, string _localekeygroup, string _localekey);

        IEnumerable<Tbl_LocalizedProperty> GetAllEntityidLocalizedPropByMany(string _localekeygroup, string _localekey, int _languageid);

        IEnumerable<Tbl_LocalizedProperty> GetAllLocalizedPropByMany(int _entityid, string _localekeygroup, string _localekey, int _languageid);

        IEnumerable<Tbl_LocalizedProperty> GetSomeEntityLocalizedPropByMany(int[] _entityid, string _localekeygroup, string _localekey, int _languageid);

        Tbl_LocalizedProperty GetEntityidLocalizedProp(int _entityid, string _localekeygroup, string _localekey, int _languageid);

        bool InsertLocalized(int? _entityid, int? _languageid, string _localekeygroup, string _localekey, string _localevalue);

        bool UpdateLocalized(int? _entityid, int? _languageid, string _localekeygroup, string _localekey, string _localevalue);

        void Dispose();

    }
}
