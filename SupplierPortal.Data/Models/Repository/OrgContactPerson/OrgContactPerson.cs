﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using SupplierPortal.Data.Models.Repository.LogOrgContactPerson;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System.Web;
using SupplierPortal.Data.Helper;

namespace SupplierPortal.Data.Models.Repository.OrgContactPerson
{
    public partial class OrgContactPerson : IOrgContactPerson
    {
        private SupplierPortalEntities context;
        ILogOrgContactPersonRepository _logOrgContactPersonRepository;
        IOrganizationRepository _organizationRepository;
       

        public OrgContactPerson()
        {
            context = new SupplierPortalEntities();
            _logOrgContactPersonRepository = new LogOrgContactPersonRepository();
            _organizationRepository = new OrganizationRepository();
        }

        public OrgContactPerson(
            SupplierPortalEntities context,
            LogOrgContactPersonRepository logOrgContactPersonRepository,
            OrganizationRepository organizationRepository
            )
        {
            this.context = context;
            _logOrgContactPersonRepository = logOrgContactPersonRepository;
            _organizationRepository = organizationRepository;
        }

        public virtual bool CheckOrgContactPersonDuplicate(string FirstName_Contact, string LastName_Contact, int SupplierID)
        {
            var qurey1 = from a in context.Tbl_OrgContactPerson
                        join b in context.Tbl_ContactPerson on a.ContactID equals b.ContactID into c
                        from d in c.DefaultIfEmpty()
                        where a.SupplierID == SupplierID
                        && d.FirstName_Local == FirstName_Contact
                        && d.LastName_Local == LastName_Contact
                        select a;

            var result1 = qurey1.ToList();
            if (result1.Count() > 0)
            {
                return true;
            }


            var qurey2 = from a in context.Tbl_OrgContactPerson
                         join b in context.Tbl_ContactPerson on a.ContactID equals b.ContactID into c
                         from d in c.DefaultIfEmpty()
                         where a.SupplierID == SupplierID
                         && d.FirstName_Inter == FirstName_Contact
                         && d.LastName_Inter == LastName_Contact
                         select a;

            var result2 = qurey2.ToList();
            if (result2.Count() > 0)
            {
                return true;
            }

            return false;
        }

        public virtual IEnumerable<Tbl_User> GetUserDuplicate(string FirstName_Contact, string LastName_Contact, int SupplierID)
        {
            var qurey1 = from a in context.Tbl_OrgContactPerson
                         join b in context.Tbl_ContactPerson on a.ContactID equals b.ContactID into c
                         from d in c.DefaultIfEmpty()
                         join e in context.Tbl_User on d.ContactID equals e.ContactID into f
                         from g in f.DefaultIfEmpty()
                         where a.SupplierID == SupplierID
                         && d.FirstName_Local == FirstName_Contact
                         && d.LastName_Local == LastName_Contact
                         select g;
            var result1 = qurey1.ToList();
            if (result1.Count() > 0)
            {
                return result1;
            }


            var qurey2 = from a in context.Tbl_OrgContactPerson
                         join b in context.Tbl_ContactPerson on a.ContactID equals b.ContactID into c
                         from d in c.DefaultIfEmpty()
                         join e in context.Tbl_User on d.ContactID equals e.ContactID into f
                         from g in f.DefaultIfEmpty()
                         where a.SupplierID == SupplierID
                         && d.FirstName_Inter == FirstName_Contact
                         && d.LastName_Inter == LastName_Contact
                         select g;
            var result2 = qurey2.ToList();

            return result2;
        }

        public virtual Tbl_OrgContactPerson GetOrgContactPersonBySupplierIDAndContactID(int supplierID, int contactID)
        {
            var qurey = from a in context.Tbl_OrgContactPerson
                        where a.SupplierID == supplierID
                        && a.ContactID == contactID
                        select a;

            return qurey.FirstOrDefault();
        }

        public virtual void Update(Tbl_OrgContactPerson model)
        {
            var tempModel = context.Tbl_OrgContactPerson.Find(model.SupplierID, model.ContactID);

            if (tempModel != null)
            {
                tempModel = model;
                context.SaveChanges();
            }
        }

        public virtual IEnumerable<Tbl_OrgContactPerson> GetOrgContactPersonBySupplierID(int supplierID)
        {
            var query = from a in context.Tbl_OrgContactPerson
                        where a.SupplierID == supplierID
                        select a;

            var result = query.ToList();

            return result;
        }

        public virtual IEnumerable<ContactPersonTableModel> GetOrgContactPersonTableBySupplierID(int supplierID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var orgModel = _organizationRepository.GetDataBySupplierID(supplierID);
            string orgID = orgModel.OrgID;
            var listUser = context.Tbl_User.Where(m => m.OrgID == orgID).Select(m => m.ContactID).ToList();

            var query = from a in context.Tbl_OrgContactPerson
                        join b in context.Tbl_ContactPerson on a.ContactID equals b.ContactID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_NameTitle on d.TitleID equals e.TitleID into f
                        from g in f.DefaultIfEmpty()
                        join h in context.Tbl_JobTitle on d.JobTitleID equals h.JobTitleID into i
                        from j in i.DefaultIfEmpty()
                        where a.SupplierID == supplierID
                        && d.isDeleted != 1
                        select new ContactPersonTableModel
                        {
                            ContactID = a.ContactID,
                            TitleContactName = (from lp in context.Tbl_LocalizedProperty
                                                where lp.EntityID == d.TitleID
                                                && lp.LocaleKeyGroup == "Tbl_NameTitle"
                                                && lp.LocaleKey == "TitleName"
                                                && lp.LanguageID == languageId
                                                select lp.LocaleValue).FirstOrDefault() ?? g.TitleName,
                            Fullname = d.FirstName_Local + "  "+d.LastName_Local,
                            TitleID = d.TitleID,
                            FirstName = d.FirstName_Local,
                            LastName = d.LastName_Local,
                            JobTitleID = d.JobTitleID,
                            JobTitleName = (from lp2 in context.Tbl_LocalizedProperty
                                            where lp2.EntityID == d.JobTitleID
                                            && lp2.LocaleKeyGroup == "Tbl_JobTitle"
                                            && lp2.LocaleKey == "JobTitleName"
                                            && lp2.LanguageID == languageId
                                            select lp2.LocaleValue).FirstOrDefault() ?? j.JobTitleName,
                            OtherJobTitle = d.OtherJobTitle,
                            Department = d.Department,
                            PhoneNo = d.PhoneNo,
                            PhoneExt = d.PhoneExt,
                            MobileNo = d.MobileNo,
                            FaxNo = d.FaxNo,
                            FaxExt = d.FaxExt,
                            Email = d.Email
                        };

            var result = query.Where(m => !(listUser.Contains(m.ContactID))).ToList();

            return result;
        }

        public virtual void Insert(Tbl_OrgContactPerson model, int updateBy)
        {
            try
            {
                if (model != null)
                {
                    context.Tbl_OrgContactPerson.Add(model);
                    context.SaveChanges();

                    _logOrgContactPersonRepository.Insert(model.SupplierID, model.ContactID, "Add", updateBy);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Delete(int supplierID,int contactID, int updateBy)
        {
            try
            {
                var model = context.Tbl_OrgContactPerson.Where(m => m.SupplierID == supplierID && m.ContactID == contactID).FirstOrDefault();

                if (model != null)
                {
                    context.Tbl_OrgContactPerson.Remove(model);

                    _logOrgContactPersonRepository.Insert(model.SupplierID, model.ContactID, "Remove", updateBy);

                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
