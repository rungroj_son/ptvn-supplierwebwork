﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;

namespace SupplierPortal.Data.Models.Repository.OrgContactPerson
{
    public partial interface IOrgContactPerson
    {
        //IEnumerable<Tbl_OrgContactPerson> GetOrgContactPersonByIsAdmin(int isadmin);

        //string[] GetEmailSupplierAdminList();

        bool CheckOrgContactPersonDuplicate(string FirstName_Contact, string LastName_Contact, int SupplierID);

        IEnumerable<Tbl_User> GetUserDuplicate(string FirstName_Contact, string LastName_Contact, int SupplierID);

        Tbl_OrgContactPerson GetOrgContactPersonBySupplierIDAndContactID(int supplierID, int contactID);

        void Update(Tbl_OrgContactPerson model);

        IEnumerable<Tbl_OrgContactPerson> GetOrgContactPersonBySupplierID(int supplierID);

        IEnumerable<ContactPersonTableModel> GetOrgContactPersonTableBySupplierID(int supplierID);

        void Insert(Tbl_OrgContactPerson model, int updateBy);

        void Delete(int supplierID, int contactID, int updateBy);

        void Dispose();
    }
}
