﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogRegServiceTypeMapping;

namespace SupplierPortal.Data.Models.Repository.RegServiceTypeMapping
{
    public partial class RegServiceTypeMappingRepository : IRegServiceTypeMappingRepository
    {

        private SupplierPortalEntities context;

        ILogRegServiceTypeMappingRepository _logRegServiceTypeMappingRepository;

        public RegServiceTypeMappingRepository()
        {
            context = new SupplierPortalEntities();
            _logRegServiceTypeMappingRepository = new LogRegServiceTypeMappingRepository();
        }

        public RegServiceTypeMappingRepository(
            SupplierPortalEntities context,
            LogRegServiceTypeMappingRepository logRegServiceTypeMappingRepository
            )
        {
            this.context = context;
            _logRegServiceTypeMappingRepository = logRegServiceTypeMappingRepository;
        }


        public virtual void InsertByValue(int regID, int serviceTypeID, string invitationCode)
        {
            try
            {

                var model = new Tbl_RegServiceTypeMapping()
                {
                    RegID = regID,
                    ServiceTypeID = serviceTypeID,
                    InvitationCode = invitationCode
                };

                if (model != null)
                {
                    context.Tbl_RegServiceTypeMapping.Add(model);

                    context.SaveChanges();

                    int logId = model.Id;
                    _logRegServiceTypeMappingRepository.Insert(logId, 1, "Add");
                    //_logRegServiceTypeMappingRepository.Dispose();
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void UpdateByValue(int regID, int serviceTypeID, string invitationCode)
        {
            try
            {


                var dataModel = (from a in context.Tbl_RegServiceTypeMapping
                            where a.RegID == regID && a.ServiceTypeID == serviceTypeID
                            select a).FirstOrDefault();

                if (dataModel != null)
                {

                    dataModel.InvitationCode = invitationCode;

                    context.SaveChanges();
                    //InsertAddress(model);
                    int logId = dataModel.Id;
                    _logRegServiceTypeMappingRepository.Insert(logId, 1, "Modify");
                }

                //var model = new Tbl_RegServiceTypeMapping()
                //{
                //    RegID = regID,
                //    ServiceTypeID = serviceTypeID,
                //    InvitationCode = invitationCode
                //};

                //if (model != null)
                //{
                //    context.Tbl_RegServiceTypeMapping.Add(model);

                //    context.SaveChanges();

                //    int logId = model.Id;
                //    _logRegServiceTypeMappingRepository.Insert(logId, 1, "Add");
     
                //}
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void RemoveByValue(int regID, int serviceTypeID)
        {
            try
            {
                var model = context.Tbl_RegServiceTypeMapping.Where(m => m.RegID == regID && m.ServiceTypeID == serviceTypeID).FirstOrDefault();

                if (model != null)
                {
                    int logId = model.Id;
                    _logRegServiceTypeMappingRepository.Insert(logId, 1, "Remove");

                    context.Tbl_RegServiceTypeMapping.Remove(model);
                    context.SaveChanges();
                    
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void RemoveByRegID(int regId)
        {
            var data = context.Tbl_RegServiceTypeMapping.Where(a => a.RegID == regId);
            context.Tbl_RegServiceTypeMapping.RemoveRange(data);
            context.SaveChanges();
        }

        public virtual bool CheckExistsServiceMappingByValue(int regID, int serviceTypeID)
        {
            var query = from a in context.Tbl_RegServiceTypeMapping
                        where a.RegID == regID
                        && a.ServiceTypeID == serviceTypeID
                        select a;

            var result = query.ToList();

            if (result.Count() > 0)
            {
                return true;
            }else
            {
                return false;
            }
        }

        public virtual IEnumerable<Tbl_RegServiceTypeMapping> GetRegServiceListByRegID(int regID)
        {
            var query = from a in context.Tbl_RegServiceTypeMapping
                        where a.RegID == regID
                        orderby a.ServiceTypeID
                        select a;

            var result = query.ToList();

            return result;

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }


    }
}
