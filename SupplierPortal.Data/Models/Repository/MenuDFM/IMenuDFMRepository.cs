﻿using SupplierPortal.Data.Models.MappingTable;
using System.Collections.Generic;

namespace SupplierPortal.Data.Models.Repository.MenuDFM
{
    public partial interface IMenuDFMRepository
    {

        Tbl_MenuDFM GetMenuDFMByMenuID(int menuID);
        Tbl_MenuDFM GetFirstOrDefaultMenuDFMByMenu();
    }
}
