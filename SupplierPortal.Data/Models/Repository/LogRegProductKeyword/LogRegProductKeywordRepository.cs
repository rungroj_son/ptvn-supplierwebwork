﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogRegProductKeyword
{
    public partial class LogRegProductKeywordRepository : ILogRegProductKeywordRepository
    {
        private SupplierPortalEntities context;

        public LogRegProductKeywordRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogRegProductKeywordRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }


        public virtual void Insert(int id, int regid, string keyword, string logAction)
        {
            try
            {

                var model = new Tbl_LogRegProductKeyword
                {
                    Id = id,
                    RegID = regid,
                    Keyword = keyword,
                    //OtherBusinessType = _info.OtherBusinessType,
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };

                if (model != null)
                {
                    context.Tbl_LogRegProductKeyword.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
