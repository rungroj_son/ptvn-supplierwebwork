﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.AutoComplete_State
{
    public partial interface IAutoComplete_StateRepository
    {
        IEnumerable<Tbl_AutoComplete_State> GetStateByMany(string countrycode, int languageID);

        IEnumerable<Tbl_AutoComplete_State> GetStateListByCountrycode(string countrycode);

        IQueryable<Tbl_AutoComplete_State> GetStateAllByCountrycodeAndLanguageID(string countrycode, int languageID);

        IQueryable<Tbl_AutoComplete_State> GetStateAllByCountrycode(string countrycode);

        IQueryable<Tbl_AutoComplete_State> GetStateRange(int skip, int take, string filter);

        IEnumerable<Tbl_AutoComplete_State> GetStateList(string term);

        void Dispose();
    }
}
