﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.TrackingRequest
{
    public partial interface ITrackingRequestRepository
    {
        int InsertReturnTrackingReqID(Tbl_TrackingRequest model,int updateBy);

        IQueryable<ViewTrackingRequestModel> GetTrackingRequestWaitingApproveBySupplierID(int supplierID);

        IQueryable<ViewTrackingRequestModel> GetTrackingRequestAllBySupplierID(int supplierID);

        Tbl_TrackingRequest GetTrackingRequestByTrackingReqID(int trackingReqID);

        Tbl_TrackingGroup GetTrackingGroupByTrackingGrpID(int trackingGrpID);

        string GetTrackingStatusNameWaitingApproveByTrackingStatusID(int trackingStatusID);

        void Update(Tbl_TrackingRequest model, int updateBy);

        void Dispose();
    }
}
