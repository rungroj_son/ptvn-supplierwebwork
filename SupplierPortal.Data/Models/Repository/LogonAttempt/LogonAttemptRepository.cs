﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SupplierPortal.Data.Models.SupportModel;


namespace SupplierPortal.Data.Models.Repository.LogonAttempt
{
    public partial class LogonAttemptRepository : ILogonAttemptRepository
    {
        private SupplierPortalEntities context;

        public LogonAttemptRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogonAttemptRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(Tbl_LogonAttempt model)
        {
            try
            {

                if (model != null)
                {

                    context.Tbl_LogonAttempt.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual IEnumerable<Tbl_LogonAttempt> GetLogonAttemptByUsernameAndPeriodTime(string username, DateTime datePeriodCheck)
        {
            try
            {
                var query = from a in context.Tbl_LogonAttempt
                            where a.Username == username
                            && a.isLoginStatus == "Fail"
                            && a.isActive == 1
                            && a.LoginTime >= datePeriodCheck
                            select a;

                var result = query.ToList();

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public virtual bool CheckLockedLogonAttempt(string username, int lockTime)
        {
            try
            {
                bool isLocked = false;

                DateTime dateNow = DateTime.UtcNow;

                var query = from a in context.Tbl_LogonAttempt
                            where a.Username == username
                            && a.isLoginStatus == "Locked"
                            && a.isActive == 1
                            && (dateNow >= a.LoginTime && dateNow <= DbFunctions.AddSeconds(a.LoginTime, lockTime))
                            select a;

                if (query.Count() > 0)
                {
                    isLocked = true;
                }

                return isLocked;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public virtual void Update(Tbl_LogonAttempt model)
        {
            try
            {
                if (model != null)
                {

                    var tbl_LogonAttempt = context.Tbl_LogonAttempt.Find(model.ID);
                    tbl_LogonAttempt = model;
                    context.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public virtual IEnumerable<UserUnlockAccountModel> GetAllUserLock(int languageID, int lockTime)
        {

            DateTime dateNow = DateTime.UtcNow;
            TimeSpan time = new TimeSpan(0, 0, 5, 0);
            DateTime combined = dateNow.Add(time);

            //var query = (from a in context.Tbl_User
            //             join b in context.Tbl_ContactPerson on a.ContactID equals b.ContactID into join_b
            //             from jab in join_b.DefaultIfEmpty()
            //             join c in context.Tbl_Organization on a.SupplierID equals c.SupplierID into join_c
            //             from jac in join_c.DefaultIfEmpty()
            //             join be in context.Tbl_BusinessEntity on jac.BusinessEntityID equals be.Id into join_be
            //             from jbe in join_be.DefaultIfEmpty()
            //             join lp in context.Tbl_LocalizedProperty.Where(w => w.LocaleKeyGroup == "Tbl_BusinessEntity" && w.LocaleKey == "BusinessEntityDisplay" && w.LanguageID == languageID) on jbe.Id equals lp.EntityID into join_lp
            //             from jlp in join_lp.DefaultIfEmpty()
            //             join d in context.Tbl_LogonAttempt.Where(w => w.isLoginStatus == "Locked" && w.isActive == 1 && (dateNow >= w.LoginTime && dateNow <= DbFunctions.AddMinutes(w.LoginTime, lockTime))) on a.Username equals d.Username into join_d
            //             from jad in join_d.DefaultIfEmpty()
            //             select new UserUnlockAccountModel()
            //             {
            //                 //ID = jad.ID,
            //                 Username = a.Username,
            //                 Fullname = languageID == 1 ? jab.FirstName_Inter + " " + jab.LastName_Inter : jab.FirstName_Local + " " + jab.LastName_Local,
            //                 CompanyName = languageID == 1 ? jac.CompanyName_Inter + " " + jlp.LocaleValue : jbe.BusinessEntityDisplay + " " + jac.CompanyName_Local
            //             });

            var query = (from a in context.Tbl_LogonAttempt.Where(w => w.isLoginStatus == "Locked" && w.isActive == 1 && (dateNow >= w.LoginTime && dateNow <= DbFunctions.AddSeconds(w.LoginTime, lockTime)))
                         join b in context.Tbl_User on a.Username equals b.Username into join_b
                         from jab in join_b.DefaultIfEmpty()
                         join c in context.Tbl_ContactPerson on jab.ContactID equals c.ContactID into join_c
                         from jac in join_c.DefaultIfEmpty()
                         join d in context.Tbl_Organization on jab.SupplierID equals d.SupplierID into join_d
                         from jad in join_d.DefaultIfEmpty()
                         join be in context.Tbl_BusinessEntity on jad.BusinessEntityID equals be.Id into join_be
                         from jbe in join_be.DefaultIfEmpty()
                         join lp in context.Tbl_LocalizedProperty.Where(w => w.LocaleKeyGroup == "Tbl_BusinessEntity" && w.LocaleKey == "BusinessEntityDisplay" && w.LanguageID == languageID) on jbe.Id equals lp.EntityID into join_lp
                         from jlp in join_lp.DefaultIfEmpty()
                         select new UserUnlockAccountModel()
                             {
                                 ID = a.ID,
                                 Username = a.Username,
                                 Fullname = languageID == 1 ? jac.FirstName_Inter + " " + jac.LastName_Inter : jac.FirstName_Local + " " + jac.LastName_Local,
                                 CompanyName = languageID == 1 ? jad.CompanyName_Inter + " " + jlp.LocaleValue : jbe.BusinessEntityDisplay + " " + jad.CompanyName_Local
                             });

            return query.ToList();
        }


        public virtual void UnlockAccount(int ID,string updateBy) 
        {
            try
            {
                if (ID != null)
                {

                    var tbl_LogonAttempt = context.Tbl_LogonAttempt.Find(ID);
                    tbl_LogonAttempt.isActive = 0;
                    context.SaveChanges();

                    var logLogAttempt = new Tbl_LogUnlockUser()
                    {   
                        ID = ID,
                        UnblockBy = updateBy,
                        UnblockDate = DateTime.UtcNow,
                        Username = tbl_LogonAttempt.Username
                    };

                    context.Tbl_LogUnlockUser.Add(logLogAttempt);
                    context.SaveChanges();
                    
                }
            }
            catch (Exception)
            {

                throw;
            }

          
        }

        public virtual Tbl_LogonAttempt GetLockedLogonAttempt(string username, int lockTime)
        {
            try
            {

                DateTime dateNow = DateTime.UtcNow;

                var query = from a in context.Tbl_LogonAttempt
                            where a.Username == username
                            && a.isLoginStatus == "Locked"
                            && a.isActive == 1
                            && (dateNow >= a.LoginTime && dateNow <= DbFunctions.AddSeconds(a.LoginTime, lockTime))
                            select a;

                var result = query.FirstOrDefault();

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
