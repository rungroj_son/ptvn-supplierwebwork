﻿using SupplierPortal.Data.Models.MappingTable;
using System.Collections.Generic;
using System;
using SupplierPortal.Data.Models.SupportModel;


namespace SupplierPortal.Data.Models.Repository.LogonAttempt
{
    public partial interface ILogonAttemptRepository
    {
        void Insert(Tbl_LogonAttempt model);

        IEnumerable<Tbl_LogonAttempt> GetLogonAttemptByUsernameAndPeriodTime(string username,DateTime datePeriodCheck);

        bool CheckLockedLogonAttempt(string username, int lockTime);

        void Update(Tbl_LogonAttempt model);

        IEnumerable<UserUnlockAccountModel> GetAllUserLock(int languageID, int lockTime);

        void UnlockAccount(int ID, string updateBy);

        Tbl_LogonAttempt GetLockedLogonAttempt(string username, int lockTime);
    }
}
