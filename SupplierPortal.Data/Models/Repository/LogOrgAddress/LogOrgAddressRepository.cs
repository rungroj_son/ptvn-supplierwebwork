﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgAddress
{
    public partial class LogOrgAddressRepository : ILogOrgAddressRepository
    {
        private SupplierPortalEntities context;

        public LogOrgAddressRepository()
        {
            context = new SupplierPortalEntities();
        }
        public LogOrgAddressRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }
        public void Insert(int AddressID, int AddressTypeID, int UpdateBy, string logAction)
        {
            try
            {
                var LogOrgAdd = context.Tbl_OrgAddress.FirstOrDefault(m => m.AddressID == AddressID && m.AddressTypeID == AddressTypeID);

                if (LogOrgAdd != null)
                {
                    var _logOrg = new Tbl_LogOrgAddress()
                    {
                        SupplierID = LogOrgAdd.SupplierID,
                        AddressID = LogOrgAdd.AddressID,
                        AddressTypeID = LogOrgAdd.AddressTypeID,
                        SeqNo = LogOrgAdd.SeqNo,
                        UpdateBy = UpdateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgAddress.Add(_logOrg);
                    context.SaveChanges();
                }

            }
            catch
            {
                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
