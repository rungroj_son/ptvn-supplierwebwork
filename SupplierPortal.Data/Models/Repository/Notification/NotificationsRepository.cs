﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.Notification
{
    public partial class NotificationsRepository : INotificationsRepository
    {
        private SupplierPortalEntities context;

        public NotificationsRepository()
        {
            context = new SupplierPortalEntities();
        }

        public NotificationsRepository(
           SupplierPortalEntities context
           )
        {
            this.context = context;
        }

        public virtual Tbl_Notification GetDataByNotiID(int NotiID)
        {
            var query = from a in context.Tbl_Notification
                        where a.NotificationID == NotiID
                        select a;
            return query.FirstOrDefault();

        }

        public void changeIsRead(int NotiID) 
        {
            if (NotiID != null)
            {
                var dataNoti = context.Tbl_Notification.Find(NotiID);
                //addressTemp = model;
                dataNoti.isRead = 1;  

                context.SaveChanges();
            }
        }

        public void changeIsDelete(int NotiID)
        {
            if (NotiID != null)
            {
                var dataNoti = context.Tbl_Notification.Find(NotiID);
                //addressTemp = model;
                dataNoti.isDeleted = 1;

                context.SaveChanges();
            }
        }

        public void UpdateStopTimeWhenRead(int NotiID, DateTime stopTime)
        {

            if (NotiID != null)
            {
                var dataNoti = context.Tbl_Notification.Find(NotiID);
                //addressTemp = model;
                dataNoti.StopTime = stopTime;

                context.SaveChanges();
            }
        }

        public void Dispose()
        {
            context.Dispose();
        }

    }
}
