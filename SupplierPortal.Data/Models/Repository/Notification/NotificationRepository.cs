﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.LogNotification;
using System.Data.SqlClient;

namespace SupplierPortal.Data.Models.Repository.Notification
{
    public partial class NotificationRepository : INotificationRepository
    {
        private SupplierPortalEntities context;
        ILogNotificationRepository _logNotificationRepository;

        public NotificationRepository()
        {
            context = new SupplierPortalEntities();
            _logNotificationRepository = new LogNotificationRepository();
        }

        public NotificationRepository(
            SupplierPortalEntities context,
            LogNotificationRepository logNotificationRepository
            )
        {
            this.context = context;
            _logNotificationRepository = logNotificationRepository;
        }

        public virtual IEnumerable<Tbl_Notification> GetNotificationList(string notificationKey, int systemID, int topicID)
        {
            var query = from a in context.Tbl_Notification
                        where a.NotificationKey == notificationKey
                        && a.SystemID == systemID
                        && a.TopicID == topicID
                        select a;

            return query.ToList();
        }

        public virtual IEnumerable<Tbl_Notification> GetNotificationByUserIDAndTopicID(int userID, int topicID)
        {
            var query = from a in context.Tbl_Notification
                        where a.UserID == userID
                        && a.TopicID == topicID
                        where a.isDeleted != 1
                        select a;

            return query.ToList();
        }

        public virtual IEnumerable<Tbl_Notification> GetNotificationByNotificationKeyAndTopicID(string notificationKey, int topicID)
        {
            var query = from a in context.Tbl_Notification
                        where a.NotificationKey == notificationKey
                        && a.TopicID == topicID
                        where a.isDeleted != 1
                        select a;

            return query.ToList();
        }

        public virtual void Insert(Tbl_Notification model, int updateBy)
        {
            try
            {
                int notificationID = 0;

                if (model != null)
                {
                    context.Tbl_Notification.Add(model);
                    context.SaveChanges();

                    notificationID = model.NotificationID;

                    _logNotificationRepository.Insert(notificationID, "Add", updateBy);
                }
            }
            catch
            {
                throw;
            }
        }

        public virtual int InsertReturnNotificationID(Tbl_Notification model, int updateBy)
        {
            try
            {
                int notificationID = 0;

                if (model != null)
                {
                    context.Tbl_Notification.Add(model);
                    context.SaveChanges();

                    notificationID = model.NotificationID;

                    _logNotificationRepository.Insert(notificationID, "Add", updateBy);
                }

                return notificationID;
            }
            catch
            {
                throw;
            }
        }

        public virtual void Delete(int notificationID, int updateBy)
        {
            try
            {
                //var model = context.Tbl_Notification.Where(m => m.NotificationID == notificationID).FirstOrDefault();
                var model = context.Tbl_Notification.Find(notificationID);
                if (model != null)
                {
                    _logNotificationRepository.Insert(model.NotificationID, "Remove", updateBy);
                    context.Tbl_Notification.Remove(model);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            
        }

        public virtual void DeleteRange(List<Tbl_Notification> modelList, int updateBy)
        {
            try
            {
                //var model = context.Tbl_Notification.Where(m => m.NotificationID == notificationID).FirstOrDefault();
                //var model = context.Tbl_Notification.Find(notificationID);
                //if (model != null)
                //{
                foreach (var item in modelList)
                {
                    _logNotificationRepository.Insert(item.NotificationID, "Remove", updateBy);
                }
                context.Tbl_Notification.RemoveRange(modelList);
                context.SaveChanges();

                //}
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void DeleteByCommand(int notificationID, int updateBy)
        {
            try
            {
                //ConnectionStringSettings dependencyConnection = ConfigurationManager.ConnectionStrings["DependencyEntities"];

                using (var sc = new SqlConnection(context.Database.Connection.ConnectionString))
                {
                    using (var cmd = sc.CreateCommand())
                    {
                        sc.Open();
                        cmd.CommandText = "DELETE FROM Tbl_Notification WHERE NotificationID = @notificationID";
                        cmd.Parameters.AddWithValue("@notificationID", notificationID);
                        cmd.ExecuteNonQuery();

                    }
                }
                
            }
            catch (Exception e)
            {
                throw;
            }

        }


        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
