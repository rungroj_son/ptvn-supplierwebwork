﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.Notification
{
    public partial interface INotificationRepository
    {
        IEnumerable<Tbl_Notification> GetNotificationList(string notificationKey,int systemID,int topicID);

        IEnumerable<Tbl_Notification> GetNotificationByUserIDAndTopicID(int userID, int topicID);

        IEnumerable<Tbl_Notification> GetNotificationByNotificationKeyAndTopicID(string notificationKey, int topicID);

        void Insert(Tbl_Notification model, int updateBy);

        int InsertReturnNotificationID(Tbl_Notification model, int updateBy);

        void Delete(int notificationID, int updateBy);

        void DeleteRange(List<Tbl_Notification> modelList, int updateBy);

        void DeleteByCommand(int notificationID, int updateBy);

        void Dispose();
    }
}
