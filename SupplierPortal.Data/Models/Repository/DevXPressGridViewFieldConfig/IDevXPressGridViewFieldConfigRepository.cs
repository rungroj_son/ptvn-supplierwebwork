﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.DevXPressGridViewFieldConfig
{
    public partial interface IDevXPressGridViewFieldConfigRepository
    {
        IEnumerable<Tbl_DevXPressGridViewFieldConfig> GetDevXPressGridViewFieldConfigByGridName(string gridName);

        void Dispose();
    }
}
