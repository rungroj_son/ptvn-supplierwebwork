﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.DevXPressGridViewFieldConfig
{
    public partial class DevXPressGridViewFieldConfigRepository : IDevXPressGridViewFieldConfigRepository
    {
        private SupplierPortalEntities context;

        public DevXPressGridViewFieldConfigRepository()
        {
            context = new SupplierPortalEntities();
        }

        public DevXPressGridViewFieldConfigRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<Tbl_DevXPressGridViewFieldConfig> GetDevXPressGridViewFieldConfigByGridName(string gridName)
        {
            var query = from a in context.Tbl_DevXPressGridViewFieldConfig
                        where a.GridName == gridName
                        select a;

            var result = query.ToList().OrderBy(m => m.DisplayOrder);

            return result;
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
