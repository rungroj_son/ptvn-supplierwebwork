﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;



namespace SupplierPortal.Data.Models.Repository.ADSBuyer
{
    public partial class ADSBuyerRepository : IADSBuyerRepository
    {
        private SupplierPortalEntities context;

        public ADSBuyerRepository()
        {
            context = new SupplierPortalEntities();
        }

        public ADSBuyerRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual string GetBuyerOrgIDByInvitationCode(string invitationCode)
        {
            string buyerOrgID = "";

            DateTime now = DateTime.UtcNow;

            var query = from a in context.Tbl_ADSBuyer
                        where a.InvitationCode == invitationCode
                        && (now >= a.EffectiveDate && now <= a.ExpireDate)
                        select a;

            var result = query.FirstOrDefault();

            if(result != null)
            {
                buyerOrgID = result.BuyerOrgID;
            }

            return buyerOrgID;

        }
    }
}
