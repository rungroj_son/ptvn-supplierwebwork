﻿using System.Collections.Generic;
using System.Linq;


namespace SupplierPortal.Data.Models.Repository.ADSBuyer
{
    public partial interface IADSBuyerRepository
    {
        string GetBuyerOrgIDByInvitationCode(string invitationCode);
    }
}
