﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgContactAddressMapping
{
    public partial interface ILogOrgContactAddressMappingRepository
    {
        void Insert(int id, string logAction, int updateBy);

        void Dispose();
    }
}
