﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogRegProductKeyword;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.RegProductKeyword
{
    public partial class RegProductKeywordRepository : IRegProductKeywordRepository
    {
        private SupplierPortalEntities context;
        ILogRegProductKeywordRepository _logRegProductKeywordRepository;

        public RegProductKeywordRepository()
        {
            context = new SupplierPortalEntities();
            _logRegProductKeywordRepository = new LogRegProductKeywordRepository();
        }

        public RegProductKeywordRepository(
            SupplierPortalEntities ctx,
             LogRegProductKeywordRepository logRegProductKeywordRepository
            )
        {
            this.context = ctx;
            _logRegProductKeywordRepository = logRegProductKeywordRepository;
        }

        public virtual void Insert(Tbl_RegProductKeyword model)
        {
            int RegID = 0;

            try
            {
                context.Tbl_RegProductKeyword.Add(model);
                context.SaveChanges();

                RegID = model.RegID ?? 0;

                _logRegProductKeywordRepository.Insert(model.Id,RegID,model.Keyword, "Add");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Edit(Tbl_RegProductKeyword model)
        {
            int RegID = 0;

            try
            {
                context.Tbl_RegProductKeyword.Add(model);
                context.SaveChanges();

                RegID = model.RegID ?? 0;

                _logRegProductKeywordRepository.Insert(model.Id, RegID, model.Keyword, "Edit");
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual IEnumerable<Tbl_RegProductKeyword> GetRegProductKeywordByRegID(int regid)
        {
            return (from list in context.Tbl_RegProductKeyword
                    where list.RegID == regid
                    select list).ToList<Tbl_RegProductKeyword>();
        }

        public void removeKeyWordList(IEnumerable<Tbl_RegProductKeyword> Data)
        {
            foreach (var item in Data)
            {
                context.Tbl_RegProductKeyword.Remove(item);
                context.SaveChanges();
            }

        }

        public void RemoveByRegID(int regId)
        {
            var data = context.Tbl_RegProductKeyword.Where(a => a.RegID == regId);
            context.Tbl_RegProductKeyword.RemoveRange(data);
            context.SaveChanges();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
