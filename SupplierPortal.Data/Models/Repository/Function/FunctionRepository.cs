﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.Function
{
    public partial class FunctionRepository : IFunctionRepository
    {
        private SupplierPortalEntities context;

        public FunctionRepository()
        {
            context = new SupplierPortalEntities();
        }

        public FunctionRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual Boolean GetResultByQuery(string sqlQuery)
        {
            bool _result = false;

            try
            {
              var resultQuery  = context.Database.SqlQuery<int>(sqlQuery).FirstOrDefault();

              if (Convert.ToBoolean(resultQuery))
                {
                    _result = Convert.ToBoolean(resultQuery);
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return _result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
