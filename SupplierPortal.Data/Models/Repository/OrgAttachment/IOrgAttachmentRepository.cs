﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OrgAttachment
{
    public partial interface IOrgAttachmentRepository
    {
        OrgAttachmentListModels GetOrgAttachmentListBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID, int companyTypeID);

        IEnumerable<Tbl_OrgAttachment> GetOrgAttachmentBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID);

        Tbl_OrgAttachment GetOrgAttachmentByAttachmentID(int attachmentID);

        bool OrgAttachmentExists(string guidName, int supplierID);

        bool OrgAttachmentExistsFileName(string fileName, int supplierID);

        bool OrgAttachmentExistsByDocumentNameID(int documentNameID, int supplierID);

        void Insert(Tbl_OrgAttachment model);

        void Insert(Tbl_OrgAttachment model, int updateBy);

        int InsertReturnAttachmentID(Tbl_OrgAttachment model, int updateBy);

        void Delete(int supplierID, int attachmentid);

        Tbl_OrgAttachment GetOrgAttachmentByGuidName(string guidName);

        IEnumerable<Tbl_OrgAttachment> GetOrgAttachmentBySupplierIDAndDocumentNameID(int supplierID, int documentNameID);

        int GetOrgAttachmentCountBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID);

        void DeleteUpdateFlag(int supplierID, int attachmentid, int updateBy);

        void Dispose();
        IEnumerable<Tbl_OrgAttachment> GetOrgAttachmentBySupplierIDAndDocumentNameIDAndDocumentNameID(int supplierID, int v1, int v2);
    }
}
