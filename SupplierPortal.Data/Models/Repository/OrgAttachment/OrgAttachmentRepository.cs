﻿using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SupplierPortal.Data.Models.Repository.LogOrgAttachment;

namespace SupplierPortal.Data.Models.Repository.OrgAttachment
{
    public partial class OrgAttachmentRepository : IOrgAttachmentRepository
    {
        private SupplierPortalEntities context;
        ILogOrgAttachmentRepository _logOrgAttachmentRepository;

        public OrgAttachmentRepository()
        {
            context = new SupplierPortalEntities();
            _logOrgAttachmentRepository = new LogOrgAttachmentRepository();
        }

        public OrgAttachmentRepository(
            SupplierPortalEntities context,
            LogOrgAttachmentRepository logOrgAttachmentRepository

            )
        {
            this.context = context;
            _logOrgAttachmentRepository = logOrgAttachmentRepository;
        }

        public virtual OrgAttachmentListModels GetOrgAttachmentListBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID, int companyTypeID)
        {

            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            OrgAttachmentListModels models = new OrgAttachmentListModels();


            var query1 = from a in context.Tbl_OrgAttachment
                         join b in context.Tbl_CustomerDocName on a.DocumentNameID equals b.DocumentNameID
                         where a.SupplierID == supplierID
                         && b.DocumentTypeID == documentTypeID
                         && a.isDeleted != 1
                         select a;

            var result1 = query1.ToList();

            var query2 = (from a in context.Tbl_CustomerDocName
                          where a.DocumentTypeID == documentTypeID
                          select new CustomerDocNameModels
                          {
                              DocumentNameID = a.DocumentNameID,
                              DocumentName = (from lp in context.Tbl_LocalizedProperty
                                              where lp.EntityID == a.DocumentNameID
                                              && lp.LocaleKeyGroup == "Tbl_CustomerDocName"
                                              && lp.LocaleKey == "DocumentName"
                                              && lp.LanguageID == languageId
                                              select lp.LocaleValue).FirstOrDefault() ?? a.DocumentName,
                              SeqNo = a.SeqNo ?? 0,
                              DocumentTypeID = a.DocumentTypeID ?? 0
                          }).Distinct();



            var result2 = query2.ToList();



            //models.OrgAttachment = result1;
            models.DocumentName = result2;
            models.CompanyTypeID = companyTypeID;


            return models;
        }

        public virtual IEnumerable<Tbl_OrgAttachment> GetOrgAttachmentBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID)
        {
            var query = from a in context.Tbl_OrgAttachment
                         join b in context.Tbl_CustomerDocName on a.DocumentNameID equals b.DocumentNameID
                         where a.SupplierID == supplierID
                         && b.DocumentTypeID == documentTypeID
                         && a.isDeleted != 1
                         select a;

            var result = query.ToList();

            return result;
        }

        public virtual IEnumerable<Tbl_OrgAttachment> GetOrgAttachmentBySupplierIDAndDocumentNameIDAndDocumentNameID(int supplierID, int docNameId, int docNameId2)
        {
            var query = from a in context.Tbl_OrgAttachment
                        join b in context.Tbl_CustomerDocName on a.DocumentNameID equals b.DocumentNameID
                        where a.SupplierID == supplierID
                        && (b.DocumentNameID == docNameId || b.DocumentNameID == docNameId2)
                        && a.isDeleted != 1
                        select a;

            var result = query.ToList();

            return result;
        }

        public virtual Tbl_OrgAttachment GetOrgAttachmentByAttachmentID(int attachmentID)
        {
            var query = from a in context.Tbl_OrgAttachment
                        where a.AttachmentID == attachmentID
                        select a;

            var result = query.FirstOrDefault();

            return result;
        }

        public virtual bool OrgAttachmentExists(string guidName, int supplierID)
        {
            var count = context.Tbl_OrgAttachment.Count(m => m.AttachmentNameUnique == guidName && m.SupplierID == supplierID && m.isDeleted != 1);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual bool OrgAttachmentExistsFileName(string fileName, int supplierID)
        {
            var count = context.Tbl_OrgAttachment.Count(m => m.AttachmentName == fileName && m.SupplierID == supplierID && m.isDeleted != 1);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual bool OrgAttachmentExistsByDocumentNameID(int documentNameID, int supplierID)
        {
            var count = context.Tbl_OrgAttachment.Count(m => m.DocumentNameID == documentNameID && m.SupplierID == supplierID && m.isDeleted != 1);

            if (count > 0)
            {
                return true;
            }

            return false;
        }

        public virtual void Insert(Tbl_OrgAttachment tbl_OrgAttachment)
        {
            try
            {

                if (tbl_OrgAttachment != null)
                {

                    context.Tbl_OrgAttachment.Add(tbl_OrgAttachment);
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void Insert(Tbl_OrgAttachment tbl_OrgAttachment, int updateBy)
        {
            try
            {

                if (tbl_OrgAttachment != null)
                {

                    context.Tbl_OrgAttachment.Add(tbl_OrgAttachment);
                    context.SaveChanges();

                    _logOrgAttachmentRepository.Insert(tbl_OrgAttachment.SupplierID, tbl_OrgAttachment.AttachmentID, "Add", updateBy);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual int InsertReturnAttachmentID(Tbl_OrgAttachment model, int updateBy)
        {
            try
            {
                int attachmentID = 0;

                if (model != null)
                {

                    context.Tbl_OrgAttachment.Add(model);
                    context.SaveChanges();

                    attachmentID = model.AttachmentID;

                    _logOrgAttachmentRepository.Insert(model.SupplierID, model.AttachmentID, "Add", updateBy);
                }

                return attachmentID;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Delete(int supplierID, int attachmentid)
        {
            try
            {
                var regAttachmentTmp = context.Tbl_OrgAttachment.Find(supplierID, attachmentid);

                if (regAttachmentTmp != null)
                {

                    context.Tbl_OrgAttachment.Remove(regAttachmentTmp);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual Tbl_OrgAttachment GetOrgAttachmentByGuidName(string guidName)
        {

            var query = from db in context.Tbl_OrgAttachment
                        where db.AttachmentNameUnique == guidName
                        && db.isDeleted != 1
                        select db;


            var result = query.FirstOrDefault();

            return result;
        }

        public virtual IEnumerable<Tbl_OrgAttachment> GetOrgAttachmentBySupplierIDAndDocumentNameID(int supplierID, int documentNameID)
        {

            var query = from a in context.Tbl_OrgAttachment
                        where a.SupplierID == supplierID
                        && a.DocumentNameID == documentNameID
                        && a.isDeleted != 1
                        select a;


            var result = query.ToList();

            return result;
        }

        public virtual int GetOrgAttachmentCountBySupplierIDAndDocumentTypeID(int supplierID, int documentTypeID)
        {
            var query = from a in context.Tbl_OrgAttachment
                         join b in context.Tbl_CustomerDocName on a.DocumentNameID equals b.DocumentNameID
                         where a.SupplierID == supplierID
                         && b.DocumentTypeID == documentTypeID
                         && a.isDeleted != 1
                         select a;

            var result = query.Count();

            return result;
        }

        public virtual void DeleteUpdateFlag(int supplierID, int attachmentid, int updateBy)
        {
            try
            {
                var regAttachmentTmp = context.Tbl_OrgAttachment.Find(supplierID, attachmentid);

                if (regAttachmentTmp != null)
                {

                    regAttachmentTmp.isDeleted = 1;
                    context.SaveChanges();

                    _logOrgAttachmentRepository.Insert(regAttachmentTmp.SupplierID, regAttachmentTmp.AttachmentID, "Delete", updateBy);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
