﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.EmailAccount
{
    public partial interface IEmailAccountRepository
    {
        IEnumerable<Tbl_EmailAccount> GetEmailAccountByEmailAccountID(int id);
        void Dispose();
    }
}
