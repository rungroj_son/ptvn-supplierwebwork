﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.AutoComplete_City
{
    public partial class AutoComplete_CityRepository : IAutoComplete_CityRepository
    {
        private SupplierPortalEntities context;

        public AutoComplete_CityRepository()
        {
            context = new SupplierPortalEntities();
        }

        public AutoComplete_CityRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<Tbl_AutoComplete_City> GetCityByMany(string countrycode, int languageID)
        {
            return (from list in context.Tbl_AutoComplete_City
                    where list.CountryCode == countrycode
                    && list.LanguageID == languageID
                    select list).ToList<Tbl_AutoComplete_City>();
        }

        public virtual IEnumerable<Tbl_AutoComplete_City> GetCityByState(string countrycode, int stateID, int languageID)
        {

            var query = from a in context.Tbl_AutoComplete_City
                        where a.LanguageID == languageID
                        && a.StateID == stateID
                        && a.CountryCode == countrycode
                        select a;

            var result = query.ToList();

            return result;
        }

        public IQueryable<Tbl_AutoComplete_City> GetCityAllByCountrycodeAndLanguageID(string countrycode, int languageID)
        {

            IQueryable<Tbl_AutoComplete_City> query = context.Tbl_AutoComplete_City.Where(m => m.CountryCode == countrycode && m.LanguageID == languageID).OrderBy(c => c.CityName);
            return query;
        }

        public IQueryable<Tbl_AutoComplete_City> GetCityAllByCountrycode(string countrycode)
        {

            IQueryable<Tbl_AutoComplete_City> query = context.Tbl_AutoComplete_City.Where(m => m.CountryCode == countrycode).OrderBy(c => c.CityName);
            return query;
        }

        public IQueryable<Tbl_AutoComplete_City> GetCityRange(int skip, int take, string filter)
        {

            IQueryable<Tbl_AutoComplete_City> query = context.Tbl_AutoComplete_City.Where(c => c.CityName.StartsWith(filter)).OrderBy(c => c.CityName).Skip(skip).Take(take);
            return query;
        }

        public IEnumerable<Tbl_AutoComplete_City> GetCityList(string term) 
        {
            var query = (from a in context.Tbl_AutoComplete_City
                        where a.CityName.ToLower().Contains(term.Trim().ToLower())
                         select a).Take(20);

            var result = query.ToList();

            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
