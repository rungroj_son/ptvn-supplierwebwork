﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgFacility
{
    public partial interface ILogOrgFacilityRepository
    {
        void Insert(int id, int UpdateBy, string logAction);

        void Dispose();
    }
}
