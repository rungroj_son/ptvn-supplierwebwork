﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Profile;

namespace SupplierPortal.Data.Models.Repository.SystemRole
{
    public partial class SystemRepository : ISystemRepository
    {

        private SupplierPortalEntities context;


        public SystemRepository()
        {
            context = new SupplierPortalEntities();
        }

        public SystemRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        //public virtual IEnumerable<SystemRoleListModels> GetSystemListByUsernameForUpdate(string username)
        //{
        //    var query = from a in context.Tbl_UserSystemMapping
        //                join b in context.Tbl_System on a.SystemID equals b.SystemID into c
        //                from d in c.DefaultIfEmpty()
        //                join e in context.Tbl_User on a.UserID equals e.UserID into f
        //                from g in f.DefaultIfEmpty()
        //                join h in context.Tbl_Organization on g.OrgID equals h.OrgID into i
        //                from j in i.Where(x=>x.isDeleted != 1).DefaultIfEmpty()
        //                join k in context.Tbl_OrgSystemMapping on j.SupplierID equals k.SupplierID into l
        //                from m in l.Where(x => x.SystemID == a.SystemID).DefaultIfEmpty()
        //                where g.Username == username
        //                && d.IsActive == 1
        //                && d.IsShowOnList == 1
        //                && d.IsUpdateSysUser == 1
        //                && m.isAllowConnecting == 1
        //                select new SystemRoleListModels
        //                {
        //                    Username = username,
        //                    SystemID = a.SystemID,
        //                    SystemName = d.SystemName,
        //                    Description = d.Description,
        //                    SystemGrpID = d.SystemGrpID,
        //                    IsShowOnList = d.IsShowOnList,
        //                    IsShowRoleList = d.IsShowRoleList,
        //                    API_URL = d.API_URL,
        //                    API_Key = d.API_Key,
        //                    //APIName_InsertUser = d.APIName_InsertUser,
        //                    //APIName_UpdateUser = d.APIName_UpdateUser,
        //                    //APIName_UpdateSupplier = d.APIName_UpdateSupplier,
        //                    IsChecked = a.isEnableService ?? 0,
        //                    SysRoleID = "PrimaryUser"
        //                };

        //    //SysRoleID = "PrimaryUser" ฟิกไว้เพื่อไป Get ค่า SysRoleID ของ User ที่เป็น Primary กรณีที่ต้อง call update ใน user ที่มาจากการ merge OP ซึ่งอาจมาหลาย SysRoleID

        //    var result = query.Distinct().ToList();

        //    return result;
        //}

        //public virtual IEnumerable<SystemRoleListModels> GetSystemListByUsernameAndSystemIDForUpdate(string username,int systemID)
        //{
        //    var query = from a in context.Tbl_UserSystemMapping
        //                join b in context.Tbl_System on a.SystemID equals b.SystemID into c
        //                from d in c.DefaultIfEmpty()
        //                join e in context.Tbl_User on a.UserID equals e.UserID into f
        //                from g in f.DefaultIfEmpty()
        //                join h in context.Tbl_Organization on g.OrgID equals h.OrgID into i
        //                from j in i.Where(x=>x.isDeleted != 1).DefaultIfEmpty()
        //                join k in context.Tbl_OrgSystemMapping on j.SupplierID equals k.SupplierID into l
        //                from m in l.Where(x => x.SystemID == a.SystemID).DefaultIfEmpty()
        //                where g.Username == username
        //                && d.IsActive == 1
        //                && d.IsShowOnList == 1
        //                && (d.IsInsertSysUser == 1 || d.IsUpdateSysUser == 1)
        //                && m.isAllowConnecting == 1
        //                && a.SystemID == systemID
        //                select new SystemRoleListModels
        //                {
        //                    Username = username,
        //                    SystemID = a.SystemID,
        //                    SystemName = d.SystemName,
        //                    Description = d.Description,
        //                    SystemGrpID = d.SystemGrpID,
        //                    IsShowOnList = d.IsShowOnList,
        //                    IsShowRoleList = d.IsShowRoleList,
        //                    API_URL = d.API_URL,
        //                    API_Key = d.API_Key,
        //                    //APIName_InsertUser = d.APIName_InsertUser,
        //                    //APIName_UpdateUser = d.APIName_UpdateUser,
        //                    //APIName_UpdateSupplier = d.APIName_UpdateSupplier,
        //                    IsChecked = a.isEnableService ?? 0,
        //                    SysRoleID = a.SysRoleID.ToString()
        //                };

        //    var result = query.Distinct().ToList();

        //    return result;
        //}

        public virtual IEnumerable<SystemRoles_sel_Result> GetSystemRoleSelectList(int systemID, int sysRoleID)
        {
            
            var query = context.SystemRoles_sel(systemID, sysRoleID);

            var result = query.ToList();

            return result;

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
