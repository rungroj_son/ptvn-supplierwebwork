﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogAddress
{
    public partial interface ILogAddressRepository
    {

        void Insert(int addrid, string logAction,int updateBy);

        void Dispose();
    }
}
