﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.LogAddress
{
    public partial class LogAddressRepository : ILogAddressRepository
    {

        private SupplierPortalEntities context;

        public LogAddressRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogAddressRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }


        public virtual void Insert(int addrid, string logAction, int updateBy)
        {
            try
            {
                var _regAddr = context.Tbl_Address.FirstOrDefault(m => m.AddressID == addrid);

                if (_regAddr != null)
                {
                    var _logRegAddress = new Tbl_LogAddress()
                    {
                        AddressID = _regAddr.AddressID,
                        HouseNo_Local = _regAddr.HouseNo_Local,
                        HouseNo_Inter = _regAddr.HouseNo_Inter,
                        VillageNo_Local = _regAddr.VillageNo_Local,
                        VillageNo_Inter = _regAddr.VillageNo_Inter,
                        Lane_Local = _regAddr.Lane_Local,
                        Lane_Inter = _regAddr.Lane_Inter,
                        Road_Local = _regAddr.Road_Local,
                        Road_Inter = _regAddr.Road_Inter,
                        SubDistrict_Local = _regAddr.SubDistrict_Local,
                        SubDistrict_Inter = _regAddr.SubDistrict_Inter,
                        City_Local = _regAddr.City_Local,
                        City_Inter = _regAddr.City_Inter,
                        State_Local = _regAddr.State_Local,
                        State_Inter = _regAddr.State_Inter,
                        CountryCode = _regAddr.CountryCode,
                        PostalCode = _regAddr.PostalCode,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogAddress.Add(_logRegAddress);
                    context.SaveChanges();
                }

            }
            catch
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
