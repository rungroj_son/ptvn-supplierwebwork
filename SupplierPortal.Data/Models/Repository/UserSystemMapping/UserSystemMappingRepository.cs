﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Core.Caching;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogUserSystemMapping;

namespace SupplierPortal.Data.Models.Repository.UserSystemMapping
{
    public partial class UserSystemMappingRepository : IUserSystemMappingRepository
    {

        private SupplierPortalEntities context;
        ILogUserSystemMappingRepository _logUserSystemMappingRepository;

        public UserSystemMappingRepository()
        {
            context = new SupplierPortalEntities();
            _logUserSystemMappingRepository = new LogUserSystemMappingRepository();
        }

        public UserSystemMappingRepository(
            SupplierPortalEntities ctx,
            LogUserSystemMappingRepository logUserSystemMappingRepository
            )
        {
            this.context = ctx;
            _logUserSystemMappingRepository = logUserSystemMappingRepository;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMapping(string username)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username.ToUpper() == username.ToUpper()
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingByUserID(int userID)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        where db.UserID == userID
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingNonMergByUserID(int userID)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        where db.UserID == userID
                        && db.MergeFromUserID == 0
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingByGroup(string username, int systemGrpId)
        {

            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into j
                        from d in j.DefaultIfEmpty()
                        join c in context.Tbl_System on db.SystemID equals c.SystemID
                        where d.Username.ToUpper() == username.ToUpper()
                        && c.SystemGrpID == systemGrpId
                        && c.IsActive == 1
                        && db.MergeFromUserID == 0
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingByGroupNonCheckMerg(string username, int systemGrpId)
        {

            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into j
                        from d in j.DefaultIfEmpty()
                        join c in context.Tbl_System on db.SystemID equals c.SystemID
                        where d.Username.ToUpper() == username.ToUpper()
                        && c.SystemGrpID == systemGrpId
                        && c.IsActive == 1
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingByGroupNonCheckMerg(int userID, int systemGrpId)
        {

            var query = from db in context.Tbl_UserSystemMapping
                        join c in context.Tbl_System on db.SystemID equals c.SystemID
                        where db.UserID == userID
                        && c.SystemGrpID == systemGrpId
                        && c.IsActive == 1
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_OrgSystemMapping> GetUserSystemMappingMenuByGroup(string username, int systemGrpId)
        {

            var query = from a in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on a.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_System on a.SystemID equals e.SystemID into f
                        from g in f.DefaultIfEmpty()
                        join h in context.Tbl_Organization on d.OrgID equals h.OrgID into i
                        from j in i.DefaultIfEmpty()
                        join k in context.Tbl_OrgSystemMapping on j.SupplierID equals k.SupplierID into l
                        from m in l.Where(m => m.SystemID == a.SystemID && m.isCancelService != 1)
                        where d.Username.ToUpper() == username.ToUpper()
                        && g.SystemGrpID == systemGrpId
                        && g.IsActive == 1
                        && j.isDeleted != 1
                        select m;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingEnableService(string username, int systemId)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username.ToUpper() == username.ToUpper()
                        && db.SystemID == systemId
                        && db.isEnableService == 1
                        && db.MergeFromUserID == 0
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingEnableService(int userID, int systemId)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        where db.UserID == userID
                        && db.SystemID == systemId
                        && db.isEnableService == 1
                        && db.MergeFromUserID == 0
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMapping(string username, int systemId)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username.ToUpper() == username.ToUpper()
                        && db.SystemID == systemId
                        && db.MergeFromUserID == 0
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMapping(int userID, int systemId)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        where db.UserID == userID
                        && db.SystemID == systemId
                        && db.MergeFromUserID == 0
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingOrderSysRoleIDASC(int userID, int systemId)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_ACL_SystemRole on db.SysRoleID equals b.SysRoleID into c
                        from d in c.DefaultIfEmpty()
                        where db.UserID == userID
                        && db.SystemID == systemId
                        orderby d.SeqNo ascending, db.MergeFromUserID ascending
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingNonCheckMerg(string username, int systemId)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username.ToUpper() == username.ToUpper()
                        && db.SystemID == systemId
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingNonCheckMergByUserName(string username)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username.ToUpper() == username.ToUpper()
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingNonCheckMerg(int userID, int systemId)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        where db.UserID == userID
                        && db.SystemID == systemId
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_OrgSystemMapping> GetUserSystemMappingMenu(string username, int systemId)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_Organization on d.OrgID equals e.OrgID into f
                        from g in f.DefaultIfEmpty()
                        join h in context.Tbl_OrgSystemMapping on g.SupplierID equals h.SupplierID into i
                        from j in i.Where(m => m.SystemID == db.SystemID).DefaultIfEmpty()
                        where d.Username.ToUpper() == username.ToUpper()
                        && db.SystemID == systemId
                        && g.isDeleted != 1
                        select j;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_OrgSystemMapping> GetUserSystemMappingMenuByUserName(string username)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_Organization on d.OrgID equals e.OrgID into f
                        from g in f.DefaultIfEmpty()
                        join h in context.Tbl_OrgSystemMapping on g.SupplierID equals h.SupplierID into i
                        from j in i.Where(m => m.SystemID == db.SystemID).DefaultIfEmpty()
                        where d.Username.ToUpper() == username.ToUpper()
                        && g.isDeleted != 1
                        select j;
            var result = query.ToList();
            return result;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingMergeAll(string username, int systemId)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username.ToUpper() == username.ToUpper()
                        && db.SystemID == systemId
                        select db;
            var result = query.ToList();
            return result;
        }

        public virtual Tbl_UserSystemMapping GetUserSystemMappingByUsernameAndSystemID(string username, int systemId)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username.ToUpper() == username.ToUpper()
                        && db.SystemID == systemId
                        select db;

            var result = query.FirstOrDefault();
            return result;
        }

        public virtual string GetUserIDFirstOrDefault(string username, int systemId)
        {
            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username.ToUpper() == username.ToUpper()
                        && db.SystemID == systemId
                        select db;
            var result = query.FirstOrDefault();
            if (result != null)
            {
                return result.SysUserID;
            }

            return "";
        }

        public void Insert(Tbl_UserSystemMapping tbl_UserSystemMapping)
        {
            try
            {
                context.Tbl_UserSystemMapping.Add(tbl_UserSystemMapping);

                context.SaveChanges();

                #region  Insert Log
                string userUpdate = "";
                if (System.Web.HttpContext.Current.Session["username"] != null)
                {
                    userUpdate = System.Web.HttpContext.Current.Session["username"].ToString();
                }
                var _user = context.Tbl_User.FirstOrDefault(m => m.Username == userUpdate);
                int userIDUpdateBy = 1;
                if (_user != null)
                {
                    userIDUpdateBy = _user.UserID;
                }
                var _logModel = new Tbl_LogUserSystemMapping()
                {
                    UserID = tbl_UserSystemMapping.UserID,
                    SysUserID = tbl_UserSystemMapping.SysUserID,
                    SysPassword = tbl_UserSystemMapping.SysPassword,
                    SystemID = tbl_UserSystemMapping.SystemID,
                    SysRoleID = tbl_UserSystemMapping.SysRoleID,
                    isEnableService = tbl_UserSystemMapping.isEnableService,
                    MergeFromUserID = tbl_UserSystemMapping.MergeFromUserID,
                    UpdateBy = userIDUpdateBy,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = "Add"
                };
                _logUserSystemMappingRepository.Insert(_logModel);
                #endregion
            }
            catch (Exception e)
            {

                throw;
            }

        }

        public virtual void Update(Tbl_UserSystemMapping tbl_UserSystemMapping)
        {
            try
            {

                if (tbl_UserSystemMapping != null)
                {
                    var useSystemMappingTmp = context.Tbl_UserSystemMapping.Find(tbl_UserSystemMapping.UserID, tbl_UserSystemMapping.SysUserID, tbl_UserSystemMapping.SystemID);

                    if (useSystemMappingTmp != null)
                    {
                        useSystemMappingTmp.SysPassword = tbl_UserSystemMapping.SysPassword;
                        useSystemMappingTmp.SysRoleID = tbl_UserSystemMapping.SysRoleID;
                        useSystemMappingTmp.isEnableService = tbl_UserSystemMapping.isEnableService;
                        useSystemMappingTmp.MergeFromUserID = tbl_UserSystemMapping.MergeFromUserID;

                        context.SaveChanges();

                        #region  Insert Log
                        string userUpdate = "";
                        if (System.Web.HttpContext.Current.Session["username"] != null)
                        {
                            userUpdate = System.Web.HttpContext.Current.Session["username"].ToString();
                        }
                        var _user = context.Tbl_User.FirstOrDefault(m => m.Username == userUpdate);
                        int userIDUpdateBy = 1;
                        if (_user != null)
                        {
                            userIDUpdateBy = _user.UserID;
                        }
                        var _logModel = new Tbl_LogUserSystemMapping()
                        {
                            UserID = tbl_UserSystemMapping.UserID,
                            SysUserID = tbl_UserSystemMapping.SysUserID,
                            SysPassword = tbl_UserSystemMapping.SysPassword,
                            SystemID = tbl_UserSystemMapping.SystemID,
                            SysRoleID = tbl_UserSystemMapping.SysRoleID,
                            isEnableService = tbl_UserSystemMapping.isEnableService,
                            MergeFromUserID = tbl_UserSystemMapping.MergeFromUserID,
                            UpdateBy = userIDUpdateBy,
                            UpdateDate = DateTime.UtcNow,
                            LogAction = "Modify"
                        };
                        _logUserSystemMappingRepository.Insert(_logModel);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void UpdateByMergAccount(Tbl_UserSystemMapping tbl_UserSystemMapping, int masterUserID)
        {
            try
            {

                if (tbl_UserSystemMapping != null)
                {
                    var useSystemMappingTmp = context.Tbl_UserSystemMapping.Find(tbl_UserSystemMapping.UserID, tbl_UserSystemMapping.SysUserID, tbl_UserSystemMapping.SystemID);

                    if (useSystemMappingTmp != null)
                    {
                        var newUseSystemMappingTmp = new Tbl_UserSystemMapping()
                        {
                            UserID = masterUserID,
                            SysUserID = useSystemMappingTmp.SysUserID,
                            SysPassword = useSystemMappingTmp.SysPassword,
                            SystemID = useSystemMappingTmp.SystemID,
                            SysRoleID = useSystemMappingTmp.SysRoleID,
                            isEnableService = useSystemMappingTmp.isEnableService,
                            MergeFromUserID = useSystemMappingTmp.UserID
                        };

                        context.Tbl_UserSystemMapping.Remove(useSystemMappingTmp);
                        context.Tbl_UserSystemMapping.Add(newUseSystemMappingTmp);

                        context.SaveChanges();

                        #region  Insert Log
                        string userUpdate = "";
                        if (System.Web.HttpContext.Current.Session["username"] != null)
                        {
                            userUpdate = System.Web.HttpContext.Current.Session["username"].ToString();
                        }
                        var _user = context.Tbl_User.FirstOrDefault(m => m.Username == userUpdate);
                        int userIDUpdateBy = 1;
                        if (_user != null)
                        {
                            userIDUpdateBy = _user.UserID;
                        }
                        var _logModel = new Tbl_LogUserSystemMapping()
                        {
                            UserID = newUseSystemMappingTmp.UserID,
                            SysUserID = newUseSystemMappingTmp.SysUserID,
                            SysPassword = newUseSystemMappingTmp.SysPassword,
                            SystemID = newUseSystemMappingTmp.SystemID,
                            SysRoleID = newUseSystemMappingTmp.SysRoleID,
                            isEnableService = newUseSystemMappingTmp.isEnableService,
                            MergeFromUserID = newUseSystemMappingTmp.MergeFromUserID,
                            UpdateBy = userIDUpdateBy,
                            UpdateDate = DateTime.UtcNow,
                            LogAction = "ModifyByMerge"
                        };
                        _logUserSystemMappingRepository.Insert(_logModel);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void Delete(Tbl_UserSystemMapping tbl_UserSystemMapping)
        {
            try
            {

                if (tbl_UserSystemMapping != null)
                {
                    var useSystemMappingTmp = context.Tbl_UserSystemMapping.Find(tbl_UserSystemMapping.UserID, tbl_UserSystemMapping.SysUserID, tbl_UserSystemMapping.SystemID);

                    if (useSystemMappingTmp != null)
                    {
                        context.Tbl_UserSystemMapping.Remove(useSystemMappingTmp);

                        #region  Insert Log
                        string userUpdate = "";
                        if (System.Web.HttpContext.Current.Session["username"] != null)
                        {
                            userUpdate = System.Web.HttpContext.Current.Session["username"].ToString();
                        }
                        var _user = context.Tbl_User.FirstOrDefault(m => m.Username == userUpdate);
                        int userIDUpdateBy = 1;
                        if (_user != null)
                        {
                            userIDUpdateBy = _user.UserID;
                        }
                        var _logModel = new Tbl_LogUserSystemMapping()
                        {
                            UserID = useSystemMappingTmp.UserID,
                            SysUserID = useSystemMappingTmp.SysUserID,
                            SysPassword = useSystemMappingTmp.SysPassword,
                            SystemID = useSystemMappingTmp.SystemID,
                            SysRoleID = useSystemMappingTmp.SysRoleID,
                            isEnableService = useSystemMappingTmp.isEnableService,
                            MergeFromUserID = useSystemMappingTmp.MergeFromUserID,
                            UpdateBy = userIDUpdateBy,
                            UpdateDate = DateTime.UtcNow,
                            LogAction = "Remove"
                        };
                        _logUserSystemMappingRepository.Insert(_logModel);
                        #endregion

                        context.SaveChanges();

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual bool CheckExistsUserSystemMappingByUserIDAndSystemID(int userID, int systemID)
        {
            var model = context.Tbl_UserSystemMapping.Where(m => m.UserID == userID && m.SystemID == systemID).ToList();

            if (model.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual int GetSysRoleIDPrimaryUserByUserIDAndSystemID(int userID, int systemID)
        {
            int sysRole = 0;

            var query = from a in context.Tbl_UserSystemMapping
                        where a.UserID == userID
                        && a.SystemID == systemID
                        && a.MergeFromUserID == 0
                        select a;

            var result = query.FirstOrDefault();

            if (result != null)
            {
                sysRole = result.SysRoleID ?? 0;
            }

            return sysRole;
        }

        public virtual IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingBySysUserIDAndSystemIdAndOrgID(string sysUserID, int systemId, string orgID)
        {
            var query = from a in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on a.UserID equals b.UserID
                        where a.SysUserID == sysUserID
                        && a.SystemID == systemId
                        && b.OrgID == orgID
                        select a;

            return query.ToList();
        }

        public void Dispose()
        {
            context.Dispose();
        }



    }
}
