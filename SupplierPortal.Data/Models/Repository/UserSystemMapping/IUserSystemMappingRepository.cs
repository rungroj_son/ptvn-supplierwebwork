﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.UserSystemMapping
{
    public partial interface IUserSystemMappingRepository
    {

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMapping(string username);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingByUserID(int userID);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingNonMergByUserID(int userID);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingByGroup(string username, int systemGrpId);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingByGroupNonCheckMerg(string username, int systemGrpId);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingByGroupNonCheckMerg(int userID, int systemGrpId);

        IEnumerable<Tbl_OrgSystemMapping> GetUserSystemMappingMenuByGroup(string username, int systemGrpId);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMapping(string username, int systemId);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMapping(int userID, int systemId);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingOrderSysRoleIDASC(int userID, int systemId);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingNonCheckMerg(string username, int systemId);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingNonCheckMergByUserName(string username);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingNonCheckMerg(int userID, int systemId);

        IEnumerable<Tbl_OrgSystemMapping> GetUserSystemMappingMenu(string username, int systemId);

        IEnumerable<Tbl_OrgSystemMapping> GetUserSystemMappingMenuByUserName(string username);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingMergeAll(string username, int systemId);

        Tbl_UserSystemMapping GetUserSystemMappingByUsernameAndSystemID(string username, int systemId);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingEnableService(string username, int systemId);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingEnableService(int userID, int systemId);

        string GetUserIDFirstOrDefault(string username, int systemId);

        void Insert(Tbl_UserSystemMapping tbl_UserSystemMapping);

        void Update(Tbl_UserSystemMapping tbl_UserSystemMapping);

        void UpdateByMergAccount(Tbl_UserSystemMapping tbl_UserSystemMapping, int masterUserID);

        void Delete(Tbl_UserSystemMapping tbl_UserSystemMapping);

        bool CheckExistsUserSystemMappingByUserIDAndSystemID(int userID, int systemID);

        int GetSysRoleIDPrimaryUserByUserIDAndSystemID(int userID, int systemID);

        IEnumerable<Tbl_UserSystemMapping> GetUserSystemMappingBySysUserIDAndSystemIdAndOrgID(string sysUserID, int systemId,string orgID);

        void Dispose();
        
    }
}
