﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OPNBuyer
{
    public partial interface IOPNBuyerRepository
    {
        bool verifyInvitationCode(string invitationCode);
    }
}
