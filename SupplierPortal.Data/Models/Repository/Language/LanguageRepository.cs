﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using System.Collections;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.Language
{
    public partial class LanguageRepository : ILanguageRepository
    {
        private SupplierPortalEntities context;

        public LanguageRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LanguageRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<Tbl_Language> List()
        {
            return context.Tbl_Language.ToList();
        }
		
		public virtual IEnumerable<Tbl_Language> ListOrderByDisplayOrder()
        {
            return context.Tbl_Language.OrderBy(m => m.DisplayOrder).ToList();
        }

        public virtual IEnumerable<Tbl_Language> ListOrderByAsc()
        {
            var result = from db in context.Tbl_Language
                         orderby db.DisplayOrder
                         select db;
            return result.ToList();
        }

        public virtual Tbl_Language First()
        {
            return context.Tbl_Language.Where(m => m.isStandard == 1).FirstOrDefault();
        }

        public virtual IEnumerable<string> GetAllLanguageCode()
        {
            var result = from db in context.Tbl_Language.ToList()
                         select db.LanguageCode.ToString();
            return result;
        }

        public virtual IEnumerable<string> GetAllLanguageId()
        {
            var result = from db in context.Tbl_Language.ToList()
                         select db.LanguageID.ToString();
            return result;
        }


        public virtual Tbl_Language GetLanguageById(int languageId)
        {
            return context.Tbl_Language.Find(languageId);
        }

        public virtual string GetLanguageIdByCode(string languageCode)
        {
            string languageID = "";

            var result = context.Tbl_Language.Where(b => b.LanguageCode == languageCode).FirstOrDefault();

            if (result!=null)
            {
                languageID = result.LanguageID.ToString();
            }else
            {
                var result2 = context.Tbl_Language.Where(b => b.isInter ==  1).FirstOrDefault();

                languageID = result2.LanguageID.ToString();
            }

            return languageID;
        }

        public virtual string GetLanguageCodeById(int languageId)
        {
            var result = context.Tbl_Language
                     .Where(b => b.LanguageID == languageId)
                     .FirstOrDefault();
            return result.LanguageCode.ToString();
        }

        public virtual DynamicLanguageModels GetDynamicLanguage()
        {
            DynamicLanguageModels model = new DynamicLanguageModels();

            var query1 = from a in context.Tbl_Language
                         where a.isInter == 1 || a.isLocal == 1
                         orderby a.isInter
                         select a;

            var result1 = query1.ToList();

            var query2 = from a in context.Tbl_Language
                         where a.isInter == 0 && a.isLocal == 0
                         select a;

            var result2 = query2.ToList();

            model.LanguageLocalInter = result1;
            model.LanguageLocalized = result2;

            return model;
        }

        public virtual Tbl_Language GetLocalLanguage()
        {
            return context.Tbl_Language.Where(m => m.isLocal == 1).FirstOrDefault();
        }

        public virtual Tbl_Language GetInterLanguage()
        {
            return context.Tbl_Language.Where(m => m.isInter ==1).FirstOrDefault();
        }

        public void Dispose()
        {
            context.Dispose();
        }

    }
}
