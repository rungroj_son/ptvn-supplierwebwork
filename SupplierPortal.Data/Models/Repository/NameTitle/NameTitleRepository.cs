﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;

namespace SupplierPortal.Data.Models.Repository.NameTitle
{
    public partial class NameTitleRepository : INameTitleRepository
    {
        private SupplierPortalEntities context;

        public NameTitleRepository()
        {
            context = new SupplierPortalEntities();
        }

        public NameTitleRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<TempForMultiLang> GetMultiLangNameTitleByLangid(int languageID)
        {
          

            var query = from a in context.Tbl_NameTitle
                        orderby a.SeqNo
                        select new TempForMultiLang
                        {
                            EntityID = a.TitleID,
                            EntityValue = (from lp in context.Tbl_LocalizedProperty
                                            where lp.EntityID == a.TitleID
                                           && lp.LocaleKeyGroup == "Tbl_NameTitle"
                                           && lp.LocaleKey == "TitleName"
                                           && lp.LanguageID == languageID
                                           select lp.LocaleValue).FirstOrDefault() ?? a.TitleName
                        };

            var result = query.ToList();

            return result;
        }

        public virtual string GetTitleNameByIdAndLanguageID(int titleID,int languageID)
        {
            var query = from a in context.Tbl_NameTitle
                        where a.TitleID == titleID
                        select new
                        {
                            EntityValue = (from lp in context.Tbl_LocalizedProperty
                                            where lp.EntityID == a.TitleID
                                           && lp.LocaleKeyGroup == "Tbl_NameTitle"
                                           && lp.LocaleKey == "TitleName"
                                           && lp.LanguageID == languageID
                                           select lp.LocaleValue).FirstOrDefault() ?? a.TitleName
                        };

            var result = query.FirstOrDefault();

            return result.EntityValue.ToString();
        }
		public virtual IEnumerable<Tbl_NameTitle> GetNameTitleList(int languageID)
        {
            //var query1 = from db in context.Tbl_LocalizedProperty
            //             where db.LanguageID == languageID
            //             && db.LocaleKeyGroup == "Tbl_NameTitle"
            //             && db.LocaleKey == "TitleName"
            //             select new Tbl_NameTitle
            //             {
            //                 //TitleID = db.EntityID ?? default(int),
            //                 TitleID = db.EntityID ?? default(int),
            //                 TitleName = db.LocaleValue
            //             };

            //return query1.ToList();
            var query1 = (from db in context.Tbl_LocalizedProperty
                          where db.LanguageID == languageID
                          && db.LocaleKeyGroup == "Tbl_NameTitle"
                          && db.LocaleKey == "TitleName"
                          select new { TitleID = db.EntityID, TitleName = db.LocaleValue }).ToList()
           .Select(x => new Tbl_NameTitle { TitleID = x.TitleID ?? default(int), TitleName = x.TitleName });

           // return (from db in context.Tbl_LocalizedProperty
           //         where db.LanguageID == languageID
           //         && db.LocaleKeyGroup == "Tbl_NameTitle"
           //         && db.LocaleKey == "TitleName"
           //         select new { TitleID = db.EntityID, TitleName = db.LocaleValue}).ToList()
           //.Select(x => new Tbl_NameTitle { TitleID = x.TitleID ?? default(int), TitleName = x.TitleName });

            if (query1.Count() > 0)
            {
                return query1;
            }

            var query2 = from db in context.Tbl_NameTitle
                         select db;



            return query2.ToList();

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
