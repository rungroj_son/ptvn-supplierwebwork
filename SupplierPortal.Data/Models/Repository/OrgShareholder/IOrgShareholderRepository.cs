﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OrgShareholder
{
    public partial interface IOrgShareholderRepository
    {
        List<Tbl_OrgShareholder> GetDataBySupplierID(int SupplierID);
        Tbl_OrgShareholder GetDataByID(int id);
        void InsertShareholder(Tbl_OrgShareholder model);
        void UpdateShareholder(Tbl_OrgShareholder model);
        void DeleteSharehoder(Tbl_OrgShareholder model);
        void Dispose();
    }
}
