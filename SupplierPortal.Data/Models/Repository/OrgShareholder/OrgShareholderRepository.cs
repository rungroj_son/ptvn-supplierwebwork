﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogOrgShareholder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OrgShareholder
{
    public partial class OrgShareholderRepository : IOrgShareholderRepository
    {
        private SupplierPortalEntities context;
        ILogOrgShareholderRepository _logOrgShareholderRepository;

        public OrgShareholderRepository()
        {
            context = new SupplierPortalEntities();
            _logOrgShareholderRepository = new LogOrgShareholderRepository();
        }

        public OrgShareholderRepository
            (
            SupplierPortalEntities context,
            LogOrgShareholderRepository logOrgShareholderRepository
            )
        {
            this.context = context;
            _logOrgShareholderRepository = logOrgShareholderRepository;
        }


        public virtual List<Tbl_OrgShareholder> GetDataBySupplierID(int SupplierID)
        {

            return context.Tbl_OrgShareholder.Where(w => w.SupplierID == SupplierID).ToList();
        }

        public virtual Tbl_OrgShareholder GetDataByID(int id)
        {
            return context.Tbl_OrgShareholder.Where(w => w.Id == id).FirstOrDefault();
        }

        public virtual void InsertShareholder(Tbl_OrgShareholder model)
        {
            try
            {
                //   if (tempModel != null)
                // {
                var orgAdd = new Tbl_OrgShareholder()
                {
                    SupplierID = model.SupplierID,
                    SeqNo = model.SeqNo,
                    FirstName_Local = model.FirstName_Local,
                    FirstName_Inter = model.FirstName_Inter,
                    LastName_Local = model.LastName_Local,
                    LastName_Inter = model.LastName_Inter,
                    CountyCode = model.CountyCode,
                    IdentityNo = model.IdentityNo,
                    TitleID = model.TitleID
                };

                // context.Tbl_OrgShareholder.Remove(tempModel);

                context.Tbl_OrgShareholder.Add(orgAdd);
                context.SaveChanges();
                //InsertAddress(model);

                _logOrgShareholderRepository.Insert(orgAdd.Id, orgAdd.SupplierID, "Add");
                //   }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void UpdateShareholder(Tbl_OrgShareholder model)
        {
            try
            {
                //   if (tempModel != null)
                // {
                //var orgAdd = context.Tbl_OrgShareholder.Where(w => w.Id == model.Id).FirstOrDefault();

                //orgAdd.SupplierID = model.SupplierID;
                //orgAdd.SeqNo = model.SeqNo;
                //orgAdd.FirstName_Local = model.FirstName_Local;
                //orgAdd.FirstName_Inter = model.FirstName_Inter;
                //orgAdd.LastName_Local = model.LastName_Local;
                //orgAdd.LastName_Inter = model.LastName_Inter;
                //orgAdd.CountyCode = model.CountyCode;
                //orgAdd.IdentityNo = model.IdentityNo;
                //orgAdd.TitleID = model.TitleID;


                // context.Tbl_OrgShareholder.Remove(tempModel);

                //context.Tbl_OrgShareholder.Add(orgAdd);
                context.SaveChanges();
                //InsertAddress(model);

                _logOrgShareholderRepository.Insert(model.Id, model.SupplierID, "Modify");
                //   }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void DeleteSharehoder(Tbl_OrgShareholder model)
        {
            try
            {
                context.Tbl_OrgShareholder.Remove(model);
                context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
