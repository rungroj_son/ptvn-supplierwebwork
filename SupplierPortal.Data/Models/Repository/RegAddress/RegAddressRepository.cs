﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LogRegAddress;

namespace SupplierPortal.Data.Models.Repository.RegAddress
{
    public partial class RegAddressRepository : IRegAddressRepository
    {
        private SupplierPortalEntities context;
        ILogRegAddressRepository _logRegAddressRepository;

        public RegAddressRepository()
        {
            context = new SupplierPortalEntities();
            _logRegAddressRepository = new LogRegAddressRepository();
        }

        public RegAddressRepository(
            SupplierPortalEntities context,
            LogRegAddressRepository  logRegAddressRepository
            )
        {
            this.context = context;
            _logRegAddressRepository = logRegAddressRepository;
        }

        public virtual IEnumerable<Tbl_RegAddress> GetRegAddressByaddrid(int addrid)
        {
            return (from list in context.Tbl_RegAddress
                    where list.AddressID == addrid
                    select list).ToList<Tbl_RegAddress>();
        }

        public virtual Tbl_RegAddress GetRegAddressMappingByContactID(int contactID)
        {
            var qurey = from a in context.Tbl_RegAddress
                        join b in context.Tbl_RegContactAddressMapping on a.AddressID equals b.AddressID
                        where b.ContactID == contactID
                        select a;

            var result = qurey.FirstOrDefault();

            return result;

        }
        public virtual int InsertReturnAddressID(Tbl_RegAddress model)
        {
            try
            {
                int addressID = 0;

                if (model != null)
                {
                    context.Tbl_RegAddress.Add(model);
                    context.SaveChanges();

                    addressID = model.AddressID;

                    _logRegAddressRepository.Insert(addressID, "Add");
                    //_logRegAddressRepository.Dispose();
                }

                return addressID;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void UpdateRegAddress(Tbl_RegAddress model)
        {
            try
            {

                if (model != null)
                {

                    var regAddressTemp = context.Tbl_RegAddress.Find(model.AddressID);
                    regAddressTemp = model;
                    context.SaveChanges();

                    _logRegAddressRepository.Insert(regAddressTemp.AddressID, "Modify");
                    _logRegAddressRepository.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void InsertAddress(RegisContinuePortalModel _model, bool _iscompaddr, out int _addrid)
        {
            int _Addrid = 0;
            try
            {
                var _regAddress = new Tbl_RegAddress()
                    {
                        HouseNo_Local = _iscompaddr == true ? _model.RegCompanyAddress_HouseNo_Local : _model.RegDeliverAddress_HouseNo_Local,
                        HouseNo_Inter = _iscompaddr == true ? _model.RegCompanyAddress_HouseNo_Inter : _model.RegDeliverAddress_HouseNo_Inter,
                        VillageNo_Local = _iscompaddr == true ? _model.RegCompanyAddress_VillageNo_Local : _model.RegDeliverAddress_VillageNo_Local,
                        VillageNo_Inter = _iscompaddr == true ? _model.RegCompanyAddress_VillageNo_Inter : _model.RegDeliverAddress_VillageNo_Inter,
                        Lane_Local = _iscompaddr == true ? _model.RegCompanyAddress_Lane_Local : _model.RegDeliverAddress_Lane_Local,
                        Lane_Inter = _iscompaddr == true ? _model.RegCompanyAddress_Lane_Inter : _model.RegDeliverAddress_Lane_Inter,
                        Road_Local = _iscompaddr == true ? _model.RegCompanyAddress_Road_Local : _model.RegDeliverAddress_Road_Local,
                        Road_Inter = _iscompaddr == true ? _model.RegCompanyAddress_Road_Inter : _model.RegDeliverAddress_Road_Inter,
                        SubDistrict_Local = _iscompaddr == true ? _model.RegCompanyAddress_SubDistrict_Local : _model.RegDeliverAddress_SubDistrict_Local,
                        SubDistrict_Inter = _iscompaddr == true ? _model.RegCompanyAddress_SubDistrict_Inter : _model.RegDeliverAddress_SubDistrict_Inter,
                        City_Local = _iscompaddr == true ? _model.RegCompanyAddress_City_Local : _model.RegDeliverAddress_City_Local,
                        City_Inter = _iscompaddr == true ? _model.RegCompanyAddress_City_Inter : _model.RegDeliverAddress_City_Inter,
                        State_Local = _iscompaddr == true ? _model.RegCompanyAddress_State_Local : _model.RegDeliverAddress_State_Local,
                        State_Inter = _iscompaddr == true ? _model.RegCompanyAddress_State_Inter : _model.RegDeliverAddress_State_Inter,
                        CountryCode = _iscompaddr == true ? _model.RegCompanyAddress_CountryCode : _model.RegDeliverAddress_CountryCode,
                        PostalCode = _iscompaddr == true ? _model.RegCompanyAddress_PostalCode : _model.RegDeliverAddress_PostalCode
                    };
                if (_regAddress != null)
                {
                    context.Tbl_RegAddress.Add(_regAddress);
                    context.SaveChanges();
                    
                    _Addrid = _regAddress.AddressID;

                    ILogRegAddressRepository _logRegAddrRepo = new LogRegAddressRepository();
                    _logRegAddrRepo.Insert(_Addrid, "Add");
                    _logRegAddrRepo.Dispose();
                }
                _addrid = _Addrid;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual bool UpdateAddress(RegisContinuePortalModel _model, int _addrid, bool _iscompaddr)
        {
            try
            {
                var model = context.Tbl_RegAddress.FirstOrDefault(m => m.AddressID == _addrid);
                model.HouseNo_Local = _iscompaddr == true ? _model.RegCompanyAddress_HouseNo_Local : _model.RegDeliverAddress_HouseNo_Local;
                model.HouseNo_Inter = _iscompaddr == true ? _model.RegCompanyAddress_HouseNo_Inter : _model.RegDeliverAddress_HouseNo_Inter;
                model.VillageNo_Local = _iscompaddr == true ? _model.RegCompanyAddress_VillageNo_Local : _model.RegDeliverAddress_VillageNo_Local;
                model.VillageNo_Inter = _iscompaddr == true ? _model.RegCompanyAddress_VillageNo_Inter : _model.RegDeliverAddress_VillageNo_Inter;
                model.Lane_Local = _iscompaddr == true ? _model.RegCompanyAddress_Lane_Local : _model.RegDeliverAddress_Lane_Local;
                model.Lane_Inter = _iscompaddr == true ? _model.RegCompanyAddress_Lane_Inter : _model.RegDeliverAddress_Lane_Inter;
                model.Road_Local = _iscompaddr == true ? _model.RegCompanyAddress_Road_Local : _model.RegDeliverAddress_Road_Local;
                model.Road_Inter = _iscompaddr == true ? _model.RegCompanyAddress_Road_Inter : _model.RegDeliverAddress_Road_Inter;
                model.SubDistrict_Local = _iscompaddr == true ? _model.RegCompanyAddress_SubDistrict_Local : _model.RegDeliverAddress_SubDistrict_Local;
                model.SubDistrict_Inter = _iscompaddr == true ? _model.RegCompanyAddress_SubDistrict_Inter : _model.RegDeliverAddress_SubDistrict_Inter;
                model.City_Local = _iscompaddr == true ? _model.RegCompanyAddress_City_Local : _model.RegDeliverAddress_City_Local;
                model.City_Inter = _iscompaddr == true ? _model.RegCompanyAddress_City_Inter : _model.RegDeliverAddress_City_Inter;
                model.State_Local = _iscompaddr == true ? _model.RegCompanyAddress_State_Local : _model.RegDeliverAddress_State_Local;
                model.State_Inter = _iscompaddr == true ? _model.RegCompanyAddress_State_Inter : _model.RegDeliverAddress_State_Inter;
                model.CountryCode = _iscompaddr == true ? _model.RegCompanyAddress_CountryCode : _model.RegDeliverAddress_CountryCode;
                model.PostalCode = _iscompaddr == true ? _model.RegCompanyAddress_PostalCode : _model.RegDeliverAddress_PostalCode;
                if (model != null)
                {
                    context.Entry(model).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();

                    ILogRegAddressRepository _logRegAddrRepo = new LogRegAddressRepository();
                    _logRegAddrRepo.Insert(model.AddressID, "Modify");
                    _logRegAddrRepo.Dispose();
                }

                return true;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void InsertAddressPersonal(RegisContinuePortalModel _model, out int _addrid)
        {
            int _Addrid = 0;
            try
            {
                var _regAddress = new Tbl_RegAddress()
                {
                    HouseNo_Local = _model.RegPersonalAddress_HouseNo_Local,
                    HouseNo_Inter = _model.RegPersonalAddress_HouseNo_Inter,
                    VillageNo_Local = _model.RegPersonalAddress_VillageNo_Local,
                    VillageNo_Inter = _model.RegPersonalAddress_VillageNo_Inter,
                    Lane_Local = _model.RegPersonalAddress_Lane_Local,
                    Lane_Inter = _model.RegPersonalAddress_Lane_Inter,
                    Road_Local = _model.RegPersonalAddress_Road_Local,
                    Road_Inter = _model.RegPersonalAddress_Road_Inter,
                    SubDistrict_Local = _model.RegPersonalAddress_SubDistrict_Local,
                    SubDistrict_Inter = _model.RegPersonalAddress_SubDistrict_Inter,
                    City_Local = _model.RegPersonalAddress_City_Local,
                    City_Inter = _model.RegPersonalAddress_City_Inter,
                    State_Local = _model.RegPersonalAddress_State_Local,
                    State_Inter = _model.RegPersonalAddress_State_Inter,
                    CountryCode = _model.RegPersonalAddress_CountryCode,
                    PostalCode = _model.RegPersonalAddress_PostalCode
                };
                if (_regAddress != null)
                {
                    context.Tbl_RegAddress.Add(_regAddress);
                    context.SaveChanges();

                    _Addrid = _regAddress.AddressID;

                    ILogRegAddressRepository _logRegAddrRepo = new LogRegAddressRepository();
                    _logRegAddrRepo.Insert(_Addrid, "Add");
                    _logRegAddrRepo.Dispose();
                }
                _addrid = _Addrid;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void UpdateAddressPersonal(RegisContinuePortalModel _model, int _addrid)
        {
            try
            {
                var model = context.Tbl_RegAddress.FirstOrDefault(m => m.AddressID == _addrid);
                model.HouseNo_Local = _model.RegPersonalAddress_HouseNo_Local;
                    model.HouseNo_Inter = _model.RegPersonalAddress_HouseNo_Inter;
                    model.VillageNo_Local = _model.RegPersonalAddress_VillageNo_Local;
                    model.VillageNo_Inter = _model.RegPersonalAddress_VillageNo_Inter;
                    model.Lane_Local = _model.RegPersonalAddress_Lane_Local;
                    model.Lane_Inter = _model.RegPersonalAddress_Lane_Inter;
                    model.Road_Local = _model.RegPersonalAddress_Road_Local;
                    model.Road_Inter = _model.RegPersonalAddress_Road_Inter;
                    model.SubDistrict_Local = _model.RegPersonalAddress_SubDistrict_Local;
                    model.SubDistrict_Inter = _model.RegPersonalAddress_SubDistrict_Inter;
                    model.City_Local = _model.RegPersonalAddress_City_Local;
                    model.City_Inter = _model.RegPersonalAddress_City_Inter;
                    model.State_Local = _model.RegPersonalAddress_State_Local;
                    model.State_Inter = _model.RegPersonalAddress_State_Inter;
                    model.CountryCode = _model.RegPersonalAddress_CountryCode;
                    model.PostalCode = _model.RegPersonalAddress_PostalCode;

                    if (model != null)
                    {
                        context.Entry(model).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();

                        ILogRegAddressRepository _logRegAddrRepo = new LogRegAddressRepository();
                        _logRegAddrRepo.Insert(model.AddressID, "Modify");
                        _logRegAddrRepo.Dispose();
                    }
            }
            catch (Exception e)
            {

                throw;
            }
        }
        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
