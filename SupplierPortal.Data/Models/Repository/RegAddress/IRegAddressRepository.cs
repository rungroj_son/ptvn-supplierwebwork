﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.RegAddress
{
    public partial interface IRegAddressRepository
    {
        IEnumerable<Tbl_RegAddress> GetRegAddressByaddrid(int addrid);

        Tbl_RegAddress GetRegAddressMappingByContactID(int contactID);

        void InsertAddress(RegisContinuePortalModel _model, bool _iscompaddr, out int _addrid);

        bool UpdateAddress(RegisContinuePortalModel _model, int _addrid, bool _iscompaddr);

        void InsertAddressPersonal(RegisContinuePortalModel _model, out int _addrid);

        void UpdateAddressPersonal(RegisContinuePortalModel _model, int _addrid);

        int InsertReturnAddressID(Tbl_RegAddress model);

        void UpdateRegAddress(Tbl_RegAddress model);

        void Dispose();
    }
}
