﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.LogContactPerson
{
    public partial class LogContactPersonRepository : ILogContactPersonRepository
    {

        private SupplierPortalEntities context;


        public LogContactPersonRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogContactPersonRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual void Insert(int contactID, string logAction,string updateBy)
        {

            try
            {
                var _contactPerson = context.Tbl_ContactPerson.FirstOrDefault(m => m.ContactID == contactID);

                var _user = context.Tbl_User.FirstOrDefault(m => m.Username == updateBy);

                int userID = 0;
                if (_user != null)
                {
                    userID = _user.UserID;
                }
                var _logcontactPerson = new Tbl_LogContactPerson()
               {
                   ContactID = _contactPerson.ContactID,
                   Title = _contactPerson.TitleID.ToString(),
                   FirstName_Local = _contactPerson.FirstName_Local,
                   FirstName_Inter = _contactPerson.FirstName_Inter,
                   LastName_Local = _contactPerson.LastName_Local,
                   LastName_Inter = _contactPerson.LastName_Inter,
                   JobTitleID = _contactPerson.JobTitleID,
                   OtherJobTitle = _contactPerson.OtherJobTitle,
                   Department = _contactPerson.Department,
                   PhoneCountryCode = _contactPerson.PhoneCountryCode,
                   PhoneNo = _contactPerson.PhoneNo,
                   PhoneExt = _contactPerson.PhoneExt,
                   MobileNo = _contactPerson.MobileNo,
                   MobileCountryCode = _contactPerson.MobileCountryCode,
                   FaxNo = _contactPerson.FaxNo,
                   FaxExt = _contactPerson.FaxExt,
                   Email = _contactPerson.Email,
                   isDeleted = _contactPerson.isDeleted,
                   UpdateBy = userID,
                   UpdateDate = DateTime.UtcNow,
                   LogAction = logAction
                   
               };

                context.Tbl_LogContactPerson.Add(_logcontactPerson);
                context.SaveChanges();

            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Insert(int contactID, string logAction, int updateBy)
        {

            try
            {
                var _contactPerson = context.Tbl_ContactPerson.FirstOrDefault(m => m.ContactID == contactID);

                
                var _logcontactPerson = new Tbl_LogContactPerson()
                {
                    ContactID = _contactPerson.ContactID,
                    Title = _contactPerson.TitleID.ToString(),
                    FirstName_Local = _contactPerson.FirstName_Local,
                    FirstName_Inter = _contactPerson.FirstName_Inter,
                    LastName_Local = _contactPerson.LastName_Local,
                    LastName_Inter = _contactPerson.LastName_Inter,
                    JobTitleID = _contactPerson.JobTitleID,
                    OtherJobTitle = _contactPerson.OtherJobTitle,
                    Department = _contactPerson.Department,
                    PhoneCountryCode = _contactPerson.PhoneCountryCode,
                    PhoneNo = _contactPerson.PhoneNo,
                    PhoneExt = _contactPerson.PhoneExt,
                    MobileNo = _contactPerson.MobileNo,
                    MobileCountryCode = _contactPerson.MobileCountryCode,
                    FaxNo = _contactPerson.FaxNo,
                    FaxExt = _contactPerson.FaxExt,
                    Email = _contactPerson.Email,
                    isDeleted = _contactPerson.isDeleted,
                    UpdateBy = updateBy,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction

                };

                context.Tbl_LogContactPerson.Add(_logcontactPerson);
                context.SaveChanges();

            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }


    }
}
