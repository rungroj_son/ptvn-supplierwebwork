﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.AppConfig
{
    public partial class AppConfigRepository : IAppConfigRepository
    {

        private SupplierPortalEntities context;

        public AppConfigRepository()
        {
            context = new SupplierPortalEntities();
        }

        public AppConfigRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual Tbl_AppConfig GetAppConfigByName(string name)
        {
            return context.Tbl_AppConfig.Where(m => m.Name == name).FirstOrDefault();
        }

        public virtual string GetValueAppConfigByName(string name)
        {
            string valueReturn = "";

            var model = context.Tbl_AppConfig.Where(m => m.Name == name).FirstOrDefault();
            if(model != null)
            {
                valueReturn = model.Value;
            }

            return valueReturn;
        }


        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
