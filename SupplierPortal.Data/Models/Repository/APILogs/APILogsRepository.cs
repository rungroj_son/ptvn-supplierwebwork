﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.APILogs
{
    public partial class APILogsRepository : IAPILogsRepository
    {

        private SupplierPortalEntities context;

        public APILogsRepository()
        {
            context = new SupplierPortalEntities();
        }

        public APILogsRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual void Insert(Tbl_APILogs apiLogs)
        {

            try
            {
                if (apiLogs != null)
                {
                    var _logapiLogs = new Tbl_APILogs()
               {
                   ReqID = apiLogs.ReqID,
                   SystemID = apiLogs.SystemID,
                   Events = apiLogs.Events,
                   Events_Time = apiLogs.Events_Time,
                   IPAddress_Client = apiLogs.IPAddress_Client,
                   APIKey = apiLogs.APIKey,
                   APIName = apiLogs.APIName,
                   Return_Code = apiLogs.Return_Code,
                   Return_Status = apiLogs.Return_Status,
                   Return_Message = apiLogs.Return_Message,
                   Data = apiLogs.Data,
                   UserGUID = apiLogs.UserGUID,

               };
                    context.Tbl_APILogs.Add(_logapiLogs);
                    context.SaveChanges();
                
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void InsertLogByParam(
            string reqID = "",
            string events = "",
            string iPAddress_Client = "",
            string aPIKey = "",
            string aPIName = "",
            string return_Code = "",
            string return_Status = "",
            string return_Message = "",
            string data = "",
            string userGUID = "",
            int systemID = 0)
        {
            try
            {
                var _logapiLogs = new Tbl_APILogs()
                {
                    ReqID = reqID,
                    Events = events,
                    Events_Time = DateTime.UtcNow,
                    IPAddress_Client = iPAddress_Client,
                    APIKey = aPIKey,
                    APIName = aPIName,
                    Return_Code = return_Code,
                    Return_Status = return_Status,
                    Return_Message = return_Message,
                    Data = data,
                    UserGUID = userGUID,
                    SystemID = systemID

                };
                context.Tbl_APILogs.Add(_logapiLogs);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
