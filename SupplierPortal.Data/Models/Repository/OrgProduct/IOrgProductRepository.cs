﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.OrgProduct
{
    public partial interface IOrgProductRepository
    {
        IEnumerable<OrgProductGridViewModel> GetOrgProductBySupplierIDAndLanguageID(int supplierID, int languageID);

        IEnumerable<ProductTypeAddModel> GetProductTypeBySupplierIDAndProductCode(int supplierID, string productCode);

        bool OrgProductExists(int supplierID, string productCode, int productTypeID);

        bool OrgProductExists(int supplierID, string productCode);

        void Insert(int supplierID, int productTypeID, string productCode, int updateBy);

        void Delete(int supplierID, int productTypeID, string productCode, int updateBy);

        void Delete(int supplierID, string productcode, int updateBy);

        void Dispose();
    }
}
