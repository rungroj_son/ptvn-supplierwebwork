﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Data.Models.Repository.OrgAddress;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using System.Web;
using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.Repository.LogTrackingCompProfile;
using SupplierPortal.Data.Models.Repository.LogTrackingRequest;

namespace SupplierPortal.Data.Models.Repository.TrackingComprofile
{
    public partial class TrackingComprofileRepository : ITrackingComprofileRepository
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private SupplierPortalEntities context;
        IOrganizationRepository _organizationRepository;
        IAddressRepository _addressRepository;
        IOrgAddressRepository _orgAddressRepository;
        ILogTrackingCompProfileRepository _logTrackingCompProfileRepository;
        ILogTrackingRequestRepository _logTrackingRequestRepository;
        public TrackingComprofileRepository()
        {
            context = new SupplierPortalEntities();
            _organizationRepository = new OrganizationRepository();
            _addressRepository = new AddressRepository();
            _orgAddressRepository = new OrgAddressRepository();
            _logTrackingCompProfileRepository = new LogTrackingCompProfileRepository();
            _logTrackingRequestRepository = new LogTrackingRequestRepository();
        }

        public TrackingComprofileRepository
            (
            SupplierPortalEntities context,
            OrganizationRepository organizationRepository,
            AddressRepository addressRepository,
            OrgAddressRepository orgAddressRepository,
            LogTrackingCompProfileRepository logTrackingCompProfileRepository,
            ILogTrackingRequestRepository logTrackingRequestRepository
            )
        {
            this.context = context;
            _organizationRepository = organizationRepository;
            _addressRepository = addressRepository;
            _orgAddressRepository = orgAddressRepository;
            _logTrackingCompProfileRepository = logTrackingCompProfileRepository;
            _logTrackingRequestRepository = logTrackingRequestRepository;
        }

        //public  int GetTrackingStatus(this Tbl_TrackingCompProfile aaa)
        //{
        //    return 0;
        //}
        public void Insert(string Variable, int SupID, string value, int ReqId, int EntityID, int status)
        {

            try
            {

                var tracking = context.Tbl_PortalFieldConfig.FirstOrDefault(m => m.Variable == Variable); //หาค่า tbl.field == พารามิเตอร์ค่า Variable
                var trackingKeyGroup = tracking.Variable.Split('.').FirstOrDefault(); //ตัดเอาเฉพาะชื่อตาราง
                var trackingKey = tracking.Variable.Split('.').LastOrDefault(); //ตัดเอาเฉพาะชื่อฟิว


                if (trackingKeyGroup == "Tbl_Organization" && tracking != null)
                {

                    var Orgdata = _organizationRepository.GetDataBySupplierID(SupID); //get data Orgenization
                    var Oldval = (Orgdata.GetType().GetProperty(trackingKey.ToString()).GetValue(Orgdata) ?? string.Empty).ToString(); //get data OldKeyValue
                    if (trackingKey == "BusinessEntityID" && Orgdata.BusinessEntityID == -1)
                    {
                        Oldval = Orgdata.BusinessEntityID + "|" + Orgdata.OtherBusinessEntity;
                    }

                    var dataTracking = context.Tbl_TrackingCompProfile.FirstOrDefault(m => m.TrackingStatusID == 2 && m.TrackingKeyGroup == "Tbl_Organization" && m.TrackingKey == trackingKey && m.EntityID == EntityID) ?? new Tbl_TrackingCompProfile();

                    //if (Oldval == value && dataTracking.TrackingStatusID == 2)
                    //{
                    //    statusID = 2;
                    //}
                    //else if (Oldval != value)
                    //{
                    //    statusID = 2;
                    //}
                    //else if (Oldval == value)
                    //{
                    //    statusID = 1;
                    //}
                    //===================================TrackingComPro=====================================
                    var _TrackingCompro = new Tbl_TrackingCompProfile()
                    {
                        TrackingReqID = ReqId,
                        EntityID = SupID,
                        TrackingKeyGroup = trackingKeyGroup ?? "",
                        TrackingKey = trackingKey ?? "",
                        OldKeyValue = Oldval == null ? "" : Oldval,//Oldval ?? "",
                        NewKeyValue = value == null ? "" : value, //value ?? "",
                        TrackingStatusID = status,
                        FieldID = tracking.FieldID,
                        Remark = "",
                        TrackingStatusDate = DateTime.UtcNow
                        //UpdateBy = UpdateBy,
                        //UpdateDate = DateTime.UtcNow,
                    };
                    context.Tbl_TrackingCompProfile.Add(_TrackingCompro);
                    logger.Debug("(before Insert) Organization : " + _TrackingCompro.EntityID);
                    context.SaveChanges();
                    logger.Debug("(After Insert) Organization : " + _TrackingCompro.EntityID);
                    if (_TrackingCompro.OldKeyValue != _TrackingCompro.NewKeyValue)
                    {
                        _logTrackingCompProfileRepository.Insert(_TrackingCompro.TrackingItemID, SupID, "Add");
                    }

                }
                else if (trackingKeyGroup == "CompanyAddress" && tracking != null)
                {

                    Tbl_OrgAddress OrgAddress;
                    var Orgdata = new Tbl_Address();
                    var split = "";
                    if (trackingKey.Contains("CompanyAddress"))
                    {
                        OrgAddress = _orgAddressRepository.GetAddresType(SupID, 1);
                        Orgdata = _addressRepository.GetAddressByAddressID(OrgAddress.AddressID);
                        split = trackingKey.Replace(string.Format("{0}_", "CompanyAddress"), string.Empty);
                    }
                    else if (trackingKey.Contains("DeliverAddress"))
                    {
                        OrgAddress = _orgAddressRepository.GetAddresType(SupID, 2);
                        Orgdata = _addressRepository.GetAddressByAddressID(OrgAddress.AddressID);
                        split = trackingKey.Replace(string.Format("{0}_", "DeliverAddress"), string.Empty);
                    }
                    else
                    {
                        OrgAddress = _orgAddressRepository.GetAddresType(SupID, 1);
                        Orgdata = _addressRepository.GetAddressByAddressID(OrgAddress.AddressID);
                        split = trackingKey.Replace(string.Format("{0}_", "CompanyAddress"), string.Empty);
                    }

                    var Oldval = (Orgdata.GetType().GetProperty(split).GetValue(Orgdata) ?? string.Empty).ToString(); //get data OldKeyValue


                    var dataTracking = context.Tbl_TrackingCompProfile.FirstOrDefault(m => m.TrackingStatusID == 2 && m.TrackingKeyGroup == "Tbl_Address" && m.TrackingKey == split && m.EntityID == EntityID) ?? new Tbl_TrackingCompProfile();

                    //===================================TrackingComPro=====================================
                    if (trackingKey.Contains("CompanyAddress"))
                    {
                        logger.Debug("(before Insert) CompanyAddress");
                        var _TrackingCompro = new Tbl_TrackingCompProfile()
                        {
                            TrackingReqID = ReqId,
                            EntityID = Orgdata.AddressID,
                            TrackingKeyGroup = "Tbl_Address",
                            TrackingKey = split ?? "",
                            OldKeyValue = Oldval == null ? "" : Oldval, //Oldval ?? "",
                            NewKeyValue =  value == null ? "" : value,  //value ?? "",
                            TrackingStatusID = status,
                            FieldID = tracking.FieldID,
                            Remark = "",
                            TrackingStatusDate = DateTime.UtcNow
                            //UpdateBy = UpdateBy,
                            //UpdateDate = DateTime.UtcNow,
                        };
                        context.Tbl_TrackingCompProfile.Add(_TrackingCompro);
                        logger.Debug("(before Insert) CompanyAddress : " + _TrackingCompro.EntityID);
                        context.SaveChanges();
                        logger.Debug("(After Insert) CompanyAddress : " + _TrackingCompro.EntityID);
                        if (_TrackingCompro.OldKeyValue != _TrackingCompro.NewKeyValue)
                        {
                            _logTrackingCompProfileRepository.Insert(_TrackingCompro.TrackingItemID, SupID, "Add");
                        }
                    }
                    //===================================TrackingComPro=====================================
                    else if (trackingKey.Contains("DeliverAddress"))
                    {
                        var _TrackingCompro = new Tbl_TrackingCompProfile()
                        {
                            TrackingReqID = ReqId,
                            EntityID = Orgdata.AddressID,
                            TrackingKeyGroup = "Tbl_Address",
                            TrackingKey = split ?? "",
                            OldKeyValue = Oldval == null ? "" : Oldval,//Oldval ?? "",
                            NewKeyValue = value == null ? "" : value, //value ?? "",
                            TrackingStatusID = status,
                            FieldID = tracking.FieldID,
                            Remark = "",
                            TrackingStatusDate = DateTime.UtcNow
                            //UpdateBy = UpdateBy,
                            //UpdateDate = DateTime.UtcNow,
                        };
                        context.Tbl_TrackingCompProfile.Add(_TrackingCompro);
                        logger.Debug("(before Insert) DeliverAddress : " + _TrackingCompro.EntityID);
                        context.SaveChanges();
                        logger.Debug("(After Insert) DeliverAddress : " + _TrackingCompro.EntityID);
                        if (_TrackingCompro.OldKeyValue != _TrackingCompro.NewKeyValue)
                        {
                            _logTrackingCompProfileRepository.Insert(_TrackingCompro.TrackingItemID, SupID, "Add");
                        }
                    }
                }

            }
            catch
            {
                throw;
            }

        }

        public void Update(string Variable, int SupID, string value, int ReqId, int EntityID)
        {
            try
            {
                var tracking = context.Tbl_PortalFieldConfig.FirstOrDefault(m => m.Variable == Variable); //หาค่า tbl.field == พารามิเตอร์ค่า Variable
                var trackingKeyGroup = tracking.Variable.Split('.').FirstOrDefault(); //ตัดเอาเฉพาะชื่อตาราง
                var trackingKey = tracking.Variable.Split('.').LastOrDefault(); //ตัดเอาเฉพาะชื่อฟิว

                var trackingKeys = string.Empty;
                if (trackingKey.Contains("CompanyAddress"))
                {
                    trackingKeys = trackingKey.Replace(string.Format("{0}_", "CompanyAddress"), string.Empty);
                }
                else if (trackingKey.Contains("DeliverAddress"))
                {
                    trackingKeys = trackingKey.Replace(string.Format("{0}_", "DeliverAddress"), string.Empty);
                }
                else
                {
                    trackingKeys = trackingKey;
                }

                var modelTrackingCom = context.Tbl_TrackingCompProfile.FirstOrDefault(m => m.TrackingKey == trackingKeys && m.EntityID == EntityID) ?? null;
                if (modelTrackingCom != null)
                {
                    if (modelTrackingCom.TrackingKeyGroup == "Tbl_Organization" && modelTrackingCom.TrackingStatusID != 2)
                    {
                        var Orgdata = _organizationRepository.GetDataBySupplierID(SupID); //get data Orgenization
                        var Oldval = (Orgdata.GetType().GetProperty(trackingKey.ToString()).GetValue(Orgdata) ?? string.Empty).ToString(); //get data OldKeyValue


                        var statusID = 0;

                        var dataTracking = context.Tbl_TrackingCompProfile.FirstOrDefault(m => m.TrackingStatusID == 2 && m.TrackingKeyGroup == "Tbl_Organization" && m.TrackingKey == trackingKey && m.EntityID == EntityID) ?? new Tbl_TrackingCompProfile();
                        if (Oldval == value && ReqId != 0)
                        {
                            statusID = 2;
                        }
                        else if (Oldval == value && dataTracking.TrackingStatusID == 2)
                        {
                            statusID = 2;
                        }
                        else if (Oldval != value)
                        {
                            statusID = 2;
                        }
                        else if (Oldval == value)
                        {
                            statusID = 1;
                        }

                        //===================================TrackingComPro=====================================
                        if (modelTrackingCom.TrackingStatusID != 2)
                        {
                            context.Tbl_TrackingCompProfile.Remove(modelTrackingCom);

                            var _TrackingCompro = new Tbl_TrackingCompProfile()
                            {
                                TrackingReqID = ReqId,
                                EntityID = SupID,
                                TrackingKeyGroup = trackingKeyGroup ?? "",
                                TrackingKey = trackingKey ?? "",
                                OldKeyValue = Oldval ?? "",
                                NewKeyValue = value ?? "",
                                TrackingStatusID = statusID,
                                FieldID = tracking.FieldID,
                                Remark = "",
                                TrackingStatusDate = DateTime.UtcNow
                                //UpdateBy = UpdateBy,
                                //UpdateDate = DateTime.UtcNow,
                            };
                            context.Tbl_TrackingCompProfile.Add(_TrackingCompro);
                            logger.Debug("(before Update) Organization : " + _TrackingCompro.EntityID);
                            context.SaveChanges();
                            logger.Debug("(After Update) Organization : " + _TrackingCompro.EntityID);
                            if (_TrackingCompro.OldKeyValue != _TrackingCompro.NewKeyValue)
                            {
                                _logTrackingCompProfileRepository.Insert(_TrackingCompro.TrackingItemID, SupID, "Modify");
                            }
                        }
                        else
                        {
                            modelTrackingCom.EntityID = SupID;
                            modelTrackingCom.TrackingKeyGroup = trackingKeyGroup ?? "";
                            modelTrackingCom.TrackingKey = trackingKey ?? "";
                            modelTrackingCom.OldKeyValue = Oldval ?? "";
                            modelTrackingCom.NewKeyValue = value ?? "";
                            modelTrackingCom.TrackingStatusID = statusID;
                            modelTrackingCom.FieldID = tracking.FieldID;
                            modelTrackingCom.Remark = "";
                            modelTrackingCom.TrackingStatusDate = DateTime.UtcNow;
                            //model.UpdateBy = UpdateBy;
                            //model.UpdateDate = DateTime.UtcNow;
                            context.SaveChanges();

                            if (modelTrackingCom.OldKeyValue != modelTrackingCom.NewKeyValue)
                            {
                                _logTrackingCompProfileRepository.Insert(modelTrackingCom.TrackingItemID, SupID, "Modify");
                            }
                        }

                    }
                    else if (modelTrackingCom.TrackingKeyGroup == "Tbl_Address" && modelTrackingCom.TrackingStatusID != 2)
                    {
                        Tbl_OrgAddress OrgAddress;
                        var Orgdata = new Tbl_Address();
                        var split = "";
                        if (trackingKey.Contains("CompanyAddress"))
                        {
                            OrgAddress = _orgAddressRepository.GetAddresType(SupID, 1);
                            Orgdata = _addressRepository.GetAddressByAddressID(OrgAddress.AddressID);
                            split = trackingKey.Replace(string.Format("{0}_", "CompanyAddress"), string.Empty);
                        }
                        else if (trackingKey.Contains("DeliverAddress"))
                        {
                            OrgAddress = _orgAddressRepository.GetAddresType(SupID, 2);
                            Orgdata = _addressRepository.GetAddressByAddressID(OrgAddress.AddressID);
                            split = trackingKey.Replace(string.Format("{0}_", "DeliverAddress"), string.Empty);
                        }
                        else
                        {
                            OrgAddress = _orgAddressRepository.GetAddresType(SupID, 1);
                            Orgdata = _addressRepository.GetAddressByAddressID(OrgAddress.AddressID);
                            split = trackingKey.Replace(string.Format("{0}_", "CompanyAddress"), string.Empty);
                        }
                        var Oldval = (Orgdata.GetType().GetProperty(split).GetValue(Orgdata) ?? string.Empty).ToString(); //get data OldKeyValue


                        var statusID = 0;

                        var dataTracking = context.Tbl_TrackingCompProfile.FirstOrDefault(m => m.TrackingStatusID == 2 && m.TrackingKeyGroup == "Tbl_Address" && m.TrackingKey == split && m.EntityID == EntityID) ?? new Tbl_TrackingCompProfile();

                        if (Oldval == value && ReqId != 0)
                        {
                            statusID = 2;
                        }
                        else if (Oldval == value && dataTracking.TrackingStatusID == 2)
                        {
                            statusID = 2;
                        }

                        else if (Oldval != value)
                        {
                            statusID = 2;
                        }

                        else if (Oldval == value)
                        {
                            statusID = 1;
                        }

                        //===================================TrackingComPro=====================================
                        if (trackingKey.Contains("CompanyAddress"))
                        {
                            if (modelTrackingCom.TrackingStatusID != 2)
                            {
                                context.Tbl_TrackingCompProfile.Remove(modelTrackingCom);

                                var _TrackingCompro = new Tbl_TrackingCompProfile()
                                {
                                    TrackingReqID = ReqId,
                                    EntityID = Orgdata.AddressID,
                                    TrackingKeyGroup = "Tbl_Address",
                                    TrackingKey = split ?? "",
                                    OldKeyValue = Oldval ?? "",
                                    NewKeyValue = value ?? "",
                                    TrackingStatusID = statusID,
                                    FieldID = tracking.FieldID,
                                    Remark = "",
                                    TrackingStatusDate = DateTime.UtcNow
                                    //UpdateBy = UpdateBy,
                                    //UpdateDate = DateTime.UtcNow,
                                };
                                context.Tbl_TrackingCompProfile.Add(_TrackingCompro);
                                logger.Debug("(before Update) CompanyAddress : " + _TrackingCompro.EntityID);
                                context.SaveChanges();
                                logger.Debug("(After Update) CompanyAddress : " + _TrackingCompro.EntityID);
                                if (_TrackingCompro.OldKeyValue != _TrackingCompro.NewKeyValue)
                                {
                                    _logTrackingCompProfileRepository.Insert(_TrackingCompro.TrackingItemID, SupID, "Modify");
                                }
                            }
                            else
                            {
                                modelTrackingCom.EntityID = Orgdata.AddressID;
                                modelTrackingCom.TrackingKeyGroup = "Tbl_Address";
                                modelTrackingCom.TrackingKey = split ?? "";
                                modelTrackingCom.OldKeyValue = Oldval ?? "";
                                modelTrackingCom.NewKeyValue = value ?? "";
                                modelTrackingCom.TrackingStatusID = statusID;
                                modelTrackingCom.FieldID = tracking.FieldID;
                                modelTrackingCom.Remark = "";
                                modelTrackingCom.TrackingStatusDate = DateTime.UtcNow;
                                //model.UpdateBy = UpdateBy;
                                //model.UpdateDate = DateTime.UtcNow;
                                logger.Debug("(before Update) CompanyAddress : " + modelTrackingCom.EntityID);
                                context.SaveChanges();
                                logger.Debug("(After Update) CompanyAddress : " + modelTrackingCom.EntityID);
                                if (modelTrackingCom.OldKeyValue != modelTrackingCom.NewKeyValue)
                                {
                                    _logTrackingCompProfileRepository.Insert(modelTrackingCom.TrackingItemID, SupID, "Modify");
                                }
                            }
                        }
                        //===================================TrackingComPro=====================================
                        if (trackingKey.Contains("DeliverAddress"))
                        {
                            if (modelTrackingCom.TrackingStatusID != 2)
                            {
                                context.Tbl_TrackingCompProfile.Remove(modelTrackingCom);

                                var _TrackingCompro = new Tbl_TrackingCompProfile()
                                {
                                    TrackingReqID = ReqId,
                                    EntityID = Orgdata.AddressID,
                                    TrackingKeyGroup = "Tbl_Address",
                                    TrackingKey = split ?? "",
                                    OldKeyValue = Oldval ?? "",
                                    NewKeyValue = value ?? "",
                                    TrackingStatusID = statusID,
                                    FieldID = tracking.FieldID,
                                    Remark = "",
                                    TrackingStatusDate = DateTime.UtcNow
                                    //UpdateBy = UpdateBy,
                                    //UpdateDate = DateTime.UtcNow,
                                };
                                context.Tbl_TrackingCompProfile.Add(_TrackingCompro);
                                logger.Debug("(before Update) DeliverAddress : " + _TrackingCompro.EntityID);
                                context.SaveChanges();
                                logger.Debug("(After Update) DeliverAddress : " + _TrackingCompro.EntityID);
                                if (_TrackingCompro.OldKeyValue != _TrackingCompro.NewKeyValue)
                                {
                                    _logTrackingCompProfileRepository.Insert(_TrackingCompro.TrackingItemID, SupID, "Modify");
                                }
                            }
                            else
                            {
                                modelTrackingCom.EntityID = Orgdata.AddressID;
                                modelTrackingCom.TrackingKeyGroup = "Tbl_Address";
                                modelTrackingCom.TrackingKey = split ?? "";
                                modelTrackingCom.OldKeyValue = Oldval ?? "";
                                modelTrackingCom.NewKeyValue = value ?? "";
                                modelTrackingCom.TrackingStatusID = statusID;
                                modelTrackingCom.FieldID = tracking.FieldID;
                                modelTrackingCom.Remark = "";
                                modelTrackingCom.TrackingStatusDate = DateTime.UtcNow;
                                //model.UpdateBy = UpdateBy;
                                //model.UpdateDate = DateTime.UtcNow;
                                logger.Debug("(before Update) DeliverAddress : " + modelTrackingCom.EntityID);
                                context.SaveChanges();
                                logger.Debug("(before Update) DeliverAddress : " + modelTrackingCom.EntityID);
                                if (modelTrackingCom.OldKeyValue != modelTrackingCom.NewKeyValue)
                                {
                                    _logTrackingCompProfileRepository.Insert(modelTrackingCom.TrackingItemID, SupID, "Modify");
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public virtual IEnumerable<Tbl_TrackingCompProfile> GetDataBySupplierIDandTrackingKey(int EntityID, string trackingKey, string trackingKeyGrp)
        {
            return (from list in context.Tbl_TrackingCompProfile
                    where list.EntityID == EntityID && list.TrackingKeyGroup == trackingKeyGrp && list.TrackingKey == trackingKey && list.TrackingStatusID == 2
                    select list).ToList<Tbl_TrackingCompProfile>();

        }
        public virtual IEnumerable<Tbl_TrackingCompProfile> GetDataRefBySupplierIDandTrackingKey(int EntityID, string trackingKey, string trackingKeyGrp)
        {
            return (from list in context.Tbl_TrackingCompProfile
                    where list.EntityID == EntityID && list.TrackingKeyGroup == trackingKeyGrp && list.TrackingKey == trackingKey && list.TrackingStatusID == 5
                    select list).ToList<Tbl_TrackingCompProfile>();

        }
        public virtual Tbl_TrackingRequest GetDataByUser(int UserID)
        {
            return (from data in context.Tbl_TrackingRequest
                    where data.ReqBy == UserID
                    && data.isDeleted != 1
                    select data).OrderByDescending(m => m.ReqDate).FirstOrDefault();
        }

        public virtual int InsertReqAndRetrun(int UpdateBy, int supID)
        {
            try
            {
                var trackingReqID = 0;
                var _TrackingReq = new Tbl_TrackingRequest()
                {
                    SupplierID = supID,
                    ReqBy = UpdateBy,
                    ReqDate = DateTime.UtcNow,
                    ApproveBy = 0,
                    TrackingGrpID = 1,
                    TrackingStatusID = 2,
                    isDeleted = 0,
                    Remark = "",
                    TrackingStatusDate = DateTime.UtcNow
                };
                context.Tbl_TrackingRequest.Add(_TrackingReq);
                context.SaveChanges();

                trackingReqID = _TrackingReq.TrackingReqID;

                _logTrackingRequestRepository.Insert(_TrackingReq.TrackingReqID, supID, "Add");

                return trackingReqID;
            }
            catch (Exception)
            {

                throw;
            }

        }
        //public virtual string GetNewKeyValue(int SupID, string Variable) 
        //{ 
        //    return (from item in context.Tbl_TrackingCompProfile
        //                where item.EntityID == SupID && item.TrackingKey == Variable
        //                select item.NewKeyValue).ToString();
        //}

        //public virtual Tbl_TrackingCompProfile GetDataStatusNotEqual(string variable)
        //{

        //    return context.Tbl_TrackingCompProfile.LastOrDefault(m => m.TrackingStatusID == 1 && m.TrackingKey == variable) ?? new Tbl_TrackingCompProfile();
        //}

        public virtual IEnumerable<TrackingFieldCompanyProfileModel> GetTrackingFieldCompanyProfileByTrackingReqID(int trackingReqID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from a in context.Tbl_TrackingCompProfile
                        join b in context.Tbl_TrackingStatus on a.TrackingStatusID equals b.TrackingStatusID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_PortalFieldConfig on a.FieldID equals e.FieldID into f
                        from g in f.DefaultIfEmpty()
                        where a.TrackingReqID == trackingReqID
                        orderby g.SeqNo
                        select new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = a.TrackingReqID,
                            TrackingKey = a.TrackingKey,
                            FieldID = a.FieldID ?? 0,
                            ResourceName = g.ResourceName,
                            isForeignKey = g.isForeignKey ?? 0,
                            OtherFieldID = g.OtherFieldID ?? 0,
                            ForeignKeyGroup = g.ForeignKeyGroup,
                            ForeignKey = g.ForeignKey,
                            OldKeyValue = a.OldKeyValue,
                            NewKeyValue = a.NewKeyValue,
                            TrackingStatusID = a.TrackingStatusID ?? 0,
                            TrackingStatusName = (from lp in context.Tbl_LocalizedProperty
                                                  where lp.EntityID == a.TrackingStatusID
                                                  && lp.LocaleKeyGroup == "Tbl_TrackingStatus"
                                                  && lp.LocaleKey == "TrackingStatusName"
                                                  && lp.LanguageID == languageId
                                                  select lp.LocaleValue).FirstOrDefault() ?? d.TrackingStatusName,


                        };

            var result = query.ToList();

            return result;
        }

        public virtual IEnumerable<TrackingFieldCompanyProfileModel> GetTrackingFieldCompanyProfileByTrackingItemID(int trackingItemID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from a in context.Tbl_TrackingCompProfile
                        join b in context.Tbl_TrackingStatus on a.TrackingStatusID equals b.TrackingStatusID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_PortalFieldConfig on a.FieldID equals e.FieldID into f
                        from g in f.DefaultIfEmpty()
                        where a.TrackingItemID == trackingItemID
                        select new TrackingFieldCompanyProfileModel
                        {
                            TrackingReqID = a.TrackingReqID,
                            TrackingKey = a.TrackingKey,
                            FieldID = a.FieldID ?? 0,
                            ResourceName = g.ResourceName,
                            isForeignKey = g.isForeignKey ?? 0,
                            OtherFieldID = g.OtherFieldID ?? 0,
                            ForeignKeyGroup = g.ForeignKeyGroup,
                            ForeignKey = g.ForeignKey,
                            OldKeyValue = a.OldKeyValue,
                            NewKeyValue = a.NewKeyValue,
                            TrackingStatusID = a.TrackingStatusID ?? 0,
                            TrackingStatusName = (from lp in context.Tbl_LocalizedProperty
                                                  where lp.EntityID == a.TrackingStatusID
                                                  && lp.LocaleKeyGroup == "Tbl_TrackingStatus"
                                                  && lp.LocaleKey == "TrackingStatusName"
                                                  && lp.LanguageID == languageId
                                                  select lp.LocaleValue).FirstOrDefault() ?? d.TrackingStatusName,


                        };

            var result = query.ToList();

            return result;
        }

        public virtual IEnumerable<Tbl_TrackingCompProfile> GetdataRefByAddressID(int AddressID)
        {
            var query = (from a in context.Tbl_TrackingCompProfile
                         where a.EntityID == AddressID
                         && a.TrackingStatusID == 2
                         select a);
            return query.ToList();
        }

        public virtual Tbl_TrackingCompProfile GetdataRefByfieldID_AddressID(int fieldID, int AddresID)
        {
            var query = (from a in context.Tbl_TrackingCompProfile
                         where a.EntityID == AddresID
                         && a.FieldID == fieldID
                         && a.TrackingStatusID != 2
                         select a);
            return query.FirstOrDefault();
        }

        public virtual Tbl_TrackingCompProfile GetTrackingCompProfileWaitingApproveByTrackingItemID(int trackingItemID)
        {
            var result = context.Tbl_TrackingCompProfile.Where(m => m.TrackingItemID == trackingItemID && (m.TrackingStatusID == 2 || m.TrackingStatusID == 5)).FirstOrDefault();

            return result;
        }

        public void Update(Tbl_TrackingCompProfile tbl_TrackingCompProfile, int updateBy)
        {
            try
            {
                if (tbl_TrackingCompProfile != null)
                {
                    var tempModel = context.Tbl_TrackingCompProfile.Where(m => m.TrackingItemID == tbl_TrackingCompProfile.TrackingItemID).FirstOrDefault();

                    if (tempModel != null)
                    {
                        tempModel = tbl_TrackingCompProfile;
                        context.SaveChanges();

                        _logTrackingCompProfileRepository.Insert(tempModel.TrackingItemID, updateBy, "Modify");
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public virtual Tbl_PortalFieldConfig GetDataOtherFiled(string Variable)
        {
            var query = (from a in context.Tbl_PortalFieldConfig
                         where a.Variable == Variable
                         && a.OtherFieldID != 0
                         select a).FirstOrDefault();
            return query;
        }

        public virtual Tbl_PortalFieldConfig GetDataOtherFiledByID(int ID)
        {
            var query = (from a in context.Tbl_PortalFieldConfig
                         where a.FieldID == ID
                         select a).FirstOrDefault();
            return query;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
