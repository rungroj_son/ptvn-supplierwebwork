﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.RegContactRequestServiceTypeMapping
{
    public partial interface IRegContactRequestServiceTypeMappingRepository
    {
        void Insert(Tbl_RegContactRequestServiceTypeMapping model);

        void InsertByValue(int userRegID,int serviceTypeID);

        void Dispose();
    }
}
