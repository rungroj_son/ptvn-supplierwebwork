﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.LogRegContactRequestServiceTypeMapping;

namespace SupplierPortal.Data.Models.Repository.RegContactRequestServiceTypeMapping
{
    public partial class RegContactRequestServiceTypeMappingRepository : IRegContactRequestServiceTypeMappingRepository
    {
        private SupplierPortalEntities context;
        ILogRegContactReqServiceTypeMappingRepository _logRegContactReqServiceTypeMappingRepository; 

        public RegContactRequestServiceTypeMappingRepository()
        {
            context = new SupplierPortalEntities();
            _logRegContactReqServiceTypeMappingRepository = new LogRegContactReqServiceTypeMappingRepository();
        }

        public RegContactRequestServiceTypeMappingRepository(
            SupplierPortalEntities ctx,
            LogRegContactReqServiceTypeMappingRepository logRegContactReqServiceTypeMappingRepository
            )
        {
            this.context = ctx;
            _logRegContactReqServiceTypeMappingRepository = logRegContactReqServiceTypeMappingRepository;
        }

        public virtual void Insert(Tbl_RegContactRequestServiceTypeMapping model)
        {
            try
            {
                if (model != null)
                {


                    context.Tbl_RegContactRequestServiceTypeMapping.Add(model);

                    context.SaveChanges();

                    _logRegContactReqServiceTypeMappingRepository.Insert(model.Id, 1, "Add");
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void InsertByValue(int userRegID, int serviceTypeID)
        {
            try
            {
                var model = new Tbl_RegContactRequestServiceTypeMapping()
                {
                    UserReqId = userRegID,
                    ServiceTypeID = serviceTypeID
                };

                if(model != null)
                {
                    context.Tbl_RegContactRequestServiceTypeMapping.Add(model);

                    context.SaveChanges();

                    _logRegContactReqServiceTypeMappingRepository.Insert(model.Id, 1, "Add");
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }
        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
