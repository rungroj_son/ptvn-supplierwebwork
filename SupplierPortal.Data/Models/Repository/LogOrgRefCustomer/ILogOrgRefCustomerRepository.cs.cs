﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgRefCustomer
{
    public partial interface ILogOrgRefCustomerRepository
    {
        void Insert(int id, int UpdateBy, string logAction);

        void Dispose();
    }
}
