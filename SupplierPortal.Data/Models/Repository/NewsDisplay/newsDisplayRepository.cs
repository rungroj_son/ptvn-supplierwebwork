﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using System.Linq.Expressions;



namespace SupplierPortal.Data.Models.Repository.NewsDisplay
{
   public partial class newsDisplayRepository :InewsDisplayRepository
    {
        private SupplierPortalEntities context;

        public newsDisplayRepository() {

            context = new SupplierPortalEntities();
        }

        public newsDisplayRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IQueryable<NewsDisplayModel> findAllNewData(int langID)
        {


            var query = from a in context.Tbl_NewsDisplay
                        join b in context.Tbl_News on a.NewsId equals b.NewsId into c
                        from d in c.DefaultIfEmpty()
                        where DateTime.Now >= a.StartDate && DateTime.UtcNow < a.ExpireDate //&& d.LanguageID == langID
                        orderby a.SeqNo
                        select new NewsDisplayModel
                        {
                            NewsId = d.NewsId,
                            LanguageID = d.LanguageID,
                            NewsGroupID = d.NewsGroupID,
                            NewsHeader = d.NewsHeader,
                            NewsDetail = d.NewsDetail,
                            SeqNo = a.SeqNo
                        };

            return query;
        }

        public Tbl_News findlangID(int NewsID,int LangID) {
            var query = from db in context.Tbl_News
                        where db.NewsId == NewsID && db.LanguageID == LangID
                        select db;

            return query.FirstOrDefault();
           
        
        }

        public virtual IEnumerable<NewsDisplayModel> GetNewsShow(int languageId)
        {
            var query = from a in context.Tbl_NewsDisplay
                        join b in context.Tbl_News on a.NewsId equals b.NewsId into c
                        from d in c.Where(m => m.LanguageID == languageId).DefaultIfEmpty()
                        where (DateTime.UtcNow >= a.StartDate && DateTime.UtcNow <= a.ExpireDate)
                        orderby a.SeqNo
                        select new NewsDisplayModel
                        {
                            NewsId = d.NewsId,
                            LanguageID = d.LanguageID,
                            NewsGroupID = d.NewsGroupID,
                            NewsHeader = d.NewsHeader ?? "",
                            NewsDetail = d.NewsDetail ?? "",
                            SeqNo = a.SeqNo
                        };

            return query.ToList();
        }

        public virtual NewsDisplayModel GetNewsByNewsId(int newsId, int languageId)
        {
            var query = from a in context.Tbl_News
                        where a.NewsId == newsId
                        && a.LanguageID == languageId
                        select new NewsDisplayModel
                        {
                            NewsId = a.NewsId,
                            LanguageID = a.LanguageID,
                            NewsGroupID = a.NewsGroupID,
                            NewsHeader = a.NewsHeader ?? "",
                            NewsDetail = a.NewsDetail ?? "",

                        };

            return query.FirstOrDefault();
        }


    }
}
