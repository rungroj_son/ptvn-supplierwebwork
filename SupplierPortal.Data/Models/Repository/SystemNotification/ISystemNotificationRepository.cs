﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.SystemNotification
{
    public partial interface ISystemNotificationRepository
    {

        IEnumerable<Tbl_System> GetSystemBySystemGrpID(int systemgrpID);

        IEnumerable<Tbl_System> GetSystemShowNotiBySystemGrpIDAndNotificationID(int systemGrpID, int notificationID);

        IEnumerable<Tbl_System> GetSystemIsActive();

    }
}
