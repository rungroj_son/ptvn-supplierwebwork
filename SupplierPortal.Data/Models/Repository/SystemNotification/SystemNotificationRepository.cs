﻿using SupplierPortal.Core.Caching;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.SystemNotification
{
    public partial class SystemNotificationRepository : ISystemNotificationRepository
    {

        private SupplierPortalEntities context;

        public SystemNotificationRepository()
        {
            context = new SupplierPortalEntities();
        }

        public SystemNotificationRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<Tbl_System> GetSystemBySystemGrpID(int systemgrpID)
        {
            var query = context.Tbl_System.Where(m => m.SystemGrpID == systemgrpID && m.IsActive == 1).ToList();

            return query;
        }

        public virtual IEnumerable<Tbl_System> GetSystemIsActive()
        {
            var query = context.Tbl_System.Where(m => m.IsActive == 1).ToList();
            return query;
        }

        public virtual IEnumerable<Tbl_System> GetSystemShowNotiBySystemGrpIDAndNotificationID(int systemGrpID, int notificationID)
        {
            var query = from a in context.Tbl_System
                        join b in context.Tbl_SystemNotification on a.SystemGrpID equals b.SystemGrpID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_SystemConfigureAPI on a.SystemID equals e.SystemID
                        join f in context.Tbl_API on e.APIId equals f.APIId
                        where a.IsActive == 1
                        && d.NotificationID == notificationID
                        && e.isActive == 1
                        && f.isActive == 1
                        && f.API_Name == "GetNotification"
                        select a;

            var result = query.Distinct().ToList();

            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
