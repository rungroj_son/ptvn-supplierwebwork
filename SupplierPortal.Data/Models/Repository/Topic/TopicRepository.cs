﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.Topic
{
    public partial class TopicRepository : ITopicRepository
    {

        private SupplierPortalEntities context;

        public TopicRepository()
        {
            context = new SupplierPortalEntities();
        }

        public TopicRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual string GetResourceBody(string resourceKey, string languageId)
        {

                int languageID = Convert.ToInt32(languageId);
                var result = context.Tbl_Topic.Where(b => b.SectionName == resourceKey && b.LanguageID == languageID).FirstOrDefault();

                if (result != null)
                {
                    return result.Body;
                }else
                {
                    return "";
                }


        }

        public virtual IEnumerable<Tbl_Topic> GetTopicDataListParam(string sectionname, int languageid)
        {
            return (from list in context.Tbl_Topic
                    where list.SectionName == sectionname &&
                          list.LanguageID == languageid
                    select list).ToList<Tbl_Topic>();
        }
        public virtual string GetDownloadLinkAgreement(int languageID)
        {
            var register_AgreementTermName = languageID == 1 ? "Register_AgreementTermPath_EN" : "Register_AgreementTermPath_TH";
            var register_AgreementTermPath = context.Tbl_AppConfig
                                                      .FirstOrDefault(f => f.Name.Equals(register_AgreementTermName));
            return (register_AgreementTermPath.Value);
        }
        public virtual string GetDownloadLinkOPAgreement(int languageID) 
        {
            var register_OPAgreementTermName = languageID == 1 ? "Register_OPAgreementTermPath_EN" : "Register_OPAgreementTermPath_TH";
            var register_OPAgreementTermPath = context.Tbl_AppConfig
                                                      .FirstOrDefault(f => f.Name.Equals(register_OPAgreementTermName));
            return (register_OPAgreementTermPath.Value);
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
