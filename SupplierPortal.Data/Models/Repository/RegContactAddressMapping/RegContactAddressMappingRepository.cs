﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.RegContactAddressMapping
{
    public partial class RegContactAddressMappingRepository : IRegContactAddressMappingRepository
    {
        private SupplierPortalEntities context;

        public RegContactAddressMappingRepository()
        {
            context = new SupplierPortalEntities();
        }

        public RegContactAddressMappingRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }


        public virtual Tbl_RegContactAddressMapping GetRegContactAddressMappingBycontactIDAndAddressID(int contactID, int addressID)
        {
            return context.Tbl_RegContactAddressMapping.Where(m => m.ContactID == contactID && m.AddressID == addressID).FirstOrDefault();

        }

        public virtual void Insert(int contactID, int addressID)
        {
            try
            {
                var _regContactAddressMapping = new Tbl_RegContactAddressMapping()
                    {
                        ContactID = contactID,
                        AddressID = addressID
                    };

                if (_regContactAddressMapping != null)
                {

                    context.Tbl_RegContactAddressMapping.Add(_regContactAddressMapping);
                    context.SaveChanges();
                }

            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
