﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.RegContactAddressMapping
{
    public partial interface  IRegContactAddressMappingRepository
    {

        Tbl_RegContactAddressMapping GetRegContactAddressMappingBycontactIDAndAddressID(int contactID, int addressID);

        void Insert(int contactID, int addressID);

        void Dispose();
    }
}
