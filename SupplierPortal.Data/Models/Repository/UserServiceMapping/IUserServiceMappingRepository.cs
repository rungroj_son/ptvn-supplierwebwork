﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Profile;

namespace SupplierPortal.Data.Models.Repository.UserServiceMapping
{
    public partial interface  IUserServiceMappingRepository
    {
        IEnumerable<UserServiceAvailableModels> GetUserServiceAvailableByUserID(int userID);

        IEnumerable<Tbl_UserServiceMapping> GetUserServiceMappingByUserID(int userID);

        IEnumerable<byte[]> GetIconNotificationByUserID(int userID);

        Tbl_UserServiceMapping GetUserServiceMappingByUserIDAndServiceName(int userID, string serviceName);

        void Insert(Tbl_UserServiceMapping model,int updateBy);

        bool CheckExistsUserServiceMapping(int userID, int serviceID);

        void Dispose();
    }
}
