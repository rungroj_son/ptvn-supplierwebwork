﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogOrgRefCustomer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OrgRefCustomer
{
    public partial class OrgRefCustomerRepository : IOrgRefCustomerRepository
    {
        private SupplierPortalEntities context;
        ILogOrgRefCustomerRepository _logOrgRefCustomerRepository;

        public OrgRefCustomerRepository()
        {
            context = new SupplierPortalEntities();
            _logOrgRefCustomerRepository = new LogOrgRefCustomerRepository();
        }

        public OrgRefCustomerRepository
            (
            SupplierPortalEntities context,
            LogOrgRefCustomerRepository logOrgRefCustomerRepository
            )
        {
            this.context = context;
            _logOrgRefCustomerRepository = logOrgRefCustomerRepository;
        }

        public virtual List<Tbl_OrgRefCustomer> GetDataBySupplierID(int SupplierID)
        {

            return context.Tbl_OrgRefCustomer.Where(w => w.SupplierID == SupplierID).ToList();
        }
        public virtual Tbl_OrgRefCustomer GetDataById(int id)
        {
            return (from i in context.Tbl_OrgRefCustomer
                    where i.Id == id
                    select i).FirstOrDefault();
        }
        public virtual void InsertRefCustomer(Tbl_OrgRefCustomer model)
        {
            try
            {
                //   if (tempModel != null)
                // {
                var orgAdd = new Tbl_OrgRefCustomer()
                {
                    SupplierID = model.SupplierID,
                    SeqNo = model.SeqNo,
                    CompanyName_Local = model.CompanyName_Local,
                    CompanyName_Inter = model.CompanyName_Inter,
                    BusinessEntityID = model.BusinessEntityID,
                    OtherBusinessEntity = model.OtherBusinessEntity
                };

                // context.Tbl_OrgShareholder.Remove(tempModel);

                context.Tbl_OrgRefCustomer.Add(orgAdd);
                context.SaveChanges();
                //InsertAddress(model);

                _logOrgRefCustomerRepository.Insert(orgAdd.Id, orgAdd.SupplierID, "Add");
                //   }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public virtual void UpdateRefCustomer(Tbl_OrgRefCustomer model)
        {
            try
            {

                var dataRef = (from i in context.Tbl_OrgRefCustomer
                               where i.Id == model.Id
                               select i).FirstOrDefault();


                dataRef.SupplierID = model.SupplierID;
                dataRef.SeqNo = model.SeqNo;
                dataRef.CompanyName_Local = model.CompanyName_Local;
                dataRef.CompanyName_Inter = model.CompanyName_Inter;
                dataRef.BusinessEntityID = model.BusinessEntityID;
                dataRef.OtherBusinessEntity = model.OtherBusinessEntity;

                context.SaveChanges();
                //InsertAddress(model);

                _logOrgRefCustomerRepository.Insert(model.Id, model.SupplierID, "Modify");
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public virtual void DeleteRefCustomer(Tbl_OrgRefCustomer model)
        {
            try
            {
                context.Tbl_OrgRefCustomer.Remove(model);
                context.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
