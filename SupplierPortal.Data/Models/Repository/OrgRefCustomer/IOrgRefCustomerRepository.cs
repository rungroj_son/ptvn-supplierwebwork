﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OrgRefCustomer
{
    public partial interface IOrgRefCustomerRepository
    {
        List<Tbl_OrgRefCustomer> GetDataBySupplierID(int SupplierID);
        Tbl_OrgRefCustomer GetDataById(int id);
        void InsertRefCustomer(Tbl_OrgRefCustomer model);
        void UpdateRefCustomer(Tbl_OrgRefCustomer model);
        void DeleteRefCustomer(Tbl_OrgRefCustomer model);
        void Dispose();
    }
}
