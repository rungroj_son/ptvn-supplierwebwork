﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.ContactPerson
{
    public partial interface IContactPersonRepository
    {

        Tbl_ContactPerson GetContactPersonByContectID(int contectID);

        ContactPersonDataModels JsonContactPersonByContectID(int contectID);

        ContactPersonDataModels JsonContactPersonJoinOrgContactPersonBySupplierID(int supplierID, int isPrimaryContact);

        IEnumerable<string> GetEmailContactPersonByAll();

        void Update(Tbl_ContactPerson tbl_ContactPerson);

        bool EmailExists(string email, string initialEmail);

        IEnumerable<ContactPersonModels> ContactPersonList();

        IEnumerable<ContactPersonModels> ContactPersonListByOrgID(string orgID);

        ContactPersonAdminModels ContactPersonAdminBySupplierID(int supplierID);

        Tbl_ContactPerson ContactPersonAdminBSPBySupplierID(int supplierID);

        int ContactPersonIsAdmin(string username);

        bool CheckIsAdminBestSupply(string username);

        int InsertContactPersonReturnContactID(Tbl_ContactPerson model,int updateByUserID);

        void DeleteByContactID(int contactID, int updateByUserID);

        void Update(Tbl_ContactPerson tbl_ContactPerson, int updateByUserID);

        void Dispose();

    }
}
