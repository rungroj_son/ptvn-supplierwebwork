﻿using SupplierPortal.Data.Helper;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LogContactPerson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace SupplierPortal.Data.Models.Repository.ContactPerson
{
    public partial class ContactPersonRepository : IContactPersonRepository
    {

        private SupplierPortalEntities context;
        ILogContactPersonRepository _logContactPersonRepository;

        public ContactPersonRepository()
        {
            context = new SupplierPortalEntities();
            _logContactPersonRepository = new LogContactPersonRepository();
        }

        public ContactPersonRepository(
            SupplierPortalEntities ctx,
            LogContactPersonRepository logContactPersonRepository

            )
        {
            this.context = ctx;
            _logContactPersonRepository = logContactPersonRepository;
        }

        public virtual Tbl_ContactPerson GetContactPersonByContectID(int contectID)
        {
            var result = context.Tbl_ContactPerson
                    .Where(b => b.ContactID == contectID)
                    .FirstOrDefault();

            return result;
        }

        public virtual ContactPersonDataModels JsonContactPersonByContectID(int contectID)
        {

            int isInter = LanguageHelper.GetLanguageIsInter();

            var result = from db in context.Tbl_ContactPerson
                         where db.ContactID == contectID
                         select new ContactPersonDataModels
                         {
                             ContactID = db.ContactID,
                             TitleID = db.TitleID,
                             FirstName = (isInter == 1 ? db.FirstName_Inter : db.FirstName_Local),
                             LastName = (isInter == 1 ? db.LastName_Inter : db.LastName_Local),
                             JobTitleID = db.JobTitleID,
                             OtherJobTitle = db.OtherJobTitle,
                             Department = db.Department,
                             PhoneNo = db.PhoneNo,
                             PhoneExt = db.PhoneExt,
                             MobileNo = db.MobileNo,
                             FaxNo = db.FaxNo,
                             FaxExt = db.FaxExt,
                             Email = db.Email
                         };

            return result.FirstOrDefault();
        }

        public virtual ContactPersonDataModels JsonContactPersonJoinOrgContactPersonBySupplierID(int supplierID, int isPrimaryContact)
        {

            var result = from db in context.Tbl_ContactPerson
                         join o in context.Tbl_OrgContactPerson on db.ContactID equals o.ContactID
                         where o.SupplierID == supplierID && o.isPrimaryContact == isPrimaryContact
                         select new ContactPersonDataModels
                         {
                             ContactID = db.ContactID,
                             TitleID = db.TitleID,
                             FirstName = db.FirstName_Local,
                             LastName = db.LastName_Local,
                             JobTitleID = db.JobTitleID,
                             OtherJobTitle = db.OtherJobTitle,
                             Department = db.Department,
                             PhoneNo = db.PhoneNo,
                             PhoneExt = db.PhoneExt,
                             MobileNo = db.MobileNo,
                             FaxNo = db.FaxNo,
                             FaxExt = db.FaxExt,
                             Email = db.Email
                         };

            return result.FirstOrDefault();
        }

        public virtual IEnumerable<string> GetEmailContactPersonByAll()
        {
            var query = from db in context.Tbl_ContactPerson
                        select db.Email;
            var result = query.ToList();

            return result;
        }

        public virtual bool EmailExists(string email, string initialEmail)
        {
            //var query = from db in context.Tbl_ContactPerson
            //            where db.Email == email
            //            && db.Email != initialEmail
            //            select db;

            var count = context.Tbl_ContactPerson.Count(m => m.Email == email && m.Email != initialEmail);

            if (count > 0)
            {
                return true;
            }

            return false;
        }


        public virtual void Update(Tbl_ContactPerson tbl_ContactPerson)
        {
            try
            {

                if (tbl_ContactPerson != null)
                {

                    var contactPersontmp = context.Tbl_ContactPerson.Find(tbl_ContactPerson.ContactID);
                    contactPersontmp = tbl_ContactPerson;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual IEnumerable<ContactPersonModels> ContactPersonList()
        {
            int isInter = LanguageHelper.GetLanguageIsInter();

            var query = from db in context.Tbl_ContactPerson
                        select new ContactPersonModels
                        {

                            ContactID = db.ContactID,
                            ContactName = (isInter == 1 ? db.FirstName_Inter : db.FirstName_Local) + "  " + (isInter == 1 ? db.LastName_Inter : db.LastName_Local)

                        };
            var result = query.ToList();

            return result;
        }

        public virtual IEnumerable<ContactPersonModels> ContactPersonListByOrgID(string orgID)
        {
            int isInter = LanguageHelper.GetLanguageIsInter();

            var query = from a in context.Tbl_ContactPerson
                        join b in context.Tbl_User on a.ContactID equals b.ContactID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_UserRole on d.UserID equals e.UserID into f
                        from g in f.DefaultIfEmpty()
                        where d.OrgID == orgID
                        && g.RoleID != 1
                        select new ContactPersonModels
                        {
                            ContactID = a.ContactID,
                            ContactName = (isInter == 1 ? a.FirstName_Inter : a.FirstName_Local) + "  " + (isInter == 1 ? a.LastName_Inter : a.LastName_Local)

                        };

            var result = query.Distinct().ToList();

            return result;
        }

        public virtual int ContactPersonIsAdmin(string username)
        {

            var query = from a in context.Tbl_User
                        join b in context.Tbl_UserRole on a.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where a.Username == username
                        select new
                        {
                            isAdmin = d.RoleID == null ? 0 : (int)d.RoleID,
                        };

            //var result = query.ToList();//.GroupBy(test => test.username)

            return query.Select(m => m.isAdmin).FirstOrDefault();
        }

        public virtual ContactPersonAdminModels ContactPersonAdminBySupplierID(int supplierID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var _tempLanguage = context.Tbl_Language.Find(languageId);
            int isInter = 0;
            if (_tempLanguage != null)
            {
                isInter = _tempLanguage.isInter ?? 0;
            }

            var query = from a in context.Tbl_Organization
                        join b in context.Tbl_User on a.OrgID equals b.OrgID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_ContactPerson on d.ContactID equals e.ContactID into f
                        from g in f.DefaultIfEmpty()
                        join h in context.Tbl_UserRole on d.UserID equals h.UserID into i
                        from j in i.DefaultIfEmpty()
                        join k in context.Tbl_NameTitle on g.TitleID equals k.TitleID into m
                        from n in m.DefaultIfEmpty()
                        where a.SupplierID == supplierID
                        && a.isDeleted != 1
                        && j.RoleID == 1
                        orderby g.ContactID
                        select new ContactPersonAdminModels
                        {
                            SupplierID = a.SupplierID,
                            ContactID = g.ContactID,
                            Company = (isInter == 1 ? a.CompanyName_Inter : a.CompanyName_Local),
                            TitleName = ((from ml in context.Tbl_LocalizedProperty
                                          where ml.EntityID == n.TitleID
                                          && ml.LanguageID == languageId
                                          && ml.LocaleKeyGroup == "Tbl_NameTitle"
                                          && ml.LocaleKey == "TitleName"
                                          select ml.LocaleValue
                                           )).FirstOrDefault() ?? n.TitleName,
                            FirstName = (isInter == 1 ? g.FirstName_Inter : g.FirstName_Local),
                            LastName = (isInter == 1 ? g.LastName_Inter : g.LastName_Local),
                            PhoneNo = g.PhoneNo,
                            PhoneExt = g.PhoneExt,
                            Email = g.Email
                        };

            var result = query.FirstOrDefault();

            return result;
        }

        public virtual Tbl_ContactPerson ContactPersonAdminBSPBySupplierID(int supplierID)
        {
            var query = from a in context.Tbl_Organization
                        join b in context.Tbl_User on a.OrgID equals b.OrgID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_ContactPerson on d.ContactID equals e.ContactID into f
                        from g in f.DefaultIfEmpty()
                        join h in context.Tbl_UserSystemMapping on d.UserID equals h.UserID into i
                        from j in i.DefaultIfEmpty()
                        where a.SupplierID == supplierID
                        && a.isDeleted != 1
                        && j.SysRoleID == 1
                        && j.SystemID == 1
                        select g;

            var result = query.FirstOrDefault();

            return result;
        }

        public virtual bool CheckIsAdminBestSupply(string username)
        {
            bool chk = false;

            var query = from db in context.Tbl_UserSystemMapping
                        join b in context.Tbl_User on db.UserID equals b.UserID into c
                        from d in c.DefaultIfEmpty()
                        where d.Username == username
                        && db.SystemID == 1
                        && (db.SysRoleID == 1 || db.SysRoleID == 5)
                        select db;

            var result = query.FirstOrDefault();

            if (result != null)
            {
                chk = true;
            }

            return chk;
        }

        public virtual int InsertContactPersonReturnContactID(Tbl_ContactPerson model, int updateByUserID)
        {
            try
            {
                int contactID = 0;
                if (model != null)
                {
                    context.Tbl_ContactPerson.Add(model);
                    context.SaveChanges();

                    contactID = model.ContactID;

                    _logContactPersonRepository.Insert(contactID, "Add", updateByUserID);

                }

                return contactID;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public virtual void DeleteByContactID(int contactID, int updateByUserID)
        {
            try
            {
                var contactPersontmp = context.Tbl_ContactPerson.Find(contactID);
                if (contactPersontmp != null)
                {
                    contactPersontmp.isDeleted = 1;
                    context.SaveChanges();

                    _logContactPersonRepository.Insert(contactID, "Delete", updateByUserID);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void Update(Tbl_ContactPerson tbl_ContactPerson, int updateByUserID)
        {
            try
            {

                if (tbl_ContactPerson != null)
                {

                    var contactPersontmp = context.Tbl_ContactPerson.Find(tbl_ContactPerson.ContactID);
                    contactPersontmp = tbl_ContactPerson;
                    context.SaveChanges();

                    _logContactPersonRepository.Insert(tbl_ContactPerson.ContactID, "Modify", updateByUserID);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
