﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Data.Models.SupportModel.SystemAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.SystemConfigureAPI
{
    public partial class SystemConfigureAPIRepository : ISystemConfigureAPIRepository
    {

        private SupplierPortalEntities context;


        public SystemConfigureAPIRepository()
        {
            context = new SupplierPortalEntities();
        }

        public SystemConfigureAPIRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual IEnumerable<SystemAPIModel> GetSystemShowNotiBySystemGrpIDAndNotificationID(int systemGrpID, int notificationID)
        {
            var query = from a in context.Tbl_System
                        join b in context.Tbl_SystemConfigureAPI on a.SystemID equals b.SystemID
                        join c in context.Tbl_API on b.APIId equals c.APIId
                        join d in context.Tbl_SystemNotification on a.SystemGrpID equals d.SystemGrpID
                        where b.isActive == 1
                        && c.isActive == 1
                        && a.SystemGrpID == systemGrpID
                        && d.NotificationID == notificationID
                        && c.API_Name == "GetNotification"
                        select new SystemAPIModel
                        {
                            SystemID = a.SystemID,
                            SystemGrpID = a.SystemGrpID,
                            APIName = c.API_Name,
                            API_Key = c.API_Key,
                            API_URL = c.API_URL
                        };

            var result = query.Distinct().ToList();

            return result;
        }

        public virtual IEnumerable<SystemRoleListModels> GetSystemListByUsernameForUpdateAPI(string username)
        {
            var query = from a in context.Tbl_UserSystemMapping
                        join b in context.Tbl_System on a.SystemID equals b.SystemID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_User on a.UserID equals e.UserID into f
                        from g in f.DefaultIfEmpty()
                        join h in context.Tbl_Organization on g.OrgID equals h.OrgID into i
                        from j in i.Where(x => x.isDeleted != 1).DefaultIfEmpty()
                        join k in context.Tbl_OrgSystemMapping on j.SupplierID equals k.SupplierID into l
                        from m in l.Where(x => x.SystemID == a.SystemID).DefaultIfEmpty()
                        join n in context.Tbl_SystemConfigureAPI on d.SystemID equals n.SystemID into o
                        from p in o.DefaultIfEmpty()
                        join q in context.Tbl_API on p.APIId equals q.APIId into r
                        from s in r.DefaultIfEmpty()
                        where g.Username == username
                        && d.IsActive == 1
                        && m.isAllowConnecting == 1
                        && p.isActive == 1
                        && s.isActive == 1
                        && s.API_Name == "UpdateUser"
                        select new SystemRoleListModels
                        {
                            Username = username,
                            SystemID = a.SystemID,
                            SystemName = d.SystemName,
                            Description = d.Description,
                            SystemGrpID = d.SystemGrpID,
                            IsShowOnList = d.IsShowOnList,
                            IsShowRoleList = d.IsShowRoleList,
                            API_URL = s.API_URL,
                            API_Key = s.API_Key,
                            API_Name = s.API_Name,
                            //APIName_InsertUser = d.APIName_InsertUser,
                            //APIName_UpdateUser = d.APIName_UpdateUser,
                            //APIName_UpdateSupplier = d.APIName_UpdateSupplier,
                            IsChecked = a.isEnableService ?? 0,
                            SysRoleID = "PrimaryUser"
                        };

            //SysRoleID = "PrimaryUser" ฟิกไว้เพื่อไป Get ค่า SysRoleID ของ User ที่เป็น Primary กรณีที่ต้อง call update ใน user ที่มาจากการ merge OP ซึ่งอาจมาหลาย SysRoleID

            var result = query.Distinct().ToList();

            return result;
        }

        public virtual IEnumerable<SystemRoleListModels> GetSystemListByUsernameAndSystemIDForUpdateAPI(string username, int systemID)
        {
            var query = from a in context.Tbl_UserSystemMapping
                        join b in context.Tbl_System on a.SystemID equals b.SystemID into c
                        from d in c.DefaultIfEmpty()
                        join e in context.Tbl_User on a.UserID equals e.UserID into f
                        from g in f.DefaultIfEmpty()
                        join h in context.Tbl_Organization on g.OrgID equals h.OrgID into i
                        from j in i.Where(x => x.isDeleted != 1).DefaultIfEmpty()
                        join k in context.Tbl_OrgSystemMapping on j.SupplierID equals k.SupplierID into l
                        from m in l.Where(x => x.SystemID == a.SystemID).DefaultIfEmpty()
                        join n in context.Tbl_SystemConfigureAPI on d.SystemID equals n.SystemID into o
                        from p in o.DefaultIfEmpty()
                        join q in context.Tbl_API on p.APIId equals q.APIId into r
                        from s in r.DefaultIfEmpty()
                        where g.Username == username
                        && d.IsActive == 1
                        && m.isAllowConnecting == 1
                        && p.isActive == 1
                        && s.isActive == 1
                        && s.API_Name == "UpdateUser"
                        && a.SystemID == systemID
                        select new SystemRoleListModels
                        {
                            Username = username,
                            SystemID = a.SystemID,
                            SystemName = d.SystemName,
                            Description = d.Description,
                            SystemGrpID = d.SystemGrpID,
                            IsShowOnList = d.IsShowOnList,
                            IsShowRoleList = d.IsShowRoleList,
                            API_URL = s.API_URL,
                            API_Key = s.API_Key,
                            API_Name = s.API_Name,
                            IsChecked = a.isEnableService ?? 0,
                            SysRoleID = a.SysRoleID.ToString()
                        };

            var result = query.Distinct().ToList();

            return result;
        }

        public virtual SystemRoleListModels GetSystemRoleAPIBySystemIDAndAPI_Name(int systemID, string api_Name)
        {
            var query = from a in context.Tbl_System
                        join b in context.Tbl_SystemConfigureAPI on a.SystemID equals b.SystemID
                        join c in context.Tbl_API on b.APIId equals c.APIId
                        where a.IsActive == 1
                        && b.isActive == 1
                        && c.isActive == 1
                        && c.API_Name == api_Name
                        && a.SystemID == systemID
                        select new SystemRoleListModels
                        {
                            Username = "",
                            SystemID = a.SystemID,
                            SystemName = a.SystemName,
                            Description = a.Description,
                            SystemGrpID = a.SystemGrpID,
                            IsShowOnList = a.IsShowOnList,
                            IsShowRoleList = a.IsShowRoleList,
                            API_URL = c.API_URL,
                            API_Key = c.API_Key,
                            API_Name = c.API_Name,
                            SysRoleID = "0"
                        };

            var result = query.FirstOrDefault();

            return result;
        }

        public virtual Tbl_API GetAPIBySystemIDAndAPI_Name(int systemID, string api_Name)
        {
            var query = from a in context.Tbl_System
                        join b in context.Tbl_SystemConfigureAPI on a.SystemID equals b.SystemID
                        join c in context.Tbl_API on b.APIId equals c.APIId
                        where a.IsActive == 1
                        && b.isActive == 1
                        && c.isActive == 1
                        && c.API_Name == api_Name
                        && a.SystemID == systemID
                        select c;

            var result = query.FirstOrDefault();

            return result;
        }


        public virtual IEnumerable<SystemRoleListModels> GetSystemListBySupplierIDForUpdateAPIwhereUpdateSupplier(int supplierID)
        {
            var query = from a in context.Tbl_API.Where(w => w.API_Name == "UpdateSupplier" && w.isActive == 1 && w.isActive == 1)
                        join b in context.Tbl_SystemConfigureAPI on a.APIId equals b.APIId into join_a
                        from ab in join_a.DefaultIfEmpty()
                        join c in context.Tbl_OrgSystemMapping on ab.SystemID equals c.SystemID into join_ab
                        from bc in join_ab.DefaultIfEmpty()
                        join d in context.Tbl_System on bc.SystemID equals d.SystemID into join_cd
                        from cd in join_cd.DefaultIfEmpty()
                        join e in context.Tbl_SystemGroup.Where(w => w.SystemGrpID == 2 && w.SystemGrpID == 3) on cd.SystemGrpID equals e.SystemGrpID into join_de
                        from de in join_de.DefaultIfEmpty()
                        where bc.SupplierID == supplierID
                        select new SystemRoleListModels
                        {
                            SystemID = bc.SystemID,
                            API_URL = a.API_URL,
                            API_Key = a.API_Key,
                            API_Name = a.API_Name,
                            SysRoleID = "PrimaryUser"
                        };

            //var query = from a in context.Tbl_UserSystemMapping
            //            join b in context.Tbl_System on a.SystemID equals b.SystemID into c
            //            from d in c.DefaultIfEmpty()
            //            join e in context.Tbl_User on a.UserID equals e.UserID into f
            //            from g in f.DefaultIfEmpty()
            //            join h in context.Tbl_Organization on g.OrgID equals h.OrgID into i
            //            from j in i.Where(x => x.isDeleted != 1).DefaultIfEmpty()
            //            join k in context.Tbl_OrgSystemMapping on j.SupplierID equals k.SupplierID into l
            //            from m in l.Where(x => x.SystemID == a.SystemID).DefaultIfEmpty()
            //            join n in context.Tbl_SystemConfigureAPI on d.SystemID equals n.SystemID into o
            //            from p in o.DefaultIfEmpty()
            //            join q in context.Tbl_API on p.APIId equals q.APIId into r
            //            from s in r.DefaultIfEmpty()
            //            where g.Username == username
            //            && d.IsActive == 1
            //            && m.isAllowConnecting == 1
            //            && p.isActive == 1
            //            && s.isActive == 1
            //            && s.API_Name == "UpdateSupplier"
            //            select new SystemRoleListModels
            //            {
            //                Username = username,
            //                SystemID = a.SystemID,
            //                SystemName = d.SystemName,
            //                Description = d.Description,
            //                SystemGrpID = d.SystemGrpID,
            //                IsShowOnList = d.IsShowOnList,
            //                IsShowRoleList = d.IsShowRoleList,
            //                API_URL = s.API_URL,
            //                API_Key = s.API_Key,
            //                API_Name = s.API_Name,
            //                //APIName_InsertUser = d.APIName_InsertUser,
            //                //APIName_UpdateUser = d.APIName_UpdateUser,
            //                //APIName_UpdateSupplier = d.APIName_UpdateSupplier,
            //                IsChecked = a.isEnableService ?? 0,
            //                SysRoleID = "PrimaryUser"
            //            };

            //SysRoleID = "PrimaryUser" ฟิกไว้เพื่อไป Get ค่า SysRoleID ของ User ที่เป็น Primary กรณีที่ต้อง call update ใน user ที่มาจากการ merge OP ซึ่งอาจมาหลาย SysRoleID

            var result = query.Distinct().ToList();

            return result;
        }
    }
}
