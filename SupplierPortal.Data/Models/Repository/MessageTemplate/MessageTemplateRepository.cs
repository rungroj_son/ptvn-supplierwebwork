﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.MessageTemplate
{
    public partial class MessageTemplateRepository : IMessageTemplateRepository
    {
         private SupplierPortalEntities context;


        public MessageTemplateRepository()
        {
            context = new SupplierPortalEntities();
        }

        public MessageTemplateRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual Tbl_MessageTemplate GetMessageTemplateByName(string name)
        {
            var result = context.Tbl_MessageTemplate.Where(m => m.IsActive == 1 && m.Name == name).FirstOrDefault();

            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
