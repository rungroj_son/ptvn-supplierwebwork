﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.BillingDetail;

namespace SupplierPortal.Data.Models.Repository.BillingDetail
{
    public partial interface IBillingDetailRepository
    {
        IQueryable<Tbl_BillingDetail> GetAll();

        IQueryable<BillingAndPaymentModels> GetBillingAndPaymentAll();

        IEnumerable<Tbl_BillingDetail> GetBillingDetailAll();

        IQueryable<BillingAndPaymentModels> BillingAndPaymentPerform();

        IQueryable<BillingAndPaymentModels> BillingAndPaymentPerformBySupplierID(int supplierID);

        void Dispose();

    }
}
