﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.BillingDetail;
using SupplierPortal.Data.Helper;
using System.Web;

namespace SupplierPortal.Data.Models.Repository.BillingDetail
{
    public partial class BillingDetailRepository : IBillingDetailRepository
    {

        private SupplierPortalEntities context;


        public BillingDetailRepository()
        {
            context = new SupplierPortalEntities();
        }

        public BillingDetailRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public IQueryable<Tbl_BillingDetail> GetAll()
        {

            IQueryable<Tbl_BillingDetail> query = context.Tbl_BillingDetail;
            return query;
        }

        public IQueryable<BillingAndPaymentModels> GetBillingAndPaymentAll()
        {
            IQueryable<BillingAndPaymentModels> query = from db in context.Tbl_BillingDetail
                                                        select new BillingAndPaymentModels
                                                                      {
                                                                          SupplierID = db.SupplierID,
                                                                          DocNo = db.DocNo,
                                                                          BillingServiceID = db.BillingServiceID,
                                                                          DocTypeID = db.DocTypeID,                                                                          
                                                                          DocDate = db.DocDate,
                                                                          DueDate = db.DueDate,
                                                                          Volume = db.Volume,
                                                                          InvoiceAmount = db.InvoiceAmount,
                                                                          CNAmount=db.CNAmount,
                                                                          RemainAmount=db.RemainAmount                                                                         
                                                                      };
            return query;
        }

        public IEnumerable<Tbl_BillingDetail> GetBillingDetailAll()
        {

            var query = from db in context.Tbl_BillingDetail
                        select db;

            var result = query.ToList();

            return result;
        }

        public virtual IQueryable<BillingAndPaymentModels> BillingAndPaymentPerform()
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from db in context.Tbl_BillingDetail
                        join bc in context.Tbl_BillingDocType on db.DocTypeID equals bc.DocTypeID into pg
                        from dt in pg.DefaultIfEmpty()
                        join ac in context.Tbl_BillingService on db.BillingServiceID equals ac.BillingServiceID into jc
                        from bs in jc.DefaultIfEmpty()
                        select new BillingAndPaymentModels
                        {
                            SupplierID = db.SupplierID,
                            BillingService = ((from ml in context.Tbl_LocalizedProperty
                                               where ml.EntityID == db.BillingServiceID
                                                && ml.LanguageID == languageId
                                                && ml.LocaleKeyGroup.Trim() == "Tbl_BillingService"
                                                && ml.LocaleKey.Trim() == "BillingService"
                                               select ml.LocaleValue
                                                )).FirstOrDefault() ?? bs.BillingService,           
                            DocNo = db.DocNo,    
                            BillingServiceID = db.BillingServiceID,
                            DocTypeID = db.DocTypeID,
                            DocType = ((from pd in context.Tbl_LocalizedProperty
                                        where pd.EntityID == db.DocTypeID
                                        && pd.LanguageID == languageId
                                        && pd.LocaleKeyGroup == "Tbl_BillingDocType"
                                        && pd.LocaleKey == "DocType"
                                        select pd.LocaleValue
                                       )).FirstOrDefault() ?? dt.DocType,
                            DocDate = db.DocDate,
                            DueDate = db.DueDate,
                            Volume = db.Volume,
                            InvoiceAmount = db.InvoiceAmount
                        };
            return query;
        }

        public virtual IQueryable<BillingAndPaymentModels> BillingAndPaymentPerformBySupplierID(int supplierID)
        {
            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var query = from db in context.Tbl_BillingDetail
                        join bc in context.Tbl_BillingDocType on db.DocTypeID equals bc.DocTypeID into pg
                        from dt in pg.DefaultIfEmpty()
                        join ac in context.Tbl_BillingService on db.BillingServiceID equals ac.BillingServiceID into jc
                        from bs in jc.DefaultIfEmpty()
                        where db.SupplierID == supplierID
                        select new BillingAndPaymentModels
                        {
                            SupplierID = db.SupplierID,                         
                            DocNo = db.DocNo,
                            BillingServiceID = db.BillingServiceID,                           
                            DocTypeID = db.DocTypeID,
                            DocType = ((from pd in context.Tbl_LocalizedProperty
                                        where pd.EntityID == db.DocTypeID
                                        && pd.LanguageID == languageId
                                        && pd.LocaleKeyGroup == "Tbl_BillingDocType"
                                        && pd.LocaleKey == "DocType"
                                        select pd.LocaleValue
                                       )).FirstOrDefault() ?? dt.DocType,
                            DocDate = db.DocDate,
                            DueDate = db.DueDate,
                            Volume = db.Volume,
                            VolumeCurrency = db.VolumeCurrency,
                            InvoiceAmount = db.InvoiceAmount,
                            CNAmount = db.CNAmount,
                            RemainAmount = db.RemainAmount,
                            PaymentAmount = db.PaymentAmount,
                            BillingService = ((from ml in context.Tbl_LocalizedProperty
                                               where ml.EntityID == db.BillingServiceID
                                                && ml.LanguageID == languageId
                                                && ml.LocaleKeyGroup.Trim() == "Tbl_BillingService"
                                                && ml.LocaleKey.Trim() == "BillingService"
                                               select ml.LocaleValue
                                               )).FirstOrDefault() ?? bs.BillingService,
                           
                        };
            return query;
        }

        public void Dispose()
        {
            context.Dispose();
        }

    }
}
