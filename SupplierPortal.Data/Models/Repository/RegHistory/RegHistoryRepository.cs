﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.RegHistory
{
    public partial class RegHistoryRepository : IRegHistoryRepository
    {

        SupplierPortalEntities context;

        public RegHistoryRepository()
        {
            context = new SupplierPortalEntities();
        }

        public RegHistoryRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

         public virtual void Insert(Tbl_RegHistory model)
        {
            try
            {
                if (model != null)
                {
                    context.Tbl_RegHistory.Add(model);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
