﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.OrgAddress
{
    public partial interface IOrgAddressRepository
    {
        Tbl_OrgAddress GetAddresType(int supplierID, int addressTypeID);

        Tbl_OrgAddress GetAddress(int supplierID, int addressID, int addressTypeID);

        void UpdateAddress(Tbl_OrgAddress model, int address);

        void InsertAddress(Tbl_OrgAddress model);


        void Dispose();
    }
}
