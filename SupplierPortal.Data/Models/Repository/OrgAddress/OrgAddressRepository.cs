﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.LogOrgAddress;


namespace SupplierPortal.Data.Models.Repository.OrgAddress
{
    public partial class OrgAddressRepository : IOrgAddressRepository
    {
        private SupplierPortalEntities context;
        ILogOrgAddressRepository _logOrgAddressRepository;

        public OrgAddressRepository()
        {
            context = new SupplierPortalEntities();
            _logOrgAddressRepository = new LogOrgAddressRepository();
        }

        public OrgAddressRepository(
            SupplierPortalEntities context,
            LogOrgAddressRepository logOrgAddressRepository
            )
        {
            this.context = context;
            _logOrgAddressRepository = logOrgAddressRepository;
        }

        public virtual Tbl_OrgAddress GetAddresType(int supplierID, int addressTypeID)
        {
            var data = from list in context.Tbl_OrgAddress
                       where list.AddressTypeID == addressTypeID
                       && list.SupplierID == supplierID
                       select list;

            var result = data.FirstOrDefault();
            return result;
        }

        public virtual Tbl_OrgAddress GetAddress(int supplierID, int addressID, int addressTypeID)
        {
            var data = from list in context.Tbl_OrgAddress
                       where list.AddressID == addressID
                       && list.SupplierID == supplierID
                       && list.AddressTypeID == addressTypeID
                       select list;

            var result = data.FirstOrDefault();
            return result;
        }

        public virtual void InsertAddress(Tbl_OrgAddress model)
        {

            try
            {
                int addressID = 0;

                if (model != null)
                {
                    context.Tbl_OrgAddress.Add(model);
                    context.SaveChanges();

                    addressID = model.AddressID;

                    _logOrgAddressRepository.Insert(addressID,model.AddressTypeID, model.SupplierID, "Add");
                }
            }
            catch
            {
                throw;
            }


        }

        public virtual void UpdateAddress(Tbl_OrgAddress model, int address)
        {
            try
            {
                var tempModel = context.Tbl_OrgAddress.Find(model.SupplierID, model.AddressID, model.AddressTypeID);

                if (tempModel != null)
                {

                    var orgAdd = new Tbl_OrgAddress()
                    {
                        SupplierID = model.SupplierID,
                        AddressID = address,
                        AddressTypeID = model.AddressTypeID,
                        SeqNo = model.SeqNo
                    };

                    context.Tbl_OrgAddress.Remove(tempModel);

                    context.Tbl_OrgAddress.Add(orgAdd);
                    context.SaveChanges();
                    //InsertAddress(model);

                    _logOrgAddressRepository.Insert(orgAdd.AddressID,orgAdd.AddressTypeID,orgAdd.SupplierID ,"Modify");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }

}
