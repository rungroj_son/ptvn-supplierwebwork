﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogRegServiceTypeMapping
{
    public partial class LogRegServiceTypeMappingRepository : ILogRegServiceTypeMappingRepository
    { 
        SupplierPortalEntities context;

        public LogRegServiceTypeMappingRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogRegServiceTypeMappingRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(int Id, int updateBy,string logAction)
        {
            try
            {
                var tempModel = context.Tbl_RegServiceTypeMapping.Where(m => m.Id == Id).FirstOrDefault();
                if (tempModel != null)
                {
                    var _logModel = new Tbl_LogRegServiceTypeMapping()
                    {
                        Id = tempModel.Id,
                        RegID = tempModel.RegID,
                        ServiceTypeID = tempModel.ServiceTypeID,
                        InvitationCode = tempModel.InvitationCode,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogRegServiceTypeMapping.Add(_logModel);
                    context.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
