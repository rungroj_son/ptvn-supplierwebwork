﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrgProductKeyword
{
    public partial interface ILogOrgProductKeywordRepository
    {
        void Insert(int supplierID, string keyword, string logAction, int updateBy);

        void Dispose();
    }
}
