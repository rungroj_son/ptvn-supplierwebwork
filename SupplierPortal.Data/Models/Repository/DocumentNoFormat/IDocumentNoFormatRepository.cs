﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.DocumentNoFormat
{
    public partial interface IDocumentNoFormatRepository
    {
        IEnumerable<Tbl_DocumentNoFormat> GetDocumentNoFormatListBy(string docName);
        void Dispose();
    }
}
