﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.RegisProductCategory
{
    public partial interface IRegisProductCategoryRepository
    {
        void Insert(Tbl_RegProductCategory model);

        void Edit(Tbl_RegProductCategory model);

        IEnumerable<Tbl_RegProductCategory> GetProductCatRegis(int? RegisId);

        void RemoveDataOrgProductCategory(int RegisId);

        void Dispose();
    }
}
