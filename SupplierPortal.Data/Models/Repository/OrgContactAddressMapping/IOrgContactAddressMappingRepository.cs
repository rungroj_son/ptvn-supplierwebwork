﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.OrgContactAddressMapping
{
    public partial interface IOrgContactAddressMappingRepository
    {

        Tbl_OrgContactAddressMapping GetOrgContactAddrMappingByContactID(int contactID);

        void Insert(Tbl_OrgContactAddressMapping model, int updateBy);

        void Delete(int id, int updateBy);

        void Dispose();
    }
}
