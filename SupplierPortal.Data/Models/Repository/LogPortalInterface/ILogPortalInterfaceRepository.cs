﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.LogPortalInterface
{
    public partial interface ILogPortalInterfaceRepository
    {

        void Insert(Tbl_LogPortalInterface logPortalInterface);

        void Dispose();

    }
}
