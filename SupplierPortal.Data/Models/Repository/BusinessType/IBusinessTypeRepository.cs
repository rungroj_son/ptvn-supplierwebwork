﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.BusinessType
{
    public partial interface IBusinessTypeRepository
    {
        IEnumerable<TempForMultiLang> GetMultiLangBusinessTypeByLangid(int languageID);

        IEnumerable<Tbl_BusinessType> GetBusinessTypeAll();

        void Dispose();
    }
}
