﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;

namespace SupplierPortal.Data.Models.Repository.BusinessType
{
    public partial class BusinessTypeRepository : IBusinessTypeRepository
    {
        private SupplierPortalEntities context;

        public BusinessTypeRepository()
        {
            context = new SupplierPortalEntities();
        }

        public BusinessTypeRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<TempForMultiLang> GetMultiLangBusinessTypeByLangid(int languageID)
        {
            ILocalizedPropertyRepository _repo = new LocalizedPropertyRepository();
            var result = _repo.GetAllEntityidLocalizedPropByMany("Tbl_BusinessType", "BusinessType", languageID);
            _repo.Dispose();

            if (result.Count() > 0)
            {
                return (from list in result
                        select new TempForMultiLang
                        {
                            EntityID = list.EntityID,
                            EntityValue = list.LocaleValue,
                        }).ToList<TempForMultiLang>();
            }
            else
            {
                return (from list in context.Tbl_BusinessType
                        select new TempForMultiLang
                        {
                            EntityID = list.BusinessTypeID,
                            EntityValue = list.BusinessTypeDesc,
                        }).ToList<TempForMultiLang>();
            }
        }

        public virtual IEnumerable<Tbl_BusinessType> GetBusinessTypeAll()
        {
            var query = from a in context.Tbl_BusinessType
                        orderby a.BusinessTypeID
                        select a;

            return query.ToList();
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
