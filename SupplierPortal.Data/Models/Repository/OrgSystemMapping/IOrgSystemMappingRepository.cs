﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.OrgSystemMapping
{
    public partial interface IOrgSystemMappingRepository
    {
        //IEnumerable<Tbl_OrgSystemMapping> GetOrgSystemMappingBySupplierIdAndSystemId(int supplierID, int systemID);

        bool CheckExistsOrgSystemMappingBySupplierIdAndSystemId(int supplierID, int systemID);

        void Dispose();
    }
}
