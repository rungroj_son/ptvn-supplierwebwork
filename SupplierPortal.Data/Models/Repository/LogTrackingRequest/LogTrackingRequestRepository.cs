﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogTrackingRequest
{
    public partial class LogTrackingRequestRepository : ILogTrackingRequestRepository
    {
        SupplierPortalEntities context;

        public LogTrackingRequestRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogTrackingRequestRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(int trackingReqID, int updateBy, string logAction)
        {
            try
            {
                var tempModel = context.Tbl_TrackingRequest.Where(m => m.TrackingReqID == trackingReqID).FirstOrDefault();
                if (tempModel != null)
                {
                    var model = new Tbl_LogTrackingRequest()
                    {
                        TrackingReqID = tempModel.TrackingReqID,
                        SupplierID = tempModel.SupplierID,
                        ReqBy = tempModel.ReqBy,
                        ReqDate = tempModel.ReqDate,
                        ApproveBy = tempModel.ApproveBy,
                        TrackingGrpID = tempModel.TrackingGrpID,
                        TrackingStatusID = tempModel.TrackingStatusID,
                        isDeleted = tempModel.isDeleted,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction,
                        Remark = "",
                        TrackingStatusDate = tempModel.TrackingStatusDate
                    };
                    context.Tbl_LogTrackingRequest.Add(model);
                    context.SaveChanges();
                }
            }
            catch
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
