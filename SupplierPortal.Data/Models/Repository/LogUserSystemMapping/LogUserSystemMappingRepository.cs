﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogUserSystemMapping
{
    public partial class LogUserSystemMappingRepository : ILogUserSystemMappingRepository
    {
        SupplierPortalEntities context;

        public LogUserSystemMappingRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogUserSystemMappingRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(Tbl_LogUserSystemMapping model)
        {
            try
            {
                if (model != null)
                {
                    context.Tbl_LogUserSystemMapping.Add(model);
                    context.SaveChanges();
                }
            }
            catch
            {
                throw;
            }

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }

    }
}
