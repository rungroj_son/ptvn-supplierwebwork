﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogRegContactRequestServiceTypeMapping
{
    public partial class LogRegContactReqServiceTypeMappingRepository : ILogRegContactReqServiceTypeMappingRepository
    {

        SupplierPortalEntities context;

        public LogRegContactReqServiceTypeMappingRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogRegContactReqServiceTypeMappingRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(int Id, int updateBy, string logAction)
        {
            var tempModel = context.Tbl_RegContactRequestServiceTypeMapping.Where(m => m.Id == Id).FirstOrDefault();
            if (tempModel != null)
            {
                var _logModel = new Tbl_LogRegContactRequestServiceTypeMapping()
                {
                    Id = tempModel.Id,
                    UserReqId = tempModel.UserReqId,
                    ServiceTypeID = tempModel.ServiceTypeID,
                    InvitationCode = tempModel.InvitationCode,
                    UpdateBy = updateBy,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };

                context.Tbl_LogRegContactRequestServiceTypeMapping.Add(_logModel);
                context.SaveChanges();
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
