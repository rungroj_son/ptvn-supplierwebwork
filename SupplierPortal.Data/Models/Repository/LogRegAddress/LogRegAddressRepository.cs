﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;

namespace SupplierPortal.Data.Models.Repository.LogRegAddress
{
    public partial class LogRegAddressRepository : ILogRegAddressRepository
    {
        private SupplierPortalEntities context;

        public LogRegAddressRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogRegAddressRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual bool Insert(int addrid, string logAction)
        {
            try
            {
                var _regAddr = context.Tbl_RegAddress.FirstOrDefault(m => m.AddressID == addrid);

                var _logRegAddress = new Tbl_LogRegAddress()
                {
                    AddressID = _regAddr.AddressID,
                    HouseNo_Local = _regAddr.HouseNo_Local,
                    HouseNo_Inter = _regAddr.HouseNo_Inter,
                    VillageNo_Local = _regAddr.VillageNo_Local,
                    VillageNo_Inter = _regAddr.VillageNo_Inter,
                    Lane_Local = _regAddr.Lane_Local,
                    Lane_Inter = _regAddr.Lane_Inter,
                    Road_Local = _regAddr.Road_Local,
                    Road_Inter = _regAddr.Road_Inter,
                    SubDistrict_Local = _regAddr.SubDistrict_Local,
                    SubDistrict_Inter = _regAddr.SubDistrict_Inter,
                    City_Local = _regAddr.City_Local,
                    City_Inter = _regAddr.City_Inter,
                    State_Local = _regAddr.State_Local,
                    State_Inter = _regAddr.State_Inter,
                    CountryCode = _regAddr.CountryCode,
                    PostalCode = _regAddr.PostalCode,
                    UpdateBy = 1,
                    UpdateDate = DateTime.UtcNow,
                    LogAction = logAction
                };
                if (_logRegAddress != null)
                {
                    context.Tbl_LogRegAddress.Add(_logRegAddress);
                    context.SaveChanges();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
