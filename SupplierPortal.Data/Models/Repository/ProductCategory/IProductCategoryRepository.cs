﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.Register;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.ProductCategory
{
    public partial interface IProductCategoryRepository
    {
        IEnumerable<Tbl_ProductCategoryLev1> GetProductCat1(int languageId);

        IEnumerable<Tbl_ProductCategoryLev2> GetProductCat2(int idCat1, int languageId);

        IEnumerable<Tbl_ProductCategoryLev3> GetProductCat3(int idCat2, int languageId);

        IEnumerable<CategoryModel> GetItemCat1ByCat3(int?[] idCat3);

        IEnumerable<Tbl_ProductCategoryLev3> GetItemCat3(int[] idCat3);

        IEnumerable<Tbl_OrgProductCategory> GetOrgCatDataBySupplierID(int SupplierID);

        void Insert(IEnumerable<Tbl_OrgProductCategory> model);

        void RemoveDataOrgProductCategory(int SupplierID);

        void Dispose();
    }
}
