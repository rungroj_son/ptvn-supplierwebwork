﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;


namespace SupplierPortal.Data.Models.Repository.DocumentDwnld
{
    public partial interface IDocumentDwnldRepository
    {

        IEnumerable<Tbl_DocumentDwnld> List();

        IEnumerable<Tbl_DocumentDwnld> ListByLG(int languageID);

        void Dispose();
    }
}
