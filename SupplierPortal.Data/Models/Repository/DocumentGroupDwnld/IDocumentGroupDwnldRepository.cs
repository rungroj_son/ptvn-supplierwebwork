﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.DocumentGroupDwnld
{
    public partial interface IDocumentGroupDwnldRepository
    {

        IEnumerable<Tbl_DocumentGroupDwnld> List();

        void Dispose();
    }
}
