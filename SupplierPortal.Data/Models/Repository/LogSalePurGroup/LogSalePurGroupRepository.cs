﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.LogSalePurGroup
{
    public partial class LogSalePurGroupRepository : ILogSalePurGroupRepository
    {
        private SupplierPortalEntities context;

        public LogSalePurGroupRepository()
        {
            context = new SupplierPortalEntities();
        }

        public LogSalePurGroupRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public void insertLogProductList(int id, int updateBy, string logAction)
        {
            try
            {
                var _data = context.Tbl_OrgSaleInfo_ProductList.FirstOrDefault(m => m.ContactID == id);

                if (_data != null)
                {
                    var _logData = new Tbl_LogOrgSaleInfo_ProductList()
                    {
                        ContactID = _data.ContactID,
                        ProductList = _data.ProductList,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgSaleInfo_ProductList.Add(_logData);
                    context.SaveChanges();
                }

            }
            catch
            {
                throw;
            }
        }

        public void insertLogBuyerList(int id,int Purgroup, int updateBy, string logAction)
        {
            try
            {
                var _data = context.Tbl_OrgSaleInfo_Purgroup.FirstOrDefault(m => m.ContactID == id && m.PurGroupId == Purgroup);

                if (_data != null)
                {
                    var _logData = new Tbl_LogOrgSaleInfo_Purgroup()
                    {
                        ContactID = _data.ContactID,
                        Id = _data.Id,
                        PurGroupId = _data.PurGroupId,
                        UpdateBy = updateBy,
                        UpdateDate = DateTime.UtcNow,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrgSaleInfo_Purgroup.Add(_logData);
                    context.SaveChanges();
                }

            }
            catch
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
