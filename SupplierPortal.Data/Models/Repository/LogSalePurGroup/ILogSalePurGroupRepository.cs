﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogSalePurGroup
{
    public partial interface ILogSalePurGroupRepository
    {
        void insertLogProductList(int id, int updateBy, string logAction);

        void insertLogBuyerList(int id,int Purgroup, int updateBy, string logAction);

        void Dispose();
    }
}
