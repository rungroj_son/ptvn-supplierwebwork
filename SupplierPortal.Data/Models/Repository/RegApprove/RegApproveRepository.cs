﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.RegApprove
{
    public partial class RegApproveRepository : IRegApproveRepository
    {
        private SupplierPortalEntities context;

        public RegApproveRepository()
        {
            context = new SupplierPortalEntities();
        }

        public RegApproveRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public Tbl_RegApprove GetRegApproveByRegID(int regID)
        {
            Tbl_RegApprove result;
            try
            {
                result = new Tbl_RegApprove();
                var temp = context.Tbl_RegApprove.Where(w => w.RegID == regID);

                if (temp != null)
                {
                    result = temp.FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
