﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrganization
{
    public partial class LogOrganizationRepository : ILogOrganizationRepository
    {
        private SupplierPortalEntities context;

        public LogOrganizationRepository()
        {
            context = new SupplierPortalEntities();
        }
        public LogOrganizationRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual void Insert(int SupID,int UpdateBy, string logAction)
        {
            try
            {
                var model = context.Tbl_Organization.FirstOrDefault(m => m.SupplierID == SupID);

                if (model != null)
                {
                    var _logOrg = new Tbl_LogOrganization()
                    {
                        SupplierID = model.SupplierID,
                        OrgID = model.OrgID,
                        PursiteID = model.PursiteID,
                        CountryCode = model.CountryCode,
                        TaxID = model.TaxID,
                        DUNSNumber = model.DUNSNumber,
                        CompanyTypeID = model.CompanyTypeID,
                        BusinessEntityID = model.BusinessEntityID,
                        OtherBusinessEntity = model.OtherBusinessEntity,
                        CompanyName_Local = model.CompanyName_Local,
                        CompanyName_Inter = model.CompanyName_Inter,
                        InvName_Local = model.InvName_Local,
                        InvName_Inter = model.InvName_Inter,
                        BranchNo = model.BranchNo,
                        BranchName_Local = model.BranchName_Local,
                        BranchName_Inter = model.BranchName_Inter,
                        MainBusinessCode = model.MainBusinessCode,
                        PhoneNo = model.PhoneNo,
                        PhoneExt = model.PhoneExt,
                        MobileNo = model.MobileNo,
                        FaxNo = model.FaxNo,
                        FaxExt = model.FaxExt,
                        WebSite = model.WebSite,
                        Latitude = model.Latitude,
                        Longtitude = model.Longtitude,
                        YearEstablished = model.YearEstablished,
                        RegisteredCapital = model.RegisteredCapital,
                        CurrencyCode = model.CurrencyCode,
                        NumberOfEmp = model.NumberOfEmp,
                        isPTVNVerified = model.isPTVNVerified,
                        isAffiliateCP = model.isAffiliateCP,
                        LastUpdate = DateTime.UtcNow,
                        UpdateBy = UpdateBy,
                        UpdateDate = DateTime.UtcNow,
                        isDeleted = model.isDeleted,
                        LogAction = logAction
                    };

                    context.Tbl_LogOrganization.Add(_logOrg);
                    context.SaveChanges();
                }

            }
            catch
            {
                throw;
            }
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
