﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Data.Models.Repository.LogOrganization
{
        public partial interface ILogOrganizationRepository
        {

            void Insert(int SupID, int UpdateBy, string logAction);

            void Dispose();
        }
}
