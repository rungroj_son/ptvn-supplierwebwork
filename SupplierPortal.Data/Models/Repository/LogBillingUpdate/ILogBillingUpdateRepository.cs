﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.LogBillingUpdate
{
    public partial interface ILogBillingUpdateRepository
    {
        Tbl_BillingDetailLogUpdate GetLastUpdate();

        void Dispose();
    }
}
