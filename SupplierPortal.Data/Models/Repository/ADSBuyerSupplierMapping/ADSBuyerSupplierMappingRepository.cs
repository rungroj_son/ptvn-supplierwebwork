﻿using SupplierPortal.Data.Models.MappingTable;
using System.Collections.Generic;
using System.Linq;

namespace SupplierPortal.Data.Models.Repository.ADSBuyerSupplierMapping
{
    public partial class ADSBuyerSupplierMappingRepository : IADSBuyerSupplierMappingRepository
    {

        private SupplierPortalEntities context;

        public ADSBuyerSupplierMappingRepository()
        {
            context = new SupplierPortalEntities();
        }

        public ADSBuyerSupplierMappingRepository(SupplierPortalEntities ctx)
        {
            this.context = ctx;
        }

        public virtual Tbl_ADSBuyerSupplierMapping GetADSBuyerSupplierMappingByBuyerOrgIDAndSupplierOrgID(string buyerOrgID, string supplierOrgID)
        {
            var query = from a in context.Tbl_ADSBuyerSupplierMapping
                        where a.BuyerOrgID == buyerOrgID
                        && a.SupplierOrgID == supplierOrgID
                        select a;

            return query.FirstOrDefault();
        }

    }
}
