﻿using System.Collections.Generic;
using System.Linq;
using SupplierPortal.Data.Models.MappingTable;


namespace SupplierPortal.Data.Models.Repository.ADSBuyerSupplierMapping
{
    public partial interface IADSBuyerSupplierMappingRepository
    {
        Tbl_ADSBuyerSupplierMapping GetADSBuyerSupplierMappingByBuyerOrgIDAndSupplierOrgID(string buyerOrgID,string supplierOrgID);
    }
}
