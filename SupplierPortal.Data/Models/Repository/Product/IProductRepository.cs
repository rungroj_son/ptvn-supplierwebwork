﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Models.SupportModel;

namespace SupplierPortal.Data.Models.Repository.Product
{
    public partial interface IProductRepository
    {
        IEnumerable<TempForMultiLang> GetMultiLangProductByLangid(int languageID);

        IEnumerable<TempProductModel> GetMultiLangProductByMany(int languageID, string[] id);

        IEnumerable<TempProductModel> GetMultiLangProductByManyCode(int languageID, string[] productcode);

        IQueryable<ProductModel> GetQrbMultiLangProductByLangid(int languageID);

        IQueryable<ProductModel> GetProductByLanguageID(int languageID);

        IQueryable<ProductModel> GetProducRange(int skip, int take, string filter);

        IQueryable<ProductModel> GetProductAll();

        IEnumerable<ProductCategoryResultModels> GetSearchProductandService(string languageID, string keySearch);

        TempProductModel GetMultiLangProductByProductCode(int languageID, int productID);

        void Dispose();
    }
}
