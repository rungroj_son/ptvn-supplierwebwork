﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.AutoComplete_SubDistrict
{
    public partial interface IAutoComplete_SubDistrictRepository
    {
        IEnumerable<Tbl_AutoComplete_SubDistrict> GetSubDistrictByMany(string countrycode, int languageID);

        IEnumerable<Tbl_AutoComplete_SubDistrict> GetSubDistrictByCityID(string countrycode,int cityID, int languageID);

        IQueryable<Tbl_AutoComplete_SubDistrict> GetSubDistrictAllByCountrycodeAndLanguageID(string countrycode, int languageID);

        IQueryable<Tbl_AutoComplete_SubDistrict> GetSubDistrictAllByCountrycode(string countrycode);

        IQueryable<Tbl_AutoComplete_SubDistrict> GetSubDistrictRange(int skip, int take, string filter);

        IEnumerable<Tbl_AutoComplete_SubDistrict> GetSubDistrictList(string term);

        void Dispose();
    }
}
