﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Data.Models.Repository.AutoComplete_SubDistrict
{
    public partial class AutoComplete_SubDistrictRepository : IAutoComplete_SubDistrictRepository
    {
        private SupplierPortalEntities context;

        public AutoComplete_SubDistrictRepository()
        {
            context = new SupplierPortalEntities();
        }

        public AutoComplete_SubDistrictRepository(SupplierPortalEntities context)
        {
            this.context = context;
        }

        public virtual IEnumerable<Tbl_AutoComplete_SubDistrict> GetSubDistrictByMany(string countrycode, int languageID)
        {
            return (from list in context.Tbl_AutoComplete_SubDistrict
                    where list.CountryCode == countrycode
                    && list.LanguageID == languageID
                    select list).ToList<Tbl_AutoComplete_SubDistrict>();
        }

        public virtual IEnumerable<Tbl_AutoComplete_SubDistrict> GetSubDistrictByCityID(string countrycode, int cityID, int languageID)
        {
            var query = from a in context.Tbl_AutoComplete_SubDistrict
                        where a.CountryCode == countrycode
                        && a.CityID == cityID
                        && a.LanguageID == languageID
                        select a;

            var result = query.ToList();

            return result;
        }

        public IQueryable<Tbl_AutoComplete_SubDistrict> GetSubDistrictAllByCountrycodeAndLanguageID(string countrycode, int languageID)
        {

            IQueryable<Tbl_AutoComplete_SubDistrict> query = context.Tbl_AutoComplete_SubDistrict.Where(m => m.CountryCode == countrycode && m.LanguageID == languageID).OrderBy(c => c.SubDistrictName);
            return query;
        }

        public IQueryable<Tbl_AutoComplete_SubDistrict> GetSubDistrictAllByCountrycode(string countrycode)
        {

            IQueryable<Tbl_AutoComplete_SubDistrict> query = context.Tbl_AutoComplete_SubDistrict.Where(m => m.CountryCode == countrycode).OrderBy(c => c.SubDistrictName);
            return query;
        }

        public IQueryable<Tbl_AutoComplete_SubDistrict> GetSubDistrictRange(int skip, int take, string filter)
        {

            IQueryable<Tbl_AutoComplete_SubDistrict> query = context.Tbl_AutoComplete_SubDistrict.Where(c => c.SubDistrictName.StartsWith(filter)).OrderBy(c => c.SubDistrictName).Skip(skip).Take(take);
            return query;
        }

        public IEnumerable<Tbl_AutoComplete_SubDistrict> GetSubDistrictList(string term)
        {
            var query = (from a in context.Tbl_AutoComplete_SubDistrict
                         where a.SubDistrictName.ToLower().Contains(term.Trim().ToLower())
                         select a).Take(20);

            var result = query.ToList();

            return result;
        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
