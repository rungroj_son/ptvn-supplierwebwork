﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SupplierPortal.Data.Models.Repository.APILogs;
using SupplierPortal.WebApi.Models.Notification;
using SupplierPortal.Framework.MethodHelper;
using Newtonsoft.Json;
using SupplierPortal.Data.Models.Repository.Notification;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using SupplierPortal.Data.Models.Repository.APISecurity;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.MainFunction.Helper;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/Notification")]
    public class NotificationController : ApiController
    {
        IAPILogsRepository _aPILogsRepository;
        INotificationRepository _notificationRepository;
        IUserSystemMappingRepository _userSystemMappingRepository;
        IAPISecurityRepository _aPISecurityRepository;

        public NotificationController()
        {
            _aPILogsRepository = new APILogsRepository();
            _notificationRepository = new NotificationRepository();
            _userSystemMappingRepository = new UserSystemMappingRepository();
            _aPISecurityRepository = new APISecurityRepository();
        }

        public NotificationController(
            APILogsRepository aPILogsRepository,
            NotificationRepository notificationRepository,
            UserSystemMappingRepository userSystemMappingRepository,
            APISecurityRepository aPISecurityRepository
              )
        {

            _aPILogsRepository = aPILogsRepository;
            _notificationRepository = notificationRepository;
            _userSystemMappingRepository = userSystemMappingRepository;
            _aPISecurityRepository = aPISecurityRepository;

        }
        // POST api/Notification/AddNotification
        [AllowAnonymous]
        [Route("AddNotification")]
        public HttpResponseMessage AddNotification(AddNotificationModel model)
        {
            try
            {


                #region Check IP Security
                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();

                //string visitorsIPAddr2 = GetClientIPAddressHelper.GetClientIPAddresNonByPass();

                bool checkIPSecurity = _aPISecurityRepository.CheckAPISecurityByNameAndIpAddress("SWW_AddNotification", visitorsIPAddr);

                if (!checkIPSecurity)
                {
                    HttpResponseMessage respIPNotFound = new HttpResponseMessage();
                    respIPNotFound.ReasonPhrase = "IP address is NotFound.";
                    respIPNotFound.StatusCode = HttpStatusCode.NotFound;

                    var result = new List<Object>();
                    #region Insert Log
                    try
                    {
                        string jsonData = JsonHelper.CompleteJsonString(result, respIPNotFound);

                        _aPILogsRepository.InsertLogByParam(model.ReqID,
                            "Response",
                            visitorsIPAddr,
                            model.APIKey,
                            "AddNotification",
                            ((Int32?)respIPNotFound.StatusCode).ToString(),
                            respIPNotFound.IsSuccessStatusCode ? "true" : "false",
                            respIPNotFound.ReasonPhrase, jsonData, "", model.SystemID);
                    }
                    catch //(Exception ex)
                    {
                        throw;
                    }
                    #endregion

                    return JsonHelper.CompleteJson(result, respIPNotFound);
                }
                #endregion

                string keyNameAll = "";

                List<string> dataValueList = new List<string>();

                foreach (var dataKey in model.DataKey)
                {
                    string keyName = dataKey.Value;
                    dataValueList.Add(keyName);

                }

                foreach (var dataKey in dataValueList.OrderBy(m=>m))
                {
                    string keyName = dataKey;
                    keyNameAll += keyName + "|";

                }

                keyNameAll = keyNameAll.Remove(keyNameAll.Length - 1);

                string jsonRawData = JsonConvert.SerializeObject(model);

                var notificationList = _notificationRepository.GetNotificationList(keyNameAll, model.SystemID,model.TopicID);

                //_notificationRepository.DeleteRange(notificationList.ToList(),1);


                //foreach (var item in notificationList)
                //{
                //    _notificationRepository.DeleteByCommand(item.NotificationID, 1);
                //}

                foreach (var item in notificationList)
                {
                    _notificationRepository.Delete(item.NotificationID, 1);
                }

                List<int> userID = new List<int>();

                foreach (var item in model.SysUserInfo)
                {
                    var sysUserID = item.SysUserID;

                    string orgID = item.SupplierShortName;

                    var userMappingList = _userSystemMappingRepository.GetUserSystemMappingBySysUserIDAndSystemIdAndOrgID(sysUserID, model.SystemID, orgID);

                    var userIDList = userMappingList.Select(m => m.UserID).Distinct();

                    foreach (var list in userIDList)
                    {
                        userID.Add(list);
                    }
                }

                List<NotificationModel> NotificationIDList = new List<NotificationModel>();

                foreach (var item in userID.Distinct())
                {
                    var tbl_Notification = new Tbl_Notification()
                    {
                        NotificationKey = keyNameAll,
                        SystemID = model.SystemID,
                        TopicID = model.TopicID,
                        UserID = item,
                        RawData = jsonRawData,
                        StartTime = model.StartTime,
                        StopTime = model.StopTime,
                        isRead = 0,
                        isDeleted = 0,
                    };

                    int notificationID = _notificationRepository.InsertReturnNotificationID(tbl_Notification,1);

                    var notificationIDModel = new NotificationModel
                    {
                        NotificationID = notificationID
                    };
                    NotificationIDList.Add(notificationIDModel);
                }

                HttpResponseMessage respSuccess = new HttpResponseMessage();
                respSuccess.ReasonPhrase = "Success";
                respSuccess.StatusCode = HttpStatusCode.OK;

                var resultListSuccess = NotificationIDList;

                #region Insert Log
                try
                {
                    //string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    string jsonData = JsonHelper.CompleteJsonString(resultListSuccess, respSuccess);

                    _aPILogsRepository.InsertLogByParam(model.ReqID,
                        "Response",
                        visitorsIPAddr,
                        model.APIKey,
                        "AddNotification",
                        ((Int32?)respSuccess.StatusCode).ToString(),
                        respSuccess.IsSuccessStatusCode ? "true" : "false",
                        respSuccess.ReasonPhrase, jsonData, "",model.SystemID);
                }
                catch (Exception ex)
                {
                    throw;
                }
                #endregion

                return JsonHelper.CompleteJson(resultListSuccess, respSuccess);

            }
            catch (Exception ex)
            {

                string errorMessageAll = "";

                string jsonRawData = JsonConvert.SerializeObject(model);
                var strFormat = string.Format("{0}",ex);
                errorMessageAll += "{ ex.Message ==> " + ex.Message+" } ";
                errorMessageAll += "{ ex.strFormat ==> " + strFormat + " } ";
                errorMessageAll += " { ex.TargetSite ==> " + ex.TargetSite.ToString()+" } ";
                errorMessageAll += " { ex.StackTrace ==> " + ex.StackTrace.ToString() + " } ";
                errorMessageAll += " { jsonRawData ==> " + jsonRawData + " } ";

                HttpResponseMessage respError = new HttpResponseMessage();
                respError.ReasonPhrase = ex.Message;
                respError.StatusCode = HttpStatusCode.BadRequest;
                var result = new List<Object>();
                string jsonModelData = JsonConvert.SerializeObject(model);
                #region Insert Log

                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                string jsonData = JsonHelper.CompleteJsonString(result, respError);

                _aPILogsRepository.InsertLogByParam(model.ReqID,
                    "Response_Exception",
                    visitorsIPAddr,
                    model.APIKey,
                    "AddNotification",
                    ((Int32?)respError.StatusCode).ToString(),
                    respError.IsSuccessStatusCode ? "true" : "false",
                    errorMessageAll, jsonData, "", model.SystemID);

                #endregion

                return JsonHelper.CompleteJson(result, respError);

            }

            //HttpResponseMessage respNotFound = new HttpResponseMessage();
            //respNotFound.ReasonPhrase = "Unsuccessful";
            //respNotFound.StatusCode = HttpStatusCode.NotFound;

            //var resultListNotFound = new List<Object>();

            //#region Insert Log
            //try
            //{
            //    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
            //    string jsonData = JsonHelper.CompleteJsonString(resultListNotFound, respNotFound);

            //    _aPILogsRepository.InsertLogByParam("",
            //        "Response",
            //        visitorsIPAddr,
            //        "",
            //        "VerifyUserSession",
            //        ((Int32?)respNotFound.StatusCode).ToString(),
            //        respNotFound.IsSuccessStatusCode ? "true" : "false",
            //        respNotFound.ReasonPhrase, jsonData, "");
            //}
            //catch (Exception ex)
            //{
            //    throw;
            //}
            //#endregion

            //return JsonHelper.CompleteJson(resultListNotFound, respNotFound);
        }

    }
}
