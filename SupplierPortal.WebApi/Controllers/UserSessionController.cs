﻿using SupplierPortal.BusinessLogic.UserSession;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.UserSession;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/UserSession")]
    public class UserSessionController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUserSessionManager _userSessionManager;

        public UserSessionController()
        {
            _userSessionManager = new UserSessionManager();
        }

        [AllowAnonymous]
        [Route("GetUserSession")]
        public HttpResponseMessage GetUserSession(string userGuid)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _userSessionManager.GetUserSession(userGuid);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetUserSession (API) : " + ex.Message);
            }
            return response;
        }

        [AllowAnonymous]
        [Route("SCFUAA")]
        public HttpResponseMessage GetUserSessionSCFUAA(string userGuid)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _userSessionManager.GetUserSessionForSCF(userGuid);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetUserSession (API) : " + ex.Message);
            }
            return response;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("Update")]
        public HttpResponseMessage Update(Tbl_UserSession userSession)
        {
            HttpResponseMessage response = null;
            try
            {
                bool results = _userSessionManager.Update(userSession);

                response = new HttpResponseMessage();
                response.StatusCode = results == true ? HttpStatusCode.OK : HttpStatusCode.NotFound;
                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetUserSession (API) : " + ex.Message);
            }
            return response;
        }
    }
}