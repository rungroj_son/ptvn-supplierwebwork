﻿using SupplierPortal.BusinessLogic.PDPA;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/pdpa")]
    public class UserPDPAController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IUserPDPAManager _manager;

        public UserPDPAController()
        {
            _manager = new UserPDPAManager();
        }

        [Route("get")]
        [HttpPost]
        public HttpResponseMessage GettUserPDPA([FromBody] string username)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _manager.GetUserPDPA(username);

                response = new HttpResponseMessage();

                if (results != null)
                {
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                }

                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("UserPDPA (API) : " + ex.Message);
            }

            return response;
        }

        [Route("insert")]
        [HttpPost]
        public HttpResponseMessage InsertUserPDPA([FromBody] Users_PDPA user)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _manager.InsertUserPDPA(user);
                response = new HttpResponseMessage();

                if (results > 0)
                {
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                }

                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("UserPDPA (API) : " + ex.Message);
            }

            return response;
        }
    }
}