﻿using Newtonsoft.Json;
using SupplierPortal.BusinessLogic.CompanyProfile;
using SupplierPortal.Data.CustomModels.CompanyProfile;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.WebApi.Models.CompanyProfile;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/companyprofile")]
    public class CompanyProfileExternalController : ApiController
    {

        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IImageCompanyManager _imageCompanyManager;
        private readonly IAppConfigRepository _appConfigRepository;
        private readonly IApprovalListCompanyManager _approvalListCompanyManager;
        public CompanyProfileExternalController()
        {
            _imageCompanyManager = new ImageCompanyManager();
            _appConfigRepository = new AppConfigRepository();
            _approvalListCompanyManager = new ApprovalListCompanyManager();
        }

        [Route("data")]
        [HttpGet]
        public async Task<HttpResponseMessage> GetImageListOfCompanyAsync(string taxId, string branchNumber)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var imageListResponse = await _imageCompanyManager.GetImagePowerAndCompany(taxId, branchNumber);

                string jsonResult = new JavaScriptSerializer().Serialize(imageListResponse);
                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("Error GetImageListOfCompnay (API) : " + ex.Message);
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Content = new StringContent(ex.StackTrace);
                return response;
            }

            return response;
        }

        [Route("download")]
        [HttpGet]
        public HttpResponseMessage GetImageFileOfCompany(string attachmentNameUnique, string taxId, string branchNumber)
        {
            if (String.IsNullOrEmpty(attachmentNameUnique))
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var supplierId = _imageCompanyManager.GetSupplierIdByTaxId(taxId, branchNumber);
            string filePath;
            string filePathResponse;
            string pathAttachment = System.Web.Configuration.WebConfigurationManager.AppSettings["attachmentURL"];
            string pathSave = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentPath").Value;

            string pathForSaving = pathAttachment + pathSave + supplierId;
            filePath = pathForSaving + "/" + attachmentNameUnique;
            filePathResponse = filePath;

            if (!System.IO.File.Exists(@filePath))
            {
                filePath = @pathAttachment + @"\SWWDocuments\UserManualForDownload\EPInstruction.pdf";
            }

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            };
            response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = attachmentNameUnique
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");


            return response;
        }



        [Route("approvalList")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetApprovalListAsync(CompanyProfileApprovalRequest companyProfileApprovalRequestModel)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

            var approvalListResponse = await _approvalListCompanyManager.GetTrackingApprovalList(companyProfileApprovalRequestModel);
            string jsonResult = JsonConvert.SerializeObject(approvalListResponse,
                             Newtonsoft.Json.Formatting.None,
                             new JsonSerializerSettings
                             {
                                 NullValueHandling = NullValueHandling.Ignore
                             });
            response.StatusCode = HttpStatusCode.OK;
            response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            return response;

        }

    }


}