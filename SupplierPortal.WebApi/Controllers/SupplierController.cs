﻿
using SupplierPortal.BusinessLogic.ContactPerson;
using SupplierPortal.BusinessLogic.Organization;
using SupplierPortal.BusinessLogic.SDFavorite;
using SupplierPortal.Data.CustomModels.Organization;
using SupplierPortal.Data.CustomModels.SearchResult;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Models.Dto;
using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/th/supplier")]
    public class SupplierController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ISDFavoriteManager _SDManager;
        private readonly IContactPersonManager _ContactPersonManager;
        private readonly IOrganizationManager _organizationManager;

        public SupplierController()
        {
            _SDManager = new SDFavoriteManager();
            _ContactPersonManager = new ContactPersonManager();
            _organizationManager = new OrganizationManager();
        }

        #region GET
        [HttpGet]
        [Route("sdfavorites")]
        public HttpResponseMessage GetSDFavorites(int eid, int supplierId, int sysUserId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                List<Core_SupplierContactFavorite_Result> results = _SDManager.GetSDFavorites(eid, supplierId, sysUserId);
                int filteredCount = 0;
                int total = 0;
                if (results != null)
                {
                    filteredCount = results.Count;
                    total = results.Count;
                }

                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(new DatatableResponse<Core_SupplierContactFavorite_Result>() { data = results, draw = 0, recordsFiltered = filteredCount, Message = "success", recordsTotal = total, Success = true });
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("SDFavoriteManager (API) : " + ex.InnerException);
            }

            return response;
        }

        [HttpPost]
        [Route("users")]
        public HttpResponseMessage GetUserContactPersonsOfOrganization([FromBody] GetUsersRequest request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                Tuple<List<Tbl_ContactPersonDto>, int, int> tuple = null;
                DatatableResponse<Tbl_ContactPersonDto> result = null;
                tuple = _ContactPersonManager.GetUserContactPersonsOfOrganization(request.supplierId, request);

                if (tuple != null)
                {
                    if (tuple.Item1 != null)
                    {
                        result = new DatatableResponse<Tbl_ContactPersonDto>();
                        result.data = tuple.Item1;
                        result.recordsFiltered = tuple.Item2;
                        result.recordsTotal = tuple.Item3;
                        result.Success = true;
                        result.Message = "";
                    }
                }

                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(result);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("SDFavoriteManager (API) : " + ex.InnerException);
            }

            return response;
        }

        [HttpPost]
        [Route("shortdetail")]
        public HttpResponseMessage GetOrganizationShortDetail([FromBody] GetOrganizationShortDetailRequest request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                OrganizationShortDetailDto result = null;
                string taxId = request.taxId == null ? "" : request.taxId.Trim();
                string branchNo = request.branchNo == null ? "" : request.branchNo.Trim();
                if (string.IsNullOrEmpty(taxId))
                {
                    response = new HttpResponseMessage();
                    response.StatusCode = HttpStatusCode.NotFound;
                    return response;
                }

                result = _organizationManager.GetOrganizationShortDetail(taxId, branchNo);

                if (result == null)
                {
                    response = new HttpResponseMessage();
                    response.StatusCode = HttpStatusCode.NotFound;
                    return response;
                }

                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(result);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");

            }
            catch (Exception ex)
            {
                logger.Error("GetOrganizationShortDetail (API) : " + ex.InnerException);
            }

            return response;
        }

        [HttpGet]
        [Route("checkDuplicate/{taxID}/{countryCode}")]
        public HttpResponseMessage CheckDuplicateTaxID(string taxID, string countryCode)
        {
            HttpResponseMessage response = new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK
            };
            try
            {
                if (string.IsNullOrEmpty(taxID) || string.IsNullOrEmpty(countryCode))
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
                else
                {
                    var organizationList = _organizationManager.GetOrganizationList(taxID, countryCode);
                    if (organizationList == null || organizationList.Count == 0)
                    {
                        response.StatusCode = HttpStatusCode.NotFound;
                    }

                    string jsonResult = new JavaScriptSerializer().Serialize(new { data = organizationList });
                    response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
                }
            }
            catch (Exception ex)
            {
                logger.Error("CheckDuplicateTaxID (API) : " + ex.InnerException);
            }

            return response;
        }

        [HttpPost]
        [Route("get")]
        public HttpResponseMessage GetOrganizationWithPagination([FromBody] SearchResultRequest request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                SearchResultResponse<List<SearchOrgResponse>> result = new SearchResultResponse<List<SearchOrgResponse>>();
                result = _organizationManager.GetOrganizationWithPagination(request);

                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(result);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                response.StatusCode = HttpStatusCode.BadRequest;
                response.Content = new StringContent("_ContactPersion.ErrorMsg.InvalidColumnNameOrFormatValue", System.Text.Encoding.UTF8, "application/json");

                logger.Error("GetOrganizationShortDetail (API) : " + ex.InnerException);
            }

            return response;
        }
        #endregion

        #region UPDATE
        [HttpPut]
        [Route("sdfavorite")]

        public HttpResponseMessage UpdateSDFavorit([FromBody] Tbl_SDFavoriteDto request)
        {
            HttpResponseMessage response = null;
            try
            {
                var model = _SDManager.GetSDFavorite(request.SysUserID, request.EID.Value, request.UserID.Value);
                int updateBy = Int32.Parse(request.SysUserID);
                int resultId = 0;
                if (model != null)
                {
                    resultId = _SDManager.Delete(model.Id, updateBy);
                }
                else
                {
                    var modelTemp = new Tbl_SDFavorite();
                    modelTemp.SysUserID = request.SysUserID;
                    modelTemp.EID = request.EID;
                    modelTemp.UserID = request.UserID;

                    resultId = _SDManager.Insert(modelTemp, updateBy);
                }


                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = "";
                if (resultId > 0)
                {
                    jsonResult = new JavaScriptSerializer().Serialize(new BaseReponse() { Message = "success", Success = true });
                }
                else
                {
                    jsonResult = new JavaScriptSerializer().Serialize(new BaseReponse() { Message = "un success", Success = false });
                }



                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("SDFavoriteManager (API) : " + ex.InnerException);
            }

            return response;
        }

        #endregion
    }
}