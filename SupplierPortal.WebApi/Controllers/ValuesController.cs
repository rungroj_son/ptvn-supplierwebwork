﻿using SupplierPortal.MainFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.MessageTemplate;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using SupplierPortal.Data.Models.SupportModel.DynamicData;
using SupplierPortal.Framework.AccountManage;
using SupplierPortal.MainFunction.Helper;

namespace SupplierPortal.WebApi.Controllers
{
    //[Authorize]
    [RoutePrefix("api/TestCode")]
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
            
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }



        [AllowAnonymous]
        [Route("DebugCode")]
        public HttpResponseMessage Debug(string model, string languageId = "")
        {
            IUserRepository _repoUser = null;
            _repoUser = new UserRepository();

            IMessageTemplateRepository _repoMessageTemplate = null;
            _repoMessageTemplate = new MessageTemplateRepository();

            ILanguageRepository _repoLanguage = null;
             _repoLanguage = new LanguageRepository();

            var tbl_User = _repoUser.FindByUsername(model);

            var messageTemplate = _repoMessageTemplate.GetMessageTemplateByName("Login.ForgotPassword");

            string languageID = _repoLanguage.GetLanguageIdByCode(tbl_User.LanguageCode);
            string localeKeyGroup = "Tbl_MessageTemplate";
            string localeKey = "Subject";
            string pkTableMain = messageTemplate.MessageId.ToString();
            

            string xx = "";
            xx = GetLocalizedProperty.GetLocalizedValue(languageID, localeKeyGroup, localeKey, pkTableMain);

            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = "Unsuccessful";
            respNotFound.StatusCode = HttpStatusCode.NotFound;

            var resultListNotFound = new List<Object>();

            return JsonHelper.CompleteJson(resultListNotFound, respNotFound);


        }

        [AllowAnonymous]
        [Route("DebugCodeGetLocalizedValue")]
        public HttpResponseMessage DebugCodeGetLocalizedValue(string languageID, string localeKeyGroup, string pkTableMain)
        {
            IUserRepository _repoUser = null;
            _repoUser = new UserRepository();

            IMessageTemplateRepository _repoMessageTemplate = null;
            _repoMessageTemplate = new MessageTemplateRepository();

            //ILanguageRepository _repoLanguage = null;
            //_repoLanguage = new LanguageRepository();

            string sqlQuery = "";

            ILocalizedPropertyRepository _repoLocalizedProperty = null;
            _repoLocalizedProperty = new LocalizedPropertyRepository();

            sqlQuery = "EXEC LocalizedProperty_find '" + languageID + "','" + localeKeyGroup + "','" + pkTableMain + "' ";

            var result = _repoLocalizedProperty.GetLocalizedListByStore(sqlQuery);

            //DynamicDataModel model = _repoLocalizedProperty.GetLocalizedDynamicByStore(languageID, localeKeyGroup, pkTableMain);
            

            var messageTemplate = _repoMessageTemplate.GetMessageTemplateByName("Login.ForgotPassword");

            //var query = from db in result
            //            join a in messageTemplate
            //            on db.
            //var menuTop = query.ToList();

            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = "Unsuccessful";
            respNotFound.StatusCode = HttpStatusCode.NotFound;

            var resultListNotFound = new List<Object>();

            return JsonHelper.CompleteJson(resultListNotFound, respNotFound);


        }

        [AllowAnonymous]
        [Route("DebugCodePwdEncryptBSP")]
        public HttpResponseMessage DebugCodePwdEncryptBSP(string password,string oldPassword = "")
        {
            string hashPassword = "";
            string hashPassword2 = "";

            hashPassword = PasswordHashing.HashPasswordSHA1NoSalt(password);

            hashPassword2 = PasswordHashing.HashPasswordSSHA1(password, oldPassword);

            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = hashPassword2 + " [=====] " + hashPassword;
            respNotFound.StatusCode = HttpStatusCode.NotFound;

            var resultListNotFound = new List<Object>();

            return JsonHelper.CompleteJson(resultListNotFound, respNotFound);
        }

        [AllowAnonymous]
        [Route("DebugCodePwdEncrypteRFX")]
        public HttpResponseMessage DebugCodePwdEncrypteRFX(string password)
        {
            string hashPassword = "";

            hashPassword = PasswordHashing.GetMd5Hash(password);


            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = hashPassword;
            respNotFound.StatusCode = HttpStatusCode.NotFound;

            var resultListNotFound = new List<Object>();

            return JsonHelper.CompleteJson(resultListNotFound, respNotFound);
        }

    }
}
