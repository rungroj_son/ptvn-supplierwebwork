﻿using SupplierPortal.BusinessLogic.Language;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.APILogs;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.MainFunction.Helper;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/Language")]
    public class LanguageController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ILanguageManager _languagePortalManager;
        private readonly IAPILogsRepository _aPILogsRepository;
        public LanguageController()
        {
            _languagePortalManager = new LanguageManager();
            _aPILogsRepository = new APILogsRepository();
        }

        [HttpGet]
        [Route("GetLanguageIdByCode")]
        public HttpResponseMessage GetLanguageIdByCode(string languageCode)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _languagePortalManager.GetLanguageIdByCode(languageCode);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(new Tbl_Language() { LanguageID = int.Parse(results) });
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetLanguageIdByCode (API) : " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("islocal")]
        public HttpResponseMessage isLocalLanguage(int languageId)
        {
            HttpResponseMessage response = null;
            try
            {
                bool result = _languagePortalManager.IsLocalLanguage(languageId);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;

                string jsonResult = new JavaScriptSerializer().Serialize(result);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("isLocalLanguage (API) : " + ex.Message);
            }
            return response;
        }

        [HttpGet]
        [Route("GetLanguage")]
        public HttpResponseMessage GetLanguage()
        {
            HttpResponseMessage response = null;
            try
            {
                List<Tbl_Language> language = null;
                language = _languagePortalManager.GetLanguage();

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;

                string jsonResult = new JavaScriptSerializer().Serialize(language);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetLanguage (API) : " + ex.Message);
            }
            return response;
        }
    }
}