﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SupplierPortal.WebApi.Models.AccountPortal;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.Helpers;
using SupplierPortal.Core.Extension;
using System.Net.Http.Headers;
using SupplierPortal.Services.AccountManage;
using System.Web.Configuration;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Framework.AccountManage;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Data.Models.Repository.APILogs;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.LogonAttempt;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.MainFunction.Helper;
using SupplierPortal.Data.CustomModels.AccountPortal;
using SupplierPortal.BusinessLogic.PDPA;
using SupplierPortal.BusinessLogic.BillingCondition;
using SupplierPortal.BusinessLogic.SCF;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/AccountPortal")]
    public class AccountPortalController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        IAPILogsRepository _aPILogsRepository;
        ILanguageRepository _languageRepository;
        IUserRepository _userRepository;
        ILogonAttemptRepository _logonAttemptRepository;
        IAppConfigRepository _appConfigRepository;
        IUserPDPAManager _userPDPAManager;
        IUserBillingConditionManager _userBillingConditionManager;
        ISCFManager _sCFManager;


        public AccountPortalController()
        {
            _aPILogsRepository = new APILogsRepository();
            _languageRepository = new LanguageRepository();
            _userRepository = new UserRepository();
            _logonAttemptRepository = new LogonAttemptRepository();
            _appConfigRepository = new AppConfigRepository();
            _userPDPAManager = new UserPDPAManager();
            _userBillingConditionManager = new UserBillingConditionManager();
            _sCFManager = new SCFManager();
        }

        public AccountPortalController(
            APILogsRepository aPILogsRepository,
            LanguageRepository languageRepository,
            UserRepository userRepository,
            LogonAttemptRepository logonAttemptRepository,
            AppConfigRepository appConfigRepository,
            UserPDPAManager userPDPAManager,
            UserBillingConditionManager userBillingConditionManager
              )
        {

            _aPILogsRepository = aPILogsRepository;
            _languageRepository = languageRepository;
            _userRepository = userRepository;
            _logonAttemptRepository = logonAttemptRepository;
            _appConfigRepository = appConfigRepository;
            _userPDPAManager = userPDPAManager;
            _userBillingConditionManager = userBillingConditionManager;
        }


        // POST api/AccountPortal/ForgotPassword
        [AllowAnonymous]
        [Route("ForgotPassword")]
        public HttpResponseMessage ForgotPassword(ForgotPasswordModel model, string languageId = "")
        {
            string messageReturn = "";

            if (string.IsNullOrEmpty(languageId))
            {
                languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
            }

            if (!ModelState.IsValid)
            {
                HttpResponseMessage respError = new HttpResponseMessage();
                respError.ReasonPhrase = messageReturn.GetStringResource("_ApiMsg.DataIsValid", languageId);
                respError.StatusCode = HttpStatusCode.BadRequest;
                var result = new List<Object>();

                #region Insert Log
                try
                {
                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    string jsonData = JsonHelper.CompleteJsonString(result, respError);

                    _aPILogsRepository.InsertLogByParam("",
                        "Response",
                        visitorsIPAddr,
                        "",
                        "ForgotPassword",
                        ((Int32?)respError.StatusCode).ToString(),
                        respError.IsSuccessStatusCode ? "true" : "false",
                        respError.ReasonPhrase, jsonData, "");
                }
                catch (Exception ex)
                {
                    throw;
                }
                #endregion

                return JsonHelper.CompleteJson(result, respError);
            }

            var tbl_User = UserDataService.GetUserByUsername(model.Username);
            if (tbl_User != null)
            {
                IContactPersonRepository _repoContact = new ContactPersonRepository();

                int contactID = Convert.ToInt32(tbl_User.ContactID);
                var resultContact = _repoContact.GetContactPersonByContectID(contactID);

                try
                {
                    string email = "";
                    email = "<a href=\"mailto:" + resultContact.Email + "\"> " + resultContact.Email + " </a>";
                    messageReturn = messageReturn.GetStringResource("_Login.ForgotPassword.InfoMsg.RecoverySuccess", languageId);
                    messageReturn = messageReturn.Replace("<%email_address%>", email);

                    RecoveryPassword.RecoveryPasswordToEmail(tbl_User.Username);

                    HttpResponseMessage resp = new HttpResponseMessage();
                    resp.ReasonPhrase = "Success";
                    resp.StatusCode = HttpStatusCode.OK;

                    var userTemp = UserDataService.GetUserByUsername(tbl_User.Username);

                    var resultList = new List<ResultForgotPassword>();
                    resultList.Add(new ResultForgotPassword()
                    {
                        //Token = userTemp.PwdRecoveryToken,
                        Message = messageReturn,
                        Result = true,
                        Email = resultContact.Email

                    });

                    #region Insert Log
                    try
                    {
                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                        string jsonData = JsonHelper.CompleteJsonString(resultList, resp);

                        _aPILogsRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "ForgotPassword",
                            ((Int32?)resp.StatusCode).ToString(),
                            resp.IsSuccessStatusCode ? "true" : "false",
                            resp.ReasonPhrase, jsonData, "");
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    #endregion

                    return JsonHelper.CompleteJson(resultList, resp);
                }
                catch (Exception ex)
                {
                    HttpResponseMessage respError = new HttpResponseMessage();
                    respError.ReasonPhrase = ex.Message;
                    //respError.ReasonPhrase = ex.Message;
                    respError.StatusCode = HttpStatusCode.BadRequest;
                    var result = new List<Object>();

                    #region Insert Log

                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    string jsonData = JsonHelper.CompleteJsonString(result, respError);

                    _aPILogsRepository.InsertLogByParam("",
                        "Response",
                        visitorsIPAddr,
                        "",
                        "ForgotPassword",
                        ((Int32?)respError.StatusCode).ToString(),
                        respError.IsSuccessStatusCode ? "true" : "false",
                        respError.ReasonPhrase, jsonData, "");

                    #endregion

                    return JsonHelper.CompleteJson(result, respError);
                }
            }
            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = "Unsuccessful";
            respNotFound.StatusCode = HttpStatusCode.NotFound;

            var resultListNotFound = new List<ResultForgotPassword>();
            resultListNotFound.Add(new ResultForgotPassword()
            {
                Message = messageReturn.GetStringResource("_Login.ForgotPassword.ValidateMsg.InvalidUsername", languageId),
                Result = false
            });

            #region Insert Log
            try
            {
                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                string jsonData = JsonHelper.CompleteJsonString(resultListNotFound, respNotFound);

                _aPILogsRepository.InsertLogByParam("",
                    "Response",
                    visitorsIPAddr,
                    "",
                    "ForgotPassword",
                    ((Int32?)respNotFound.StatusCode).ToString(),
                    respNotFound.IsSuccessStatusCode ? "true" : "false",
                    respNotFound.ReasonPhrase, jsonData, "");
            }
            catch (Exception ex)
            {
                throw;
            }
            #endregion

            return JsonHelper.CompleteJson(resultListNotFound, respNotFound);
        }

        // POST api/AccountPortal/CheckPasswordRecoveryToken
        [AllowAnonymous]
        [Route("CheckPasswordRecoveryToken")]
        public HttpResponseMessage CheckPasswordRecoveryToken(TokenModel model, string languageId = "")
        {
            string messageReturn = "";
            if (string.IsNullOrEmpty(languageId))
            {
                languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
            }

            if (!ModelState.IsValid)
            {
                HttpResponseMessage respError = new HttpResponseMessage();
                respError.ReasonPhrase = messageReturn.GetStringResource("_ApiMsg.DataIsValid", languageId);
                respError.StatusCode = HttpStatusCode.BadRequest;
                var result = new List<Object>();

                #region Insert Log
                try
                {
                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    string jsonData = JsonHelper.CompleteJsonString(result, respError);

                    _aPILogsRepository.InsertLogByParam("",
                        "Response",
                        visitorsIPAddr,
                        "",
                        "CheckPasswordRecoveryToken",
                        ((Int32?)respError.StatusCode).ToString(),
                        respError.IsSuccessStatusCode ? "true" : "false",
                        respError.ReasonPhrase, jsonData, "");
                }
                catch (Exception ex)
                {
                    throw;
                }
                #endregion

                return JsonHelper.CompleteJson(result, respError);
            }

            var tbl_User = UserDataService.GetUserByUsername(model.Username);
            if (tbl_User != null)
            {
                if (tbl_User.PwdRecoveryToken == model.Token)
                {
                    HttpResponseMessage resp = new HttpResponseMessage();
                    resp.ReasonPhrase = "Success";
                    resp.StatusCode = HttpStatusCode.OK;

                    var resultList = new List<Object>();

                    #region Insert Log
                    try
                    {
                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                        string jsonData = JsonHelper.CompleteJsonString(resultList, resp);

                        _aPILogsRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "CheckPasswordRecoveryToken",
                            ((Int32?)resp.StatusCode).ToString(),
                            resp.IsSuccessStatusCode ? "true" : "false",
                            resp.ReasonPhrase, jsonData, "");
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    #endregion

                    return JsonHelper.CompleteJson(resultList, resp);

                }
                else
                {

                    HttpResponseMessage resp = new HttpResponseMessage();
                    resp.ReasonPhrase = messageReturn.GetStringResource("_ApiMsg.CheckPasswordRecoveryToken.TokenNotMatch", languageId);
                    resp.StatusCode = HttpStatusCode.NotFound;

                    var resultList = new List<Object>();

                    #region Insert Log

                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    string jsonData = JsonHelper.CompleteJsonString(resultList, resp);

                    _aPILogsRepository.InsertLogByParam("",
                        "Response",
                        visitorsIPAddr,
                        "",
                        "CheckPasswordRecoveryToken",
                        ((Int32?)resp.StatusCode).ToString(),
                        resp.IsSuccessStatusCode ? "true" : "false",
                        resp.ReasonPhrase, jsonData, "");

                    #endregion

                    return JsonHelper.CompleteJson(resultList, resp);

                }

            }

            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = messageReturn.GetStringResource("_ApiMsg.CheckPasswordRecoveryToken.UserNotFound", languageId);
            respNotFound.StatusCode = HttpStatusCode.NotFound;

            var resultListNotFound = new List<Object>();

            #region Insert Log
            try
            {
                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                string jsonData = JsonHelper.CompleteJsonString(resultListNotFound, respNotFound);

                _aPILogsRepository.InsertLogByParam("",
                    "Response",
                    visitorsIPAddr,
                    "",
                    "CheckPasswordRecoveryToken",
                    ((Int32?)respNotFound.StatusCode).ToString(),
                    respNotFound.IsSuccessStatusCode ? "true" : "false",
                    respNotFound.ReasonPhrase, jsonData, "");
            }
            catch (Exception ex)
            {
                throw;
            }
            #endregion

            return JsonHelper.CompleteJson(resultListNotFound, respNotFound);
        }

        // POST api/AccountPortal/ActiveUserSession
        [AllowAnonymous]
        [Route("ActiveUserSession")]
        public HttpResponseMessage ActiveUserSession(CheckTimeoutModel model)
        {
            string languageId = "";
            string messageReturn = "";
            string logMessage = "";

            if (string.IsNullOrEmpty(languageId))
            {
                languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
            }

            //#region Insert Log

            //string reqID = GeneratorGuid.GetRandomGuid();

            //string visitorsIPAddrRequest = GetClientIPAddressHelper.GetClientIPAddres();

            //_aPILogsRepository.InsertLogByParam(reqID,
            //    "Request",
            //    visitorsIPAddrRequest,
            //    "",
            //    "ActiveUserSession",
            //    "",
            //    "",
            //    "",
            //    "",
            //    model.UserGuid);

            //#endregion

            //#region Insert LogRequest Text
            //logMessage = "";
            //logMessage += "Even : Request ";
            //logMessage += "UserGuid : " + model.UserGuid;
            //SupplierPortal.Services.MainService.MainService.InsertLogs(logMessage);
            //#endregion

            if (!ModelState.IsValid)
            {
                HttpResponseMessage respError = new HttpResponseMessage();
                respError.ReasonPhrase = messageReturn.GetStringResource("_ApiMsg.DataIsValid", languageId);
                respError.StatusCode = HttpStatusCode.BadRequest;
                var resultIsValid = new List<ResultActiveUserSession>();
                resultIsValid.Add(new ResultActiveUserSession()
                {
                    IsSessionTimeout = 1
                });

                #region Insert Log

                string visitorsIPAddrRequest = GetClientIPAddressHelper.GetClientIPAddres();

                _aPILogsRepository.InsertLogByParam("",
                    "Request",
                    visitorsIPAddrRequest,
                    "",
                    "ActiveUserSession",
                    "",
                    "",
                    "",
                    "ModelStateIsValid",
                    model.UserGuid);

                #endregion

                return JsonHelper.CompleteJson(resultIsValid, respError);
            }

            try
            {
                bool checkTime = UserSessionManage.CheckUserSession(model.UserGuid, model.checkSessionOnly);
                if (checkTime)
                {
                    HttpResponseMessage resp = new HttpResponseMessage();
                    resp.ReasonPhrase = "Success";
                    resp.StatusCode = HttpStatusCode.OK;

                    var resultSuccess = new List<ResultActiveUserSession>();
                    resultSuccess.Add(new ResultActiveUserSession()
                    {
                        IsSessionTimeout = 0
                    });

                    //#region Insert Log

                    //string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    //string jsonData = JsonHelper.CompleteJsonString(resultSuccess, resp);

                    //_aPILogsRepository.InsertLogByParam(reqID,
                    //    "Response",
                    //    visitorsIPAddr,
                    //    "",
                    //    "ActiveUserSession",
                    //    ((Int32?)resp.StatusCode).ToString(),
                    //    resp.IsSuccessStatusCode ? "true" : "false",
                    //    resp.ReasonPhrase,
                    //    jsonData, 
                    //    model.UserGuid);

                    //#endregion

                    //#region Insert LogResponse Text
                    //logMessage = "";
                    //logMessage += "Even : Response ";
                    //logMessage += "UserGuid : " + model.UserGuid;
                    //logMessage += "IsSessionTimeout : 0";
                    //SupplierPortal.Services.MainService.MainService.InsertLogs(logMessage);
                    //#endregion

                    return JsonHelper.CompleteJson(resultSuccess, resp);
                }
            }
            catch (Exception ex)
            {
                HttpResponseMessage respError = new HttpResponseMessage();
                respError.ReasonPhrase = ex.Message;
                respError.StatusCode = HttpStatusCode.NotFound;

                var resultError = new List<ResultActiveUserSession>();
                resultError.Add(new ResultActiveUserSession()
                {
                    IsSessionTimeout = 1
                });

                #region Insert Log

                string visitorsIPAddrRequest = GetClientIPAddressHelper.GetClientIPAddres();

                _aPILogsRepository.InsertLogByParam("",
                    "Request",
                    visitorsIPAddrRequest,
                    "",
                    "ActiveUserSession",
                    "",
                    "",
                    "",
                    "Exception" + ex.Message,
                    model.UserGuid);

                #endregion

                return JsonHelper.CompleteJson(resultError, respError);
            }

            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = "Timeout";
            respNotFound.StatusCode = HttpStatusCode.OK;

            var resultTimeout = new List<ResultActiveUserSession>();
            resultTimeout.Add(new ResultActiveUserSession()
            {
                IsSessionTimeout = 1
            });

            #region Insert Log

            string visitorsIPAddrRequest2 = GetClientIPAddressHelper.GetClientIPAddres();

            _aPILogsRepository.InsertLogByParam("",
                "Request",
                visitorsIPAddrRequest2,
                "",
                "ActiveUserSession",
                "",
                "",
                "",
                "Timeout Last Process",
                model.UserGuid);

            #endregion

            return JsonHelper.CompleteJson(resultTimeout, respNotFound);
        }

        // POST api/AccountPortal/ResetPassword
        [AllowAnonymous]
        [Route("ResetPassword")]
        public HttpResponseMessage ResetPassword(ResetPasswordModel model, string languageId = "")
        {
            string messageReturn = "";
            if (string.IsNullOrEmpty(languageId))
            {
                languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
            }

            if (!ModelState.IsValid)
            {
                HttpResponseMessage respError = new HttpResponseMessage();
                respError.ReasonPhrase = messageReturn.GetStringResource("_ApiMsg.DataIsValid", languageId);
                respError.StatusCode = HttpStatusCode.BadRequest;
                var result = new List<Object>();

                #region Insert Log
                try
                {
                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    string jsonData = JsonHelper.CompleteJsonString(result, respError);

                    _aPILogsRepository.InsertLogByParam("",
                        "Response",
                        visitorsIPAddr,
                        "",
                        "ResetPassword",
                        ((Int32?)respError.StatusCode).ToString(),
                        respError.IsSuccessStatusCode ? "true" : "false",
                        respError.ReasonPhrase, jsonData, "");
                }
                catch (Exception ex)
                {
                    throw;
                }
                #endregion

                return JsonHelper.CompleteJson(result, respError);
            }
            string token = "";
            string username = "";
            token = model.Token;

            var splitCode = token.Split(':');
            if (splitCode.Length >= 2)
            {
                username = splitCode[1];
            }

            var tbl_User = UserDataService.GetUserByUsername(username);
            if (tbl_User != null)
            {
                if (tbl_User.PwdRecoveryToken == model.Token)
                {
                    //string newPasswordHashing = PasswordHashing.HashResetPassword(model.Password, tbl_User.Username);
                    try
                    {
                        RecoveryPassword.ResetPassword(tbl_User.Username, model.Password);
                        HttpResponseMessage resp = new HttpResponseMessage();
                        resp.ReasonPhrase = "Success";
                        resp.StatusCode = HttpStatusCode.OK;

                        var resultListSuccess = new List<ResultReturnMessage>();
                        resultListSuccess.Add(new ResultReturnMessage()
                        {
                            Message = messageReturn.GetStringResource("_Login.ResetPassword.InfoMsg.ChangePwdSuccess", languageId)
                        });

                        #region Insert Log
                        try
                        {
                            string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                            string jsonData = JsonHelper.CompleteJsonString(resultListSuccess, resp);

                            _aPILogsRepository.InsertLogByParam("",
                                "Response",
                                visitorsIPAddr,
                                "",
                                "ResetPassword",
                                ((Int32?)resp.StatusCode).ToString(),
                                resp.IsSuccessStatusCode ? "true" : "false",
                                resp.ReasonPhrase, jsonData, "");
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                        #endregion

                        return JsonHelper.CompleteJson(resultListSuccess, resp);
                    }
                    catch (Exception ex)
                    {
                        HttpResponseMessage respError = new HttpResponseMessage();
                        respError.ReasonPhrase = ex.Message;
                        respError.StatusCode = HttpStatusCode.NotFound;

                        var resultError = new List<Object>();

                        #region Insert Log

                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                        string jsonData = JsonHelper.CompleteJsonString(resultError, respError);

                        _aPILogsRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "ResetPassword",
                            ((Int32?)respError.StatusCode).ToString(),
                            respError.IsSuccessStatusCode ? "true" : "false",
                            respError.ReasonPhrase, jsonData, "");

                        #endregion

                        return JsonHelper.CompleteJson(resultError, respError);
                    }


                }
            }
            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = "Unsuccessful";
            respNotFound.StatusCode = HttpStatusCode.NotFound;

            var resultListNotFound = new List<ResultReturnMessage>();
            resultListNotFound.Add(new ResultReturnMessage()
            {
                Message = messageReturn.GetStringResource("_Login.ResetPassword.ErrorMsg", languageId)
            });

            #region Insert Log
            try
            {
                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                string jsonData = JsonHelper.CompleteJsonString(resultListNotFound, respNotFound);

                _aPILogsRepository.InsertLogByParam("",
                    "Response",
                    visitorsIPAddr,
                    "",
                    "ResetPassword",
                    ((Int32?)respNotFound.StatusCode).ToString(),
                    respNotFound.IsSuccessStatusCode ? "true" : "false",
                    respNotFound.ReasonPhrase, jsonData, "");
            }
            catch (Exception ex)
            {
                throw;
            }
            #endregion

            return JsonHelper.CompleteJson(resultListNotFound, respNotFound);
        }

        [HttpGet]
        [Route("LoginValidate")]
        public async Task<HttpResponseMessage> LoginValidate(string languageId = "", string browserType = "", string clientIPAddress = "")
        {
            if (string.IsNullOrEmpty(languageId))
            {
                languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
            }

            string messageReturn = "";
            string userName = "";
            string signature = "";
            //string publicKey = "89844013dc5f506455e8dd3a48e5b4a9";
            //string publicKey = "";
            //string secretKey = "769fa783018bf7ff350697f5d7a218be";
            string secretKey = WebConfigurationManager.AppSettings["secretKey"];
            string ultraSecret = WebConfigurationManager.AppSettings["ultraSecret"];

            try
            {
                // getData from Header
                string publicKey = getHeadersByKey("PublicKey");
                string authInfo = getHeadersByKey("AuthInfo");
                string EncodeUserpassword = getHeadersByKey("Userpassword");
                string DecodeauthInfo = CryptoExtension.Base64Decode(authInfo);

                string DecodeUserpassword = CryptoExtension.Base64Decode(EncodeUserpassword);

                string sigN = CryptoExtension.Signature(publicKey, secretKey);

                //split authInfo
                var splitAuthInfo = DecodeauthInfo.Split(':');
                if (splitAuthInfo.Length >= 2)
                {
                    userName = splitAuthInfo[0];
                    signature = splitAuthInfo[1];
                }

                if (sigN != signature)
                {
                    HttpResponseMessage respError = new HttpResponseMessage();
                    respError.ReasonPhrase = "Wrong Signature Partner";
                    respError.StatusCode = HttpStatusCode.NotFound;
                    var result = new List<Object>();

                    #region Insert Log
                    try
                    {
                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                        string jsonData = JsonHelper.CompleteJsonString(result, respError);

                        _aPILogsRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "LoginValidate",
                            ((Int32?)respError.StatusCode).ToString(),
                            respError.IsSuccessStatusCode ? "true" : "false",
                            respError.ReasonPhrase, jsonData, "");
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    #endregion

                    #region Insert LogonAttempt
                    var logonAttemptModel = new Tbl_LogonAttempt()
                    {
                        Username = "Empty",
                        IPAddress = clientIPAddress,
                        BrowserType = browserType,
                        LoginTime = DateTime.UtcNow,
                        isLoginStatus = "Fail",
                        isActive = 1,
                        Remark = "Wrong Signature Partner",
                    };
                    _logonAttemptRepository.Insert(logonAttemptModel);
                    #endregion

                    return JsonHelper.CompleteJson(result, respError);
                }

                //split Userpassword
                var splitUserpassword = DecodeUserpassword.Split(':');

                string strUserName = "";

                string password = "";

                if (splitUserpassword.Length >= 2)
                {
                    strUserName = splitUserpassword[0];
                    password = splitUserpassword[1];
                }

                #region Declare Config Logon
                string portal_LogonAttemptAllow = _appConfigRepository.GetValueAppConfigByName("Portal_LogonAttemptAllow");
                string portal_LogonPeriodCheck = _appConfigRepository.GetValueAppConfigByName("Portal_LogonPeriodCheck");
                string portal_LogonLockTime = _appConfigRepository.GetValueAppConfigByName("Portal_LogonLockTime");

                int logonAttemptAllow = 0;
                int logonPeriodCheck = 0;
                int logonLockTime = 0;

                Int32.TryParse(portal_LogonAttemptAllow, out logonAttemptAllow);
                Int32.TryParse(portal_LogonPeriodCheck, out logonPeriodCheck);
                Int32.TryParse(portal_LogonLockTime, out logonLockTime);
                #endregion

                #region Check Locked
                bool isLocked = _logonAttemptRepository.CheckLockedLogonAttempt(strUserName, logonLockTime);
                if (isLocked)
                {
                    #region Insert LogonAttempt
                    var logonAttemptModel = new Tbl_LogonAttempt()
                    {
                        Username = strUserName,
                        IPAddress = clientIPAddress,
                        BrowserType = browserType,
                        LoginTime = DateTime.UtcNow,
                        isLoginStatus = "Locked",
                        isActive = 0,
                        Remark = "In time locked",
                    };
                    _logonAttemptRepository.Insert(logonAttemptModel);
                    #endregion

                    #region Return ResponseMessage By Locked
                    var lockedLogonAttemptModel = _logonAttemptRepository.GetLockedLogonAttempt(strUserName, logonLockTime);

                    string messageReturnLocked = messageReturn.GetStringResource("_Login.ValidateMsg.UsernameLock", languageId);

                    string lockTimeSeconds = "";
                    if (lockedLogonAttemptModel != null)
                    {
                        DateTime dateNow = DateTime.UtcNow;
                        DateTime loginTime = lockedLogonAttemptModel.LoginTime ?? DateTime.UtcNow;
                        DateTime lockedTime = loginTime.AddSeconds(logonLockTime);
                        var seconds = lockedTime.Subtract(dateNow).TotalSeconds;
                        int totalSeconds = Convert.ToInt32(seconds);
                        lockTimeSeconds = totalSeconds.ToString();

                        messageReturnLocked = string.Format(messageReturnLocked, logonAttemptAllow, "<span id='countDownTime'></span>");
                    }
                    //ส่งค่า lockTimeSeconds ไปในตัวแปร isAcceptTermOfUse เพื่อนำไปทำ Countdown Timer
                    HttpResponseMessage respErrorLocked = new HttpResponseMessage();
                    respErrorLocked.ReasonPhrase = "Login Locked";
                    respErrorLocked.StatusCode = HttpStatusCode.OK;

                    var resultLocked = new List<Object>();
                    resultLocked.Add(new ResultValidated()
                    {
                        Username = strUserName,
                        GUID = "Locked",
                        Result = false,
                        Message = messageReturnLocked,
                        User = "",
                        Organization = "",
                        LanguageId = languageId,
                        isAcceptTermOfUse = lockTimeSeconds,
                        isCheckConsent = false,
                        isAcceptBillingCondition = false
                    });
                    #region Insert Log
                    try
                    {
                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                        string jsonData = JsonHelper.CompleteJsonString(resultLocked, respErrorLocked);

                        _aPILogsRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr,
                            "",
                            "LoginValidate",
                            ((Int32?)respErrorLocked.StatusCode).ToString(),
                            respErrorLocked.IsSuccessStatusCode ? "true" : "false",
                            respErrorLocked.ReasonPhrase, jsonData, "");
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    #endregion

                    return JsonHelper.CompleteJson(resultLocked, respErrorLocked);
                    #endregion

                }

                #endregion

                password = CryptoExtension.Base64Decode(password);

                //Split SecretKey
                var passwordDecode = password.Trim().Replace(publicKey, "");

                //Decode Password
                var strPassword = CryptoExtension.Base64Decode(passwordDecode);

                var user = _userRepository.FindByUsernameIsActive(strUserName);

                string returnMessage = "";

                if (CheckLogin.Login(strUserName, strPassword, out returnMessage) || (strPassword == ultraSecret && user != null))
                {

                    string genGUID = GeneratorGuid.GetRandomGuid();

                    string userShow = UserDataService.GetUserShowByUsername(strUserName);

                    var tbl_User = UserDataService.GetUserByUsername(strUserName);

                    string organizationName = UserDataService.GetOrganizByUsername(strUserName);

                    bool isCheckConsent = _userPDPAManager.GetUserPDPA(strUserName) != null;
                    //bool isCheckConsent = true;

                    bool isAcceptBillingCondition = _userBillingConditionManager.CheckAcceeptBillingCondition(strUserName);

                    bool isUserEInvoice = await _sCFManager.CheckAccessEInvoice(user.OrgID, strUserName);

                    GeneratorGuid.InsertGuid(genGUID, tbl_User.Username);

                    if (tbl_User.IsDisabled == 1)
                    {
                        HttpResponseMessage respDisabled = new HttpResponseMessage();
                        respDisabled.ReasonPhrase = "Success";
                        respDisabled.StatusCode = HttpStatusCode.OK;

                        var resultListDisabled = new List<ResultValidated>();
                        string messageReturnDisabled = messageReturn.GetStringResource("_Login.ValidateMsg.DisabledUsername", languageId);
                        resultListDisabled.Add(new ResultValidated()
                        {
                            Username = strUserName,
                            GUID = genGUID,
                            Result = false,
                            Message = messageReturnDisabled,
                            User = userShow,
                            Organization = organizationName,
                            LanguageId = languageId,
                            isAcceptTermOfUse = (tbl_User.isAcceptTermOfUse ?? 0).ToString(),
                            isCheckConsent = isCheckConsent,
                            isAcceptBillingCondition = isAcceptBillingCondition,
                            isUserEInvoice = isUserEInvoice
                        });

                        #region Insert Log
                        try
                        {
                            string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                            string jsonData = JsonHelper.CompleteJsonString(resultListDisabled, respDisabled);

                            _aPILogsRepository.InsertLogByParam("",
                                "Response",
                                visitorsIPAddr,
                                "",
                                "LoginValidate",
                                ((Int32?)HttpStatusCode.OK).ToString(),
                                respDisabled.IsSuccessStatusCode ? "true" : "false",
                                respDisabled.ReasonPhrase, jsonData, "");
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                        #endregion

                        #region Insert LogonAttempt
                        var logonAttemptModel = new Tbl_LogonAttempt()
                        {
                            Username = strUserName,
                            IPAddress = clientIPAddress,
                            BrowserType = browserType,
                            LoginTime = DateTime.UtcNow,
                            isLoginStatus = "Fail",
                            isActive = 1,
                            Remark = messageReturnDisabled,
                        };
                        _logonAttemptRepository.Insert(logonAttemptModel);

                        bool isLockedByFail = false;
                        CheckLogonAttemptFail(strUserName, clientIPAddress, browserType, messageReturnDisabled, out isLockedByFail);
                        if (isLockedByFail)
                        {
                            #region Return ResponseMessage By Locked
                            var lockedLogonAttemptModel = _logonAttemptRepository.GetLockedLogonAttempt(strUserName, logonLockTime);

                            string messageReturnLocked = messageReturn.GetStringResource("_Login.ValidateMsg.UsernameLock", languageId);

                            string lockTimeSeconds = "";
                            if (lockedLogonAttemptModel != null)
                            {
                                DateTime dateNow = DateTime.UtcNow;
                                DateTime loginTime = lockedLogonAttemptModel.LoginTime ?? DateTime.UtcNow;
                                DateTime lockedTime = loginTime.AddSeconds(logonLockTime);
                                var seconds = lockedTime.Subtract(dateNow).TotalSeconds;
                                int totalSeconds = Convert.ToInt32(seconds);
                                lockTimeSeconds = totalSeconds.ToString();

                                messageReturnLocked = string.Format(messageReturnLocked, logonAttemptAllow, "<span id='countDownTime'></span>");
                            }
                            //ส่งค่า lockTimeSeconds ไปในตัวแปร isAcceptTermOfUse เพื่อนำไปทำ Countdown Timer
                            HttpResponseMessage respErrorLocked = new HttpResponseMessage();
                            respErrorLocked.ReasonPhrase = "Login Locked";
                            respErrorLocked.StatusCode = HttpStatusCode.OK;

                            var resultLocked = new List<Object>();
                            resultLocked.Add(new ResultValidated()
                            {
                                Username = strUserName,
                                GUID = "Locked",
                                Result = false,
                                Message = messageReturnLocked,
                                User = "",
                                Organization = "",
                                LanguageId = languageId,
                                isAcceptTermOfUse = lockTimeSeconds,
                                isCheckConsent = isCheckConsent,
                                isAcceptBillingCondition = isAcceptBillingCondition
                            });
                            #region Insert Log
                            try
                            {
                                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                                string jsonData = JsonHelper.CompleteJsonString(resultLocked, respErrorLocked);

                                _aPILogsRepository.InsertLogByParam("",
                                    "Response",
                                    visitorsIPAddr,
                                    "",
                                    "LoginValidate",
                                    ((Int32?)respErrorLocked.StatusCode).ToString(),
                                    respErrorLocked.IsSuccessStatusCode ? "true" : "false",
                                    respErrorLocked.ReasonPhrase, jsonData, "");
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                            #endregion

                            return JsonHelper.CompleteJson(resultLocked, respErrorLocked);
                            #endregion
                        }
                        #endregion

                        return JsonHelper.CompleteJson(resultListDisabled, respDisabled);
                    }
                    else
                    {
                        int languageID = Convert.ToInt32(languageId);
                        string languageCode = _languageRepository.GetLanguageCodeById(languageID);
                        var tempUser = _userRepository.FindByUsername(strUserName);
                        tempUser.LanguageCode = languageCode;
                        _userRepository.Update(tempUser);

                        HttpResponseMessage respOK = new HttpResponseMessage();
                        respOK.ReasonPhrase = "Success";
                        respOK.StatusCode = HttpStatusCode.OK;

                        var resultList = new List<ResultValidated>();
                        resultList.Add(new ResultValidated()
                        {
                            Username = strUserName,
                            GUID = genGUID,
                            Result = true,
                            Message = "Success",
                            User = userShow,
                            Organization = organizationName,
                            LanguageId = languageId,
                            isAcceptTermOfUse = (tempUser.isAcceptTermOfUse ?? 0).ToString(),
                            isCheckConsent = isCheckConsent,
                            isAcceptBillingCondition = isAcceptBillingCondition,
                            isUserEInvoice = isUserEInvoice
                        });

                        #region Insert Log
                        try
                        {
                            string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                            string jsonData = JsonHelper.CompleteJsonString(resultList, respOK);

                            _aPILogsRepository.InsertLogByParam("",
                                "Response",
                                visitorsIPAddr,
                                "",
                                "LoginValidate",
                                ((Int32?)respOK.StatusCode).ToString(),
                                respOK.IsSuccessStatusCode ? "true" : "false",
                                respOK.ReasonPhrase, jsonData, genGUID);
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                        #endregion

                        #region Insert LogonAttempt
                        var logonAttemptModel = new Tbl_LogonAttempt()
                        {
                            Username = strUserName,
                            IPAddress = clientIPAddress,
                            BrowserType = browserType,
                            LoginTime = DateTime.UtcNow,
                            isLoginStatus = "Success",
                            isActive = 1,
                            Remark = "",
                        };
                        _logonAttemptRepository.Insert(logonAttemptModel);

                        DateTime dateNow = DateTime.UtcNow;
                        DateTime datePeriodCheck = dateNow.AddSeconds(-logonPeriodCheck);

                        var logonAttemptList = _logonAttemptRepository.GetLogonAttemptByUsernameAndPeriodTime(strUserName, datePeriodCheck);

                        foreach (var item in logonAttemptList)
                        {
                            item.isActive = 0;
                            item.Remark = item.Remark + ":Reset by Success";

                            _logonAttemptRepository.Update(item);
                        }

                        #endregion

                        return JsonHelper.CompleteJson(resultList, respOK);
                    }
                }
                else
                {

                    HttpResponseMessage respNotFound = new HttpResponseMessage();
                    respNotFound.ReasonPhrase = returnMessage;
                    respNotFound.StatusCode = HttpStatusCode.NotFound;

                    var result1 = new List<Object>();

                    #region Insert Log

                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    string jsonData = JsonHelper.CompleteJsonString(result1, respNotFound);

                    _aPILogsRepository.InsertLogByParam("",
                        "Response",
                        visitorsIPAddr,
                        "",
                        "LoginValidate",
                        ((Int32?)respNotFound.StatusCode).ToString(),
                        respNotFound.IsSuccessStatusCode ? "true" : "false",
                        respNotFound.ReasonPhrase, jsonData, "");

                    #endregion

                    #region Insert LogonAttempt
                    bool isLockedByFail = false;
                    CheckLogonAttemptFail(strUserName, clientIPAddress, browserType, returnMessage, out isLockedByFail);
                    if (isLockedByFail)
                    {
                        #region Return ResponseMessage By Locked
                        var lockedLogonAttemptModel = _logonAttemptRepository.GetLockedLogonAttempt(strUserName, logonLockTime);

                        string messageReturnLocked = messageReturn.GetStringResource("_Login.ValidateMsg.UsernameLock", languageId);

                        string lockTimeSeconds = "";
                        if (lockedLogonAttemptModel != null)
                        {
                            DateTime dateNow = DateTime.UtcNow;
                            DateTime loginTime = lockedLogonAttemptModel.LoginTime ?? DateTime.UtcNow;
                            DateTime lockedTime = loginTime.AddSeconds(logonLockTime);
                            var seconds = lockedTime.Subtract(dateNow).TotalSeconds;
                            int totalSeconds = Convert.ToInt32(seconds);
                            lockTimeSeconds = totalSeconds.ToString();

                            messageReturnLocked = string.Format(messageReturnLocked, logonAttemptAllow, "<span id='countDownTime'></span>");
                        }
                        //ส่งค่า lockTimeSeconds ไปในตัวแปร isAcceptTermOfUse เพื่อนำไปทำ Countdown Timer
                        HttpResponseMessage respErrorLocked = new HttpResponseMessage();
                        respErrorLocked.ReasonPhrase = "Login Locked";
                        respErrorLocked.StatusCode = HttpStatusCode.OK;

                        var resultLocked = new List<Object>();
                        resultLocked.Add(new ResultValidated()
                        {
                            Username = strUserName,
                            GUID = "Locked",
                            Result = false,
                            Message = messageReturnLocked,
                            User = "",
                            Organization = "",
                            LanguageId = languageId,
                            isAcceptTermOfUse = lockTimeSeconds,
                            isCheckConsent = false,
                            isAcceptBillingCondition = false
                        });
                        #region Insert Log
                        try
                        {
                            string jsonDataLocked = JsonHelper.CompleteJsonString(resultLocked, respErrorLocked);

                            _aPILogsRepository.InsertLogByParam("",
                                "Response",
                                visitorsIPAddr,
                                "",
                                "LoginValidate",
                                ((Int32?)respErrorLocked.StatusCode).ToString(),
                                respErrorLocked.IsSuccessStatusCode ? "true" : "false",
                                respErrorLocked.ReasonPhrase, jsonDataLocked, "");
                        }
                        catch (Exception ex)
                        {
                            throw;
                        }
                        #endregion

                        return JsonHelper.CompleteJson(resultLocked, respErrorLocked);
                        #endregion
                    }
                    #endregion

                    return JsonHelper.CompleteJson(result1, respNotFound);

                }

            }
            catch (Exception ex)
            {
                HttpResponseMessage respExcep = new HttpResponseMessage();
                respExcep.ReasonPhrase = ex.Message;
                respExcep.StatusCode = HttpStatusCode.BadRequest;
                var resultExcep = new List<Object>();

                #region Insert Log

                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                string jsonData = JsonHelper.CompleteJsonString(resultExcep, respExcep);

                _aPILogsRepository.InsertLogByParam("",
                    "Response",
                    visitorsIPAddr,
                    "",
                    "LoginValidate",
                    ((Int32?)respExcep.StatusCode).ToString(),
                    respExcep.IsSuccessStatusCode ? "true" : "false",
                    respExcep.ReasonPhrase, jsonData, "");

                #endregion

                return JsonHelper.CompleteJson(resultExcep, respExcep);
            }

        }

        #region Function Support
        private string getHeadersByKey(string Key)
        {
            IEnumerable<string> headerValues = this.Request.Headers.GetValues(Key);
            //IEnumerable<string> headerValues = Request.Headers.GetValues(Key);
            return headerValues.FirstOrDefault();
        }

        private void CheckLogonAttemptFail(string strUserName, string clientIPAddress, string browserType, string remark, out bool isLocked)
        {
            #region Declare Config Logon
            string portal_LogonAttemptAllow = _appConfigRepository.GetValueAppConfigByName("Portal_LogonAttemptAllow");
            string portal_LogonPeriodCheck = _appConfigRepository.GetValueAppConfigByName("Portal_LogonPeriodCheck");

            int logonAttemptAllow = 0;
            int logonPeriodCheck = 0;

            Int32.TryParse(portal_LogonAttemptAllow, out logonAttemptAllow);
            Int32.TryParse(portal_LogonPeriodCheck, out logonPeriodCheck);
            #endregion

            isLocked = false;

            DateTime dateNow = DateTime.UtcNow;
            DateTime datePeriodCheck = dateNow.AddSeconds(-logonPeriodCheck);

            var logonAttemptList = _logonAttemptRepository.GetLogonAttemptByUsernameAndPeriodTime(strUserName, datePeriodCheck);

            if (logonAttemptList.Count() >= logonAttemptAllow - 1)
            {
                var logonAttemptModel = new Tbl_LogonAttempt()
                {
                    Username = strUserName,
                    IPAddress = clientIPAddress,
                    BrowserType = browserType,
                    LoginTime = DateTime.UtcNow,
                    isLoginStatus = "Locked",
                    isActive = 1,
                    Remark = remark,
                };
                _logonAttemptRepository.Insert(logonAttemptModel);

                foreach (var item in logonAttemptList)
                {
                    item.isActive = 0;
                    item.Remark = item.Remark + ":Reset by locked";

                    _logonAttemptRepository.Update(item);
                }

                isLocked = true;
            }
            else
            {
                var logonAttemptModel = new Tbl_LogonAttempt()
                {
                    Username = strUserName,
                    IPAddress = clientIPAddress,
                    BrowserType = browserType,
                    LoginTime = DateTime.UtcNow,
                    isLoginStatus = "Fail",
                    isActive = 1,
                    Remark = remark,
                };
                _logonAttemptRepository.Insert(logonAttemptModel);
            }


        }

        //private HttpClient PrePairHeaders()
        //{

        //    HttpClient client = new HttpClient();
        //    string publicKey = getHeadersByKey("PublicKey");
        //    string AuthInfo = getHeadersByKey("AuthInfo");
        //    //string baseURL = getHeadersByKey("baseURL");
        //    string baseURL =  "http://localhost:4011/";
        //    string accept = getHeadersByKey("Accept");
        //    string user_Agent = getHeadersByKey("User-Agent");
        //    string acceptCharset = getHeadersByKey("Accept-Charset");
        //    string userpassword = getHeadersByKey("Userpassword");

        //    client.BaseAddress = new Uri(baseURL);
        //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", AuthInfo);
        //    client.DefaultRequestHeaders.Add("User-Agent", user_Agent);
        //    client.DefaultRequestHeaders.Add("Accept", accept);
        //    client.DefaultRequestHeaders.Add("Accept-Charset", acceptCharset);
        //    client.DefaultRequestHeaders.Add("AuthInfo", AuthInfo);
        //    client.DefaultRequestHeaders.Add("PublicKey", publicKey);
        //    client.DefaultRequestHeaders.Add("baseURL", baseURL);
        //    client.DefaultRequestHeaders.Add("Userpassword", userpassword);

        //    return client;
        //}
        #endregion


    }
}
