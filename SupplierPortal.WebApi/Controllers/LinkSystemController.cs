﻿using SupplierPortal.Framework.Localization;
using SupplierPortal.WebApi.Models.LinkSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SupplierPortal.Services.LinkSystemManage;
using SupplierPortal.Data.Models.Repository.APILogs;
using SupplierPortal.Framework.MethodHelper;
using Newtonsoft.Json;
using SupplierPortal.MainFunction.Helper;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/LinkSystem")]
    public class LinkSystemController : ApiController
    {
        IAPILogsRepository _aPILogsRepository;
        ILinkSystemService _linkSystemService;

        public LinkSystemController()
        {
            _aPILogsRepository = new APILogsRepository();
            _linkSystemService = new LinkSystemService();
         }

        public LinkSystemController(
            APILogsRepository aPILogsRepository,
            LinkSystemService linkSystemService
              )
          {

              _aPILogsRepository = aPILogsRepository;
              _linkSystemService = linkSystemService;
          }
        // POST api/AccountPortal/VerifyUserSession
        [AllowAnonymous]
        [Route("VerifyUserSession")]
        public HttpResponseMessage VerifyUserSession(VerifyUserSessionModel model)
        {
            string languageId = "";

            string messageReturn = "";

            if (string.IsNullOrEmpty(languageId))
            {
                languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe
            }

            if (!ModelState.IsValid)
            {
                HttpResponseMessage respError = new HttpResponseMessage();
                respError.ReasonPhrase = messageReturn.GetStringResource("_ApiMsg.DataIsValid", languageId);
                respError.StatusCode = HttpStatusCode.BadRequest;
                var result = new List<Object>();

                #region Insert Log
                try
                {
                    string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                    string jsonData = JsonHelper.CompleteJsonString(result, respError);
                    
                    _aPILogsRepository.InsertLogByParam("",
                        "Response",
                        visitorsIPAddr,
                        "",
                        "VerifyUserSession",
                        ((Int32?)respError.StatusCode).ToString(),
                        respError.IsSuccessStatusCode ? "true" : "false", 
                        respError.ReasonPhrase, jsonData, "");
                }
                catch (Exception ex)
                {
                    throw;
                }
                #endregion

                return JsonHelper.CompleteJson(result, respError);
            }



            try
            {

                if (_linkSystemService.CheckUserGuid(model.UserGUID))
                {
                    var result = _linkSystemService.GetVerifyUserSession(model.UserGUID, model.SystemID, model.SystemGrpID, model.RequireAllUserMapping, model.PageID, model.AppType);

                    HttpResponseMessage resp = new HttpResponseMessage();
                    resp.ReasonPhrase = "Success";
                    resp.StatusCode = HttpStatusCode.OK;

                    #region Insert Log
                    try
                    {
                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                        string jsonData = JsonHelper.CompleteJsonString(result, resp);

                        _aPILogsRepository.InsertLogByParam("",
                            "Response",
                            visitorsIPAddr, 
                            "",
                            "VerifyUserSession",
                            ((Int32?)resp.StatusCode).ToString(),
                            resp.IsSuccessStatusCode ? "true" : "false", 
                            resp.ReasonPhrase, jsonData, model.UserGUID);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    #endregion

                    return JsonHelper.CompleteJson(result, resp);

                }
            
            }         
            catch (Exception ex)
                {

                    HttpResponseMessage respError = new HttpResponseMessage();
                    respError.ReasonPhrase = ex.Message;
                    respError.StatusCode = HttpStatusCode.BadRequest;
                    var result = new List<Object>();
                    //string jsonModelData = JsonConvert.SerializeObject(model);
                    #region Insert Log
                  
                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                        string jsonData = JsonHelper.CompleteJsonString(result, respError);

                        _aPILogsRepository.InsertLogByParam("",
                            "Response_Exception",
                            visitorsIPAddr,
                            "",
                            "VerifyUserSession",
                            ((Int32?)respError.StatusCode).ToString(),
                            respError.IsSuccessStatusCode ? "true" : "false",
                            respError.ReasonPhrase, jsonData, model.UserGUID);
                   
                    #endregion

                    return JsonHelper.CompleteJson(result, respError);

                }


            HttpResponseMessage respNotFound = new HttpResponseMessage();
            respNotFound.ReasonPhrase = "Unsuccessful";
            respNotFound.StatusCode = HttpStatusCode.NotFound;

            var resultListNotFound = new List<Object>();

            #region Insert Log
            try
            {
                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                string jsonData = JsonHelper.CompleteJsonString(resultListNotFound, respNotFound);

                _aPILogsRepository.InsertLogByParam("",
                    "Response",
                    visitorsIPAddr,
                    "",
                    "VerifyUserSession",
                    ((Int32?)respNotFound.StatusCode).ToString(),
                    respNotFound.IsSuccessStatusCode ? "true" : "false", 
                    respNotFound.ReasonPhrase, jsonData, model.UserGUID);
            }
            catch (Exception ex)
            {
                throw;
            }
            #endregion

            return JsonHelper.CompleteJson(resultListNotFound, respNotFound);

        }

    }
}
