﻿using Newtonsoft.Json;
using SupplierPortal.BusinessLogic.ExternalService;
using SupplierPortal.Data.Models.SupportModel.UAA;
using SupplierPortal.DataAccess.User;
using SupplierPortal.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/uaa")]
    public class UAAController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        UAAServiceManager _uaaServiceManager;

        public UAAController()
        {
            _uaaServiceManager = new UAAServiceManager();
        }

        [Route("token")]
        [HttpPost]
        public async Task<HttpResponseMessage> AuthToken([FromBody] UAAOAuthRequestModel request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            BaseResponseDto result = new BaseResponseDto() { isSuccess = false };

            try
            {
                UAAOAuthModel uaaAccessToken = await _uaaServiceManager.GetAccessToken(request);
                if(uaaAccessToken != null)
                {
                    result.isSuccess = true;
                    result.data = uaaAccessToken;
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    result.errorMessage = "Can not get access token.";
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
            }
            catch(Exception ex)
            {
                logger.Error("UAAController (API) : " + ex.Message);
            }

            response.Content = new StringContent(JsonConvert.SerializeObject(result), System.Text.Encoding.UTF8, "application/json");
            return response;
        }
    }
}