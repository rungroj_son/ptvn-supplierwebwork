﻿using SupplierPortal.BusinessLogic.RegMigrateAttachmentToSC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/migrate")]
    public class MigrateAttachmentController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IRegMigrateAttachmentToSCManager _regMigrateAttachmentToSC;

        public MigrateAttachmentController()
        {
            _regMigrateAttachmentToSC = new RegMigrateAttachmentToSCManager();
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("to/minio")]
        public async Task<HttpResponseMessage> MigrateAttachmentToSC()
        {
            HttpResponseMessage response = null;
            try
            {

                var result = await _regMigrateAttachmentToSC.MigrateAttachmentToSC();

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(result);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("MigrateAttachmentToSC (API) : " + ex.Message);
            }
            return response;
        }
    }
}