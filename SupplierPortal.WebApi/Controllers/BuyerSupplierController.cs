﻿using SupplierPortal.BusinessLogic.Buyer;
using SupplierPortal.BusinessLogic.RolePrivilege;
using SupplierPortal.Data.CustomModels.Buyer;
using SupplierPortal.Data.CustomModels.BuyerSuppliers;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/buyer")]
    public class BuyerSupplierController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IBuyerManager _manager;

        public BuyerSupplierController()
        {
            _manager = new BuyerManager();
        }

        #region Supplier & UserSupplier

        #region INSERT

        [Route("supplier/user")]
        [HttpPost]
        public HttpResponseMessage InsertSupplierUser([FromBody] SupplierUserRequest request)
        {
            HttpResponseMessage response = null;
            try
            {
                SupplierUserModel objRequest = request.SupplierUser;
                IBuyerManager manager = new BuyerManager();
                int id = manager.insertSupplierUser(objRequest);

                CreateResponse result = new CreateResponse
                {
                    Id = id,
                    Message = "success",
                    Success = true
                };


                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(result);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("InsertBuyerUser (API) : " + ex.Message);
            }
            return response;
        }
        #endregion

        #region UPDATE
        [Route("UpdateBuyer")]
        [HttpPost]
        public HttpResponseMessage UpdateSupplierContactPerson([FromBody]BuyerSuppliersModel buyerSupplier)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _manager.UpdateSupplierContactPerson(buyerSupplier);
                response = new HttpResponseMessage();
                if (results)
                {
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.NoContent;
                }

                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("UpdateBuyerContactPerson (API) : " + ex);
            }
            return response;
        }
        #endregion

        #region GET
        // GET: Supplier
        [Route("suppliers")]
        [HttpPost]
        public HttpResponseMessage Suppliers([FromBody] GetBuyerSuppliersRequest request)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _manager.GetSuppliers(request);
                int filteredCount = results.Item2;
                int total = results.Item3;

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(new GetBuyerSuppliersResponse() { data = results.Item1, draw = request.draw, recordsFiltered = filteredCount, Message = "success", recordsTotal = total, Success = true });
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("SuppliersList (API) : " + ex.InnerException);
            }

            return response;
        }

        [Route("Detail")]
        [HttpPost]
        public HttpResponseMessage GetBuyerSupplierContactDetail([FromBody] int userId)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _manager.GetBuyerSupplierContactDetail(userId);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetBuyerSupplierContactDetail (API) : " + ex.Message);
            }
            return response;
        }

        [Route("ListHistory")]
        [HttpPost]
        public HttpResponseMessage GetListHistorySupplierContactPersonByUserId([FromBody] int userId)
        {
            HttpResponseMessage response = null;
            try
            {
                var results = _manager.GetListHistorySupplierContactPersonByUserId(userId);
                response = new HttpResponseMessage();
                if (results != null)
                {
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.NoContent;
                }

                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("GetListHistoryBuyerContactPersonByUserId (API) : " + ex.Message);
            }
            return response;
        }
        #endregion
        #endregion

        #region Organizaation

        #region GET
        // GET: Supplier
        [Route("organization")]
        [HttpGet]
        public HttpResponseMessage organization([FromUri] int id)
        {
            HttpResponseMessage response = null;
            try
            {
                var result = _manager.GetOrganization(id);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(new GetOrganizationResponse() { data = result, Message = "", Success = true });
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("SuppliersList (API) : " + ex.Message);
            }
            return response;
        }
        #endregion

        #endregion

        #region Buyer & BuyerUser
        #region INSERT
        [Route("user")]
        [HttpPost]
        public HttpResponseMessage InsertBuyerUser([FromBody] BuyerUserRequest request)
        {
            HttpResponseMessage response = null;
            try
            {
                BuyerUserModel objRequest = request.BuyerUser;
                IBuyerManager manager = new BuyerManager();
                int id = manager.InsertBuyerUser(objRequest);

                CreateResponse result = new CreateResponse
                {
                    Id = id,
                    Message = "success",
                    Success = true
                };


                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(result);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("InsertBuyerUser (API) : " + ex.Message);
            }
            return response;
        }
        #endregion
        #endregion

        #region Privilege

        #region GET

        [Route("role/privilege")]
        [HttpGet]
        public HttpResponseMessage GetBuyerRolePrivilege([FromUri] int sysuserid)
        {
            IRolePrivilegeManager manager = new RolePrivilegeManager();
            List<Tbl_BuyerRolePrivilege> results = manager.GetBuyerRolePrivileges(sysuserid);

            HttpResponseMessage response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            string jsonResult = new JavaScriptSerializer().Serialize(results);
            response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");

            return response;
        }

        [Route("role/epprivilege")]
        [HttpGet]
        public HttpResponseMessage GetBuyerRolePrivilgeByEPUserId([FromUri]string sessionUserId, int epUserId, int eid)
        {
            HttpResponseMessage response = null;
            try
            {
                IRolePrivilegeManager manager = new RolePrivilegeManager();
                List<Tbl_BuyerRolePrivilege> results = manager.GetBuyerRolePrivilgeByEPUserId(sessionUserId, epUserId, eid);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");

                return response;
            }
            catch (Exception ex)
            {
                logger.Error("GetBuyerRolePrivilgeByEPUserId (API) : " + ex);
            }
            return response;
        }

        [Route("Epconfig")]
        [HttpGet]
        public HttpResponseMessage GetBuyerConfigByEID([FromUri] int eid)
        {
            HttpResponseMessage response = null;
            Tbl_BuyerConfig results = null;
            try
            {
                results = new Tbl_BuyerConfig();
                results = _manager.GetBuyerConfigByEID(eid);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");

            }
            catch (Exception ex)
            {
                logger.Error("GetBuyerUserWithConfigBySysUserID (API) : " + ex.Message);
            }
            return response;
        }
        #endregion

        #endregion

        #region Reset Supplier User Passwrod
        [Route("email/invite/resetpassword")]
        [HttpGet]
        public HttpResponseMessage InviteResetPassword()
        {
            HttpResponseMessage response = null;
            try
            {
                string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                IBuyerManager manager = new BuyerManager();
                bool result = manager.resetPasswordOfSupplierUser(visitorsIPAddr);

                string statusMsg = "send email is fail";
                if (result)
                {
                    statusMsg = "send email is success";
                }

                SendEmailResponse emailResponse = new SendEmailResponse
                {
                    Message = statusMsg,
                    Success = result
                };


                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                string jsonResult = new JavaScriptSerializer().Serialize(emailResponse);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("InviteResetPassword (API) : " + ex.InnerException);

                SendEmailResponse emailResponse = new SendEmailResponse
                {
                    Message = ex.Message,
                    Success = false
                };


                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.InternalServerError;
                string jsonResult = new JavaScriptSerializer().Serialize(emailResponse);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");

            }
            return response;
        }
        #endregion











    }
}