﻿using SupplierPortal.BusinessLogic.SCF;
using SupplierPortal.Data.CustomModels.SCF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace SupplierPortal.WebApi.Controllers
{
    [RoutePrefix("api/scf")]
    public class SCFController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly ISCFManager _manager;

        public SCFController()
        {
            _manager = new SCFManager();
        }

        #region Function
        [Route("save")]
        [HttpPost]
        public async Task<HttpResponseMessage> SaveSCFRequest(SCFRequest request)
        {
            HttpResponseMessage response = null;

            if (!ModelState.IsValid)
            {
                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.BadRequest;
                string validResult = new JavaScriptSerializer().Serialize(ModelState);

                SCFResponse sCFResponse = new SCFResponse();
                sCFResponse.isError = true;
                sCFResponse.msgError = validResult;

                response.Content = new StringContent(new JavaScriptSerializer().Serialize(sCFResponse), System.Text.Encoding.UTF8, "application/json");
                return response;
            }

            try
            {

                SCFResponse results = await _manager.SaveSCFRequest(request);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;

                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("SaveSCFRequest (API) : " + ex.Message);
            }

            return response;
        }

        [Route("validate")]
        [HttpPost]
        public async Task<HttpResponseMessage> CheckSCFRequestStatusInProgress(SCFRequest request)
        {
            HttpResponseMessage response = null;
            try
            {
                SCFResponse results = await _manager.CheckSCFRequestStatusInProgress(request);

                response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;

                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("SaveSCFRequest (API) : " + ex.Message);
            }

            return response;
        }

        [Route("vault/encrypt")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetVaultEncrypt(VaultEncryptRequest request)
        {
            HttpResponseMessage response = null;
            try
            {
                VaultEncryptResponse results = await _manager.GetVaultEncrypt(request);

                response = new HttpResponseMessage();

                if (results != null)
                {
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                }

                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("JobRunningUpdateBuyerSupplierMapping (API) : " + ex.Message);
            }

            return response;
        }

        [Route("vault/decrypt")]
        [HttpPost]
        public async Task<HttpResponseMessage> GetVaultDecrypt(VaultDecryptRequest request)
        {
            HttpResponseMessage response = null;
            try
            {
                VaultDecryptResponse results = await _manager.GetVaultDecrypt(request);

                response = new HttpResponseMessage();

                if (results != null)
                {
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                }

                string jsonResult = new JavaScriptSerializer().Serialize(results);
                response.Content = new StringContent(jsonResult, System.Text.Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                logger.Error("JobRunningUpdateBuyerSupplierMapping (API) : " + ex.Message);
            }

            return response;
        }
        #endregion Function
    }
}