﻿using System;
using System.Web.Script.Serialization;
namespace SupplierPortal.WebApi.Models.CompanyProfile
{

    public partial class TrackingApprovalModel
    {
        public int itemId { get; set; }
        [ScriptIgnore]
        public int addressTypeId { get; set; }
        [ScriptIgnore]
        public string addressTypeName { get; set; }
        public Nullable<int> trackingStatusId { get; set; }
        public string trackingStatusName { get; set; }
        public DateTime trackingStatudateDate { get; set; }
        public string remark { get; set; }

    }
}
