﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace SupplierPortal.WebApi.Models.AccountPortal
{
    public class TokenModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Token { get; set; }

    }

    public class ResultTokenModel
    {
        public string Username { get; set; }

    }
}