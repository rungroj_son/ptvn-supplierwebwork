﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SupplierPortal.WebApi.Models.AccountPortal
{
    public class LoginPortalModel
    {
        [Required]
        [Display(Name = "Username")]
        public string Username { get; set; }

        // Test for get DisplayName by Locale

        //[Required]
        //[ResourceDisplayName(Name = "Username")]
        //public string Username { get; set; }


        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }


    }
}