﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SupplierPortal.WebApi.Models.AccountPortal
{
    public class ResultForgotPassword
    {
        //public string Token { get; set; }
        public string Message { get; set; }
        public bool Result { get; set; }
        public string Email { get; set; }
    }
}