﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SupplierPortal.WebApi.Models.AccountPortal
{
    public class ForgotPasswordModel
    {
        [Required(ErrorMessage = "Please fill in all required field(*)<br>- Login")]
        [Display(Name = "Username")]
        public string Username { get; set; }
        //public int isRequireSendingMail { get; set; }
    }
}