﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupplierPortal.WebApi.Models.AccountPortal
{
    public class ResultActiveUserSession
    {
        public int IsSessionTimeout { get; set; }
    }
}