﻿using System;
using System.Collections.Generic;


namespace SupplierPortal.WebApi.Models.AccountPortal
{
    public class ResultReturnMessage
    {

        public string Message { get; set; }
    }
}