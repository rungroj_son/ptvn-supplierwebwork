﻿using System;
using System.Collections.Generic;


namespace SupplierPortal.WebApi.Models.LinkSystem
{
    public class ResultVerifyUserSessionModel
    {
        public string UserGUID { get; set; }
        public string LanguageCode { get; set; }
        public Systemid[] SystemID { get; set; }
        public Menu[] Menu { get; set; }

    }
}




//public class Rootobject
//{
//    public Result result { get; set; }
//    public Datum[] data { get; set; }
//}

//public class Result
//{
//    public string Code { get; set; }
//    public bool SuccessStatus { get; set; }
//    public string Message { get; set; }
//}

//public class Datum
//{
//    public string Guid { get; set; }
//    public string LanguageCode { get; set; }
//    public Systemid[] SystemID { get; set; }
//    public Menu[] Menu { get; set; }
//}

public class Systemid
{
    public string value { get; set; }
}

public class Menu
{
    public string MenuCaption { get; set; }
    public string MenuLink { get; set; }
}


