﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SupplierPortal.WebApi.Models.LinkSystem
{
    public class CheckUserGuidModel
    {

        [Required]
        public string Guid { get; set; }

    }
}