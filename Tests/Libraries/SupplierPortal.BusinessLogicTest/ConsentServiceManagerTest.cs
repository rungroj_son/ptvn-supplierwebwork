﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SupplierPortal.BusinessLogic.ExternalService;
using SupplierPortal.BusinessLogic.ExternalService.Factory;
using System.Web.Configuration;
using Moq;
using SupplierPortal.Data.Models.SupportModel.Consent;
using System.Threading.Tasks;
using SupplierPortal.Services.APIService;
using System.Net.Http;
using SupplierPortal.Data.CustomModels.Consent;

namespace SupplierPortal.BusinessLogicTest
{
    [TestClass]
    public class ConsentServiceManagerTest
    {
        [TestMethod]
        public void PTVN_Initial_Constructor_Valid()
        {
            PTVNConsentServiceManager objTest = new PTVNConsentServiceManager();
            Assert.AreEqual(objTest.api, WebConfigurationManager.AppSettings["ptvn_consent_api"]);
            Assert.AreEqual(objTest.requestAcceptEndpoint, WebConfigurationManager.AppSettings["ptvn_consent_request_accept"]);
            Assert.AreEqual(objTest.acceptConsentEndpoint, WebConfigurationManager.AppSettings["ptvn_consent_accept_consent"]);
        }

        [TestMethod]
        public void Get_RequestConcentAccept_Call()
        {
            var resultMock = Task.FromResult(new ConsentRequestAcceptModel());
            var consentManagementMock = new Mock<IConsentServiceManager>();
            consentManagementMock.Setup(x => x.GetRequestConcentAccept(It.IsAny<string>())).Returns(resultMock);
            Assert.AreEqual(consentManagementMock.Object.GetRequestConcentAccept(It.IsAny<string>()), resultMock);
            consentManagementMock.Verify(o => o.GetRequestConcentAccept(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void Post_AcceptConsent_Call()
        {
            var resultMock = Task.FromResult(new ConsentAcceptModel());
            var consentManagementMock = new Mock<IConsentServiceManager>();
            consentManagementMock.Setup(x => x.AcceptConsent(It.IsAny<ConsentAcceptRequest>())).Returns(resultMock);
            Assert.AreEqual(consentManagementMock.Object.AcceptConsent(It.IsAny<ConsentAcceptRequest>()), resultMock);
            consentManagementMock.Verify(o => o.AcceptConsent(It.IsAny<ConsentAcceptRequest>()), Times.Once);
        }
    }
}
