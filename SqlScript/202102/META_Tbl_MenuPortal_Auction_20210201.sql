Begin Transaction
If Exists (Select * From Tbl_MenuPortal Where MenuName = 'm_Sourcing_Auction')
	Begin
		Update	Tbl_MenuPortal
		Set		SystemPageID = 0,
				ControllerName = 'Redirect',
				ActionName = 'RedirectAuction'
		Where	MenuName = 'm_Sourcing_Auction'
	End
Else
	Begin
		INSERT INTO [dbo].[Tbl_MenuPortal]
           ([MenuName]
           ,[ResourceName]
           ,[MenuLevel]
           ,[Section]
           ,[SeqNo]
           ,[SystemGrpID]
           ,[SystemPageID]
           ,[ControllerName]
           ,[ActionName]
           ,[SystemURL]
           ,[PrivilegeTypeID]
           ,[NotificationID]
           ,[Icon]
           ,[IsActive]
           ,[ParentMenuID])
     VALUES
           ('m_Sourcing_Auction'
           ,'_MenuSub.Sourcing.AuctionEvent'
           ,2
           ,'Sourcing'
           ,3
           ,2
           ,0
           ,'Redirect'
           ,'RedirectAuction'
           ,'http://192.168.10.186:8080/OALC_SP/bidder/VerifiedAuctionLogin.action'
           ,0
           ,1
           ,''
           ,1
           ,0)
	End
Commit