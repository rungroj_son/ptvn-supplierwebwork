﻿If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 1 And ResourceName = '_MenuSub.SupplyTool.ProcurementBoard')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (1,'_MenuSub.SupplyTool.ProcurementBoard', 'Procurement Board', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_MenuSub.SupplyTool.ProcurementBoard')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (2,'_MenuSub.SupplyTool.ProcurementBoard', N'บอร์ดงานจัดซื้อจัดจ้าง', NULL);
End

  update [Tbl_MenuPortal] set ControllerName = 'Redirect', ActionName = 'RedirectProcurementBoard' , SystemPageID = 0
  where MenuName = 'm_SupplyTool_ProcurementBoard'
