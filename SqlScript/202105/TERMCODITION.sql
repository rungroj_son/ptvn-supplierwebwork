﻿If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 1 And ResourceName = '_Agreement.TermAndCondition.TermService')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (1,'_Agreement.TermAndCondition.TermService', 'Term of Service', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_Agreement.SCFAgreementDetailModal.TermService')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (2,'_Agreement.TermAndCondition.TermService', N'ข้อกำหนดและเงื่อนไขบริการ', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 1 And ResourceName = '_Agreement.TermAndCondition.PrivacyPolicy')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (1,'_Agreement.TermAndCondition.PrivacyPolicy', 'Privacy Policy', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_Agreement.SCFAgreementDetailModal.PrivacyPolicy')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (2,'_Agreement.TermAndCondition.PrivacyPolicy', N'นโยบายการคุ้มครองข้อมูลส่วนบุคคล', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 1 And ResourceName = '_Agreement.TermAndCondition.DeclineLogOut')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (1,'_Agreement.TermAndCondition.DeclineLogOut', 'Decline and Log out', NULL);
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_Agreement.SCFAgreementDetailModal.DeclineLogOut')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (2,'_Agreement.TermAndCondition.DeclineLogOut', N'ไม่ยอมรับและออกจากระบบ', NULL);
End