USE [SupplierPortal]
GO
/****** Object:  StoredProcedure [dbo].[ContactWithPagination_Sel]    Script Date: 11/26/2020 1:54:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Piyavut.Cho>
-- Create date: <C25-11-2020>
-- Description:	<Get Organize with Pagination>
-- =============================================
ALTER PROCEDURE [dbo].[ContactWithPagination_Sel]
	-- Add the parameters for the stored procedure here
	@pPageIndex INT,
	@pPageSize INT,
	@pSearch VARCHAR(max),
	@pSorting VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @vSqlText NVARCHAR(MAX), @vTotalRecordSql NVARCHAR(MAX)
	DECLARE @vStartIndex INT, @vEndIndex  INT, @vTotalRecord INT


	DECLARE @vTableTotalRecord TABLE (
		TotlaRecord				INT
	)

	DECLARE @vTableContact TABLE (
		RowNumber				INT,
		OrgID					NVARCHAR(50),
		SupplierID				INT,
		CompanyName_Inter		NVARCHAR(500),
		CompanyName_Local		NVARCHAR(500),
		ContactID				INT,
		FirstName_Local			NVARCHAR(500), 
		LastName_Local			NVARCHAR(500),
		FirstName_Inter			NVARCHAR(500),
		LastName_Inter			NVARCHAR(500),
		PhoneNo					NVARCHAR(500),
		PhoneExt				NVARCHAR(500),
		PhoneCountryCode		NVARCHAR(500),
		MobileNo				NVARCHAR(500),
		MobileCountryCode		NVARCHAR(500),
		Email					NVARCHAR(500),
		UserID					INT,
		Username				NVARCHAR(500)
	)

	SET @vStartIndex = ((@pPageSize * @pPageIndex)  - @pPageSize) + 1
	SET @vEndIndex = @vStartIndex + @pPageSize - 1

	SET @vSqlText = 'SELECT * FROM(
	SELECT	ROW_NUMBER() OVER(ORDER BY '  + @pSorting + ') AS RowNumber,
				org.OrgID,
				org.SupplierID,
				org.CompanyName_Inter,
				org.CompanyName_Local,
				Tbl_ContactPerson.ContactID,
				Tbl_ContactPerson.FirstName_Local, 
				Tbl_ContactPerson.LastName_Local,
				Tbl_ContactPerson.FirstName_Inter,
				Tbl_ContactPerson.LastName_Inter,
				Tbl_ContactPerson.PhoneNo,
				Tbl_ContactPerson.PhoneExt,
				Tbl_ContactPerson.PhoneCountryCode,
				Tbl_ContactPerson.MobileNo,
				Tbl_ContactPerson.MobileCountryCode,
				Tbl_ContactPerson.Email,
				Tbl_User.UserID,
				Tbl_User.Username
	FROM		Tbl_ContactPerson 
	LEFT JOIN	Tbl_User ON Tbl_User.ContactID  = Tbl_ContactPerson.ContactID
	LEFT JOIN	Tbl_Organization org ON org.SupplierID  = Tbl_User.SupplierID  WHERE 1 = 1 ' + @pSearch + ')  Temp 
	WHERE 1 = 1 AND RowNumber >= ' + CAST(@vStartIndex AS NVARCHAR(50)) +' AND RowNumber <= ' + CAST(@vEndIndex AS NVARCHAR(50))

	SET @vTotalRecordSql = 'SELECT COUNT(''*'') FROM Tbl_ContactPerson 
	LEFT JOIN	Tbl_User ON Tbl_User.ContactID  = Tbl_ContactPerson.ContactID
	LEFT JOIN	Tbl_Organization org ON org.SupplierID  = Tbl_User.SupplierID WHERE 1 = 1 ' + @pSearch

	INSERT INTO @vTableContact
	EXECUTE(@vSqlText)

	INSERT INTO @vTableTotalRecord
	EXECUTE(@vTotalRecordSql)

	SET @vTotalRecord = (SELECT TotlaRecord FROM @vTableTotalRecord)

	SELECT		@vTotalRecord AS TotalRecord,
				RowNumber,
				OrgID,
				SupplierID,
				CompanyName_Inter,
				CompanyName_Local,
				ContactID,
				FirstName_Local, 
				LastName_Local,
				FirstName_Inter,
				LastName_Inter,
				PhoneNo,
				PhoneExt,
				PhoneCountryCode,
				MobileNo,
				MobileCountryCode,
				Email,
				UserID,
				Username 
	FROM		@vTableContact

END
