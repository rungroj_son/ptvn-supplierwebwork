USE [SupplierPortal]
GO
/****** Object:  StoredProcedure [dbo].[OrganizationWithPagination_Sel]    Script Date: 11/26/2020 1:20:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Piyavut.Cho>
-- Create date: <C25-11-2020>
-- Description:	<Get Organize with Pagination>
-- =============================================
ALTER PROCEDURE [dbo].[OrganizationWithPagination_Sel]
	-- Add the parameters for the stored procedure here
	@pPageIndex INT,
	@pPageSize INT,
	@pSearch VARCHAR(max),
	@pSorting VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @vSearchOrganize NVARCHAR(MAX), @vTotalRecordSql NVARCHAR(MAX)
	DECLARE @vStartIndex INT, @vEndIndex  INT, @vTotalRecord INT

	DECLARE @vTableOrganization TABLE (
		RowNumber				INT,
		OrgID					NVARCHAR(50),
		SupplierID				INT,
		CompanyName_Inter		NVARCHAR(500),
		CompanyName_Local		NVARCHAR(500)
	)

	DECLARE @vTableTotalRecord TABLE (
		TotlaRecord				INT
	)

	SET @vStartIndex = ((@pPageSize * @pPageIndex)  - @pPageSize) + 1
	SET @vEndIndex = @vStartIndex + @pPageSize - 1

	SET @vSearchOrganize = 'SELECT	RowNumber, OrgID, SupplierID, CompanyName_Inter,  CompanyName_Local  FROM
    (SELECT ROW_NUMBER() OVER(ORDER BY '  + @pSorting + ') AS RowNumber, SupplierID, OrgID, CompanyName_Inter, CompanyName_Local
	FROM Tbl_Organization org WHERE 1 = 1 ' + @pSearch + ') Temp WHERE 1 = 1 AND RowNumber >= ' + CAST(@vStartIndex AS NVARCHAR(50)) +' AND RowNumber <= ' + CAST(@vEndIndex AS NVARCHAR(50))
	
	SET @vTotalRecordSql = 'SELECT COUNT(''*'') FROM Tbl_Organization org WHERE 1 = 1 ' + @pSearch

	INSERT INTO @vTableTotalRecord
	EXECUTE(@vTotalRecordSql)

	INSERT INTO @vTableOrganization
	EXECUTE(@vSearchOrganize)

	print @vSearchOrganize
	print @vTotalRecordSql

	SET @vTotalRecord = (SELECT TotlaRecord FROM @vTableTotalRecord)

	SELECT		@vTotalRecord AS TotalRecord,
				org.RowNumber,
				org.OrgID,
				org.SupplierID,
				org.CompanyName_Inter,
				org.CompanyName_Local,
				Tbl_ContactPerson.ContactID,
				Tbl_ContactPerson.FirstName_Local,
				Tbl_ContactPerson.LastName_Local,
				Tbl_ContactPerson.FirstName_Inter,
				Tbl_ContactPerson.LastName_Inter,
				Tbl_ContactPerson.PhoneNo,
				Tbl_ContactPerson.PhoneExt,
				Tbl_ContactPerson.PhoneCountryCode,
				Tbl_ContactPerson.MobileNo,
				Tbl_ContactPerson.MobileCountryCode,
				Tbl_ContactPerson.Email,
				Tbl_User.UserID,
				Tbl_User.Username
	FROM		@vTableOrganization org
	LEFT JOIN	Tbl_User ON Tbl_User.SupplierID  = org.SupplierID
	LEFT JOIN	Tbl_ContactPerson ON Tbl_ContactPerson.ContactID  = Tbl_User.ContactID
	ORDER BY RowNumber  ASC

END
