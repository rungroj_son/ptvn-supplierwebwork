﻿Begin Tran

Select ''[BeforeUpdate],* From [dbo].[Tbl_LocaleStringResource] Where [ResourceName] = '_MenuSub.Sourcing.ApproveVendorList'

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 1 And ResourceName = '_MenuSub.Sourcing.ApproveVendorList')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (1,'_MenuSub.Sourcing.ApproveVendorList', 'Code of Conduct', NULL);
End
Else If Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 1 And ResourceName = '_MenuSub.Sourcing.ApproveVendorList')
Begin
	Update	[dbo].[Tbl_LocaleStringResource]
	Set		[ResourceValue] = 'Code of Conduct'
	Where	LanguageID = 1 
	And		ResourceName = '_MenuSub.Sourcing.ApproveVendorList'
End

If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_MenuSub.Sourcing.ApproveVendorList')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (2,'_MenuSub.Sourcing.ApproveVendorList', 'แบบประเมินจรรยาบรรณธุรกิจ', NULL);
End
Else If Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_MenuSub.Sourcing.ApproveVendorList')
Begin
	Update	[dbo].[Tbl_LocaleStringResource]
	Set		[ResourceValue] = 'แบบประเมินจรรยาบรรณธุรกิจ'
	Where	LanguageID = 2
	And		ResourceName = '_MenuSub.Sourcing.ApproveVendorList'
End

Select ''[AfterUpdate],* From [dbo].[Tbl_LocaleStringResource] Where [ResourceName] = '_MenuSub.Sourcing.ApproveVendorList'

Commit