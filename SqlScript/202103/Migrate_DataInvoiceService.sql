USE [SupplierPortal]
GO
/****** Object:  StoredProcedure [dbo].[Migrate_SupplierMappingInvoiceService]    Script Date: 3/12/2021 10:15:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Piyavut.cho
-- Create date: 2021-03-08
-- Description:	SupplierMappingInvoiceService
-- =============================================
ALTER PROCEDURE [dbo].[Migrate_SupplierMappingInvoiceService]
	@pSupplierID NVARCHAR(MAX) = ''
AS
BEGIN 
--BEGIN TRANSACTION	
	DECLARE @vSqlListOrgSystemSupplierID NVARCHAR(MAX), @vSqlListOrgMenuSupplierID NVARCHAR(MAX)
	DECLARE @vSystemID INT,@vInvoiceSystemID INT, @vOrgSupplierID INT, @vUserID INT, @vServiceID INT
	DECLARE @vListOrgSystemSupplierID TABLE
	(
		SupplierID INT
	)

	DECLARE @vListOrgMenuSupplierID TABLE
	(
		SupplierID INT
	)

	DECLARE @vListSupplierID TABLE
	(
		SupplierID INT
	)

	DECLARE @vListUserID TABLE
	(
		UserID INT
	)
	SELECT @vSystemID = 1
	SELECT @vInvoiceSystemID = SystemID FROM Tbl_System WHERE SystemName = 'Invoice'
	SELECT @vServiceID = ServiceID FROM Tbl_Service WHERE ServiceName = 'Invoice'

	---- INSERT Supplier FROM Parameter
	BEGIN
	IF LEN(@pSupplierID) > 0 
		BEGIN
			SET @vSqlListOrgSystemSupplierID = 'SELECT DISTINCT(SupplierID) FROM Tbl_OrgSystemMapping WHERE SupplierID IN ('+ @pSupplierID +') AND SystemID = 1'
			SET @vSqlListOrgMenuSupplierID = 'SELECT DISTINCT(SupplierID) FROM Tbl_ACL_OrgMenuMapping WHERE SupplierID IN ('+ @pSupplierID +') AND MenuID IN (40,42)'

			INSERT INTO @vListOrgSystemSupplierID
			EXEC sp_executesql @vSqlListOrgSystemSupplierID

			INSERT INTO @vListOrgMenuSupplierID
			EXEC sp_executesql @vSqlListOrgMenuSupplierID
		END
	ELSE
		BEGIN
			INSERT INTO @vListOrgSystemSupplierID
			SELECT DISTINCT(SupplierID) FROM Tbl_OrgSystemMapping WHERE SystemID = @vSystemID

			INSERT INTO @vListOrgMenuSupplierID
			SELECT DISTINCT(SupplierID) FROM Tbl_ACL_OrgMenuMapping WHERE SupplierID in (SELECT SupplierID from Tbl_OrgSystemMapping where SystemID = @vSystemID) AND MenuID IN (40,42)			 
		END	
	END	
	---- INSERT Supplier FROM Parameter

	---- INSERT ALL Supplier
	BEGIN
		INSERT INTO @vListSupplierID
		SELECT DISTINCT(SupplierID) FROM (
			SELECT SupplierID FROM @vListOrgMenuSupplierID
			UNION ALL
			SELECT SupplierID FROM @vListOrgSystemSupplierID
		) AS ListTemp
	END
	---- INSERT ALL Supplier

	---- INSERT User
	BEGIN				
		INSERT INTO @vListUserID
		SELECT UserID FROM Tbl_User WHERE SupplierID IN (SELECT SupplierID FROM @vListSupplierID)
	END
	---- INSERT User

	---------- Org Menu
	BEGIN
		DECLARE	cOrgSupplier CURSOR
			FOR
			SELECT SupplierID FROM @vListSupplierID

			OPEN	cOrgSupplier
			FETCH NEXT
			FROM	cOrgSupplier
			INTO	@vOrgSupplierID

				WHILE @@FETCH_STATUS = 0
					BEGIN
						SELECT 'Menu',@vOrgSupplierID
						IF NOT EXISTS(SELECT '*' FROM Tbl_OrgSystemMapping WHERE SupplierID = @vOrgSupplierID AND SystemID = @vInvoiceSystemID)
							BEGIN
								print '@vOrgSupplierID'
								print @vOrgSupplierID
								INSERT INTO Tbl_OrgSystemMapping VALUES(@vOrgSupplierID,@vInvoiceSystemID,1,0,0,1)
							END

						IF NOT EXISTS(SELECT '*' FROM Tbl_ACL_OrgMenuMapping WHERE SupplierID = @vOrgSupplierID AND MenuID IN (40,42))
							BEGIN
								print 'OrgSystem'
								print @vOrgSupplierID
								INSERT INTO Tbl_ACL_OrgMenuMapping VALUES(@vOrgSupplierID,40,1)
								INSERT INTO Tbl_ACL_OrgMenuMapping VALUES(@vOrgSupplierID,42,1)
							END
							
				FETCH NEXT
			FROM	cOrgSupplier
			INTO	@vOrgSupplierID
					END
				CLOSE cOrgSupplier
				DEALLOCATE cOrgSupplier
	END
	---------- Org Menu

	---------- Tbl_UserSystemMapping Tbl_UserServiceMapping
	BEGIN
		DECLARE	cUserSystemMapping CURSOR
			FOR
			SELECT	UserID FROM @vListUserID

			OPEN	cUserSystemMapping
			FETCH NEXT
			FROM	cUserSystemMapping
			INTO	@vUserID

				WHILE @@FETCH_STATUS = 0
					BEGIN
						IF NOT EXISTS(SELECT '*' FROM Tbl_UserSystemMapping WHERE UserID = @vUserID AND SystemID = @vInvoiceSystemID)
							BEGIN
								print 'Tbl_UserSystemMapping'
								print @vUserID
								INSERT INTO Tbl_UserSystemMapping VALUES(@vUserID,(SELECT Username FROM Tbl_User WHERE UserID = @vUserID),'',@vInvoiceSystemID,0,1,0)
							END
						IF NOT EXISTS(SELECT '*' FROM Tbl_UserServiceMapping WHERE UserID = @vUserID AND ServiceID = @vServiceID)
							BEGIN
								print 'Tbl_UserServiceMapping'
								print @vUserID
								INSERT INTO Tbl_UserServiceMapping VALUES(@vUserID,@vServiceID,1)
							END

				FETCH NEXT
			FROM	cUserSystemMapping
			INTO	@vUserID
					END
				CLOSE cUserSystemMapping
				DEALLOCATE cUserSystemMapping
	END
	---------- Tbl_UserSystemMapping Tbl_UserServiceMapping
		--ROLLBACK
END 
