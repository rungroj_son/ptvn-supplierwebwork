IF EXISTS(select * FROM sys.views where name = 'ViewOrgVendorConsent')
Begin
	DROP VIEW [dbo].[ViewOrgVendorConsent]
End
GO
CREATE VIEW [dbo].[ViewOrgVendorConsent] AS
Select		Distinct
			Tbl_User.UserID,
			Tbl_User.Username,
			Tbl_User.SupplierID,
			Tbl_SupplierConsent.ApplicationName,
			Tbl_SupplierConsent.BuyerShortName,
			Tbl_SupplierConsent.SupplierShortName
From		Tbl_User
Inner Join	Tbl_Organization
On			Tbl_Organization.SupplierID = Tbl_User.SupplierID
Inner Join	Tbl_SupplierConsent
On			Tbl_SupplierConsent.SupplierShortName = Tbl_Organization.OrgID