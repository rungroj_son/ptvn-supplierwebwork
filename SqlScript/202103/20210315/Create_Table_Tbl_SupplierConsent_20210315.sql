If Not Exists (Select 'x' From INFORMATION_SCHEMA.TABLES Where TABLE_NAME = 'Tbl_SupplierConsent')
Begin
	CREATE TABLE [dbo].[Tbl_SupplierConsent](
		[ApplicationID] [nvarchar](50) NULL,
		[ApplicationName] [nvarchar](255) NULL,
		[BuyerShortName] [nvarchar](50) NULL,
		[SupplierShortName] [nvarchar](50) NULL
	) ON [PRIMARY]
End