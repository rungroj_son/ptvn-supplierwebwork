﻿If Not Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_Agreement.CodeOfConduct.HeaderTitle')
Begin
	INSERT INTO [dbo].[Tbl_LocaleStringResource] ([LanguageID], [ResourceName], [ResourceValue], [OldResourceName])
	VALUES (2,'_Agreement.CodeOfConduct.HeaderTitle', 'จรรยาบรรณและข้อพึงปฏิบัติ สำหรับคู่ค้า (Supplier Code of Conduct)', NULL);
End
Else If Exists (Select 'x' From [dbo].[Tbl_LocaleStringResource] Where LanguageID = 2 And ResourceName = '_Agreement.CodeOfConduct.HeaderTitle')
Begin
	Update  [dbo].[Tbl_LocaleStringResource]
	Set 	[ResourceValue] = 'จรรยาบรรณและข้อพึงปฏิบัติ สำหรับคู่ค้า (Supplier Code of Conduct)'
	Where 	LanguageID = 2 
	And 	ResourceName = '_Agreement.CodeOfConduct.HeaderTitle'
End