﻿EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewOrgVendor'
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewOrgVendor'
GO

/****** Object:  View [dbo].[ViewOrgVendor]    Script Date: 10/21/2020 11:07:17 AM ******/
DROP VIEW [dbo].[ViewOrgVendor]
GO

/****** Object:  View [dbo].[ViewOrgVendor]    Script Date: 10/21/2020 11:07:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[ViewOrgVendor]
AS

SELECT     ISNULL(LTRIM(RTRIM(A.SupplierID)),'0') SupplierID, /*A.OrgID  ,*/ ISNULL(LTRIM(RTRIM(A.EID)),'0') EID, LTRIM(RTRIM(ISNULL(STUFF
                          ((SELECT     ','+ LTRIM(RTRIM(GA.VendorNum))
                              FROM         (SELECT     org.SupplierID, org.OrgID, org.TaxID, org.CompanyName_Inter, org.CompanyName_Local, imven.SupplierShortName, imven.VendorName, imven.VendorNum, imven.EID, 
                                                                            org.BranchNo
                                                     FROM          Tbl_Organization org INNER JOIN
                                                                            Tbl_SDImportVendor imven 
																			ON org.OrgID = imven.SupplierShortName
																			AND org.BranchNo = imven.Branch 
                                                     UNION
                                                     SELECT     org.SupplierID, org.OrgID, org.TaxID, org.CompanyName_Inter, org.CompanyName_Local, imven.SupplierShortName, imven.VendorName, imven.VendorNum, imven.EID, 
                                                                           org.BranchNo
                                                     FROM         Tbl_Organization org INNER JOIN
                                                                           Tbl_SDImportVendor imven ON org.TaxID = imven.TaxID AND org.CompanyName_Inter = imven.VendorName AND org.BranchNo = imven.Branch
                                                     UNION
                                                     SELECT     org.SupplierID, org.OrgID, org.TaxID, org.CompanyName_Inter, org.CompanyName_Local, imven.SupplierShortName, imven.VendorName, imven.VendorNum, imven.EID, 
                                                                           org.BranchNo
                                                     FROM         Tbl_Organization org INNER JOIN
                                                                           Tbl_SDImportVendor imven ON org.TaxID = imven.TaxID AND org.CompanyName_Local = imven.VendorName AND org.BranchNo = imven.Branch) AS GA
                              WHERE     GA.SupplierID = A.SupplierID AND GA.EID = A.EID
                              GROUP BY GA.VendorNum FOR XML PATH('')), 1, 1, ''), ''))) AS VendorNum
FROM         (SELECT     org.SupplierID, org.OrgID, org.TaxID, org.CompanyName_Inter, org.CompanyName_Local, imven.SupplierShortName, imven.VendorName, imven.VendorNum, imven.EID, 
                                              org.BranchNo
                       FROM          Tbl_Organization org INNER JOIN
                                              Tbl_SDImportVendor imven ON org.OrgID = imven.SupplierShortName
                       UNION
                       SELECT     org.SupplierID, org.OrgID, org.TaxID, org.CompanyName_Inter, org.CompanyName_Local, imven.SupplierShortName, imven.VendorName, imven.VendorNum, imven.EID, 
                                             org.BranchNo
                       FROM         Tbl_Organization org INNER JOIN
                                             Tbl_SDImportVendor imven ON org.TaxID = imven.TaxID AND org.CompanyName_Inter = imven.VendorName AND org.BranchNo = imven.Branch
                       UNION
                       SELECT     org.SupplierID, org.OrgID, org.TaxID, org.CompanyName_Inter, org.CompanyName_Local, imven.SupplierShortName, imven.VendorName, imven.VendorNum, imven.EID, 
                                             org.BranchNo
                       FROM         Tbl_Organization org INNER JOIN
                                             Tbl_SDImportVendor imven ON org.TaxID = imven.TaxID AND org.CompanyName_Local = imven.VendorName AND org.BranchNo = imven.Branch) AS A
GROUP BY A.SupplierID, A.OrgID, A.EID

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[16] 4[28] 2[38] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewOrgVendor'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'ViewOrgVendor'
GO


