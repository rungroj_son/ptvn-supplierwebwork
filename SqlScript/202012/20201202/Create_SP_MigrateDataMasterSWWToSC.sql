USE [Supplier_Connect]
GO

/****** Object:  StoredProcedure [dbo].[SP_MigrateDataSWWToSC]    Script Date: 12/15/2020 2:13:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Piyavut>
-- Create date: <2020-11-05>
-- Description:	<Migrate Data from Supplier webwork to Supplier Connect>
-- =============================================
ALTER PROCEDURE [dbo].[SP_MigrateMasterDataSWWToSC] @pListOfIDs NVARCHAR(1000) = ''
AS
BEGIN
	DECLARE @tableRegInfo TABLE (
		RegId INT
		,SupplierId INT
		,TaxId NVARCHAR(50)
		,TaxCountryCode NVARCHAR(10)
		,BranchNumber NVARCHAR(100)
		)
	DECLARE @tableSupplierID TABLE (SupplierID INT)

	BEGIN --SWW Variable
		DECLARE @sww_CountryCode NVARCHAR(1000)
			,@sww_TaxID NVARCHAR(1000)
			,@sww_OtherBusinessEntity NVARCHAR(1000)
			,@sww_CompanyName_Local NVARCHAR(1000)
			,@sww_YearEstablished NVARCHAR(1000)
			,@sww_CompanyName_Inter NVARCHAR(1000)
			,@sww_BranchNo NVARCHAR(1000)
			,@sww_BranchName_Local NVARCHAR(1000)
			,@sww_BranchName_Inter NVARCHAR(1000)
			,@sww_SupplierPhoneNo NVARCHAR(1000)
			,@sww_SupplierPhoneExt NVARCHAR(1000)
			,@sww_SupplierMobileNo NVARCHAR(1000)
			,@sww_SupplierFaxNo NVARCHAR(1000)
			,@sww_SupplierFaxExt NVARCHAR(1000)
			,@sww_SupplierWebSite NVARCHAR(1000)
			,@sww_CurrencyCode NVARCHAR(1000)
			-- Address
			,@sww_HouseNo_Local NVARCHAR(100)
			,@sww_HouseNo_Inter NVARCHAR(100)
			,@sww_VillageNo_Local NVARCHAR(100)
			,@sww_VillageNo_Inter NVARCHAR(100)
			,@sww_Lane_Local NVARCHAR(100)
			,@sww_Lane_Inter NVARCHAR(100)
			,@sww_Road_Local NVARCHAR(100)
			,@sww_Road_Inter NVARCHAR(100)
			,@sww_SubDistrict_Local NVARCHAR(100)
			,@sww_SubDistrict_Inter NVARCHAR(100)
			,@sww_City_Local NVARCHAR(100)
			,@sww_City_Inter NVARCHAR(100)
			,@sww_State_Local NVARCHAR(100)
			,@sww_State_Inter NVARCHAR(100)
			,@sww_PostalCode NVARCHAR(1000)
			,@sww_FirstName_Local NVARCHAR(1000)
			,@sww_FirstName_Inter NVARCHAR(1000)
			,@sww_LastName_Local NVARCHAR(1000)
			,@sww_LastName_Inter NVARCHAR(1000)
			,@sww_OtherJobTitle NVARCHAR(1000)
			,@sww_Department NVARCHAR(1000)
			,@sww_ContactPhoneCountryCode NVARCHAR(1000)
			,@sww_ContactPhoneNo NVARCHAR(1000)
			,@sww_ContactPhoneExt NVARCHAR(1000)
			,@sww_ContactMobileCountryCode NVARCHAR(1000)
			,@sww_ContactMobileNo NVARCHAR(1000)
			,@sww_ContactFaxNo NVARCHAR(1000)
			,@sww_ContactFaxExt NVARCHAR(1000)
			,@sww_ContactEmail NVARCHAR(1000)
			,@sww_SqlText NVARCHAR(1000)
		DECLARE @sww_SupplierID INT
			,@sww_CompanyTypeID INT
			,@sww_BusinessEntityID INT
			,@sww_ContactID INT
			,@sww_TitleID INT
			,@sww_JobTitleID INT
			,@sww_RegID INT
		DECLARE @sww_IsPTVNVerified BIT
			,@sww_IsAffiliateCP BIT
			,@sww_IsDeleted BIT
		DECLARE @sww_LastUpdate DATETIME
		DECLARE @sww_RegisteredCapital DECIMAL(18, 2)
	END

	BEGIN --SC Variable
		DECLARE @sc_CountryCodeThailand NVARCHAR(100)
			,@sc_OtherAddress NVARCHAR(1000)
			,@sc_OtherAddressLocal NVARCHAR(1000)
			,@sc_HouseNumber NVARCHAR(1000)
			,@sc_HouseNumberLocal NVARCHAR(1000)
			,@sc_MooNumber NVARCHAR(1000)
			,@sc_MooNumberLocal NVARCHAR(1000)
			,@sc_Alley NVARCHAR(1000)
			,@sc_AlleyLocal NVARCHAR(1000)
			,@sc_Street NVARCHAR(1000)
			,@sc_StreetLocal NVARCHAR(1000)
			,@sc_ProvinceName NVARCHAR(1000)
			,@sc_ProvinceNameLocal NVARCHAR(1000)
			,@sc_DistrictName NVARCHAR(1000)
			,@sc_DistrictNameLocal NVARCHAR(1000)
			,@sc_SubDistrictName NVARCHAR(1000)
			,@sc_SubDistrictNameLocal NVARCHAR(1000)
			,@sc_PostalCode NVARCHAR(100)
		DECLARE @sc_CreatedBy INT = 1
			,@sc_UserId INT
			,@sc_SupplierId INT
			,@sc_BranchId INT
			,@sc_CountryId INT
			,@sc_BranchStatusId INT
			,@sc_SupplierStatusId_Approve INT
			,@sc_BranchStatusId_Approve INT
			,@sc_UserStatusId_Active INT
			,@sc_UserBranchStatusId_Approve INT
			,@sc_ActivateStatusId_Active INT
		DECLARE @sc_IsHeadQuarter BIT
			,@sc_IsEnglishOfficial BIT
			,@sc_IsThailand BIT
			,@sc_IsUseOP BIT
			,@sc_IsManufacturer BIT
			,@sc_IsDistributor BIT
			,@sc_IsDealer BIT
			,@sc_IsServiceProvider BIT
			,@sc_IsAddressMasterMatch BIT

		SET @sc_CountryCodeThailand = 'th'
		SET @sc_SupplierStatusId_Approve = (
				SELECT Id
				FROM SupplierStatus
				WHERE Name = 'Common.SupplierStatus.Approved'
				)
		SET @sc_BranchStatusId_Approve = (
				SELECT Id
				FROM BranchStatus
				WHERE Name = 'Common.BranchStatus.Approved'
				)
		SET @sc_UserStatusId_Active = (
				SELECT Id
				FROM UserStatus
				WHERE Name = 'Common.UserStatus.Active'
				)
		SET @sc_UserBranchStatusId_Approve = (
				SELECT Id
				FROM UserBranchStatus
				WHERE Name = 'Common.UserBranchStatus.Approved'
				)
		SET @sc_ActivateStatusId_Active = (
				SELECT Id
				FROM ActivateStatus
				WHERE Name = 'Common.ActivateStatus.Active'
				)
	END

	BEGIN
		IF (LEN(RTRIM(LTRIM(@pListOfIDs))) = 0)
		BEGIN
			SET @sww_SqlText = 'SELECT SupplierID FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_Organization] 
									WHERE SupplierID NOT IN (SELECT SWW_RegId FROM Temp_MigrateDataSWWAndSC)'
		END
		ELSE
		BEGIN
			SET @sww_SqlText = 'SELECT SupplierID FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_Organization] WHERE SupplierID IN (' + @pListOfIDs + ')'
		END

		INSERT INTO @tableSupplierID
		EXECUTE (@sww_SqlText)
	END

	BEGIN --TRANSACTION
		DECLARE cInsertSupplier CURSOR
		FOR
		SELECT SupplierID
			,CountryCode
			,TaxID
			,CompanyTypeID
			,BusinessEntityID
			,OtherBusinessEntity
			,CompanyName_Local
			,CompanyName_Inter
			,BranchNo
			,BranchName_Local
			,BranchName_Inter
			,PhoneNo
			,PhoneExt
			,MobileNo
			,FaxNo
			,FaxExt
			,WebSite
			,YearEstablished
			,RegisteredCapital
			,CurrencyCode
			,isPTVNVerified
			,isAffiliateCP
			,LastUpdate
			,isDeleted
		FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_Organization]
		WHERE isDeleted = 0 AND SupplierID IN (
				SELECT SupplierID
				FROM @tableSupplierID
				)

		--WHERE RegID IN (477,551,662)
		OPEN cInsertSupplier

		FETCH NEXT
		FROM cInsertSupplier
		INTO @sww_SupplierID
			,@sww_CountryCode
			,@sww_TaxID
			,@sww_CompanyTypeID
			,@sww_BusinessEntityID
			,@sww_OtherBusinessEntity
			,@sww_CompanyName_Local
			,@sww_CompanyName_Inter
			,@sww_BranchNo
			,@sww_BranchName_Local
			,@sww_BranchName_Inter
			,@sww_SupplierPhoneNo
			,@sww_SupplierPhoneExt
			,@sww_SupplierMobileNo
			,@sww_SupplierFaxNo
			,@sww_SupplierFaxExt
			,@sww_SupplierWebSite
			,@sww_YearEstablished
			,@sww_RegisteredCapital
			,@sww_CurrencyCode
			,@sww_IsPTVNVerified
			,@sww_IsAffiliateCP
			,@sww_LastUpdate
			,@sww_IsDeleted

		WHILE @@FETCH_STATUS = 0
		BEGIN
			PRINT '---START---'
			PRINT @sww_SupplierID

			BEGIN
				SET @sc_IsUseOP = 0
				SET @sc_CountryCodeThailand = 'th'
				SET @sc_IsThailand = CASE 
						WHEN UPPER(@sww_CountryCode) = UPPER(@sc_CountryCodeThailand)
							THEN 1
						ELSE 0
						END
				SET @sc_IsHeadQuarter = CASE 
						WHEN CAST(@sww_BranchNo AS INT) = 0
							THEN 1
						ELSE 0
						END

				IF EXISTS (
						SELECT '*'
						FROM Country
						WHERE CountryCode = @sww_CountryCode
						)
				BEGIN
					SELECT @sc_CountryId = Id
						,@sc_IsEnglishOfficial = IsEnglishOfficial
					FROM Country
					WHERE CountryCode = @sww_CountryCode
				END
				ELSE
				BEGIN
					SELECT @sc_CountryId = Id
						,@sc_IsEnglishOfficial = IsEnglishOfficial
					FROM Country
					WHERE CountryCode = 'TH'
				END

				IF EXISTS (
						SELECT '*'
						FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_OrgSystemMapping]
						WHERE SystemID IN (
								SELECT SystemID
								FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_System]
								WHERE SystemGrpID IN (
										SELECT SystemGrpID
										FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_SystemGroup]
										WHERE SystemGrpID = 1
										)
								)
							AND SupplierID = @sww_SupplierID
						)
				BEGIN
					SET @sc_IsUseOP = 1
				END

				SELECT TOP 1 @sww_Lane_Local = Lane_Local
					,@sww_Lane_Inter = Lane_Inter
					,@sww_SubDistrict_Local = SubDistrict_Local
					,@sww_SubDistrict_Inter = SubDistrict_Inter
					,@sww_City_Local = City_Local
					,@sww_City_Inter = City_Inter
				FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_OrgAddress]
				INNER JOIN [192.168.10.23].[SupplierPortal].[dbo].[Tbl_Address] ON Tbl_OrgAddress.AddressID = Tbl_Address.AddressID
				WHERE SupplierID = @sww_SupplierID
					AND AddressTypeID = 1

				IF (
						@sww_BranchName_Local = ''
						OR @sww_BranchName_Local IS NULL
						)
					AND (
						@sww_BranchName_Inter = ''
						OR @sww_BranchName_Inter IS NULL
						)
				BEGIN
					IF @sc_IsThailand = 1
					BEGIN
						IF (
								@sww_Lane_Local = ''
								OR @sww_Lane_Local IS NULL
								)
							AND (
								@sww_Lane_Inter = ''
								OR @sww_Lane_Inter IS NULL
								)
						BEGIN
							IF (
									@sww_SubDistrict_Local = ''
									OR @sww_SubDistrict_Local IS NULL
									)
								AND (
									@sww_SubDistrict_Inter = ''
									OR @sww_SubDistrict_Inter IS NULL
									)
							BEGIN
								SET @sww_BranchName_Local = CASE 
										WHEN @sww_City_Local = ''
											OR @sww_City_Local IS NULL
											THEN @sww_City_Inter
										ELSE @sww_City_Local
										END
								SET @sww_BranchName_Inter = CASE 
										WHEN @sww_City_Inter = ''
											OR @sww_City_Inter IS NULL
											THEN @sww_City_Local
										ELSE @sww_City_Inter
										END
							END
							ELSE
							BEGIN
								SET @sww_BranchName_Local = CASE 
										WHEN @sww_SubDistrict_Local = ''
											OR @sww_SubDistrict_Local IS NULL
											THEN @sww_SubDistrict_Inter
										ELSE @sww_SubDistrict_Local
										END
								SET @sww_BranchName_Inter = CASE 
										WHEN @sww_SubDistrict_Inter = ''
											OR @sww_SubDistrict_Inter IS NULL
											THEN @sww_SubDistrict_Local
										ELSE @sww_SubDistrict_Inter
										END
							END
						END
						ELSE
						BEGIN
							SET @sww_BranchName_Local = CASE 
									WHEN @sww_Lane_Local = ''
										OR @sww_Lane_Local IS NULL
										THEN @sww_Lane_Inter
									ELSE @sww_Lane_Local
									END
							SET @sww_BranchName_Inter = CASE 
									WHEN @sww_Lane_Inter = ''
										OR @sww_Lane_Inter IS NULL
										THEN @sww_Lane_Local
									ELSE @sww_Lane_Inter
									END
						END
					END
					ELSE
					BEGIN
						IF (
								@sww_SubDistrict_Local = ''
								OR @sww_SubDistrict_Local IS NULL
								)
							AND (
								@sww_SubDistrict_Inter = ''
								OR @sww_SubDistrict_Inter IS NULL
								)
						BEGIN
							SET @sww_BranchName_Local = CASE 
									WHEN @sww_City_Local = ''
										OR @sww_City_Local IS NULL
										THEN @sww_City_Inter
									ELSE @sww_City_Local
									END
							SET @sww_BranchName_Inter = CASE 
									WHEN @sww_City_Inter = ''
										OR @sww_City_Inter IS NULL
										THEN @sww_City_Local
									ELSE @sww_City_Inter
									END
						END
						ELSE
						BEGIN
							SET @sww_BranchName_Local = CASE 
									WHEN @sww_SubDistrict_Local = ''
										OR @sww_SubDistrict_Local IS NULL
										THEN @sww_SubDistrict_Inter
									ELSE @sww_SubDistrict_Local
									END
							SET @sww_BranchName_Inter = CASE 
									WHEN @sww_SubDistrict_Inter = ''
										OR @sww_SubDistrict_Inter IS NULL
										THEN @sww_SubDistrict_Local
									ELSE @sww_SubDistrict_Inter
									END
						END
					END
				END
				ELSE
				BEGIN
					SET @sww_BranchName_Local = CASE 
							WHEN @sww_BranchName_Local = ''
								OR @sww_BranchName_Local IS NULL
								THEN @sww_BranchName_Inter
							ELSE @sww_BranchName_Local
							END
					SET @sww_BranchName_Inter = CASE 
							WHEN @sww_BranchName_Inter = ''
								OR @sww_BranchName_Inter IS NULL
								THEN @sww_BranchName_Local
							ELSE @sww_BranchName_Inter
							END
				END
			END

			BEGIN -- Set Data Prinmary Contact AND Insert User
				SELECT TOP 1 @sww_ContactID = c.ContactID
					,@sww_TitleID = c.TitleID
					,@sww_FirstName_Local = c.FirstName_Local
					,@sww_FirstName_Inter = c.FirstName_Inter
					,@sww_LastName_Local = c.LastName_Local
					,@sww_LastName_Inter = c.LastName_Inter
					,@sww_JobTitleID = c.JobTitleID
					,@sww_OtherJobTitle = c.OtherJobTitle
					,@sww_Department = c.Department
					,@sww_ContactPhoneCountryCode = c.PhoneCountryCode
					,@sww_ContactPhoneNo = c.PhoneNo
					,@sww_ContactPhoneExt = c.PhoneExt
					,@sww_ContactMobileCountryCode = c.MobileCountryCode
					,@sww_ContactMobileNo = c.MobileNo
					,@sww_ContactFaxNo = c.FaxNo
					,@sww_ContactFaxExt = c.FaxExt
					,@sww_ContactEmail = c.Email
				FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_ContactPerson] c
				INNER JOIN [192.168.10.23].[SupplierPortal].[dbo].[Tbl_OrgContactPerson] o ON c.ContactID = o.ContactID
				WHERE o.SupplierID = @sww_SupplierID
					AND o.isPrimaryContact = 1

				IF @sww_ContactEmail IS NULL
					OR @sww_ContactEmail = ''
				BEGIN
					SET @sww_ContactEmail = 'webwork_' + CAST(@sww_ContactID AS NVARCHAR(100)) + '@noreply.com'
				END

				IF NOT EXISTS (
						SELECT '*'
						FROM Users
						WHERE Username = @sww_ContactEmail
						)
				BEGIN
					INSERT INTO Users (
						Username
						,[Password]
						,FirstName
						,LastName
						,FirstNameLocal
						,LastNameLocal
						,DefaultLanguageCode
						,Email
						,Telephone
						,CountryId
						,StatusId
						,IsAcceptTermOfService
						,IsCheckConsent
						,TicketCode
						,IsTicketCodeTerminate
						)
					VALUES (
						@sww_ContactEmail --Username
						,'{bcrypt}$2a$10$9VwOtR5bggHNZMTbENlByOuncWf1Ejs.sg/fpmHQRvKklEMmBel2q' --,[Password]
						,@sww_FirstName_Inter --,FirstName
						,@sww_LastName_Local --,LastName
						,@sww_FirstName_Local --,FirstNameLocal
						,@sww_LastName_Inter --,LastNameLocal
						,'en' --,DefaultLanguageCode
						,@sww_ContactEmail --,Email
						,@sww_ContactPhoneNo --,Telephone
						,@sc_CountryId --,CountryId							
						,@sc_UserStatusId_Active --,StatusId
						,1 --,IsAcceptTermOfService
						,1 --,IsCheckConsent
						,'' --,TicketCode
						,1 --,IsTicketCodeTerminate
						)

					SET @sc_UserId = SCOPE_IDENTITY()
				END
				ELSE
				BEGIN
					SET @sc_UserId = (
							SELECT TOP 1 Id
							FROM Users
							WHERE Email = @sww_ContactEmail
							)
				END
			END

			BEGIN -- Insert Supplier
				IF NOT EXISTS (
						SELECT '*'
						FROM Supplier
						WHERE TaxId = @sww_TaxID
							AND TaxCountryCode = @sww_CountryCode
						)
				BEGIN
					INSERT INTO Supplier (
						SupplierName
						,SupplierNameLocal
						,FirstName
						,LastName
						,FirstNameLocal
						,LastNameLocal
						,TaxId
						,IsVatRegistration
						,IsRDVerify
						,TaxCountryCode
						,IdentityTypeId
						,SupplierStatusId
						,BusinessEntityId
						,OtherBusinessEntityName
						,CreatedBy
						,CreatedAt
						,UpdatedBy
						,UpdatedAt
						)
					VALUES (
						@sww_CompanyName_Inter
						,--SupplierName,
						@sww_CompanyName_Local
						,--SupplierNameLocal,
						CASE 
							WHEN @sww_CompanyTypeID = 2
								THEN @sww_FirstName_Inter
							ELSE ''
							END
						,--FirstName,
						CASE 
							WHEN @sww_CompanyTypeID = 2
								THEN @sww_LastName_Inter
							ELSE ''
							END
						,--LastName,
						CASE 
							WHEN @sww_CompanyTypeID = 2
								THEN @sww_FirstName_Local
							ELSE ''
							END
						,--FirstNameLocal,
						CASE 
							WHEN @sww_CompanyTypeID = 2
								THEN @sww_LastName_Local
							ELSE ''
							END
						,--LastNameLocal,
						@sww_TaxId
						,--TaxId,
						1
						,--IsVatRegistration,
						1
						,--IsRDVerify,
						@sww_CountryCode
						,--TaxCountryCode,
						CASE 
							WHEN @sww_CompanyTypeID = 2
								THEN 2
							ELSE 1
							END
						,--IdentityTypeId,
						@sc_SupplierStatusId_Approve
						,--SupplierStatusId,
						ISNULL(@sww_BusinessEntityID, 1)
						,--BusinessEntityId,
						@sww_OtherBusinessEntity
						,--OtherBusinessEntityName,
						@sc_UserId
						,--CreatedBy,
						@sww_LastUpdate
						,--CreatedAt,
						@sc_UserId
						,--UpdatedBy,
						@sww_LastUpdate --UpdatedAt
						)

					SET @sc_SupplierId = SCOPE_IDENTITY()
				END
				ELSE
				BEGIN
					SET @sc_SupplierId = (
							SELECT TOP 1 Id
							FROM Supplier
							WHERE TaxId = @sww_TaxID
								AND TaxCountryCode = @sww_CountryCode
							)
				END
			END

			BEGIN -- Insert Branch
				INSERT INTO Branch (
					SupplierId
					,BranchNumber
					,BranchName
					,BranchNameLocal
					,BranchEmail
					,BranchWebSite
					,BranchStatusId
					,RegisteredCapital
					,CurrencyCode
					,YearEstablished
					,IsUseOP
					,InvitationCode
					,IsHeadQuarter
					,BillingContactName
					,BillingContactNameLocal
					,BillingContactEmail
					,BillingContactPhone
					,BillingContactCountryCode
					,IsSubmittedByAnother
					,IsApprovedByAnother
					,CreatedBy
					,CreatedAt
					,UpdatedBy
					,UpdatedAt
					)
				VALUES (
					@sc_SupplierId
					,--SupplierId,
					CASE 
						WHEN @sc_IsHeadQuarter = 1
							THEN N'00000'
						ELSE '0000' + CAST((
									(
										SELECT COUNT(Id)
										FROM Branch
										WHERE SupplierId = @sc_SupplierId
										) + 1
									) AS NVARCHAR(50))
						END
					,--BranchNumber,
					@sww_BranchName_Inter
					,--BranchName,
					@sww_BranchName_Local
					,--BranchNameLocal,
					@sww_ContactEmail
					,--BranchEmail,
					ISNULL(@sww_SupplierWebSite, '')
					,--BranchWebSite,
					@sc_BranchStatusId_Approve
					,--BranchStatusId,
					ISNULL(@sww_RegisteredCapital, 0)
					,--RegisteredCapital,
					@sww_CurrencyCode
					,--CurrencyCode,
					@sww_YearEstablished
					,--YearEstablished,
					@sc_IsUseOP
					,--IsUseOP,
					''
					,--InvitationCode,
					@sc_IsHeadQuarter
					,--IsHeadQuarter,
					CASE 
						WHEN @sc_IsUseOP = 1
							THEN @sww_FirstName_Inter + ' ' + @sww_LastName_Inter
						ELSE ''
						END
					,--BillingContactName,
					CASE 
						WHEN @sc_IsUseOP = 1
							THEN @sww_FirstName_Local + ' ' + @sww_LastName_Local
						ELSE ''
						END
					,--BillingContactNameLocal,
					CASE 
						WHEN @sc_IsUseOP = 1
							THEN @sww_ContactEmail
						ELSE ''
						END
					,--BillingContactEmail,
					CASE 
						WHEN @sc_IsUseOP = 1
							THEN @sww_ContactPhoneNo
						ELSE ''
						END
					,--BillingContactPhone,
					CASE 
						WHEN @sc_IsUseOP = 1
							THEN @sww_ContactPhoneCountryCode
						ELSE ''
						END
					,--BillingContactCountryCode,
					0
					,--IsSubmittedByAnother,
					0
					,--IsApprovedByAnother,
					0
					,--CreatedBy,
					@sww_LastUpdate
					,--CreatedAt,
					0
					,--UpdatedBy,
					@sww_LastUpdate --UpdatedAt
					)

				SET @sc_BranchId = SCOPE_IDENTITY()
			END

			BEGIN
				IF EXISTS (
						SELECT '*'
						FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegApprove]
						WHERE SupplierID = @sww_SupplierID
						)
				BEGIN
					SET @sww_RegID = (
							SELECT RegID
							FROM [192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegApprove]
							WHERE SupplierID = @sww_SupplierID
							)
				END
				ELSE
				BEGIN
					SET @sww_RegID = @sww_SupplierID * - 1

					INSERT INTO [192.168.10.23].[SupplierPortal].[dbo].[Tbl_RegApprove]
					SELECT @sww_RegID
						,''
						,0
						,1
						,'00000000'
						,GETDATE()
						,@sww_SupplierID
				END

				INSERT INTO UserBranch (
					UserId
					,BranchId
					,RoleId
					,ActivateStatusId
					,UserBranchStatusId
					,JobTitleId
					,OtherJobTitleName
					,ContactEmail
					,ContactPhoneNumber
					,ContactPhoneNumberExt
					,SWWRegId
					,ContactPhoneCountryCode
					)
				VALUES (
					@sc_UserId --UserId
					,@sc_BranchId --,BranchId
					,NULL --,RoleId
					,@sc_ActivateStatusId_Active --,ActivateStatusId
					,@sc_UserBranchStatusId_Approve --,UserBranchStatusId
					,@sww_JobTitleID --,JobTitleId
					,@sww_OtherJobTitle --,OtherJobTitleName
					,@sww_ContactEmail --,ContactEmail
					,@sww_ContactPhoneNo --,ContactPhoneNumber
					,@sww_ContactPhoneExt --,ContactPhoneNumberExt
					,@sww_RegID --,SWWRegId
					,@sww_ContactPhoneCountryCode --,ContactPhoneCountryCode
					)
			END

			BEGIN
				INSERT INTO Temp_MigrateDataSWWAndSC
				SELECT @sww_RegID
					,@sc_SupplierId
					,@sc_BranchId
					,@sc_UserId
			END

			PRINT @sww_SupplierID
			PRINT '---END---'

			FETCH NEXT
			FROM cInsertSupplier
			INTO @sww_SupplierID
				,@sww_CountryCode
				,@sww_TaxID
				,@sww_CompanyTypeID
				,@sww_BusinessEntityID
				,@sww_OtherBusinessEntity
				,@sww_CompanyName_Local
				,@sww_CompanyName_Inter
				,@sww_BranchNo
				,@sww_BranchName_Local
				,@sww_BranchName_Inter
				,@sww_SupplierPhoneNo
				,@sww_SupplierPhoneExt
				,@sww_SupplierMobileNo
				,@sww_SupplierFaxNo
				,@sww_SupplierFaxExt
				,@sww_SupplierWebSite
				,@sww_YearEstablished
				,@sww_RegisteredCapital
				,@sww_CurrencyCode
				,@sww_IsPTVNVerified
				,@sww_IsAffiliateCP
				,@sww_LastUpdate
				,@sww_IsDeleted
		END

		CLOSE cInsertSupplier

		DEALLOCATE cInsertSupplier
	END

	--COMMIT
	--ROLLBACK
	SELECT *
	FROM [Temp_MappingDataSWWAndSC]
END
