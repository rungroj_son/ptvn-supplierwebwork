-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Piyavut>
-- Create date: <2020-12-02>
-- Description:	<Migrate Attachment from Supplier webwork to Supplier Connect>
-- =============================================
ALTER PROCEDURE [dbo].[SP_MigrateAttachmentSWWToSC]
	-- Add the parameters for the stored procedure here

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO RegistrationFile
	   (
		  [Name],
		  [Extension],
		  [Size],
		  [UniqueName],
		  [DocumentTypeLv1],
		  [DocumentTypeLv2],
		  [BranchId]
	  )
	SELECT	SC_Name,
			SC_Extension,
			SC_Size,
			SC_UniqueName,
			SC_DocumentTypeLv1,
      		SC_DocumentTypeLv2,
			(SELECT Top 1 SC_BranchId FROM Temp_MappingDataSWWAndSC WHERE SWW_RegId = RegID)
    FROM	[192.168.10.23].[SupplierPortal].[dbo].[Temp_RegMigrateAttachmentToSC] 
	WHERE	SC_DocumentTypeLV1 <> '' OR SC_DocumentTypeLV2 <> ''

END
GO
