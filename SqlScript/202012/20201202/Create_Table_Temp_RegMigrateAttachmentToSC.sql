USE [SupplierPortal]
GO

/****** Object:  Table [dbo].[Temp_RegMigrateAttachmentToSC]    Script Date: 12/2/2020 1:40:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Temp_RegMigrateAttachmentToSC](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RegId] [int] NULL,
	[AttachmentID] [int] NULL,
	[AttachmentName] [nvarchar](255) NULL,
	[AttachmentNameUnique] [nvarchar](255) NULL,
	[DocumentNameID] [int] NULL,
	[OtherDocument] [nvarchar](255) NULL,
	[SC_Name] [nvarchar](255) NULL,	
    [SC_Extension] [nvarchar](255) NULL,
    [SC_Size] [bigint] NULL,
    [SC_UniqueName] [bigint] NULL,
    [SC_DocumentTypeLv1] [nvarchar](255) NULL,
    [SC_DocumentTypeLv2] [nvarchar](255) NULL,
    [SC_BranchId] [int] NULL,
	[IsMigrateComplete] [bit] NULL,

 CONSTRAINT [PK_Tbl_Temp_RegMigrateAttachmentToSC] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


