﻿using SupplierPortal.WindowService;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Supplier.WindowService
{
    public partial class SupplierPortalService : ServiceBase
    {

        WriteLog writeLog;
        private static System.Timers.Timer timerExecution = null;
        private static System.Timers.Timer timerExecution_OPN = null;
        private static System.Timers.Timer timerExecution_SC = null;

        private static short _intInterval = 1;// 60 minutes
        private static short _intInterval_OPN = 1;// 60 minutes
        private static short _intInterval_SC = 1;// 60 minutes


        private static string _scheduledHour = string.Empty;
        private static string _resetPasswordURL = string.Empty;
        private static string _opnJobURL = string.Empty;
        private static string _scRegJobURL = string.Empty;

        public SupplierPortalService()
        {
            writeLog = new WriteLog();
            InitializeComponent();

            writeLog.WriteToFile("InitializeComponent " + DateTime.Now);

            timerExecution = new System.Timers.Timer(1000);
            timerExecution.Elapsed += new System.Timers.ElapsedEventHandler(OnTimerExecutionEvent);
            timerExecution.Enabled = false;

            timerExecution_OPN = new System.Timers.Timer(1000);
            timerExecution_OPN.Elapsed += new System.Timers.ElapsedEventHandler(OnTimerExecutionEvent_OPN);
            timerExecution_OPN.Enabled = false;

            timerExecution_SC = new System.Timers.Timer(1000);
            timerExecution_SC.Elapsed += new System.Timers.ElapsedEventHandler(OnTimerExecutionEvent_SC);
            timerExecution_SC.Enabled = false;
        }

        protected override void OnStart(string[] args)
        {
            ReadConfigurationSettings();

            timerExecution.Interval = _intInterval * 1000;
            timerExecution.Enabled = true;
            timerExecution.Start();

            timerExecution_OPN.Interval = _intInterval_OPN * 1000;
            timerExecution_OPN.Enabled = true;
            timerExecution_OPN.Start();

            timerExecution_SC.Interval = _intInterval_SC * 1000;
            timerExecution_SC.Enabled = true;
            timerExecution_SC.Start();

            writeLog.WriteToFile("Service Start " + DateTime.Now);
        }

        protected override void OnStop()
        {
            timerExecution.Enabled = false;
            timerExecution.Stop();

            timerExecution_OPN.Enabled = false;
            timerExecution_OPN.Stop();

            timerExecution_SC.Enabled = false;
            timerExecution_SC.Stop();

            writeLog.WriteToFile("Service Stop " + DateTime.Now);
        }

        protected override void OnPause()
        {
            timerExecution.Enabled = false;
            timerExecution.Stop();

            timerExecution_OPN.Enabled = false;
            timerExecution_OPN.Stop();

            timerExecution_SC.Enabled = false;
            timerExecution_SC.Stop();

            writeLog.WriteToFile("Service Pause " + DateTime.Now);
        }

        protected override void OnContinue()
        {
            ReadConfigurationSettings();

            timerExecution.Interval = _intInterval * 1000;
            timerExecution.Enabled = true;
            timerExecution.Start();

            timerExecution_OPN.Interval = _intInterval_OPN * 1000;
            timerExecution_OPN.Enabled = true;
            timerExecution_OPN.Start();

            timerExecution_SC.Interval = _intInterval_SC * 1000;
            timerExecution_SC.Enabled = true;
            timerExecution_SC.Start();

            writeLog.WriteToFile("Service Countinue " + DateTime.Now);
        }

        #region Execution
        private async void OnTimerExecutionEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            bool result = false;
            try
            {
                timerExecution.Stop();

                result = await CheckStatusSendMailWelcome();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (!timerExecution.Enabled)
                    timerExecution.Enabled = true;

                timerExecution.Start();
            }
        }

        private async void OnTimerExecutionEvent_OPN(object sender, System.Timers.ElapsedEventArgs e)
        {
            bool result = false;
            try
            {
                timerExecution_OPN.Stop();

                result = await UpdateBuyerSupplierMapping();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (!timerExecution_OPN.Enabled)
                    timerExecution_OPN.Enabled = true;

                timerExecution_OPN.Start();
            }
        }

        private async void OnTimerExecutionEvent_SC(object sender, System.Timers.ElapsedEventArgs e)
        {
            bool result = false;
            try
            {
                timerExecution_SC.Stop();

                result = await UpdateRegisterSupplierAndRequestToJoin();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (!timerExecution_SC.Enabled)
                    timerExecution_SC.Enabled = true;

                timerExecution_SC.Start();
            }
        }
        #endregion Execution

        #region Api Connectiuon
        public async Task<HttpResponseMessage> GetApiConnection(string url)
        {
            HttpClient client = new HttpClient();
            try
            {
                string languageURL = string.Empty;
                string baseURL = ConfigurationManager.AppSettings["baseURL"];

                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(baseURL + url);
                return response;
            }
            catch (Exception ex)
            {
                writeLog.WriteToFile("APIConnectionService Get(Error) " + DateTime.Now + " " + ex.Message);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        #endregion Api Connectiuon

        #region Function Call Api
        public async Task<SendEmailStatus> SendMailWelcome()
        {
            SendEmailStatus result = null;
            HttpResponseMessage httpResponse = null;
            try
            {
                httpResponse = await this.GetApiConnection(_resetPasswordURL);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = new SendEmailStatus();
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    //writeLog.WriteToFile("SendMailWelcome(Success) " + DateTime.Now + " " + jsonResult);
                    result = JsonConvert.Deserialize<SendEmailStatus>(jsonResult);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();
            }

            return result;
        }

        public async Task<bool> UpdateSupplierBuyerOneplanet()
        {
            bool result = false;
            HttpResponseMessage httpResponse = null;
            try
            {

                httpResponse = await this.GetApiConnection(_opnJobURL);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    //writeLog.WriteToFile("SendMailWelcome(Success) " + DateTime.Now + " " + jsonResult);
                    result = JsonConvert.Deserialize<bool>(jsonResult);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();
            }

            return result;
        }

        public async Task<bool> CallRegisterSupplierAndRequestToJoin()
        {
            bool result = false;
            HttpResponseMessage httpResponse = null;
            try
            {
                httpResponse = await this.GetApiConnection(_scRegJobURL);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    result = true;
                }          
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();
            }

            return result;
        }
        #endregion Function Call Api

        #region Function
        public async Task<bool> CheckStatusSendMailWelcome()
        {
            bool result = false;
            HttpResponseMessage httpResponse = null;
            SendEmailStatus status = null;
            try
            {
                status = new SendEmailStatus();
                status = await SendMailWelcome();

                if (status.Success)
                {
                    result = await this.CheckStatusSendMailWelcome();
                    writeLog.WriteToFile("Send Email (Success) " + DateTime.Now);
                }
            }
            catch (Exception ex)
            {
                writeLog.WriteToFile("CheckStatusSendMailWelcome (Error) " + DateTime.Now + " " + ex.Message);
                throw ex;
            }
            finally
            {
                if (httpResponse != null)
                    httpResponse.Dispose();

                status = null;
            }

            return result;
        }

        public async Task<bool> UpdateBuyerSupplierMapping()
        {
            bool result = false;

            try
            {
                result = await UpdateSupplierBuyerOneplanet();
                writeLog.WriteToFile("UpdateSupplierBuyerOneplanet (Success) " + DateTime.Now);
            }
            catch (Exception ex)
            {
                writeLog.WriteToFile("UpdateSupplierBuyerOneplanet (Error) " + DateTime.Now + " " + ex.Message);
                throw ex;
            }

            return result;
        }

        public async Task<bool> UpdateRegisterSupplierAndRequestToJoin()
        {
            bool result = false;

            try
            {
                result = await CallRegisterSupplierAndRequestToJoin();
                writeLog.WriteToFile("UpdateRegisterSupplierAndRequestToJoin (Success) " + DateTime.Now);
            }
            catch (Exception ex)
            {
                writeLog.WriteToFile("UpdateRegisterSupplierAndRequestToJoin (Error) " + DateTime.Now + " " + ex.Message);
                throw ex;
            }

            return result;
        }
        #endregion Function

        #region ReadConfigurationSettings
        private static void ReadConfigurationSettings()
        {
            _intInterval = Convert.ToInt16(ConfigurationManager.AppSettings["interval_int"]);
            _intInterval_OPN = Convert.ToInt16(ConfigurationManager.AppSettings["interval_opn_int"]);
            _intInterval_SC = Convert.ToInt16(ConfigurationManager.AppSettings["interval_sc_reg_int"]);

            _scheduledHour = ConfigurationManager.AppSettings["scheduled_hour"];
            _resetPasswordURL = ConfigurationManager.AppSettings["reset_password_endpoint"];
            _opnJobURL = ConfigurationManager.AppSettings["opn_job_endpoint"];
            _scRegJobURL = ConfigurationManager.AppSettings["sc_reg_job_endpoint"];
        }
        #endregion ReadConfigurationSettings

        public class SendEmailStatus
        {
            public Boolean Success { get; set; }
            public string Message { get; set; }
        }
    }
}
