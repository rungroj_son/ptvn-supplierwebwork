﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.LogSDFavoriteRepository
{
    public interface ILogSDFavoriteRepository
    {
        #region Insert
        void Insert(int id, int updateBy, string logAction);
        #endregion
    }
}
