﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.DataAccess.PDPA
{
    public partial interface IUserPDPARepository
    {
        Users_PDPA GetUserPDPA(string username);
        int InsertUserPDPA(Users_PDPA users);

        bool UpdateUserPDPA(Users_PDPA users);
        void Dispose();
    }
}
