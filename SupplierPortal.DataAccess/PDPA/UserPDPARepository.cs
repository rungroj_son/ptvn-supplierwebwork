﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.DataAccess.PDPA
{
    public partial class UserPDPARepository : BaseRepository, IUserPDPARepository
    {

        #region GET
        public Users_PDPA GetUserPDPA(string username)
        {
            Users_PDPA result;
            try
            {
                result = new Users_PDPA();
                result = context.Users_PDPA.FirstOrDefault(w => w.Username == username);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #endregion

        #region INERT
        public int InsertUserPDPA(Users_PDPA users)
        {
            int result = 0;
            try
            {
                var contactTemp = this.context.Users_PDPA.Add(users);
                this.context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #endregion

        #region INERT
        public bool UpdateUserPDPA(Users_PDPA users)
        {
            bool result = false;
            try
            {
                var contactTemp = this.context.Users_PDPA.Find(users.Id);
                contactTemp.IsAcceptTerm = true;
                contactTemp.IsCheckConsent = true;
                this.context.SaveChanges();

                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #endregion

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
