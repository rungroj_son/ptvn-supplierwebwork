﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.RegFromConnect
{
    public interface IRegFromConnectRepository
    {
        Tbl_RegFromConnect GetByRegId(int regId);
        List<Tbl_RegFromConnect> GetAllByIsJobCompleted(bool isCompleted);
        int Insert(Tbl_RegFromConnect regFromConnect);
        bool Update(Tbl_RegFromConnect regFromConnect);
    }
}
