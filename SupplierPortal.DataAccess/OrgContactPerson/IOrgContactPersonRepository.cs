﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.OrgContactPerson
{
    public partial interface IOrgContactPersonRepository
    {
        #region INSERT
        int InsertOrgContactPerson(Tbl_OrgContactPerson tbl_OrgContactPerson);

        int InsertLogOrgContactPerson(Tbl_LogOrgContactPerson tbl_LogOrgContactPerson);
        #endregion

        #region UPDATE

        Boolean UpdatePrimaryOrgContactPersonToDefault(int supplierId);
        #endregion

        #region DELETE
        Boolean RemoveOrgContactPerson(Tbl_OrgContactPerson tbl_OrgContactPerson);

        #endregion

        #region GET
        Tbl_OrgContactPerson GetOrgContactPersonBySupplierContactID(int supplierID, int contactID);
        #endregion

        void Dispose();
    }
}
