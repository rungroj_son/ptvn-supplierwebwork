﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.Buyer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.OrgContactPerson
{
    public partial class OrgContactPersonRepository : BaseRepository, IOrgContactPersonRepository
    {
        #region INERT
        public int InsertOrgContactPerson(Tbl_OrgContactPerson tbl_OrgContactPerson)
        {
            int result = 0;
            try
            {
                if (tbl_OrgContactPerson != null)
                {
                    var contactTemp = this.context.Tbl_OrgContactPerson.Add(tbl_OrgContactPerson);
                    this.context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public int InsertLogOrgContactPerson(Tbl_LogOrgContactPerson tbl_LogOrgContactPerson)
        {
            int result = 0;
            try
            {
                if (tbl_LogOrgContactPerson != null)
                {
                    var contactTemp = this.context.Tbl_LogOrgContactPerson.Add(tbl_LogOrgContactPerson);
                    this.context.SaveChanges();

                    result = tbl_LogOrgContactPerson.LogID;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        #endregion

        #region UPDATE
        public Boolean UpdatePrimaryOrgContactPersonToDefault(int supplierId)
        {
            try
            {
                List<Tbl_OrgContactPerson> query = this.context.Tbl_OrgContactPerson.Where(orgc => orgc.SupplierID == supplierId).ToList();
                if (query.Any())
                {
                    query.ForEach(a => a.isPrimaryContact = 0);
                    this.context.SaveChanges();
                }

                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DELETE
        public Boolean RemoveOrgContactPerson(Tbl_OrgContactPerson tbl_OrgContactPerson)
        {
            bool result = false;
            try
            {
                if (tbl_OrgContactPerson != null)
                {
                    var contactTemp = this.context.Tbl_OrgContactPerson.Remove(tbl_OrgContactPerson);
                    this.context.SaveChanges();

                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        #endregion

        #region GET
        public Tbl_OrgContactPerson GetOrgContactPersonBySupplierContactID(int supplierID, int contactID)
        {
            Tbl_OrgContactPerson tbl_OrgContactPerson = null;
            try
            {
                tbl_OrgContactPerson = this.context.Tbl_OrgContactPerson.Where(w => w.SupplierID == supplierID && w.ContactID == contactID).FirstOrDefault();
            }
            catch (Exception ex)
            {

            }
            return tbl_OrgContactPerson;
        }
        #endregion

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
