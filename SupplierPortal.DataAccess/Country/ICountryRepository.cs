﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.Country
{
    public interface ICountryRepository
    {
        Tbl_Country GetCountry(string countryCode);
    }
}
