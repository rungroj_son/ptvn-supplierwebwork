﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.Buyer;
using SupplierPortal.Models.Dto;
using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.ContactPerson
{
    public partial class ContactPersonRepository : BaseRepository, IContactPersonRepository
    {
        #region INSERT
        public int Insert(Tbl_ContactPerson tbl_ContactPerson)
        {
            int result = 0;
            try
            {
                if (tbl_ContactPerson != null)
                {
                    this.context.Tbl_ContactPerson.Add(tbl_ContactPerson);
                    this.context.SaveChanges();

                    result = tbl_ContactPerson.ContactID;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public int InsertLogContactPerson(Tbl_LogContactPerson tbl_LogContactPerson)
        {
            int result = 0;
            try
            {
                if (tbl_LogContactPerson != null)
                {
                    var contactTemp = this.context.Tbl_LogContactPerson.Add(tbl_LogContactPerson);
                    this.context.SaveChanges();

                    result = tbl_LogContactPerson.LogID;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        #endregion

        #region UPDATE
        public Boolean Update(Tbl_ContactPerson tbl_ContactPerson)
        {
            bool result = false;
            try
            {
                if (tbl_ContactPerson != null)
                {
                    var contactTemp = this.context.Tbl_ContactPerson.Find(tbl_ContactPerson.ContactID);
                    contactTemp = tbl_ContactPerson;
                    this.context.SaveChanges();

                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        #endregion

        #region DELTE

        #endregion

        #region GET

        public Tbl_ContactPerson GetContactPersonByContactId(int contactId)
        {
            Tbl_ContactPerson tbl_ContactPerson = null;
            try
            {
                if (contactId > 0)
                {

                    tbl_ContactPerson = new Tbl_ContactPerson();
                    tbl_ContactPerson = this.context.Tbl_ContactPerson.Where(w => w.ContactID == contactId).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

            }
            return tbl_ContactPerson;
        }

        public List<ContactWithPagination_Sel_Result> GetContactWithPagination(int pageIndex, int pageSize, string search, string sorting)
        {
            List<ContactWithPagination_Sel_Result> result = null;
            try
            {
                result = new List<ContactWithPagination_Sel_Result>();
                result = context.ContactWithPagination_Sel(pageIndex, pageSize, search, sorting).ToList();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public Tuple<List<Tbl_ContactPersonDto>, int, int> GetUserContactPersonsOfOrganization(int supplierId, DatatableRequest request)
        {
            List<Tbl_ContactPersonDto> results = null;
            int filteredResultsCount = 0;
            int totalResultsCount = 0;
            try
            {
                var queryData = from org in this.context.Tbl_Organization
                                join u in this.context.Tbl_User on org.SupplierID equals u.SupplierID
                                join cp in this.context.Tbl_ContactPerson on u.ContactID equals cp.ContactID
                                where org.SupplierID == supplierId
                                && org.isDeleted == 0
                                && u.IsDeleted == 0
                                && u.IsDisabled == 0
                                && u.IsActivated == 1
                                && cp.isDeleted == 0
                                select new Tbl_ContactPersonDto
                                {
                                    ContactID = cp.ContactID,
                                    TitleID = cp.TitleID,
                                    FirstName_Local = cp.FirstName_Local,
                                    FirstName_Inter = cp.FirstName_Inter,
                                    LastName_Local = cp.LastName_Local,
                                    LastName_Inter = cp.LastName_Inter,
                                    JobTitleID = cp.JobTitleID,
                                    OtherJobTitle = cp.OtherJobTitle,
                                    Department = cp.Department,
                                    PhoneCountryCode = cp.PhoneCountryCode,
                                    PhoneNo = cp.PhoneNo,
                                    PhoneExt = cp.PhoneExt,
                                    MobileCountryCode = cp.MobileCountryCode,
                                    MobileNo = cp.MobileNo,
                                    FaxNo = cp.FaxNo,
                                    FaxExt = cp.FaxExt,
                                    Email = cp.Email,
                                    isDeleted = cp.isDeleted,
                                    Tbl_JobTitle = cp.Tbl_JobTitle == null ? null : new Tbl_JobTitleDto
                                    {
                                        JobTitleID = cp.Tbl_JobTitle.JobTitleID,
                                        JobTitleName = cp.Tbl_JobTitle.JobTitleName,
                                        SeqNo = cp.Tbl_JobTitle.SeqNo
                                    },
                                    Tbl_NameTitle = cp.Tbl_NameTitle == null ? null : new Tbl_NameTitleDto
                                    {
                                        TitleID = cp.Tbl_NameTitle.TitleID,
                                        TitleName = cp.Tbl_NameTitle.TitleName,
                                        SeqNo = cp.Tbl_NameTitle.SeqNo
                                    },

                                };
                totalResultsCount = queryData.Count();
                if (supplierId > 0 && request.length == -1)
                {
                    // select All
                    results = queryData.OrderBy(i => i.ContactID).Skip(request.start).ToList();
                    filteredResultsCount = queryData.Count();
                }
                else
                {
                    // Paging
                    results = queryData.OrderBy(i => i.ContactID).Skip(request.start).Take(request.length).ToList();
                    filteredResultsCount = queryData.Count();
                }


            }
            catch (Exception ex)
            {

            }
            return new Tuple<List<Tbl_ContactPersonDto>, int, int>(results, filteredResultsCount, totalResultsCount);
        }

        public bool CheckEmailContactPersionDuplicate(string email)
        {
            return this.context.Tbl_ContactPerson.Any(a => a.Email.ToLower().Equals(email.ToLower()));
        }
        #endregion



    }
}
