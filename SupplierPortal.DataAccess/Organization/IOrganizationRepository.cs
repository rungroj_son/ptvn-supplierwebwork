﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.Organization
{
    public interface IOrganizationRepository
    {
        Tbl_Organization GetOrganization(int supplierId);
        Tbl_Organization GetOrganization(string taxid, string branchNo);
        List<Tbl_Organization> GetOrganizationByTaxIdAndCountryCode(string taxid, string countryCode);

        List<OrganizationWithPagination_Sel_Result> GetOrganizationWithPagination(int pageIndex, int pageSize, string search, string sorting);
    }
}
