﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.RegMigrateAttachmentToSC
{
    public class RegMigrateAttachmentToSCRepository : BaseRepository, IRegMigrateAttachmentToSCRepository
    {
        #region Get
        public List<Temp_RegMigrateAttachmentToSC> FindAll()
        {
            List<Temp_RegMigrateAttachmentToSC> result = null;
            try
            {
                result = new List<Temp_RegMigrateAttachmentToSC>();
                result = context.Temp_RegMigrateAttachmentToSC.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public List<Temp_RegMigrateAttachmentToSC> FindByIsMigrateComplete(bool isMigrateComplete)
        {
            List<Temp_RegMigrateAttachmentToSC> result = null;
            try
            {
                result = new List<Temp_RegMigrateAttachmentToSC>();
                result = context.Temp_RegMigrateAttachmentToSC.Where(w => w.IsMigrateComplete == isMigrateComplete).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion Get

        #region Insert
        public int Insert(Temp_RegMigrateAttachmentToSC attachmentToSC)
        {
            int result = 0;
            try
            {
                context.Temp_RegMigrateAttachmentToSC.Add(attachmentToSC);
                context.SaveChanges();
                result = attachmentToSC.Id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion Insert

        #region Update
        public bool Update(Temp_RegMigrateAttachmentToSC attachmentToSC)
        {
            bool result = false;
            try
            {
                if (attachmentToSC != null)
                {
                    var temp = context.Temp_RegMigrateAttachmentToSC.Find(attachmentToSC.Id);
                    temp = attachmentToSC;
                    context.SaveChanges();

                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion Update
    }
}
