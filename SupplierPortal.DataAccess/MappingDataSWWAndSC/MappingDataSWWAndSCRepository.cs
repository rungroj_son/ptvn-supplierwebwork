﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.MappingDataSWWAndSC
{
    public class MappingDataSWWAndSCRepository : BaseRepository, IMappingDataSWWAndSCRepository
    {
        public List<Temp_MappingDataSWWAndSC> FindAll()
        {
            List<Temp_MappingDataSWWAndSC> result = new List<Temp_MappingDataSWWAndSC>();
            try
            {
                result = this.context.Temp_MappingDataSWWAndSC.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
    }
}
