﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.BusinessType
{
    public interface IBusinessTypeRepository 
    {
        #region GET
        Tbl_BusinessType GetBusinessType(int businessTypeId);
        #endregion
    }
}
