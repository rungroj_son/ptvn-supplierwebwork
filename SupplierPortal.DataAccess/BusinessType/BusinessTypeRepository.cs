﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.DataAccess.BusinessType
{
    public class BusinessTypeRepository : BaseRepository, IBusinessTypeRepository
    {
        #region GET
        public Tbl_BusinessType GetBusinessType(int businessTypeId)
        {
            try
            {
                Tbl_BusinessType result = context.Tbl_BusinessType.Where(i => i.BusinessTypeID == businessTypeId).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
