﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.RegApprove
{
    public class RegApproveRepository : BaseRepository, IRegApproveRepository
    {
        #region GET
        public Tbl_RegApprove GetRegApproveByRegID(int regID)
        {
            Tbl_RegApprove result = null;
            try
            {
                result = new Tbl_RegApprove();
                result = context.Tbl_RegApprove.Where(i => i.RegID == regID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion GET
    }
}
