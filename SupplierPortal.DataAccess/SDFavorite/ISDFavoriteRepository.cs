﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.SDFavorite
{
    public interface ISDFavoriteRepository
    {
        #region GET
        IEnumerable<Tbl_SDFavorite> GetSDFavoriteByEID(int EID, string sysUserId);
        List<Core_SupplierContactFavorite_Result> GetSDFavorites(int eid, int supplierId, int sysUserId);
        Tbl_SDFavorite GetSDFavorite(string sysUserID, int EID, int userID);
        #endregion

        #region Insert
        int Insert(Tbl_SDFavorite tbl_SDFavorite, int updateBy);
        int Delete(int id, int updateBy);
        #endregion

    }
}
