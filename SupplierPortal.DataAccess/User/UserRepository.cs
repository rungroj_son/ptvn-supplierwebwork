﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.UAA;
using SupplierPortal.DataAccess.Buyer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.User
{
    public partial class UserRepository : BaseRepository, IUserRepository
    {

        #region INSERT
        public int InsertLogUser(Tbl_LogUser tbl_LogUser)
        {
            int result = 0;
            try
            {
                if (tbl_LogUser != null)
                {
                    this.context.Tbl_LogUser.Add(tbl_LogUser);
                    this.context.SaveChanges();

                    result = (int)tbl_LogUser.UserID;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public int InsertUser(Tbl_User tbl_User)
        {
            int result = 0;
            try
            {
                if (tbl_User != null)
                {
                    this.context.Tbl_User.Add(tbl_User);
                    this.context.SaveChanges();

                    result = tbl_User.UserID;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }
        #endregion

        #region UPDATE
        public Boolean UpdateUser(Tbl_User tbl_User)
        {
            bool result = false;
            try
            {
                if (tbl_User != null)
                {
                    var temp = this.context.Tbl_User.Find(tbl_User.UserID);
                    temp = tbl_User;
                    this.context.SaveChanges();

                    result = true;
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public bool UpdateUserNotifyChangePassword(string pSystemUserID, string pBuyerEID, string pErfx_id)
        {
            try
            {
                using (var ctx = new SupplierPortalEntities())
                {
                    using (var cmd = ctx.Database.Connection.CreateCommand())
                    {
                        ctx.Database.Connection.Open();
                        cmd.CommandText = "EXEC UpdateUser_Notify_ChangePassword '" + pSystemUserID + "', '" + pBuyerEID + "', '" + pErfx_id + "'";
                        //cmd.Parameters.AddWithValue("@pSystemUserID", pSystemUserID);
                        //cmd.Parameters.AddWithValue("@pBuyerEID", pBuyerEID);
                        //cmd.Parameters.AddWithValue("@pErfx_id", pErfx_id);
                        //cmd.ExecuteNonQuery();
                        cmd.ExecuteReader();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region DELETE

        #endregion

        #region GET
        public Tbl_User GetUserByUserId(int userId)
        {
            Tbl_User user = null;
            try
            {
                if (userId > 0)
                {
                    user = this.context.Tbl_User.Where(w => w.UserID == userId).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return user;
        }
        public Tbl_User GetUserByUsername(string username)
        {
            Tbl_User user = null;
            try
            {
                user = this.context.Tbl_User.Where(w => w.Username == username).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return user;
        }
        public Gen_Portal_UserPassword_Result GetRandomSaltAndPassword()
        {
            try
            {
                Gen_Portal_UserPassword_Result result = context.Gen_Portal_UserPassword_Result().FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<GetUser_Notify_ChangePassword_Result> GetUserNotifyChangePassword()
        {
            try
            {
                List<GetUser_Notify_ChangePassword_Result> result = context.GetUser_Notify_ChangePassword_Result().ToList();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UAAOuthResponse GetUserForUAAByUsername(string username)
        {
            UAAOuthResponse result = null;
            try
            {

                var data = from user in context.Tbl_User
                           join userrole in context.Tbl_UserRole on user.UserID equals userrole.UserID
                           join aclrole in context.Tbl_ACL_Role on userrole.RoleID equals aclrole.RoleID
                           where user.Username.Equals(username)
                           select new UAAOuthResponse
                           {
                               username = user.Username,
                               password = user.Password,
                               salt = user.Salt,
                               tenantid = user.OrgID,
                               rolename = aclrole.RoleName
                           };

                result = data.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #endregion

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
