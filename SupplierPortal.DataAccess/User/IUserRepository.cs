﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.UAA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.User
{
    public partial interface IUserRepository
    {
        #region INSERT
        int InsertLogUser(Tbl_LogUser tbl_LogUser);

        int InsertUser(Tbl_User tbl_User);
        #endregion

        #region UPDATE
        Boolean UpdateUser(Tbl_User tbl_User);
        bool UpdateUserNotifyChangePassword(string pSystemUserID, string pBuyerEID, string pErfx_id);
        #endregion

        #region DELETE

        #endregion

        #region GET
        Tbl_User GetUserByUserId(int userId);

        Tbl_User GetUserByUsername(string username);

        Gen_Portal_UserPassword_Result GetRandomSaltAndPassword();

        List<GetUser_Notify_ChangePassword_Result> GetUserNotifyChangePassword();

        UAAOuthResponse GetUserForUAAByUsername(string username);
        #endregion

        void Dispose();
    }
}
