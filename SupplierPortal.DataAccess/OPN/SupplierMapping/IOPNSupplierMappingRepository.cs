﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.OPN.SupplierMapping
{
    public interface IOPNSupplierMappingRepository
    {
        #region GET
        Tbl_OPNSupplierMapping GetOPNBuyerSupplierMappingByRegID(int regID);

        List<Tbl_OPNBuyerSupplierMapping> GetOPNBuyerSupplierMappingByIsJobSuccess(bool isJobSuccess);
        #endregion

        #region INSERT
        int InsertOPNSupplierMapping(Tbl_OPNSupplierMapping tbl_OPNSupplierMapping);
        #endregion

        #region UPDATE
        bool UpdateOPNSupplierMapping(Tbl_OPNSupplierMapping supplierMapping);
        #endregion
    }
}
