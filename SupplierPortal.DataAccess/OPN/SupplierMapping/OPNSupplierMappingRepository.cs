﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.OPN.SupplierMapping
{
    public class OPNSupplierMappingRepository: BaseRepository, IOPNSupplierMappingRepository
    {
        #region GET

        public Tbl_OPNSupplierMapping GetOPNBuyerSupplierMappingByRegID(int regID)
        {
            Tbl_OPNSupplierMapping result = null;
            try
            {
                result = new Tbl_OPNSupplierMapping();
                result = context.Tbl_OPNSupplierMapping.Where(f => f.RegID == regID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public List<Tbl_OPNBuyerSupplierMapping> GetOPNBuyerSupplierMappingByIsJobSuccess(bool isJobSuccess)
        {
            List<Tbl_OPNBuyerSupplierMapping> result = null;
            try
            {
                result = new List<Tbl_OPNBuyerSupplierMapping>();
                result = context.Tbl_OPNBuyerSupplierMapping.Where(i => i.IsJobSuccess == isJobSuccess).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion GET

        #region INSERT
        public int InsertOPNSupplierMapping(Tbl_OPNSupplierMapping tbl_OPNSupplierMapping)
        {
            try
            {
                this.context.Tbl_OPNSupplierMapping.Add(tbl_OPNSupplierMapping);
                this.context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tbl_OPNSupplierMapping.ID;
        }
        #endregion

        #region UPDATE
        public bool UpdateOPNSupplierMapping(Tbl_OPNSupplierMapping supplierMapping)
        {
            bool result = false;
            try
            {
                if (supplierMapping != null)
                {
                    var temp = this.context.Tbl_OPNSupplierMapping.Find(supplierMapping.ID);
                    temp = supplierMapping;
                    this.context.SaveChanges();

                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion UPDATE
    }
}
