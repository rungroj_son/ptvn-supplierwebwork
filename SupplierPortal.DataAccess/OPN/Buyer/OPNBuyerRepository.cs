﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.OPN.Buyer
{
    public class OPNBuyerRepository : BaseRepository, IOPNBuyerRepository
    {

        #region GET
        public Tbl_OPNBuyer getOPNBuyer(string invitationCode)
        {
            Tbl_OPNBuyer result = null;
            try
            {
                result = this.context.Tbl_OPNBuyer.Where(i => i.InvitationCode == invitationCode).FirstOrDefault();
               
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        public Tbl_OPNBuyer getOPNBuyer(int buyerId)
        {
            Tbl_OPNBuyer result = null;
            try
            {
                result = this.context.Tbl_OPNBuyer.Where(i => i.BuyerID == buyerId).FirstOrDefault();

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        #endregion
    }
}
