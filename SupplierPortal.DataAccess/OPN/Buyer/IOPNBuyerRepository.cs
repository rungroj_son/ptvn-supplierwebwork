﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.OPN.Buyer
{
    public interface IOPNBuyerRepository
    {
        #region INSERT
        Tbl_OPNBuyer getOPNBuyer(string invitationCode);
        Tbl_OPNBuyer getOPNBuyer(int buyerId);
        #endregion
    }
}
