﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.OPN.BuyerSupplierMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.OPN.BuyerSupplierMapping
{
    public class OPNBuyerSupplierMappingRepository : BaseRepository, IOPNBuyerSupplierMappingRepository
    {

        #region GET
        public Tbl_OPNBuyerSupplierMapping GetOPNBuyerSupplierMappingByRegID(int regID)
        {
            Tbl_OPNBuyerSupplierMapping result = null;
            try
            {
                result = new Tbl_OPNBuyerSupplierMapping();
                result = context.Tbl_OPNBuyerSupplierMapping.Where(i => i.RegID == regID).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public List<Tbl_OPNBuyerSupplierMapping> GetOPNBuyerSupplierMappingByIsJobSuccess(bool isJobSuccess)
        {
            List<Tbl_OPNBuyerSupplierMapping> result = null;
            try
            {
                result = new List<Tbl_OPNBuyerSupplierMapping>();
                result = context.Tbl_OPNBuyerSupplierMapping.Where(i => i.IsJobSuccess == isJobSuccess).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public List<Tbl_OPNBuyerSupplierMapping> GetOPNBuyerSupplierMappingBySupplierId(int supplierId)
        {
            List<Tbl_OPNBuyerSupplierMapping> result = null;
            try
            {
                result = new List<Tbl_OPNBuyerSupplierMapping>();
                result = context.Tbl_OPNBuyerSupplierMapping.Where(i => i.SupplierID == supplierId).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion GET

        #region UPDATE
        public bool UpdateOPNBuyerSupplierMapping(Tbl_OPNBuyerSupplierMapping supplierMapping)
        {
            bool result = false;
            try
            {
                if (supplierMapping != null)
                {
                    var temp = this.context.Tbl_OPNBuyerSupplierMapping.Find(supplierMapping.ID);
                    temp = supplierMapping;
                    this.context.SaveChanges();

                    result = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion UPDATE

        #region INSERT 
        public int InsertOPNBuyerSupplierMapping(Tbl_OPNBuyerSupplierMapping tbl_OPNBuyerSupplierMapping)
        {
            bool result = false;
            try
            {
                this.context.Tbl_OPNBuyerSupplierMapping.Add(tbl_OPNBuyerSupplierMapping);
                this.context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return tbl_OPNBuyerSupplierMapping.ID;
        }
        #endregion
    }
}
