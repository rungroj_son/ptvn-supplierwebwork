﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.BuyerConfig
{
    public interface IBuyerConfigRepository
    {
        #region GET
        Tbl_BuyerConfig GetBuyerConfig(string section, string name, int EID);
        #endregion
    }
}
