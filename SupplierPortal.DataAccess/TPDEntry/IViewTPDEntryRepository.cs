﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.TPDEntry
{
    public partial interface IViewTPDEntryRepository
    {
        ViewTPDEntry GetTPDEntryBytpOrgId(string tpOrgId);
    }
}
