﻿using SupplierPortal.Data.CustomModels.BuyerSuppliers;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.Language;
using SupplierPortal.Models.Dto.DataContract;
using SupplierPortal.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SupplierPortal.DataAccess.Buyer
{
    public partial class BuyerRepository : BaseRepository, IBuyerRepository
    {
        private ILanguageRepository _languageRepository;
        SupplierPortal.Models.Enums.EnumHelper enumhelper;
        public BuyerRepository()
        {
            _languageRepository = new LanguageRepository();
            enumhelper = new SupplierPortal.Models.Enums.EnumHelper();
        }

        #region BuyerSupplier

        #region INSERT

        public int InsertBuyerUser(Tbl_BuyerUser tbl_BuyerUser)
        {
            int result = 0;
            try
            {
                if (tbl_BuyerUser != null)
                {
                    this.context.Tbl_BuyerUser.Add(tbl_BuyerUser);
                    this.context.SaveChanges();

                    result = tbl_BuyerUser.SysUserID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        #endregion

        #region GET
        public Tuple<List<Buyer_Suppliers_Result>, int, int> GetSuppliers(GetBuyerSuppliersRequest request)
        {
            // Searching 
            List<string> condition = new List<string>();
            try
            {
                if (request.columns.Any())
                {
                    //Company Name
                    if (!string.IsNullOrEmpty(request.columns[0].search.value))
                    {
                        string input = request.columns[0].search.value;
                        string output = input.Substring(input.Length - 1, 1);
                        string searchValue = request.columns[0].search.value;

                        if (output == "*")
                        {
                            searchValue = searchValue.Remove(searchValue.Length - 1, 1); // remove * in last index of string
                            searchValue = searchValue.Trim();
                            condition.Add("TaxID like '" + searchValue + "%'");
                        }
                        else
                        {
                            searchValue = searchValue.Trim();
                            condition.Add("TaxID like '%" + searchValue + "%'");
                        }
                    }
                    //Company Name
                    if (!string.IsNullOrEmpty(request.columns[1].search.value))
                    {
                        string input = request.columns[1].search.value;
                        string output = input.Substring(input.Length - 1, 1);
                        string searchValue = request.columns[1].search.value;

                        if (output == "*")
                        {
                            searchValue = searchValue.Remove(searchValue.Length - 1, 1); // remove * in last index of string
                            searchValue = searchValue.Trim();
                            condition.Add("CompanyName like '" + searchValue + "%'");
                        }
                        else
                        {
                            searchValue = searchValue.Trim();
                            condition.Add("CompanyName like '%" + searchValue + "%'");
                        }
                    }
                    // Contact Name
                    if (!string.IsNullOrEmpty(request.columns[2].search.value))
                    {
                        string input = request.columns[2].search.value;
                        string output = input.Substring(input.Length - 1, 1);
                        string searchValue = request.columns[2].search.value;

                        if (output == "*")
                        {
                            searchValue = searchValue.Remove(searchValue.Length - 1, 1); // remove * in last index of string
                            searchValue = searchValue.Trim();
                            condition.Add("Full_Name like '" + searchValue + "%'");
                        }
                        else
                        {
                            searchValue = searchValue.Trim();
                            condition.Add("Full_Name like '%" + searchValue + "%'");
                        }
                    }
                    //// Username
                    //if (!string.IsNullOrEmpty(request.columns[3].search.value))
                    //{
                    //    condition.Add("Username like '%" + request.columns[3].search.value + "%'");
                    //}
                    // Email
                    if (!string.IsNullOrEmpty(request.columns[3].search.value))
                    {
                        string input = request.columns[3].search.value;
                        string output = input.Substring(input.Length - 1, 1);
                        string searchValue = request.columns[3].search.value;

                        if (output == "*")
                        {
                            searchValue = searchValue.Remove(searchValue.Length - 1, 1); // remove * in last index of string
                            searchValue = searchValue.Trim();
                            condition.Add("Email like '" + searchValue + "%'");
                        }
                        else
                        {
                            searchValue = searchValue.Trim();
                            condition.Add("Email like '%" + searchValue + "%'");
                        }
                    }
                    // Tel
                    if (!string.IsNullOrEmpty(request.columns[4].search.value))
                    {
                        string input = request.columns[4].search.value;
                        string output = input.Substring(input.Length - 1, 1);
                        string searchValue = request.columns[4].search.value;

                        if (output == "*")
                        {
                            searchValue = searchValue.Remove(searchValue.Length - 1, 1); // remove * in last index of string
                            searchValue = searchValue.Trim();
                            condition.Add("PhoneNo like '" + searchValue + "%'");
                        }
                        else
                        {
                            searchValue = searchValue.Trim();
                            condition.Add("PhoneNo like '%" + searchValue + "%'");
                        }
                    }
                }
                string sqlTxt = "";
                if (condition.Any())
                {
                    sqlTxt = String.Join(" AND ", condition);
                    sqlTxt = "AND " + sqlTxt;
                }
                //Query
                List<Buyer_Suppliers_Result> query = context.Buyer_Suppliers(request.Language.ToString(),
                                                                                "",
                                                                                "",
                                                                                request.start.ToString(),
                                                                                request.length.ToString(),
                                                                                request.EID.ToString(),
                                                                                sqlTxt).ToList();
                //Count

                int totalResultsCount = 0;
                int filteredResultsCount = 0;
                if (query != null)
                {
                    if (query.Count > 0)
                    {
                        totalResultsCount = query.FirstOrDefault().TotalResult;
                        filteredResultsCount = query.FirstOrDefault().TotalResult;
                    }
                }
                //int totalResultsCount = (context.Buyer_Suppliers(
                //                            request.Language.ToString(),
                //                            "",
                //                            "",
                //                            request.start.ToString(),
                //                            request.length.ToString(),
                //                            request.EID.ToString(),
                //                            "").ToList()
                //                         ).Count();

                return new Tuple<List<Buyer_Suppliers_Result>, int, int>(query, filteredResultsCount, totalResultsCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #endregion

        #region GetBuyerSupplierContact

        #region GET
        public ViewBuyerSupplier GetBuyerSupplierContactDetail(int userId)
        {
            ViewBuyerSupplier result = null;
            try
            {
                result = this.context.ViewBuyerSuppliers.Where(w => w.UserID == userId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }
        #endregion

        #endregion

        #region GetBuyerSupplierContact

        #region GET
        public List<BuyerHistoryContactPersonModel> GetListHistorySupplierContactPersonByUserId(int userId)
        {
            List<BuyerHistoryContactPersonModel> result = null;
            try
            {
                var query1 = from user in this.context.Tbl_User
                             join contact in this.context.Tbl_ContactPerson on user.ContactID equals contact.ContactID
                             join title_local in this.context.Tbl_NameTitle on contact.TitleID equals title_local.TitleID
                             join title_inter in this.context.Tbl_LocalizedProperty on contact.TitleID equals title_inter.EntityID
                             where (title_inter.LocaleKey == "TitleName" && title_inter.LocaleKeyGroup == "Tbl_NameTitle") && (user.UserID == userId)
                             select (new BuyerHistoryContactPersonModel
                             {
                                 UserID = user.UserID,
                                 Username = user.Username,
                                 SupplierID = user.SupplierID,
                                 OrgID = user.OrgID,
                                 ContactID = user.ContactID,
                                 Title_Local = title_local.TitleName,
                                 Title_Inter = title_inter.LocaleValue,
                                 FirstName_Local = contact.FirstName_Local,
                                 LastName_Local = contact.LastName_Local,
                                 FirstName_Inter = contact.FirstName_Inter,
                                 LastName_Inter = contact.LastName_Inter,
                                 PhoneNo = contact.PhoneNo,
                                 PhoneExt = contact.PhoneExt,
                                 Email = contact.Email,
                                 MobileNo = contact.MobileNo,
                                 MobileCountryCode = contact.MobileCountryCode,
                                 isDeleted = contact.isDeleted,
                                 TimeZone = user.TimeZone,
                                 UpdateById = null,
                                 UpdateByName = null,
                                 UpdateDate = null
                             });

                var query2 = from user in this.context.Tbl_BuyerUser
                             join buyerUser in context.Tbl_LogUserByBuyer on user.SysUserID equals buyerUser.UpdateBy
                             join contact in context.Tbl_ContactPerson on buyerUser.ContactID equals contact.ContactID
                             join title_local in this.context.Tbl_NameTitle on contact.TitleID equals title_local.TitleID
                             join title_inter in this.context.Tbl_LocalizedProperty on contact.TitleID equals title_inter.EntityID
                             where (title_inter.LocaleKey == "TitleName" && title_inter.LocaleKeyGroup == "Tbl_NameTitle") && (buyerUser.UserID == userId)
                             select (new BuyerHistoryContactPersonModel
                             {
                                 UserID = buyerUser.UserID,
                                 Username = buyerUser.Username,
                                 SupplierID = buyerUser.SupplierID,
                                 OrgID = buyerUser.OrgID,
                                 ContactID = user.ContactID,
                                 Title_Local = title_local.TitleName,
                                 Title_Inter = title_inter.LocaleValue,
                                 FirstName_Local = contact.FirstName_Local,
                                 LastName_Local = contact.LastName_Local,
                                 FirstName_Inter = contact.FirstName_Inter,
                                 LastName_Inter = contact.LastName_Inter,
                                 PhoneNo = contact.PhoneNo,
                                 PhoneExt = contact.PhoneExt,
                                 Email = contact.Email,
                                 MobileNo = contact.MobileNo,
                                 MobileCountryCode = contact.MobileCountryCode,
                                 isDeleted = contact.isDeleted,
                                 TimeZone = buyerUser.TimeZone,
                                 UpdateById = user.SysUserID,
                                 UpdateByName = user.Login,
                                 UpdateDate = buyerUser.UpdateDate
                             });


                if (query1 != null && query2 != null)
                {
                    var resultData = query1.Union(query2.OrderByDescending(o => o.UpdateDate)).ToList();

                    result = new List<BuyerHistoryContactPersonModel>();
                    result = resultData.ToList();
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public int InsertLogUserByBuyer(Tbl_LogUserByBuyer tbl_LogUserByBuyer)
        {
            int result = 0;
            try
            {
                this.context.Tbl_LogUserByBuyer.Add(tbl_LogUserByBuyer);
                this.context.SaveChanges();

                result = tbl_LogUserByBuyer.LogId;
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public virtual Tbl_BuyerConfig GetBuyerConfigByEID(int eid)
        {
            Tbl_BuyerConfig result = null;
            try
            {
                var query = from config in this.context.Tbl_BuyerConfig
                            where config.Section == "Supplier_Directory" && config.Name == "Access_Type_Supplier" && config.EID == eid
                            select config;

                result = query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }


        #endregion

        #endregion

    }
}
