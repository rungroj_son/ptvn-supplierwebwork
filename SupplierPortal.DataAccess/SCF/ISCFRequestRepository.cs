﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.SCF
{
    public interface ISCFRequestRepository
    {
        int GetTopIdSCFRequest();

        List<Tbl_SCFRequest> GetSCFRequestByOrgID(string orgID);
        int InsertSCFRequest(Tbl_SCFRequest sCFRequest);

        bool UpdateSCFRequest(Tbl_SCFRequest sCFRequest);
        bool RemoveSCFRequest(Tbl_SCFRequest sCFRequest);
        void Dispose();
    }
}
