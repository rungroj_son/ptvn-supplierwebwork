﻿using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess
{
    public class BaseRepository : IBaseRepository
    {
        protected SupplierPortalEntities context;

        public BaseRepository()
        {
            context = new SupplierPortalEntities();
        }
    }
}
