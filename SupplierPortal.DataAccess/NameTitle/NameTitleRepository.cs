﻿using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.DataAccess.Buyer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.DataAccess.NameTitle
{
    public partial class NameTitleRepository : BaseRepository, INameTitleRepository
    {
        public List<Tbl_NameTitle> GetNameTitleList(int languageID)
        {
            List<Tbl_NameTitle> result = null;
            try
            {
                var query1 = (from db in this.context.Tbl_LocalizedProperty
                              where db.LanguageID == languageID
                              && db.LocaleKeyGroup == "Tbl_NameTitle"
                              && db.LocaleKey == "TitleName"
                              select new { TitleID = db.EntityID, TitleName = db.LocaleValue }).ToList()
               .Select(x => new Tbl_NameTitle { TitleID = x.TitleID ?? default(int), TitleName = x.TitleName });

                if (query1.Count() > 0)
                {
                    result = query1.ToList();
                }
                else
                {
                   var query2 = (from db in this.context.Tbl_NameTitle
                             select new { TitleID = db.TitleID, TitleName = db.TitleName }).ToList()
                    .Select(x => new Tbl_NameTitle { TitleID = x.TitleID, TitleName = x.TitleName });

                    result = query2.ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;

        }

        public virtual void Dispose()
        {
            context.Dispose();
        }
    }
}
