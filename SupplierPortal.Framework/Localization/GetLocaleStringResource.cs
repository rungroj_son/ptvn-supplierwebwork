﻿using System;
using System.Collections.Generic;
using SupplierPortal.Data.Models.Repository.LocaleStringResource;

namespace SupplierPortal.Framework.Localization
{
    public static class GetLocaleStringResource
    {
        //private Localizer _localizer;
        public static string GetStringResource(this string lable, string resourecName, string languageId)
        {
            //string _localizer = "";
            ILocaleStringRepository _repo = null;
            _repo = new LocaleStringRepository();
            string resFormat = _repo.GetResource(resourecName, languageId);

            if (string.IsNullOrEmpty(resFormat))
            {
                return resourecName;
            }
            return resFormat;
        }

        public static string GetStringResourceCurrent(string resourecName, string languageId = "")
        {
            ILocaleStringRepository _repo = new LocaleStringRepository();

            languageId = CultureHelper.GetImplementedCulture(languageId); // This is safe

            string resFormat = _repo.GetResource(resourecName, languageId);

            if (string.IsNullOrEmpty(resFormat))
            {
                return resourecName;
            }
            return resFormat;
        }

        public static string GetStringResource(this string lable, string resourecName)
        {
            //string _localizer = "";
            ILocaleStringRepository _repo = null;
            _repo = new LocaleStringRepository();
            string resFormat = _repo.GetResource(resourecName);

            if (string.IsNullOrEmpty(resFormat))
            {
                return resourecName;
            }
            return resFormat;
        }

        public static string GetStringResourceByResourecNameAndLanguageId(string resourecName, string languageId)
        {
            //string _localizer = "";
            ILocaleStringRepository _repo = null;
            _repo = new LocaleStringRepository();
            string resFormat = _repo.GetResource(resourecName);

            if (string.IsNullOrEmpty(resFormat))
            {
                return resourecName;
            }
            return resFormat;
        }

        public static string getStringResourceByResourceName(string resourceName)
        {

            ILocaleStringRepository _Local = null;
            _Local = new LocaleStringRepository();
            string timeFormat = _Local.getStringResourceByResourceName(resourceName);
            return timeFormat;
        }

    }
}
