﻿#region Using...

using System;
using System.IO;
using System.Web.Mvc;
using System.Web.WebPages;

using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.LocaleStringResource;
using SupplierPortal.Data.Models.Repository.Topic;
using System.Web;


#endregion

namespace SupplierPortal.Framework.ViewEngines.Razor
{
    public abstract class WebViewPage<TModel> : System.Web.Mvc.WebViewPage<TModel>
    {

        private Localizer _localizer;

        public Localizer T
        {
            get
            {
                _localizer = (format, args) =>
                {

                    ILocaleStringRepository _repo = null;
                    _repo = new LocaleStringRepository();
                    var resFormat = _repo.GetResource(format);

                    if (string.IsNullOrEmpty(resFormat))
                    {
                        return new LocalizedString(format);
                    }
                    return
                        new LocalizedString((args == null || args.Length == 0)
                                                ? resFormat
                                                : string.Format(resFormat, args));
                };
                return _localizer;
            }

        }

        public Localizer TP
        {
            get
            {
                _localizer = (format, args) =>
                {

                    ITopicRepository _repo = null;
                    _repo = new TopicRepository();
                    string languageId = "";

                    HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];
                    if (cultureCookie != null)
                    {
                        languageId = cultureCookie.Value.ToString();

                    }
                    else
                    {
                        languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
                    }
                    var resFormat = _repo.GetResourceBody(format, languageId);

                    if (string.IsNullOrEmpty(resFormat))
                    {
                        return new LocalizedString(format);
                    }
                    return
                        new LocalizedString((args == null || args.Length == 0)
                                                ? resFormat
                                                : string.Format(resFormat, args));
                };
                return _localizer;
            }

        }

        public HelperResult RenderWrappedSection(string name, object wrapperHtmlAttributes)
        {
            Action<TextWriter> action = delegate(TextWriter tw)
            {
                var htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(wrapperHtmlAttributes);
                var tagBuilder = new TagBuilder("div");
                tagBuilder.MergeAttributes(htmlAttributes);

                var section = RenderSection(name, false);
                if (section != null)
                {
                    tw.Write(tagBuilder.ToString(TagRenderMode.StartTag));
                    section.WriteTo(tw);
                    tw.Write(tagBuilder.ToString(TagRenderMode.EndTag));
                }
            };
            return new HelperResult(action);
        }

        public HelperResult RenderSection(string sectionName, Func<object, HelperResult> defaultContent)
        {
            return IsSectionDefined(sectionName) ? RenderSection(sectionName) : defaultContent(new object());
        }


        public override string Layout
        {
            get
            {
                var layout = base.Layout;

                if (!string.IsNullOrEmpty(layout))
                {
                    var filename = System.IO.Path.GetFileNameWithoutExtension(layout);
                    ViewEngineResult viewResult = System.Web.Mvc.ViewEngines.Engines.FindView(ViewContext.Controller.ControllerContext, filename, "");

                    if (viewResult.View != null && viewResult.View is RazorView)
                    {
                        layout = (viewResult.View as RazorView).ViewPath;
                    }
                }

                return layout;
            }
            set
            {
                base.Layout = value;
            }
        }


        public int GetSelectedTabIndex()
        {
            //keep this method synchornized with
            //"SetSelectedTabIndex" method of \Administration\Controllers\BaseNopController.cs
            int index = 0;
            string dataKey = "portal.selected-tab-index";
            if (ViewData[dataKey] is int)
            {
                index = (int)ViewData[dataKey];
            }
            if (TempData[dataKey] is int)
            {
                index = (int)TempData[dataKey];
            }

            //ensure it's not negative
            if (index < 0)
                index = 0;

            return index;
        }

    }

    public abstract class WebViewPage : WebViewPage<dynamic>
    {

    }
}
