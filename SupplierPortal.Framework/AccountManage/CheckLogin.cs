﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.User;


namespace SupplierPortal.Framework.AccountManage
{
    public static class CheckLogin
    {
        public static Boolean Login(string username, string password)
        {

            IUserRepository _repo = new UserRepository();

            var resalt = _repo.FindByUsernameIsActive(username);
            if (resalt != null)
            {
                if (resalt.MigrateFlag == 1)
                {
                    if (PasswordHashing.CheckPasswordBSPUser(password, resalt.Username))
                    {
                        PasswordHashing.ResetMigratePassword(resalt.Username, password);

                        return true;
                    }
                }
                else if (resalt.MigrateFlag == 2)
                {
                    if (PasswordHashing.CheckPasswordeRFXUser(password, resalt.Username))
                    {
                        PasswordHashing.ResetMigratePassword(resalt.Username, password);

                        return true;
                    }
                }
                else if (resalt.MigrateFlag == 3)
                {
                    if (PasswordHashing.CheckPasswordOnlineAuctionUser(password, resalt.Username))
                    {
                        PasswordHashing.ResetMigratePassword(resalt.Username, password);

                        return true;
                    }
                }
                else if (resalt.MigrateFlag == 4)
                {
                    if (PasswordHashing.CheckPasswordOnlineAuction5User(password, resalt.Username))
                    {
                        PasswordHashing.ResetMigratePassword(resalt.Username, password);

                        return true;
                    }
                }
                else
                {
                    if (PasswordHashing.CheckPasswordPortalUser(password, resalt.Username))
                    {
                        return true;
                    }
                }
            }


            return false;

        }

        //Edit 22-05-2017  เพิ่ม out parameter เพื่อแจ้งสถานะการ Login Fix in LogonAttempt By Techit_khu
        public static Boolean Login(string username, string password, out string returnMessage)
        {

            IUserRepository _repo = new UserRepository();
            returnMessage = "";

            var resalt = _repo.FindByUsernameIsActive(username);
            if (resalt != null)
            {
                if (resalt.MigrateFlag == 1)
                {
                    if (PasswordHashing.CheckPasswordBSPUser(password, resalt.Username))
                    {
                        PasswordHashing.ResetMigratePassword(resalt.Username, password);

                        return true;
                    }
                    else
                    {
                        returnMessage = "Wrong password in BSP User";
                    }
                }
                else if (resalt.MigrateFlag == 2)
                {
                    if (PasswordHashing.CheckPasswordeRFXUser(password, resalt.Username))
                    {
                        PasswordHashing.ResetMigratePassword(resalt.Username, password);

                        return true;
                    }
                    else
                    {
                        returnMessage = "Wrong password in RFX User";
                    }
                }
                else if (resalt.MigrateFlag == 3)
                {
                    if (PasswordHashing.CheckPasswordOnlineAuctionUser(password, resalt.Username))
                    {
                        PasswordHashing.ResetMigratePassword(resalt.Username, password);

                        return true;
                    }
                    else
                    {
                        returnMessage = "Wrong password in OnlineAuction User";
                    }
                }
                else if (resalt.MigrateFlag == 4)
                {
                    if (PasswordHashing.CheckPasswordOnlineAuction5User(password, resalt.Username))
                    {
                        PasswordHashing.ResetMigratePassword(resalt.Username, password);

                        return true;
                    }
                    else
                    {
                        returnMessage = "Wrong password in OnlineAuction5 User";
                    }
                }
                else
                {
                    if (PasswordHashing.CheckPasswordPortalUser(password, resalt.Username))
                    {
                        return true;
                    }
                    else
                    {
                        returnMessage = "Wrong password in SWW User";
                    }
                }

            }
            else
            {
                returnMessage = "Username not found";
                return false;
            }


            return false;

        }
    }
}
