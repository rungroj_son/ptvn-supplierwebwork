﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using System.Security.Cryptography;

namespace SupplierPortal.Framework.AccountManage
{
    public static class PasswordHashing
    {

        public static string HashPassword(string password, out string saltOut)
        {
            byte[] salt = ClsHashManagement.GetRandomSalt();
            string saltStr = Convert.ToBase64String(salt);
            string fullPwdStr = password + saltStr;

            byte[] passwordHash = ClsHashManagement.ComputeHash(fullPwdStr);
            saltOut = saltStr; // salt must keep for each login-password for hashing
            return Convert.ToBase64String(passwordHash);

        }

        public static void ResetMigratePassword(string userName, string password)
        {            
            try
            {
                IUserRepository _repoUser = new UserRepository();
                IUserSystemMappingRepository _repoUserSystemMapping = new UserSystemMappingRepository();

                var tbl_User = _repoUser.FindByUsername(userName);

                string userPassword = tbl_User.Password;

                string newPasswordHashing = HashResetPassword(password, tbl_User.Username);

                tbl_User.Password = newPasswordHashing;
                tbl_User.MigrateFlag = 0;

                _repoUser.Update(tbl_User);
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }

        public static string HashResetPassword(string password, string username)
        {
            IUserRepository _repo = new UserRepository();

            string userSalt = ""; //Retrieve from DB by user id

            var result = _repo.FindByUsername(username);

            userSalt = result.Salt;

            string fullPwdStr = password + userSalt;

            byte[] passwordHash = ClsHashManagement.ComputeHash(fullPwdStr);
            string inPassword = Convert.ToBase64String(passwordHash);

            return inPassword;

        }

        public static Boolean CheckPasswordPortalUser(string password, string username)
        {
            IUserRepository _repo = new UserRepository();
            // Get Password of Portal User for comparing to the password given
            // Uid is User ID for getting password stored in DB to verify with  input password 

            string userSalt = ""; //Retrieve from DB by user id
            string userPassword = ""; //Retrieve from DB by user id

            var result = _repo.FindByUsername(username);

            userSalt = result.Salt;
            userPassword = result.Password;

            string fullPwdStr = password + userSalt;
            byte[] passwordHash = ClsHashManagement.ComputeHash(fullPwdStr);
            string inPassword = Convert.ToBase64String(passwordHash);

            return userPassword == inPassword;
        }

        public static bool CheckPasswordBSPUser(string password, string username)
        {
            bool chk = false;
            IUserRepository _repo = new UserRepository();
            IUserSystemMappingRepository _repoMapping = new UserSystemMappingRepository();

            string passwordHash = "";
            var result = _repo.FindByUsername(username);
            var userMapping = _repoMapping.GetUserSystemMappingByUsernameAndSystemID(username,1);

            

            if (userMapping != null)
            {
                string userPassword = userMapping.SysPassword;

                if (userPassword.Contains("{SHA}"))
                {
                    passwordHash = "{SHA}" + HashPasswordSHA1NoSalt(password);

                    if (passwordHash.ToUpper() == userPassword.ToUpper())
                    {
                        chk = true;
                    }
                }
                else if (userPassword.Contains("{SSHA}"))
                {
                    passwordHash = "{SSHA}" + HashPasswordSSHA1(password, userPassword.Replace("{SSHA}", ""));
                    //userPassword.Replace("{SHHA}", "")
                    if (passwordHash.ToUpper() == userPassword.ToUpper())
                    {
                        chk = true;
                    }
                }
            }

            return chk;
        }

        public static bool CheckPasswordeRFXUser(string password, string username)
        {
            bool chk = false;
            IUserRepository _repo = new UserRepository();
            IUserSystemMappingRepository _repoMapping = new UserSystemMappingRepository();

            var result = _repo.FindByUsername(username);
            var userMapping = _repoMapping.GetUserSystemMappingByUsernameAndSystemID(username, 3);

            if (userMapping != null)
            {
                string passwordHash = GetMd5Hash(password).ToUpper();

                string userPassword = userMapping.SysPassword.ToUpper();

                if (passwordHash == userPassword)
                {
                    chk = true;
                }
            }
            

            return chk;
        }

        public static bool CheckPasswordOnlineAuctionUser(string password, string username)
        {
            bool chk = false;
            IUserRepository _repo = new UserRepository();
            IUserSystemMappingRepository _repoMapping = new UserSystemMappingRepository();

            var result = _repo.FindByUsername(username);
            var userMapping = _repoMapping.GetUserSystemMappingByUsernameAndSystemID(username, 2);

            if (userMapping != null)
            {
                string passwordHash = GetMd5Hash(password).ToUpper();

                string userPassword = userMapping.SysPassword.ToUpper();

                if (passwordHash == userPassword)
                {
                    chk = true;
                }
            }


            return chk;
        }

        public static bool CheckPasswordOnlineAuction5User(string password, string username)
        {
            bool chk = false;
            IUserRepository _repo = new UserRepository();
            IUserSystemMappingRepository _repoMapping = new UserSystemMappingRepository();

            var result = _repo.FindByUsername(username);
            var userMapping = _repoMapping.GetUserSystemMappingByUsernameAndSystemID(username, 4);

            if (userMapping != null)
            {
                string passwordHash = GetMd5Hash(password).ToUpper();

                string userPassword = userMapping.SysPassword.ToUpper();

                if (passwordHash == userPassword)
                {
                    chk = true;
                }
            }


            return chk;
        }

        public static string GetSHA1HashData(string data)
        {
            //create new instance of md5
            SHA1 sha1 = SHA1.Create();

            //convert the input text to array of bytes
            byte[] hashData = sha1.ComputeHash(Encoding.Default.GetBytes(data));

            //create new instance of StringBuilder to save hashed data
            StringBuilder returnValue = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(hashData[i].ToString());
            }

            // return hexadecimal string
            return returnValue.ToString();
        }

        private static byte[] Combine(byte[] a, byte[] b)
        {
            byte[] c = new byte[a.Length + b.Length];
            System.Buffer.BlockCopy(a, 0, c, 0, a.Length);
            System.Buffer.BlockCopy(b, 0, c, a.Length, b.Length);
            return c;
        }

        public static string HashPasswordSHA1NoSalt(string password)
        {

            string fullPwdStr = password;

            byte[] passwordHash = ClsHashManagement.ComputeHashBSP_SHA1(fullPwdStr);

            Byte[] passwordHash20 = new Byte[20];
            Array.Copy(passwordHash, 0, passwordHash20, 0, 20);
            return Convert.ToBase64String(passwordHash20);

        }

        public static string GetMd5Hash(string input)
        {
            //create new instance of md5
            MD5 md5 = MD5.Create();

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }

        public static string HashPasswordSSHA1(string password, string oldPassword)
        {

            // Get Salt from old password and collect value in saltHash

            byte[] fullPasswordHash = System.Convert.FromBase64String(oldPassword.Trim().Replace(" ", "+"));
            //Encoding.UTF8.GetBytes(oldPassword);
            //Convert.FromBase64String(oldPassword);           

            byte[] passwordHash20 = new Byte[20];
            byte[] saltHash = new Byte[fullPasswordHash.Length - 20];

            Array.Copy(fullPasswordHash, 0, passwordHash20, 0, 20);
            Array.Copy(fullPasswordHash, 20, saltHash, 0, fullPasswordHash.Length - 20);


            // Hash new password with the existing salt and cut size to 20 byte

            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            byte[] fullPasswordBytes = new byte[passwordBytes.Length + saltHash.Length];

            Array.Copy(passwordBytes, 0, fullPasswordBytes, 0, passwordBytes.Length);
            Array.Copy(saltHash, 0, fullPasswordBytes, passwordBytes.Length, saltHash.Length);

            byte[] newPasswordHash = ClsHashManagement.ComputeHashBSP_SHA1(fullPasswordBytes);

            // Concat byte array password with byte array salt and convert to string for comparation.
            Byte[] newFullPasswordHash = new Byte[newPasswordHash.Length + saltHash.Length];
            Array.Copy(newPasswordHash, 0, newFullPasswordHash, 0, 20);
            Array.Copy(saltHash, 0, newFullPasswordHash, 20, saltHash.Length);

            return Convert.ToBase64String(newFullPasswordHash);

        }


        public static string EncodeBase64(string data)
        {
            string s = data.Trim().Replace(" ", "+");
            if (s.Length % 4 > 0)
                s = s.PadRight(s.Length + 4 - s.Length % 4, '=');
            return Encoding.UTF8.GetString(Convert.FromBase64String(s));
        }
    }
}
