﻿using SupplierPortal.Framework.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SupplierPortal.Framework.MethodHelper
{
    public static class GetCurrentLanguageHelper
    {

        public static string GetStringLanguageIDCurrent()
        {
            HttpCookie cultureCookie = System.Web.HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }

            return languageID;
        }

        public static int GetIntLanguageIDCurrent()
        {
            HttpCookie cultureCookie = System.Web.HttpContext.Current.Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }

            int languageId = Convert.ToInt32(languageID);

            return languageId;
        }
    }
}
