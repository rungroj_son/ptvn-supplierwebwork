﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.Repository.MenuPortal;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Framework.MethodHelper
{
    public static class GetMenuPortalHelper
    {
        public static Tbl_MenuPortal GetMenuByMenuName(string menuName)
        {
            IMenuPortalRepository _repo = new MenuPortalRepository();

            var result = _repo.GetMenuByMenuName(menuName);

            return result;

        }

    }
}
