﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.BillingDetail;
using SupplierPortal.Data.Models.SupportModel.BillingDetail;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.TimeZone;
using SupplierPortal.Data.Models.Repository.LogBillingUpdate;
using SupplierPortal.Data.Models.Repository.Locale;
using System.Web;
using System.Threading;
using System.Globalization;
using SupplierPortal.Framework.Localization;



namespace SupplierPortal.Framework.MethodHelper
{
    public static class CurrentTime
    {

        public static string getDateTime(string userName)
        {

            string UserName = userName;
            //  string timeZoneID="";
            DateTime dateTimeQuery = DateTime.Now;
            string timeZone = "";
            string offSet;
            double dOffSet;

            LogBillingUpdateRepository _logBillingUpdate = null;
            _logBillingUpdate = new LogBillingUpdateRepository();
            IUserRepository _userRepository = null;
            _userRepository = new UserRepository();
            ITimeZoneRepository _timeZoneRepository = null;
            _timeZoneRepository = new TimeZoneRepository();

            var Model = _userRepository.FindAllByUsername(UserName);
            timeZone = Model == null ? "Asia/Bangkok" : Model.TimeZone;
            var modelTimeZone = _timeZoneRepository.GetTimeZoneByTimeZoneIDString(timeZone);
            offSet = modelTimeZone.MinuteOffset.ToString();
            Double.TryParse(offSet, out dOffSet);

            var LastUpdate = _logBillingUpdate.GetLastUpdate();
            DateTime test = Convert.ToDateTime(LastUpdate.LastUpdate);
            //แปลงจาก minite offset เป็น timezoneinfo
            TimeZoneInfo timezoneInfo = TimeZoneInfo.CreateCustomTimeZone(timeZone, TimeSpan.FromMinutes(dOffSet), timeZone, timeZone);
            DateTime time = TimeZoneInfo.ConvertTimeFromUtc(test, timezoneInfo);

            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];
            string LanguageID = cultureCookie.Value;
            string timeLanguage = "yyyy-dd-MM HH:mm:ss";


            //   msgReturnTime = GetLocaleStringResource.GetStringResource("Portal_DateFormat_TH",languageId);


            switch (LanguageID)
            {
                case "2":
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("th-TH");
                    timeLanguage = GetLocaleStringResource.getStringResourceByResourceName("Portal_DateFormat_TH");
                    break;

                case "1":
                default:
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    timeLanguage = GetLocaleStringResource.getStringResourceByResourceName("Portal_DateFormat_EN");
                    break;

            }
            return time.ToString(timeLanguage);

        }

        public static string getLanguageID()
        {

            HttpCookie cultureCookie = HttpContext.Current.Request.Cookies["_culture"];
            string LanguageID = cultureCookie.Value;
            return LanguageID;
        }

        public static void SetCurrentCultureByLanguageID(string languageID)
        {
            switch (languageID)
            {
                case "2":
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("th-TH");
                    break;

                case "1":
                default:
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    break;

            }
        }

        public static void SetCurrentCultureByUsername(string userName)
        {
            IUserRepository _userRepository = new UserRepository();
            ILocaleRepository _localeRepository = new LocaleRepository();

            string localeName = "";
            string cultureInfoCode = "";
            

            try
            {
                var userModel = _userRepository.FindByUsername(userName);

                if (userModel != null)
                {
                    localeName = userModel.Locale;
                }

                var localeModel = _localeRepository.GetLocaleByLocaleName(localeName);

                if (localeModel != null)
                {
                    cultureInfoCode = localeModel.CultureInfoCode;
                    
                }

                Thread.CurrentThread.CurrentCulture = new CultureInfo(cultureInfoCode);
            }
            catch (Exception e)
            {

                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            }

        }

        public static string GetDateTimeShortDate(string userName,DateTime dateTime)
        {
            IUserRepository _userRepository = new UserRepository();
            ILocaleRepository _localeRepository = new LocaleRepository();
            try
            {

                string timeZone = "Asia/Bangkok"; //default
                string localeName = "";
                string dateTimeFormat = "MM/dd/yyyy"; //default

                var userModel = _userRepository.FindByUsername(userName);
                if (userModel != null)
                {
                    timeZone = userModel.TimeZone;
                    localeName = userModel.Locale;
                }

                var localeModel = _localeRepository.GetLocaleByLocaleName(localeName);
                if (localeModel != null)
                {
                    dateTimeFormat = localeModel.ShortDateMask;
                }

                //แปลงจาก minite offset เป็น timezoneinfo
                TimeZoneInfo timeZoneInfo = GetTimeZoneInfo(timeZone, dateTime);

                DateTime time = TimeZoneInfo.ConvertTimeFromUtc(dateTime, timeZoneInfo);

                return time.ToString(dateTimeFormat);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public static string GetDateTimeMediumDate(string userName, DateTime dateTime)
        {
            IUserRepository _userRepository = new UserRepository();
            ILocaleRepository _localeRepository = new LocaleRepository();
            try
            {

                string timeZone = "Asia/Bangkok"; //default
                string localeName = "";
                string dateTimeFormat = "MM/dd/yyyy"; //default

                var userModel = _userRepository.FindByUsername(userName);
                if (userModel != null)
                {
                    timeZone = userModel.TimeZone;
                    localeName = userModel.Locale;
                }

                var localeModel = _localeRepository.GetLocaleByLocaleName(localeName);
                if (localeModel != null)
                {
                    dateTimeFormat = localeModel.MediumDateMask;
                }

                //แปลงจาก minite offset เป็น timezoneinfo
                TimeZoneInfo timeZoneInfo = GetTimeZoneInfo(timeZone, dateTime);

                DateTime time = TimeZoneInfo.ConvertTimeFromUtc(dateTime, timeZoneInfo);

                return time.ToString(dateTimeFormat);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public static string GetDateTimeLongDate(string userName, DateTime dateTime)
        {
            IUserRepository _userRepository = new UserRepository();
            ILocaleRepository _localeRepository = new LocaleRepository();
            try
            {

                string timeZone = "Asia/Bangkok"; //default
                string localeName = "";
                string dateTimeFormat = "MM/dd/yyyy"; //default

                var userModel = _userRepository.FindByUsername(userName);
                if (userModel != null)
                {
                    timeZone = userModel.TimeZone;
                    localeName = userModel.Locale;
                }

                var localeModel = _localeRepository.GetLocaleByLocaleName(localeName);
                if (localeModel != null)
                {
                    dateTimeFormat = localeModel.LongDateMask;
                }

                //แปลงจาก minite offset เป็น timezoneinfo
                TimeZoneInfo timeZoneInfo = GetTimeZoneInfo(timeZone, dateTime);

                DateTime time = TimeZoneInfo.ConvertTimeFromUtc(dateTime, timeZoneInfo);

                return time.ToString(dateTimeFormat);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public static string GetDateTimeCustomDate(string userName, DateTime dateTime,string formatDate)
        {
            IUserRepository _userRepository = new UserRepository();
            //ILocaleRepository _localeRepository = new LocaleRepository();
            try
            {

                string timeZone = "Asia/Bangkok"; //default
                //string localeName = "";
                //string dateTimeFormat = "MM/dd/yyyy"; //default

                var userModel = _userRepository.FindByUsername(userName);
                if (userModel != null)
                {
                    timeZone = userModel.TimeZone;
                    //localeName = userModel.Locale;
                }

                //var localeModel = _localeRepository.GetLocaleByLocaleName(localeName);
                //if (localeModel != null)
                //{
                //    dateTimeFormat = localeModel.LongDateMask;
                //}

                //แปลงจาก minite offset เป็น timezoneinfo
                TimeZoneInfo timeZoneInfo = GetTimeZoneInfo(timeZone, dateTime);

                DateTime time = TimeZoneInfo.ConvertTimeFromUtc(dateTime, timeZoneInfo);

                return time.ToString(formatDate);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        private static TimeZoneInfo GetTimeZoneInfo(string timeZone, DateTime dateTime)
        {
            ITimeZoneRepository _timeZoneRepository = new TimeZoneRepository();
            try
            {
                string offSet;
                double dOffSet = 0.00;

                var timeZoneModel = _timeZoneRepository.GetTimeZoneByTimeZoneIDString(timeZone);
                if (timeZoneModel != null)
                {
                    offSet = timeZoneModel.MinuteOffset.ToString();
                    Double.TryParse(offSet, out dOffSet);
                }

                //แปลงจาก minite offset เป็น timezoneinfo
                TimeZoneInfo timezoneInfo = TimeZoneInfo.CreateCustomTimeZone(timeZone, TimeSpan.FromMinutes(dOffSet), timeZone, timeZone);

                return timezoneInfo;
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}
