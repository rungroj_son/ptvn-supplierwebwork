﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SupplierPortal.Framework.MethodHelper
{
    public static class GetStringFunctionHelper
    {
        public static string GetMakeVerifyString(string str)
        {
            string pattern = @"[ |.|\-|,|(|)]";
            Regex rgx = new Regex(pattern);
            string result = rgx.Replace(str, "");
            return result;
        }
    }
}
