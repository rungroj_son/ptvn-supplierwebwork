﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierPortal.Framework.MethodHelper
{
    public static class GetGenerateGuidHelper
    {
        public static string GetGenerateRandomGuid()
        {
            string guid = "";
            Guid g;
            // Create and display the value of two GUIDs.
            g = Guid.NewGuid();

            guid = g.ToString();

            guid = guid.Replace("-", "");

            return guid;

        }
    }
}
