﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SupplierPortal.Startup))]
namespace SupplierPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
