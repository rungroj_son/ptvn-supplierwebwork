﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Configuration;
using Microsoft.AspNet.SignalR.Hubs;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Hubs
{
    public class MessagesHub : Hub
    {
        private static string conString = ConfigurationManager.ConnectionStrings["SupplierPortalEntities"].ToString();
        public void Hello()
        {
            Clients.All.hello();
        }

        [HubMethodName("sendMessages")]
        public static void SendMessages(List<Tbl_Notification> datas)
        {
            var ctx = new SupplierPortalEntities();
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<MessagesHub>();
            //context.Clients.User(userId).updateMessages();
            var userChanged = datas.Select(s => s.UserID).Distinct();
            foreach (var user in userChanged)
            {
                var userName = (from n in ctx.Tbl_User
                                where n.UserID == user
                                select n.Username).FirstOrDefault();

                var data = datas.Where(s => s.UserID == user).ToList();

                context.Clients.User(userName).updateMessages();
            }
        }
    }
}