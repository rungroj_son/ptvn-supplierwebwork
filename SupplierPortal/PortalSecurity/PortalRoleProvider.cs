﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.PortalSecurity
{
    public class PortalRoleProvider : RoleProvider
    {
        public override bool IsUserInRole(string username, string roleName)
        {
            using (var ctx = new SupplierPortalEntities())
            {
                var user = ctx.Tbl_User.Where(m => m.Username == username).FirstOrDefault();
                if (user == null)
                    return false;
            
                var query = from a in ctx.Tbl_UserRole
                            join h in ctx.Tbl_User on a.UserID equals h.UserID into i
                            from j in i.DefaultIfEmpty()
                            join b in ctx.Tbl_ACL_RolePrivilege on a.RoleID equals b.RoleID into c
                            from d in c.DefaultIfEmpty()
                            join e in ctx.Tbl_ACL_Privilege on new { d.ACL_ID } equals new { e.ACL_ID } into f
                            from g in f.DefaultIfEmpty()
                            where j.Username == username
                            select new
                            {
                                roleName = g.PrivilegeCode

                            };

                return query.Select(m => m.roleName).Count() > 0 && query.Any(r => r.roleName == roleName);
            }
        }


        public override string[] GetRolesForUser(string username)
        {
            using (var ctx = new SupplierPortalEntities())
            {
                var user = ctx.Tbl_User.SingleOrDefault(u => u.Username == username);

                //var role = ctx.Tbl_UserRole.SingleOrDefault(u => u.Username == username);
                if (user == null)
                {
                    return new string[] { };
                }
                    

                var query = from a in ctx.Tbl_UserRole
                            join h in ctx.Tbl_User on a.UserID equals h.UserID into i
                            from j in i.DefaultIfEmpty()
                            join b in ctx.Tbl_ACL_RolePrivilege on a.RoleID equals b.RoleID into c
                            from d in c.DefaultIfEmpty()
                            join e in ctx.Tbl_ACL_Privilege on new { d.ACL_ID } equals new { e.ACL_ID } into f
                            from g in f.DefaultIfEmpty()
                            where j.Username == username && d.RoleID == a.RoleID
                            select new
                            {
                                roleName = g.PrivilegeCode

                            };


                return query.Select(m => m.roleName).Count() == 0 ? new string[] { } :
                  query.Select(u => u.roleName).ToArray();
            }
        }


        public override string[] GetAllRoles()
        {
            using (var ctx = new SupplierPortalEntities())
            {
                return ctx.Tbl_ACL_Privilege.Select(r => r.PrivilegeCode).ToArray();
                //return usersContext.Roles.Select(r => r.RoleName).ToArray();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}