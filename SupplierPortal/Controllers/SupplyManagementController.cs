﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Services.SupplyManagement;
using SupplierPortal.Data.Models.SupportModel.SupplyManagement;
using SupplierPortal.Models.SupplyManagement;
using SupplierPortal.Data.Models.Repository.MenuDFM;
using SupplierPortal.Data.Models.Repository.MenuPortal;
using SupplierPortal.Data.Models.SupportModel.MenuPortal;
using System.Text;
using SupplierPortal.Data.Models.Repository.User;
using JWT;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.MappingTable;

namespace SupplierPortal.Controllers
{
    public class SupplyManagementController : ControllerBase
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        IReportService _reportServiceRepository;
        IMenuDFMRepository _menuDFMRepository;
        IMenuPortalRepository _menuPortalRepository;
        IUserRepository _userRepository;
        ILanguageRepository _languageRepository;

        public SupplyManagementController()
        {
            _reportServiceRepository = new ReportService();
            _menuDFMRepository = new MenuDFMRepository();
            _menuPortalRepository = new MenuPortalRepository();
            _userRepository = new UserRepository();
            _languageRepository = new LanguageRepository();
        }

        public SupplyManagementController(
            ReportService reportServiceRepository,
            MenuDFMRepository menuDFMRepository,
            MenuPortalRepository menuPortalRepository,
            UserRepository userRepository,
            LanguageRepository languageRepository


            )
        {
            _reportServiceRepository = reportServiceRepository;
            _menuDFMRepository = menuDFMRepository;
            _menuPortalRepository = menuPortalRepository;
            _userRepository = userRepository;
            _languageRepository = languageRepository;

        }


        // GET: SupplyManagement
        public ActionResult Index()
        {
            return View();
        }

        // GET: SupplyManagement/ReportsList
        public ActionResult ReportsList()
        {
            string username = "";
            if (Session["username"] != null)
			{
				username = Session["username"].ToString();
			}

            var model = _reportServiceRepository.ReportsListByUser(username);

            if (Request.Cookies["_year"] != null && Request.Cookies["_buyer"] != null)
            {
                HttpCookie cookieYear = Request.Cookies["_year"];
                HttpCookie cookieBuyerGrp = Request.Cookies["_buyer"];

                string year = null;

                DateTime dt = DateTime.Now;
                year = dt.Year.ToString();
                cookieYear = new HttpCookie("_year");
                cookieYear.Value = year;
                cookieYear.Expires = DateTime.Now.AddDays(30);
                cookieBuyerGrp = new HttpCookie("_buyer");
                cookieBuyerGrp.Value = "All";
                cookieBuyerGrp.Expires = DateTime.Now.AddDays(30);

                Response.Cookies.Add(cookieYear);
                Response.Cookies.Add(cookieBuyerGrp);
            }

            return View(model);
        }

        // POST: SupplyManagement
        [HttpPost]
        public ActionResult TestData(TestDataModel model)
        {

            var datatest = model;

            return View(datatest);
        }

        public ActionResult DFMCaller()
        {
            Tbl_MenuDFM menuDFMModel = null;
            try
            {
                int languageID = GetCurrentLanguageHelper.GetIntLanguageIDCurrent();

                string languageCode = _languageRepository.GetLanguageCodeById(languageID);

                string username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }

                int menuID = 0;

                var menuPortalModel = _menuPortalRepository.GetMenuByMenuName("m_SupplyTool_DFM_DocGrp1"); //  จะได้ menuID = 38 
                menuDFMModel = new Tbl_MenuDFM();

                if (menuPortalModel == null)
                {
                    menuDFMModel = _menuDFMRepository.GetFirstOrDefaultMenuDFMByMenu();
                }
                else
                {
                    menuID = menuPortalModel.MenuID;
                    menuDFMModel = _menuDFMRepository.GetMenuDFMByMenuID(menuID);
                }

                  


                //var menuDFMModel = _menuDFMRepository.GetMenuDFMByMenuID(38); // menuID 38 คือ m_SupplyTool_DFM_DocGrp1

                int userID = _userRepository.GetUserIDByUsername(username);

                string urliFrame = "";
                string hexKey = "";

                string userId = userID.ToString(); 
                string locale = languageCode;
                string system = "";
                int eboId = 0;
                string priv = "";
                string docGroup = "";
                


                if (menuDFMModel != null)
                {
                    urliFrame = menuDFMModel.URL;
                    hexKey = menuDFMModel.TokenKey;
                    docGroup = menuDFMModel.DocGroup;
                    system = menuDFMModel.System;
                    eboId = Convert.ToInt32(menuDFMModel.EBOId);
                    priv = menuDFMModel.Priv;

                    
                }

                byte[] key = StringToByteArray(hexKey);

                double seconds = 60;

                DateTime expire = DateTime.Now.AddSeconds(seconds);

                var payload = new Dictionary<string, object>()
                {
                    { "userId", userId },
                    { "locale", locale },
                    { "system", system },
                    { "eboId", eboId },
                    { "priv", priv }
                    //{ "exp", ToUnixTime(expire).ToString() }
                };

                //Dictionary<string, object> header = new Dictionary<string, object>()
                //{
                //    { "typ", "JWT" }
                //};

                string token = JWT.JsonWebToken.Encode(payload, key, JWT.JwtHashAlgorithm.HS512);

                logger.Info("[JWT Token] --> " + token);

                var jsonResult = JsonWebToken.Decode(token, key, true);

                logger.Info("Data Json Send[JWT Decode] --> " + jsonResult);

                IFrameMenuModel model = new IFrameMenuModel()
                {
                    UrliFrame = urliFrame,
                    Token = token,
                    DocGroup = docGroup,
                    PageNo = 1
                };
               
                return View(model);

            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            
        }

        public  byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public long ToUnixTime(DateTime dateTime)
        {
            return (int)(dateTime.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
    }
}