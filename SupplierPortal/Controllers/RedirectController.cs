﻿using Newtonsoft.Json;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Data.Models.SupportModel.UAA;
using SupplierPortal.Models.Dto;
using SupplierPortal.Models.Enums;
using SupplierPortal.Models.Enums.Enums;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace SupplierPortal.Controllers
{
    public class RedirectController : BaseController
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        IUserSessionRepository _userSessionRepository;
        IUserRepository _userRepository;
        IContactPersonRepository _contactPersonRepository;

        public RedirectController()
        {
            _userSessionRepository = new UserSessionRepository();
            _userRepository = new UserRepository();
            _contactPersonRepository = new ContactPersonRepository();
        }

        public async Task<ActionResult> RedirectAVL()
        {
            UAAOAuthModel token = await this.getUAATokenByMenuName(MenuNameEnum.AVL, GrantTypeEnum.UserDetail);
            if (token != null)
            {
                string url = WebConfigurationManager.AppSettings["avl_auth_url"];
                string urlRedirect = string.Format(url, token.access_token, token.refresh_token);

                return Redirect(urlRedirect);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult RedirectSC(string ticketCode)
        {
            string url = WebConfigurationManager.AppSettings["sc_ticket_redirect_url"];
            return Redirect(string.Format(url, ticketCode));
        }

        public ActionResult RedirectCompanyProfileSC(string emailEncrypt, string taxId, string branchNumber, string countryCode)
        {
            string url = WebConfigurationManager.AppSettings["sc_company_profile_redirect_url"];
            if (!String.IsNullOrEmpty(url))
            {
                return Redirect(string.Format(url + "&t={1}&b={2}&c={3}", Uri.EscapeDataString(emailEncrypt), taxId, branchNumber, countryCode));
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult RedirectRegisterSC()
        {
            string url = WebConfigurationManager.AppSettings["join_now_url"];
            return Redirect(string.Format(url));
        }

        #region Auction
        public async Task<ActionResult> RedirectAuction()
        {
            UAAOAuthModel token = await this.getUAATokenByMenuName(MenuNameEnum.Auction, GrantTypeEnum.Password);
            if (token != null && token.access_token != null)
            {
                string url = WebConfigurationManager.AppSettings["auction_auth_url"];
                string state = WebConfigurationManager.AppSettings["auction_auth_state"];
                string urlRedirect = string.Format(url, state, token.access_token);

                if (Session["AUCTION_URI"] != null && Session["AUCTION_STATE"] != null)
                {
                    url = Session["AUCTION_URI"].ToString();
                    state = Session["AUCTION_STATE"].ToString();
                    urlRedirect = url + "?state=" + state + "&code=" + token.access_token;
                }

                Session.Remove("AUCTION_SCOPE");
                Session.Remove("AUCTION_STATE");
                Session.Remove("AUCTION_URI");
                Session.Remove("redirect_action_name");
                return Redirect(urlRedirect);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public async Task<ActionResult> AuthAuction(string scope, string state, string response_type, string client_id, string redirect_uri)
        {
            this.ClearSession();
            if (!String.IsNullOrWhiteSpace(scope) && !String.IsNullOrWhiteSpace(state) && !String.IsNullOrWhiteSpace(redirect_uri))
            {
                Session["AUCTION_SCOPE"] = scope;
                Session["AUCTION_STATE"] = state;
                Session["AUCTION_URI"] = redirect_uri;
                Session["redirect_action_name"] = RedirectActionNameEnum.RedirectAuction.GetAttributeCode().ToString();
            }

            if (!this.CheckUserSessionTimeout())
            {
                return RedirectToAction("RedirectAuction", "Redirect");
            }
            else
            {
                return RedirectToAction("Login", "AccountPortal");
            }
        }
        #endregion Auction

        #region Procurement Board
        public async Task<ActionResult> RedirectProcurementBoard()
        {
            UAAOAuthModel token = await this.getUAATokenByMenuName(MenuNameEnum.ProcurementBoard, GrantTypeEnum.Password);
            if (token != null && token.auth_code != null)
            {
                string url = WebConfigurationManager.AppSettings["procurement_auth_url"];
                string state = WebConfigurationManager.AppSettings["procurement_auth_state"];
                string urlRedirect = string.Format(url, state, token.auth_code);

                if (Session["PROCUREMENT_URI"] != null && Session["PROCUREMENT_STATE"] != null)
                {
                    url = Session["PROCUREMENT_URI"].ToString();
                    state = Session["PROCUREMENT_STATE"].ToString();
                    urlRedirect = url + "?state=" + state + "&code=" + token.auth_code;
  
                }

                Session.Remove("PROCUREMENT_STATE");
                Session.Remove("PROCUREMENT_URI");
                Session.Remove("redirect_action_name");
                Session["RedirectUriMarketPlace"] = urlRedirect;

                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public async Task<ActionResult> AuthProcurementBoard(string state, string redirect_uri)
        {
            this.ClearSession();
            if (!String.IsNullOrWhiteSpace(state) && !String.IsNullOrWhiteSpace(redirect_uri))
            {
                Session["PROCUREMENT_STATE"] = state;
                Session["PROCUREMENT_URI"] = redirect_uri;
                Session["redirect_action_name"] = RedirectActionNameEnum.RedirectProcurementBoard.GetAttributeCode().ToString();
            }

            if (!this.CheckUserSessionTimeout())
            {
                return RedirectToAction("RedirectProcurementBoard", "Redirect");
            }
            else
            {
                return RedirectToAction("Login", "AccountPortal");
            }
        }
        #endregion Procurement Board

        #region Helper
        private async Task<UAAOAuthModel> getUAATokenByMenuName(MenuNameEnum menuName, GrantTypeEnum grantType)
        {
            UAAOAuthModel result = null;
            try
            {
                result = await getUAAToken(grantType.GetAttributeTypeCode().ToString());
            }
            catch (Exception ex)
            {
                logger.Error("getUAATokenByMenuName: " + ex.InnerException);
            }
            return result;
        }

        private async Task<UAAOAuthModel> getUAAToken(string grantType)
        {

            string userPassword = Session["UAA_PWD"] != null ? Session["UAA_PWD"].ToString() : String.Empty;
            string baseURL = WebConfigurationManager.AppSettings["baseURL"];
            string userGuid = Session["GUID"].ToString();
            var userSession = _userSessionRepository.GetUserSession(userGuid);
            var user = _userRepository.FindAllByUsername(userSession.Username);

            Tbl_ContactPerson userContactPerson = new Tbl_ContactPerson();
            if (user != null && user.ContactID.HasValue)
            {
                userContactPerson = _contactPersonRepository.GetContactPersonByContectID(user.ContactID.Value);
            }

            string urlUAAToken = string.Format("{0}api/uaa/token/", baseURL);
            using (HttpClient clientCoc = new HttpClient())
            {
                HttpResponseMessage response = await clientCoc.PostAsJsonAsync(urlUAAToken, new UAAOAuthRequestModel()
                {
                    username = user.Username,
                    roleName = user.Tbl_UserRole.Select(s => s.Tbl_ACL_Role.RoleName).FirstOrDefault(),
                    firstName = userContactPerson.FirstName_Inter,
                    lastName = userContactPerson.LastName_Inter,
                    email = userContactPerson.Email,
                    firstNameLocal = userContactPerson.FirstName_Local,
                    lastNameLocal = userContactPerson.LastName_Local,
                    telephone = userContactPerson.MobileNo,
                    password = userPassword,
                    grant_type = grantType
                });

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    BaseResponseDto responseDto = this.streamReaderToObject<BaseResponseDto>(response);
                    if (responseDto != null && responseDto.isSuccess)
                    {
                        UAAOAuthModel tokenModel = JsonConvert.DeserializeObject<UAAOAuthModel>(JsonConvert.SerializeObject(responseDto.data));
                        return tokenModel;
                    }
                }
            }

            return null;
        }

        private T streamReaderToObject<T>(HttpResponseMessage httpResponse)
        {
            string jsonResult = "";
            using (Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result)
            {
                using (StreamReader r = new StreamReader(responseString))
                {
                    jsonResult = r.ReadToEnd();
                }
            }

            JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
            return JsonConvert.Deserialize<T>(jsonResult);
        }
        #endregion

        #region
        private bool CheckUserSessionTimeout()
        {
            bool result = true;
            if (Session["GUID"] != null)
            {
                string userGuid = Session["GUID"].ToString();
                var userSession = _userSessionRepository.GetUserSession(userGuid);
                if (userSession != null && userSession.isTimeout == 0)
                {
                    result = false;
                }
            }
            return result;
        }

        private void ClearSession()
        {
            Session.Remove("PROCUREMENT_STATE");
            Session.Remove("PROCUREMENT_URI");

            Session.Remove("AUCTION_SCOPE");
            Session.Remove("AUCTION_STATE");
            Session.Remove("AUCTION_URI");

            Session.Remove("redirect_action_name");
        }
        #endregion
    }
}