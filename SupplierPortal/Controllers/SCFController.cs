﻿using SupplierPortal.Data.CustomModels.SCF;
using SupplierPortal.ServiceClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SupplierPortal.Controllers
{
    public class SCFController : BaseController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private SCFService _sCFService;

        public SCFController()
        {
            _sCFService = new SCFService();
        }

        // GET: SCF
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult EInvoiceLink(string actionFrom)
        {
            try
            {
                string guid = Session["GUID"].ToString();
                Session["SCFActionFrom"] = actionFrom;
            }
            catch (Exception ex)
            {
                logger.Error("SaveSCFRequest : " + ex);
            }

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult RemoveEInvoiceSession()
        {
            try
            {
                string guid = Session["GUID"].ToString();
                Session["SCFActionFrom"] = null;
            }
            catch (Exception ex)
            {
                logger.Error("SaveSCFRequest : " + ex);
            }

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public async Task<ActionResult> SaveSCFRequest()
        {
            SCFResponse response = null;
            try
            {
                string guid = Session["GUID"].ToString();
                string actionFrom = Session["SCFActionFrom"] != null ? Session["SCFActionFrom"].ToString() : "Webwork";
                SCFRequest request = new SCFRequest()
                {
                    userGUID = guid,
                    actionFrom = actionFrom
                };

                response = await _sCFService.SaveSCFRequest(request);
                Session["SCFActionFrom"] = null;
            }
            catch (Exception ex)
            {
                logger.Error("SaveSCFRequest : " + ex);
            }

            return Json(response);
        }

        [AllowAnonymous]
        public async Task<ActionResult> SCFStatusInProgress()
        {
            SCFResponse response = null;
            try
            {
                string guid = Session["GUID"].ToString();
                SCFRequest request = new SCFRequest()
                {
                    userGUID = guid,
                    actionFrom = ""
                };

                response = await _sCFService.CheckSCFRequestStatusInProgress(request);

                if (response.isError)
                {
                    Session["SCFActionFrom"] = null;
                }
            }
            catch (Exception ex)
            {
                logger.Error("SCFStatusInProgress : " + ex);
            }
            return Json(response);
        }
    }
}