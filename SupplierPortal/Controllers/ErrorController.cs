﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Models.Error;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.UserSession;

namespace SupplierPortal.Controllers
{
    public class ErrorController : BaseController
    {

        IUserSessionRepository _userSessionRepository;

        public ErrorController()
        {
            _userSessionRepository = new UserSessionRepository();

        }


        public ErrorController(UserSessionRepository userSessionRepository)
        {
            _userSessionRepository = userSessionRepository;

        }


        // GET: Error
        public ActionResult Error(string ErrorMessage="")
        {
            if (string.IsNullOrEmpty(ErrorMessage.Trim()))
            {
                ViewBag.ErrorMessage = Session["ErrorMessage"].ToString();
                return View();
            }

            ViewBag.ErrorMessage = ErrorMessage;
            return View();
        }

        public ActionResult BuyerPortalError(string ErrorMessage = "")
        {
            if (string.IsNullOrEmpty(ErrorMessage.Trim()))
            {
                ViewBag.ErrorMessage = Session["ErrorMessage"].ToString();
                return View();
            }

            ViewBag.ErrorMessage = ErrorMessage;
            return View();
        }


        // GET: Error
        public ActionResult TestPostError()
        {


            return View();
        }


        // POST: Error
        [HttpPost]
        public ActionResult Error(ErrorMessageModels model)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }         

            string messageReturn = "";

           

            if (!ModelState.IsValid)
            {
                Session["ErrorMessage"] = messageReturn.GetStringResource("_Error.ErrorMessage.DataIsValid", languageId);
                ViewBag.ErrorMessage = messageReturn.GetStringResource("_Error.ErrorMessage.DataIsValid", languageId);
                return View();
            }

            var userSession = _userSessionRepository.GetUserSession(model.UserGUID);

            if (userSession != null)
            {
                Session["ErrorMessage"] = model.ErrorMessage;
                ViewBag.ErrorMessage = model.ErrorMessage;
                return View();
            }

            string errorMessage = "";
            errorMessage = messageReturn.GetStringResource("_Error.ErrorMessage.GuidNotFound", languageId);
            errorMessage = errorMessage.Replace("<%UserGUID%>", model.UserGUID);

            Session["ErrorMessage"] = errorMessage;
            ViewBag.ErrorMessage = errorMessage;
            return View();
        }

        public ActionResult Timeout()
        {
            
            return View();
        }

        public ActionResult UserTimeout()
        {
            return View();
        }

        public ActionResult LandingPage()
        {
            return View();
        }

        // GET: RegisterPortalError
        public ActionResult RegisterPortalError(string ErrorMessage = "")
        {
            if (string.IsNullOrEmpty(ErrorMessage.Trim()))
            {
                ViewBag.ErrorMessage = Session["ErrorMessage"].ToString();
                return View();
            }

            ViewBag.ErrorMessage = ErrorMessage;
            return View();
        }
    }
}