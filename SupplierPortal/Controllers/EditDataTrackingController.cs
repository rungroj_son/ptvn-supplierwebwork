﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.TrackingRequest;
using SupplierPortal.Data.Models.Repository.TrackingComprofile;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.BusinessEntity;
using SupplierPortal.Data.Models.Repository.PortalFieldConfig;
using SupplierPortal.Data.Models.Repository.TrackingOrgAddress;
using SupplierPortal.Data.Models.Repository.OrgAddress;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.AutoComplete_State;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.CustomerDocType;
using SupplierPortal.Data.Models.Repository.CustomerDocName;
using SupplierPortal.Data.Models.Repository.TrackingOrgAttachment;
using SupplierPortal.Data.Models.Repository.OrgAttachment;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.LogOrgAttachment;
using SupplierPortal.Services.CompanyProfileManage;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Framework.MethodHelper;
using Newtonsoft.Json;
using System.Web.Configuration;
using System.IO;
namespace SupplierPortal.Controllers
{
    public class EditDataTrackingController : BaseController
    {

        ITrackingRequestRepository _trackingRequestRepository;
        ITrackingComprofileRepository _trackingComprofileRepository;
        IComProfileDataService _comProfileDataService;
        ICountryRepository _countryRepository;
        IBusinessEntityRepository _businessEntityRepository;
        IPortalFieldConfigRepository _portalFieldConfigRepository;
        IOrgAddressRepository _orgAddressRepository;
        ITrackingOrgAddressRepository _trackingOrgAddressRepository;
        ILanguageRepository _languageRepository;
        IAutoComplete_StateRepository _autoComplete_StateRepository;
        IAddressRepository _addressRepository;
        IOrganizationRepository _organizationRepository;
        ICustomerDocTypeRepository _customerDocTypeRepository;
        ICustomerDocNameRepository _customerDocNameRepository;
        IComProfileTrackingService _comProfileTrackingService;
        ITrackingOrgAttachmentRepository _trackingOrgAttachmentRepository;
        IOrgAttachmentRepository _orgAttachmentRepository;
        IUserRepository _userRepository;
        IAppConfigRepository _appConfigRepository;
        ILogOrgAttachmentRepository _logOrgAttachmentRepository;

        public EditDataTrackingController()
        {
            _trackingRequestRepository = new TrackingRequestRepository();
            _trackingComprofileRepository = new TrackingComprofileRepository();
            _comProfileDataService = new ComProfileDataService();
            _countryRepository = new CountryRepository();
            _businessEntityRepository = new BusinessEntityRepository();
            _portalFieldConfigRepository = new PortalFieldConfigRepository();
            _orgAddressRepository = new OrgAddressRepository();
            _trackingOrgAddressRepository = new TrackingOrgAddressRepository();
            _languageRepository = new LanguageRepository();
            _autoComplete_StateRepository = new AutoComplete_StateRepository();
            _addressRepository = new AddressRepository();
            _organizationRepository = new OrganizationRepository();
            _customerDocTypeRepository = new CustomerDocTypeRepository();
            _customerDocNameRepository = new CustomerDocNameRepository();
            _comProfileTrackingService = new ComProfileTrackingService();
            _trackingOrgAttachmentRepository = new TrackingOrgAttachmentRepository();
            _orgAttachmentRepository = new OrgAttachmentRepository();
            _userRepository = new UserRepository();
            _appConfigRepository = new AppConfigRepository();
            _logOrgAttachmentRepository = new LogOrgAttachmentRepository();
        }

        public EditDataTrackingController(
            TrackingRequestRepository trackingRequestRepository,
            TrackingComprofileRepository trackingComprofileRepository,
            ComProfileDataService comProfileDataService,
            CountryRepository countryRepository,
            BusinessEntityRepository businessEntityRepository,
            PortalFieldConfigRepository portalFieldConfigRepository,
            OrgAddressRepository orgAddressRepository,
            TrackingOrgAddressRepository trackingOrgAddressRepository,
            LanguageRepository languageRepository,
            AutoComplete_StateRepository autoComplete_StateRepository,
            AddressRepository addressRepository,
            OrganizationRepository organizationRepository,
            CustomerDocTypeRepository customerDocTypeRepository,
            CustomerDocNameRepository customerDocNameRepository,
            ComProfileTrackingService comProfileTrackingService,
            TrackingOrgAttachmentRepository trackingOrgAttachmentRepository,
            OrgAttachmentRepository orgAttachmentRepository,
            UserRepository userRepository,
            AppConfigRepository appConfigRepository,
            LogOrgAttachmentRepository logOrgAttachmentRepository
            )
        {
            _trackingRequestRepository = trackingRequestRepository;
            _trackingComprofileRepository = trackingComprofileRepository;
            _comProfileDataService = comProfileDataService;
            _countryRepository = countryRepository;
            _businessEntityRepository = businessEntityRepository;
            _portalFieldConfigRepository = portalFieldConfigRepository;
            _orgAddressRepository = orgAddressRepository;
            _trackingOrgAddressRepository = trackingOrgAddressRepository;
            _languageRepository = languageRepository;
            _autoComplete_StateRepository = autoComplete_StateRepository;
            _addressRepository = addressRepository;
            _organizationRepository = organizationRepository;
            _customerDocTypeRepository = customerDocTypeRepository;
            _customerDocNameRepository = customerDocNameRepository;
            _comProfileTrackingService = comProfileTrackingService;
            _trackingOrgAttachmentRepository = trackingOrgAttachmentRepository;
            _orgAttachmentRepository = orgAttachmentRepository;
            _userRepository = userRepository;
            _appConfigRepository = appConfigRepository;
            _logOrgAttachmentRepository = logOrgAttachmentRepository;

        }


        // GET: EditDataTracking
        public ActionResult Index()
        {
            return View();
        }



        #region General Page

        public ActionResult General(int trackingReqID = 0, int trackingItemID = 0, string trackingType = "ByRequest")
        {

            try
            {
                // trackingType มี 2 ค่าคือ = ByRequest , ByItem (Fix)
                int supplierID = 0;
                int trackingGrpID = 0;

                var trackingRequestModel = _trackingRequestRepository.GetTrackingRequestByTrackingReqID(trackingReqID);

                supplierID = trackingRequestModel.SupplierID ?? 0;
                trackingGrpID = trackingRequestModel.TrackingGrpID ?? 0;

                var model = _comProfileDataService.GetGeneralComProfileBySupplierID(supplierID);
                model.TrackingReqID = trackingReqID;
                model.TrackingItemID = trackingItemID;
                model.TrackingType = trackingType;
                model.trackingFields = model.GetTrackingStatus<Tbl_Organization>(supplierID);

                #region Add bundleField to list

                var listBundleField = new List<int>();

                if (trackingType == "ByItem")
                {
                    foreach (KeyValuePair<string, TrackingCompProfile> entry in model.trackingFields)
                    {
                        if (entry.Value.TrackingItemID == trackingItemID)
                        {
                            var fieldConfig = _portalFieldConfigRepository.GetdataForcheckRef(entry.Value.FieldID ?? 0);
                            if (fieldConfig != null)
                            {
                                string bundleField = fieldConfig.BundleFieldID;

                                if (!string.IsNullOrEmpty(bundleField))
                                {
                                    string[] bundleList = bundleField.Split(',');
                                    foreach (var list in bundleList)
                                    {
                                        if (!string.IsNullOrEmpty(list) && list.Trim() != "0")
                                        {
                                            int bundleFieldID = Convert.ToInt32(list);
                                            listBundleField.Add(bundleFieldID);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                #endregion

                #region Add value model from data tracking

                if (model.trackingFields["CompanyTypeID"].TrackingStatusID == 2 || model.trackingFields["CompanyTypeID"].TrackingStatusID == 5)
                {
                    model.CompanyTypeID = int.Parse(model.trackingFields["CompanyTypeID"].NewKeyValue);

                    #region Set EditTracking And BundleFieldID Tracking
                    if (trackingType == "ByRequest")
                    {
                        if (model.trackingFields["CompanyTypeID"].ReqId == trackingReqID)
                        {
                            model.trackingFields["CompanyTypeID"].EditTracking = true;
                        }
                    }
                    else
                    {
                        //trackingType By trackingItemID
                        if (model.trackingFields["CompanyTypeID"].TrackingItemID == trackingItemID)
                        {
                            model.trackingFields["CompanyTypeID"].EditTracking = true;

                        }
                        else
                        {
                            // เชคว่า Field มีอยู่ในรายการ BundleField หรือเปล่า
                            if (listBundleField.Contains(model.trackingFields["CompanyTypeID"].FieldID ?? 0))
                            {
                                model.trackingFields["CompanyTypeID"].EditTracking = true;
                            }
                        }
                    }
                    #endregion
                }
                if (model.trackingFields["CountryCode"].TrackingStatusID == 2 || model.trackingFields["CountryCode"].TrackingStatusID == 5)
                {
                    model.CountryCode = model.trackingFields["CountryCode"].NewKeyValue;

                    string countryName = _countryRepository.GetCountryNameByCountryCode(model.CountryCode);
                    model.CountryName = countryName;

                    #region Set EditTracking And BundleFieldID Tracking
                    if (trackingType == "ByRequest")
                    {
                        if (model.trackingFields["CountryCode"].ReqId == trackingReqID)
                        {
                            model.trackingFields["CountryCode"].EditTracking = true;
                        }
                    }
                    else
                    {
                        //trackingType By trackingItemID
                        if (model.trackingFields["CountryCode"].TrackingItemID == trackingItemID)
                        {
                            model.trackingFields["CountryCode"].EditTracking = true;

                        }
                        else
                        {
                            // เชคว่า Field มีอยู่ในรายการ BundleField หรือเปล่า
                            if (listBundleField.Contains(model.trackingFields["CountryCode"].FieldID ?? 0))
                            {
                                model.trackingFields["CountryCode"].EditTracking = true;
                            }
                        }
                    }
                    #endregion
                }
                if (model.trackingFields["TaxID"].TrackingStatusID == 2  || model.trackingFields["TaxID"].TrackingStatusID == 5)
                {
                    model.TaxID = model.trackingFields["TaxID"].NewKeyValue;

                    #region Set EditTracking And BundleFieldID Tracking
                    if (trackingType == "ByRequest")
                    {
                        if (model.trackingFields["TaxID"].ReqId == trackingReqID)
                        {
                            model.trackingFields["TaxID"].EditTracking = true;
                        }
                    }
                    else
                    {
                        //trackingType By trackingItemID
                        if (model.trackingFields["TaxID"].TrackingItemID == trackingItemID)
                        {
                            model.trackingFields["TaxID"].EditTracking = true;

                        }
                        else
                        {
                            // เชคว่า Field มีอยู่ในรายการ BundleField หรือเปล่า
                            if (listBundleField.Contains(model.trackingFields["TaxID"].FieldID ?? 0))
                            {
                                model.trackingFields["TaxID"].EditTracking = true;
                            }
                        }
                    }
                    #endregion

                }
                if (model.trackingFields["DUNSNumber"].TrackingStatusID == 2 || model.trackingFields["DUNSNumber"].TrackingStatusID == 5)
                {
                    model.DUNSNumber = model.trackingFields["DUNSNumber"].NewKeyValue;

                    #region Set EditTracking And BundleFieldID Tracking
                    if (trackingType == "ByRequest")
                    {
                        if (model.trackingFields["DUNSNumber"].ReqId == trackingReqID)
                        {
                            model.trackingFields["DUNSNumber"].EditTracking = true;
                        }
                    }
                    else
                    {
                        //trackingType By trackingItemID
                        if (model.trackingFields["DUNSNumber"].TrackingItemID == trackingItemID)
                        {
                            model.trackingFields["DUNSNumber"].EditTracking = true;

                        }
                        else
                        {
                            // เชคว่า Field มีอยู่ในรายการ BundleField หรือเปล่า
                            if (listBundleField.Contains(model.trackingFields["DUNSNumber"].FieldID ?? 0))
                            {
                                model.trackingFields["DUNSNumber"].EditTracking = true;
                            }
                        }
                    }
                    #endregion

                }
                if (model.trackingFields["BusinessEntityID"].TrackingStatusID == 2 || model.trackingFields["BusinessEntityID"].TrackingStatusID == 5)
                {
                    model.BusinessEntityID = int.Parse(model.trackingFields["BusinessEntityID"].NewKeyValue);

                    #region Set EditTracking And BundleFieldID Tracking
                    if (trackingType == "ByRequest")
                    {
                        if (model.trackingFields["BusinessEntityID"].ReqId == trackingReqID)
                        {
                            model.trackingFields["BusinessEntityID"].EditTracking = true;
                        }
                    }
                    else
                    {
                        //trackingType By trackingItemID
                        if (model.trackingFields["BusinessEntityID"].TrackingItemID == trackingItemID)
                        {
                            model.trackingFields["BusinessEntityID"].EditTracking = true;

                        }
                        else
                        {
                            // เชคว่า Field มีอยู่ในรายการ BundleField หรือเปล่า
                            if (listBundleField.Contains(model.trackingFields["BusinessEntityID"].FieldID ?? 0))
                            {
                                model.trackingFields["BusinessEntityID"].EditTracking = true;
                            }
                        }
                    }
                    #endregion

                    #region Set OtherBusinessEntity
                    if (model.BusinessEntityID == -1)
                    {
                        var otherData = _trackingComprofileRepository.GetDataBySupplierIDandTrackingKey(model.SupplierID, "BusinessEntityID", "Tbl_Organization");
                        foreach (var item in otherData)
                        {
                            model.OtherBusinessEntity = item.NewKeyValue.Split('|').LastOrDefault();
                        }
                        if (model.trackingFields["BusinessEntityID"].ReqId == trackingReqID)
                        {
                            model.trackingFields["OtherBusinessEntity"].EditTracking = true;
                        }
                        //model.trackingFields["OtherBusinessEntity"].EditTracking = true;
                    }
                    #endregion

                }
                if (model.trackingFields["CompanyName_Local"].TrackingStatusID == 2 || model.trackingFields["CompanyName_Local"].TrackingStatusID == 5)
                {
                    model.CompanyName_Local = model.trackingFields["CompanyName_Local"].NewKeyValue;

                    #region Set EditTracking And BundleFieldID Tracking
                    if (trackingType == "ByRequest")
                    {
                        if (model.trackingFields["CompanyName_Local"].ReqId == trackingReqID)
                        {
                            model.trackingFields["CompanyName_Local"].EditTracking = true;
                        }
                    }
                    else
                    {
                        //trackingType By trackingItemID
                        if (model.trackingFields["CompanyName_Local"].TrackingItemID == trackingItemID)
                        {
                            model.trackingFields["CompanyName_Local"].EditTracking = true;

                        }
                        else
                        {
                            // เชคว่า Field มีอยู่ในรายการ BundleField หรือเปล่า
                            if (listBundleField.Contains(model.trackingFields["CompanyName_Local"].FieldID ?? 0))
                            {
                                model.trackingFields["CompanyName_Local"].EditTracking = true;
                            }
                        }
                    }
                    #endregion
                }
                if (model.trackingFields["CompanyName_Inter"].TrackingStatusID == 2 || model.trackingFields["CompanyName_Inter"].TrackingStatusID == 5)
                {
                    model.CompanyName_Inter = model.trackingFields["CompanyName_Inter"].NewKeyValue;

                    #region Set EditTracking And BundleFieldID Tracking
                    if (trackingType == "ByRequest")
                    {
                        if (model.trackingFields["CompanyName_Inter"].ReqId == trackingReqID)
                        {
                            model.trackingFields["CompanyName_Inter"].EditTracking = true;
                        }
                    }
                    else
                    {
                        //trackingType By trackingItemID
                        if (model.trackingFields["CompanyName_Inter"].TrackingItemID == trackingItemID)
                        {
                            model.trackingFields["CompanyName_Inter"].EditTracking = true;

                        }
                        else
                        {
                            // เชคว่า Field มีอยู่ในรายการ BundleField หรือเปล่า
                            if (listBundleField.Contains(model.trackingFields["CompanyName_Inter"].FieldID ?? 0))
                            {
                                model.trackingFields["CompanyName_Inter"].EditTracking = true;
                            }
                        }
                    }
                    #endregion
                }
                if (model.trackingFields["BranchNo"].TrackingStatusID == 2 || model.trackingFields["BranchNo"].TrackingStatusID == 5)
                {
                    model.BranchNo = model.trackingFields["BranchNo"].NewKeyValue;

                    #region Set EditTracking And BundleFieldID Tracking
                    if (trackingType == "ByRequest")
                    {
                        if (model.trackingFields["BranchNo"].ReqId == trackingReqID)
                        {
                            model.trackingFields["BranchNo"].EditTracking = true;
                        }
                    }
                    else
                    {
                        //trackingType By trackingItemID
                        if (model.trackingFields["BranchNo"].TrackingItemID == trackingItemID)
                        {
                            model.trackingFields["BranchNo"].EditTracking = true;

                        }
                        else
                        {
                            // เชคว่า Field มีอยู่ในรายการ BundleField หรือเปล่า
                            if (listBundleField.Contains(model.trackingFields["BranchNo"].FieldID ?? 0))
                            {
                                model.trackingFields["BranchNo"].EditTracking = true;
                            }
                        }
                    }
                    #endregion
                }
                if (model.trackingFields["BranchName_Local"].TrackingStatusID == 2 || model.trackingFields["BranchName_Local"].TrackingStatusID == 5)
                {
                    model.BranchName_Local = model.trackingFields["BranchName_Local"].NewKeyValue;

                    #region Set EditTracking And BundleFieldID Tracking
                    if (trackingType == "ByRequest")
                    {
                        if (model.trackingFields["BranchName_Local"].ReqId == trackingReqID)
                        {
                            model.trackingFields["BranchName_Local"].EditTracking = true;
                        }
                    }
                    else
                    {
                        //trackingType By trackingItemID
                        if (model.trackingFields["BranchName_Local"].TrackingItemID == trackingItemID)
                        {
                            model.trackingFields["BranchName_Local"].EditTracking = true;

                        }
                        else
                        {
                            // เชคว่า Field มีอยู่ในรายการ BundleField หรือเปล่า
                            if (listBundleField.Contains(model.trackingFields["BranchName_Local"].FieldID ?? 0))
                            {
                                model.trackingFields["BranchName_Local"].EditTracking = true;
                            }
                        }
                    }
                    #endregion
                }

                if (model.trackingFields["BranchName_Inter"].TrackingStatusID == 2 || model.trackingFields["BranchName_Inter"].TrackingStatusID == 5)
                {
                    model.BranchName_Inter = model.trackingFields["BranchName_Inter"].NewKeyValue;

                    #region Set EditTracking And BundleFieldID Tracking
                    if (trackingType == "ByRequest")
                    {
                        if (model.trackingFields["BranchName_Inter"].ReqId == trackingReqID)
                        {
                            model.trackingFields["BranchName_Inter"].EditTracking = true;
                        }
                    }
                    else
                    {
                        //trackingType By trackingItemID
                        if (model.trackingFields["BranchName_Inter"].TrackingItemID == trackingItemID)
                        {
                            model.trackingFields["BranchName_Inter"].EditTracking = true;

                        }
                        else
                        {
                            // เชคว่า Field มีอยู่ในรายการ BundleField หรือเปล่า
                            if (listBundleField.Contains(model.trackingFields["BranchName_Inter"].FieldID ?? 0))
                            {
                                model.trackingFields["BranchName_Inter"].EditTracking = true;
                            }
                        }
                    }
                    #endregion
                }

                #endregion



                //แปลง model.trackingFields เป็น Json Format เพื่อนส่งกลับเวลากด Save Post form กลับมา
                string jsonStr = JsonConvert.SerializeObject(model.trackingFields);
                model.JsonStr = jsonStr;
                return View(model);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }
        }

        //public ActionResult CountryCompanyPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyGeneralModel model)
        //{

        //    ViewData["CountryList"] = _countryRepository.GetCountryList();
        //    return PartialView("_CountryCompanyPartial", model);
        //}

        //public ActionResult BusinessEntityPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyGeneralModel model)
        //{
        //    var corperate = Request.Params["CorperateValue"] as string ?? string.Empty;

        //    int companyTypeID = 1;
        //    if (!string.IsNullOrEmpty(corperate))
        //    {
        //        companyTypeID = Convert.ToInt32(corperate);
        //    }
        //    else if (model.CompanyTypeID != null)
        //    {
        //        companyTypeID = model.CompanyTypeID ?? 1;
        //    }

        //    string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

        //    int languageId = Convert.ToInt32(languageID);

        //    ViewData["BusinessEntityList"] = _businessEntityRepository.GetMultiLangBusinessEntity(companyTypeID, languageId);

        //    return PartialView("_BusinessEntityPartial", model);
        //}

        public ActionResult BusinessEntityPartialNew(CompanyGeneralModel model)
        {
            //,Dictionary<string,object> customAttr
            var corperate = Request.Params["CorperateValue"] as string ?? string.Empty;

            int companyTypeID = 1;
            if (!string.IsNullOrEmpty(corperate))
            {
                companyTypeID = Convert.ToInt32(corperate);
            }
            else if (model.CompanyTypeID != null)
            {
                companyTypeID = model.CompanyTypeID ?? 1;
            }

            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            int languageId = Convert.ToInt32(languageID);

            ViewBag.BusinessEntityList = _businessEntityRepository.GetMultiLangBusinessEntity(companyTypeID, languageId);

            var data = model.GetTrackingStatus<Tbl_Organization>(model.SupplierID);

            ViewBag.cusAttr = data;

            return PartialView(@"~/Views/EditDataTracking/_BusinessEntityPartialNew.cshtml", model);
        }

        [HttpPost]
        public ActionResult GeneralSave(CompanyGeneralModel model)
        {
            try
            {

                string username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
                var user = _userRepository.FindByUsername(username);
                int userID = user.UserID;

                // ค่า model.trackingFields ที่แปลงเป็น Json
                Dictionary<string, TrackingCompProfile> trackingFields = JsonConvert.DeserializeObject<Dictionary<string, TrackingCompProfile>>(model.JsonStr);

                foreach (KeyValuePair<string, TrackingCompProfile> entry in trackingFields)
                {

                    if (entry.Value.EditTracking)
                    {
                        var trackingModel = _trackingComprofileRepository.GetTrackingCompProfileWaitingApproveByTrackingItemID(entry.Value.TrackingItemID);
                        string oldNewKeyValue = "";
                        if (trackingModel != null)
                        {
                            #region Check trackingFields status 5 change
                            oldNewKeyValue = trackingModel.NewKeyValue;
                            #endregion

                            #region Update new value from trackingFields
                            if (entry.Key == "CompanyTypeID")
                            {
                                trackingModel.NewKeyValue = model.CompanyTypeID.ToString();
                            }
                            if (entry.Key == "CountryCode")
                            {
                                trackingModel.NewKeyValue = model.CountryCode ?? "";
                            }
                            if (entry.Key == "TaxID")
                            {
                                trackingModel.NewKeyValue = model.TaxID ?? "";
                            }
                            if (entry.Key == "DUNSNumber")
                            {
                                trackingModel.NewKeyValue = model.DUNSNumber??"";
                            }
                            if (entry.Key == "BusinessEntityID")
                            {
                                if (model.BusinessEntityID == -1)
                                {
                                    trackingModel.NewKeyValue = model.BusinessEntityID.ToString() + "|" + model.OtherBusinessEntity ?? "";
                                }
                                else
                                {
                                    trackingModel.NewKeyValue = model.BusinessEntityID.ToString();
                                }
                                
                            }
                            if (entry.Key == "CompanyName_Local")
                            {
                                trackingModel.NewKeyValue = model.CompanyName_Local ?? "";
                            }
                            if (entry.Key == "CompanyName_Inter")
                            {
                                trackingModel.NewKeyValue = model.CompanyName_Inter ?? "";
                            }
                            if (entry.Key == "BranchNo")
                            {
                                trackingModel.NewKeyValue = model.BranchNo ?? "";
                            }
                            if (entry.Key == "BranchName_Local")
                            {
                                trackingModel.NewKeyValue = model.BranchName_Local ?? "";
                            }

                            if (entry.Key == "BranchName_Inter")
                            {
                                trackingModel.NewKeyValue = model.BranchName_Inter ?? "";
                            }
                            #endregion

                            #region Check trackingFields status 5 change

                            if(trackingModel.TrackingStatusID == 5)
                            {
                                if(oldNewKeyValue.Trim() != trackingModel.NewKeyValue.Trim())
                                {
                                    trackingModel.TrackingStatusID = 2;
                                    trackingModel.TrackingStatusDate = DateTime.UtcNow;
                                }
                            }

                            #endregion

                            _trackingComprofileRepository.Update(trackingModel, userID);
                        }

                        
                    }

                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return RedirectToAction("General", new { trackingReqID = model.TrackingReqID, trackingItemID = model.TrackingItemID, trackingType = model.TrackingType });
        }

        #endregion

        #region Address Page

        public ActionResult Address(int trackingReqID = 0, int trackingItemID = 0, string trackingType = "ByRequest")
        {
            try
            {
                int supplierID = 0;

                var trackingRequestModel = _trackingRequestRepository.GetTrackingRequestByTrackingReqID(trackingReqID);

                supplierID = trackingRequestModel.SupplierID ?? 0;

                #region Get basic address data
                var model = new CompanyAddress();
                int compAddressID = 0;
                int deliverAddressID = 0;
                bool checkModifyDelvAddr = false;
                model.SupplierID = supplierID;

                var orgCompanyAddressMapping = _orgAddressRepository.GetAddresType(supplierID, 1);
                var orgDeliverAddressMapping = _orgAddressRepository.GetAddresType(supplierID, 2);
                if (orgCompanyAddressMapping != null)
                {
                    compAddressID = orgCompanyAddressMapping.AddressID;
                }
                if (orgDeliverAddressMapping != null)
                {
                    var deliverAddressTracking = _trackingOrgAddressRepository.GetLastDataBySupplierID(supplierID);
                    if (deliverAddressTracking != null)
                    {
                        deliverAddressID = deliverAddressTracking.NewAddressID ?? 0;
                        //มีข้อมูล Tracking Address ที่ AddressTypeID = 2
                        checkModifyDelvAddr = true;
                    }
                    else
                    {
                        deliverAddressID = orgDeliverAddressMapping.AddressID;
                    }
                }
                else
                {
                    deliverAddressID = orgCompanyAddressMapping.AddressID;
                }

                #endregion

                #region Get Tracking Company Address
                model.trackingFields = model.GetTrackingStatus<Tbl_Address>(compAddressID);

                var orgCompanyAddress = _addressRepository.GetAddressByAddressID(compAddressID);

                #region Add value to model.CompanyAddress
                if (orgCompanyAddress != null)
                {
                    string companyAddress_CountryCode = _countryRepository.GetCountryNameByCountryCode(orgCompanyAddress.CountryCode);

                    model.CompanyAddress_HouseNo_Local = orgCompanyAddress.HouseNo_Local;
                    model.CompanyAddress_HouseNo_Inter = orgCompanyAddress.HouseNo_Inter;
                    model.CompanyAddress_VillageNo_Local = orgCompanyAddress.VillageNo_Local;
                    model.CompanyAddress_VillageNo_Inter = orgCompanyAddress.VillageNo_Inter;
                    model.CompanyAddress_Lane_Local = orgCompanyAddress.Lane_Local;
                    model.CompanyAddress_Lane_Inter = orgCompanyAddress.Lane_Inter;
                    model.CompanyAddress_Road_Local = orgCompanyAddress.Road_Local;
                    model.CompanyAddress_Road_Inter = orgCompanyAddress.Road_Inter;
                    model.CompanyAddress_SubDistrict_Local = orgCompanyAddress.SubDistrict_Local;
                    model.CompanyAddress_SubDistrict_Inter = orgCompanyAddress.SubDistrict_Inter;
                    model.CompanyAddress_City_Local = orgCompanyAddress.City_Local;
                    model.CompanyAddress_City_Inter = orgCompanyAddress.City_Inter;
                    model.CompanyAddress_State_Local = orgCompanyAddress.State_Local;
                    model.CompanyAddress_State_Inter = orgCompanyAddress.State_Inter;
                    model.CompanyAddress_PostalCode = orgCompanyAddress.PostalCode;
                    model.CompanyAddress_CountryCode = orgCompanyAddress.CountryCode;
                    model.CompanyAddress_CountryName = companyAddress_CountryCode;
                }

                #endregion

                #region Get tracking data
                // ใน Addres จะมี BundleField กันทุก Field เลยไม่ต้องเชคแยก Tracking
                if (orgCompanyAddress != null)
                {
                    model.CompanyAddressID = compAddressID;

                    if (model.trackingFields["HouseNo_Local"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_HouseNo_Local = model.trackingFields["HouseNo_Local"].NewKeyValue;
                        model.trackingFields["HouseNo_Local"].EditTracking = true;
                    }

                    if (model.trackingFields["HouseNo_Inter"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_HouseNo_Inter = model.trackingFields["HouseNo_Inter"].NewKeyValue;
                        model.trackingFields["HouseNo_Inter"].EditTracking = true;
                    }

                    if (model.trackingFields["VillageNo_Local"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_VillageNo_Local = model.trackingFields["VillageNo_Local"].NewKeyValue;
                        model.trackingFields["VillageNo_Local"].EditTracking = true;
                    }

                    if (model.trackingFields["VillageNo_Inter"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_VillageNo_Inter = model.trackingFields["VillageNo_Inter"].NewKeyValue;
                        model.trackingFields["VillageNo_Inter"].EditTracking = true;
                    }

                    if (model.trackingFields["Lane_Local"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_Lane_Local = model.trackingFields["Lane_Local"].NewKeyValue;
                        model.trackingFields["Lane_Local"].EditTracking = true;
                    }

                    if (model.trackingFields["Lane_Inter"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_Lane_Inter = model.trackingFields["Lane_Inter"].NewKeyValue;
                        model.trackingFields["Lane_Inter"].EditTracking = true;
                    }

                    if (model.trackingFields["Road_Local"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_Road_Local = model.trackingFields["Road_Local"].NewKeyValue;
                        model.trackingFields["Road_Local"].EditTracking = true;
                    }

                    if (model.trackingFields["Road_Inter"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_Road_Inter = model.trackingFields["Road_Inter"].NewKeyValue;
                        model.trackingFields["Road_Inter"].EditTracking = true;
                    }

                    if (model.trackingFields["SubDistrict_Local"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_SubDistrict_Local = model.trackingFields["SubDistrict_Local"].NewKeyValue;
                        model.trackingFields["SubDistrict_Local"].EditTracking = true;
                    }

                    if (model.trackingFields["SubDistrict_Inter"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_SubDistrict_Inter = model.trackingFields["SubDistrict_Inter"].NewKeyValue;
                        model.trackingFields["SubDistrict_Inter"].EditTracking = true;
                    }

                    if (model.trackingFields["City_Local"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_City_Local = model.trackingFields["City_Local"].NewKeyValue;
                        model.trackingFields["City_Local"].EditTracking = true;
                    }

                    if (model.trackingFields["City_Inter"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_City_Inter = model.trackingFields["City_Inter"].NewKeyValue;
                        model.trackingFields["City_Inter"].EditTracking = true;
                    }

                    if (model.trackingFields["State_Local"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_State_Local = model.trackingFields["State_Local"].NewKeyValue;
                        model.trackingFields["State_Local"].EditTracking = true;
                    }

                    if (model.trackingFields["State_Inter"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_State_Inter = model.trackingFields["State_Inter"].NewKeyValue;
                        model.trackingFields["State_Inter"].EditTracking = true;
                    }

                    if (model.trackingFields["PostalCode"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_PostalCode = model.trackingFields["PostalCode"].NewKeyValue;
                        model.trackingFields["PostalCode"].EditTracking = true;
                    }

                    if (model.trackingFields["CountryCode"].TrackingStatusID == 2)
                    {
                        model.CompanyAddress_CountryCode = model.trackingFields["CountryCode"].NewKeyValue;
                        model.trackingFields["CountryCode"].EditTracking = true;
                    }

                }
                #endregion

                #endregion

                #region Get Tracking Deliver Address
                if (deliverAddressID != compAddressID)
                {
                    //get data trackingstatus
                    model.trackingFieldsDel = model.GetTrackingStatus<Tbl_Address>(deliverAddressID);
                }
                var orgDeliverAddress = _addressRepository.GetAddressByAddressID(deliverAddressID);

                #region Add value to model.DeliverAddress
                if (orgDeliverAddress != null)
                {
                    string deliverAddress_CountryName = _countryRepository.GetCountryNameByCountryCode(orgDeliverAddress.CountryCode);

                    model.DeliverAddressID = deliverAddressID;
                    model.DeliverAddress_HouseNo_Local = orgDeliverAddress.HouseNo_Local;
                    model.DeliverAddress_HouseNo_Inter = orgDeliverAddress.HouseNo_Inter;
                    model.DeliverAddress_VillageNo_Local = orgDeliverAddress.VillageNo_Local;
                    model.DeliverAddress_VillageNo_Inter = orgDeliverAddress.VillageNo_Inter;
                    model.DeliverAddress_Lane_Local = orgDeliverAddress.Lane_Local;
                    model.DeliverAddress_Lane_Inter = orgDeliverAddress.Lane_Inter;
                    model.DeliverAddress_Road_Local = orgDeliverAddress.Road_Local;
                    model.DeliverAddress_Road_Inter = orgDeliverAddress.Road_Inter;
                    model.DeliverAddress_SubDistrict_Local = orgDeliverAddress.SubDistrict_Local;
                    model.DeliverAddress_SubDistrict_Inter = orgDeliverAddress.SubDistrict_Inter;
                    model.DeliverAddress_City_Local = orgDeliverAddress.City_Local;
                    model.DeliverAddress_City_Inter = orgDeliverAddress.City_Inter;
                    model.DeliverAddress_State_Local = orgDeliverAddress.State_Local;
                    model.DeliverAddress_State_Inter = orgDeliverAddress.State_Inter;
                    model.DeliverAddress_PostalCode = orgDeliverAddress.PostalCode;
                    model.DeliverAddress_CountryCode = orgDeliverAddress.CountryCode;
                    model.DeliverAddress_CountryName = deliverAddress_CountryName;
                }
                #endregion

                #region Get tracking data
                if (model.trackingFieldsDel != null)
                {
                    model.DeliverAddressID = deliverAddressID;
                    if (model.trackingFieldsDel["HouseNo_Local"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_HouseNo_Local = model.trackingFieldsDel["HouseNo_Local"].NewKeyValue;
                        model.trackingFieldsDel["HouseNo_Local"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["HouseNo_Inter"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_HouseNo_Inter = model.trackingFieldsDel["HouseNo_Inter"].NewKeyValue;
                        model.trackingFieldsDel["HouseNo_Inter"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["VillageNo_Local"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_VillageNo_Local = model.trackingFieldsDel["VillageNo_Local"].NewKeyValue;
                        model.trackingFieldsDel["VillageNo_Local"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["VillageNo_Inter"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_VillageNo_Inter = model.trackingFieldsDel["VillageNo_Inter"].NewKeyValue;
                        model.trackingFieldsDel["VillageNo_Inter"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["Lane_Local"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_Lane_Local = model.trackingFieldsDel["Lane_Local"].NewKeyValue;
                        model.trackingFieldsDel["Lane_Local"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["Lane_Inter"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_Lane_Inter = model.trackingFieldsDel["Lane_Inter"].NewKeyValue;
                        model.trackingFieldsDel["Lane_Inter"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["Road_Local"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_Road_Local = model.trackingFieldsDel["Road_Local"].NewKeyValue;
                        model.trackingFieldsDel["Road_Local"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["Road_Inter"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_Road_Inter = model.trackingFieldsDel["Road_Inter"].NewKeyValue;
                        model.trackingFieldsDel["Road_Inter"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["SubDistrict_Local"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_SubDistrict_Local = model.trackingFieldsDel["SubDistrict_Local"].NewKeyValue;
                        model.trackingFieldsDel["SubDistrict_Local"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["SubDistrict_Inter"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_SubDistrict_Inter = model.trackingFieldsDel["SubDistrict_Inter"].NewKeyValue;
                        model.trackingFieldsDel["SubDistrict_Inter"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["City_Local"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_City_Local = model.trackingFieldsDel["City_Local"].NewKeyValue;
                        model.trackingFieldsDel["City_Local"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["City_Inter"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_City_Inter = model.trackingFieldsDel["City_Inter"].NewKeyValue;
                        model.trackingFieldsDel["City_Inter"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["State_Local"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_State_Local = model.trackingFieldsDel["State_Local"].NewKeyValue;
                        model.trackingFieldsDel["State_Local"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["State_Inter"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_State_Inter = model.trackingFieldsDel["State_Inter"].NewKeyValue;
                        model.trackingFieldsDel["State_Inter"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["PostalCode"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_PostalCode = model.trackingFieldsDel["PostalCode"].NewKeyValue;
                        model.trackingFieldsDel["PostalCode"].EditTracking = true;
                    }

                    if (model.trackingFieldsDel["CountryCode"].TrackingStatusID == 2)
                    {
                        model.DeliverAddress_CountryCode = model.trackingFieldsDel["CountryCode"].NewKeyValue;
                        model.trackingFieldsDel["CountryCode"].EditTracking = true;
                    }
                }
                else
                {
                    model.trackingFieldsDel = model.GetTrackingStatus<Tbl_Address>(0);
                }
                #endregion



                #endregion

                if (compAddressID == deliverAddressID)
                {
                    model.CheckB = true;
                }
                else
                {
                    model.CheckB = false;
                }
                //เชคว่า Delivered Address มีการเปลี่ยนแปลงหรือไม่
                model.CheckModifyDelvAddr = checkModifyDelvAddr;
                //แปลง model.trackingFields เป็น Json Format เพื่อนส่งกลับเวลากด Save Post form กลับมา
                string jsonStr = JsonConvert.SerializeObject(model.trackingFields);
                model.JsonStr = jsonStr;
                string jsonStrDel = JsonConvert.SerializeObject(model.trackingFieldsDel);
                model.JsonStrDel = jsonStrDel;


                //เช็ค diable ส่วนที่ไม่ได้แก้ไข 02-02-2017
                var dataOrgAddressTrackingForChkDisable = _trackingOrgAddressRepository.GetTrackingContactAddressByTrackingReqID(trackingReqID);
                var chkDisableEditData = "";
                if (dataOrgAddressTrackingForChkDisable.Count() > 0 && dataOrgAddressTrackingForChkDisable.Count() == 1)
                {
                    var dataOrgAddressTracking = dataOrgAddressTrackingForChkDisable.FirstOrDefault();
                    if (dataOrgAddressTracking.OldAddressID != dataOrgAddressTracking.NewAddressID)
                    {
                        chkDisableEditData = "editDeliver";
                    }
                    else if (dataOrgAddressTracking.AdressTypeID == 2)
                    {
                        chkDisableEditData = "editDeliver";
                    }
                    else
                    {
                        chkDisableEditData = "editAddress";
                    }
                    ViewData["chkDisableEditData"] = chkDisableEditData;
                }

                return View(model);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }
            
        }

        [HttpPost]
        public ActionResult AddressSave(CompanyAddress model)
        {
            try
            {
                string username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
                var user = _userRepository.FindByUsername(username);
                int userID = user.UserID;

                #region Update Data Tracking Company Address
                // ค่า model.trackingFields ที่แปลงเป็น Json
                Dictionary<string, TrackingCompProfile> trackingFields = JsonConvert.DeserializeObject<Dictionary<string, TrackingCompProfile>>(model.JsonStr);
                foreach (KeyValuePair<string, TrackingCompProfile> entry in trackingFields)
                {
                    if (entry.Value.EditTracking)
                    {
                        var trackingModel = _trackingComprofileRepository.GetTrackingCompProfileWaitingApproveByTrackingItemID(entry.Value.TrackingItemID);
                        if (trackingModel != null)
                        {
                            #region Update new value from trackingFields
                            if (entry.Key == "HouseNo_Local")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_HouseNo_Local??"";
                            }
                            if (entry.Key == "HouseNo_Inter")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_HouseNo_Inter ?? "";
                            }
                            if (entry.Key == "VillageNo_Local")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_VillageNo_Local ?? "";
                            }
                            if (entry.Key == "VillageNo_Inter")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_VillageNo_Inter ?? "";
                            }
                            if (entry.Key == "Lane_Local")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_Lane_Local ?? "";
                            }
                            if (entry.Key == "Lane_Inter")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_Lane_Inter ?? "";
                            }
                            if (entry.Key == "Road_Local")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_Road_Local ?? "";
                            }
                            if (entry.Key == "Road_Inter")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_Road_Inter ?? "";
                            }
                            if (entry.Key == "SubDistrict_Local")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_SubDistrict_Local ?? "";
                            }
                            if (entry.Key == "SubDistrict_Inter")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_SubDistrict_Inter ?? "";
                            }
                            if (entry.Key == "City_Local")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_City_Local ?? "";
                            }
                            if (entry.Key == "City_Inter")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_City_Inter ?? "";
                            }
                            if (entry.Key == "State_Local")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_State_Local ?? "";
                            }
                            if (entry.Key == "State_Inter")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_State_Inter ?? "";
                            }
                            if (entry.Key == "PostalCode")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_PostalCode ?? "";
                            }
                            if (entry.Key == "CountryCode")
                            {
                                trackingModel.NewKeyValue = model.CompanyAddress_CountryCode ?? "";
                            }
                            
                            #endregion
                            _trackingComprofileRepository.Update(trackingModel, userID);
                        }
                    }
                }
                #endregion

                if(model.CheckModifyDelvAddr)
                {

                    var delvAddrTracking = _trackingOrgAddressRepository.GetTrackingOrgAddressWaitingApproveByTrackingReqIDAndAddressTypeID(model.TrackingReqID??0, 2);
                    if (delvAddrTracking != null)
                    {
                        
                        if (delvAddrTracking.OldAddressID == delvAddrTracking.NewAddressID)
                        {
                            //แก้จากที่อยู่เดิมที่มี ไม่ได้เกิดจากการแยกที่อยู่
                            #region Update Data Tracking Company Address
                            Dictionary<string, TrackingCompProfile> trackingFieldsDel = JsonConvert.DeserializeObject<Dictionary<string, TrackingCompProfile>>(model.JsonStrDel);
                            foreach (KeyValuePair<string, TrackingCompProfile> entry in trackingFieldsDel)
                            {
                                if (entry.Value.EditTracking)
                                {
                                    var trackingModel = _trackingComprofileRepository.GetTrackingCompProfileWaitingApproveByTrackingItemID(entry.Value.TrackingItemID);
                                    if (trackingModel != null)
                                    {
                                        #region Update new value from trackingFields
                                        if (entry.Key == "HouseNo_Local")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_HouseNo_Local ?? "";
                                        }
                                        if (entry.Key == "HouseNo_Inter")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_HouseNo_Inter ?? "";
                                        }
                                        if (entry.Key == "VillageNo_Local")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_VillageNo_Local ?? "";
                                        }
                                        if (entry.Key == "VillageNo_Inter")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_VillageNo_Inter ?? "";
                                        }
                                        if (entry.Key == "Lane_Local")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_Lane_Local ?? "";
                                        }
                                        if (entry.Key == "Lane_Inter")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_Lane_Inter ?? "";
                                        }
                                        if (entry.Key == "Road_Local")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_Road_Local ?? "";
                                        }
                                        if (entry.Key == "Road_Inter")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_Road_Inter ?? "";
                                        }
                                        if (entry.Key == "SubDistrict_Local")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_SubDistrict_Local ?? "";
                                        }
                                        if (entry.Key == "SubDistrict_Inter")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_SubDistrict_Inter ?? "";
                                        }
                                        if (entry.Key == "City_Local")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_City_Local ?? "";
                                        }
                                        if (entry.Key == "City_Inter")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_City_Inter ?? "";
                                        }
                                        if (entry.Key == "State_Local")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_State_Local ?? "";
                                        }
                                        if (entry.Key == "State_Inter")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_State_Inter ?? "";
                                        }
                                        if (entry.Key == "PostalCode")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_PostalCode ?? "";
                                        }
                                        if (entry.Key == "CountryCode")
                                        {
                                            trackingModel.NewKeyValue = model.DeliverAddress_CountryCode ?? "";
                                        }

                                        #endregion
                                        _trackingComprofileRepository.Update(trackingModel, userID);
                                    }
                                }
                            }
                            #endregion
                        }else
                        {
                            //แก้จากการแยกที่อยู่แล้วเพิ่มที่อยู่ใหม่ใน Tbl_Address
                            #region Update Tbl_Address
                            var deliverAddress = _addressRepository.GetAddressByAddressID(model.DeliverAddressID ?? 0);
                            if (deliverAddress != null)
                            {
                                deliverAddress.HouseNo_Local = model.DeliverAddress_HouseNo_Local ?? "";
                                deliverAddress.HouseNo_Inter = model.DeliverAddress_HouseNo_Inter ?? "";
                                deliverAddress.VillageNo_Local = model.DeliverAddress_VillageNo_Local ?? "";
                                deliverAddress.VillageNo_Inter = model.DeliverAddress_VillageNo_Inter ?? "";
                                deliverAddress.Lane_Local = model.DeliverAddress_Lane_Local ?? "";
                                deliverAddress.Lane_Inter = model.DeliverAddress_Lane_Inter ?? "";
                                deliverAddress.Road_Local = model.DeliverAddress_Road_Local ?? "";
                                deliverAddress.Road_Inter = model.DeliverAddress_Road_Inter ?? "";
                                deliverAddress.SubDistrict_Local = model.DeliverAddress_SubDistrict_Local ?? "";
                                deliverAddress.SubDistrict_Inter = model.DeliverAddress_SubDistrict_Inter ?? "";
                                deliverAddress.City_Local = model.DeliverAddress_City_Local ?? "";
                                deliverAddress.City_Inter = model.DeliverAddress_City_Inter ?? "";
                                deliverAddress.State_Local = model.DeliverAddress_State_Local ?? "";
                                deliverAddress.State_Inter = model.DeliverAddress_State_Inter ?? "";
                                deliverAddress.CountryCode = model.DeliverAddress_CountryCode ?? "";
                                deliverAddress.PostalCode = model.DeliverAddress_PostalCode ?? "";

                                _addressRepository.UpdateAddress(deliverAddress, userID);
                            }
                            #endregion                           
                        }

                        
                    }
                    
                    
                }
               

            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }
            return RedirectToAction("Address", new { trackingReqID = model.TrackingReqID });
        }

        #region AutoComplete Company Address

        //public ActionResult SubCompLocalPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var compAddrCountry = Request.Params["CompanyAddress_CountryCode"] ?? string.Empty;
        //    string countrycode = "";
        //    if (!string.IsNullOrEmpty(compAddrCountry))
        //    {
        //        countrycode = compAddrCountry;
        //    }

        //    int languageId = 1;
        //    var language = _languageRepository.GetLocalLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }
        //    ViewData["SubDistrictCompLocalLanguageId"] = languageId;
        //    ViewData["SubDistrictCompLocalCountrycode"] = countrycode;

        //    return PartialView("_SubCompLocalPartial", model);
        //}

        //public ActionResult CityCompLocalPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var compAddrCountry = Request.Params["CompanyAddress_CountryCode"] ?? string.Empty;
        //    string countrycode = "";
        //    if (!string.IsNullOrEmpty(compAddrCountry))
        //    {
        //        countrycode = compAddrCountry;
        //    }

        //    int languageId = 1;
        //    var language = _languageRepository.GetLocalLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }

        //    ViewData["CityCompLocalLanguageId"] = languageId;
        //    ViewData["CityCompLocalCountrycode"] = countrycode;

        //    return PartialView("_CityCompLocalPartial", model);
        //}

        //public ActionResult StateCompLocalPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var compAddrCountry = Request.Params["CompanyAddress_CountryCode"] ?? string.Empty;

        //    string countrycode = "";
        //    if (!string.IsNullOrEmpty(compAddrCountry))
        //    {
        //        countrycode = compAddrCountry;
        //    }

        //    int languageId = 1;
        //    var language = _languageRepository.GetLocalLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }

        //    ViewData["StateCompAddrLocal"] = _autoComplete_StateRepository.GetStateByMany(countrycode, Convert.ToInt32(languageId));

        //    return PartialView("_StateCompLocalPartial", model);
        //}

        //public ActionResult SubCompInterPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var compAddrCountry = Request.Params["CompanyAddress_CountryCode"] ?? string.Empty;
        //    string countrycode = "";
        //    if (!string.IsNullOrEmpty(compAddrCountry))
        //    {
        //        countrycode = compAddrCountry;
        //    }

        //    int languageId = 1;
        //    var language = _languageRepository.GetInterLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }
        //    ViewData["SubDistrictCompInterLanguageId"] = languageId;
        //    ViewData["SubDistrictCompInterCountrycode"] = countrycode;

        //    return PartialView("_SubCompInterPartial", model);
        //}

        //public ActionResult CityCompInterPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var compAddrCountry = Request.Params["CompanyAddress_CountryCode"] ?? string.Empty;
        //    string countrycode = "";
        //    if (!string.IsNullOrEmpty(compAddrCountry))
        //    {
        //        countrycode = compAddrCountry;
        //    }

        //    int languageId = 1;
        //    var language = _languageRepository.GetInterLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }

        //    ViewData["CityCompInterLanguageId"] = languageId;
        //    ViewData["CityCompInterCountrycode"] = countrycode;

        //    return PartialView("_CityCompInterPartial", model);
        //}

        //public ActionResult StateCompInterPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var compAddrCountry = Request.Params["CompanyAddress_CountryCode"] ?? string.Empty;

        //    string countrycode = "";
        //    if (!string.IsNullOrEmpty(compAddrCountry))
        //    {
        //        countrycode = compAddrCountry;
        //    }

        //    int languageId = 1;
        //    var language = _languageRepository.GetInterLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }

        //    ViewData["StateCompAddrInter"] = _autoComplete_StateRepository.GetStateByMany(countrycode, Convert.ToInt32(languageId));

        //    return PartialView("_StateCompInterPartial", model);
        //}

        //public ActionResult AddressCompanyContryPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{

        //    int? supId = model.SupplierID;
        //    var data = model.GetTrackingStatus<Tbl_Address>(model.CompanyAddressID ?? 0);
        //    ViewBag.cusAttr = data;
        //    ViewData["CountryList2"] = _countryRepository.GetCountryList();
        //    return PartialView("_AddressCompanyContryPartial", model);
        //}

        #endregion

        #region AutoComplete Delivered Address

        //public ActionResult SubDelvLocalPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var delvAddrCountry = Request.Params["DeliverAddress_CountryCode"] ?? string.Empty;
        //    string countrycode = "";
        //    if (!string.IsNullOrEmpty(delvAddrCountry))
        //    {
        //        countrycode = delvAddrCountry;
        //    }

        //    int languageId = 1;
        //    var language = _languageRepository.GetLocalLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }
        //    ViewData["SubDistrictDelvLocalLanguageId"] = languageId;
        //    ViewData["SubDistrictDelvLocalCountrycode"] = countrycode;
            
        //    return PartialView("_SubDelvLocalPartial", model);
        //}

        //public ActionResult CityDelvLocalPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var delvAddrCountry = Request.Params["DeliverAddress_CountryCode"] ?? string.Empty;
        //    string countrycode = "";

        //    if (!string.IsNullOrEmpty(delvAddrCountry))
        //    {
        //        countrycode = delvAddrCountry;
        //    }
        //    int languageId = 1;
        //    var language = _languageRepository.GetLocalLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }

        //    ViewData["CityDelvLocalLanguageId"] = languageId;
        //    ViewData["CityDelvLocalCountrycode"] = countrycode;

        //    return PartialView("_CityDelvLocalPartial", model);
        //}

        //public ActionResult StateDelvLocalPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var delvAddrCountry = Request.Params["DeliverAddress_CountryCode"] ?? string.Empty;

        //    string countrycode = "";
        //    if (!string.IsNullOrEmpty(delvAddrCountry))
        //    {
        //        countrycode = delvAddrCountry;
        //    }

        //    int languageId = 1;
        //    var language = _languageRepository.GetLocalLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }

        //    ViewData["StateDelvAddrLocal"] = _autoComplete_StateRepository.GetStateByMany(countrycode, Convert.ToInt32(languageId));

        //    return PartialView("_StateDelvLocalPartial", model);
        //}

        //public ActionResult SubDelvInterPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var delvAddrCountry = Request.Params["DeliverAddress_CountryCode"] ?? string.Empty;
        //    string countrycode = "";
        //    if (!string.IsNullOrEmpty(delvAddrCountry))
        //    {
        //        countrycode = delvAddrCountry;
        //    }

        //    int languageId = 1;
        //    var language = _languageRepository.GetInterLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }
        //    ViewData["SubDistrictDelvInterLanguageId"] = languageId;
        //    ViewData["SubDistrictDelvInterCountrycode"] = countrycode;

        //    return PartialView("_SubDelvInterPartial", model);
        //}

        //public ActionResult CityDelvInterPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var delvAddrCountry = Request.Params["DeliverAddress_CountryCode"] ?? string.Empty;
        //    string countrycode = "";

        //    if (!string.IsNullOrEmpty(delvAddrCountry))
        //    {
        //        countrycode = delvAddrCountry;
        //    }
        //    int languageId = 1;
        //    var language = _languageRepository.GetInterLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }

        //    ViewData["CityDelvInterLanguageId"] = languageId;
        //    ViewData["CityDelvInterCountrycode"] = countrycode;
 
        //    return PartialView("_CityDelvInterPartial", model);
        //}

        //public ActionResult StateDelvInterPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    var delvAddrCountry = Request.Params["DeliverAddress_CountryCode"] ?? string.Empty;

        //    string countrycode = "";
        //    if (!string.IsNullOrEmpty(delvAddrCountry))
        //    {
        //        countrycode = delvAddrCountry;
        //    }

        //    int languageId = 1;
        //    var language = _languageRepository.GetInterLanguage();
        //    if (language != null)
        //    {
        //        languageId = language.LanguageID;
        //    }

        //    ViewData["StateDelvAddrInter"] = _autoComplete_StateRepository.GetStateByMany(countrycode, Convert.ToInt32(languageId));
  
        //    return PartialView("_StateDelvInterPartial", model);
        //}

        //public ActionResult AddressDeliveContryPartial([ModelBinder(typeof(DevExpressEditorsBinder))] CompanyAddress model)
        //{
        //    int? supId = model.SupplierID;
        //    if (model.DeliverAddressID != model.CompanyAddressID)
        //    {
        //        var data = model.GetTrackingStatus<Tbl_Address>(model.DeliverAddressID ?? 0);
        //        ViewBag.cusAttr = data;
        //    }
        //    else
        //    {
        //        var data = model.GetTrackingStatus<Tbl_Address>(0);
        //        ViewBag.cusAttr = data;
        //    }

        //    ViewData["CountryList2"] = _countryRepository.GetCountryList();
        //    return PartialView("_AddressDeliveContryPartial", model);
        //}

        #endregion

        #endregion

        #region Document Page

        public ActionResult Document(int trackingReqID = 0, int trackingItemID = 0, string trackingType = "ByRequest")
        {
            try
            {
                // trackingType มี 2 ค่าคือ = ByRequest , ByItem (Fix)
                int supplierID = 0;
                int trackingGrpID = 0;

                var trackingRequestModel = _trackingRequestRepository.GetTrackingRequestByTrackingReqID(trackingReqID);

                supplierID = trackingRequestModel.SupplierID ?? 0;
                trackingGrpID = trackingRequestModel.TrackingGrpID ?? 0;

                var orgModel = _organizationRepository.GetDataBySupplierID(supplierID);
                CompanyDocumentModel model = new CompanyDocumentModel();
                model.SupplierID = orgModel.SupplierID;
                model.CompanyTypeID = orgModel.CompanyTypeID;
                model.CountryCode = orgModel.CountryCode;
                model.CheckModify = "0";
                model.TrackingReqID = trackingReqID;
                model.TrackingItemID = trackingItemID;
                model.TrackingType = trackingType;

                return View(model);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }
            
        }

        public ActionResult AttachmentPartial(int CompanyTypeID, string CountryCode, int SupplierID, int TrackingReqID, int TrackingItemID, string TrackingType)
        {
            var model = _customerDocTypeRepository.GetDocumentTypeByCompanyTypeIDAndCountrycode(CompanyTypeID, CountryCode);

            ViewData["SupplierID"] = SupplierID;
            ViewData["TrackingReqID"] = TrackingReqID;
            ViewData["TrackingItemID"] = TrackingItemID;
            ViewData["TrackingType"] = TrackingType;

            return PartialView(@"~/Views/EditDataTracking/_AttachmentPartial.cshtml", model);
        }

        public ActionResult CustomerDocNamePartial(int DocumentTypeID, string Countrycode, int CompanyTypeID)
        {
            var model = _customerDocNameRepository.GetCustomerDocNameByDocumentTypeIDAndCountryCode(DocumentTypeID, Countrycode, CompanyTypeID);
            
            ViewData["Countrycode"] = Countrycode;
            ViewData["CompanyTypeID"] = CompanyTypeID;

            return PartialView(@"~/Views/EditDataTracking/_CustomerDocNamePartial.cshtml", model);
        }

        public ActionResult CustomerDocNamePartialNew(int DocumentTypeID, string Countrycode, int CompanyTypeID)
        {

            var model = _customerDocNameRepository.GetCustomerDocNameByDocumentTypeIDAndCountryCode(DocumentTypeID, Countrycode, CompanyTypeID);

            return PartialView(@"~/Views/EditDataTracking/_CustomerDocNamePartialNew.cshtml", model);
        }

        public ActionResult AttachmentItemListPartial(int DocumentTypeID, int SupplierID, int CompanyTypeID, int TrackingReqID, int TrackingItemID, string TrackingType)
        {
            ViewData["SupplierID"] = SupplierID;
            ViewData["TrackingReqID"] = TrackingReqID;
            ViewData["TrackingItemID"] = TrackingItemID;
            ViewData["TrackingType"] = TrackingType;

            var model = _comProfileTrackingService.GetOrgAttachmentListBySupplierIDAndDocumentTypeID(SupplierID, DocumentTypeID, CompanyTypeID);

            return PartialView(@"~/Views/EditDataTracking/_AttachmentItemListPartial.cshtml", model);
        }

        [HttpPost]
        public ActionResult DocumentSave(CompanyDocumentModel model)
        {
            try
            {
                string username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
                var user = _userRepository.FindByUsername(username);
                int userID = user.UserID;


                var documentName = _customerDocNameRepository.GetCustomerDocNameAll();

                foreach (var item in documentName)
                {

                    var hidFile = Request.Params["hidFile_" + item.DocumentNameID + "[]"] ?? string.Empty;

                    var hidGuid = Request.Params["hidGuid_" + item.DocumentNameID + "[]"] ?? string.Empty;

                    var hidOtherDocument = Request.Params["hidOtherDocument_" + item.DocumentNameID + "[]"] ?? string.Empty;

                    var splitHidFile = hidFile.Split(',');

                    var splitHidGuid = hidGuid.Split(',');

                    var splitHidOtherDocument = hidOtherDocument.Split(',');

                    #region-----------------Insert Attachment------------------
                    if (splitHidFile.Length > 0 && (!string.IsNullOrEmpty(hidFile)))
                    {
                        for (var i = 0; i < splitHidFile.Length; i++)
                        {
                            string fileName = splitHidFile[i];

                            string guidName = splitHidGuid[i];

                            string otherDocument = "";

                            if (item.DocumentTypeID == -1)
                            {
                                otherDocument = splitHidOtherDocument[i];
                            }
                            UploadAttachment(fileName, guidName, model.SupplierID, item.DocumentNameID, otherDocument, model.TrackingReqID??0,userID);
                        }
                    }
                    #endregion End Insert Attachment

                    
                }
                #region-----------------Remove Attachment------------------
                var hidRemoveFile = Request.Params["hidRemoveAttachmentID[]"] ?? string.Empty;

                var splitHidRemoveFile = hidRemoveFile.Split(',');
                if (splitHidRemoveFile.Length > 0 && (!string.IsNullOrEmpty(hidRemoveFile)))
                {
                    foreach (var itemlist in splitHidRemoveFile)
                    {
                        if (!string.IsNullOrEmpty(itemlist))
                        {
                            int attachmentID = Convert.ToInt32(itemlist);
                            TrackingRemoveAttachment(attachmentID, model.TrackingReqID ?? 0, userID);
                        }
                        
                    }
                }
                #endregion End Remove Attachment

                #region-----------------Restore Attachment------------------
                //มาจากกรณีการกด icon Restore ไฟล์ที่เคยกดลบโดยไม่ได้แนบไฟล์มาเพิ่ม **(NewAttachmentID == 0)
                var hidRestoreDocument = Request.Params["hidRestoreDocument[]"] ?? string.Empty;

                var splitHidRestoreDocument = hidRestoreDocument.Split(',');

                if (splitHidRestoreDocument.Length > 0 && (!string.IsNullOrEmpty(hidRestoreDocument)))
                {
                    foreach (var itemlist in splitHidRestoreDocument)
                    {
                        
                        int trackingItemID =  0;
                        bool chkNumber = int.TryParse(itemlist, out trackingItemID);
                        if (chkNumber)
                        {
                            var trackingOrgAttachment = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentByTrackingItemID(trackingItemID);
                            if (trackingOrgAttachment != null)
                            {
                                //ให้ update TrackingItem นี้ โดยให้ OldAttachmentID = NewAttachmentID เปรียบเสมือนกลับไปใช้ไฟล์เดิม
                                trackingOrgAttachment.NewAttachmentID = trackingOrgAttachment.OldAttachmentID;
                                _trackingOrgAttachmentRepository.Update(trackingOrgAttachment, userID);
                            }
                        
                        }
                    }

                }

                #endregion End Restore Attachment
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return RedirectToAction("Document", new { trackingReqID = model.TrackingReqID });
        }

        public ActionResult Download(string filePath, string fileName = "")
        {
            string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
            filePath = pathAttachment + filePath;

            if (!System.IO.File.Exists(@filePath))
            {
                filePath = @pathAttachment + @"\SWWDocuments\UserManualForDownload\EPInstruction.pdf";
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(@filePath);

            if (string.IsNullOrEmpty(fileName))
            {
                fileName = "download";
            }

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }


        private void UploadAttachment(string fileName, string guidName, int supplierID, int documentNameID, string otherDocument, int trackingReqID, int updateBy)
        {
            try
            {
                if (!_orgAttachmentRepository.OrgAttachmentExists(guidName, supplierID))
                {

                    string pathTemp = _appConfigRepository.GetAppConfigByName("Portal_AttachmentPathTemp").Value;
                    string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];

                    string pathTempSupplier = "Supplier";

                    string pathForSavingTemp = pathAttachment + pathTemp + pathTempSupplier;
                    //string pathForSavingTemp = Server.MapPath("~/Register_AttachmentPathTemp/" + regID);

                    string filePathTemp = pathForSavingTemp + "/" + guidName;

                    if (System.IO.File.Exists(filePathTemp))
                    {

                        byte[] fileBytes = System.IO.File.ReadAllBytes(filePathTemp);


                        string virtualFilePath = filePathTemp;
                        string fileName2 = Path.GetFileName(virtualFilePath);
                        var fileTemp = File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);

                        if (fileTemp != null && fileTemp.FileContents.Length != 0)
                        {
                            string pathSave = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentPath").Value;
                            string pathForSaving = pathAttachment + pathSave + supplierID;
                            //string pathForSaving = Server.MapPath("~/Register_AttachmentPath/" + regID);

                            string filePath = pathForSaving + "/" + guidName;

                            if (this.CreateFolderIfNeeded(pathForSaving))
                            {
                                // Ensure that the target does not exist. 
                                if (System.IO.File.Exists(filePath))
                                    System.IO.File.Delete(filePath);

                                // Move the file.
                                System.IO.File.Move(filePathTemp, filePath);

                                var models = new Tbl_OrgAttachment
                                {
                                    SupplierID = supplierID,
                                    AttachmentName = fileName,
                                    AttachmentNameUnique = guidName,
                                    SizeAttach = fileTemp.FileContents.Length.ToString(),
                                    DocumentNameID = documentNameID,
                                    OtherDocument = otherDocument,
                                    isDeleted = 0

                                };

                                int attachmentID = _orgAttachmentRepository.InsertReturnAttachmentID(models, updateBy);

                                #region-----------------Check Tracking DocumentNameID------------------

                                //*** โดยที่ DocumentNameID != -1 ถ้า DocumentNameID == -1 จะเป็นอีกกรณี
                                if (documentNameID != -1)
                                {
                                    var trackingOrgAttachment = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentWaitingByTrackingReqIDAndDocumentNameID(trackingReqID, documentNameID);
                                    //เชคว่า DocumentNameID นี้ มีใน Tbl_TrackingOrgAttachment ที่ trackingReqID นี้หรือไม่ (ที่รอการอนุมัติ)
                                    if (trackingOrgAttachment != null)
                                    {
                                        //ถ้ามีแสดงว่า DocumentNameID นี้เคยมีการ Tracking แล้ว ให้ update attachmentID ใส่ NewAttachmentID ของ TrackingItem นี้
                                        trackingOrgAttachment.NewAttachmentID = attachmentID;
                                        _trackingOrgAttachmentRepository.Update(trackingOrgAttachment, updateBy);

                                    }else
                                    {
                                        //ถ้าไม่มีแสดงว่ายังไม่มีการ Tracking มาก่อน ให้เพิ่ม TrackingItem ภายใต้ trackingReqID นี้
                                        //โดยกระบวนการเดียวกันเหมือนหน้าหลัก
                                        TrackingInsertAttachment(attachmentID, trackingReqID, updateBy);
                                    }
                                }
                                else
                                {
                                    //ถ้าเป็นกรณี DocumentNameID == -1 แล้วเพิ่มใหม่เข้ามา จะเป็นการเพิ่ม TrackingItem ภายใต้ trackingReqID นี้
                                    var trackingModel = new Tbl_TrackingOrgAttachment()
                                    {
                                        TrackingReqID = trackingReqID,
                                        OldAttachmentID = 0,
                                        NewAttachmentID = attachmentID,
                                        TrackingStatusID = 2,
                                        TrackingStatusDate = DateTime.UtcNow,
                                        Remark = "",
                                        DocumentNameID = documentNameID
                                    };

                                    _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);
                                }
                                #endregion End Insert Attachment
                                
                            }
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void TrackingRemoveAttachment(string guidName, int trackingReqID, int updateBy)
        {
            try
            {
                var attachmentModel = _orgAttachmentRepository.GetOrgAttachmentByGuidName(guidName);

                if (attachmentModel != null)
                {
                    int attachSupplierID = attachmentModel.SupplierID;
                    int attachDocumentNameID = attachmentModel.DocumentNameID ?? 0;
                    int attachmentID = attachmentModel.AttachmentID;

                    //เชคก่อนว่า DocumentNameID ของ เอกสารนี้ ได้อยู่ระหว่างการ Tracking ที่รออนุมัติหรือไม่
                    //*** โดยที่ DocumentNameID != -1 ถ้า DocumentNameID == -1 จะเป็นอีกกรณี
                    #region-----------------Check Tracking DocumentNameID------------------
                    //เชคว่า DocumentNameID นี้ มีใน Tbl_TrackingOrgAttachment ที่ trackingReqID นี้หรือไม่ (ที่รอการอนุมัติ)
                    if (attachDocumentNameID != -1)
                    {
                        var trackingOrgAttachment = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentWaitingByTrackingReqIDAndDocumentNameID(trackingReqID, attachDocumentNameID);
                        if (trackingOrgAttachment != null)
                        {
                            //ถ้ามีแสดงว่า DocumentNameID นี้เคยมีการ Tracking แล้ว ให้เอา AttachmentID ที่จะลบ เทียบกับ NewAttachmentID
                            if (attachmentID != trackingOrgAttachment.NewAttachmentID)
                            {
                                //ถ้า AttachmentID != NewAttachmentID แสดงว่ามีการเพิ่มไฟล์มาใหม่ด้วย update NewAttachmentID ตั้งแต่ขั้นตอน Insert Attachment แล้ว
                                var orgAttachmentList = _orgAttachmentRepository.GetOrgAttachmentBySupplierIDAndDocumentNameID(attachSupplierID, attachDocumentNameID);
                                if (orgAttachmentList.Count() > 2)
                                {
                                    //จะมีมากกว่า 2 ไฟล์ได้กรณีที่ ไฟล์นี้อยู่ระหว่าง Tracking โดย old และ new ไม่เท่ากับ 0 และเกิดจากการกด ลบไฟล์ tracking แล้วแนบมาใหม่อีกรอบ จะเกิดเป็น 3 ไฟล์ตั้งแต่ฟังชั่น Insert Attachment
                                    //ให้ไปลบ Attachment ในตาราง Tbl_OrgAttachment อย่างเดียว
                                    RemoveAttachment(attachmentID, updateBy);                                  
                                }
                                //ถ้ามีไฟล์ 1 แล้วกดลบ จะไม่สามารถเกิดขึ้นเพราะ ถ้าอยู่ในระหว่างการ tracking จะมาจากกรณี oldID = 0 และ newID จะต้อง = attachmentID ที่กดลบ จะไปที่ else
                                //ถ้ามีไฟล์ 2 แล้วกดลบ และ attachmentID != NewAttachmentID เกิดจากกรณีกดลบไฟล์ที่ tracking ที่ oldID & newID เท่ากัน แล้วเพิ่มไฟล์มาใหม่ newID จึงถูก update ที่ Insert Attachment
                                //ตั้งนั้นข้อมูลจะเหมือน ลบแล้วแนบไฟล์มาใหม่ โดยที่เดิมมีไฟล์อยู่แล้ว จึงไม่ต้องทำอะไร
                            }else
                            {
                                //ถ้ามี AttachmentID == NewAttachmentID แสดงว่ามีการลบไฟล์ที่อยู่ระหว่าง Trackig โดยไม่ได้เพิ่มไฟล์เข้าเข้ามาทดแทน
                                if (trackingOrgAttachment.OldAttachmentID == 0)
                                {
                                    //ถ้า OldAttachmentID == 0 แสดงว่าก่อน Track ไม่มีไฟล์ ให้ลบไฟล์ใน Attachment ในตาราง Tbl_OrgAttachment ออก
                                    //และลบ TrackingItemID นี้ออกจาก TrackingRequest
                                    RemoveAttachment(attachmentID, updateBy);
                                    _trackingOrgAttachmentRepository.Delete(trackingOrgAttachment, updateBy);
                                }
                                else
                                {
                                    //ถ้า OldAttachmentID == NewAttachmentID แสดงว่ามีการกดลบไฟล์ที่มีการ กด Restore ก่อนหน้า คือการลบไฟล์นี้อีกครั้ง
                                    if (trackingOrgAttachment.OldAttachmentID == trackingOrgAttachment.NewAttachmentID)
                                    {
                                        trackingOrgAttachment.NewAttachmentID = 0;
                                        _trackingOrgAttachmentRepository.Update(trackingOrgAttachment, updateBy);

                                    }else
                                    {
                                        //ถ้า OldAttachmentID != 0 แสดงว่า DocumentNameID ของ Request นี้ ก่อน Track มีไฟล์อยู่ก่อนแล้ว
                                        //ให้ update TrackingItem นี้ โดยให้ OldAttachmentID = NewAttachmentID เปรียบเสมือนกลับไปใช้ไฟล์เดิม
                                        //และลบไฟล์ Attachment ในตาราง Tbl_OrgAttachment ออก
                                        trackingOrgAttachment.NewAttachmentID = trackingOrgAttachment.OldAttachmentID;
                                        _trackingOrgAttachmentRepository.Update(trackingOrgAttachment, updateBy);
                                        RemoveAttachment(attachmentID, updateBy);
                                    }
                                    
                                }
                            }                           
                        }else
                        {
                            //ไม่มีข้องมูล Tracking ของ DocumentNameID นี้แสดงว่า DocumentNameID นี้ไม่เคยผ่านการ Track ให้ทำเหมือนกระบวนการ Tracking จากหน้าหลัก
                            //คือจะไปเพิ่มข้อมูล Tracking โดยไม่ไปยุ่งกับตารางหลัก
                            var orgAttachmentList = _orgAttachmentRepository.GetOrgAttachmentBySupplierIDAndDocumentNameID(attachSupplierID, attachDocumentNameID);
                            if (orgAttachmentList.Count() == 1)
                            {
                                //ลบโดยที่ไม่ได้เพิ่ม DocumentNameID ใหม่ กรณีที่มีการเพิ่มใหม่ ค่านี้จะถูกเพิ่มในฟังชั่น TrackingInsertAttachment แล้ว  (จะไม่มีทางเป็น 0 เพราะจะมีอย่างน้อย 1 ตัวถึงจะกดลบได้)
                                var trackingModel = new Tbl_TrackingOrgAttachment()
                                {
                                    TrackingReqID = trackingReqID,
                                    OldAttachmentID = attachmentModel.AttachmentID,
                                    NewAttachmentID = 0,
                                    TrackingStatusID = 2,
                                    TrackingStatusDate = DateTime.UtcNow,
                                    Remark = "",
                                    DocumentNameID = attachmentModel.DocumentNameID
                                };

                                _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);
                            }
                        }
                    }else
                    {
                        //ถ้าเป็นกรณี DocumentNameID == -1 แล้วมีการลบ ให้เชคว่าไฟล์ที่ลบอยู่ระหว่างการ Track หรือไม่
                        var trackingOrgAttachment = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentWaitingByAttachmentID(attachmentID, trackingReqID, attachDocumentNameID);
                        if (trackingOrgAttachment != null)
                        {
                            //มีข้อมูล Tracking
                            if (trackingOrgAttachment.OldAttachmentID == 0)
                            {
                                //ถ้าอยู่ในระหว่างการ Track และ OldAttachmentID == 0 แสดงว่าเป็นการเพิ่มไฟล์มาใหม่ 
                                //case นี้จึงเป็นการลบไฟล์ที่มีการแนบมาโดยไม่มีไฟล์เดิมก่อน Track 
                                //ต้องทำการ ลบข้อมูลใน Tbl_OrgAttachment และลบ และลบ TrackingItemID นี้ออกจาก TrackingRequest
                                RemoveAttachment(attachmentID, updateBy);
                                _trackingOrgAttachmentRepository.Delete(trackingOrgAttachment, updateBy);
                                
                            }else
                            {
                                if (trackingOrgAttachment.NewAttachmentID == 0)
                                {
                                    //ถ้าอยู่ในระหว่างการ Track และ NewAttachmentID == 0 แสดงว่าเป็นการลบไฟล์จากที่เคยมีไฟล์มาก่อนการ Tracking
                                    //ให้ update TrackingItem นี้ โดยให้ OldAttachmentID = NewAttachmentID เปรียบเสมือนกลับไปใช้ไฟล์เดิม
                                    trackingOrgAttachment.NewAttachmentID = trackingOrgAttachment.OldAttachmentID;
                                    _trackingOrgAttachmentRepository.Update(trackingOrgAttachment, updateBy);
                                }else
                                {
                                    //จะเป็นกรณี NewAttachmentID และ OldAttachmentID != 0 กรณีนี้จะไม่มีเ
                                    //พราะที่ปรับใหม่ DocumentNameID == -1 กดลบจะลบไปเลย กดเพิ่มก็เพิ่มใหม่ ไม่มีการแทนที่ไฟล์เดิม
                                }
                            }
                        }
                        else
                        {
                            //ไม่มีข้อมูล Tracking จะเป็นการเพิ่ม TrackingItemID ใน TrackingRequest นี้
                            //โดยให้ NewAttachmentID == 0
                            var trackingModel = new Tbl_TrackingOrgAttachment()
                            {
                                TrackingReqID = trackingReqID,
                                OldAttachmentID = attachmentModel.AttachmentID,
                                NewAttachmentID = 0,
                                TrackingStatusID = 2,
                                TrackingStatusDate = DateTime.UtcNow,
                                Remark = "",
                                DocumentNameID = attachmentModel.DocumentNameID
                            };

                            _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void TrackingRemoveAttachment(int attachmentIDRemove, int trackingReqID, int updateBy)
        {
            try
            {
                var attachmentModel = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(attachmentIDRemove);

                if (attachmentModel != null)
                {
                    int attachSupplierID = attachmentModel.SupplierID;
                    int attachDocumentNameID = attachmentModel.DocumentNameID ?? 0;
                    int attachmentID = attachmentModel.AttachmentID;

                    //เชคก่อนว่า DocumentNameID ของ เอกสารนี้ ได้อยู่ระหว่างการ Tracking ที่รออนุมัติหรือไม่
                    //*** โดยที่ DocumentNameID != -1 ถ้า DocumentNameID == -1 จะเป็นอีกกรณี
                    #region-----------------Check Tracking DocumentNameID------------------
                    //เชคว่า DocumentNameID นี้ มีใน Tbl_TrackingOrgAttachment ที่ trackingReqID นี้หรือไม่ (ที่รอการอนุมัติ)
                    if (attachDocumentNameID != -1)
                    {
                        var trackingOrgAttachment = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentWaitingByTrackingReqIDAndDocumentNameID(trackingReqID, attachDocumentNameID);
                        if (trackingOrgAttachment != null)
                        {
                            //ถ้ามีแสดงว่า DocumentNameID นี้เคยมีการ Tracking แล้ว ให้เอา AttachmentID ที่จะลบ เทียบกับ NewAttachmentID
                            if (attachmentID != trackingOrgAttachment.NewAttachmentID)
                            {
                                //ถ้า AttachmentID != NewAttachmentID แสดงว่ามีการเพิ่มไฟล์มาใหม่ด้วย update NewAttachmentID ตั้งแต่ขั้นตอน Insert Attachment แล้ว
                                var orgAttachmentList = _orgAttachmentRepository.GetOrgAttachmentBySupplierIDAndDocumentNameID(attachSupplierID, attachDocumentNameID);
                                if (orgAttachmentList.Count() > 2)
                                {
                                    //จะมีมากกว่า 2 ไฟล์ได้กรณีที่ ไฟล์นี้อยู่ระหว่าง Tracking โดย old และ new ไม่เท่ากับ 0 และเกิดจากการกด ลบไฟล์ tracking แล้วแนบมาใหม่อีกรอบ จะเกิดเป็น 3 ไฟล์ตั้งแต่ฟังชั่น Insert Attachment
                                    //ให้ไปลบ Attachment ในตาราง Tbl_OrgAttachment อย่างเดียว
                                    RemoveAttachment(attachmentID, updateBy);
                                }
                                if (orgAttachmentList.Count() == 2)
                                {
                                    
                                    if (trackingOrgAttachment.OldAttachmentID == 0)
                                    {
                                        //ถ้ามีไฟล์ 2 แล้วกดลบ และ attachmentID != NewAttachmentID เกิดจากกรณีกดลบไฟล์ที่ tracking ที่ oldID == 0 แล้วเพิ่มไฟล์มาใหม่ newID จึงถูก update ที่ Insert Attachment
                                        //ให้ลบไล์เดิมในตาราง Tbl_OrgAttachment ออก
                                        RemoveAttachment(attachmentID, updateBy);
                                    }
                                    //ถ้ามีไฟล์ 2 แล้วกดลบ และ attachmentID != NewAttachmentID เกิดจากกรณีกดลบไฟล์ที่ tracking ที่ oldID & newID เท่ากัน แล้วเพิ่มไฟล์มาใหม่ newID จึงถูก update ที่ Insert Attachment
                                    //แสดงว่ามีไฟล์ตั้งแต่ก่อน Track แล้ว ตั้งนั้นข้อมูลจะเหมือน ลบแล้วแนบไฟล์มาใหม่ โดยที่เดิมมีไฟล์อยู่แล้ว จึงไม่ต้องทำอะไร
                                }

                                //ถ้ามีไฟล์ 1 แล้วกดลบ จะไม่สามารถเกิดขึ้นเพราะ ถ้าอยู่ในระหว่างการ tracking จะมาจากกรณี oldID = 0 และ newID จะต้อง = attachmentID ที่กดลบ จะไปที่ else
                                
                            }
                            else
                            {
                                //ถ้ามี AttachmentID == NewAttachmentID แสดงว่ามีการลบไฟล์ที่อยู่ระหว่าง Trackig โดยไม่ได้เพิ่มไฟล์เข้าเข้ามาทดแทน
                                if (trackingOrgAttachment.OldAttachmentID == 0)
                                {
                                    //ถ้า OldAttachmentID == 0 แสดงว่าก่อน Track ไม่มีไฟล์ ให้ลบไฟล์ใน UpdateFlag Attachment ในตาราง Tbl_OrgAttachment เพื่อจะได้ดูประวัติได้
                                    //และ update TrackingItemID นี้เป็น ยกเลิก (TrackingStatusID = 4) TrackingRequest
                                    RemoveAttachmentUpdateFlag(attachmentID, attachSupplierID, updateBy);
                                    trackingOrgAttachment.TrackingStatusID = 4;
                                    trackingOrgAttachment.TrackingStatusDate = DateTime.UtcNow;
                                    _trackingOrgAttachmentRepository.Update(trackingOrgAttachment, updateBy);

                                    //เชคว่าใน trackingReqID นี้มี Item ที่ ไม่ใช่ ยกเลิก อันอื่นๆหรือไม่ ถ้ามีข้าม ถ้าไม่มี ให้ update trackingReqID นี้เป็น ยกเลิก
                                    var trackingOrgAttachmentNonCancel = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentNonCancelByTrackingReqID(trackingReqID);
                                    if (trackingOrgAttachmentNonCancel.Count() == 0)
                                    {
                                        var trackingRequest = _trackingRequestRepository.GetTrackingRequestByTrackingReqID(trackingReqID);
                                        if (trackingRequest != null)
                                        {
                                            trackingRequest.TrackingStatusID = 4;
                                            trackingRequest.TrackingStatusDate = DateTime.UtcNow;

                                            _trackingRequestRepository.Update(trackingRequest, updateBy);
                                        }
                                    }
                                }
                                else
                                {
                                    //ถ้า OldAttachmentID == NewAttachmentID แสดงว่ามีการกดลบไฟล์ที่มีการ กด Restore ก่อนหน้า คือการลบไฟล์นี้อีกครั้ง
                                    if (trackingOrgAttachment.OldAttachmentID == trackingOrgAttachment.NewAttachmentID)
                                    {
                                        trackingOrgAttachment.NewAttachmentID = 0;
                                        _trackingOrgAttachmentRepository.Update(trackingOrgAttachment, updateBy);

                                    }
                                    else
                                    {
                                        //ถ้า OldAttachmentID != 0 แสดงว่า DocumentNameID ของ Request นี้ ก่อน Track มีไฟล์อยู่ก่อนแล้ว
                                        //ให้ update TrackingItem นี้ โดยให้ OldAttachmentID = NewAttachmentID เปรียบเสมือนกลับไปใช้ไฟล์เดิม
                                        //และลบไฟล์ Attachment ในตาราง Tbl_OrgAttachment ออก
                                        trackingOrgAttachment.NewAttachmentID = trackingOrgAttachment.OldAttachmentID;
                                        _trackingOrgAttachmentRepository.Update(trackingOrgAttachment, updateBy);
                                        RemoveAttachment(attachmentID, updateBy);
                                    }

                                }
                            }
                        }
                        else
                        {
                            //ไม่มีข้องมูล Tracking ของ DocumentNameID นี้แสดงว่า DocumentNameID นี้ไม่เคยผ่านการ Track ให้ทำเหมือนกระบวนการ Tracking จากหน้าหลัก
                            //คือจะไปเพิ่มข้อมูล Tracking โดยไม่ไปยุ่งกับตารางหลัก
                            var orgAttachmentList = _orgAttachmentRepository.GetOrgAttachmentBySupplierIDAndDocumentNameID(attachSupplierID, attachDocumentNameID);
                            if (orgAttachmentList.Count() == 1)
                            {
                                //ลบโดยที่ไม่ได้เพิ่ม DocumentNameID ใหม่ กรณีที่มีการเพิ่มใหม่ ค่านี้จะถูกเพิ่มในฟังชั่น TrackingInsertAttachment แล้ว  (จะไม่มีทางเป็น 0 เพราะจะมีอย่างน้อย 1 ตัวถึงจะกดลบได้)
                                var trackingModel = new Tbl_TrackingOrgAttachment()
                                {
                                    TrackingReqID = trackingReqID,
                                    OldAttachmentID = attachmentModel.AttachmentID,
                                    NewAttachmentID = 0,
                                    TrackingStatusID = 2,
                                    TrackingStatusDate = DateTime.UtcNow,
                                    Remark = "",
                                    DocumentNameID = attachmentModel.DocumentNameID
                                };

                                _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);
                            }
                        }
                    }
                    else
                    {
                        //ถ้าเป็นกรณี DocumentNameID == -1 แล้วมีการลบ ให้เชคว่าไฟล์ที่ลบอยู่ระหว่างการ Track หรือไม่
                        var trackingOrgAttachment = _trackingOrgAttachmentRepository.GetTrackingOrgAttachmentWaitingByAttachmentID(attachmentID, trackingReqID, attachDocumentNameID);
                        if (trackingOrgAttachment != null)
                        {
                            //มีข้อมูล Tracking
                            if (trackingOrgAttachment.OldAttachmentID == 0)
                            {
                                //ถ้าอยู่ในระหว่างการ Track และ OldAttachmentID == 0 แสดงว่าเป็นการเพิ่มไฟล์มาใหม่ 
                                //case นี้จึงเป็นการลบไฟล์ที่มีการแนบมาโดยไม่มีไฟล์เดิมก่อน Track 
                                //ต้องทำการ ลบข้อมูลใน Tbl_OrgAttachment และลบ และลบ TrackingItemID นี้ออกจาก TrackingRequest
                                RemoveAttachment(attachmentID, updateBy);
                                _trackingOrgAttachmentRepository.Delete(trackingOrgAttachment, updateBy);

                            }
                            else
                            {
                                if (trackingOrgAttachment.NewAttachmentID == 0)
                                {
                                    //ถ้าอยู่ในระหว่างการ Track และ NewAttachmentID == 0 แสดงว่าเป็นการลบไฟล์จากที่เคยมีไฟล์มาก่อนการ Tracking
                                    //ให้ update TrackingItem นี้ โดยให้ OldAttachmentID = NewAttachmentID เปรียบเสมือนกลับไปใช้ไฟล์เดิม
                                    trackingOrgAttachment.NewAttachmentID = trackingOrgAttachment.OldAttachmentID;
                                    _trackingOrgAttachmentRepository.Update(trackingOrgAttachment, updateBy);
                                }
                                else
                                {
                                    //จะเป็นกรณี NewAttachmentID และ OldAttachmentID != 0 จะมาจาก case เริ่มแรกกด restore แล้วมากด icon ลบ อีกที
                                    trackingOrgAttachment.NewAttachmentID = 0;
                                    _trackingOrgAttachmentRepository.Update(trackingOrgAttachment, updateBy);

                                }
                            }
                        }
                        else
                        {
                            //ไม่มีข้อมูล Tracking จะเป็นการเพิ่ม TrackingItemID ใน TrackingRequest นี้
                            //โดยให้ NewAttachmentID == 0
                            var trackingModel = new Tbl_TrackingOrgAttachment()
                            {
                                TrackingReqID = trackingReqID,
                                OldAttachmentID = attachmentModel.AttachmentID,
                                NewAttachmentID = 0,
                                TrackingStatusID = 2,
                                TrackingStatusDate = DateTime.UtcNow,
                                Remark = "",
                                DocumentNameID = attachmentModel.DocumentNameID
                            };

                            _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion


        #region------------------------------------------------Function Helper------------------------------------------------------
        private bool CreateFolderIfNeeded(string path)
        {
            bool result = true;
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception)
                {
                    /*TODO: You must process this exception.*/
                    result = false;
                }
            }
            return result;
        }

        private void TrackingInsertAttachment(int attachmentID, int trackingReqID, int updateBy)
        {
            try
            {
                var orgAttachmentModel = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(attachmentID);

                if (orgAttachmentModel != null)
                {
                    int attachSupplierID = orgAttachmentModel.SupplierID;
                    int attachDocumentNameID = orgAttachmentModel.DocumentNameID ?? 0;

                    var orgAttachmentList = _orgAttachmentRepository.GetOrgAttachmentBySupplierIDAndDocumentNameID(attachSupplierID, attachDocumentNameID);

                    if (orgAttachmentList.Count() == 1)
                    {
                        // เพิ่มเข้ามาใหม่โดยที่ยังไม่มี DocumentNameID ใน Supplier นี้มาก่อน case เพิ่มใหม่จากไม่เคยมี
                        var trackingModel = new Tbl_TrackingOrgAttachment()
                        {
                            TrackingReqID = trackingReqID,
                            OldAttachmentID = 0,
                            NewAttachmentID = attachmentID,
                            TrackingStatusID = 2,
                            TrackingStatusDate = DateTime.UtcNow,
                            Remark = "",
                            DocumentNameID = attachDocumentNameID
                        };
                        _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);

                    }
                    else
                    {
                        // มีมากกว่า 1 แสดงว่า เคยมี DocumentNameID ใน Supplier มาก่อน case ลบแล้วเพิ่มเข้ามาใหม่ (จะไม่มีทางเป็น 0 เพราะจะมีอย่างน้อย 1 ตัวที่เพิ่งเพิ่ม)
                        var oldOrgAttachmentModel = orgAttachmentList.Where(m => m.AttachmentID != attachmentID).FirstOrDefault();
                        var trackingModel = new Tbl_TrackingOrgAttachment()
                        {
                            TrackingReqID = trackingReqID,
                            OldAttachmentID = oldOrgAttachmentModel.AttachmentID,
                            NewAttachmentID = attachmentID,
                            TrackingStatusID = 2,
                            TrackingStatusDate = DateTime.UtcNow,
                            Remark = "",
                            DocumentNameID = attachDocumentNameID
                        };
                        _trackingOrgAttachmentRepository.Insert(trackingModel, updateBy);

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void RemoveAttachment(int attachmentID, int updateBy)
        {
            try
            {

                var attachmentModel = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(attachmentID);

                if (attachmentModel != null)
                {
                    string pathSave = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentPath").Value;
                    string pathDelete = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentDelPath").Value;
                    string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
                    string pathForSaving = pathAttachment + pathSave + attachmentModel.SupplierID;
                    string pathForDelete = pathAttachment + pathDelete + attachmentModel.SupplierID;
                    string guidName = attachmentModel.AttachmentNameUnique;
                    //string pathForSaving = Server.MapPath("~/Register_AttachmentPath/" + attachmentModel.RegID);

                    string filePath = pathForSaving + "/" + guidName;

                   if (System.IO.File.Exists(filePath))
                    {
                        byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

                        string virtualFilePath = filePath;
                        string fileName2 = Path.GetFileName(virtualFilePath);

                        var fileTemp = File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);

                        if (fileTemp != null && fileTemp.FileContents.Length != 0)
                        {

                            string filePathDel = pathForDelete + "/" + guidName;

                            if (this.CreateFolderIfNeeded(pathForDelete))
                            {
                                // Ensure that the target does not exist. 
                                if (System.IO.File.Exists(filePathDel))
                                    System.IO.File.Delete(filePathDel);

                                // Move the file.
                                System.IO.File.Move(filePath, filePathDel);
                            }
                        }
                    }


                   _logOrgAttachmentRepository.Insert(attachmentModel.SupplierID, attachmentModel.AttachmentID, "Delete", updateBy);
                    _orgAttachmentRepository.Delete(attachmentModel.SupplierID, attachmentModel.AttachmentID);

                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void RemoveAttachmentUpdateFlag(int attachmentID, int supplierID, int updateBy)
        {
            try
            {

                var attachmentModel = _orgAttachmentRepository.GetOrgAttachmentByAttachmentID(attachmentID);

                if (attachmentModel != null)
                {
                    string pathSave = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentPath").Value;
                    string pathDelete = _appConfigRepository.GetAppConfigByName("Portal_SupplierDocumentDelPath").Value;
                    string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
                    string pathForSaving = pathAttachment + pathSave + attachmentModel.SupplierID;
                    string pathForDelete = pathAttachment + pathDelete + attachmentModel.SupplierID;
                    string guidName = attachmentModel.AttachmentNameUnique;
                    //string pathForSaving = Server.MapPath("~/Register_AttachmentPath/" + attachmentModel.RegID);

                    string filePath = pathForSaving + "/" + guidName;

                    if (System.IO.File.Exists(filePath))
                    {
                        byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

                        string virtualFilePath = filePath;
                        string fileName2 = Path.GetFileName(virtualFilePath);

                        var fileTemp = File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);

                        if (fileTemp != null && fileTemp.FileContents.Length != 0)
                        {

                            string filePathDel = pathForDelete + "/" + guidName;

                            if (this.CreateFolderIfNeeded(pathForDelete))
                            {
                                // Ensure that the target does not exist. 
                                if (System.IO.File.Exists(filePathDel))
                                    System.IO.File.Delete(filePathDel);

                                // Move the file.
                                System.IO.File.Move(filePath, filePathDel);
                            }
                        }
                    }
                    _orgAttachmentRepository.DeleteUpdateFlag(attachmentModel.SupplierID, attachmentModel.AttachmentID, updateBy);

                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion
    }
}