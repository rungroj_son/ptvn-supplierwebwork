﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.User;
using System.Text;
using System.Web.UI;
using SupplierPortal.Services.AccountManage;

namespace SupplierPortal.Controllers
{
    public class SetCultureController : Controller
    {

        ILanguageRepository _languageRepository;
        IUserRepository _userRepository;

        public SetCultureController()
        {
            _languageRepository = new LanguageRepository();
            _userRepository = new UserRepository();
        }

        public SetCultureController(LanguageRepository languageRepository, UserRepository userRepository)
        {
            _languageRepository = languageRepository;
            _userRepository = userRepository;
        }

        public ActionResult SetCulture_Layout(string countries,string ReturnUrl = "")
        {
            // Validate input
            string culture_id;

            culture_id = countries;

            culture_id = CultureHelper.GetImplementedCulture(culture_id);

            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];

            if (cookie != null)
            {

                cookie.Value = culture_id;   // update cookie value
            }
            else
            {

                cookie = new HttpCookie("_culture");

                cookie.Value = culture_id;
                cookie.Expires = DateTime.Now.AddDays(30);

            }
            Response.Cookies.Add(cookie);

            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());

        }

        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult SetCulture_Test(string countries, string ReturnUrl = "")
        {

            // Validate input
            string culture_id;

            culture_id = countries;

            culture_id = CultureHelper.GetImplementedCulture(culture_id);

            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];

            if (cookie != null)
            {

                cookie.Value = culture_id;   // update cookie value
            }
            else
            {

                cookie = new HttpCookie("_culture");

                cookie.Value = culture_id;
                cookie.Expires = DateTime.Now.AddDays(30);

            }
            Response.Cookies.Add(cookie);

            //Response.Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
           
            //return Response.Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
            //return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
            return View();
        }
        

        [HttpPost]
        public ActionResult SetCulture_LangingPage(FormCollection collection)
        {
            // Validate input
            string culture_id;
            

            culture_id = collection["countries"];

            culture_id = CultureHelper.GetImplementedCulture(culture_id);

            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];

            if (cookie != null)
            {

                cookie.Value = culture_id;   // update cookie value
            }
            else
            {

                cookie = new HttpCookie("_culture");

                cookie.Value = culture_id;
                cookie.Expires = DateTime.Now.AddDays(30);

            }
            Response.Cookies.Add(cookie);
            string languageCode = _languageRepository.GetLanguageCodeById(Convert.ToInt32(culture_id));
            var url = ControllerContext.HttpContext.Request.UrlReferrer.ToString();
            var q = HttpUtility.ParseQueryString(Request.UrlReferrer.Query);
            string language = q["language"];
            string languageOld = "";
            string languageChange = "";
            if (!string.IsNullOrEmpty(language))
            {
                languageOld = "language=" + language;
                languageChange = "language=" + languageCode;

                url = url.Replace(languageOld, languageChange);  
            }
            return Redirect(url);
            //return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
        
        }

        [HttpPost]
        public ActionResult SetCulture(FormCollection collection)
        {
            // Validate input
            string culture_id;

            string user = "";
            string organization = "";

            culture_id = collection["countries"];

            culture_id = CultureHelper.GetImplementedCulture(culture_id);

            // Save culture in a cookie
            HttpCookie cookie = Request.Cookies["_culture"];

            if (cookie != null)
            {

                cookie.Value = culture_id;   // update cookie value
            }
            else
            {

                cookie = new HttpCookie("_culture");

                cookie.Value = culture_id;
                cookie.Expires = DateTime.Now.AddDays(30);

            }
            Response.Cookies.Add(cookie);

           
            try 
            {
                

                int languageId = Convert.ToInt32(culture_id);

                string languageCode = _languageRepository.GetLanguageCodeById(languageId);

                string username = "";

                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }


                if (!string.IsNullOrEmpty(username))
                {

                    var userTemp = _userRepository.FindByUsername(username);

                    userTemp.LanguageCode = languageCode;

                    _userRepository.Update(userTemp);

                    user = UserDataService.GetUserShowByUsernameAndLanguageId(username, languageId);
                    organization = UserDataService.GetOrganizByUsernameAndLanguageId(username, languageId);

                    Session["Org"] = organization;
                    Session["user"] = user;

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }




            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        public ActionResult CountrySelect()
        {
            var result = _languageRepository.ListOrderByAsc();

            //return PartialView("_PartialCountrySelect", result);
            return PartialView(@"~/Views/SetCulture/_PartialCountrySelect.cshtml", result);
        }

    }
}