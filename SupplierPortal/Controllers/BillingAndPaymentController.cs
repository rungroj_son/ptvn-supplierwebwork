﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.BillingDetail;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.DevXPressGridViewFieldConfig;
using System.Collections;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Data.Models.SupportModel.BillingDetail;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.SupportModel;

namespace SupplierPortal.Controllers
{
    public class BillingAndPaymentController : ControllerBase
    {
        IBillingDetailRepository _billingDetailRepository;
        IOrganizationRepository _organizationRepository;
        IDevXPressGridViewFieldConfigRepository _devXPressGridViewFieldConfigRepository;

        public BillingAndPaymentController()
        {
            _billingDetailRepository = new BillingDetailRepository();
            _organizationRepository = new OrganizationRepository();
            _devXPressGridViewFieldConfigRepository = new DevXPressGridViewFieldConfigRepository();

        }

        public BillingAndPaymentController(
            BillingDetailRepository billingDetailRepository,
            OrganizationRepository organizationRepository,
            DevXPressGridViewFieldConfigRepository devXPressGridViewFieldConfigRepository
            )
        {
            _billingDetailRepository = billingDetailRepository;
            _organizationRepository = organizationRepository;
            _devXPressGridViewFieldConfigRepository = devXPressGridViewFieldConfigRepository;

        }

        // GET: BillingAndPayment
        public ActionResult Index()
        {
            Session["ExpandAll"] = false;
            Session["CollapseAll"] = false;
            Session["Layout_Billing"] = null;


            string username = "metro-admin";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            // CurrentTime.getDateTime(username);
            var LastUpdate = new BillingAndPaymentModels();
            LastUpdate.dateTime = CurrentTime.getDateTime(username);

            int supplierID = 0;
            supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            ViewBag.SupplierID = supplierID;
            var columnModel = _devXPressGridViewFieldConfigRepository.GetDevXPressGridViewFieldConfigByGridName("Billing");
            //ClsSupportReport clsSupport = new ClsSupportReport();

            return View(LastUpdate);
        }

        public ActionResult GridViewPart()
        {
            string username = "metro-admin";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            int supplierID = 0;
            supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            ViewBag.SupplierID = supplierID;

            return PartialView(@"~/Views/BillingAndPayment/_GridViewPartial_Billing.cshtml");
        }


        #region ExpandAll/CollapseAll

        public void ExpandAll()
        {
            Session["ExpandAll"] = true;
            Session["CollapseAll"] = false;
        }

        public void CollapseAll()
        {
            Session["ExpandAll"] = false;
            Session["CollapseAll"] = true;
        }

        #endregion

        public ActionResult GridViewBillingPaymentDT(DTParameters param)
        {
            #region Find Supplier ID

            // Get username from session
            var username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            // Find supplier id
            int supplierId = 0;
            supplierId = _organizationRepository.GetSupplierIDByUsername(username);

            #endregion

            #region Filter, Order by, Paging

            // Get all parameters
            var search = param.Search.Value;
            var sortColumn = param.Order[0].Column;
            var sortDirection = param.Order[0].Dir;

            // All rows
            var all = SupplierPortal.Services.BillingManage.BillingManage.BillingAndPaymentPerformBySupplierID(supplierId);

            // Filtered
            var filtered = all;
            if (!string.IsNullOrEmpty(search))
            {
                //filtered = all.Where(x => x.BillingService.Contains(search) ||
                //                          x.DocType.Contains(search) ||
                //                          x.DocNo.Contains(search));

                var temp = all.ToList().Where(x => x.BillingService.Contains(search) ||
                                                   x.DocType.Contains(search) ||
                                                   x.DocNo.Contains(search)).ToList();

                filtered = temp.AsQueryable();
            }

            // Sort order
            var sort = filtered;
            switch (sortColumn)
            {
                //case 0:
                //    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderBy(x => x.BillingService) : filtered.OrderBy(x => x.BillingService);
                //    break;
                case 1:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.DocType) : filtered.OrderBy(x => x.DocType);
                    break;
                case 2:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.DocNo) : filtered.OrderBy(x => x.DocNo);
                    break;
                case 3:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.DocDate) : filtered.OrderBy(x => x.DocDate);
                    break;
                case 4:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.DueDate) : filtered.OrderBy(x => x.DueDate);
                    break;
                case 5:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.InvoiceAmount) : filtered.OrderBy(x => x.InvoiceAmount);
                    break;
                case 6:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.CNAmount) : filtered.OrderBy(x => x.CNAmount);
                    break;
                case 7:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.RemainAmount) : filtered.OrderBy(x => x.RemainAmount);
                    break;
                case 8:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.PaymentAmount) : filtered.OrderBy(x => x.PaymentAmount);
                    break;
                default:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.BillingService) : filtered.OrderBy(x => x.BillingService);
                    break;
            }

            // Paging
            var result = sort.Skip(param.Start).Take(param.Length);

            #endregion

            #region Return to Client JSON Format

            return Json(new
            {
                draw = param.Draw,
                recordsTotal = all.Count(),
                recordsFiltered = filtered.Count(),
                data = result,
                error = ""
            }, JsonRequestBehavior.AllowGet);

            #endregion
        }
    }

    public static class GridViewHelper
    {
        //private static GridViewSettings exportGridView_BillingSettings;

        //public static GridViewSettings ExportGridView_BillingSettings
        //{
        //    get
        //    {
        //        exportGridView_BillingSettings = CreateExportGridView_BillingSettings();
        //        return exportGridView_BillingSettings;
        //    }
        //}

        //private static GridViewSettings CreateExportGridView_BillingSettings()
        //{
        //    GridViewSettings settings = new GridViewSettings();
        //    settings.Name = "GridView_Billing";
        //    settings.CallbackRouteValues = new { Controller = "BillingAndPayment", Action = "GridViewPart" };

        //    settings.KeyboardSupport = true;
        //    settings.AccessKey = "G";
        //    settings.KeyFieldName = "SupplierID;DocNo";
        //    settings.Theme = "Metropolis";
        //    settings.Width = System.Web.UI.WebControls.Unit.Percentage(100);

        //    settings.Styles.Header.BackgroundImage.ImageUrl = "~/Content/imagescss/fhbg.gif";
        //    settings.Styles.Header.ForeColor = System.Drawing.Color.Black;
        //    settings.Styles.Header.Font.Bold = true;


        //    settings.Styles.FocusedGroupRow.BackColor = System.Drawing.Color.Gainsboro;
        //    settings.Styles.FocusedGroupRow.ForeColor = System.Drawing.Color.Black;

        //    settings.Styles.Header.HorizontalAlign = System.Web.UI.WebControls.HorizontalAlign.Center;

        //    settings.Styles.FocusedRow.BackColor = System.Drawing.Color.FromArgb(241, 241, 241);
        //    settings.Styles.FocusedRow.ForeColor = System.Drawing.Color.FromArgb(51, 51, 51);

        //    settings.SettingsPager.PageSize = 10;
        //    settings.SettingsPager.Visible = true;
        //    settings.Settings.ShowGroupPanel = true;
        //    settings.Settings.ShowFilterRow = true;
        //    settings.Settings.ShowFilterRowMenu = true;

        //    settings.Settings.ShowFilterRowMenuLikeItem = true;
        //    settings.SettingsBehavior.AutoFilterRowInputDelay = 20000;

        //    settings.Settings.ShowFooter = true;
        //    settings.Settings.ShowFilterBar = GridViewStatusBarMode.Visible;

        //    settings.SettingsPager.ShowSeparators = true;
        //    settings.Settings.ShowHeaderFilterButton = true;
        //    settings.SettingsPopup.HeaderFilter.Height = 200;

        //    settings.CommandColumn.Visible = true;
        //    settings.CommandColumn.ShowClearFilterButton = true;
        //    settings.CommandColumn.Width = System.Web.UI.WebControls.Unit.Pixel(35);

        //    settings.ClientSideEvents.EndCallback = "OnEndCallback";
        //    settings.ClientSideEvents.Init = "OnEndCallback";

        //    settings.SettingsPager.FirstPageButton.Visible = true;
        //    settings.SettingsPager.LastPageButton.Visible = true;
        //    settings.SettingsPager.PageSizeItemSettings.Visible = true;
        //    settings.SettingsPager.PageSizeItemSettings.Items = new string[] { "10", "20", "50", "100", "500", "1000" };

        //    settings.ClientSideEvents.CustomizationWindowCloseUp = "grid_CustomizationWindowCloseUp";
        //    settings.SettingsBehavior.EnableCustomizationWindow = true;

        //    settings.Settings.HorizontalScrollBarMode = ScrollBarMode.Visible;
        //    settings.Settings.VerticalScrollBarMode = ScrollBarMode.Visible;

        //    settings.Settings.GridLines = System.Web.UI.WebControls.GridLines.Vertical;

        //    settings.BeforeGetCallbackResult = (sender, e) =>
        //    {
        //        MVCxGridView grid = (MVCxGridView)sender;

        //        if (HttpContext.Current.Session["ExpandAll"].Equals(true))
        //        {
        //            grid.ExpandAll();
        //            HttpContext.Current.Session["ExpandAll"] = false;
        //            HttpContext.Current.Session["CollapseAll"] = false;
        //            HttpContext.Current.Session["CollapseStateString_Billing"] = "";
        //        }
        //        else if (HttpContext.Current.Session["CollapseAll"].Equals(true))
        //        {
        //            grid.CollapseAll();
        //            HttpContext.Current.Session["ExpandAll"] = false;
        //            HttpContext.Current.Session["CollapseAll"] = false;
        //            HttpContext.Current.Session["CollapseStateString_Billing"] = "";
        //        }
        //        else
        //        {
        //            HttpContext.Current.Session["ExpandAll"] = false;
        //            HttpContext.Current.Session["CollapseAll"] = false;
        //            HttpContext.Current.Session["CollapseStateString_Billing"] = "";
        //        }

        //        string actions = "";
        //        if (HttpContext.Current.Session["action"] != null)
        //        {
        //            actions = HttpContext.Current.Session["action"].ToString();
        //            switch (actions)
        //            {
        //                case "Saving":
        //                    HttpContext.Current.Session["Layout_Billing"] = grid.SaveClientLayout();
        //                    break;
        //                case "Loading":
        //                    grid.LoadClientLayout(HttpContext.Current.Session["Layout_Billing"].ToString());
        //                    break;
        //                default:
        //                    HttpContext.Current.Session["Layout_Billing"] = grid.SaveClientLayout();
        //                    break;
        //            }
        //            HttpContext.Current.Session["action"] = null;
        //        }
        //        else
        //        {
        //            HttpContext.Current.Session["Layout_Billing"] = grid.SaveClientLayout();
        //        }
        //    };

        //    settings.ClientLayout = (sender, e) =>
        //    {
        //        if ((e.LayoutMode == ClientLayoutMode.Loading) && (HttpContext.Current.Session["Layout_Billing"] != null))
        //        {
        //            e.LayoutData = HttpContext.Current.Session["Layout_Billing"] as string;
        //        }
        //        else if (e.LayoutMode == ClientLayoutMode.Saving)
        //        {
        //            HttpContext.Current.Session["Layout_Billing"] = e.LayoutData;
        //        }
        //    };

        //    settings.CustomJSProperties = (s, e) =>
        //    {
        //        MVCxGridView g = s as MVCxGridView;
        //        HttpContext.Current.Session["FilterExpression_Billing"] = g.FilterExpression;
        //        e.Properties["cpGridFilterExpression"] = g.FilterExpression;
        //        e.Properties["cpVisibleRowCount"] = g.VisibleRowCount;
        //    };

        //    IDevXPressGridViewFieldConfigRepository _repo = new DevXPressGridViewFieldConfigRepository();
        //    var columnModel = _repo.GetDevXPressGridViewFieldConfigByGridName("Billing");
        //    foreach (var list in columnModel)
        //    {
        //        settings.Columns.Add(GridViewHelper.setColumn(list));
        //    }

        //    return settings;
        //}
        //public static DevExpress.Web.Mvc.MVCxGridViewColumn setColumn(Tbl_DevXPressGridViewFieldConfig model)
        //{
        //    string languageId = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

        //    DevExpress.Web.Mvc.MVCxGridViewColumn column = new DevExpress.Web.Mvc.MVCxGridViewColumn();
        //    column.FieldName = model.FieldName;
        //    column.Caption = GetLocaleStringResource.GetStringResourceCurrent(model.ResourceName, languageId);
        //    column.CellStyle.Wrap = DefaultBoolean.True;
        //    if (model.SortOrderID != 0)
        //    {
        //        column.SortOrder = setSorting(model.SortOrderID ?? 0);
        //        column.SortIndex = model.SortIndex ?? 0;
        //    }
        //    column.Settings.AutoFilterCondition = AutoFilterCon(model.FilterTypeID ?? 0);
        //    if (model.ColumnTypeID != 0)
        //    {
        //        column.ColumnType = setColumnType(model.ColumnTypeID ?? 0);
        //    }
        //    if (!string.IsNullOrEmpty(model.DisplayFormat))
        //    {
        //        column.PropertiesEdit.DisplayFormatString = model.DisplayFormat;
        //    }

        //    if (model.Width != null)
        //    {
        //        column.Width = System.Web.UI.WebControls.Unit.Pixel(model.Width ?? 0);
        //    }
        //    column.Visible = visible(model.VisibleFlag ?? 0);

        //    if (model.TooltipFlag == 1)
        //    {
        //        column.ToolTip = model.Tooltip ?? "";
        //    }
        //    if (model.FilterHeaderFlag == 0)
        //    {
        //        column.Settings.AllowHeaderFilter = DefaultBoolean.False;
        //    }
        //    return column;
        //}

        //private static DevExpress.Web.ASPxGridView.AutoFilterCondition AutoFilterCon(int filterTypeID)
        //{
        //    if (filterTypeID == 1)
        //    {
        //        return AutoFilterCondition.BeginsWith;
        //    }
        //    else if (filterTypeID == 2)
        //    {
        //        return AutoFilterCondition.Contains;
        //    }
        //    else if (filterTypeID == 3)
        //    {
        //        return AutoFilterCondition.Equals;
        //    }
        //    else
        //    {
        //        return AutoFilterCondition.Default;
        //    }
        //}

        //private static DevExpress.Web.Mvc.MVCxGridViewColumnType setColumnType(int columnTypeID)
        //{
        //    if (columnTypeID == 1)
        //    {
        //        return MVCxGridViewColumnType.DateEdit;
        //    }
        //    else if (columnTypeID == 2)
        //    {
        //        return MVCxGridViewColumnType.TimeEdit;
        //    }
        //    else
        //    {
        //        return MVCxGridViewColumnType.Default;
        //    }
        //}

        //private static DevExpress.Data.ColumnSortOrder setSorting(int sortOrderID)
        //{
        //    if (sortOrderID == 1)
        //    {
        //        return DevExpress.Data.ColumnSortOrder.Ascending;
        //    }
        //    else
        //    {
        //        return DevExpress.Data.ColumnSortOrder.Descending;
        //    }
        //}

        private static bool visible(int visibleFlag)
        {
            if (visibleFlag == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //private static DevExpress.Web.Mvc.MVCxGridViewColumn Display(string DisplayFormat)
        //{
        //    DevExpress.Web.Mvc.MVCxGridViewColumn column = new DevExpress.Web.Mvc.MVCxGridViewColumn();
        //    column.PropertiesEdit.DisplayFormatString = Convert.ToString(DisplayFormat);
        //    return column;
        //}
    }
}
