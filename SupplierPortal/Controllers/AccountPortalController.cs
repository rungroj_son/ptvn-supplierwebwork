﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Core.Extension;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Web.Configuration;
using Newtonsoft.Json;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Services.AccountManage;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Data.Models.Repository.NewsDisplay;
using System.Web.Security;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.MainFunction.Crypto;
using System.Security.Claims;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using System.Configuration;
using SupplierPortal.Core.Caching;
using SupplierPortal.ServiceClients;
using MarkdownSharp;

namespace SupplierPortal.Controllers
{
    public class AccountPortalController : BaseController
    {

        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        IUserSessionRepository _userSessionRepository;
        ILanguageRepository _languageRepository;
        InewsDisplayRepository _newsDisplayRepository;
        TermAndConditionService _termAndConditionService;

        private readonly ICacheManager _cacheManager;
        public AccountPortalController()
        {
            _userSessionRepository = new UserSessionRepository();
            _languageRepository = new LanguageRepository();
            _newsDisplayRepository = new newsDisplayRepository();
            _termAndConditionService = new TermAndConditionService();
            _cacheManager = new MemoryCacheManager();
        }

        public AccountPortalController(
             UserSessionRepository userSessionRepository,
            LanguageRepository languageRepository,
            newsDisplayRepository newsDisplayRepository
             )
        {
            _userSessionRepository = userSessionRepository;
            _languageRepository = languageRepository;
            _newsDisplayRepository = newsDisplayRepository;
        }

        public IAuthenticationManager Authentication
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        #region-------------Base Action in this Controller---------------
        // Landing Page
        public ActionResult Landingpage()
        {
            return View();
        }

        // GET: AccountPortal
        public ActionResult Error()
        {
            return View();
        }

        // GET: CheckAutoTimeOut
        public ActionResult CheckAutoTimeOut()
        {
            if (Session["GUID"] == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string guid = Session["GUID"].ToString();

                if (UserSessionManage.CheckUserSessionTimeout(guid))
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl, string language = "")
        {


            #region----------------------------Set Cookies Language By Link------------------------
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (!string.IsNullOrEmpty(language))
            {
                string culture_id;

                culture_id = _languageRepository.GetLanguageIdByCode(language);

                culture_id = CultureHelper.GetImplementedCulture(culture_id);

                if (cultureCookie != null)
                {

                    cultureCookie.Value = culture_id;   // update cookie value
                }
                else
                {

                    cultureCookie = new HttpCookie("_culture");

                    cultureCookie.Value = culture_id;
                    cultureCookie.Expires = DateTime.Now.AddDays(30);

                }
                Response.Cookies.Add(cultureCookie);
            }
            #endregion

            ViewBag.LanguageId = cultureCookie.Value;
            bool isShowModalTerm = false;
            if (Session["IsLoginPass"] != null && Convert.ToBoolean(Session["IsLoginPass"]))
            {
                if (Session["IsAcceptTermCondition"] != null && !Convert.ToBoolean(Session["IsAcceptTermCondition"]))
                {
                    Markdown markdown = new Markdown();
                    markdown.Transform(Session["TermAndConditionEN"].ToString());

                    ViewBag.TermAndConditionEN = markdown.Transform(Session["TermAndConditionEN"].ToString());
                    ViewBag.TermAndConditionTH = markdown.Transform(Session["TermAndConditionTH"].ToString());

                    ViewBag.PrivacyNoticeEN = markdown.Transform(Session["PrivacyNoticeEN"].ToString());
                    ViewBag.PrivacyNoticeTH = markdown.Transform(Session["PrivacyNoticeTH"].ToString());
                    isShowModalTerm = true;
                }
                Session.Remove("IsLoginPass");
            }

            ViewBag.IsShowModalTerm = isShowModalTerm;
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        public ActionResult NewsFeedsPartial()
        {
            #region----------------------------Set News------------------------

            //var NewsList = _newsDisplayRepository.findAllNewData(Convert.ToInt32(cultureCookie.Value)).Select(s => new { s.NewsId, s.LanguageID, s.NewsDetail, s.NewsGroupID, s.NewsHeader }).ToList();
            //var id = _newsDisplayRepository.findlangID(0, Convert.ToInt32(cultureCookie.Value));

            int languageId = GetCurrentLanguageHelper.GetIntLanguageIDCurrent();

            List<NewsDisplayModel> model = new List<NewsDisplayModel>();

            var newsModel = _newsDisplayRepository.GetNewsShow(languageId);

            if (newsModel.Count() > 0)
            {
                model = newsModel.ToList();
            }
            else
            {
                var newsDefaultModel = _newsDisplayRepository.GetNewsByNewsId(0, languageId);
                if (newsDefaultModel != null)
                {
                    model.Add(newsDefaultModel);
                }
            }

            #endregion

            return PartialView(@"~/Views/AccountPortal/_PartialNews.cshtml", model);
        }

        [AllowAnonymous]
        public ActionResult LoginTest(string returnUrl, string language = "")
        {


            #region----------------------------Set Cookies Language By Link------------------------
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (!string.IsNullOrEmpty(language))
            {
                string culture_id;

                culture_id = _languageRepository.GetLanguageIdByCode(language);

                culture_id = CultureHelper.GetImplementedCulture(culture_id);

                if (cultureCookie != null)
                {

                    cultureCookie.Value = culture_id;   // update cookie value
                }
                else
                {

                    cultureCookie = new HttpCookie("_culture");

                    cultureCookie.Value = culture_id;
                    cultureCookie.Expires = DateTime.Now.AddDays(30);

                }
                Response.Cookies.Add(cultureCookie);
            }
            #endregion

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        public ActionResult Logout(string returnUrl)
        {

            try
            {
                string userGuid = "";
                if (Session["GUID"] != null)
                {
                    userGuid = Session["GUID"].ToString();

                    var userSession = _userSessionRepository.GetUserSession(userGuid);

                    if (userSession != null)
                    {
                        userSession.isTimeout = 1;
                        userSession.LogoutTime = DateTime.UtcNow;
                        _userSessionRepository.Update(userSession);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Logout Controller : " + ex);
            }
            finally
            {
                Authentication.SignOut();
                _cacheManager.Clear();
                Session.Clear();
            }
            //if (Request.Cookies["MyCookie"] != null)
            //{
            //    var c = new HttpCookie("MyCookie");
            //    c.Expires = DateTime.Now.AddDays(-1);
            //    Response.Cookies.Add(c);
            //}

            //HttpCookie cultureCookie = Request.Cookies["_culture"];
            //if (cultureCookie != null)
            //{

            //    var c = new HttpCookie("_culture");
            //    c.Expires = DateTime.Now.AddDays(-1);
            //    Response.Cookies.Add(c);
            //}

            return RedirectToAction("Login", "AccountPortal");
        }
        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ForgotPasswordConfirmation(ForgotPasswordModel model, string returnUrl)
        {
            string message = "";

            if (ModelState.IsValid)
            {
                try
                {
                    HttpClient client = new HttpClient();

                    string baseURL = WebConfigurationManager.AppSettings["baseURL"];

                    string languageId = "";
                    HttpCookie cultureCookie = Request.Cookies["_culture"];
                    languageId = cultureCookie.Value;

                    string languageURL = "?languageId=" + languageId;

                    client.BaseAddress = new Uri(baseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    string Jsonstr = JsonConvert.SerializeObject(model);
                    HttpContent content = new StringContent(Jsonstr);
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    string urlAll = string.Format("{0}api/AccountPortal/ForgotPassword{1}", baseURL, languageURL);

                    HttpResponseMessage response = await client.PostAsync(urlAll, content);

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var responseBody = response.Content.ReadAsStringAsync().Result;

                        JObject _response = JObject.Parse(responseBody);


                        var query = from c in _response["data"].Children()
                                    select c;
                        query = query.ToList();

                        var responseResult = (from x in query
                                              select new
                                              {
                                                  respMessage = (string)x.SelectToken("Message"),
                                                  respResult = (bool)x.SelectToken("Result"),
                                                  respEmail = (string)x.SelectToken("Email")
                                              }).ToList();

                        var respResult = responseResult.Select(x => x.respResult).FirstOrDefault();
                        message = responseResult.Select(x => x.respMessage).FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
                }
            }
            ViewBag.Message = message;
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(string code, string language = "")
        {
            if (!string.IsNullOrEmpty(code))
            {
                #region JWT Decrypt Token

                string decryptToken = "";
                var dic = new Dictionary<string, string>();

                try
                {
                    var decryptKey = ConfigurationManager.AppSettings["Jwt_ForgotPasswordKey"];
                    if (string.IsNullOrEmpty(decryptKey))
                        throw new Exception("Jwt_ForgotPasswordKey is null or empty.");

                    dic = SupplierPortal.Services.JWTService.JWTDecrypt(decryptKey, code);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
                }

                if (!dic.ContainsKey("temp_token"))
                    return RedirectToAction("Error", "Error", new { ErrorMessage = "Reset password error." });
                else
                    decryptToken = dic["temp_token"];

                #endregion

                string token = "";
                string username = "";

                var splitCode = decryptToken.Split(':');
                if (splitCode.Length >= 2)
                {
                    token = decryptToken;
                    username = splitCode[1];
                }

                TokenModel model = new TokenModel
                {
                    Username = username,
                    Token = token
                };

                HttpCookie cultureCookie = Request.Cookies["_culture"];

                #region----------------------------Set Cookies Language By Link------------------------
                if (!string.IsNullOrEmpty(language))
                {
                    string culture_id;

                    culture_id = _languageRepository.GetLanguageIdByCode(language);

                    culture_id = CultureHelper.GetImplementedCulture(culture_id);

                    if (cultureCookie != null)
                    {

                        cultureCookie.Value = culture_id;   // update cookie value
                    }
                    else
                    {

                        cultureCookie = new HttpCookie("_culture");

                        cultureCookie.Value = culture_id;
                        cultureCookie.Expires = DateTime.Now.AddDays(30);

                    }
                    Response.Cookies.Add(cultureCookie);
                }
                #endregion


                HttpClient client = new HttpClient();

                string baseURL = WebConfigurationManager.AppSettings["baseURL"];

                string languageId = "";

                languageId = cultureCookie.Value;

                string languageURL = "?languageId=" + languageId;

                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string Jsonstr = JsonConvert.SerializeObject(model);
                HttpContent content = new StringContent(Jsonstr);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string urlAll = string.Format("{0}api/AccountPortal/CheckPasswordRecoveryToken{1}", baseURL, languageURL);

                HttpResponseMessage response = await client.PostAsync(urlAll, content);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseBody = response.Content.ReadAsStringAsync().Result;

                    try
                    {
                        JObject _response = JObject.Parse(responseBody);

                        if (Convert.ToBoolean(_response["result"].SelectToken("SuccessStatus")))
                        {
                            //ViewBag.Username = username;
                            ResetPasswordModel modelReset = new ResetPasswordModel
                            {
                                Token = token
                            };
                            return View(modelReset);
                        }
                        else
                        {
                            return RedirectToAction("Login", "AccountPortal");
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
                    }
                }
            }

            return RedirectToAction("Error", "Error", new { ErrorMessage = "Reset password error." });
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(ResetPasswordModel model)
        {
            string message = "";

            if (ModelState.IsValid)
            {
                HttpClient client = new HttpClient();

                string baseURL = WebConfigurationManager.AppSettings["baseURL"];

                string languageId = "";
                HttpCookie cultureCookie = Request.Cookies["_culture"];
                languageId = cultureCookie.Value;

                string languageURL = "?languageId=" + languageId;

                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string Jsonstr = JsonConvert.SerializeObject(model);
                HttpContent content = new StringContent(Jsonstr);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string urlAll = string.Format("{0}api/AccountPortal/ResetPassword{1}", baseURL, languageURL);

                HttpResponseMessage response = await client.PostAsync(urlAll, content);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseBody = response.Content.ReadAsStringAsync().Result;
                    try
                    {
                        JObject _response = JObject.Parse(responseBody);

                        var query = from c in _response["data"].Children()
                                    select c;
                        query = query.ToList();

                        var responseResult = (from x in query
                                              select new
                                              {
                                                  respMessage = (string)x.SelectToken("Message"),

                                              }).ToList();

                        message = responseResult.Select(x => x.respMessage).FirstOrDefault();

                        if (Convert.ToBoolean(_response["result"].SelectToken("SuccessStatus")))
                        {
                            ViewBag.Message = message;
                            return View();
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                        return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
                    }
                }
            }

            return RedirectToAction("Error", "Error", new { ErrorMessage = "Reset password error." });
        }

        private void FormLogin(Tbl_User user)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, user.Username));
            claims.Add(new Claim(ClaimTypes.Role, user.OrgID));
            var id = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
            Authentication.SignIn(id);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginPortalModel model, string returnUrl)
        {
            string languageIdLocal = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageIdLocal = cultureCookie.Value.ToString();

            }
            else
            {
                languageIdLocal = CultureHelper.GetImplementedCulture(languageIdLocal);// This is safe
            }

            int languageID = Convert.ToInt32(languageIdLocal);
            string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
            string browserType = Request.Browser.Type;

            string languageURL = "?languageId=" + languageIdLocal;
            string browserTypeURL = "&browserType=" + browserType;
            string clientIPAddressURL = "&clientIPAddress=" + visitorsIPAddr;

            string messageReturn = "";

            string message = messageReturn.GetStringResource("_Login.ValidateMsg.InvalidUsername", languageIdLocal);

            if (ModelState.IsValid)
            {

                try
                {
                    string publicKey = WebConfigurationManager.AppSettings["publicKey"];

                    string baseURL = WebConfigurationManager.AppSettings["baseURL"];

                    string strUserName = model.Username;
                    string password = model.Password;
                    string userPassword = "";

                    bool checkLogin = false;
                    string username = "";
                    string guid = "";
                    string user = "";
                    string organization = "";
                    string languageId = "";
                    string isAcceptTermOfUse = "";
                    bool isCheckConsent = false;
                    bool isAcceptBillingCondition = false;
                    bool isUserEInvoice = false;
                    int lockTimeSeconds = 0;


                    string result = "";
                    //encode password
                    result = CryptoExtension.Base64Encode(password);
                    //PublicKey + result password
                    result = result + publicKey;
                    // encode rusult
                    result = CryptoExtension.Base64Encode(result);
                    //strUserName : result 
                    result = strUserName + ":" + result;
                    //encode result
                    userPassword = CryptoExtension.Base64Encode(result);

                    HttpClient client = new HttpClient();
                    client = SetHeaderLoginPortal(strUserName, userPassword);
                    string urlAll = string.Format("{0}api/AccountPortal/LoginValidate{1}{2}{3}", baseURL, languageURL, browserTypeURL, clientIPAddressURL);
                    HttpResponseMessage response = client.GetAsync(urlAll).Result;

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        var responseBody = response.Content.ReadAsStringAsync().Result;
                        JObject _response = JObject.Parse(responseBody);

                        if (Convert.ToBoolean(_response["result"].SelectToken("SuccessStatus")))
                        {
                            var query = from c in _response["data"].Children()
                                        select c;
                            query = query.ToList();

                            //result
                            var responseResult = (from x in query
                                                  select new
                                                  {
                                                      respUsername = (string)x.SelectToken("Username"),
                                                      respSessionID = (string)x.SelectToken("GUID"),
                                                      respResult = (bool)x.SelectToken("Result"),
                                                      respMessage = (string)x.SelectToken("Message"),
                                                      respUser = (string)x.SelectToken("User"),
                                                      respOrganization = (string)x.SelectToken("Organization"),
                                                      respLanguageId = (string)x.SelectToken("LanguageId"),
                                                      respIsAcceptTermOfUse = (string)x.SelectToken("isAcceptTermOfUse"),
                                                      respIsCheckConsent = (bool)x.SelectToken("isCheckConsent"),
                                                      respIsAcceptBillingCondition = (bool)x.SelectToken("isAcceptBillingCondition"),
                                                      respIsUserEInvoice = (bool)x.SelectToken("isUserEInvoice")
                                                  }).ToList();

                            var respResult = responseResult.Select(x => x.respResult).FirstOrDefault();

                            username = responseResult.Select(x => x.respUsername).FirstOrDefault();
                            guid = responseResult.Select(x => x.respSessionID).FirstOrDefault();
                            //user = responseResult.Select(x => x.respUser).FirstOrDefault();
                            //organization = responseResult.Select(x => x.respOrganization).FirstOrDefault();
                            languageId = responseResult.Select(x => x.respLanguageId).FirstOrDefault();
                            isAcceptTermOfUse = responseResult.Select(x => x.respIsAcceptTermOfUse).FirstOrDefault();
                            isCheckConsent = responseResult.Select(x => x.respIsCheckConsent).FirstOrDefault();
                            isAcceptBillingCondition = responseResult.Select(x => x.respIsAcceptBillingCondition).FirstOrDefault();
                            isUserEInvoice = responseResult.Select(x => x.respIsUserEInvoice).FirstOrDefault();
                            message = responseResult.Select(x => x.respMessage).FirstOrDefault();

                            checkLogin = respResult;

                            //รับค่า lockTimeSeconds ไปในตัวแปร isAcceptTermOfUse เพื่อนำไปทำ Countdown Timer
                            //guid == "Locked" Fix มาจาก API กรณีที่ user กำลังอยู่ในช่วงเวลา Locked
                            if (guid == "Locked")
                            {
                                lockTimeSeconds = Convert.ToInt32(isAcceptTermOfUse);
                                ViewBag.lockTimeSeconds = lockTimeSeconds;
                            }
                        }
                    }

                    if (checkLogin)
                    {
                        user = UserDataService.GetUserShowByUsernameAndLanguageId(username, languageID);
                        organization = UserDataService.GetOrganizByUsernameAndLanguageId(username, languageID);

                        Session["username"] = username;
                        Session["GUID"] = guid;
                        Session["Org"] = organization;
                        Session["user"] = user;
                        Session["isCheckConsent"] = isCheckConsent;
                        Session["isAcceptBillingCondition"] = isAcceptBillingCondition;
                        Session["isUserEInvoice"] = isUserEInvoice;

                        #region----------Update UserSession-------------------
                        var userSession = _userSessionRepository.GetUserSession(guid);
                        if (userSession != null)
                        {

                            userSession.IPAddress = visitorsIPAddr;
                            userSession.BrowserType = browserType;
                            userSession.LoginTime = DateTime.UtcNow;

                            _userSessionRepository.Update(userSession);
                        }
                        #endregion

                        if (!string.IsNullOrEmpty(languageId))
                        {
                            HttpCookie cookie = Request.Cookies["_culture"];

                            if (cookie != null)
                            {

                                cookie.Value = languageId;   // update cookie value
                            }
                            Response.Cookies.Add(cookie);
                        }

                        //  FormsAuthentication.SetAuthCookie(username, false);

                        // Check code of conduct on WEOMNI
                        bool resultCOC = await callCOCWeomni(strUserName);
                        Session["IsAcceptCodeOfConduct"] = !resultCOC;

                        // Check Accept Term and Condition on WEOMNI
                        bool openTerm = false;
                        Session["IsAcceptTermCondition"] = true;
                        string uesTermWeomni = WebConfigurationManager.AppSettings["switch_use_term_weomni"];
                        if (Convert.ToBoolean(uesTermWeomni))
                        {
                            openTerm = await callTermAndCondotionFromWeomni(strUserName);
                            Session["IsAcceptTermCondition"] = !openTerm;
                            Session["IsLoginPass"] = true;
                        }

                        //check role Admin in Tbl_user
                        var userData = UserDataService.GetUserByUsername(username);
                        this.FormLogin(userData);
                        //Administrator B34488
                        if (userData.OrgID == "Administrator")
                        {
                            return RedirectToAction("Index", "HomeForAdmin");
                        }
                        else if (openTerm)
                        {
                            return RedirectToAction("Login", "AccountPortal");
                        }
                        else
                        {
                            // Redirecto to Auction
                            Session["UAA_PWD"] = userPassword;
                            return this.redirectToAnotherSystem(returnUrl);
                            //return RedirectToLocal(returnUrl);
                        }
                        //return RedirectToLocal(returnUrl);
                    }

                }
                catch (Exception ex)
                {
                    logger.Error(ex);

                    return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
                }

            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("Error", message);
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LoginEditTracking(LoginTrackingModel model)
        {

            #region----------------------------Set Cookies Language By Link------------------------

            HttpCookie cultureCookie = Request.Cookies["_culture"];

            if (!string.IsNullOrEmpty(model.LanguageCode))
            {
                string culture_id;

                culture_id = _languageRepository.GetLanguageIdByCode(model.LanguageCode);

                culture_id = CultureHelper.GetImplementedCulture(culture_id);

                if (cultureCookie != null)
                {

                    cultureCookie.Value = culture_id;   // update cookie value
                }
                else
                {

                    cultureCookie = new HttpCookie("_culture");

                    cultureCookie.Value = culture_id;
                    cultureCookie.Expires = DateTime.Now.AddDays(30);

                }
                Response.Cookies.Add(cultureCookie);
            }
            #endregion

            string languageIdLocal = "";


            if (cultureCookie != null)
            {
                languageIdLocal = cultureCookie.Value.ToString();

            }
            else
            {
                languageIdLocal = CultureHelper.GetImplementedCulture(languageIdLocal);// This is safe
            }

            int languageID = Convert.ToInt32(languageIdLocal);

            string messageReturn = "";

            string message = messageReturn.GetStringResource("_Login.ValidateMsg.InvalidUsername", languageIdLocal);

            #region Login
            try
            {

                bool checkLogin = false;
                string username = "";
                string guid = "";
                string user = "";
                string organization = "";
                string languageId = "";
                string returnUrl = "";

                #region Check Usre By Guid
                var userSessionModel = _userSessionRepository.GetUserSession(model.UserGuid);
                if (userSessionModel != null)
                {
                    DateTime lastAccessed = userSessionModel.LastAccessed ?? DateTime.Now;

                    DateTime utcNow = DateTime.UtcNow;


                    long elapsedTicks = utcNow.Ticks - lastAccessed.Ticks;
                    TimeSpan elapsedSpan = new TimeSpan(elapsedTicks);

                    double totalDateDiff = elapsedSpan.TotalMilliseconds;

                    string strTimeout = System.Configuration.ConfigurationManager.AppSettings.Get("TimeOut");

                    long timeOut = Convert.ToInt64(strTimeout);

                    if (timeOut > totalDateDiff && userSessionModel.isTimeout == 0)
                    {
                        #region----------Update UserSession-------------------
                        string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
                        string browserType = Request.Browser.Type;

                        userSessionModel.LastAccessed = utcNow;
                        userSessionModel.isTimeout = 0;
                        userSessionModel.IPAddress = visitorsIPAddr;
                        userSessionModel.BrowserType = browserType;
                        userSessionModel.LoginTime = DateTime.UtcNow;
                        _userSessionRepository.Update(userSessionModel);
                        #endregion

                        checkLogin = true;
                        username = userSessionModel.Username;
                        guid = userSessionModel.UserGuid;
                    }
                    else
                    {
                        userSessionModel.isTimeout = 1;
                        _userSessionRepository.Update(userSessionModel);
                        checkLogin = false;

                    }

                }
                #endregion

                returnUrl = CryptographyManage.StringDecode(model.ReturnUrl);

                #region Check Login And Add Session
                if (checkLogin)
                {
                    user = UserDataService.GetUserShowByUsernameAndLanguageId(username, languageID);
                    organization = UserDataService.GetOrganizByUsernameAndLanguageId(username, languageID);



                    Session["username"] = username;
                    Session["GUID"] = guid;
                    Session["Org"] = organization;
                    Session["user"] = user;
                    Session["ReturnUrlTracking"] = returnUrl;

                    if (!string.IsNullOrEmpty(languageId))
                    {
                        HttpCookie cookie = Request.Cookies["_culture"];

                        if (cookie != null)
                        {

                            cookie.Value = languageId;   // update cookie value
                        }
                        Response.Cookies.Add(cookie);
                    }
                    FormsAuthentication.SetAuthCookie(username, false);

                    #region RedirectToAction
                    if (model.TrackingGrpID == 1)
                    {
                        return RedirectToAction("General", "EditDataTracking", new { trackingReqID = model.TrackingReqID, trackingItemID = model.TrackingItemID, trackingType = model.TrackingType });
                    }
                    else if (model.TrackingGrpID == 2)
                    {
                        return RedirectToAction("Document", "EditDataTracking", new { trackingReqID = model.TrackingReqID });
                    }
                    else if (model.TrackingGrpID == 3)
                    {
                        return RedirectToAction("Address", "EditDataTracking", new { trackingReqID = model.TrackingReqID });
                    }
                    #endregion

                }
                else
                {
                    return Redirect(returnUrl);
                }
                #endregion

            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }
            #endregion




            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("Error", message);
            return RedirectToAction("Login", "AccountPortal");
        }

        #endregion

        #region-----------------------------Action and Method Helper-------------------------------
        private HttpClient SetHeaderLoginPortal(string strUserName, string userPassword)
        {
            //string publicKey = "89844013dc5f506455e8dd3a48e5b4a9";
            string publicKey = WebConfigurationManager.AppSettings["publicKey"];
            //string strUserName = "portal@pantavanij.com";
            //string secretKey = "769fa783018bf7ff350697f5d7a218be";
            string secretKey = WebConfigurationManager.AppSettings["secretKey"];
            //string baseURL = "http://localhost:3867/";
            string baseURL = WebConfigurationManager.AppSettings["baseURL"];
            string signature = CryptoExtension.Signature(publicKey, secretKey);

            string authInfo = strUserName + ":" + signature;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
            client.DefaultRequestHeaders.Add("User-Agent", "WebApiForPortal");
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            client.DefaultRequestHeaders.Add("Accept-Charset", "UTF-8");
            client.DefaultRequestHeaders.Add("AuthInfo", authInfo);
            client.DefaultRequestHeaders.Add("PublicKey", publicKey);
            //client.DefaultRequestHeaders.Add("baseURL", baseURL);
            client.DefaultRequestHeaders.Add("Userpassword", userPassword);

            return client;
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private ActionResult redirectToAnotherSystem(string returnUrl)
        {

            if (Session["redirect_action_name"] != null)
            {
                string actionName = Session["redirect_action_name"].ToString();
                return RedirectToAction(actionName, "Redirect");
            }
            else
            {
                return RedirectToLocal(returnUrl);
            }
        }
        #endregion

        #region
        private async Task<bool> callTermAndCondotionFromWeomni(string strUserName)
        {
            bool result = false;
            try
            {
                JObject jsonData = await _termAndConditionService.GetTermAndCondition(strUserName);
                if (jsonData != null)
                {
                    if (Convert.ToBoolean(jsonData["isSuccess"]))
                    {
                        if (jsonData["data"].SelectToken("payload").Count() > 0)
                        {
                            var payload = jsonData["data"].SelectToken("payload")[0];
                            Session["TermAndCondtionConsentId"] = payload.SelectToken("id").ToString();

                            Session["PrivacyNoticeEN"] = payload.SelectToken("privacyNotice").SelectToken("descriptionEn").ToString();
                            Session["PrivacyNoticeTH"] = payload.SelectToken("privacyNotice").SelectToken("descriptionTh").ToString();

                            Session["TermAndConditionEN"] = payload.SelectToken("termsAndCondition").SelectToken("descriptionEn").ToString();
                            Session["TermAndConditionTH"] = payload.SelectToken("termsAndCondition").SelectToken("descriptionTh").ToString();
                            result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return result;
        }

        private async Task<bool> callCOCWeomni(string strUserName)
        {
            bool result = false;
            try
            {
                //HttpClient clientCoc = new HttpClient();
                //clientCoc = SetHeaderLoginPortal(strUserName, userPassword);
                //string urlClientCoc = string.Format("{0}api/coc/get/{1}", baseURL, strUserName);
                //HttpResponseMessage responseCoc = clientCoc.GetAsync(urlClientCoc).Result;
                //if (responseCoc.StatusCode == System.Net.HttpStatusCode.OK)
                //{
                //    var responseBodyCoc = responseCoc.Content.ReadAsStringAsync().Result;
                //    JObject _responseCoc = JObject.Parse(responseBodyCoc);
                JObject jsonData = await _termAndConditionService.GetCOC(strUserName);
                if (Convert.ToBoolean(jsonData["isSuccess"]))
                {
                    if (jsonData["data"].SelectToken("payload").Count() > 0)
                    {
                        var payload = jsonData["data"].SelectToken("payload")[0];
                        Session["CodeOfConductConsentId"] = payload.SelectToken("id").ToString();

                        Session["CodeOfConductPrivacyNoticeEN"] = payload.SelectToken("privacyNotice").SelectToken("descriptionEn").ToString();
                        Session["CodeOfConductPrivacyNoticeTH"] = payload.SelectToken("privacyNotice").SelectToken("descriptionTh").ToString();
                        result = true;
                    }
                }
                //}
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return result;
        }
        #endregion
    }
}
