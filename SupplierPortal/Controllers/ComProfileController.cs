﻿using SupplierPortal.Framework.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.MenuPortalComProfile;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Framework.MethodHelper;
using System.Web.Configuration;

namespace SupplierPortal.Controllers
{
    public class ComProfileController : Controller
    {
        IMenuPortalComProfileRepository _menuPortalComProfileRepository;

        public ComProfileController()
        {
            _menuPortalComProfileRepository = new MenuPortalComProfileRepository();
        }
        public ComProfileController
            (
            MenuPortalComProfileRepository menuPortalComProfileRepository
            )
        {
            _menuPortalComProfileRepository = menuPortalComProfileRepository;
        }


        // GET: ComProfile
        public ActionResult Index()
        {
            IUserSessionRepository _repoUsersession;
            IUserRepository _repoUser;
            _repoUsersession = new UserSessionRepository();
            _repoUser = new UserRepository();

            if (Session["GUID"] == null)
            {
                //Response.Redirect("~/AccountPortal/Logout");
                return RedirectToAction("Index", "AccountPortal");
            }
            else
            {
                string guid = Session["GUID"].ToString();
                var listUser = _repoUsersession.GetUserSession(guid);
                if (listUser.isTimeout == 0)
                {
                    var ChkisAcceptTermOfUse = _repoUser.FindByUsername(listUser.Username);
                    string uesTermWeomni = WebConfigurationManager.AppSettings["switch_use_term_weomni"];
                    if (ChkisAcceptTermOfUse.isAcceptTermOfUse == 0 && !Convert.ToBoolean(uesTermWeomni))
                    {
                        //Response.Redirect("~/Agreement/Index");
                        return RedirectToAction("Index", "Agreement");
                    }
                }
            }

            //string cultureId = null;
            //HttpCookie cultureCookie = Request.Cookies["_culture"];
            //var languagesBrowser = Request.UserLanguages[0];
            //string[] words = languagesBrowser.Split('-');
            //string cultureCurrenc = words[0];
            //string currencCultureId = CultureHelper.GetCultureIdByCurrencCultureCode(cultureCurrenc);

            //if (cultureCookie != null)
            //{

            //    cultureId = cultureCookie.Value;
            //}
            //else
            //{
            //    cultureId = null;
            //    cultureId = CultureHelper.GetImplementedCulture(currencCultureId); // This is safe
            //    cultureCookie = new HttpCookie("_culture");
            //    cultureCookie.Value = cultureId;
            //    cultureCookie.Expires = DateTime.Now.AddDays(30);

            //}
            //Response.Cookies.Add(cultureCookie);
            //Session["username"] = "Metro-Admin";

            return View();
        }

        public ActionResult Index2()
        {
            //string cultureId = null;
            //HttpCookie cultureCookie = Request.Cookies["_culture"];
            //var languagesBrowser = Request.UserLanguages[0];
            //string[] words = languagesBrowser.Split('-');
            //string cultureCurrenc = words[0];
            //string currencCultureId = CultureHelper.GetCultureIdByCurrencCultureCode(cultureCurrenc);

            //if (cultureCookie != null)
            //{

            //    cultureId = cultureCookie.Value;
            //}
            //else
            //{
            //    cultureId = null;
            //    cultureId = CultureHelper.GetImplementedCulture(currencCultureId); // This is safe
            //    cultureCookie = new HttpCookie("_culture");
            //    cultureCookie.Value = cultureId;
            //    cultureCookie.Expires = DateTime.Now.AddDays(30);

            //}
            //Response.Cookies.Add(cultureCookie);
            //Session["username"] = "Metro-Admin";

            return View();
        }

        public ActionResult CompanyProfileMenu()
        {
            //Session["username"] = "Metro-Admin";

            var model = _menuPortalComProfileRepository.GetMenuCompanyProfile();
            return View(@"~/Views/ComProfile/CompanyProfileMenu.cshtml", model);
        }
        public ActionResult HeaderAndProgressPercent()
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            var path = Request.Path.ToString();
            var Menu = path.Split('/', ' ');

            var TitleStr = "";
            if (Menu[1] == "CompanyContact")
            {
                TitleStr = _menuPortalComProfileRepository.GetResourceNameForTitleMenu("ContactInformation");
            }
            else if (Menu[1] == "CompanyAdditional")
            {
                TitleStr = _menuPortalComProfileRepository.GetResourceNameForTitleMenu("AdditionalInformation");
            }
            else
            {
                TitleStr = _menuPortalComProfileRepository.GetResourceNameForTitleMenu(Menu[1]);
            }

            var str = _menuPortalComProfileRepository.GetResourceName(Menu[2]);

            if (languageID == "1")
            {
                ViewBag.titleMenu = TitleStr;
                ViewBag.subManu = str;
            }
            else
            {
                ViewBag.titleMenu = TitleStr;
                ViewBag.subManu = str;
            }
            //Session["username"] = "Metro-Admin";
            return View();
        }
    }
}
