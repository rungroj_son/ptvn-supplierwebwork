﻿using JWT;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SupplierPortal.Core.Caching;
using SupplierPortal.Core.Extension;
using SupplierPortal.Data.CustomModels.AccountPortal;
using SupplierPortal.Data.CustomModels.BuyerSuppliers;
using SupplierPortal.Data.CustomModels.Parameter;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.NewsDisplay;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Data.Models.SupportModel.AccountPortal;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Models.Dto.DataContract;
using SupplierPortal.Models.Enums;
using SupplierPortal.ServiceClients;
using SupplierPortal.Services.AccountManage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace SupplierPortal.Controllers
{
    public class BuyerPortalController : BaseController
    {
        private readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string BuyerUser_Config = "BuyerUser_Config";

        private readonly ICacheManager _cacheManager;
        private NameTitleService _nameTitleService;
        private LanguageService _languageService;
        private UserSessionService _userSessionService;
        private ISupplierPortalService _supplierService;
        private BuyerPortalService _buyerPortalService;
        private UserService _userService;
        private IContactPersonRepository _contactPersonRepository;
        private IAppConfigRepository _appConfigRepository;
        private SupplierPortal.Models.Enums.EnumHelper enumhelper;

        public BuyerPortalController()
        {
            _cacheManager = new MemoryCacheManager();
            _nameTitleService = new NameTitleService();
            _languageService = new LanguageService();
            _userSessionService = new UserSessionService();
            _supplierService = new SupplierPortalService();
            _buyerPortalService = new BuyerPortalService();
            _userService = new UserService();
            _contactPersonRepository = new ContactPersonRepository();
            _appConfigRepository = new AppConfigRepository();
            enumhelper = new SupplierPortal.Models.Enums.EnumHelper();
        }

        public IAuthenticationManager Authentication
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void FormLogin(string username, string organization)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, username));
            claims.Add(new Claim(ClaimTypes.Role, organization));
            var id = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);
            Authentication.SignIn(id);
        }

        // GET: BuyerPortal
        [AllowAnonymous]
        public async Task<ActionResult> Index(string returnUrl, string language = "")
        {
            #region----------------------------Set Cookies Language By Link------------------------
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (!string.IsNullOrEmpty(language))
            {
                string culture_id;

                culture_id = await _languageService.GetLanguageIdByCode(language);

                culture_id = CultureHelper.GetImplementedCulture(culture_id);

                if (cultureCookie != null)
                {
                    cultureCookie.Value = culture_id;   // update cookie value
                }
                else
                {
                    cultureCookie = new HttpCookie("_culture");

                    cultureCookie.Value = culture_id;
                    cultureCookie.Expires = DateTime.Now.AddDays(30);
                }
                Response.Cookies.Add(cultureCookie);
            }
            #endregion
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Logout(string returnUrl)
        {
            string userGuid = string.Empty;
            Tbl_UserSession userSession = null;
            try
            {
                if (Session["BuyerGUID"] != null)
                {
                    userGuid = Session["BuyerGUID"].ToString();
                    userSession = new Tbl_UserSession();
                    userSession = await _userSessionService.GetUserSession(userGuid);

                    if (userSession != null)
                    {
                        userSession.isTimeout = 1;
                        userSession.LogoutTime = DateTime.UtcNow;
                        var result = _userSessionService.Update(userSession);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Logout Controller : " + ex);
            }
            finally
            {
                Authentication.SignOut();
                Session["BuyerUsername"] = null;
                Session["BuyerPermissions"] = null;
                Session["BuyerGUID"] = null;
                Session["BuyerOrg"] = null;
                Session["BuyerUser"] = null;
                Session["EID"] = null;
                Session["SysUserID"] = null;
            }
            return RedirectToAction("Index", "BuyerPortal");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Index(LoginPortalModel model, string returnUrl)
        {
            string languageIdLocal = string.Empty;

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageIdLocal = cultureCookie.Value.ToString();
            }
            else
            {
                languageIdLocal = CultureHelper.GetImplementedCulture(languageIdLocal);// This is safe
            }

            int languageID = Convert.ToInt32(languageIdLocal);
            string visitorsIPAddr = GetClientIPAddressHelper.GetClientIPAddres();
            string browserType = Request.Browser.Type;

            string languageURL = "?languageId=" + languageIdLocal;
            string browserTypeURL = "&browserType=" + browserType;
            string clientIPAddressURL = "&clientIPAddress=" + visitorsIPAddr;

            string messageReturn = string.Empty;

            string message = messageReturn.GetStringResource("_Login.ValidateMsg.InvalidUsername", languageIdLocal);

            if (ModelState.IsValid)
            {
                try
                {
                    BuyerPortalService buyerPortalService = null;
                    ResultValidated resultValidate = null;

                    string publicKey = WebConfigurationManager.AppSettings["publicKey"];

                    string strUserName = model.Username;
                    string password = model.Password;
                    string userPassword = string.Empty;

                    int lockTimeSeconds = 0;

                    string result = string.Empty;
                    //encode password
                    result = CryptoExtension.Base64Encode(password);
                    //PublicKey + result password
                    result = result + publicKey;
                    // encode rusult
                    result = CryptoExtension.Base64Encode(result);
                    //strUserName : result
                    result = strUserName + ":" + result;
                    //encode result
                    userPassword = CryptoExtension.Base64Encode(result);

                    buyerPortalService = new BuyerPortalService();
                    resultValidate = await buyerPortalService.LoginValidate(strUserName, userPassword, languageURL, browserTypeURL, clientIPAddressURL);

                    //รับค่า lockTimeSeconds ไปในตัวแปร isAcceptTermOfUse เพื่อนำไปทำ Countdown Timer
                    //guid == "Locked" Fix มาจาก API กรณีที่ user กำลังอยู่ในช่วงเวลา Locked
                    if (resultValidate.GUID.Equals("Locked"))
                    {
                        lockTimeSeconds = Convert.ToInt32(resultValidate.isAcceptTermOfUse);
                        ViewBag.lockTimeSeconds = lockTimeSeconds;
                    }

                    if (resultValidate.Result)
                    {
                        Session["BuyerUsername"] = resultValidate.Username;
                        Session["BuyerGUID"] = resultValidate.GUID;
                        Session["BuyerOrg"] = resultValidate.Organization;
                        Session["BuyerUser"] = resultValidate.User;
                        Session["EID"] = resultValidate.EID;
                        Session["SysUserID"] = resultValidate.SysUserID;

                        List<string> permissions = await GetBuyerRolePrivilge(Int32.Parse(resultValidate.SysUserID));
                        Session["BuyerPermissions"] = permissions;

                        #region----------Update UserSession-------------------
                        UserSessionService userSessionService = new UserSessionService();
                        Tbl_UserSession userSession = new Tbl_UserSession();
                        userSession = await userSessionService.GetUserSession(resultValidate.GUID);

                        if (userSession != null)
                        {
                            userSession.IPAddress = visitorsIPAddr;
                            userSession.BrowserType = browserType;
                            userSession.LoginTime = DateTime.UtcNow;
                            var userResult = userSessionService.Update(userSession);
                        }
                        #endregion

                        if (!string.IsNullOrEmpty(resultValidate.LanguageId))
                        {
                            HttpCookie cookie = Request.Cookies["_culture"];

                            if (cookie != null)
                            {
                                cookie.Value = resultValidate.LanguageId;   // update cookie value
                            }
                            Response.Cookies.Add(cookie);
                        }

                        this.FormLogin(resultValidate.Username, resultValidate.Organization);

                        if (resultValidate.Result)
                        {
                            return RedirectToAction("SupplierList", "BuyerPortal");
                        }
                        else
                        {
                            return RedirectToLocal(returnUrl);
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("BuyerPortalLogin Controller : " + ex);
                    return RedirectToAction("BuyerPortalError", "Error", new { ErrorMessage = ex.Message });
                }
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("Error", message);
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> SupplierUserDetail(string id = "", int eid = 0, int supid = 0, string errorMessage = "")
        {
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            BuyerSupplierProfile model = null;
            ViewBuyerSupplier buyerSupplier = null;
            List<string> permissions = null;

            string languageID = string.Empty;
            try
            {
                if (Session["BuyerPermissions"] != null)
                {
                    permissions = (List<string>)Session["BuyerPermissions"];
                    //Find Language
                    if (cultureCookie != null)
                    {
                        languageID = cultureCookie.Value.ToString();
                    }
                    if (string.IsNullOrEmpty(languageID))
                    {
                        languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
                    }

                    int languageId = Convert.ToInt32(languageID);
                    var language = await _languageService.GetLanguage();
                    var languageLocal = language.ToList().Where(m => m.isLocal == 1).FirstOrDefault();
                    var languageInter = language.ToList().Where(m => m.isInter == 1).FirstOrDefault();
                    ViewBag.NameTitle_Local = await _nameTitleService.GetNameTitleList(languageLocal.LanguageID);
                    ViewBag.NameTitle_Inter = await _nameTitleService.GetNameTitleList(languageInter.LanguageID);
                    ViewBag.ErrorMessage = errorMessage;

                    // View page
                    if (string.IsNullOrEmpty(id) && supid != 0 && eid != 0) // Insert
                    {
                        if (permissions.Contains(enumhelper.GetEnumDescription(ContactManagement.CONTACT_CREATE)))
                        {
                            model = new BuyerSupplierProfile();
                            model.EID = eid;
                            model.SupplierID = supid;
                            GetOrganizationResponse response = await _supplierService.GetOrganization(supid);
                            model.CompanyName_Local = response.data.CompanyName_Local;
                            model.CompanyName_Inter = response.data.CompanyName_Inter;
                            model.TaxID = response.data.TaxID;
                            ViewBag.ActionStage = ActionStage.INSERT_STAGE;

                            return View(model);
                        }
                    }
                    else if (!string.IsNullOrEmpty(id) && supid == 0 && eid == 0) // Update
                    {
                        if (permissions.Contains(enumhelper.GetEnumDescription(ContactManagement.CONTACT_EDIT)) || permissions.Contains(enumhelper.GetEnumDescription(ContactManagement.CONTACT_VIEW)))
                        {
                            buyerSupplier = await _buyerPortalService.GetBuyerSupplierContactDetail(Convert.ToInt32(id));
                            model = new BuyerSupplierProfile();
                            model.CompanyName_Local = buyerSupplier.CompanyName_Local;
                            model.CompanyName_Inter = buyerSupplier.CompanyName_Inter;
                            model.TaxID = buyerSupplier.TaxID;
                            model.UserLoginID = buyerSupplier.Username;
                            model.TitleID = buyerSupplier.TitleID;
                            model.FirstName_Local = buyerSupplier.FirstName_Local;
                            model.LastName_Local = buyerSupplier.LastName_Local;
                            model.FirstName_Inter = buyerSupplier.FirstName_Inter;
                            model.LastName_Inter = buyerSupplier.LastName_Inter;
                            model.PhoneNo = buyerSupplier.PhoneNo;
                            model.PhoneExt = buyerSupplier.PhoneExt;
                            model.Email = buyerSupplier.Email;
                            model.UserID = buyerSupplier.UserID;
                            model.TitleID_Inter = buyerSupplier.TitleID;
                            model.ContactID = buyerSupplier.ContactID;
                            model.MobileNo = buyerSupplier.MobileNo;
                            model.IsPrimaryContact = buyerSupplier.isPrimaryContact == 1 ? true : false;
                            model.MobileCountryCode = buyerSupplier.MobileCountryCode;
                            model.SupplierID = buyerSupplier.SupplierID;
                            model.IsDisabled = buyerSupplier.IsDisabled;
                            ViewBag.ActionStage = ActionStage.UPDATE_STAGE;

                            return View(model);
                        }
                    }
                    else
                    {
                        return RedirectToAction("BuyerPortalError", "Error", new { ErrorMessage = "Page not found" });
                    }
                }
                else
                {
                    return RedirectToAction("UserTimeout", "Error");
                }
            }
            catch (Exception ex)
            {
                logger.Error("SupplierUserDetail Controller : " + ex);
                return RedirectToAction("BuyerPortalError", "Error", new { ErrorMessage = ex.Message });
            }

            return RedirectToAction("SupplierList", "BuyerPortal");
        }

        [HttpPost]
        public async Task<ActionResult> UpdateBuyerContactPerson(BuyerSupplierProfile model)
        {
            string strSysUserID = string.Empty;
            List<string> permissions = null;

            try
            {
                if (Session["BuyerPermissions"] != null)
                {
                    permissions = (List<string>)Session["BuyerPermissions"];
                    if (model.UserID == null || model.UserID == 0) // Insert
                    {
                        if (Session["SysUserID"] != null)
                        {
                            int isPrimaryContactValue = model.IsPrimaryContact ? 1 : 0;
                            strSysUserID = Session["SysUserID"].ToString();
                            SupplierUserModel requestModel = new SupplierUserModel
                            {
                                SupplierID = model.SupplierID.Value,
                                EID = model.EID,
                                FirstName_Local = model.FirstName_Local,
                                LastName_Local = model.LastName_Local,
                                FirstName_Inter = model.FirstName_Inter,
                                LastName_Inter = model.LastName_Inter,
                                TitleID = model.TitleID,
                                PhoneNo = model.PhoneNo,
                                PhoneExt = model.PhoneExt,
                                MobileCountryCode = model.MobileCountryCode,
                                MobileNo = model.MobileNo,
                                Email = model.Email,
                                UserID = Convert.ToInt32(model.UserID),
                                Username = model.UserLoginID,
                                CurrentID = Convert.ToInt32(strSysUserID),
                                isPrimaryContact = isPrimaryContactValue
                            };
                            SupplierUserRequest request = new SupplierUserRequest { SupplierUser = requestModel };
                            CreateResponse result = await _buyerPortalService.InsertSupplierUser(request);

                            if (!result.Success)
                                return RedirectToAction("BuyerPortalError", "Error", new { ErrorMessage = result.Message });
                        }
                        else
                        {
                            return RedirectToAction("UserTimeout", "Error");
                        }
                    }
                    else // Update
                    {
                        if (permissions.Contains(enumhelper.GetEnumDescription(ContactManagement.CONTACT_EDIT)))
                        {
                            if (Session["SysUserID"] != null)
                            {
                                strSysUserID = Session["SysUserID"].ToString();
                                bool result = await _buyerPortalService.UpdateBuyerContactPerson(model, strSysUserID);

                                if (!result)
                                    return RedirectToAction("SupplierUserDetail", "BuyerPortal", new { id = model.UserID, errorMessage = "_Msg.Validate.LblNoDataChange" });
                            }
                            else
                            {
                                return RedirectToAction("UserTimeout", "Error");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("UpdateBuyerContactPerson Controller : " + ex);
                return RedirectToAction("BuyerPortalError", "Error", new { ErrorMessage = ex.Message });
            }

            return RedirectToAction("SupplierList", "BuyerPortal");
        }

        [HttpGet]
        public async Task<ActionResult> UserLogDetail(string id)
        {
            List<BuyerHistoryContactPersonModel> model = null;
            List<string> permissions = null;

            try
            {
                if (Session["BuyerPermissions"] != null)
                {
                    permissions = (List<string>)Session["BuyerPermissions"];

                    if (permissions.Contains(enumhelper.GetEnumDescription(ContactManagement.CONTACT_VIEW_HISTORY)))
                    {
                        if (Session["SysUserID"] == null)
                        {
                            return RedirectToAction("UserTimeout", "Error");
                        }
                        else
                        {
                            model = new List<BuyerHistoryContactPersonModel>();

                            model = await _buyerPortalService.GetListHistoryBuyerContactPersonByUserId(Convert.ToInt32(id));
                            if (!model.Any())
                            {
                                return RedirectToAction("SupplierList", "BuyerPortal");
                            }
                            else
                            {
                                return View(model);
                            }
                        }
                    }
                }
                else
                {
                    return RedirectToAction("UserTimeout", "Error");
                }
            }
            catch (Exception ex)
            {
                logger.Error("UserLogDetail Controller : " + ex);
                return RedirectToAction("BuyerPortalError", "Error", new { ErrorMessage = ex.Message });
            }

            return RedirectToAction("SupplierList", "BuyerPortal");
        }

        [HttpPost]
        public async Task<ActionResult> VerifyEntry(string jwt_data)
        {
            API_EPHeaderReponseModel model = null;
            string SysUserID = string.Empty;
            string eid = string.Empty;
            string sid = string.Empty;
            bool isManage = false;
            try
            {
                var key = new byte[]
                {
                    1, 2, 3, 4, 5, 6, 7, 8,
                    1, 2, 3, 4, 5, 6, 7, 8,
                    1, 2, 3, 4, 5, 6, 7, 8,
                    1, 2, 3, 4, 5, 6, 7, 8,
                    1, 2, 3, 4, 5, 6, 7, 8,
                    1, 2, 3, 4, 5, 6, 7, 8,
                    1, 2, 3, 4, 5, 6, 7, 8,
                    1, 2, 3, 4, 5, 6, 7, 8
                };

                logger.Info("VerifyEntry[JWT Token] --> " + jwt_data);

                // JWT decode by key byte array
                var jsonResult = JsonWebToken.Decode(jwt_data, key, true);
                logger.Info("VerifyEntry[JWT Decode] --> " + jsonResult);

                // Select token 'data'
                var data = JObject.Parse(jsonResult).SelectToken("data").ToString();

                // Parse to object
                model = JsonConvert.DeserializeObject<API_EPHeaderReponseModel>(data);

                if (model.result != "SUCCESS")
                {
                    logger.Error("VerifyEntry[JSON Result Data] --> " + data);
                    return View("ErrorTokenEntry", model);
                }

                Session.Clear();

                //Get Permission
                List<string> permissions = await GetBuyerRolePrivilgeByEPUserId(model.SID, Convert.ToInt32(model.SysUserID), !string.IsNullOrEmpty(model.EID) ? Int32.Parse(model.EID) : 0);
                Session["BuyerPermissions"] = permissions;
                Session["BuyerUsername"] = model.Name;
                Session["BuyerGUID"] = model.SID;
                Session["BuyerOrg"] = model.Organization;
                Session["BuyerUser"] = model.Name;
                Session["EID"] = model.EID;
                Session["SysUserID"] = model.SysUserID;
                Session["JSON_Data"] = data;
                Session["jwt_data"] = jwt_data;
                Session["IsPrivate"] = false;

                if (permissions.Any())
                {
                    var config = await _buyerPortalService.GetBuyerConfigByEID(Convert.ToInt32(model.EID));

                    isManage = permissions.Contains("scm") ? true : false;

                    if (!isManage)
                        return RedirectToAction("BuyerPortalError", "Error", new { ErrorMessage = "Page not found" });

                    if (config != null)
                        if (config.Value == "0")
                            Session["IsPrivate"] = true;
                }
            }
            catch (Exception ex)
            {
                logger.Error("VerifyEntry Controller : " + ex);
                return RedirectToAction("BuyerPortalError", "Error", new
                {
                    ErrorMessage = ex.Message
                });
            }

            return RedirectToAction("SupplierList", "BuyerPortal");
        }

        public ActionResult RedirectDirectory(string url)
        {
            string parameter = string.Empty;
            UrlParameterModel urlParameter = null;
            try
            {
                if (Session["jwt_data"] != null)
                {
                    parameter = Session["jwt_data"].ToString();
                    string redirect = WebConfigurationManager.AppSettings["home_verify"];
                    url = url + redirect;
                    urlParameter = new UrlParameterModel()
                    {
                        Url = url,
                        Parameter = parameter
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw;
            }
            finally
            {
                _cacheManager.Clear();
                Session.Clear();
            }
            return Json(urlParameter);
        }

        #region-----------------------------Action and Method Helper-------------------------------

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private async Task<List<string>> GetBuyerRolePrivilge(int sysUserId)
        {
            List<string> permissions = null;
            List<Tbl_BuyerRolePrivilege> results = await _buyerPortalService.GetBuyerRolePrivileges(sysUserId);

            if (results != null)
            {
                if (results.Any())
                {
                    permissions = new List<string>();
                    foreach (Tbl_BuyerRolePrivilege item in results)
                    {
                        permissions.Add(item.PrivilegeCode);
                    }
                }
            }
            else
            {
            }

            return permissions;
        }

        private async Task<List<string>> GetBuyerRolePrivilgeByEPUserId(string sessionUserId, int epUserId, int eid)
        {
            List<string> permissions = null;
            List<Tbl_BuyerRolePrivilege> results = await _buyerPortalService.GetBuyerRolePrivilgeByEPUserId(sessionUserId, epUserId, eid);

            if (results != null)
            {
                if (results.Any())
                {
                    permissions = new List<string>();
                    foreach (Tbl_BuyerRolePrivilege item in results)
                    {
                        permissions.Add(item.PrivilegeCode);
                    }
                }
            }
            else
            {
            }

            return permissions;
        }

        [HttpGet]
        public async Task<ActionResult> ActiveInActive(int userId, int isActive)
        {
            List<string> permissions = null;
            bool isCanEdit = false;
            try
            {
                if (Session["BuyerPermissions"] != null)
                {
                    permissions = (List<string>)Session["BuyerPermissions"];

                    if (isActive == 1)
                    {
                        if (permissions.Contains(enumhelper.GetEnumDescription(ContactManagement.CONTACT_ACTIVE)))
                        {
                            isCanEdit = true;
                        }
                    }

                    if (isActive == 0)
                    {
                        if (permissions.Contains(enumhelper.GetEnumDescription(ContactManagement.CONTACT_INACTIVE)))
                        {
                            isCanEdit = true;
                        }
                    }

                    if (isCanEdit)
                    {
                        bool result = await _userService.UpdateUserActiveInActive(userId);

                        if (result)
                            return RedirectToAction("SupplierUserDetail", "BuyerPortal", new { id = userId });
                        else
                            return RedirectToAction("SupplierUserDetail", "BuyerPortal", new { id = userId, errorMessage = "_ApiMsg.DataIsValid" });
                    }
                    else
                    {
                        return RedirectToAction("UserTimeout", "Error");
                    }
                }
                else
                {
                    return RedirectToAction("UserTimeout", "Error");
                }
            }
            catch (Exception ex)
            {
            }
            return View();
        }

        public ActionResult ClearSession()
        {
            try
            {
                Session.Clear();
            }
            catch (Exception ex)
            {
            }
            return Json(true);
        }

        #endregion

        #region View Page

        /// <summary>
        /// Supplier List View
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ///
        [AllowAnonymous]
        public async Task<ActionResult> SupplierList()
        {
            try
            {
                int EID = 0;
                Session["IsPrivate"] = true;
                if (Session["EID"] == null)
                {
                    return RedirectToAction("UserTimeout", "Error");
                }
                else
                {
                    EID = Int32.Parse(Session["EID"].ToString());
                }

                string languageId = string.Empty;
                HttpCookie cultureCookie = Request.Cookies["_culture"];
                if (cultureCookie != null)
                {
                    languageId = cultureCookie.Value.ToString();
                }
                else
                {
                    languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
                }
                //bool isLocalLanguage = await _service.IsLocalLanguage(languageId);

                //ViewBag.isLocalLanguage = isLocalLanguage;
                ViewBag.EID = EID;

                return View();
            }
            catch (Exception ex)
            {
                logger.Error("SupplierList Controller : " + ex);
                return RedirectToAction("BuyerPortalError", "Error", new { ErrorMessage = ex.Message });
            }
        }

        #endregion

        #region GET

        /// <summary>
        /// Get Buyer Supplier List by EID
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> GetBuyerSuppliers([System.Web.Http.FromBody]GetBuyerSuppliersRequest request)
        {
            string languageId = string.Empty;
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }
            request.Language = string.IsNullOrEmpty(languageId) ? 0 : int.Parse(languageId);

            GetBuyerSuppliersResponse response = await _supplierService.GetBuyerSuppliers(request);
            return Json(response);
        }

        public ActionResult IsEmailAvailableConfig(string email, string initialEmail = "")
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var IsAllowDuplicateEmail = _appConfigRepository.GetAppConfigByName("Portal_IsAllowDuplicateUserEmail").Value;

            var IsAllowMultipleEmail = _appConfigRepository.GetAppConfigByName("Portal_IsAllowMultipleUserEmail").Value;

            string messageReturn = "";

            bool IsValid = false;
            if (IsAllowDuplicateEmail != "1")
            {
                bool chk = _contactPersonRepository.EmailExists(email.Trim(), initialEmail.Trim());

                if (!chk)
                {
                    IsValid = true;
                }
                else
                {
                    string errorMessage = messageReturn.GetStringResource("_ComPro.ContactPerson.ValidateMsg.DuplicateEmail", languageId);
                    return Json(errorMessage, JsonRequestBehavior.AllowGet);
                }
            }

            if (IsAllowMultipleEmail == "1")
            {
                string emailSeparator = _appConfigRepository.GetAppConfigByName("Portal_EmailSeparator").Value;

                foreach (string value in email.Split(Char.Parse(emailSeparator)))
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        IsValid = false;
                        string errorMessage = messageReturn.GetStringResource("_ComPro.ContactPerson.ValidateMsg.InvalidFormatEmail", languageId);
                        return Json(errorMessage, JsonRequestBehavior.AllowGet);
                    }
                    string pattern = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

                    //if (IsValidEmailAddress(value))
                    if (IsValidEmailAddressRegular(value, pattern))
                    {
                        IsValid = true;
                    }
                    else
                    {
                        string errorMessage = messageReturn.GetStringResource("_ComPro.ContactPerson.ValidateMsg.InvalidFormatEmail", languageId);
                        return Json(errorMessage, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                string pattern = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

                //if (IsValidEmailAddress(value))
                if (IsValidEmailAddressRegular(email, pattern))
                {
                    IsValid = true;
                }
                else
                {
                    string errorMessage = messageReturn.GetStringResource("_ComPro.ContactPerson.ValidateMsg.InvalidFormatEmail", languageId);
                    return Json(errorMessage, JsonRequestBehavior.AllowGet);
                }
            }

            if (IsValid)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            return Json(messageReturn, JsonRequestBehavior.AllowGet);
        }

        private bool IsValidEmailAddressRegular(string emailAddress, string pattern)
        {
            return new System.ComponentModel.DataAnnotations
                .RegularExpressionAttribute(pattern)
                .IsValid(emailAddress);
        }

        #endregion
    }
}