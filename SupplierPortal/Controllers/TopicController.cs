﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SupplierPortal.Controllers
{
    public class TopicController : ControllerBase
    {
        // GET: Topic/SystemUserDisabled
        public ActionResult SystemUserDisabled()
        {
            return View();
        }

        // GET: Topic/ServiceUnavailable_4NonAdmin
        public ActionResult ServiceUnavailable_4NonAdmin()
        {
            return View();
        }

        // GET: Topic/ServiceUnavailable
        public ActionResult ServiceUnavailable()
        {
            return View();
        }

        // GET: Topic/ServiceSuspend
        public ActionResult ServiceSuspend()
        {
            return View();
        }

        // GET: Topic/ServiceCancelled
        public ActionResult ServiceCancelled()
        {
            return View();
        }
    }
}