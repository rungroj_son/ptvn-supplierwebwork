﻿using SupplierPortal.Data.Models.Repository.LogUser;
using SupplierPortal.Data.Models.Repository.Topic;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Framework.MethodHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SupplierPortal.Controllers
{
    public class AgreementController : Controller
    {
        ITopicRepository _topicRepository;
        IUserSessionRepository _repoUsersession;
        IUserRepository _repoUser;
        ILogUserRepository _repoLogUser;

        public AgreementController()
        {
            _topicRepository = new TopicRepository();
            _repoUsersession = new UserSessionRepository();
            _repoUser = new UserRepository();
            _repoLogUser = new LogUserRepository();
        }

        public AgreementController(
            TopicRepository topicRepository,
            UserSessionRepository repoUsersession,
            UserRepository repoUser,
            LogUserRepository repoLogUser
            )
        {
            _topicRepository = topicRepository;
            _repoUsersession = repoUsersession;
            _repoUser = repoUser;
            _repoLogUser = repoLogUser;
        }

        // GET: Agreement
        public ActionResult Index()
        {

            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];

            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var model = _topicRepository.GetResourceBody("Register.AgreementTerm", languageId);

            ViewBag.Body = model;

            return View();
        }


        public ActionResult UpdateAcceptTermOfUse(string AcceptID)
        {

            if (Session["GUID"] == null)
            {
                Response.Redirect("~/AccountPortal/Login");
            }

            string username = "";
            if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }
            var user = _repoUser.FindByUsername(username);
            int acceptermId = 0;
            if (user != null)
            {
                acceptermId = user.isAcceptTermOfUse??0;
            }
            if (AcceptID != "" && AcceptID != null && acceptermId != 1)
            {
                string guid = Session["GUID"].ToString();
                var listUserbyGUID = _repoUsersession.GetUserSession(guid);
                //string username = listUserbyGUID.Username;
                

                int acceptID = Convert.ToInt32(AcceptID);
                if (acceptID == 0)
                {
                    var listUserDisAg = _repoUser.FindByUsername(username);

                    _repoUser.UpdateIsAcceptermId(listUserDisAg.UserID, acceptID);

                    _repoLogUser.Insert(listUserDisAg.UserID, "Modify", listUserDisAg.Username);

                    Response.Redirect("~/AccountPortal/Logout");
                }
                else
                {
                    var listUserAg = _repoUser.FindByUsername(username);

                    _repoUser.UpdateIsAcceptermId(listUserAg.UserID, acceptID);

                    _repoLogUser.Insert(listUserAg.UserID, "Modify", listUserAg.Username);

                }
            }
            else
            {
                Response.Redirect("~/AccountPortal/Logout");
            }

            //var user = _repoUser.FindByUsername(username);
            //if (user.IsActivated == 0)
            //{
            //    Response.Redirect("~/Activated/ChangePassword");
            //}
            return RedirectToAction("Index", "Home");

        }

        public ActionResult SCFPrivacyContent()
        {
            try
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("~/Error/Timeout");
                }

                string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

                if(int.Parse(languageID) == 1)
                {
                    return PartialView(@"~/Views/Agreement/_SCFPrivacyContent_EN.cshtml");
                }
                else
                {
                    return PartialView(@"~/Views/Agreement/_SCFPrivacyContent_TH.cshtml");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }
        }

        public ActionResult SCFTermsAndConditionsContent()
        {
            try
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("~/Error/Timeout");
                }

                string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

                if (int.Parse(languageID) == 1)
                {
                    return PartialView(@"~/Views/Agreement/_SCFTermsAndConditionsContent_EN.cshtml");
                }
                else
                {
                    return PartialView(@"~/Views/Agreement/_SCFTermsAndConditionsContent_TH.cshtml");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }
        }

        public ActionResult SCFAgreementContent()
        {
            try
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("~/Error/Timeout");
                }

                string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

                if (int.Parse(languageID) == 1)
                {
                    return PartialView(@"~/Views/Agreement/_SCFAgreementContent_TH.cshtml");
                }
                else
                {
                    return PartialView(@"~/Views/Agreement/_SCFAgreementContent_TH.cshtml");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }
        }

        public ActionResult SCFInprogressContent()
        {
            try
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("~/Error/Timeout");
                }

                string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

                if (int.Parse(languageID) == 1)
                {
                    return PartialView(@"~/Views/Agreement/_SCFInprogressContent_TH.cshtml");
                }
                else
                {
                    return PartialView(@"~/Views/Agreement/_SCFInprogressContent_TH.cshtml");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }
        }

        public ActionResult SCFRegisterSuccessContent()
        {
            try
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("~/Error/Timeout");
                }

                string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

                if (int.Parse(languageID) == 1)
                {
                    return PartialView(@"~/Views/Agreement/_SCFRegisterSuccessContent_TH.cshtml");
                }
                else
                {
                    return PartialView(@"~/Views/Agreement/_SCFRegisterSuccessContent_TH.cshtml");
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }
        }
    }
}