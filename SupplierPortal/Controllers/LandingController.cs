﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.Topic;
using SupplierPortal.Data.Models.Repository.NewsDisplay;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Framework.MethodHelper;
using System.IO;
using System.Web.Configuration;

namespace SupplierPortal.Controllers
{
    public class LandingController : BaseController
    {
        ITopicRepository _topicRepository;
        InewsDisplayRepository _newsDisplayRepository; 

        public LandingController()
        {
            _topicRepository = new TopicRepository();
            _newsDisplayRepository = new newsDisplayRepository();

        }

        public LandingController(
            TopicRepository topicRepository,
            newsDisplayRepository newsDisplayRepository
            )
        {
            _topicRepository = topicRepository;
            _newsDisplayRepository = newsDisplayRepository;

        }

        // GET: Landing
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult WhyVerification()
        {

            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];

            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var model = _topicRepository.GetResourceBody("Landing.TopMenu1", languageId);

            ViewBag.Body = model;

            return View();
        }

        public ActionResult ProcessofVerification()
        {

            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];

            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var model = _topicRepository.GetResourceBody("Landing.TopMenu2", languageId);

            ViewBag.Body = model;

            return View();
        }

        public ActionResult SuccessStories()
        {

            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];

            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var model = _topicRepository.GetResourceBody("Landing.TopMenu3", languageId);

            ViewBag.Body = model;

            return View();
        }

        public ActionResult PressSection()
        {

            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];

            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var model = _topicRepository.GetResourceBody("Landing.TopMenu4", languageId);

            ViewBag.Body = model;

            return View();
        }

        public ActionResult GetVerified()
        {

            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];

            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var model = _topicRepository.GetResourceBody("Landing.GetVerify", languageId);

            ViewBag.Body = model;

            return View();
        }

        public ActionResult FAQs()
        {

            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];

            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var model = _topicRepository.GetResourceBody("Landing.TopMenu5", languageId);

            ViewBag.Body = model;

            return View();
        }

        public ActionResult FirstPage(string sectionName = "")
        {

            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];

            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();

            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var model = _topicRepository.GetResourceBody(sectionName, languageId);

            ViewBag.Body = model;

            return View();
        }

        public FileResult Download(string type)
        {
            string languageID = "";
            languageID = CurrentTime.getLanguageID();
            string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
            string file = "";

            if (type == "AGOP")
            {
                switch (languageID)
                {
                    case "2":
                        file = pathAttachment + _topicRepository.GetDownloadLinkOPAgreement(Int32.Parse(languageID));
                        break;

                    case "1":
                    default:
                        file = pathAttachment + _topicRepository.GetDownloadLinkOPAgreement(Int32.Parse(languageID));
                        break;

                }
            }
            else if(type == "AG")
            {
                switch (languageID)
                {
                    case "2":
                        file = pathAttachment + _topicRepository.GetDownloadLinkAgreement(Int32.Parse(languageID));
                        break;

                    case "1":
                    default:
                        file = pathAttachment + _topicRepository.GetDownloadLinkAgreement(Int32.Parse(languageID));
                        break;

                }
            }

            byte[] fileBytes = System.IO.File.ReadAllBytes(@file);
            string virtualFilePath = @file;
            string fileName2 = Path.GetFileName(virtualFilePath);

            //var cd = new System.Net.Mime.ContentDisposition
            //{
            //    // for example foo.bak
            //    FileName = fileName2,

            //    // always prompt the user for downloading, set to true if you want 
            //    // the browser to try to show the file inline
            //    Inline = false,
            //};
            //Response.AppendHeader("Content-Disposition", cd.ToString());
            //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);
        }

        public ActionResult News(int newsId = 0)
        {
            int languageId = GetCurrentLanguageHelper.GetIntLanguageIDCurrent();

            var model = _newsDisplayRepository.GetNewsByNewsId(newsId, languageId);

            ViewBag.Body = model.NewsDetail;

            return View();
        }

        public FileResult DownloadNews(string filePath)
        {
            string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
            filePath = pathAttachment + filePath;

            if (!System.IO.File.Exists(@filePath))
            {
                filePath = @pathAttachment + @"\SWWDocuments\UserManualForDownload\EPInstruction.pdf";
            }


            byte[] fileBytes = System.IO.File.ReadAllBytes(@filePath);
            string virtualFilePath = @filePath;
            string fileName2 = Path.GetFileName(virtualFilePath);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName2);
        }

        public ActionResult Alive()
        {
            var computername = System.Environment.MachineName;

            ViewBag.Computername = computername;

            return View();
        }
    }
}