﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Models.Repository.BusinessEntity;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.UserSession;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.TrackingOrgAttachment;
using SupplierPortal.Data.Models.Repository.TrackingComprofile;
using SupplierPortal.Data.Models.Repository.TrackingRequest;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Services.CompanyProfileManage;
using SupplierPortal.Framework.MethodHelper;
using System.Web.Configuration;
using SupplierPortal.Core.Extension;
using SupplierPortal.Data.Models.SupportModel;

namespace SupplierPortal.Controllers
{
    public class CompanyProfileController : ControllerBase
    {

        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        IBusinessEntityRepository _businessEntityRepository;
        IUserRepository _userRepository;
        ITrackingOrgAttachmentRepository _trackingOrgAttachmentRepository;
        IComProfileTrackingService _comProfileTrackingService;
        ITrackingComprofileRepository _trackingComprofileRepository;
        ITrackingRequestRepository _trackingRequestRepository;
        IOrganizationRepository _organizationRepository;

        public CompanyProfileController()
        {
            _businessEntityRepository = new BusinessEntityRepository();
            _userRepository = new UserRepository();
            _trackingOrgAttachmentRepository = new TrackingOrgAttachmentRepository();
            _comProfileTrackingService = new ComProfileTrackingService();
            _trackingComprofileRepository = new TrackingComprofileRepository();
            _trackingRequestRepository = new TrackingRequestRepository();
            _organizationRepository = new OrganizationRepository();

        }

        public CompanyProfileController(
             BusinessEntityRepository businessEntityRepository,
            UserRepository userRepository,
            TrackingOrgAttachmentRepository trackingOrgAttachmentRepository,
            ComProfileTrackingService comProfileTrackingService,
            TrackingComprofileRepository trackingComprofileRepository,
            TrackingRequestRepository trackingRequestRepository,
            OrganizationRepository organizationRepository
             )
        {
            _businessEntityRepository = businessEntityRepository;
            _userRepository = userRepository;
            _trackingOrgAttachmentRepository = trackingOrgAttachmentRepository;
            _comProfileTrackingService = comProfileTrackingService;
            _trackingComprofileRepository = trackingComprofileRepository;
            _trackingRequestRepository = trackingRequestRepository;
            _organizationRepository = organizationRepository;

        }

        // GET: CompanyProfile
        public ActionResult Index()
        {
            IUserSessionRepository _repoUsersession;
            IUserRepository _repoUser;
            _repoUsersession = new UserSessionRepository();
            _repoUser = new UserRepository();

            if (Session["GUID"] == null)
            {
                //Response.Redirect("~/AccountPortal/Logout");
                return RedirectToAction("Index", "AccountPortal");
            }
            else
            {
                string guid = Session["GUID"].ToString();
                var listUser = _repoUsersession.GetUserSession(guid);
                if (listUser.isTimeout == 0)
                {
                    var ChkisAcceptTermOfUse = _repoUser.FindByUsername(listUser.Username);
                    string uesTermWeomni = WebConfigurationManager.AppSettings["switch_use_term_weomni"];
                    if (ChkisAcceptTermOfUse.isAcceptTermOfUse == 0 && !Convert.ToBoolean(uesTermWeomni))
                    {
                        //Response.Redirect("~/Agreement/Index");
                        return RedirectToAction("Index", "Agreement");
                    }
                }
            }

            return View();
        }

        //public ActionResult BusinessEntityPartial([ModelBinder(typeof(DevExpressEditorsBinder))] GeneralModels model)
        //{
        //    HttpCookie cultureCookie = Request.Cookies["_culture"];

        //    GeneralModels generalModels = new GeneralModels();

        //    model = generalModels;

        //    string languageID = "";

        //    if (cultureCookie != null)
        //    {
        //        languageID = cultureCookie.Value.ToString();

        //    }
        //    if (string.IsNullOrEmpty(languageID))
        //    {
        //        languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
        //    }
        //    int languageId = Convert.ToInt32(languageID);

        //    var corperate = Request.Params["CorperateValue"] as string ?? string.Empty;

        //    if (!string.IsNullOrEmpty(corperate))
        //    {
        //        var isCorperate = corperate == "0" ? true : false;
        //        model.IsCorperate = isCorperate;
        //    }
        //    else
        //    {
        //        model.IsCorperate = true;
        //    }

        //    //string languageId = "";
        //    //HttpCookie cultureCookie = Request.Cookies["_culture"];
        //    //languageId = cultureCookie.Value;

        //    //IBusinessEntityRepository _repo = new BusinessEntityRepository();
        //    ViewData["BusinessEntityList"] = _businessEntityRepository.GetBusinessEntityByMany(model.IsCorperate, "Tbl_BusinessEntity", languageId);
        //    _businessEntityRepository.Dispose();

        //    return PartialView("_BusinessEntityPartial", model);
        //}

        public ActionResult TrackingComProfile()
        {
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            var user = _userRepository.FindByUsername(username);

            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            CurrentTime.SetCurrentCultureByLanguageID(languageID);

            ViewBag.SupplierID = user.SupplierID ?? 0;

            return View();
        }

        public ActionResult GridViewPart_TrackingComProfile()
        {

            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            var user = _userRepository.FindByUsername(username);

            ViewBag.SupplierID = user.SupplierID ?? 0;

            return PartialView(@"~/Views/CompanyProfile/_GridViewPartial_TrackingComProfile.cshtml");
        }
        public ActionResult GridViewPart_TrackingComProfileDT(DTParameters param, string status)
        {
            try
            {
                #region Find Supplier ID

                // Get username from session
                var username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }

                // Find supplier id
                int supplierId = 0;
                var user = _userRepository.FindByUsername(username);
                if (user != null)
                    supplierId = user.SupplierID.GetValueOrDefault(0);

                #endregion

                #region Filter, Order by, Paging

                // Get all parameters
                var search = param.Search.Value;
                var sortColumn = param.Order[0].Column;
                var sortDirection = param.Order[0].Dir;

                // All rows
                var all = ComProfileTrackingManage.GetTrackingRequestAllBySupplierID(supplierId);

                // Filtered
                var filtered = all;
                if (!string.IsNullOrEmpty(search))
                {
                    //filtered = all.Where(x => x.TrackingGrpName.Contains(search) ||
                    //                          x.TrackingStatusName.Contains(search) ||
                    //                          x.ReqDate.Contains(search) ||
                    //                          x.ReqByFullname.Contains(search) ||
                    //                          x.TrackingReqID.Contains(search));

                    var temp = all.ToList().Where(x => x.TrackingGrpName.Contains(search) ||
                                                       x.TrackingStatusName.Contains(search) ||
                                                       x.ReqByFullname.Contains(search)).ToList();

                    filtered = temp.AsQueryable();
                }

                // Custom filter
                if (!string.IsNullOrEmpty(status))
                {
                    var temp = all.ToList().Where(x => x.TrackingStatusName.Contains(status)).ToList();

                    filtered = temp.AsQueryable();
                }

                // Sort order
                var sort = filtered;
                switch (sortColumn)
                {
                    //case 0:
                    //    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderBy(x => x.TrackingGrpName) : filtered.OrderBy(x => x.TrackingGrpName);
                    //    break;
                    case 1:
                        sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.TrackingStatusName) : filtered.OrderBy(x => x.TrackingStatusName);
                        break;
                    case 2:
                        sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.ReqDate) : filtered.OrderBy(x => x.ReqDate);
                        break;
                    case 3:
                        sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.ReqByFullname) : filtered.OrderBy(x => x.ReqByFullname);
                        break;
                    case 4:
                        sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.TrackingReqID) : filtered.OrderBy(x => x.TrackingReqID);
                        break;
                    default:
                        sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.TrackingGrpName) : filtered.OrderBy(x => x.TrackingGrpName);
                        break;
                }

                // Paging
                var result = sort.Skip(param.Start).Take(param.Length);

                #endregion

                #region Return to Client JSON Format

                return Json(new
                {
                    draw = param.Draw,
                    recordsTotal = all.Count(),
                    recordsFiltered = filtered.Count(),
                    data = result,
                    error = ""
                }, JsonRequestBehavior.AllowGet);

                #endregion
            }
            catch (Exception ex)
            {
                logger.Error(ex);

                throw;
            }
        }

        public ActionResult ViewTrackingCompanyProfile(int trackingReqID, int trackingGrpID, string trackingPage = "", string trackingController = "")
        {
            string trackingType = "ByRequest";
            string actionName = "";
            string reqIDEncode = CryptoExtension.EncodeRouteData(trackingReqID.ToString());
            switch (trackingGrpID)
            {
                case 1:
                    actionName = @"~/Views/CompanyProfile/ViewTrackingCompanyInfo.cshtml";
                    break;

                case 2:
                    actionName = @"~/Views/CompanyProfile/ViewTrackingCompanyAffidavit.cshtml";
                    break;

                case 3:
                    actionName = @"~/Views/CompanyProfile/ViewTrackingContactAddress.cshtml";
                    break;

                default:
                    actionName = @"~/Views/CompanyProfile/TrackingComProfile.cshtml";
                    break;
            }

            return RedirectToAction(actionName, "CompanyProfile", new { reqID = reqIDEncode, trackingType = trackingType, trackingPage = trackingPage, trackingController = trackingController });
        }

        public ActionResult ViewTrackingFieldCompanyProfile(int trackingItemIDField, int trackingGrpID, string trackingPage = "", string trackingController = "", string trackingItemIDAddress = "")
        {
            string trackingType = "ByField";
            string actionName = "";
            string reqIDEncode = CryptoExtension.EncodeRouteData(trackingItemIDField.ToString());
            string reqAddress = trackingItemIDAddress.ToString();
            switch (trackingGrpID)
            {
                case 1:
                    actionName = @"~/Views/CompanyProfile/ViewTrackingCompanyInfo.cshtml";
                    break;

                case 2:
                    actionName = @"~/Views/CompanyProfile/ViewTrackingCompanyAffidavit.cshtml";
                    break;

                case 3:
                    actionName = @"~/Views/CompanyProfile/ViewTrackingContactAddress.cshtml";
                    break;

                default:
                    actionName = @"~/Views/CompanyProfile/TrackingComProfile.cshtml";
                    break;
            }

            return RedirectToAction(actionName, "CompanyProfile", new { reqID = reqIDEncode, reqAddress = reqAddress, trackingType = trackingType, trackingPage = trackingPage, trackingController = trackingController });
        }

        public ActionResult ViewTrackingCompanyInfo(string reqID, string trackingType, string trackingPage, string trackingController)
        {
            try
            {
                string reqIDDecode = CryptoExtension.DecodeRouteData(reqID);

                int trackingID;
                bool isNumeric = int.TryParse(reqIDDecode, out trackingID);
                if (isNumeric)
                {
                    ViewBag.TrackingPage = trackingPage;
                    ViewBag.TrackingController = trackingController;
                    if (trackingType == "ByRequest")
                    {
                        var model = _comProfileTrackingService.GetTrackingFieldCompanyProfileByTrackingReqID(trackingID).Where(m => m.TrackingStatusID == 2);
                        return View(model);
                    }
                    else
                    {
                        var model = _comProfileTrackingService.GetTrackingFieldCompanyProfileByTrackingItemID(trackingID).Where(m => m.TrackingStatusID == 2);
                        return View(model);
                    }

                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());

        }

        public ActionResult ViewTrackingCompanyAffidavit(string reqID, string trackingType, string trackingPage, string trackingController)
        {
            try
            {
                string reqIDDecode = CryptoExtension.DecodeRouteData(reqID);

                int trackingID;
                bool isNumeric = int.TryParse(reqIDDecode, out trackingID);
                if (isNumeric)
                {


                    ViewBag.TrackingPage = trackingPage;
                    ViewBag.TrackingController = trackingController;
                    if (trackingType == "ByRequest")
                    {
                        var model = _trackingOrgAttachmentRepository.GetViewTrackingCompanyAffidavitByTrackingReqID(trackingID);
                        return View(model);
                    }
                    else
                    {
                        var model = _trackingOrgAttachmentRepository.GetViewTrackingCompanyAffidavitByTrackingItemID(trackingID);
                        return View(model);
                    }

                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        public ActionResult ViewTrackingContactAddress(string reqID, string trackingType, string trackingPage, string trackingController, string reqAddress = "")
        {

            try
            {
                string reqIDDecode = CryptoExtension.DecodeRouteData(reqID);
                string orgCountryCode = "";
                int trackingID;
                bool isNumeric = int.TryParse(reqIDDecode, out trackingID);
                if (isNumeric)
                {
                    var trackingRequestModel = _trackingRequestRepository.GetTrackingRequestByTrackingReqID(trackingID);
                    if (trackingRequestModel != null)
                    {
                        int supplierID = trackingRequestModel.SupplierID ?? 0;
                        var orgModel = _organizationRepository.GetDataBySupplierID(supplierID);
                        if (orgModel != null)
                        {
                            orgCountryCode = orgModel.CountryCode;
                        }
                    }
                    ViewBag.CountryCode = orgCountryCode;
                    ViewBag.TrackingPage = trackingPage;
                    ViewBag.TrackingController = trackingController;
                    if (trackingType == "ByRequest")
                    {
                        var model = _comProfileTrackingService.GetOrgAddressTrackingViewDataByTrackingReqID(trackingID);
                        return View(model);
                    }
                    else
                    {
                        int trackingItemIDAddress = 0;
                        if (!string.IsNullOrEmpty(reqAddress))
                        {
                            trackingItemIDAddress = Convert.ToInt32(reqAddress);
                        }
                        var model = _comProfileTrackingService.GetOrgAddressTrackingByTrackingItemID(trackingItemIDAddress, trackingID);
                        return View(model);
                    }


                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        public ActionResult Download(string filePath, string fileName = "")
        {
            string pathAttachment = WebConfigurationManager.AppSettings["pathAttachment"];
            filePath = pathAttachment + filePath;

            if (!System.IO.File.Exists(@filePath))
            {
                filePath = @pathAttachment + @"\SWWDocuments\UserManualForDownload\EPInstruction.pdf";
            }
            byte[] fileBytes = System.IO.File.ReadAllBytes(@filePath);

            if (string.IsNullOrEmpty(fileName))
            {
                fileName = "download";
            }

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}
