﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.MenuPortal;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using SupplierPortal.Data.Models.Repository.ACL_OrgMenuMapping;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.SystemNotification;
using SupplierPortal.Services.MenuPortalManage;
using SupplierPortal.Data.Models.SupportModel.MenuPortal;
using SupplierPortal.Data.Models.Repository.MenuPortalComProfile;
using SupplierPortal.Data.CustomModels.AccountPortal;
using Newtonsoft.Json;
using System.IO;
using SupplierPortal.Core.Caching;
using System.Web.Configuration;
using SupplierPortal.MainFunction.Crypto;
using System.Net.Http;
using System.Web.Routing;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Framework.Localization;

namespace SupplierPortal.Controllers
{
    public class MenuPortalController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string WebWork_UserSystemMapping = "WebWork.UserSystemMapping{0}";
        private const string WebWork_System = "WebWork.System{0}";
        private const string WebWork_Menu = "WebWork.Menu{0}";
        private const string WebWork_MenuMappingEnable = "WebWork.MenuMappingEnable{0}";
        private const string WebWork_Supplier = "WebWork.Supplier{0}";
        private const string WebWork_OrgMenuSupplier = "WebWork.WebWork_OrgMenuSupplier{0}";
        private const string WebWork_SubMenu = "WebWork.WebWork_SubMenu{0}";
        private const string WebWork_TopMenu = "WebWork.WebWork_TopMenu";
        private const string WebWork_MenuProfile = "WebWork.WebWork_MenuProfile";
        private const string WebWork_CompanyProfile = "WebWork.WebWork_CompanyProfile";

        private readonly ICacheManager _cacheManager;

        IMenuPortalRepository _menuPortalRepository;
        IUserSystemMappingRepository _userSystemMappingRepository;
        IACL_OrgMenuMappingRepository _acl_OrgMenuMappingRepository;
        IOrganizationRepository _organizationRepository;
        IMenuPortalService _menuPortalService;
        ISystemNotificationRepository _systemNotificationRepository;
        IMenuPortalComProfileRepository _menuPortalComProfileRepository;
        IUserRepository _userRepository;
        IContactPersonRepository _contactPersonRepository;

        public MenuPortalController()
        {
            _menuPortalRepository = new MenuPortalRepository();
            _userSystemMappingRepository = new UserSystemMappingRepository();
            _acl_OrgMenuMappingRepository = new ACL_OrgMenuMappingRepository();
            _organizationRepository = new OrganizationRepository();
            _menuPortalService = new MenuPortalService();
            _systemNotificationRepository = new SystemNotificationRepository();
            _menuPortalComProfileRepository = new MenuPortalComProfileRepository();
            _cacheManager = new MemoryCacheManager();
            _userRepository = new UserRepository();
            _contactPersonRepository = new ContactPersonRepository();
        }

        public MenuPortalController(
            MenuPortalRepository menuPortalRepository,
            UserSystemMappingRepository userSystemMappingRepository,
            ACL_OrgMenuMappingRepository acl_OrgMenuMappingRepository,
            OrganizationRepository organizationRepository,
            MenuPortalService menuPortalService,
            SystemNotificationRepository systemNotificationRepository,
            MenuPortalComProfileRepository menuPortalComProfileRepository,
            UserRepository userRepository,
            ContactPersonRepository contactPersonRepository
            )
        {
            _menuPortalRepository = menuPortalRepository;
            _userSystemMappingRepository = userSystemMappingRepository;
            _acl_OrgMenuMappingRepository = acl_OrgMenuMappingRepository;
            _organizationRepository = organizationRepository;
            _menuPortalService = menuPortalService;
            _systemNotificationRepository = systemNotificationRepository;
            _menuPortalComProfileRepository = menuPortalComProfileRepository;
            _userRepository = userRepository;
            _contactPersonRepository = contactPersonRepository;
        }

        // GET: MenuPortal
        public ActionResult Index()
        {
            return View();
        }

        public bool MenuMapping(int systemGrpID)
        {
            string key = string.Empty;
            try
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("~/Error/Timeout");
                }

                string username = Session["username"].ToString();

                key = string.Format(WebWork_System, username);
                var cacheSystemModel = _cacheManager.Get(key, () =>
                {
                    var result = _systemNotificationRepository.GetSystemIsActive().ToList();

                    return result;
                });

                var systemModel = cacheSystemModel.Where(w => w.SystemGrpID == systemGrpID);

                //var systemModel = _systemNotificationRepository.GetSystemIsActive().Where(w => w.SystemGrpID == systemGrpID);

                if (systemModel != null)
                {
                    foreach (var item in systemModel)
                    {
                        key = string.Format(WebWork_UserSystemMapping, username);
                        var cacheCheckUserMapping = _cacheManager.Get(key, () =>
                        {
                            var result = _userSystemMappingRepository.GetUserSystemMappingNonCheckMergByUserName(username).ToList();

                            return result;
                        });
                        var checkUserMapping = cacheCheckUserMapping.Where(w => w.SystemID == item.SystemID);

                        if (systemGrpID == 3 && checkUserMapping.Count() == 0)
                        {
                            return true;
                        }

                        if (checkUserMapping.Count() > 0)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }

            return false;
        }

        public bool CheckMenuPortalPrivilegeMapping(int menuID, int privilegeTypeID)
        {
            bool chkPrivilege = true;
            string key = string.Empty;
            try
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("~/Error/Timeout");
                }

                string username = Session["username"].ToString();

                if (privilegeTypeID == 0)
                {
                    chkPrivilege = true;
                }
                else if (privilegeTypeID == 1)
                {
                    chkPrivilege = false;

                    //var menuModel = _menuPortalRepository.GetMenuById(menuID);
                    key = string.Format(WebWork_Menu, username);
                    var cacheMenuModel = _cacheManager.Get(key, () =>
                    {
                        var result = _menuPortalRepository.GetMenu().ToList();

                        return result;
                    });
                    var menuModel = cacheMenuModel.Where(w => w.MenuID == menuID).FirstOrDefault();

                    if (menuModel != null)
                    {
                        //int supplierID = _organizationRepository.GetSupplierIDByUsername(username);
                        key = string.Format(WebWork_Supplier, username);
                        int supplierID = _cacheManager.Get(key, () =>
                        {
                            var result = _organizationRepository.GetSupplierIDByUsername(username);

                            return result;
                        });

                        var model = _acl_OrgMenuMappingRepository.GetOrgMenuMappingBySupplierIDAndMenuID(supplierID, menuID);
                        //key = string.Format(WebWork_OrgMenuSupplier, username);
                        //var model = _cacheManager.Get(key, () =>
                        //{
                        //    var result = _acl_OrgMenuMappingRepository.GetOrgMenuMappingBySupplierID(supplierID).Where(w => w.MenuID == menuID).FirstOrDefault();

                        //    return result;
                        //});

                        if (model != null)
                        {
                            if (model.isAllowAccess == 1)
                            {
                                chkPrivilege = true;
                            }
                        }
                    }
                }
                else if (privilegeTypeID == 2)
                {
                    bool chkAdminBestSupply = SupplierPortal.Services.ProfileManage.ProfileManage.CheckIsAdminBestSupply(username);

                    if (chkAdminBestSupply)
                    {
                        chkPrivilege = true;
                    }
                    else
                    {
                        chkPrivilege = false;
                    }
                }
                else if (privilegeTypeID == 3)
                {
                    chkPrivilege = true;
                }
            }
            catch (Exception ex)
            {
                RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }

            return chkPrivilege;
        }

        public int MenuMappingEnable(int systemGrpID)
        {
            int isEnable = 0;
            string key = string.Empty;
            try
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("~/Error/Timeout");
                }

                string username = Session["username"].ToString();

                //var systemModel = _systemNotificationRepository.GetSystemBySystemGrpID(systemGrpID);
                key = string.Format(WebWork_System, username);
                var cacheSystemModel = _cacheManager.Get(key, () =>
                {
                    var result = _systemNotificationRepository.GetSystemIsActive().ToList();

                    return result;
                });
                var systemModel = cacheSystemModel.Where(w => w.SystemGrpID == systemGrpID);

                if (systemModel != null)
                {
                    foreach (var item in systemModel)
                    {
                        //var checkUserMapping = _userSystemMappingRepository.GetUserSystemMappingMenu(username, item.SystemID).FirstOrDefault();
                        key = string.Format(WebWork_MenuMappingEnable, username);
                        var cacheCheckUserMapping = _cacheManager.Get(key, () =>
                        {
                            var result = _userSystemMappingRepository.GetUserSystemMappingMenuByUserName(username).ToList();

                            return result;
                        });
                        var checkUserMapping = cacheCheckUserMapping.Where(w => w.SystemID == item.SystemID).FirstOrDefault();

                        if (systemGrpID == 3 && checkUserMapping == null)
                        {
                            return 1;
                        }

                        if (checkUserMapping != null)
                        {
                            // Fix check for ไปหน้า  Topic/ServiceCancelled ( Fix เป็น 3 ไปเชคหน้า view submenu ต่อ)

                            if ((checkUserMapping.isCancelService ?? 0) == 1)
                            {
                                return 3;
                            }

                            // Fix check for ไปหน้า  Topic/ServiceSuspend ( Fix เป็น 2 ไปเชคหน้า view submenu ต่อ)

                            if ((checkUserMapping.isSuspendService ?? 0) == 1)
                            {
                                return 2;
                            }
                            isEnable = checkUserMapping.isAllowConnecting ?? 0;
                            if (isEnable == 1)
                            {
                                return isEnable;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }

            return isEnable;
        }

        public ActionResult ServiceUnavailable()
        {

            return View();
        }

        public ActionResult ServiceUnavailable_4NonAdmin()
        {

            return View();
        }

        public ActionResult SystemUserDisabled()
        {


            return View();
        }

        public ActionResult TopMenuPortal()
        {
            //var result = _menuPortalRepository.GetMenuTop();
            string key = string.Empty;
            key = string.Format(WebWork_TopMenu);
            var cacheTopMenu = _cacheManager.Get(key, () =>
            {
                var topmenu = _menuPortalRepository.GetMenuTop();

                return topmenu;
            });
            var result = cacheTopMenu;

            return PartialView(@"~/Views/MenuPortal/_PartialTopMenuPortal.cshtml", result);
        }

        public ActionResult SubMenuPortal()
        {
            try
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("~/Error/Timeout");
                }

                string username = Session["username"].ToString();

                string key = string.Empty;
                key = string.Format(WebWork_SubMenu, username);
                var cacheSubMenuModel = _cacheManager.Get(key, () =>
                {
                    var result = _menuPortalService.GetSubMenuPortalNonNotification();

                    return result;
                });

                var model = cacheSubMenuModel;

                //var model = _menuPortalService.GetSubMenuPortalNonNotification();


                return PartialView(@"~/Views/MenuPortal/_PartialSubMenuPortalNew.cshtml", model);
            }
            catch (Exception ex)
            {
                //return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }

            //return PartialView("_PartialSubMenuPortal", model);
        }

        public ActionResult SubMenuPortalScript()
        {

            return PartialView("_PartialScript");
        }

        public ActionResult TopMenuAgreementPortal()
        {
            if (Session["username"] == null)
            {
                Response.Redirect("~/Error/Timeout");
            }
            var menuModel = _menuPortalRepository.GetMenuById(6);

            return PartialView(@"~/Views/MenuPortal/_PartialTopMenuAgreementPortal.cshtml", menuModel);
        }

        public ActionResult ProfileMenuPortal(string section = "")
        {
            ViewBag.Section = section;

            string key = string.Empty;
            key = string.Format(WebWork_MenuProfile);
            var cacheMenuProfile = _cacheManager.Get(key, () =>
            {
                var result = _menuPortalRepository.GetMenuProfile();

                return result;
            });
            var model = cacheMenuProfile;
            //var model = _menuPortalRepository.GetMenuProfile();


            key = string.Format(WebWork_CompanyProfile);
            var cacheCompanyProfile = _cacheManager.Get(key, () =>
            {
                var result = _menuPortalComProfileRepository.GetMenuCompanyProfile();

                return result;
            });
            ViewBag.menuCompro = cacheCompanyProfile;
            //ViewBag.menuCompro = _menuPortalComProfileRepository.GetMenuCompanyProfile();

            return PartialView(@"~/Views/MenuPortal/_PartialProfileMenuPortal.cshtml", model);
        }

        public ActionResult TopMenuEditDataTracking()
        {

            return PartialView(@"~/Views/MenuPortal/_PartialTopMenuEditDataTracking.cshtml");
        }

        [HttpPost]
        public ActionResult GetNotificationByMenuID(int menuID)
        {
            int notificationValue = 0;
            string username = Session["username"].ToString();

            try
            {
                notificationValue = _menuPortalService.GetNotificationByUserNameAndMenuID(username, menuID);

                return Json(new
                {
                    isGetNotification = true,
                    notificationValue = notificationValue
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    isGetNotification = false
                });
            }

        }

        public ActionResult TopMenu()
        {
            try
            {
                string jsonFormat = "";
                //jsonFormat = "{\"APIName\":\"GetHeader\",\"APIKey\":\"Pantavanij\",\"ReqID\":\"4F1D487B7389452BADA397CF1BE235E7\",\"SID\":\"b796dce198b62f23cd131fe01ff19617|66\",\"HeaderType\":\"EP\",\"HeaderInfo\":{\"CustomerLogoURL\":\"http://trueop.demo.pantavanij.com/ebd/EBOImages/EBO66.gif\",\"TopMenu\":[{\"DisplayName\":\"Home\",\"URL\":\"http://ep.demo.pantavanij.com/true/PortalHome.action?sid=b796dce198b62f23cd131fe01ff19617|66&eid=66\"},{\"DisplayName\":\"Profile\",\"URL\":\"http://ep.demo.pantavanij.com/true/PortalRequest.action?sid=b796dce198b62f23cd131fe01ff19617|66&eid=66&page=all_profile\",\"PopupURL\":\"xxxx\"},{\"DisplayName\":\"Manual\",\"DisplayContent\":[{\"topic\":\"EP PANTAVANIJ e-Procurement\",\"content_list\":[{\"content\":\"\u0E40\u0E21\u0E19\u0E39\u0E01\u0E32\u0E23\u0E43\u0E0A\u0E49\u0E07\u0E32\u0E19\u0E1A\u0E19\u0E23\u0E30\u0E1A\u0E1A EP (Instruction)\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualEP.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"}]},{\"topic\":\"SOURCING PROCESS\",\"content_list\":[{\"content\":\"\u0E27\u0E34\u0E18\u0E35\u0E01\u0E32\u0E23\u0E2A\u0E23\u0E49\u0E32\u0E07\u0E43\u0E1A\u0E02\u0E2D\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25\u0E2A\u0E34\u0E19\u0E04\u0E49\u0E32 (Create New eRFX)\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualCreateNeweRFX.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"},{\"content\":\"\u0E27\u0E34\u0E18\u0E35\u0E01\u0E32\u0E23\u0E2D\u0E19\u0E38\u0E21\u0E31\u0E15\u0E34\u0E43\u0E1A\u0E02\u0E2D\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25\u0E2A\u0E34\u0E19\u0E04\u0E49\u0E32 (Approve eRFX)\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualApproveeRFX.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"},{\"content\":\"\u0E27\u0E34\u0E18\u0E35\u0E01\u0E32\u0E23\u0E02\u0E2D\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25\u0E2A\u0E34\u0E19\u0E04\u0E49\u0E32\u0E08\u0E32\u0E01\u0E1C\u0E39\u0E49\u0E02\u0E32\u0E22\u0E09\u0E1A\u0E31\u0E1A\u0E22\u0E48\u0E2D (eRFX send to supplier)\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualeRFXShort.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"},{\"content\":\"\u0E27\u0E34\u0E18\u0E35\u0E01\u0E32\u0E23\u0E02\u0E2D\u0E02\u0E49\u0E2D\u0E21\u0E39\u0E25\u0E2A\u0E34\u0E19\u0E04\u0E49\u0E32\u0E08\u0E32\u0E01\u0E1C\u0E39\u0E49\u0E02\u0E32\u0E22\u0E09\u0E1A\u0E31\u0E1A\u0E40\u0E15\u0E47\u0E21 (eRFX send to supplier full version)\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualeRFX.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"}]},{\"topic\":\"PURCHASING PROCESS\",\"content_list\":[{\"content\":\"\u0E27\u0E34\u0E18\u0E35\u0E01\u0E32\u0E23\u0E40\u0E1B\u0E34\u0E14\u0E43\u0E1A\u0E02\u0E2D\u0E0B\u0E37\u0E49\u0E2D\u0E43\u0E2B\u0E21\u0E48 (Create New PR)\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualCreateNewPR.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"},{\"content\":\"\u0E27\u0E34\u0E18\u0E35\u0E01\u0E32\u0E23\u0E40\u0E1B\u0E34\u0E14\u0E43\u0E1A\u0E02\u0E2D\u0E0B\u0E37\u0E49\u0E2D\u0E43\u0E2B\u0E21\u0E48 (Create New PR) \u0E1B\u0E23\u0E30\u0E40\u0E20\u0E17\u0E01\u0E32\u0E23\u0E2A\u0E31\u0E48\u0E07\u0E0B\u0E37\u0E49\u0E2DTravellingExpense\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualCreateNewTRV.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"},{\"content\":\"\u0E27\u0E34\u0E18\u0E35\u0E01\u0E32\u0E23\u0E2D\u0E19\u0E38\u0E21\u0E31\u0E15\u0E34\u0E43\u0E1A\u0E02\u0E2D\u0E0B\u0E37\u0E49\u0E2D (Approve PR)\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualApprovePR.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"},{\"content\":\"\u0E27\u0E34\u0E18\u0E35\u0E01\u0E32\u0E23\u0E40\u0E1E\u0E34\u0E48\u0E21\u0E1C\u0E39\u0E49\u0E2D\u0E19\u0E38\u0E21\u0E31\u0E15\u0E34 (Add Approver on Workflow)\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualAddApprover.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"},{\"content\":\"\u0E27\u0E34\u0E18\u0E35\u0E01\u0E32\u0E23\u0E17\u0E33\u0E23\u0E31\u0E1A\u0E2A\u0E34\u0E19\u0E04\u0E49\u0E32 (Goods Receipt)\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualGR.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"},{\"content\":\"\u0E27\u0E34\u0E18\u0E35\u0E01\u0E32\u0E23\u0E2D\u0E19\u0E38\u0E21\u0E31\u0E15\u0E34\u0E01\u0E32\u0E23\u0E23\u0E31\u0E1A\u0E2A\u0E34\u0E19\u0E04\u0E49\u0E32 (Approve GR)\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualApproveGR.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"}]},{\"topic\":\"PROCUREMENT TOOLS\",\"content_list\":[{\"content\":\"\u0E27\u0E34\u0E18\u0E35\u0E01\u0E32\u0E23\u0E02\u0E2D\u0E2A\u0E23\u0E49\u0E32\u0E07\u0E2A\u0E31\u0E0D\u0E0D\u0E32\u0E43\u0E2B\u0E21\u0E48 \u0E41\u0E25\u0E30\u0E15\u0E23\u0E27\u0E08\u0E23\u0E31\u0E1A\u0E2B\u0E19\u0E31\u0E07\u0E2A\u0E37\u0E2D\u0E04\u0E49\u0E33\u0E1B\u0E23\u0E30\u0E01\u0E31\u0E19 (Contract Bond)\",\"display_url\":\"http://ep.demo.pantavanij.com/true/manual/ManualContractManagement.pdf?sid=b796dce198b62f23cd131fe01ff19617|66\"}]}]},{\"DisplayName\":\"Contact\",\"DisplayContent\":[{\"topic\":\"Contact Customer Care\",\"content_list\":[{\"content\":\"Operation Hours: Monday - Friday 8:30-18:00Customer Care Hotline: 0-2689-4333 Fax: 0-2679-7474Email: customercare@pantavanij.com<\\/a>\"}]},{\"topic\":\"\u0E2A\u0E48\u0E27\u0E19\u0E1A\u0E23\u0E34\u0E01\u0E32\u0E23\u0E43\u0E2B\u0E49\u0E04\u0E27\u0E32\u0E21\u0E0A\u0E48\u0E27\u0E22\u0E40\u0E2B\u0E25\u0E37\u0E2D\u0E01\u0E32\u0E23\u0E43\u0E0A\u0E49\u0E07\u0E32\u0E19\u0E23\u0E30\u0E1A\u0E1A\u0E17\u0E32\u0E07\u0E42\u0E17\u0E23\u0E28\u0E31\u0E1E\u0E17\u0E4C (Customer Care)\",\"content_list\":[{\"content\":\"\u0E40\u0E27\u0E25\u0E32\u0E17\u0E33\u0E01\u0E32\u0E23\u0E02\u0E2D\u0E07\u0E40\u0E08\u0E49\u0E32\u0E2B\u0E19\u0E49\u0E32\u0E17\u0E35\u0E48 \u0E27\u0E31\u0E19\u0E08\u0E31\u0E19\u0E17\u0E23\u0E4C - \u0E27\u0E31\u0E19\u0E28\u0E38\u0E01\u0E23\u0E4C \u0E40\u0E27\u0E25\u0E32 8:30 \u0E19. - 18:00 \u0E19.\u0E42\u0E17\u0E23\u0E28\u0E31\u0E1E\u0E17\u0E4C 0-2689-4333 Fax: 0-2679-7474\u0E2D\u0E35\u0E40\u0E21\u0E25\u0E4C: customercare@pantavanij.com<\\/a>\"}]}]},{\"DisplayName\":\"Logout\",\"URL\":\"http://ep.demo.pantavanij.com/true/Logout.action?sid=b796dce198b62f23cd131fe01ff19617|66&eid=66\"}]},\"result\":\"SUCCESS\"}";

                jsonFormat = Session["JSON_Data"] as string;
                if (!string.IsNullOrEmpty(jsonFormat))
                {
                    API_EPHeaderReponseModel headerModel = JsonConvert.DeserializeObject<API_EPHeaderReponseModel>(jsonFormat);
                    if (headerModel != null && headerModel.HeaderType == "EP")
                    {
                        return PartialView(@"~/Views/MenuPortal/_PartialTopMenu.cshtml", headerModel);
                    }
                    else if (headerModel != null && headerModel.HeaderType == "OP")
                    {
                        return PartialView("_PartialTopMenuOP", headerModel);
                    }
                    else if (headerModel != null && headerModel.HeaderType == "OnePlanet")
                    {
                        return PartialView("_PartialTopMenuOnePlanet", headerModel);
                    }
                    else
                    {
                        return new EmptyResult();
                    }
                }
                else
                {
                    // Test
                    using (StreamReader r = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + @"App_Data\EP_JWT.json"))
                    {
                        jsonFormat = r.ReadToEnd();
                    }

                    API_EPHeaderReponseModel headerModel = JsonConvert.DeserializeObject<API_EPHeaderReponseModel>(jsonFormat);
                    if (headerModel != null && headerModel.HeaderType == "EP")
                    {
                        return PartialView(@"~/Views/MenuPortal/_PartialTopMenu.cshtml", headerModel);
                    }
                    else if (headerModel != null && headerModel.HeaderType == "OP")
                    {
                        return PartialView("_PartialTopMenuOP", headerModel);
                    }
                    else if (headerModel != null && headerModel.HeaderType == "OnePlanet")
                    {
                        return PartialView("_PartialTopMenuOnePlanet", headerModel);
                    }
                    else
                    {
                        return new EmptyResult();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw;
            }
        }

        [HttpPost]
        public ActionResult RenderViewByEmail()
        {
            string optionTicketCode = WebConfigurationManager.AppSettings["use_sc_ticket_code"];
            bool redirectToSC = false;
            if (!string.IsNullOrEmpty(optionTicketCode))
            {
                redirectToSC = Convert.ToBoolean(optionTicketCode);
            }

            if (redirectToSC)
            {
                string username = "";
                if (Session["username"] != null)
                {
                    username = Session["username"].ToString();
                }

                Tbl_User user = _userRepository.FindByUsername(username);

                if (user != null && user.SupplierID.HasValue)
                {
                    Tbl_ContactPerson contactPerson = _contactPersonRepository.GetContactPersonByContectID(user.ContactID ?? 0);
                    Tbl_Organization organization = _organizationRepository.GetDataBySupplierID(user.SupplierID.Value);

                    if (contactPerson.Email != null && organization.TaxID != null && organization.BranchNo != null && organization.CountryCode != null)
                    {
                        string baseURL = WebConfigurationManager.AppSettings["baseURL"];
                        string ticketKey = WebConfigurationManager.AppSettings["sc_ticket_secret_key"];
                        string emailEncrypt = CryptographyManage.EncryptAESBase64(contactPerson.Email, ticketKey);

                        HttpClient verifyTokenHttp = new HttpClient();
                        string verifyEmail = string.Format("{0}api/sc/verifyEmail?email={1}", baseURL, emailEncrypt);
                        HttpResponseMessage responseCoc = verifyTokenHttp.GetAsync(verifyEmail).Result;
                        if (responseCoc.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            return Json(new
                            {
                                Response = "Success",
                                IsRedirectToSC = redirectToSC,
                                RedirectURL = HttpUtility.UrlDecode(Url.Action("RedirectCompanyProfileSC", "Redirect", new RouteValueDictionary(new
                                {
                                    emailEncrypt = emailEncrypt,
                                    taxId = organization.TaxID,
                                    branchNumber = organization.BranchNo,
                                    countryCode = organization.CountryCode
                                })))
                            });
                        }
                        else
                        {
                            HttpCookie cultureCookie = Request.Cookies["_culture"];
                            var languageId = cultureCookie.Value;

                            string errorMessage = "";
                            errorMessage = errorMessage.GetStringResource("_Regis.Msg_EmailCodeWrong", languageId);
                            return Json(new { Response = errorMessage });
                        }
                    }
                }

                return Json(new
                {
                    Response = "Success",
                    IsRedirectToSC = redirectToSC,
                    RedirectURL = ""
                });
            }
            else
            {
                return Json(new
                {
                    Response = "Success",
                    IsRedirectToSC = redirectToSC,
                    RedirectURL = ""
                });
            }
        }

    }
}
