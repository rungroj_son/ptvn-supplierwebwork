﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.LocalizedProperty;
using SupplierPortal.Data.Models.Repository.AutoMergeUser;
using SupplierPortal.Services.AutoMergeManage;
using System.Collections;
using SupplierPortal.Data.Models.SupportModel.AutoMerge;
using SupplierPortal.Data.Helper;

namespace SupplierPortal.Controllers
{
    [RoleAuthorize(Roles = "Administrator")]
    public class DevExpressController : Controller
    {

        ILocalizedPropertyRepository _localizedPropertyRepository;
        IAutoMergeUserRepository _autoMergeUserRepository;
        IAutoMergeTransactions _autoMergeTransactions; 

          public DevExpressController()
        {
            _localizedPropertyRepository = new LocalizedPropertyRepository();
            _autoMergeUserRepository = new AutoMergeUserRepository();
            _autoMergeTransactions = new AutoMergeTransactions();
        }

          public DevExpressController(
              LocalizedPropertyRepository localizedPropertyRepository,
              AutoMergeUserRepository autoMergeUserRepository,
              AutoMergeTransactions autoMergeTransactions
              )
        {
            _localizedPropertyRepository = localizedPropertyRepository;
            _autoMergeUserRepository = autoMergeUserRepository;
            _autoMergeTransactions = autoMergeTransactions;
        }

        // GET: DevExpress
        public ActionResult Index()
        {
            return View();
        }

        // GET: DevExpress/GridViewLargeDatabase
        public ActionResult GridViewLargeDatabase()
        {
            Session["ExpandAll"] = false;
            Session["CollapseAll"] = false;
            Session["Layout_PowerLoad"] = null;

            return View();
        }

        public ActionResult GridViewPowerLoad()
        {
            return PartialView(@"~/Views/DevExpress/_GridViewPartial_TestPowerLoad.cshtml");
        }

        // GET: DevExpress/GridViewNormalDatabase
        public ActionResult GridViewNormalDatabase()
        {
            Session["ExpandAll"] = false;
            Session["CollapseAll"] = false;
            Session["Layout_PowerLoad"] = null;

            return View(GetLocalizedProperty());
        }

        public ActionResult GridViewNormalLoad()
        {
            return PartialView(@"~/Views/DevExpress/_GridViewPartial_TestNormalLoad.cshtml", GetLocalizedProperty());
        }

        public IEnumerable GetLocalizedProperty()
        {
            var result = _localizedPropertyRepository.GetLocalizedPropAll();
            return result;
        }

        public ActionResult TestMergUserAccountPartial()
        {

            return View();
        }


        public ActionResult AutoMergeUser()
        {
            ViewBag.LimitGroup = "10";
            ViewBag.CheckReady = false;
            ViewBag.CheckNotReady = false;

            return View();
        }

        [HttpPost]
        public ActionResult AutoMergeUser(string limitGroup, bool checkReady, bool checkNotReady)
        {

            ViewBag.LimitGroup = limitGroup;
            ViewBag.CheckReady = checkReady;
            ViewBag.CheckNotReady = checkNotReady;

            return View(@"~/Views/DevExpress/AutoMergeUser.cshtml");
        }

        [HttpPost]
        public ActionResult MergeUserRun(string limitGroupMerge)
        {            
            try
            {
                int limit = 1000;
                if (!string.IsNullOrEmpty(limitGroupMerge))
                {
                    if (limitGroupMerge == "All")
                    {
                        limit = 10000;
                    }
                    else
                    {
                        limit = Convert.ToInt32(limitGroupMerge);
                    }
                }
                var mergeModel = _autoMergeTransactions.GetMergUserReady(limit);

                foreach (var item in mergeModel)
                {
                    if (item.isMerge)
                    {
                        _autoMergeTransactions.MergeUser(item);
                    }else
                    {
                        _autoMergeTransactions.UpdateMergeUserNonReady(item);
                    }
                    
                }

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }

            

           return  RedirectToAction(@"~/Views/DevExpress/AutoMergeUser.cshtml");
        }

        public ActionResult MergUserReadyPartial(string limitGroup, bool checkReady, bool checkNotReady)
        {
            int limit = 1000;
            if (!string.IsNullOrEmpty(limitGroup))
            {
                if (limitGroup == "All")
                {
                    limit = 10000;
                }else
                {
                    limit = Convert.ToInt32(limitGroup);
                }
                
            }
            var mergeModel = _autoMergeTransactions.GetMergUserReady(limit);

            if (checkReady)
            {
                mergeModel = mergeModel.Where(m => m.isMerge == true);
            }
            if (checkNotReady)
            {
                mergeModel = mergeModel.Where(m => m.isMerge == false);
            }
            //var model = _autoMergeUserRepository.GetAutoMergeUserUnSuccess();


            return PartialView(@"~/Views/DevExpress/_MergUserReadyPartial.cshtml", mergeModel);
        }

        public ActionResult MergUserSuccessPartial(string limitGroup)
        {
            int limit = 1000;
            if (!string.IsNullOrEmpty(limitGroup))
            {
                if (limitGroup == "All")
                {
                    limit = 10000;
                }
                else
                {
                    limit = Convert.ToInt32(limitGroup);
                }

            }


            var mergeSuccess = _autoMergeUserRepository.GetAutoMergeUserSuccess();


            var mergeGroup = mergeSuccess.GroupBy(d => new { d.MergeID })
                .Select(
                s => new 
                {
                    MergeID = s.Key.MergeID
                }
                ).Take(limit);

            var model = mergeSuccess.Where(m => mergeGroup.Select(x=>x.MergeID).Contains(m.MergeID)).ToList();

            return PartialView(@"~/Views/DevExpress/_MergUserSuccessPartial.cshtml", model);
        }

        public ActionResult AutoUpdateUser()
        {
            ViewBag.LimitGroup = "10";
            ViewBag.CheckReady = false;

            return View();
        }

        [HttpPost]
        public ActionResult AutoUpdateUser(string limitGroup)
        {
            ViewBag.LimitGroup = limitGroup;

            return View(@"~/Views/DevExpress/AutoUpdateUser.cshml");
        }

        public ActionResult UpdateUserReadyPartial(string limitGroup)
        {
            int limit = 1000;
            if (!string.IsNullOrEmpty(limitGroup))
            {
                if (limitGroup == "All")
                {
                    limit = 10000;
                }
                else
                {
                    limit = Convert.ToInt32(limitGroup);
                }
            }
            var model = _autoMergeUserRepository.GeAutoUpdateUserUnSuccess().Take(limit);


            return PartialView(@"~/Views/DevExpress/_UpdateUserReadyPartial.cshtml", model);
        }

        public ActionResult UpdateUserSuccessPartial(string limitGroup)
        {
            int limit = 1000;
            if (!string.IsNullOrEmpty(limitGroup))
            {
                if (limitGroup == "All")
                {
                    limit = 10000;
                }
                else
                {
                    limit = Convert.ToInt32(limitGroup);
                }
            }
            var model = _autoMergeUserRepository.GeAutoUpdateUserSuccess().Take(limit);

            return PartialView(@"~/Views/DevExpress/_UpdateUserSuccessPartial.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateUserRun(string limitGroupUpdate)
        {
            try
            {
                int limit = 1000;
                if (!string.IsNullOrEmpty(limitGroupUpdate))
                {
                    if (limitGroupUpdate == "All")
                    {
                        limit = 10000;
                    }else
                    {
                        limit = Convert.ToInt32(limitGroupUpdate);
                    }
                    
                }

                var model = _autoMergeUserRepository.GeAutoUpdateUserUnSuccess().Take(limit);
                foreach (var item in model)
                {
                    _autoMergeTransactions.UpdateUser(item.Username.Trim(), item.ID);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }



            return RedirectToAction("AutoUpdateUser");
        }

        #region ExpandAll/CollapseAll

        public void ExpandAll()
        {
            Session["ExpandAll"] = true;
            Session["CollapseAll"] = false;
        }

        public void CollapseAll()
        {
            Session["ExpandAll"] = false;
            Session["CollapseAll"] = true;
        }

        #endregion

    }
}