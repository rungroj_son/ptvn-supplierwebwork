﻿using SupplierPortal.Models;
using SupplierPortal.Models.Reports;
using SupplierPortal.ReportSupport;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SupplierPortal.Controllers
{

    public class ReportController : ControllerBase
    {

        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        #region SummaryReport(Main Page)

        //Key for DecryptString From BestSupply
        private static sbyte[] key2 = new sbyte[] { 94, 41, -98, -89, 81, -83, -108, 2, 127, 69, -39, -101, 47, 118, 59, 22, -83, 91, 26, -119, -62, 100, 79, -98 };
        
        [HttpPost]
        public ActionResult SummaryReportShow(DashboardModels model)
        {
            try
            {
                ClsReportChart oRptChart = new ClsReportChart();
                DataTable dtUserData = new DataTable();

                if (!string.IsNullOrEmpty(model.OrgID) && !string.IsNullOrEmpty(model.RptID))
                {
                    Session["suppShortName"] = model.OrgID;
                    Session["rptID"] = model.RptID;
                }
                else
                {
                    return RedirectToAction("Error", "Error", new { ErrorMessage = "Can not find report host configuration. Please contact your administrator." });
                }

                ClsSupportConnection connRpt = new ClsSupportConnection();
                if (connRpt.GetServerValueBySupplier(Session["suppShortName"].ToString()) == true)
                {
                    Session["LastUpdate"] = oRptChart.getRptLastUpdate(Convert.ToInt32(Session["rptID"])).Rows[0]["LastUpdated"];
                    //Session["LastUpdate"] = "";

                }
                else
                {
                    return RedirectToAction("Error", "Error", new { ErrorMessage = "Can not find report host configuration. Please contact your administrator." });
                    //return  RedirectToAction("~/Error/Timeout");
                }

                var dtBuyerGrp = new DataTable();
                dtBuyerGrp = oRptChart.getDtBuyerGrp(Session["suppShortName"].ToString());
                int buyerGrpRound = dtBuyerGrp.Rows.Count;

                var dtYear = new DataTable();
                dtYear = oRptChart.getDtYear();
                int yearRound = dtYear.Rows.Count;

                //************************
                //assign Value to Model
                //************************
                DashboardModels dashModel = new DashboardModels
                {

                    filter_BuyerGrps = Enumerable.Range(1, buyerGrpRound).Select(x => new BuyerGrpFilter
                    {
                        buyerGrpName = Convert.ToString(dtBuyerGrp.Rows[x - 1]["GroupName"])
                    }),
                    filter_Years = Enumerable.Range(1, yearRound).Select(x => new YearFilter
                    {
                        yearID = Convert.ToInt32(dtYear.Rows[x - 1]["Year"]),
                        yearName = Convert.ToString(dtYear.Rows[x - 1]["YearName"])
                    })
                };

                HttpCookie cookieYear = Request.Cookies.Get("_year");
                HttpCookie cookieBuyerGrp = Request.Cookies.Get("_buyer");

                HttpCookie C_orgID = Request.Cookies["_orgID"];
                HttpCookie C_rptID = Request.Cookies["_rptID"];

                string orgID = null;
                string rptID = null;

                if (model.selectedYear != 0)
                {
                    cookieYear.Value = model.selectedYear.ToString();
                    cookieBuyerGrp.Value = model.selectedBuyerGrp;
                    dashModel.selectedBuyerGrp = cookieBuyerGrp.Value;
                    dashModel.selectedYear = Convert.ToInt32(cookieYear.Value);
                }
                else
                {
                    dashModel.selectedBuyerGrp = cookieBuyerGrp.Value;
                    dashModel.selectedYear = Convert.ToInt32(cookieYear.Value);
                }

                if ((C_orgID != null) && (C_rptID != null) && (C_orgID.Value != "") && (C_rptID.Value != ""))
                {
                    dashModel.OrgID = C_orgID.Value.ToString();
                    dashModel.RptID = C_rptID.Value.ToString();
                }
                else
                {
                    C_orgID = new HttpCookie("_orgID");
                    C_rptID = new HttpCookie("_rptID");

                    //orgID = "metro";
                    orgID = model.OrgID;
                    rptID = model.RptID;

                    C_orgID.Value = orgID;
                    C_rptID.Value = rptID;
                }

                dashModel.RptLastUpdate = Session["LastUpdate"].ToString();

                DataRow[] result = dtBuyerGrp.Select("GroupName ='" + dashModel.selectedBuyerGrp + "'");

                var fiterCon = "";
                if (dashModel.selectedBuyerGrp == "All")
                    fiterCon = "All Buyer Group | " + dashModel.selectedYear;
                else
                    fiterCon = result[0]["GroupName"] + " | " + dashModel.selectedYear;
                dashModel.filterCondition = fiterCon;
                dashModel.RptLastUpdate = Session["LastUpdate"].ToString();

                Response.Cookies.Add(cookieYear);
                Response.Cookies.Add(cookieBuyerGrp);
                Response.Cookies.Add(C_orgID);
                Response.Cookies.Add(C_rptID);

                return View(dashModel);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Error", new { ErrorMessage = ex.Message });
            }
           
        }

         public ActionResult SummaryReportShow()
        {

            ClsReportChart oRptChart = new ClsReportChart();
            DataTable dtUserData = new DataTable();
            HttpCookie C_orgID = Request.Cookies.Get("_orgID");
            HttpCookie C_rptID = Request.Cookies.Get("_rptID");

            if ((C_orgID != null) && (C_rptID!= null) && (C_orgID.Value != "") && (C_rptID.Value != ""))
            {
                //Session["suppShortName"] = "metro";
                Session["suppShortName"] = C_orgID.Value.ToString();
                Session["rptID"] = C_rptID.Value.ToString();
            }
            else
            {
                //return PartialView("TimeOutError");
                RedirectToAction("Error", "Error", new { ErrorMessage = "Can not find report host configuration. Please contact your administrator." });
            }

            var dtBuyerGrp = new DataTable();
            dtBuyerGrp = oRptChart.getDtBuyerGrp(Session["suppShortName"].ToString());
            int buyerGrpRound = dtBuyerGrp.Rows.Count;

            var dtYear = new DataTable();
            dtYear = oRptChart.getDtYear();
            int yearRound = dtYear.Rows.Count;

            //************************
            //assign Value to Model
            //************************
            DashboardModels dashModel = new DashboardModels
            {
                filter_BuyerGrps = Enumerable.Range(1, buyerGrpRound).Select(x => new BuyerGrpFilter
                {
                    buyerGrpName = Convert.ToString(dtBuyerGrp.Rows[x - 1]["GroupName"])
                }),
                filter_Years = Enumerable.Range(1, yearRound).Select(x => new YearFilter
                {
                    yearID = Convert.ToInt32(dtYear.Rows[x - 1]["Year"]),
                    yearName = Convert.ToString(dtYear.Rows[x - 1]["YearName"])
                })
            };

            dashModel.OrgID = Session["suppShortName"].ToString();
            dashModel.RptID = Session["rptID"].ToString();

            HttpCookie cookieYear = Request.Cookies.Get("_year");
            HttpCookie cookieBuyerGrp = Request.Cookies.Get("_buyer");

            if ((cookieYear == null) && (cookieYear.Value == ""))
            {
                cookieYear.Value = dashModel.selectedYear.ToString();
                cookieBuyerGrp.Value = dashModel.selectedBuyerGrp;
            }
            else
            {
                dashModel.selectedBuyerGrp = cookieBuyerGrp.Value;
                dashModel.selectedYear = Convert.ToInt32(cookieYear.Value);
            }

            dashModel.RptLastUpdate = Session["LastUpdate"].ToString();

            DataRow[] result = dtBuyerGrp.Select("GroupName ='" + dashModel.selectedBuyerGrp + "'");

            var fiterCon = "";
            if (dashModel.selectedBuyerGrp == "All")
                fiterCon = "All Buyer Group | " + dashModel.selectedYear;
            else
                fiterCon = result[0]["GroupName"] + " | " + dashModel.selectedYear;
            dashModel.filterCondition = fiterCon;
            dashModel.RptLastUpdate = Session["LastUpdate"].ToString();

            Response.Cookies.Add(cookieYear);
            Response.Cookies.Add(cookieBuyerGrp);
            Response.Cookies.Add(C_orgID);
            Response.Cookies.Add(C_rptID);

            return View(dashModel);
        }
    
         public ActionResult PrintPage()
        {

            ClsReportChart oRptChart = new ClsReportChart();
            DataTable dtUserData = new DataTable();

           // HttpContext.Session["suppShortName"] = "metro";

            if ((Convert.ToString(HttpContext.Session["suppShortName"]) != "") && HttpContext.Session["suppShortName"] != null)
            {
                ClsSupportConnection connRpt = new ClsSupportConnection();
                connRpt.GetServerValueBySupplier(Session["suppShortName"].ToString());
                Session["LastUpdate"] = oRptChart.getRptLastUpdate(Convert.ToInt32(Session["rptID"])).Rows[0]["LastUpdated"];
                //Session["LastUpdate"] = "";
            }
            else
            {
                //return PartialView("TimeOutError");
                return RedirectToAction("Error", "Error", new { ErrorMessage = "Can not find report host configuration. Please contact your administrator." });
            }

            var dtBuyerGrp = new DataTable();
            dtBuyerGrp = oRptChart.getDtBuyerGrp(Session["suppShortName"].ToString());
            int buyerGrpRound = dtBuyerGrp.Rows.Count;

            var dtYear = new DataTable();
            dtYear = oRptChart.getDtYear();
            int yearRound = dtYear.Rows.Count;

            //************************
            //assign Value to Model
            //************************
            DashboardModels dashModel = new DashboardModels
            {

                filter_BuyerGrps = Enumerable.Range(1, buyerGrpRound).Select(x => new BuyerGrpFilter
                {
                    buyerGrpName = Convert.ToString(dtBuyerGrp.Rows[x - 1]["GroupName"])
                }),
                filter_Years = Enumerable.Range(1, yearRound).Select(x => new YearFilter
                {
                    yearID = Convert.ToInt32(dtYear.Rows[x - 1]["Year"]),
                    yearName = Convert.ToString(dtYear.Rows[x - 1]["YearName"])
                })
            };

            HttpCookie cookieYear = Request.Cookies.Get("_year");
            HttpCookie cookieBuyerGrp = Request.Cookies.Get("_buyer");

            if ((cookieYear == null) && (cookieYear.Value == ""))
            {
                cookieYear.Value = dashModel.selectedYear.ToString();
                cookieBuyerGrp.Value = dashModel.selectedBuyerGrp;
            }
            else
            {
                dashModel.selectedBuyerGrp = cookieBuyerGrp.Value;
                dashModel.selectedYear = Convert.ToInt32(cookieYear.Value);
            }

            dashModel.SuppShortName = Session["suppShortName"].ToString();

            DataRow[] result = dtBuyerGrp.Select("GroupName ='" + dashModel.selectedBuyerGrp + "'");

            var fiterCon = "";
            if (dashModel.selectedBuyerGrp == "All")
                fiterCon = "All Buyer Group | " + dashModel.selectedYear;
            else
                fiterCon = result[0]["GroupName"] + " | " + dashModel.selectedYear;
            dashModel.filterCondition = fiterCon;
            dashModel.RptLastUpdate = Session["LastUpdate"].ToString();

            return View(dashModel);
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult PrintPage(DashboardModels dModel)
        {
            //HttpContext.Session["suppShortName"] = "metro";

            //Set chart caption for each area
            ClsReportChart oRptChart = new ClsReportChart();
            var dtBuyerGrp = new DataTable();
            dtBuyerGrp = oRptChart.getDtBuyerGrp(Session["suppShortName"].ToString());
            var dtYear = new DataTable();
            dtYear = oRptChart.getDtYear();
            int buyerGrpRound = dtBuyerGrp.Rows.Count;
            int yearRound = dtYear.Rows.Count;

            //************************
            //assign Value to Model
            //************************
            DashboardModels dashModel = new DashboardModels
            {
                filter_BuyerGrps = Enumerable.Range(1, buyerGrpRound).Select(x => new BuyerGrpFilter
                {
                    buyerGrpName = Convert.ToString(dtBuyerGrp.Rows[x - 1]["GroupName"])
                }),
                filter_Years = Enumerable.Range(1, yearRound).Select(x => new YearFilter
                {
                    yearID = Convert.ToInt32(dtYear.Rows[x - 1]["Year"]),
                    yearName = Convert.ToString(dtYear.Rows[x - 1]["YearName"])
                })
            };

            dashModel.selectedBuyerGrp = dModel.selectedBuyerGrp;
            dashModel.selectedYear = dModel.selectedYear;

            dashModel.SuppShortName = Session["suppShortName"].ToString();

            DataRow[] result = dtBuyerGrp.Select("GroupName ='" + dashModel.selectedBuyerGrp + "'");

            var fiterCon = "";
            if (dashModel.selectedBuyerGrp == "All")
                fiterCon = "All Buyer Group | " + dashModel.selectedYear;
            else
                fiterCon = result[0]["GroupName"] + " | " + dashModel.selectedYear;
            dashModel.filterCondition = fiterCon;
            dashModel.RptLastUpdate = Session["LastUpdate"].ToString();

            return View(dashModel);
        }

        #endregion

        #region No. of PO (3D Pie Chart)

        [ChildActionOnly]
        [ActionName("_NumPOGet")]
        public PartialViewResult _NumPO(int year, string buyerGroupName)
        {
            //3D Pie Chart
            DataTable dtNumPO_Chart;

            string chart_NumPO;

            DateTime dt = DateTime.Now;
            if (year == -1)
            {
                year = dt.Year;
            }

            ClsReportChart oRptChart = new ClsReportChart();
            dtNumPO_Chart = oRptChart.getDtNumPO_Chart(Session["suppShortName"].ToString(), buyerGroupName, year);
            chart_NumPO = dtNumPO_Chart.Rows[0]["NumPO_Chart"].ToString();

            DataTable dtNumPO;
            dtNumPO = oRptChart.getDtNumPO(Session["suppShortName"].ToString(), buyerGroupName, year);
            int numPORound = dtNumPO.Rows.Count;

            ChartsModels_NumPO model = new ChartsModels_NumPO
            {
                data_NumPO = Enumerable.Range(1, numPORound).Select(x => new NumPO
                {
                    buyer = Convert.ToString(dtNumPO.Rows[x - 1]["Buyer"]),
                    numPO = String.Format("{0:#,##0}", dtNumPO.Rows[x - 1]["NumPO"]),
                    percentNumPO = Convert.ToDecimal(dtNumPO.Rows[x - 1]["PercentNumPO"])
                })
            };
            model.json_NumPO = chart_NumPO;

            //return PartialView("_NumPO", model);
            return PartialView(@"~/Views/Report/_NumPO.cshtml", model);
        }

        [HttpPost]
        public PartialViewResult _NumPO(DashboardModels dModel)
        {
            //3D Pie Chart
            DataTable dtNumPO_Chart;
            string chart_NumPO;

            ClsReportChart oRptChart = new ClsReportChart();
            dtNumPO_Chart = oRptChart.getDtNumPO_Chart(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);

            chart_NumPO = dtNumPO_Chart.Rows[0]["NumPO_Chart"].ToString();

            DataTable dtNumPO;
            dtNumPO = oRptChart.getDtNumPO(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);
            int numPORound = dtNumPO.Rows.Count;

            ChartsModels_NumPO model = new ChartsModels_NumPO
            {
                data_NumPO = Enumerable.Range(1, numPORound).Select(x => new NumPO
                {
                    buyer = Convert.ToString(dtNumPO.Rows[x - 1]["Buyer"]),
                    numPO = String.Format("{0:#,##0}", dtNumPO.Rows[x - 1]["NumPO"]),
                    percentNumPO = Convert.ToDecimal(dtNumPO.Rows[x - 1]["PercentNumPO"])
                })
            };
            model.json_NumPO = chart_NumPO;
            //return PartialView("_NumPO", model);
            return PartialView(@"~/Views/Report/_NumPO.cshtml", model);
        }

        #endregion

        #region PO Volume (3D Pie Chart)

        [ChildActionOnly]
        [ActionName("_POVolumeGet")]
        public PartialViewResult _POVolume(int year, string buyerGroupName)
        {
            //3D Pie Chart
            DataTable dtPOVolume_Chart;
            string chart_POVolume;

            DateTime dt = DateTime.Now;
            if (year == -1)
            {
                year = dt.Year;
            }

            ClsReportChart oRptChart = new ClsReportChart();
            dtPOVolume_Chart = oRptChart.getDtPOVolume_Chart(Session["suppShortName"].ToString(), buyerGroupName, year);

            chart_POVolume = dtPOVolume_Chart.Rows[0]["POVolume_Chart"].ToString();

            DataTable dtPOVolume;
            dtPOVolume = oRptChart.getDtPOVolume(Session["suppShortName"].ToString(), buyerGroupName, year);
            int POVolumeRound = dtPOVolume.Rows.Count;

            ChartsModels_POVolume model = new ChartsModels_POVolume
            {
                data_POVolume = Enumerable.Range(1, POVolumeRound).Select(x => new POVolume
                {
                    buyer = Convert.ToString(dtPOVolume.Rows[x - 1]["Buyer"]),
                    volume = String.Format("{0:#,##0.00}", dtPOVolume.Rows[x - 1]["POVolume"]),
                    percentVolume = Convert.ToDecimal(dtPOVolume.Rows[x - 1]["PercentVolume"])
                })
            };
            model.json_POVolume = chart_POVolume;

            //return PartialView("_POVolume", model);
            return PartialView(@"~/Views/Report/_POVolume.cshtml", model);
        }

        [HttpPost]
        public PartialViewResult _POVolume(DashboardModels dModel)
        {
            //3D Pie Chart
            DataTable dtPOVolume_Chart;
            string chart_POVolume;

            ClsReportChart oRptChart = new ClsReportChart();
            dtPOVolume_Chart = oRptChart.getDtPOVolume_Chart(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);

            chart_POVolume = dtPOVolume_Chart.Rows[0]["POVolume_Chart"].ToString();

            DataTable dtPOVolume;
            dtPOVolume = oRptChart.getDtPOVolume(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);
            int POVolumeRound = dtPOVolume.Rows.Count;

            ChartsModels_POVolume model = new ChartsModels_POVolume
            {
                data_POVolume = Enumerable.Range(1, POVolumeRound).Select(x => new POVolume
                {
                    buyer = Convert.ToString(dtPOVolume.Rows[x - 1]["Buyer"]),
                    volume = String.Format("{0:#,##0.00}", dtPOVolume.Rows[x - 1]["POVolume"]),
                    percentVolume = Convert.ToDecimal(dtPOVolume.Rows[x - 1]["PercentVolume"])
                })
            };
            model.json_POVolume = chart_POVolume;
            //return PartialView("_POVolume", model);
            return PartialView(@"~/Views/Report/_POVolume.cshtml", model);
        }

        #endregion

        #region PO Volume by Status (3D Pie Chart)

        [ChildActionOnly]
        [ActionName("_POStatusGet")]
        public PartialViewResult _POStatus(int year, string buyerGroupName)
        {
            //3D Pie Chart
            DataTable dtPOStatus_Chart;
            string chart_POStatus;

            DateTime dt = DateTime.Now;
            if (year == -1)
            {
                year = dt.Year;
            }

            ClsReportChart oRptChart = new ClsReportChart();
            dtPOStatus_Chart = oRptChart.getDtPOStatus_Chart(Session["suppShortName"].ToString(), buyerGroupName, year);

            chart_POStatus = dtPOStatus_Chart.Rows[0]["POVolumeByStatus_Chart"].ToString();

            DataTable dtPOStatus;
            dtPOStatus = oRptChart.getDtPOStatus(Session["suppShortName"].ToString(), buyerGroupName, year);
            int POStatusRound = dtPOStatus.Rows.Count;

            ChartsModels_POStatus model = new ChartsModels_POStatus
            {
                data_POStatus = Enumerable.Range(1, POStatusRound).Select(x => new POStatus
                {
                    status = Convert.ToString(dtPOStatus.Rows[x - 1]["StatusName"]),
                    volume = String.Format("{0:#,##0.00}", dtPOStatus.Rows[x - 1]["POVolume"]),
                    numPO = String.Format("{0:#,##0}", dtPOStatus.Rows[x - 1]["NumPO"]),
                    percentNumPO = Convert.ToDecimal(dtPOStatus.Rows[x - 1]["PercentNumPO"])
                })
            };
            model.json_POStatus = chart_POStatus;

            //return PartialView("_POStatus", model);
            return PartialView(@"~/Views/Report/_POStatus.cshtml", model);
        }

        [HttpPost]
        public PartialViewResult _POStatus(DashboardModels dModel)
        {
            //3D Pie Chart
            DataTable dtPOStatus_Chart;
            string chart_POStatus;

            ClsReportChart oRptChart = new ClsReportChart();
            dtPOStatus_Chart = oRptChart.getDtPOStatus_Chart(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);

            chart_POStatus = dtPOStatus_Chart.Rows[0]["POVolumeByStatus_Chart"].ToString();

            DataTable dtPOStatus;
            dtPOStatus = oRptChart.getDtPOStatus(Session["suppShortName"].ToString(), dModel.selectedBuyerGrp, dModel.selectedYear);
            int POStatusRound = dtPOStatus.Rows.Count;

            ChartsModels_POStatus model = new ChartsModels_POStatus
            {
                data_POStatus = Enumerable.Range(1, POStatusRound).Select(x => new POStatus
                {
                    status = Convert.ToString(dtPOStatus.Rows[x - 1]["StatusName"]),
                    volume = String.Format("{0:#,##0.00}", dtPOStatus.Rows[x - 1]["POVolume"]),
                    numPO = String.Format("{0:#,##0}", dtPOStatus.Rows[x - 1]["NumPO"]),
                    percentNumPO = Convert.ToDecimal(dtPOStatus.Rows[x - 1]["PercentNumPO"])
                })
            };
            model.json_POStatus = chart_POStatus;
            //return PartialView("_POStatus", model);
            return PartialView(@"~/Views/Report/_POStatus.cshtml", model);
        }

        #endregion

       public ActionResult TimeOutError()
        {
            return View("~/Error/Timeout");
        }

        public ActionResult setDefault()
        {
            string year = null;
            HttpCookie cookieYear = Request.Cookies["_year"];
            HttpCookie cookieBuyerGrp = Request.Cookies["_buyer"];

                year = null;

                DateTime dt = DateTime.Now;
                year = dt.Year.ToString();
                cookieYear = new HttpCookie("_year");
                cookieYear.Value = year;
                cookieBuyerGrp = new HttpCookie("_buyer");
                cookieBuyerGrp.Value = "All";

            Response.Cookies.Add(cookieYear);
            Response.Cookies.Add(cookieBuyerGrp);
            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }
      
    }
}