﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Data.Models.SupportModel.CompanyProfile;
using SupplierPortal.Data.Models.Repository.OrgShareholder;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.NameTitle;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Data.Models.Repository.ReferanceCustomer;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.BusinessEntity;
using SupplierPortal.Data.Models.Repository.Currency;
using SupplierPortal.Data.Models.Repository.LogOrganization;
using SupplierPortal.Framework.MethodHelper;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.Function;
using SupplierPortal.Data.Models.Repository.OrgRefCustomer;
using SupplierPortal.Data.Models.Repository.OrgFacility;
using SupplierPortal.Data.Models.Repository.Address;
using SupplierPortal.Data.Models.Repository.FacilityType;
using SupplierPortal.Data.Models.Repository.LogOrgFacility;
using SupplierPortal.Services.CompanyProfileManage;
using SupplierPortal.Data.Models.Repository.AutoComplete_State;
using System.Text.RegularExpressions;
using SupplierPortal.Data.Models.Repository.AutoComplete_City;
using SupplierPortal.Data.Models.Repository.AutoComplete_SubDistrict;
using SupplierPortal.Data.Models.SupportModel;

namespace SupplierPortal.Controllers
{
    [Serializable()]
    public class AdditionalInformationController : ControllerBase
    {
        private IUserRepository _userRepository;
        private IOrganizationRepository _organizationRepository;
        private IOrgShareholderRepository _orgShareholderRepository;
        private IOrgFacilityRepository _orgFacilityRepository;
        private ILogOrgFacilityRepository _logorgFacilityRepository;
        private ICountryRepository _countryRepository;
        private INameTitleRepository _nameTitleRepository;
        private IOrgRefCustomerRepository _orgRefCustomerRepository;
        private IBusinessEntityRepository _businessEntityRepository;
        private ICurrencyRepository _currencyRepository;
        private ILanguageRepository _languageRepository;
        private IFunctionRepository _functionRepository;
        private IAddressRepository _addressRepository;
        private IFacilityTypeRepository _facilityRepository;
        private IComProfileDataService _comProfileDataService;
        private IAutoComplete_StateRepository _autoComplete_StateRepository;
        private IAutoComplete_CityRepository _autoComplete_CityRepository;
        private IAutoComplete_SubDistrictRepository _autoComplete_SubDistrictRepository;

        public AdditionalInformationController()
        {
            _userRepository = new UserRepository();
            _organizationRepository = new OrganizationRepository();
            _orgShareholderRepository = new OrgShareholderRepository();
            _orgFacilityRepository = new OrgFacilityRepository();
            _logorgFacilityRepository = new LogOrgFacilityRepository();
            _countryRepository = new CountryRepository();
            _nameTitleRepository = new NameTitleRepository();
            _orgRefCustomerRepository = new OrgRefCustomerRepository();
            _businessEntityRepository = new BusinessEntityRepository();
            _currencyRepository = new CurrencyRepository();
            _languageRepository = new LanguageRepository();
            _functionRepository = new FunctionRepository();
            _addressRepository = new AddressRepository();
            _facilityRepository = new FacilityTypeRepository();
            _comProfileDataService = new ComProfileDataService();
            _autoComplete_StateRepository = new AutoComplete_StateRepository();
            _autoComplete_CityRepository = new AutoComplete_CityRepository();
            _autoComplete_SubDistrictRepository = new AutoComplete_SubDistrictRepository();
        }

        public AdditionalInformationController
            (
                UserRepository userRepository,
                OrganizationRepository organizationRepository,
                OrgShareholderRepository orgShareholderRepository,
                OrgFacilityRepository orgFacilityRepository,
                LogOrgFacilityRepository logorgFacilityRepository,
                CountryRepository countryRepository,
                NameTitleRepository nameTitleRepository,
                OrgRefCustomerRepository orgRefCustomerRepository,
                BusinessEntityRepository businessEntityRepository,
                CurrencyRepository currencyRepository,
                LanguageRepository languageRepository,
                FunctionRepository functionRepository,
                AddressRepository addressRepository,
                FacilityTypeRepository facilityRepository,
                ComProfileDataService comProfileDataService,
            AutoComplete_StateRepository autoComplete_StateRepository,
            AutoComplete_CityRepository autoComplete_CityRepository,
            AutoComplete_SubDistrictRepository autoComplete_SubDistrictRepository
            )
        {
            _organizationRepository = organizationRepository;
            _userRepository = userRepository;
            _orgShareholderRepository = orgShareholderRepository;
            _orgFacilityRepository = orgFacilityRepository;
            _logorgFacilityRepository = logorgFacilityRepository;
            _countryRepository = countryRepository;
            _nameTitleRepository = nameTitleRepository;
            _orgRefCustomerRepository = orgRefCustomerRepository;
            _businessEntityRepository = businessEntityRepository;
            _currencyRepository = currencyRepository;
            _languageRepository = languageRepository;
            _functionRepository = functionRepository;
            _addressRepository = addressRepository;
            _facilityRepository = facilityRepository;
            _comProfileDataService = comProfileDataService;
            _autoComplete_StateRepository = autoComplete_StateRepository;
            _autoComplete_CityRepository = autoComplete_CityRepository;
            _autoComplete_SubDistrictRepository = autoComplete_SubDistrictRepository;
        }

        // GET: AdditionalInformation
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Additional()
        {
            string username = "";
            //int userID = 0;
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            int supID = _organizationRepository.GetSupplierIDByUsername(username);

            var Orgmodel = _organizationRepository.GetDataBySupplierID(supID);
            var model = new GeneralInfosModel();

            if (string.IsNullOrEmpty(Orgmodel.CurrencyCode))
            {
                if (Orgmodel.CountryCode == "TH")
                {
                    model.CurrencyCode = "THB";
                }
                else
                {
                    model.CurrencyCode = "USD";
                }
            }
            else
            {
                model.CurrencyCode = Orgmodel.CurrencyCode;
            }

            model.SupplierID = Orgmodel.SupplierID;
            model.YearEstablished = Orgmodel.YearEstablished;
            model.RegisteredCapital = Orgmodel.RegisteredCapital;
            //model.CurrencyCode = Orgmodel.CurrencyCode;
            model.NumberOfEmp = Orgmodel.NumberOfEmp;

            return View(@"~/Views/AdditionalInformation/Additional.cshtml", model);
        }

        [HttpPost]
        public ActionResult Additional(GeneralInfosModel model, Tbl_Organization orgmodel)
        {
            var OrgModel = _organizationRepository.GetDataBySupplierID(orgmodel.SupplierID); //orgmodel.sup
            int userID = 0;
            string username = ""; //Metro-Admin
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
            }
            if (OrgModel != null)
            {
                OrgModel.YearEstablished = (orgmodel.YearEstablished ?? "").Trim();
                OrgModel.RegisteredCapital = orgmodel.RegisteredCapital;
                OrgModel.CurrencyCode = (orgmodel.CurrencyCode ?? "").Trim();
                OrgModel.NumberOfEmp = orgmodel.NumberOfEmp;
                _organizationRepository.Update(OrgModel, userID);

                #region Update Tbl_Organization.LastUpdate

                _organizationRepository.UpdateLastUpdate(orgmodel.SupplierID, userID);

                #endregion Update Tbl_Organization.LastUpdate
            }

            return View(@"~/Views/AdditionalInformation/Additional.cshtml", model);
        }

        public ActionResult CurrencyBox(GeneralInfosModel model)
        {
            ViewBag.CurrencyList = _currencyRepository.CurrencyList().Where(w => new[] { "THB", "USD", "VND" }.Any(s => s == w.CurrencyCode));
            //var data = new List<LocalizedModel>();
            //var dataTH = new LocalizedModel()
            //{
            //    EntityID = 1,
            //    EntityValue = "THB"
            //};
            //var dataUSD = new LocalizedModel()
            //{
            //    EntityID = 2,
            //    EntityValue = "USD"
            //};
            //data.Add(dataTH);
            //data.Add(dataUSD);
            //ViewBag.CurrencyList = data;

            return PartialView(@"~/Views/AdditionalInformation/CurrencyBox.cshtml", model);
        }

        public ActionResult YearEstablishedList(GeneralInfosModel model)
        {
            var start = 1900;
            var end = DateTime.UtcNow.Year;
            var ListYear = new List<object>();
            for (var year = start; year <= end; year++)
            {
                ListYear.Add(new { YearEstablished = year });  //options.Add(year);
            }
            if (model.YearEstablished == "" || model.YearEstablished == null)
            {
                model.YearEstablished = "1900";
            }

            ViewBag.YearEstablishedList = ListYear;
            return PartialView(@"~/Views/AdditionalInformation/YearEstablishedList.cshtml", model);
        }

        public ActionResult Shareholder()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Shareholder(CompanyShareholderModel model, string command = "add")
        {
            string username = "";
            int supplierID = 0;
            int userID = 0;
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
                supplierID = user.UserID;
            }
            supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            var modelShare = new List<CompanyShareholderModel>();

            var orgShare = _orgShareholderRepository.GetDataBySupplierID(supplierID).ToList();
            try
            {
                var item = TempData["TempShareholder"] as List<CompanyShareholderModel>;
                var dataShare = new Tbl_OrgShareholder();

                if (command.Equals("add"))
                {
                    //var item = TempShareholder.FirstOrDefault(w => w.Id == Shareholder.Id);
                    dataShare.SupplierID = supplierID;
                    dataShare.SeqNo = model.SeqNo + 1;
                    dataShare.FirstName_Local = model.FirstName_Local ?? "";
                    dataShare.FirstName_Inter = model.FirstName_Inter ?? "";
                    dataShare.LastName_Local = model.LastName_Local ?? "";
                    dataShare.LastName_Inter = model.LastName_Inter ?? "";
                    dataShare.CountyCode = model.CountryCode ?? "";
                    dataShare.IdentityNo = model.IdentityNo ?? "";
                    dataShare.TitleID = model.TitleID;

                    _orgShareholderRepository.InsertShareholder(dataShare);
                }
                else if (command.Equals("edit"))
                {
                    var data = _orgShareholderRepository.GetDataByID(model.Id);
                    data.SupplierID = supplierID;
                    data.SeqNo = model.SeqNo + 1;
                    data.FirstName_Local = model.FirstName_Local ?? "";
                    data.FirstName_Inter = model.FirstName_Inter ?? "";
                    data.LastName_Local = model.LastName_Local ?? "";
                    data.LastName_Inter = model.LastName_Inter ?? "";
                    data.CountyCode = model.CountryCode ?? "";
                    data.IdentityNo = model.IdentityNo ?? "";
                    data.TitleID = model.TitleID;

                    _orgShareholderRepository.UpdateShareholder(data);
                }

                #region Update Tbl_Organization.LastUpdate

                _organizationRepository.UpdateLastUpdate(supplierID, userID);

                #endregion Update Tbl_Organization.LastUpdate
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Shareholder");
        }

        public ActionResult _ShareholderPartial()
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            int languageId = Convert.ToInt32(languageID);

            int supplierID = 0;
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            var model = new List<CompanyShareholderModel>();
            //model.SupplierID = supplierID;
            var orgShare = _orgShareholderRepository.GetDataBySupplierID(supplierID).ToList();
            var Organizetion = _organizationRepository.GetDataBySupplierID(supplierID);

            var languageLocal = _languageRepository.GetLocalLanguage();
            var languageInter = _languageRepository.GetInterLanguage();

            foreach (var item in orgShare)
            {
                //if (item != null)
                //{
                var modelItem = new CompanyShareholderModel();
                modelItem.Id = item.Id;
                modelItem.SupplierID = item.SupplierID;
                modelItem.SeqNo = item.SeqNo;
                modelItem.TitleID = item.TitleID ?? 0;
                modelItem.TitleID_Inter = item.TitleID ?? 0;
                modelItem.FirstName_Local = item.FirstName_Local;
                modelItem.FirstName_Inter = item.FirstName_Inter;
                modelItem.LastName_Local = item.LastName_Local;
                modelItem.LastName_Inter = item.LastName_Inter;
                modelItem.CountryCode = item.CountyCode;
                modelItem.IdentityNo = item.IdentityNo;
                modelItem.CompanyTypeID = Organizetion.CompanyTypeID ?? 0;
                modelItem.CountryName = _countryRepository.GetCountryNameByCountryCode(item.CountyCode);

                modelItem.TitleName = _nameTitleRepository.GetTitleNameByIdAndLanguageID(item.TitleID ?? 0, languageLocal.LanguageID);
                modelItem.TitleName_Inter = _nameTitleRepository.GetTitleNameByIdAndLanguageID(item.TitleID ?? 0, languageInter.LanguageID);

                model.Add(modelItem);

                //}
            }

            ViewBag.NameTitle_Local = _nameTitleRepository.GetNameTitleList(languageLocal.LanguageID);
            ViewBag.NameTitle_Inter = _nameTitleRepository.GetNameTitleList(languageInter.LanguageID);

            string countryName = _countryRepository.GetCountryNameByCountryCode(Organizetion.CountryCode);

            ViewBag.DefaultShareholder = new CompanyShareholderModel()
            {
                CountryCode = Organizetion.CountryCode,
                CountryName = countryName,
                TitleID = 4,
                TitleID_Inter = 4
            };

            return View(@"~/Views/AdditionalInformation/_ShareholderPartial.cshtml", model);
        }

        public List<CompanyShareholderModel> TempShareholder
        {
            get
            {
                var temp = TempData["TempShareholder"] as List<CompanyShareholderModel>;
                TempData.Keep("TempShareholder");
                return temp;
            }
            set
            {
                TempData["TempShareholder"] = value;
            }
        }

        public PartialViewResult AddShareholder()
        {
            int supplierID = 0;
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            var Organizetion = _organizationRepository.GetDataBySupplierID(supplierID);
            var model = new CompanyShareholderModel()
            {
                CountryCode = Organizetion.CountryCode,
                TitleID = 4,
                TitleID_Inter = 4
            };

            return ShareTempFromPartial(model, "add");
        }

        public PartialViewResult EditShareholder(int id)
        {
            var model = TempShareholder.FirstOrDefault(f => f.Id == id);
            // model.TitleID = ComboBoxExtension.GetValue<int>("NameTitleID");
            // model.TitleName = Request.Form["NameTitleID"];
            return ShareTempFromPartial(model, "edit");
        }

        public PartialViewResult ResetShareholder()
        {
            int supplierID = 0;
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            var Organizetion = _organizationRepository.GetDataBySupplierID(supplierID);

            string countryName = _countryRepository.GetCountryNameByCountryCode(Organizetion.CountryCode);

            var model = new CompanyShareholderModel()
            {
                CountryCode = Organizetion.CountryCode,
                CountryName = countryName,
                TitleID = 4,
                TitleID_Inter = 4
            };
            return ShareTempFromPartial(model, "add");
        }

        private PartialViewResult ShareTempFromPartial(CompanyShareholderModel model, string command)
        {
            var languageLocal = _languageRepository.GetLocalLanguage();
            var languageInter = _languageRepository.GetInterLanguage();

            ViewBag.Command = command;
            ViewBag.NameTitle_Local = _nameTitleRepository.GetNameTitleList(languageLocal.LanguageID);
            ViewBag.NameTitle_Inter = _nameTitleRepository.GetNameTitleList(languageInter.LanguageID);
            return PartialView(@"~/Views/AdditionalInformation/_ShareTampFormPatrial.cshtml", model);
        }

        public ActionResult ShareholderTemplate(int id, string command = "add")
        {
            if (command.Equals("delete"))
            {
                var item = _orgShareholderRepository.GetDataByID(id);
                _orgShareholderRepository.DeleteSharehoder(item);
            }
            return RedirectToAction("Shareholder");
        }

        public ActionResult Certificate()
        {
            return View();
        }

        public ActionResult Transport()
        {
            return View();
        }

        public ActionResult CountryCompanyPartial(CompanyShareholderModel model)
        {
            //ViewData["CountryList"] = _countryRepository.GetCountryList();
            return PartialView(@"~/Views/AdditionalInformation/_CountryCompanyPartial.cshtml", model);
        }

        public ActionResult ReferenceCustomer()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ReferenceCustomer(CompanyReferenceCustomer model, string command = "add")
        {
            string username = "";
            int supplierID = 0;
            int userID = 0;
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            if (user != null)
            {
                userID = user.UserID;
            }
            supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            var modelRef = new List<CompanyReferenceCustomer>();

            var orgRef = _orgRefCustomerRepository.GetDataBySupplierID(supplierID).ToList();
            try
            {
                var dataRef = new Tbl_OrgRefCustomer();

                if (command.Equals("add"))
                {
                    dataRef.SupplierID = supplierID;
                    dataRef.SeqNo = model.SeqNo + 1;
                    dataRef.CompanyName_Local = model.CompanyName_Local ?? "";
                    dataRef.CompanyName_Inter = model.CompanyName_Inter ?? "";
                    dataRef.BusinessEntityID = model.BusinessEntityID;
                    dataRef.OtherBusinessEntity = model.OtherBusinessEntity ?? "";

                    _orgRefCustomerRepository.InsertRefCustomer(dataRef);
                }
                else if (command.Equals("edit"))
                {
                    var item = _orgRefCustomerRepository.GetDataById(model.Id);
                    item.SeqNo = model.SeqNo;
                    item.CompanyName_Local = model.CompanyName_Local ?? "";
                    item.CompanyName_Inter = model.CompanyName_Inter ?? "";
                    item.BusinessEntityID = model.BusinessEntityID;
                    if (item.BusinessEntityID != -1)
                    {
                        model.OtherBusinessEntity = "";
                    }
                    item.OtherBusinessEntity = model.OtherBusinessEntity ?? "";

                    _orgRefCustomerRepository.UpdateRefCustomer(item);
                }

                #region Update Tbl_Organization.LastUpdate
                _organizationRepository.UpdateLastUpdate(supplierID, userID);
                #endregion Update Tbl_Organization.LastUpdate
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction(@"~/Views/AdditionalInformation/ReferenceCustomer.cshtml");
        }

        public ActionResult _ReferenceCustomerPartial()
        {
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            int languageId = Convert.ToInt32(languageID);

            int supplierID = 0;
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            var model = new List<CompanyReferenceCustomer>();
            //model.SupplierID = supplierID;
            var orgShare = _orgRefCustomerRepository.GetDataBySupplierID(supplierID).ToList();

            if (orgShare.Count() > 0)
            {
                foreach (var item in orgShare)
                {
                    if (item != null)
                    {
                        var modelItem = new CompanyReferenceCustomer();
                        modelItem.Id = item.Id;
                        modelItem.SeqNo = item.SeqNo;
                        modelItem.CompanyName_Local = item.CompanyName_Local;
                        modelItem.CompanyName_Inter = item.CompanyName_Inter;
                        modelItem.BusinessEntityID = item.BusinessEntityID;
                        modelItem.BusinessEntity = _businessEntityRepository.GetBusinessEntityByIdAndLanguageID(item.BusinessEntityID ?? 0, languageId);
                        modelItem.OtherBusinessEntity = item.OtherBusinessEntity;

                        model.Add(modelItem);
                    }
                }
            }

            return View(@"~/Views/AdditionalInformation/_ReferenceCustomerPartial.cshtml", model);
        }

        public List<CompanyReferenceCustomer> TempRefCustomer
        {
            get
            {
                var temp = TempData["TempRefCustomer"] as List<CompanyReferenceCustomer>;
                TempData.Keep("TempRefCustomer");
                return temp;
            }
            set
            {
                TempData["TempRefCustomer"] = value;
            }
        }

        public PartialViewResult AddRefCustomer()
        {
            var model = new CompanyReferenceCustomer();
            ViewBag.Command = "add";
            return PartialView(@"~/Views/AdditionalInformation/_RefTempFormPatrial.cshtml", model);
        }

        public PartialViewResult EditRefCustomer(int id)
        {
            var model = TempRefCustomer.FirstOrDefault(f => f.Id == id);
            ViewBag.Command = "edit";
            return PartialView(@"~/Views/AdditionalInformation/_RefTempFormPatrial.cshtml", model);
        }

        public PartialViewResult ResetRefCustomer()
        {
            var model = new CompanyReferenceCustomer();
            ViewBag.Command = "add";
            return PartialView(@"~/Views/AdditionalInformation/_RefTempFormPatrial.cshtml", model);
        }

        public ActionResult RefCustomerTemplate(int id, string command = "add")
        {
            if (command.Equals("delete"))
            {
                var item = _orgRefCustomerRepository.GetDataById(id);
                _orgRefCustomerRepository.DeleteRefCustomer(item);
            }
            return RedirectToAction(@"~/Views/AdditionalInformation/ReferenceCustomer.cshtml");
        }

        public ActionResult BusinessEntityPartial(CompanyReferenceCustomer model)
        {
            //var corperate = Request.Params["CorperateValue"] as string ?? string.Empty;
            int supplierID = 0;
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            var orgModel = _organizationRepository.GetDataBySupplierID(supplierID);

            int companyTypeID = 1;
            if (orgModel != null)
            {
                companyTypeID = orgModel.CompanyTypeID ?? 1;
            }
            else if (model.CompanyTypeID != null)
            {
                companyTypeID = model.CompanyTypeID ?? 1;
            }

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();
            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            ViewBag.BusinessEntityList = _businessEntityRepository.GetMultiLangBusinessEntity(companyTypeID, languageId);

            return PartialView(@"~/Views/AdditionalInformation/_BusinessEntityPartial.cshtml", model);
        }

        public ActionResult IdentityNoValid(string identityNo, string CountryCode_VI, string CompanyTypeID, int Id)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";

            if (CountryCode_VI == "TH")
            {
                var sqlQuery = "select dbo.VerifyTaxNumber('" + identityNo.Trim() + "')";
                var result = _functionRepository.GetResultByQuery(sqlQuery);

                if (!result)
                {
                    if (CompanyTypeID == "0")
                    {
                        errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatTaxID.Corp", languageId);

                        return Json(errorMessage, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.InvalidFormatTaxID.Ordinary", languageId);

                        return Json(errorMessage, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            //check Duplicate
            if (IsValid)
            {
                if (Id != 0)
                {
                    IsValid = TempShareholder.Any(a => a.IdentityNo == identityNo && a.Id == Id) ?
                        true : !TempShareholder.Any(a => a.IdentityNo == identityNo);
                }
                else
                {
                    IsValid = !TempShareholder.Any(a => a.IdentityNo == identityNo);
                }
                if (!IsValid)
                    errorMessage = "This IdentityNo is already exists in Shareholder.";
            }

            if (IsValid)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }

            return Json(errorMessage, JsonRequestBehavior.AllowGet);
        }

        //Facility
        public ActionResult Facility()
        {
            #region Set LanguageID
            #region
            //string cultureId = null;
            //var languagesBrowser = Request.UserLanguages[0];
            //string[] words = languagesBrowser.Split('-');
            //string cultureCurrenc = words[0];
            //string currencCultureId = CultureHelper.GetCultureIdByCurrencCultureCode(cultureCurrenc);

            //HttpCookie cultureCookie = Request.Cookies["_culture"];
            //if (cultureCookie != null)
            //{
            //    cultureId = cultureCookie.Value;
            //}
            //else
            //{
            //    cultureId = null;
            //    cultureId = CultureHelper.GetImplementedCulture(currencCultureId); // This is safe
            //    cultureCookie = new HttpCookie("_culture");
            //    cultureCookie.Value = cultureId;
            //    cultureCookie.Expires = DateTime.Now.AddDays(30);

            //}
            #endregion Set LanguageID
            HttpCookie cultureCookie = Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();
            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);
            //Session["faclanguageID"] = languageId;
            #endregion Set LanguageID

            //string username = "Metro-Admin";
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            //else
            //{
            //    Session["username"] = "AA00030";
            //}
            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            Session["_FacSuppID"] = supplierID;
            var Organizetion = _organizationRepository.GetDataBySupplierID(supplierID);
            string countryName = _countryRepository.GetCountryNameByCountryCode(Organizetion.CountryCode);
            var model = new CompanyFacilityModel();
            model.SupplierID = supplierID;
            model.CountryCode = Organizetion.CountryCode;
            model.CountryName = countryName;
            return View(model);
        }

        public ActionResult FacilityTypePartial(CompanyFacilityModel model)
        {
            ViewBag.FacilityList = _facilityRepository.GetMultiLFacilityList();
            return PartialView(@"~/Views/AdditionalInformation/_FacilityTypePartial.cshtml", model);
        }

        public ActionResult FacilityTable(int supplierID)
        {
            var model = _orgFacilityRepository.GetDataFacAddressBySupplierID(supplierID);

            return PartialView(@"~/Views/AdditionalInformation/_FacilityTampTablePatrtial.cshtml", model.OrderBy(m => m.SeqNo));
        }

        [HttpPost]
        public ActionResult SaveFacility(CompanyFacilityModel model)
        {
            #region  Set LanguageID
            string languageID = "";
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();
            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);
            //Session["faclanguageID"] = languageId;
            #endregion Set LanguageID
            int isInter = languageId;
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);
            int userID = user.UserID;

            if (model != null)
            {
                //int supplierID = _organizationRepository.GetSupplierIDByUsername(username);
                int supplierID = Convert.ToInt32(Session["_FacSuppID"]);
                var FacilityTypeList = _facilityRepository.GetFacilityByFacilityID(model.FacilityTypeID);
                var ORGFacList = _orgFacilityRepository.GetDataFacAddressBySupplierID(supplierID);

                if (model.Command == "edit")
                {
                    var tbFacility = new Tbl_OrgFacility();
                    var tbAddress = new Tbl_Address();

                    tbFacility.FacilityID = model.FacilityID;
                    tbFacility.FacilityName = (model.FacilityName ?? "").Trim();
                    tbFacility.FacilityTypeID = model.FacilityTypeID;
                    tbFacility.FaxExt = (model.FaxExt ?? "").Trim();
                    tbFacility.FaxNo = (model.FaxNo ?? "").Trim();
                    tbFacility.MobileNo = (model.MobileNo ?? "").Trim();
                    tbFacility.PhoneExt = (model.PhoneExt ?? "").Trim();
                    tbFacility.PhoneNo = (model.PhoneNo ?? "").Trim();
                    tbFacility.WebSite = (model.WebSite ?? "").Trim();
                    tbFacility.AddressID = model.AddressID;
                    tbFacility.SupplierID = model.SupplierID;
                    tbFacility.SeqNo = model.SeqNo;

                    var addrList = _addressRepository.GetAddressByAddressID(model.AddressID ?? 0);

                    if (isInter.Equals(1))
                    {
                        if (addrList != null)
                        {
                            tbAddress.HouseNo_Inter = model.HouseNo_Local ?? addrList.HouseNo_Inter ?? "";
                            tbAddress.VillageNo_Inter = model.VillageNo_Local ?? addrList.VillageNo_Inter ?? "";
                            tbAddress.Lane_Inter = model.Lane_Local ?? addrList.Lane_Inter ?? "";
                            tbAddress.Road_Inter = model.Road_Local ?? addrList.Road_Inter ?? "";
                            tbAddress.SubDistrict_Inter = model.SubDistrict_Local ?? addrList.SubDistrict_Inter ?? "";
                            tbAddress.City_Inter = model.City_Local ?? addrList.City_Inter ?? "";
                            tbAddress.State_Inter = model.State_Local ?? addrList.State_Inter ?? "";

                            tbAddress.PostalCode = model.PostalCode ?? addrList.PostalCode ?? "";
                            tbAddress.CountryCode = model.CountryCode ?? addrList.CountryCode ?? "";

                            tbAddress.HouseNo_Local = addrList.HouseNo_Local ?? "";
                            tbAddress.VillageNo_Local = addrList.VillageNo_Local ?? "";
                            tbAddress.Lane_Local = addrList.Lane_Local ?? "";
                            tbAddress.Road_Local = addrList.Road_Local ?? "";
                            tbAddress.SubDistrict_Local = addrList.SubDistrict_Local ?? "";
                            tbAddress.City_Local = addrList.City_Local ?? "";
                            tbAddress.State_Local = addrList.State_Local ?? "";
                        }
                        else
                        {
                            tbAddress.HouseNo_Inter = (model.HouseNo_Local ?? "").Trim();
                            tbAddress.VillageNo_Inter = (model.VillageNo_Local ?? "").Trim();
                            tbAddress.Lane_Inter = (model.Lane_Local ?? "").Trim();
                            tbAddress.Road_Inter = (model.Road_Local ?? "").Trim();
                            tbAddress.SubDistrict_Inter = (model.SubDistrict_Local ?? "").Trim();
                            tbAddress.City_Inter = (model.City_Local ?? "").Trim();
                            tbAddress.State_Inter = (model.State_Local ?? "").Trim();

                            tbAddress.PostalCode = (model.PostalCode ?? "").Trim();
                            tbAddress.CountryCode = (model.CountryCode ?? "").Trim();

                            tbAddress.HouseNo_Local = (addrList.HouseNo_Local ?? "").Trim();
                            tbAddress.VillageNo_Local = (addrList.VillageNo_Local ?? "").Trim();
                            tbAddress.Lane_Local = (addrList.Lane_Local ?? "").Trim();
                            tbAddress.Road_Local = (addrList.Road_Local ?? "").Trim();
                            tbAddress.SubDistrict_Local = (addrList.SubDistrict_Local ?? "").Trim();
                            tbAddress.City_Local = (addrList.City_Local ?? "").Trim();
                            tbAddress.State_Local = (addrList.State_Local ?? "").Trim();
                        }
                    }
                    else
                    {
                        if (addrList != null)
                        {
                            tbAddress.HouseNo_Inter = addrList.HouseNo_Inter ?? "";
                            tbAddress.VillageNo_Inter = addrList.VillageNo_Inter ?? "";
                            tbAddress.Lane_Inter = addrList.Lane_Inter ?? "";
                            tbAddress.Road_Inter = addrList.Road_Inter ?? "";
                            tbAddress.SubDistrict_Inter = addrList.SubDistrict_Inter ?? "";
                            tbAddress.City_Inter = addrList.City_Inter ?? "";
                            tbAddress.State_Inter = addrList.State_Inter ?? "";

                            tbAddress.PostalCode = model.PostalCode ?? addrList.PostalCode ?? "";
                            tbAddress.CountryCode = model.CountryCode ?? addrList.CountryCode ?? "";

                            tbAddress.HouseNo_Local = model.HouseNo_Local ?? addrList.HouseNo_Local ?? "";
                            tbAddress.VillageNo_Local = model.VillageNo_Local ?? addrList.VillageNo_Local ?? "";
                            tbAddress.Lane_Local = model.Lane_Local ?? addrList.Lane_Local ?? "";
                            tbAddress.Road_Local = model.Road_Local ?? addrList.Road_Local ?? "";
                            tbAddress.SubDistrict_Local = model.SubDistrict_Local ?? addrList.SubDistrict_Local ?? "";
                            tbAddress.City_Local = model.City_Local ?? addrList.City_Local ?? "";
                            tbAddress.State_Local = model.State_Local ?? addrList.State_Local ?? "";
                        }
                        else
                        {
                            tbAddress.HouseNo_Inter = "";
                            tbAddress.VillageNo_Inter = "";
                            tbAddress.Lane_Inter = "";
                            tbAddress.Road_Inter = "";
                            tbAddress.SubDistrict_Inter = "";
                            tbAddress.City_Inter = "";
                            tbAddress.State_Inter = "";

                            tbAddress.PostalCode = (model.PostalCode ?? "").Trim();
                            tbAddress.CountryCode = (model.CountryCode ?? "").Trim();

                            tbAddress.HouseNo_Local = (model.HouseNo_Local ?? "").Trim();
                            tbAddress.VillageNo_Local = (model.VillageNo_Local ?? "").Trim();
                            tbAddress.Lane_Local = (model.Lane_Local ?? "").Trim();
                            tbAddress.Road_Local = (model.Road_Local ?? "").Trim();
                            tbAddress.SubDistrict_Local = (model.SubDistrict_Local ?? "").Trim();
                            tbAddress.City_Local = (model.City_Local ?? "").Trim();
                            tbAddress.State_Local = (model.State_Local ?? "").Trim();
                        }
                    }

                    if (addrList != null)
                    {
                        tbAddress.AddressID = model.AddressID ?? 0;
                        _addressRepository.UpdateAddress(tbAddress, userID);
                    }
                    else
                    {
                        int addrID = _addressRepository.InsertAddressReturnaddressID(tbAddress, userID);
                        tbFacility.AddressID = addrID;
                    }

                    _orgFacilityRepository.UpdateOrgFacility(tbFacility);
                }
                else
                {
                    var tbFacility = new Tbl_OrgFacility();
                    var tbAddress = new Tbl_Address();

                    if (isInter.Equals(1))
                    {
                        tbAddress.HouseNo_Inter = (model.HouseNo_Local ?? "").Trim();
                        tbAddress.VillageNo_Inter = (model.VillageNo_Local ?? "").Trim();
                        tbAddress.Lane_Inter = (model.Lane_Local ?? "").Trim();
                        tbAddress.Road_Inter = (model.Road_Local ?? "").Trim();
                        tbAddress.SubDistrict_Inter = (model.SubDistrict_Local ?? "").Trim();
                        tbAddress.City_Inter = (model.City_Local ?? "").Trim();
                        tbAddress.State_Inter = (model.State_Local ?? "").Trim();
                    }
                    else
                    {
                        tbAddress.HouseNo_Local = (model.HouseNo_Local ?? "").Trim();
                        tbAddress.VillageNo_Local = (model.VillageNo_Local ?? "").Trim();
                        tbAddress.Lane_Local = (model.Lane_Local ?? "").Trim();
                        tbAddress.Road_Local = (model.Road_Local ?? "").Trim();
                        tbAddress.SubDistrict_Local = (model.SubDistrict_Local ?? "").Trim();
                        tbAddress.City_Local = (model.City_Local ?? "").Trim();
                        tbAddress.State_Local = (model.State_Local ?? "").Trim();
                    }

                    tbAddress.AddressID = model.AddressID ?? 0;
                    tbAddress.CountryCode = (model.CountryCode ?? "").Trim();
                    tbAddress.PostalCode = (model.PostalCode ?? "").Trim();

                    var addrID = _addressRepository.InsertAddressReturnaddressID(tbAddress, supplierID);

                    tbFacility.FacilityID = model.FacilityID;
                    tbFacility.FacilityName = (model.FacilityName ?? "").Trim();
                    tbFacility.FacilityTypeID = model.FacilityTypeID;
                    tbFacility.FaxExt = (model.FaxExt ?? "").Trim();
                    tbFacility.FaxNo = (model.FaxNo ?? "").Trim();
                    tbFacility.MobileNo = (model.MobileNo ?? "").Trim();
                    tbFacility.PhoneExt = (model.PhoneExt ?? "").Trim();
                    tbFacility.PhoneNo = (model.PhoneNo ?? "").Trim();
                    tbFacility.WebSite = (model.WebSite ?? "").Trim();
                    tbFacility.AddressID = addrID;
                    tbFacility.SupplierID = model.SupplierID;

                    int? seqno = ORGFacList.Count();
                    if (seqno > 0)
                    {
                        seqno = ORGFacList.OrderByDescending(m => m.SeqNo).FirstOrDefault().SeqNo;
                        seqno = seqno + 1;
                    }
                    else
                    {
                        seqno = 1;
                    }
                    tbFacility.SeqNo = seqno;

                    _orgFacilityRepository.InsertOrgFacility(tbFacility);
                }

                #region Update Tbl_Organization.LastUpdate
                _organizationRepository.UpdateLastUpdate(supplierID, userID);
                #endregion
            }
            return RedirectToAction("Facility");
        }

        [HttpPost]
        public ActionResult DeleteFacility(int? facilityID, int? addressID)
        {
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var model = new CompanyFacilityModel() { };

            int supplierID = Convert.ToInt32(Session["_FacSuppID"]);
            _orgFacilityRepository.DeleteOrgFacility(Convert.ToInt32(facilityID));
            _addressRepository.Delete(Convert.ToInt32(addressID), supplierID);

            return RedirectToAction("Facility");
        }

        public PartialViewResult EditFacility(int facilityID)
        {
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            int isInter = SupplierPortal.Data.Helper.LanguageHelper.GetLanguageIsInter();
            //int supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            int supplierID = Convert.ToInt32(Session["_FacSuppID"]);
            var Organizetion = _organizationRepository.GetDataBySupplierID(supplierID);
            string languageID = GetCurrentLanguageHelper.GetStringLanguageIDCurrent();

            var model = _comProfileDataService.GetDataFacAddressBySupplierID(supplierID, facilityID);

            string countryName = _countryRepository.GetCountryNameByCountryCode(model.CountryCode);

            model.Command = "edit";
            model.SupplierID = supplierID;
            model.CountryCode = (model.CountryCode ?? "").Trim();
            model.CountryName = countryName;
            model.PostalCode = (model.PostalCode ?? "").Trim();
            //model.HouseNo_Local = model.HouseNo_Local;
            //model.VillageNo_Local =model.VillageNo_Local;
            //model.Lane_Local = model.Lane_Local;
            //model.Road_Local =model.Road_Local;
            //model.SubDistrict_Local = model.SubDistrict_Local;
            //model.City_Local = model.City_Local;
            //model.State_Local = model.State_Local;

            model.HouseNo_Local = (isInter == 1 ? model.HouseNo_Inter : model.HouseNo_Local);
            model.VillageNo_Local = (isInter == 1 ? model.VillageNo_Inter : model.VillageNo_Local);
            model.Lane_Local = (isInter == 1 ? model.Lane_Inter : model.Lane_Local);
            model.Road_Local = (isInter == 1 ? model.Road_Inter : model.Road_Local);
            model.SubDistrict_Local = (isInter == 1 ? model.SubDistrict_Inter : model.SubDistrict_Local);
            model.City_Local = (isInter == 1 ? model.City_Inter : model.City_Local);
            model.State_Local = (isInter == 1 ? model.State_Inter : model.State_Local);

            return PartialView(@"~/Views/AdditionalInformation/_FacilityPartial.cshtml", model);
        }

        public PartialViewResult ResetFacility()
        {
            //int supplierID = 0;
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            //supplierID = _organizationRepository.GetSupplierIDByUsername(username);
            //var OrgFacility = _orgFacilityRepository.GetDataBySupplierID(supplierID).FirstOrDefault();

            int supplierID = Convert.ToInt32(Session["_FacSuppID"]);

            var Organizetion = _organizationRepository.GetDataBySupplierID(supplierID);
            string countryName = _countryRepository.GetCountryNameByCountryCode(Organizetion.CountryCode);
            var model = new CompanyFacilityModel() { };
            model.SupplierID = supplierID;
            model.CountryCode = Organizetion.CountryCode;
            model.CountryName = countryName;

            return PartialView(@"~/Views/AdditionalInformation/_FacilityPartial.cshtml", model);
        }

        public ActionResult IsWebsiteValid(string url)
        {
            //var IsUrlValidate = SupplierPortal.Framework.MethodHelper.GetURLStringFormatHelper.URLIsValid(url);

            return Json("");
        }

        public ActionResult YearEstablishValidate(string YearEstablished)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";
            Regex rgx = new Regex(@"^[0-9]+$");
            if (rgx.IsMatch(YearEstablished))
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.NumberOnly.Txt", languageId);
                //  errorMessage = messageReturn.GetStringResource("_Compro.ValidateMsg.DecimalNumberOnly", languageId);
                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        //public ActionResult RegisCapitalValidate(decimal? RegisteredCapital)
        //{
        //    string languageId = "";
        //    string regisCap = System.Convert.ToString(RegisteredCapital);
        //    HttpCookie cultureCookie = Request.Cookies["_culture"];
        //    if (cultureCookie != null)
        //    {
        //        languageId = cultureCookie.Value.ToString();
        //    }
        //    else
        //    {
        //        languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
        //    }

        //    bool IsValid = true;

        //    string messageReturn = "";

        //    string errorMessage = "";
        //    Regex rgx = new Regex(@"^[0-9\.]+$");
        //    if (rgx.IsMatch(regisCap))
        //    {
        //        return Json(IsValid, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        errorMessage = messageReturn.GetStringResource("_Compro.ValidateMsg.DecimalNumberOnly", languageId);
        //      //  errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.NumberOnly.Txt", languageId);
        //        return Json(errorMessage, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //--------------------------------------------------------------------

        //public ActionResult NumberOfEMPValidate(int? NumberOfEMP)
        //{
        //    string languageId = "";

        //    HttpCookie cultureCookie = Request.Cookies["_culture"];
        //    if (cultureCookie != null)
        //    {
        //        languageId = cultureCookie.Value.ToString();

        //    }
        //    else
        //    {
        //        languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
        //    }

        //    bool IsValid = true;

        //    string messageReturn = "";

        //    string errorMessage = "";

        //    if (NumberOfEMP is int)
        //    {
        //        return Json(IsValid, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        errorMessage = messageReturn.GetStringResource("_Regis.ValidateMsg.NumberOnly.Txt", languageId);
        //        return Json(errorMessage, JsonRequestBehavior.AllowGet);
        //    }
        //}
    }
}