﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;

namespace SupplierPortal.Controllers
{
    public class ValidateAjaxController : Controller
    {

        public ActionResult CheckThaiOnly(string input)
        {

            if (IsValidThaiOnlyRegular(input.Trim()))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckEngOnly(string input)
        {

            if (IsValidEngOnlyRegular(input.Trim()))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }



        #region------------------------------------------------Function Helper------------------------------------------------------

        private bool IsValidThaiOnlyRegular(string input)
        {
            string patternThai = @"^[\p{IsThai} \0-9]+$";
            string inputChar; 
            //int index = 0;

            for (int index = 0; index < input.Length; index++)
            {
                //index++;

                inputChar = input[index].ToString();
                Match match = Regex.Match(inputChar, patternThai);

                if (!match.Success)
                {
                    return false;
                }
            }
            return true;
        }

        private bool IsValidEngOnlyRegular(string input)
        {
            string patternEng = @"^[A-Za-z\d\W]+$";
            string inputChar;

            for (int index = 0; index < input.Length; index++)
            {
                //index++;

                inputChar = input[index].ToString();
                Match match = Regex.Match(inputChar, patternEng);

                if (!match.Success)
                {
                    return false;
                }
            }
            return true;
        }

        #endregion
    }
}