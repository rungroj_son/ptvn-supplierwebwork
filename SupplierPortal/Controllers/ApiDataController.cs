﻿using SupplierPortal.Data.Models.SupportModel.RegisterPortal;
using SupplierPortal.Data.Models.SupportModel.JsonAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.Product;
using SupplierPortal.Framework.Localization;
using Newtonsoft.Json;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Framework.JsonHelper;
using SupplierPortal.Data.Models.Repository.AutoComplete_SubDistrict;
using SupplierPortal.Data.Models.Repository.AutoComplete_City;
using SupplierPortal.Data.Models.Repository.AutoComplete_State;

namespace SupplierPortal.Controllers
{
    public class ApiDataController : Controller
    {

        IProductRepository _productRepository;
        ICountryRepository _countryRepository;
        IAutoComplete_SubDistrictRepository _subDistrictRepository;
        IAutoComplete_CityRepository _cityRepository;
        IAutoComplete_StateRepository _stateRepository;

        public ApiDataController()
        {
            _productRepository = new ProductRepository();
            _countryRepository = new CountryRepository();
            _subDistrictRepository = new AutoComplete_SubDistrictRepository();
            _cityRepository = new AutoComplete_CityRepository();
            _stateRepository = new AutoComplete_StateRepository();

        }

         public ApiDataController(
            ProductRepository productRepository,
            AutoComplete_SubDistrictRepository subDistrictRepository,
            AutoComplete_CityRepository cityRepository,
            AutoComplete_StateRepository stateRepository,
            CountryRepository countryRepository

            ) 
        {
            _productRepository = productRepository;
            _subDistrictRepository = subDistrictRepository;
            _cityRepository = cityRepository;
            _stateRepository = stateRepository;
            _countryRepository = countryRepository;

        }

        // GET: ApiData
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchProductandService(FormCollection collection)
        {
            int current =Convert.ToInt32(collection["current"]);
            int rowCount = Convert.ToInt32(collection["rowCount"]);
            var sortProductCode = collection["sort[ProductCode]"];
            var sortProductName = collection["sort[ProductName]"];
            var searchPhrase = collection["searchPhrase"];
            var businessType = collection["businessType"];
            var keySearch = collection["keySearch"];

            string sortOrder = "";

            if (!String.IsNullOrEmpty(sortProductCode))
            {
                if (sortProductCode == "asc")
                {
                    sortOrder = "code_asc";
                }else
                {
                    sortOrder = "code_desc";

                }
            }

            if (!String.IsNullOrEmpty(sortProductName))
            {
                if (sortProductName == "asc")
                {
                    sortOrder = "name_asc";
                }
                else
                {
                    sortOrder = "name_desc";

                }
            }

            //string Jsonstr = "{\"current\": 1,\"rowCount\": 10,\"rows\": [{\"id\": 19,\"sender\": \"123@test.de\",\"received\": \"2014-05-30T22:15:00\"},{\"id\": 14, \"sender\": \"456@test.de\",\"received\": \"2014-05-30T20:15:00\"}],\"total\": 2}";

            //string Jsonstr = "{\"current\": 1,\"rowCount\": 10,\"rows\": [{\"id\": 19,\"sender\": \"123@test.de\",\"received\": \"2014-05-30T22:15:00\"},{\"id\": 14, \"sender\": \"456@test.de\",\"received\": \"2014-05-30T20:15:00\"}],\"total\": 2}";

            var dataSearchProductandService = GetDataSearchProductandService(keySearch.Trim(), current, rowCount, sortOrder);

            string Jsonstr = JsonConvert.SerializeObject(dataSearchProductandService);

            return Json(Jsonstr);
        }

        /// <summary>
        /// The method the ajax select2 query hits to get the attendees to display in the dropdownlist
        /// </summary>
        [HttpGet]
        public ActionResult GetJsonCountry(string searchTerm = "", int pageSize = 10, int pageNum = 1)
        {

            Select2PagedResult jsonCountry = new Select2PagedResult();
            //jsonCountry.Results = new List<Select2Result>();

            List<Tbl_Country> countryModel = _countryRepository.GetCountry(searchTerm, pageSize, pageNum).ToList();

            int countryCount = _countryRepository.GetCountryCount(searchTerm, pageSize, pageNum);

            Select2PagedResult pagedCountry = CountryToSelect2Format(countryModel, countryCount);

            //Return the data as a jsonp result
            return new JsonpResult
            {
                Data = pagedCountry,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }


        public ActionResult SubCompLocalPartial(string term)
        {
            var SubCompLocals = _subDistrictRepository.GetSubDistrictList(term);
            return Json(SubCompLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubCompInterPartial(string term)
        {
            var SubCompInters = _subDistrictRepository.GetSubDistrictList(term);
            return Json(SubCompInters, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubDelvLocalPartial(string term)
        {
            var SubDelvLocals = _subDistrictRepository.GetSubDistrictList(term);
            return Json(SubDelvLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubDelvInterPartial(string term)
        {
            var SubDelvInters = _subDistrictRepository.GetSubDistrictList(term);
            return Json(SubDelvInters, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CityCompLocalPartial(string term)
        {
            var CityCompLocals = _cityRepository.GetCityList(term);
            return Json(CityCompLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CityCompInterPartial(string term)
        {
            var CityCompInters = _cityRepository.GetCityList(term);
            return Json(CityCompInters, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CityDelvLocalPartial(string term)
        {
            var CityDelvLocals = _cityRepository.GetCityList(term);
            return Json(CityDelvLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CityDelvInterPartial(string term)
        {
            var CityDelvInters = _cityRepository.GetCityList(term);
            return Json(CityDelvInters, JsonRequestBehavior.AllowGet);
        }


        public ActionResult StateCompLocalPartial(string term)
        {
            var StateCompLocals = _stateRepository.GetStateList(term);
            return Json(StateCompLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StateCompInterPartial(string term)
        {
            var StateCompInters = _stateRepository.GetStateList(term);
            return Json(StateCompInters, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StateDelvLocalPartial(string term)
        {
            var StateDelvLocals = _stateRepository.GetStateList(term);
            return Json(StateDelvLocals, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StateDelvInterPartial(string term)
        {
            var StateDelvInters = _stateRepository.GetStateList(term);
            return Json(StateDelvInters, JsonRequestBehavior.AllowGet);
        }
        #region------------------------------------------------Function Helper------------------------------------------------------
        private JsonSearchProductandServiceModels GetDataSearchProductandService(string keySearch, int current, int rowCount, string sortOrder)
        {
            HttpCookie cultureCookie = Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();

            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }

            var productSearch = _productRepository.GetSearchProductandService(languageID, keySearch);

            int countProductSearch = productSearch.Count();

            if (rowCount == -1)
            {
                productSearch = productSearch.Skip((current - 1) * rowCount);
            }
            else
            {
                //  productSearch = productSearch.Skip(current - 1).Take(rowCount);
                productSearch = productSearch.Skip((current - 1) * rowCount).Take(rowCount);
            }

            switch (sortOrder)
            {
                //case "code_asc":
                //    productSearch = productSearch.OrderBy(s => s.ProductCode);
                //    break;
                //case "code_desc":
                //    productSearch = productSearch.OrderByDescending(s => s.ProductCode);
                //    break;
                //case "name_asc":
                //    productSearch = productSearch.OrderBy(s => s.ProductName);
                //    break;
                //case "name_desc":
                //    productSearch = productSearch.OrderByDescending(s => s.ProductName);
                //    break;
                //default:
                //    productSearch = productSearch.OrderBy(s => s.ProductCode);
                //    break;

                case "code_asc":
                    productSearch = productSearch.OrderBy(s => s.CategoryID_Lev3);
                    break;
                case "code_desc":
                    productSearch = productSearch.OrderByDescending(s => s.CategoryID_Lev3);
                    break;
                case "name_asc":
                    productSearch = productSearch.OrderBy(s => s.CategoryName);
                    break;
                case "name_desc":
                    productSearch = productSearch.OrderByDescending(s => s.CategoryName);
                    break;
                default:
                    productSearch = productSearch.OrderBy(s => s.CategoryID_Lev3);
                    break;
            }

            var dataSearchProductandService = new JsonSearchProductandServiceModels();

            dataSearchProductandService.current = current;
            dataSearchProductandService.rowCount = rowCount;
            dataSearchProductandService.total = countProductSearch;
            dataSearchProductandService.rows = productSearch.ToList();

            return dataSearchProductandService;
        }


        private Select2PagedResult CountryToSelect2Format(List<Tbl_Country> country, int totalCountry)
        {
            Select2PagedResult jsonCountry = new Select2PagedResult();
            jsonCountry.Results = new List<Select2Result>();

            //Loop through our attendees and translate it into a text value and an id for the select list
            foreach (Tbl_Country a in country)
            {
                jsonCountry.Results.Add(new Select2Result { id = a.CountryCode, text = a.CountryName });
            }
            //Set the total count of the results from the query.
            jsonCountry.Total = totalCountry;

            return jsonCountry;
        }
        #endregion

    }
}