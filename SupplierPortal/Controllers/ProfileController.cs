﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierPortal.Data.Models.Repository.NameTitle;
using SupplierPortal.Data.Models.Repository.Locale;
using SupplierPortal.Data.Models.Repository.TimeZone;
using SupplierPortal.Data.Models.Repository.Currency;
using SupplierPortal.Data.Models.Repository.Language;
using SupplierPortal.Data.Models.Repository.ContactPerson;
using SupplierPortal.Data.Models.Repository.User;
using SupplierPortal.Data.Models.Repository.SystemRole;
using SupplierPortal.Data.Models.Repository.ACL_Role;
using SupplierPortal.Data.Models.Repository.AppConfig;
using SupplierPortal.Data.Models.Repository.JobTitle;
using SupplierPortal.Data.Models.Repository.AutoComplete_State;
using SupplierPortal.Data.Models.Repository.Function;
using SupplierPortal.Data.Models.Repository.Country;
using SupplierPortal.Data.Models.Repository.UserServiceMapping;
using SupplierPortal.Data.Models.Repository.Organization;
using SupplierPortal.Framework.Localization;
using SupplierPortal.Services.ProfileManage;
using SupplierPortal.Data.Models.SupportModel.Profile;
using SupplierPortal.Framework.AccountManage;
using SupplierPortal.Services.AccountManage;
using Newtonsoft.Json;
using SupplierPortal.Data.Models.Repository.UserSystemMapping;
using SupplierPortal.Data.Models.Repository.SystemConfigureAPI;
using System.Threading.Tasks;
using SupplierPortal.Data.Models.SupportModel;
using SupplierPortal.Data.Models.Repository.ACL_OrgMenuMapping;
using SupplierPortal.Data.Models.Repository.OPNBuyer;
using SupplierPortal.ServiceClients;

namespace SupplierPortal.Controllers
{
    public class ProfileController : ControllerBase
    {
        private INameTitleRepository _nameTitleRepository;
        private ILocaleRepository _localeRepository;
        private ITimeZoneRepository _timeZoneRepository;
        private ICurrencyRepository _currencyRepository;
        private ILanguageRepository _languageRepository;
        private IContactPersonRepository _contactPersonRepository;
        private IProfileDataService _profileDataService;
        private IUserRepository _userRepository;
        private ISystemRepository _systemRepository;
        private IACL_RoleRepository _acl_RoleRepository;
        private IAppConfigRepository _appConfigRepository;
        private IJobTitleRepository _jobTitleRepository;
        private IAutoComplete_StateRepository _autoComplete_StateRepository;
        private IFunctionRepository _functionRepository;
        private ICountryRepository _countryRepository;
        private IUserServiceMappingRepository _userServiceMappingRepository;
        private IProfileTransactions _profileTransactions;
        private IOrganizationRepository _organizationRepository;
        private IUserSystemMappingRepository _userSystemMappingRepository;
        private ISystemConfigureAPIRepository _systemConfigureAPIRepository;
        private IACL_OrgMenuMappingRepository _ACL_OrgMenuMappingRepository;
        private IOPNBuyerRepository _opnBuyerRepository;
        private OPNSupplierService _opnSupplierService;

        public ProfileController()
        {
            _nameTitleRepository = new NameTitleRepository();
            _localeRepository = new LocaleRepository();
            _timeZoneRepository = new TimeZoneRepository();
            _currencyRepository = new CurrencyRepository();
            _languageRepository = new LanguageRepository();
            _contactPersonRepository = new ContactPersonRepository();
            _profileDataService = new ProfileDataService();
            _userRepository = new UserRepository();
            _systemRepository = new SystemRepository();
            _acl_RoleRepository = new ACL_RoleRepository();
            _appConfigRepository = new AppConfigRepository();
            _jobTitleRepository = new JobTitleRepository();
            _autoComplete_StateRepository = new AutoComplete_StateRepository();
            _functionRepository = new FunctionRepository();
            _countryRepository = new CountryRepository();
            _userServiceMappingRepository = new UserServiceMappingRepository();
            _profileTransactions = new ProfileTransactions();
            _organizationRepository = new OrganizationRepository();
            _userSystemMappingRepository = new UserSystemMappingRepository();
            _systemConfigureAPIRepository = new SystemConfigureAPIRepository();
            _ACL_OrgMenuMappingRepository = new ACL_OrgMenuMappingRepository();
            _opnBuyerRepository = new OPNBuyerRepository();
            _opnSupplierService = new OPNSupplierService();
        }

        public ProfileController(
            NameTitleRepository nameTitleRepository,
            LocaleRepository localeRepository,
            TimeZoneRepository timeZoneRepository,
            CurrencyRepository currencyRepository,
            LanguageRepository languageRepository,
            ContactPersonRepository contactPersonRepository,
            ProfileDataService profileDataService,
            UserRepository userRepository,
            SystemRepository systemRepository,
            ACL_RoleRepository acl_RoleRepository,
            AppConfigRepository appConfigRepository,
            JobTitleRepository jobTitleRepository,
            AutoComplete_StateRepository autoComplete_StateRepository,
            FunctionRepository functionRepository,
            CountryRepository countryRepository,
            UserServiceMappingRepository userServiceMappingRepository,
            ProfileTransactions profileTransactions,
            OrganizationRepository organizationRepository,
            UserSystemMappingRepository userSystemMappingRepository,
            SystemConfigureAPIRepository systemConfigureAPIRepository,
            OPNBuyerRepository opnBuyerRepository,
            OPNSupplierService opnSupplierService
            )
        {
            _nameTitleRepository = nameTitleRepository;
            _localeRepository = localeRepository;
            _timeZoneRepository = timeZoneRepository;
            _currencyRepository = currencyRepository;
            _languageRepository = languageRepository;
            _contactPersonRepository = contactPersonRepository;
            _profileDataService = profileDataService;
            _userRepository = userRepository;
            _systemRepository = systemRepository;
            _acl_RoleRepository = acl_RoleRepository;
            _appConfigRepository = appConfigRepository;
            _jobTitleRepository = jobTitleRepository;
            _autoComplete_StateRepository = autoComplete_StateRepository;
            _functionRepository = functionRepository;
            _countryRepository = countryRepository;
            _userServiceMappingRepository = userServiceMappingRepository;
            _profileTransactions = profileTransactions;
            _organizationRepository = organizationRepository;
            _userSystemMappingRepository = userSystemMappingRepository;
            _systemConfigureAPIRepository = systemConfigureAPIRepository;
            _opnBuyerRepository = opnBuyerRepository;
            _opnSupplierService = opnSupplierService;
        }

        // GET: Profile
        public ActionResult Index(string errorMessage = "", string tab = "")
        {
            HttpCookie cultureCookie = Request.Cookies["_culture"];

            string languageID = "";

            if (cultureCookie != null)
            {
                languageID = cultureCookie.Value.ToString();
            }
            if (string.IsNullOrEmpty(languageID))
            {
                languageID = CultureHelper.GetImplementedCulture(languageID); // This is safe
            }
            int languageId = Convert.ToInt32(languageID);

            var languageLocal = _languageRepository.GetLocalLanguage();

            var languageInter = _languageRepository.GetInterLanguage();

            ViewBag.NameTitle_Local = _nameTitleRepository.GetNameTitleList(languageLocal.LanguageID);

            ViewBag.NameTitle_Inter = _nameTitleRepository.GetNameTitleList(languageInter.LanguageID);

            ViewBag.JobTitle = _jobTitleRepository.GetJobTitleList(languageId);

            ViewBag.Locale = _localeRepository.LocaleList();

            ViewBag.TimeZone = _timeZoneRepository.TimeZoneList();

            ViewBag.Currency = _currencyRepository.CurrencyList().Where(w => new[] { "THB", "USD", "VND" }.Any(s => s == w.CurrencyCode));

            ViewBag.Language = _languageRepository.ListOrderByAsc();

            ViewBag.ErrorMessage = errorMessage;

            ViewBag.Tab = tab;

            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            ViewBag.OrgID = _userRepository.FindOrgIDByUsername(username);

            ViewBag.CerrentUsername = username;

            var models = _profileDataService.GetProfileData(username);

            models.ChangePasswordData = new ChangePasswordModels();

            // Check  Permission of  _PartialGeneral, _PartialChangePassword, MergUserAccountPartial tab
            int supplierID = _organizationRepository.GetSupplierIDByUsername(username);

            bool isAllowAccess_Profile_GeneralProfile = _ACL_OrgMenuMappingRepository.CheckAllowAccessInProfileMenuBySupplierIDAndMenuID(supplierID, 43);
            bool isAllowAccess_Profile_ChangePassword = _ACL_OrgMenuMappingRepository.CheckAllowAccessInProfileMenuBySupplierIDAndMenuID(supplierID, 44);
            bool isAllowAccess_MergerUser = _ACL_OrgMenuMappingRepository.CheckAllowAccessInProfileMenuBySupplierIDAndMenuID(supplierID, 45);
            ViewBag.isAllowAccess_Profile_GeneralProfile = isAllowAccess_Profile_GeneralProfile;
            ViewBag.isAllowAccess_Profile_ChangePassword = isAllowAccess_Profile_ChangePassword;
            ViewBag.isAllowAccess_MergerUser = isAllowAccess_MergerUser;
            ViewData["suuplierId"] = supplierID;

            return View(models);
        }


        [HttpPost]
        public ActionResult CheckInvitationCode(string invitationCode)
        {
            var invitationCodeValid = false;
            if (!String.IsNullOrEmpty(invitationCode))
            {
                bool result = _opnBuyerRepository.verifyInvitationCode(invitationCode);
                if (result)
                {
                    invitationCodeValid = true;
                }
            }
            return Json(invitationCodeValid);
        }

        [HttpPost]
        public async Task<ActionResult> insertBuyerSupplierMappingByInvitation(string invitationCode, int sid)
        {
            bool isSuccess = false;
                      
            if (!String.IsNullOrEmpty(invitationCode) && sid != 0)
            {
                Tbl_OPNBuyerSupplierMappingViewModels request = new Tbl_OPNBuyerSupplierMappingViewModels
                {
                    InvitationCode = invitationCode,
                    SupplierID = sid
                };

                bool result = _opnBuyerRepository.verifyInvitationCode(invitationCode);
                if (result)
                {
                    isSuccess = await _opnSupplierService.InsertOPNBuyerSupplierMapping(request);
                }
            }
            return Json(isSuccess);
        }

        public ActionResult UserInformation(string errorMessage = "", string tab = "", string UsernameFilter = "")
        {
            ViewBag.UsernameFilter = UsernameFilter;

            return View();
        }

        public ActionResult PostalCodeValidateCompany(string Address_PostalCode, string Address_CountryCode)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            bool IsValid = true;

            string messageReturn = "";

            string errorMessage = "";
            if (string.IsNullOrEmpty(Address_PostalCode))
            {
                Address_PostalCode = "";
            }
            if (string.IsNullOrEmpty(Address_CountryCode))
            {
                Address_CountryCode = "";
            }

            var sqlQuery = "select dbo.VerifyPostalCode('" + Address_PostalCode.ToString() + "','" + Address_CountryCode.ToString() + "')";
            //IFunctionRepository _repo = new FunctionRepository();

            var result = _functionRepository.GetResultByQuery(sqlQuery);
            _functionRepository.Dispose();
            if (result)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_Profile.ValidateMsg.InvalidFormatPostalCode", languageId);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AvailableServices(int UserID)
        {
            var model = _userServiceMappingRepository.GetUserServiceAvailableByUserID(UserID);

            //return PartialView("_AvailableServicesPartial", model);
            return PartialView(@"~/Views/Profile/_AvailableServicesPartial.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateGeneral(GeneralModels model)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            string messageReturn = "";

            string errorMessage = "";

            try
            {
                string userUpdate = "";

                if (Session["username"] != null)
                {
                    userUpdate = Session["username"].ToString();
                }

                model.UserUpdate = userUpdate;

                //errorMessage = await _profileDataService.UpdateGeneralData2(model);
                errorMessage = _profileDataService.UpdateGeneralData(model);

                //Update Tbl_Organization.LastUpdate
                var user = _userRepository.FindUserByUserID(model.UserID ?? 0);
                if (user != null)
                {
                    int supplierID = _organizationRepository.GetSupplierIDByUsername(user.Username);
                    _organizationRepository.UpdateLastUpdate(supplierID, user.UserID);
                }
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            if (!string.IsNullOrEmpty(errorMessage))
            {
                string errorMessageResource = "";

                errorMessageResource = messageReturn.GetStringResource("_Profile.ErrorMsg.UpdateSysUserFail", languageId);

                errorMessage = errorMessage.Substring(0, errorMessage.Length - 1);

                errorMessage = errorMessageResource.Replace("<%SystemName%>", errorMessage);

                return RedirectToAction("Index", "Profile", new { errorMessage = errorMessage, tab = "General" });
            }

            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        public ActionResult IsEmailAvailableConfig(string email, string initialEmail)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            var IsAllowDuplicateEmail = _appConfigRepository.GetAppConfigByName("Portal_IsAllowDuplicateUserEmail").Value;

            var IsAllowMultipleEmail = _appConfigRepository.GetAppConfigByName("Portal_IsAllowMultipleUserEmail").Value;

            string messageReturn = "";

            bool IsValid = false;
            if (IsAllowDuplicateEmail != "1")
            {
                bool chk = _contactPersonRepository.EmailExists(email.Trim(), initialEmail.Trim());

                if (!chk)
                {
                    IsValid = true;
                }
                else
                {
                    string errorMessage = messageReturn.GetStringResource("_Profile.ValidateMsg.DuplicateEmail", languageId);
                    return Json(errorMessage, JsonRequestBehavior.AllowGet);
                }
            }

            if (IsAllowMultipleEmail == "1")
            {
                string emailSeparator = _appConfigRepository.GetAppConfigByName("Portal_EmailSeparator").Value;

                foreach (string value in email.Split(Char.Parse(emailSeparator)))
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        IsValid = false;
                        string errorMessage = messageReturn.GetStringResource("_Profile.ValidateMsg.InvalidFormatEmail", languageId);
                        return Json(errorMessage, JsonRequestBehavior.AllowGet);
                    }
                    string pattern = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

                    //if (IsValidEmailAddress(value))
                    if (IsValidEmailAddressRegular(value, pattern))
                    {
                        IsValid = true;
                    }
                    else
                    {
                        string errorMessage = messageReturn.GetStringResource("_Profile.ValidateMsg.InvalidFormatEmail", languageId);
                        return Json(errorMessage, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                string pattern = @"[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";

                //if (IsValidEmailAddress(value))
                if (IsValidEmailAddressRegular(email, pattern))
                {
                    IsValid = true;
                }
                else
                {
                    string errorMessage = messageReturn.GetStringResource("_Profile.ValidateMsg.InvalidFormatEmail", languageId);
                    return Json(errorMessage, JsonRequestBehavior.AllowGet);
                }
            }

            if (IsValid)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            return Json(messageReturn, JsonRequestBehavior.AllowGet);
        }

        private bool IsValidEmailAddress(string emailAddress)
        {
            return new System.ComponentModel.DataAnnotations
                                .EmailAddressAttribute()
                                .IsValid(emailAddress);
        }

        private bool IsValidEmailAddressRegular(string emailAddress, string paatten)
        {
            return new System.ComponentModel.DataAnnotations
                .RegularExpressionAttribute(paatten)
                .IsValid(emailAddress);
        }

        public ActionResult IsEmailAvailable(string email, string initialEmail)
        {
            bool chk = _contactPersonRepository.EmailExists(email.Trim(), initialEmail.Trim());

            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            string messageReturn = "";

            if (!chk)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            string errorMessage = messageReturn.GetStringResource("_Profile.ErrorMessage.EmailAvailable", languageId);

            return Json(errorMessage, JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsOldPasswordMatch(string oldPassword)
        {
            string strUserName = "";

            if (Session["username"] != null)
            {
                strUserName = Session["username"].ToString();
            }

            bool chk = CheckLogin.Login(strUserName, oldPassword);

            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            if (chk)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            string messageReturn = "";
            string errorMessage = messageReturn.GetStringResource("_Profile.ValidateMsg.InvalidPassword", languageId);

            return Json(errorMessage, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModels model)
        {
            try
            {
                string strUserName = "";

                if (Session["username"] != null)
                {
                    strUserName = Session["username"].ToString();
                }

                RecoveryPassword.ResetPassword(strUserName, model.Password);
            }
            catch (Exception ex)
            {
                Response.Redirect("~/Error/Error?ErrorMessage=" + ex.Message);
            }

            return Redirect(ControllerContext.HttpContext.Request.UrlReferrer.ToString());
        }

        public ActionResult GridViewUserinfotmationPart()
        {
            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            ViewBag.OrgID = _userRepository.FindOrgIDByUsername(username);

            //return PartialView("_GridViewPartial_UserInformation");
            return PartialView(@"~/Views/Profile/_GridViewPartial_UserInformation.cshtml");
        }

        public ActionResult GridViewUserinfotmationPartDT(DTParameters param)
        {
            #region Find Organization ID

            // Get username from session
            var username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            // Find org id
            var ordId = _userRepository.FindOrgIDByUsername(username);

            #endregion Find Organization ID

            #region Filter, Order by, Paging

            // Get all parameters
            var search = param.Search.Value;
            var sortColumn = param.Order[0].Column;
            var sortDirection = param.Order[0].Dir;

            // All rows
            var all = SupplierPortal.Services.ProfileManage.ProfileManage.UserInformationList(ordId);

            // Filtered
            var filtered = all;
            if (!string.IsNullOrEmpty(search))
            {
                //filtered = all.Where(x => x.Fullname.Contains(search) ||
                //                          x.Username.Contains(search) ||
                //                          x.JobTitleName.Contains(search) ||
                //                          x.Tel.Contains(search) ||
                //                          x.Mobile.Contains(search) ||
                //                          x.Email.Contains(search) ||
                //                          x.OPRoleName.Contains(search));

                var temp = all.ToList().Where(x => x.Fullname.Contains(search) ||
                                                   x.Username.Contains(search) ||
                                                   x.JobTitleName.Contains(search) ||
                                                   x.Tel.Contains(search) ||
                                                   x.Mobile.Contains(search) ||
                                                   x.Email.Contains(search) ||
                                                   x.OPRoleName.Contains(search)).ToList();

                filtered = temp.AsQueryable();
            }

            // Sort order
            var sort = filtered;
            switch (sortColumn)
            {
                //case 0:
                //    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderBy(x => x.Fullname) : filtered.OrderBy(x => x.Fullname);
                //    break;
                case 1:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.Username) : filtered.OrderBy(x => x.Username);
                    break;

                case 2:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.JobTitleName) : filtered.OrderBy(x => x.JobTitleName);
                    break;

                case 3:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.Tel) : filtered.OrderBy(x => x.Tel);
                    break;

                case 4:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.Mobile) : filtered.OrderBy(x => x.Mobile);
                    break;

                case 5:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.Email) : filtered.OrderBy(x => x.Email);
                    break;

                case 7:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.OPRoleName) : filtered.OrderBy(x => x.OPRoleName);
                    break;

                default:
                    sort = (sortDirection == DTOrderDir.DESC) ? filtered.OrderByDescending(x => x.TitleContactName + x.Fullname) : filtered.OrderBy(x => x.TitleContactName + x.Fullname);
                    break;
            }

            // Paging
            var result = sort.Skip(param.Start).Take(param.Length);

            #endregion Filter, Order by, Paging

            #region Return to Client JSON Format

            return Json(new
            {
                draw = param.Draw,
                recordsTotal = all.Count(),
                recordsFiltered = filtered.Count(),
                data = result,
                error = ""
            }, JsonRequestBehavior.AllowGet);

            #endregion Return to Client JSON Format
        }

        [HttpPost]
        public ActionResult PostCheckIsUserSystemMappingByUserID(string strUserId)
        {
            var userId = -1;
            int.TryParse(strUserId, out userId);
            var result = SupplierPortal.Services.ProfileManage.ProfileManage.CheckIsUserSystemMappingByUserID(userId, 1);

            return Json(result);
        }

        [HttpPost]
        public ActionResult PostCheckIsUserSystemMappingEnableServiceByUserID(string strUserId)
        {
            var userId = -1;
            int.TryParse(strUserId, out userId);
            var result = SupplierPortal.Services.ProfileManage.ProfileManage.CheckIsUserSystemMappingEnableServiceByUserID(userId, 1);

            return Json(result);
        }

        public ActionResult IsUsernameAvailable(string UserLoginID, string orgID)
        {
            string username = orgID.Trim() + "-" + UserLoginID.Trim();
            bool chk = _userRepository.UsernameExists(username);

            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            string messageReturn = "";

            if (!chk)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            string errorMessage = messageReturn.GetStringResource("_Profile.ValidateMsg.DuplicateUsername", languageId);

            return Json(errorMessage, JsonRequestBehavior.AllowGet);
        }

        public ActionResult IsUsernameAvailableConfig(string UserLoginID, string orgID)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            bool IsValid = false;

            string messageReturn = "";

            string errorMessage = "";

            string username = orgID.Trim() + "-" + UserLoginID.Trim();

            string pattern = @"([a-zA-Z0-9_-]+)";

            if (IsValidUsernameRegular(username, pattern))
            {
                IsValid = true;
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_Profile.ValidateMsg.InvalidFormatUsername", languageId);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }

            bool chk = _userRepository.UsernameExists(username);

            if (!chk)
            {
                IsValid = true;
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_Profile.ValidateMsg.DuplicateUsername", languageId);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }

            string usernameLength = _appConfigRepository.GetAppConfigByName("Portal_UsernameLength").Value;

            if (username.Length > Convert.ToInt32(usernameLength))
            {
                errorMessage = messageReturn.GetStringResource("_Profile.ValidateMsg.UsernameOverLength", languageId);

                return Json(errorMessage, JsonRequestBehavior.AllowGet);
            }

            if (IsValid)
            {
                return Json(IsValid, JsonRequestBehavior.AllowGet);
            }
            return Json(messageReturn, JsonRequestBehavior.AllowGet);
        }

        private bool IsValidUsernameRegular(string username, string paatten)
        {
            return new System.ComponentModel.DataAnnotations
                .RegularExpressionAttribute(paatten)
                .IsValid(username);
        }

        [HttpPost]
        public ActionResult LoadDataContactPerson(string contactId)
        {
            int contactID = Convert.ToInt32(contactId);
            var result = _contactPersonRepository.JsonContactPersonByContectID(contactID);

            if (result == null)
                return Json(null);

            //string Jsonstr = JsonConvert.SerializeObject(result);

            return Json(result);
        }

        [HttpPost]
        public ActionResult SaveBSPUserPreference(BSP_UserPreferenceModels model)
        {
            string languageId = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            string usernameCurrent = "";
            string usernameUpdate = "";
            string errorMessage = "";

            if (Session["username"] != null)
            {
                usernameCurrent = Session["username"].ToString();
            }

            model.UsernameCurrent = usernameCurrent;

            var user = _userRepository.FindUserByUserID(model.UserID);

            if (user != null)
            {
                usernameUpdate = user.Username;
            }

            SystemRoleListModels systemRole = new SystemRoleListModels();

            var userSystemMapping = _userSystemMappingRepository.GetUserSystemMapping(user.Username, 1); // 1= system BSP

            //เชคว่า Username นี้ มี map กับ system BSP หรือยัง ถ้ายังจะ call api InsertUser ถ้ามีแล้ว call api UpdateUser
            if (userSystemMapping.Count() > 0)
            {
                systemRole = _systemConfigureAPIRepository.GetSystemRoleAPIBySystemIDAndAPI_Name(1, "UpdateUser"); // 1= system BSP
            }
            else
            {
                systemRole = _systemConfigureAPIRepository.GetSystemRoleAPIBySystemIDAndAPI_Name(1, "InsertUser"); // 1= system BSP
            }

            if (systemRole != null)
            {
                systemRole.Username = usernameUpdate;
                systemRole.IsChecked = model.CheckedOP;
                systemRole.SysRoleID = model.SysRoleID;
            }

            model.SystemRole = systemRole;

            errorMessage = _profileDataService.CallAPIInsertOrUpdateBSPUserPreference(model, languageId);

            _profileDataService.CallAPIAddNotification(model);

            if (!string.IsNullOrEmpty(errorMessage))
            {
                return Json(new { Response = "UnSuccess", Status = false, Message = errorMessage });
            }

            _profileDataService.UpdateSessionData(model);

            return Json(new { Response = "Success", Status = true, Message = errorMessage });
        }

        [HttpPost]
        public ActionResult EditBSPUserPreference(BSP_UserPreferenceModels model)
        {
            try
            {
                _profileDataService.UpdateSessionData(model);

                return Json(new { Response = "Success", Status = true, Message = "" });
            }
            catch (Exception ex)
            {
                return Json(new { Response = "UnSuccess", Status = false, Message = ex.Message });
                //throw;
            }
        }

        [HttpPost]
        public ActionResult UpdateSessionDataMyAccount(BSP_UserPreferenceModels model)
        {
            _profileDataService.UpdateSessionData(model);

            return Json(new { Response = "Success", Status = true, Message = "" });
        }

        public ActionResult MergUserAccountPartial()
        {
            string fullname = "";

            int isInter = SupplierPortal.Data.Helper.LanguageHelper.GetLanguageIsInter();

            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var user = _userRepository.FindByUsername(username);

            var model = new MergUserAccountModels();

            if (user != null)
            {
                var contactPerson = _contactPersonRepository.GetContactPersonByContectID(user.ContactID ?? 0);
                if (contactPerson != null)
                {
                    if (isInter == 1)
                    {
                        fullname = contactPerson.FirstName_Inter + "  " + contactPerson.LastName_Inter;
                    }
                    else
                    {
                        fullname = contactPerson.FirstName_Local + "  " + contactPerson.LastName_Local;
                    }

                    model.Email = contactPerson.Email;
                    model.Username = user.Username;
                    model.UserID = user.UserID;
                    model.Name = fullname;
                }
            }

            //return PartialView("_PartialMergUserAccount", model);
            return PartialView(@"~/Views/Profile/_PartialMergUserAccount.cshtml", model);
        }

        [HttpPost]
        public ActionResult CheckAuthenMergUser(AuthenMergUserModels model)
        {
            string languageId = "";
            string errorMessage = "";
            string messageReturn = "";

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            List<UserMergModels> userMergRows = JsonConvert.DeserializeObject<List<UserMergModels>>(model.UserMerg);

            #region Check AuthenUser Is Merg In Org

            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }
            var userCurrent = _userRepository.FindByUsername(username);
            var userAuthen = _userRepository.FindAllByUsername(model.Username);
            if (userAuthen != null)
            {
                if (userAuthen.OrgID == userCurrent.OrgID)
                {
                    if ((userAuthen.MergeToUserID != 0) && (userAuthen.MergeToUserID != null))
                    {
                        int mergeToUserID = userAuthen.MergeToUserID ?? 0;
                        var userMergTo = _userRepository.FindUserByUserID(mergeToUserID);

                        string usernameMergTo = "";
                        if (userMergTo != null)
                        {
                            usernameMergTo = userMergTo.Username;

                            errorMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.ValidateMsg.UsernameAlreadyMerge", languageId);

                            errorMessage = errorMessage.Replace("<%Username%>", usernameMergTo);

                            var modelResponse = new AuthenMergUserModels()
                            {
                                Status = false,
                                Message = errorMessage
                            };
                            return Json(modelResponse);
                        }
                    }
                }
            }
            #endregion Check AuthenUser Is Merg In Org

            if (CheckLogin.Login(model.Username, model.Password))
            {
                if (_profileDataService.CheckSupplierMergUserInCurrentUser(model.CurrentUsername.Trim(), model.Username.Trim()))
                {
                    //foreach (var userMerg in userMergRows)
                    //{
                    //    if (_profileDataService.CheckUserMappingBSPInGroup(userMerg.UserMergList.Trim(), model.Username.Trim()))
                    //    {
                    //        errorMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.ValidateMsg.MergeBSPTogether", languageId);

                    //        var modelResponse = new AuthenMergUserModels()
                    //        {
                    //            Status = false,
                    //            Message = errorMessage
                    //        };
                    //        return Json(modelResponse);
                    //    }
                    //}

                    var user = _userRepository.FindByUsername(model.Username.Trim());

                    if (user != null)
                    {
                        string htmlText = GetHtmlTextUserMergByUsername(model.Username.Trim());

                        var modelResponse = new AuthenMergUserModels()
                        {
                            Status = true,
                            HtmlText = htmlText
                        };
                        return Json(modelResponse);
                    }
                }
                else
                {
                    errorMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.ValidateMsg.MergeDifferenceSupplier", languageId);

                    var modelResponse = new AuthenMergUserModels()
                    {
                        Status = false,
                        Message = errorMessage
                    };
                    return Json(modelResponse);
                }
                return Json(model);
            }
            else
            {
                errorMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.ValidateMsg.InvalidLogin", languageId);

                var modelResponse = new AuthenMergUserModels()
                {
                    Status = false,
                    Message = errorMessage
                };
                return Json(modelResponse);
            }

            return Json(new { Response = "Error" });
        }

        [HttpPost]
        public ActionResult MergUserAccount(MergAccountModels model)
        {
            string languageId = "";
            string errorMessage = "";
            string messageReturn = "";
            string returnMessage = "";

            string username = "";
            if (Session["username"] != null)
            {
                username = Session["username"].ToString();
            }

            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
            {
                languageId = cultureCookie.Value.ToString();
            }
            else
            {
                languageId = CultureHelper.GetImplementedCulture(languageId);// This is safe
            }

            try
            {
                List<UserMergModels> userMergRows = JsonConvert.DeserializeObject<List<UserMergModels>>(model.UserMergList);

                var itemtoremove = userMergRows.Where(item => item.UserMergList == model.MasterUsernameMerg).First();
                userMergRows.Remove(itemtoremove);

                //var resultMerge = await _profileTransactions.MergUserAccount2(model.MasterUsernameMerg, userMergRows, username);
                //returnMessage = resultMerge.ReturnMessage;
                //if (resultMerge.isSuccess)
                if (_profileTransactions.MergUserAccount(model.MasterUsernameMerg, userMergRows, username, out returnMessage))
                {
                    errorMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountSuccess", languageId);
                    //errorMessage = messageReturn;

                    var modelResponse = new MergAccountModels()
                    {
                        Status = true,
                        Message = errorMessage
                    };
                    return Json(modelResponse);
                }
                else
                {
                    //errorMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);
                    if (string.IsNullOrEmpty(returnMessage))
                    {
                        returnMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergAccountFail", languageId);
                    }
                    errorMessage = returnMessage;

                    var modelResponse = new MergAccountModels()
                    {
                        Status = false,
                        Message = errorMessage
                    };
                    return Json(modelResponse);
                }
            }
            catch (Exception ex)
            {
                errorMessage = messageReturn.GetStringResource("_Profile.TabMergUserAccount.InfoMsg.MergException", languageId);

                var modelResponse = new MergAccountModels()
                {
                    Status = false,
                    Message = errorMessage
                };
                return Json(modelResponse);
            }

            return Json(new { Status = false, Message = "Error" });
        }

        #region------------------------------------------------Function Helper------------------------------------------------------

        private string GetHtmlTextUserMergByUsername(string username)
        {
            string htmlTextTable = "";

            var user = _userRepository.FindByUsername(username);

            int isInter = SupplierPortal.Data.Helper.LanguageHelper.GetLanguageIsInter();

            var contactPerson = _contactPersonRepository.GetContactPersonByContectID(user.ContactID ?? 0);
            if (contactPerson != null)
            {
                string fullname = "";
                if (isInter == 1)
                {
                    fullname = contactPerson.FirstName_Inter + "  " + contactPerson.LastName_Inter;
                }
                else
                {
                    fullname = contactPerson.FirstName_Local + "  " + contactPerson.LastName_Local;
                }

                string linkDelete = "<input type=\"image\" src=\"/Content/images/icon/ico-sp_deluser2.gif\" alt=\"Submit\" id=\"deleteUserMerg\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Delete file\" >";

                htmlTextTable += "<tr class=\"main_table_row numbered_row\" id=\"" + user.Username + "\">";
                htmlTextTable += "<td align=\"center\" class=\"row_number\"></td>";
                htmlTextTable += "<td align=\"center\">" + user.Username + "</td>";
                htmlTextTable += "<td align=\"left\">" + fullname + "</td>";
                htmlTextTable += "<td align=\"left\">" + contactPerson.Email + "</td>";
                htmlTextTable += "<td align=\"center\">";
                htmlTextTable += "<input type=\"checkbox\" name=\"chk_" + user.UserID + "\" id=\"chk_" + user.Username + "\" value=\"true\" class=\"radio\" , onclick = \"chkMerg(this);\" />";
                htmlTextTable += "</td>";
                htmlTextTable += "<td align=\"center\">" + linkDelete + "</td>";
                htmlTextTable += "</tr>";
            }

            return htmlTextTable;
        }

        //public async Task<string> UpdateGeneralDataTaskAsync(GeneralModels model)
        //{
        //    string errorMessage = "";

        //    errorMessage = await _profileDataService.UpdateGeneralData2(model);

        //    return errorMessage;
        //}
        #endregion
    }
}