﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using PISDataAccess;

namespace SupplierPortal.ReportSupport
{
    public class ClsReportChart
    {

        #region Method

        #region Filter Chart

        //Get Buyer Group By Supplier Short Name
        public DataTable getDtBuyerGrp(string SuppShortName)
        {
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            DataTable dtBuyerGrp;
            try
            {
                oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);
                SqlParameter param1_1 = new SqlParameter("@SuppShortName", SuppShortName);
                dtBuyerGrp = oDBCtrl.executeQueryStoreProc("RptMappingPartner_Sel", param1_1).Tables["temp"];

                return dtBuyerGrp;

            }
            catch (Exception ex)
            {
              throw ex;
            }
            finally
            {
                oDBCtrl.disConnect();
            }

        }

        //Get Years
        public DataTable getDtYear()
        {
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            DataTable dtYear;
            try
            {

                oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);
                dtYear = oDBCtrl.executeQueryStoreProc("Year_Sel").Tables["temp"];

                return dtYear;

            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oDBCtrl.disConnect();
            }

        }

        #endregion

        #region Chart Data

        //Get Chart Caption
        //public DataTable getDtChartInfo(int chartID)
        //{
        //    ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
        //    DataTable dtChartInfo;
        //    try
        //    {
        //        oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);
        //        SqlParameter param1_1 = new SqlParameter("@ChartID", chartID);
        //        dtChartInfo = oDBCtrl.executeQueryStoreProc("ChartsCaption_Sel", param1_1).Tables["temp"];
        //        return dtChartInfo;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        oDBCtrl.disConnect();
        //    }


        //}

        //public DataTable getDtChartInfo2(int chartID,int LanguageID)
        //{
        //    ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
        //    DataTable dtChartInfo;
        //    try
        //    {
        //        oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);
        //        SqlParameter param1_1 = new SqlParameter("@ChartID", chartID);
        //        SqlParameter param1_2 = new SqlParameter("@LanguageID", LanguageID);
        //        dtChartInfo = oDBCtrl.executeQueryStoreProc("ChartsCaption_Sel2", param1_1, param1_2).Tables["temp"];
        //        return dtChartInfo;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        oDBCtrl.disConnect();
        //    }


        //}
      
        //Get Report Last Update
        public DataTable getRptLastUpdate(int rptID)
        {
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            try
            {
                oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);
                DataTable dtRptLastUpdate = new DataTable();
                SqlParameter param1_1 = new SqlParameter("@ReportID", rptID);
                dtRptLastUpdate = oDBCtrl.executeQueryStoreProc("RptLastUpdate_Sel", param1_1).Tables["Temp"];
                return dtRptLastUpdate;
            }
            catch (Exception ex)
            {

                throw ex;

            }
            finally
            {
                oDBCtrl.disConnect();
            }
        } 

        #region NumPO

        public DataTable getDtNumPO(string suppShortName, string buyerGroupName, int year)
        {
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            DataTable dtNumPO;
            try
            {
                oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);

                SqlParameter param1_1 = new SqlParameter("@SuppShortName", suppShortName);
                SqlParameter param1_2 = new SqlParameter("@BuyerGroupName", buyerGroupName);
                SqlParameter param1_3 = new SqlParameter("@Year", year);

                dtNumPO = oDBCtrl.executeQueryStoreProc("NumPO_Sel", param1_1, param1_2, param1_3).Tables["temp"];
                return dtNumPO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oDBCtrl.disConnect();
            }

        }

        #endregion

        #region POVolume

        public DataTable getDtPOVolume(string suppShortName, string buyerGroupName, int year)
        {
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            DataTable dtPOVolume;
            try
            {
                oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);

                SqlParameter param1_1 = new SqlParameter("@SuppShortName", suppShortName);
                SqlParameter param1_2 = new SqlParameter("@BuyerGroupName", buyerGroupName);
                SqlParameter param1_3 = new SqlParameter("@Year", year);

                dtPOVolume = oDBCtrl.executeQueryStoreProc("POVolume_Sel", param1_1, param1_2, param1_3).Tables["temp"];
                return dtPOVolume;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oDBCtrl.disConnect();
            }

        }

        #endregion

        #region POStatus

        public DataTable getDtPOStatus(string suppShortName, string buyerGroupName, int year)
        {
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            DataTable dtPOStatus;
            try
            {
                oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);

                SqlParameter param1_1 = new SqlParameter("@SuppShortName", suppShortName);
                SqlParameter param1_2 = new SqlParameter("@BuyerGroupName", buyerGroupName);
                SqlParameter param1_3 = new SqlParameter("@Year", year);

                dtPOStatus = oDBCtrl.executeQueryStoreProc("POVolumeByStatus_Sel", param1_1, param1_2, param1_3).Tables["temp"];
                return dtPOStatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oDBCtrl.disConnect();
            }

        }

        #endregion

        #endregion

        #region Chart

        #region NumPO

        public DataTable getDtNumPO_Chart(string suppShortName, string buyerGroupName, int year)
        {
            //3D Pie Chart
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            DataTable dtNumPO;
            try
            {
                oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);
                SqlParameter param1_1 = new SqlParameter("@SuppShortName", suppShortName);
                SqlParameter param1_2 = new SqlParameter("@BuyerGroupName", buyerGroupName);
                SqlParameter param1_3 = new SqlParameter("@Year", year);
                dtNumPO = oDBCtrl.executeQueryStoreProc("NumPO_Chart_Sel", param1_1, param1_2, param1_3).Tables["temp"];
                return dtNumPO;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oDBCtrl.disConnect();
            }

            
        }

        #endregion

        #region POVolume

        public DataTable getDtPOVolume_Chart(string suppShortName, string buyerGroupName, int year)
        {
            //3D Pie Chart
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            DataTable dtPOVolume;
            try
            {
                oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);
                SqlParameter param1_1 = new SqlParameter("@SuppShortName", suppShortName);
                SqlParameter param1_2 = new SqlParameter("@BuyerGroupName", buyerGroupName);
                SqlParameter param1_3 = new SqlParameter("@Year", year);
                dtPOVolume = oDBCtrl.executeQueryStoreProc("POVolume_Chart_Sel", param1_1, param1_2, param1_3).Tables["temp"];
                return dtPOVolume;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oDBCtrl.disConnect();
            }

        }

        #endregion

        #region POStatus

        public DataTable getDtPOStatus_Chart(string suppShortName, string buyerGroupName, int year)
        {
            //3D Pie Chart
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            DataTable dtPOStatus;
            try
            {
                oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);
                SqlParameter param1_1 = new SqlParameter("@SuppShortName", suppShortName);
                SqlParameter param1_2 = new SqlParameter("@BuyerGroupName", buyerGroupName);
                SqlParameter param1_3 = new SqlParameter("@Year", year);
                dtPOStatus = oDBCtrl.executeQueryStoreProc("POVolumeByStatus_Chart_Sel", param1_1, param1_2, param1_3).Tables["temp"];
                return dtPOStatus;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oDBCtrl.disConnect();
            }

        }

        #endregion

        #endregion

        #region User Data

        public DataTable getDtUserSessionByGUID(string userGuid)
        {
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            DataTable dtUser;
            try
            {
                oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);

                SqlParameter param1_1 = new SqlParameter("@UserGuid", userGuid);

                dtUser = oDBCtrl.executeQueryStoreProc("UserSessionByGUID_Sel", param1_1).Tables["temp"];

                return dtUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oDBCtrl.disConnect();
            }

        }

        public void deleteUsertSession(string suppShortName, string userID, string userGuid)
        {
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            try
            {
                oDBCtrl.connect(ClsSupportConnection.ServerNameIs, ClsSupportConnection.UserNameIs, ClsSupportConnection.PasswordIs, ClsSupportConnection.DBNameIs);

                SqlParameter param1_1 = new SqlParameter("@UserGuid", userGuid);
                SqlParameter param1_2 = new SqlParameter("@SuppShortName", suppShortName);
                SqlParameter param1_3 = new SqlParameter("@UserID", userID);

                oDBCtrl.executeNonQueryStoreProc("UserSession_Del", param1_1, param1_2, param1_3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                oDBCtrl.disConnect();
            }

        }

        #endregion

        #endregion

    }
}