﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PISDataAccess;
using System.Data;
using System.Data.SqlClient;
using PISBusiness;
using System.Web.Mvc;

namespace SupplierPortal.ReportSupport
{
    public class ClsSupportConnection
    {
        #region Data Member

        public static string rptHostConnStr;
        public static string ServerNameIs;
        public static string UserNameIs;
        public static string PasswordIs;
        public static string DBNameIs;
        public static int connStatus;

        #endregion

        #region Constructor

        public ClsSupportConnection()
        {
            ServerNameIs = string.Empty;
            UserNameIs = string.Empty;
            PasswordIs = string.Empty;
            DBNameIs = string.Empty;
            connStatus = 0;
        }
        #endregion

        #region Method

        public Boolean GetServerValueBySupplier(string suppShortName)  
        {
            ClsDatabaseControl oDBCtrl = new ClsDatabaseControl();
            DataTable dtConnection;

            try 
	        {
                //-----------------------------
                //          Develop
                //-----------------------------
                //rptHostConnStr = "server=sqlexpress;uid='';pwd='';database=Reporting_HostBestSupply";
                //rptHostConnStr = "server=192.168.10.23;uid=sa;pwd=kernel16fws;database=Reporting_HostBestSupply";
                rptHostConnStr = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString();
                //-----------------------------
                //          Production
                //-----------------------------
                //rptHostConnStr = "server=NPRTSQ01,1434;uid=appuser;pwd=pantavanij;database=PSFTReporting_Host";

		        oDBCtrl.connect(rptHostConnStr);
                SqlParameter param1_1 = new SqlParameter("@SuppShortName", suppShortName);
                dtConnection = oDBCtrl.executeQueryStoreProc("RptHostConfig_Sel",param1_1).Tables["temp"];

                if (dtConnection.Rows.Count == 0)
	            {
                   // throw new Exception("Can not find report host configuration. Please contact your administrator.");
                    return false;
	            }
                else
                {
                    ServerNameIs = dtConnection.Rows[0]["RptSiteServer"].ToString();
                    UserNameIs = dtConnection.Rows[0]["RptSiteUser"].ToString();
                    PasswordIs = dtConnection.Rows[0]["RptSitePassword"].ToString();
                    DBNameIs = dtConnection.Rows[0]["RptSiteDatabase"].ToString();
                    connStatus = 1;
                    return true;
                }
	        }
	        catch
	        {
                return false;
	        }
            finally
            {
                oDBCtrl.disConnect();
            }
     
        }

        #endregion
    }
}