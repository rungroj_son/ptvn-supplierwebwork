﻿using Newtonsoft.Json;
using SupplierPortal.Data.Models.SupportModel.MenuPortal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace SupplierPortal.AppConfig
{
    public class ConnectAPI
    {
        public async Task<HttpResponseMessage> BaseApiConnectionPost(string url, Object model)
        {
            HttpClient client = new HttpClient();
            string languageURL = string.Empty;
            string baseURL = WebConfigurationManager.AppSettings["baseURL"];

            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string Jsonstr = JsonConvert.SerializeObject(model);
            HttpContent content = new StringContent(Jsonstr);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            string urlAll = string.Format(url, baseURL);
            //string urlAll = string.Format("{0}api/AccountPortal/ResetPassword{1}", baseURL, languageURL);

            HttpResponseMessage response = await client.PostAsync(urlAll, content);

            return response;
        }

        public async Task<HttpResponseMessage> BaseApiConnectionGet(string url, string parameter)
        {
            HttpClient client = new HttpClient();
            string languageURL = string.Empty;
            string baseURL = WebConfigurationManager.AppSettings["baseURL"];

            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            string urlAll = string.Format(url, baseURL, parameter);
            //string urlAll = string.Format("{0}api/AccountPortal/ResetPassword{1}", baseURL, languageURL);

            HttpResponseMessage response = await client.GetAsync(urlAll);

            return response;
        }
    }
}