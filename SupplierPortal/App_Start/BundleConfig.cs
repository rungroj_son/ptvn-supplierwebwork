﻿using System.Web;
using System.Web.Optimization;

namespace SupplierPortal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/blockUI").Include(
                       "~/Scripts/jquery.blockUI.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/country-dropdown").Include(
                       "~/Content/country-dropdown/js/msdropdown/jquery.dd.min.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/placeholder").Include(
                      "~/Scripts/jquery.placeholder.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/jquery-ui-1.11.2/jqueryui").Include(
                      "~/Scripts/jquery-ui-1.11.2/jquery-ui.js"));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                      "~/Content/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
                      "~/Content/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"
                      ));

            //bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
            //          "~/Scripts/moment.min.js",
            //          "~/Scripts/bootstrap-datetimepicker.min.js"
            //          ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/customheadscript").Include(
                      "~/Scripts/customheadscript.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/custombodyscript").Include(
                      "~/Scripts/custombodyscript.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/registerscript").Include(
                      "~/Scripts/registerscript.js",
                      "~/Scripts/registerScriptNew.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/profilescript").Include(
                      "~/Scripts/profilescript.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/comprofilescript").Include(
                     "~/Scripts/comprofilescript.js",
                     "~/Scripts/comProfileScriptNew.js"
                     ));

            bundles.Add(new ScriptBundle("~/bundles/bootgrid").Include(
                      "~/Scripts/jquery.bootgrid.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap-hover-dropdown").Include(
                      "~/Scripts/bootstrap-hover-dropdown.min.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/edittrackingscript").Include(
                    "~/Scripts/edittrackingscript.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/marked").Include(
                    "~/Scripts/marked.min.js"
                    ));

            // ==========Script Report===================//

            bundles.Add(new ScriptBundle("~/bundles/ScriptReport").Include(
                      "~/Scripts/Report/jquery-1.5.1.min.js",
                      "~/Scripts/Report/MicrosoftAjax.js",
                      "~/Scripts/Report/MicrosoftMvcValidation.js"
                      ));

            // ==========End Script Report===================//

            // ==========Script InputMask===================//
            bundles.Add(new ScriptBundle("~/bundles/maskedinputscript").Include(
                      "~/Scripts/jquery.maskedinput.min.js"
                      ));
            // ==========End Script InputMask===================//

            // ==========Script FileUpload===================//
            bundles.Add(new ScriptBundle("~/bundles/fileuploadscript").Include(
                      "~/Scripts/jquery-ui-1.9.2.min.js",
                      "~/Scripts/jquery.fileupload.js",
                      "~/Scripts/jquery.fileupload-ui.js",
                      "~/Scripts/jquery.iframe-transport.js"
                      ));
            // ==========End Script FileUpload===================//

            // ==========Script Select2===================//
            bundles.Add(new ScriptBundle("~/bundles/select2").Include(
                      "~/Scripts/select2.min.js",
                      "~/Scripts/customSelect2.js"
                      ));
            // ==========End Script Select2===================//

            bundles.Add(new ScriptBundle("~/bundles/typeahead").Include(
                      "~/Scripts/typeahead.bundle.js"
                     ));
            // ==========End Script Typeahead===================//

            // ==========Script & CSS CP FreshMart Menu===================//
            bundles.Add(new StyleBundle("~/Content/menucss").Include(
                //"~/Content/FreshMartMenu/bootstrap-select.css",
                //"~/Content/FreshMartMenu/bootstrap-xl.css",
                //"~/Content/FreshMartMenu/flexslider.css",
                //"~/Content/FreshMartMenu/isotope.css",
                //"~/Content/FreshMartMenu/jcarousel.basic.css",
                //"~/Content/FreshMartMenu/jquery-ui-1.10.3.custom.min.css",
                      "~/Content/FreshMartMenu/jquery.mCustomScrollbar.css",
                //"~/Content/FreshMartMenu/megamenu.css",
                //"~/Content/FreshMartMenu/prettyPhoto.css",
                //"~/Content/FreshMartMenu/skin.css",
            "~/Content/FreshMartMenu/styles.css",
            "~/Content/FreshMartMenu/template.css",
                //"~/Content/FreshMartMenu/bootstrap.css",
            "~/Content/FreshMartMenu/typicons.css"
                //"~/Content/FreshMartMenu/zoom.css"
            ));

            bundles.Add(new ScriptBundle("~/Content/menuscript").Include(
                  "~/Content/FreshMartMenu/bootstrap-select.js",
                  "~/Content/FreshMartMenu/custom.js",
                //"~/Content/FreshMartMenu/jquery-migrate-1.2.1.min.js",
                // "~/Content/FreshMartMenu/jquery-transit-modified.js",
                //  "~/Content/FreshMartMenu/jquery-ui-1.10.3.custom.min.js",
                //   "~/Content/FreshMartMenu/jquery.easing.1.3.js",
                //    "~/Content/FreshMartMenu/jquery.elevatezoom.js",
                       "~/Content/FreshMartMenu/jquery.flexslider.js",
                //      "~/Content/FreshMartMenu/jquery.isotope.min.js",
                         "~/Content/FreshMartMenu/jquery.js",
                         "~/Content/FreshMartMenu/jquery.mCustomScrollbar.js"
                //"~/Content/FreshMartMenu/jquery.mousewheel.js"
                //"~/Content/FreshMartMenu/jquery.prettyPhoto.js",
                // "~/Content/FreshMartMenu/jquery.validate.min.js",
                //  "~/Content/FreshMartMenu/jquery.validate.unobtrusive.min.js",
                //                 "~/Content/FreshMartMenu/prototype.js",
                // "~/Content/FreshMartMenu/public.ajaxcart.js"
                //"~/Content/FreshMartMenu/public.common.js"
            ));
            // ==========Script & CSS CP FreshMart Menu===================//

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/GoogleAnalytics").Include(
                      "~/Content/GoogleAnalytics/analytics.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/msdropdown").Include(
                     "~/Content/country-dropdown/css/msdropdown/dd.css",
                     "~/Content/country-dropdown/css/msdropdown/flags.css"
                     ));

            bundles.Add(new StyleBundle("~/Content/jquery-ui-1.11.2/themes/smoothness/css").Include(
                     "~/Content/jquery-ui-1.11.2/themes/smoothness/jquery-ui.css"
                     ));

            bundles.Add(new StyleBundle("~/Content/breadcrumb").Include(
                      "~/Content/breadcrumb.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/portal").Include(
                      "~/Content/portal.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/fileuploadcss").Include(
                      "~/Content/jquery.fileupload.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/bootgrid").Include(
                      "~/Content/jquery.bootgrid.css"
                      ));


            //bundles.Add(new StyleBundle("~/Content/datepicker").Include(
            //        "~/Content/bootstrap-datetimepicker.css"
            //        ));

            bundles.Add(new StyleBundle("~/Content/datepicker").Include(

                    "~/Content/bootstrap-datepicker/css/bootstrap-datepicker3.min.css",
                    "~/Content/bootstrap-datepicker/css/bootstrap-datepicker3.standalone.min.css"
                    ));

            bundles.Add(new StyleBundle("~/Content/awesome").Include(
                      "~/Content/font-awesome.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/select2").Include(
                      "~/Content/css-select2/select2.min.css"
                      ));


            //---Devexpress ASPxScriptIntelliSense ----------------//
            bundles.Add(new ScriptBundle("~/bundles/ScriptIntelliSense").Include(
                     "~/Scripts/ASPxScriptIntelliSense.js"
                     ));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
