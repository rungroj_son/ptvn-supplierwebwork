﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SupplierPortal
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");

          

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Landing", action = "WhyVerification", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "Profile",
            //    url: "Profile/Index/{errorMessage}/{tab}",
            //    defaults: new
            //    {
            //        controller = "Profile",
            //        action = "Index",
            //        tab = UrlParameter.Optional,
            //        errorMessage = UrlParameter.Optional,

            //    }
            //);
            
        }
    }
}
