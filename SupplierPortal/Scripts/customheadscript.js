﻿//$(".btnSubmit").click(function () {
//    console.log("btnSubmit");
//    $("#isSubmit").val(true);

//});

//$(".btnSave").click(function () {
//    console.log("btnSave");
//    $("#isSubmit").val(false);
//});
function CountryChanged(s, e) {
    var countryCode = s.GetValue();

    if (countryCode == "TH") {
        $("#txtBrNo").attr('readonly', false);
        $("#txtBrNo").val("00000");
    }else
    {
        $("#txtBrNo").val("");
        $("#txtBrNo").attr('readonly', true);
    }
}


function CoLoadCheckEvent(s, e) {
    var checked = s.GetChecked();
    $("#btnAgree").attr('disabled', !checked);
    if (checked)
    {
        validateTaxid();
    }
}

function chkAddContactOrgCheckEvent(s, e) {
    var checked = s.GetChecked();
    if (checked == true) {
        $('#AddContact').show();
        $('#ContactEmail').val($('#Email').val());
        $('#FirstName_Local').val($('#FirstName').val());
        $('#LastName_Local').val($('#LastName').val());

    }
    else {
        $('#AddContact').hide();
    }
}

function validateTaxid() {
    $.ajax({
        url: '/RegisterPortal/checkvalidtaxid',
        dataType: "json",
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({
            validtaxid: {
                TaxID: $("#txtTaxID").val(),
                CompanyName: $("#txtCompName").val(),
                BranchNo: $("#txtBrNo").val(),
                BranchName: $("#txtBrName").val(),
                RegID: "",
                Message: "",
                Alert: "",
                SupplierID: "",
            }
        }),
        async: true,
        processData: false,
        cache: false,
        success: function (data) {
            //console.log(data);
            if (data.Message == "Org Duplicate") {
                chkReadAgreement.SetChecked(false);
                $("#btnAgree").attr('disabled', true);
                confirmSendMailModal(data.SupplierID);
            }
            else if (data.Message == "RegInfo Warnning Duplicate") {
                $("#message").text("");
                $("#message").text(data.Alert);
                loadPopup();//Funtion from customheadscript
                /*$("#btn").click(function () {
                    return true;
                });*/
            }
            else if (data.Message == "RegInfo Duplicate") {
                chkReadAgreement.SetChecked(false);
                $("#btnAgree").attr('disabled', true);
                $("#message").text("");
                $("#message").text(data.Alert);
                loadPopup();//Funtion from customheadscript
                /*$("#btn").click(function () {
                    return true;
                });*/
            }
        },
        error: function (xhr) {
            return false;
        }
    })

}

function Selected(s, e) {
    if (s.GetSelectedIndex() == -1) {
        e.isValid = false;
        e.errorText = "";
    }
}


function CheckboxCheckEvent(s, e) {
    var checked = s.GetChecked();
    if (checked == true) {
        $("#isSameAddress").val(true);
        $('.DvAddr').hide();
    }
    else {
        $("#isSameAddress").val(false);
        $('.DvAddr').show();
    }
}

function AdvancedSearchEvent(s, e) {
    var checked = s.GetChecked();
    if (checked == true) {

        $('.product-add').hide();
        $('#ProdSearch').show();
        $('#ProdSearchButton').show();
       
    }
    else {
        $('.product-add').show();
        $('#ProdSearch').hide();
        $('#ProdSearchButton').hide();
    }
}

function rbl_Init(s, e) {

    if(s.GetValue() == null)
    {
        s.SetValue(1);
    }else
    {
        UpdateItemsVisibility(s);

    }

}

function OnInit(s, e) {


    UpdateItemsVisibility(s);
}

function OnUserTypeChanged(s, e) {
    //alert(s);
    //alert(e);
    if (s.name == "CompanyTypeID") {
        BusinessEntityID.PerformCallback();
    }

    UpdateItemsVisibility(s);
    ChangedDocumentType(s);
}

function SetBranchValue(s, e) {

    var branchNo = $("#ddlBrNo").val();
    var branchNane = $("#hidBranchName_Local").val();

    var countryCode = $("#hidCountryCode").val();

    if (branchNo == "00000" && countryCode == "TH")
    {
        s.SetSelectedIndex(0);
        $("#ddlBrNo").attr("readonly", true);
        //console.log("Quarter");
    } else if (countryCode != "TH" && ( branchNane != ""))
    {
        s.SetSelectedIndex(1);
        $("#ddlBrNo").attr("readonly", false);
        $(".EdtBrNameTab1").attr("readonly", false);
        //console.log("Branch");
    }else
    {
        s.SetSelectedIndex(0);
        $("#ddlBrNo").attr("readonly", true);
    }

}

function UpdateItemsVisibility(radioButtonList) {
    //console.log("UpdateItemsVisibility");
    if (radioButtonList.name == "CompanyTypeID") {
        if (radioButtonList.GetSelectedIndex() == 0) {
            //console.log("radioButtonList.GetSelectedIndex() == 0");
            $("#isCorperate").val(true);
            $(".EdtBrNameTab1").val("");
            BusinessEntityID.SetValue(-1);
            //BusinessEntityID.SetSelectedIndex(1);
            rdbBusinessTab2.SetSelectedIndex(0);
            rdbBusinessTab2.SetEnabled(true);
            //$("#ddlBrNo").attr('disabled', true);
            
            

            $(".EdtBrNameTab1").attr("readonly", true);
        }
        else if (radioButtonList.GetSelectedIndex() == 1) {
            //console.log("radioButtonList.GetSelectedIndex() == 1");
            $("#isCorperate").val(false);
            $(".EdtBrNameTab1").val("");
            BusinessEntityID.SetValue(-1);
            //BusinessEntityID.SetSelectedIndex(0);
            rdbBusinessTab2.SetSelectedIndex(0);
            rdbBusinessTab2.SetEnabled(false);
            $("#ddlBrNo").attr('readonly', true);
            $("#ddlBrNo").val("");

            $(".EdtBrNameTab1").attr("readonly", true);
        }
    }
    else if (radioButtonList.name == "rdbBusinessTab2") {
        var countryCode = $("#hidCountryCode").val();
        if (radioButtonList.GetSelectedIndex() == 0) {
            //console.log("radioButtonList.GetSelectedIndex() == 0");
            $("#BranchNoValidation").css("display", "none");

            $("#isHead").val(true);
            $(".EdtBrNameTab1").val("");
            $("#ddlBrNo").attr('readonly', true);
            $("#ddlBrNo").val("");
            $(".EdtBrNameTab1").attr("readonly", true);

            if (countryCode == "TH") {
                $("#ddlBrNo").val("00000");
            } else {
                $("#ddlBrNo").val("");
                
            }
        }
        else if (radioButtonList.GetSelectedIndex() == 1) {
            //console.log("radioButtonList.GetSelectedIndex() == 1");
            $("#isHead").val(false);
            $("#ddlBrNo").attr('readonly', false);
            $("#ddlBrNo").val("");
            $(".EdtBrNameTab1").attr("readonly", false);

            if (countryCode != "TH") {
                $("#ddlBrNo").val("");
                $("#ddlBrNo").attr('readonly', true);
            }
        }
    }
}

function CmbSelectedIndexChanged(s, e) {
    if (s.GetValue() == -1) {
        $("#txtOtherBusEnt").css("display", "block");
    } else {
        $("#txtOtherBusEnt").css("display", "none");
    }
    //console.log(s.GetValue())
}
function JobTitleChanged(s, e) {
    //alert(s.GetValue());
    if (s.GetValue() == -1)
    {
        $("#OtherJobTitleShow").css("display", "block");
    } else
    {
        $("#OtherJobTitleShow").css("display", "none");
    }
}

function ContactJobTitleChanged(s, e) {
    //alert(s.GetValue());
    if (s.GetValue() == -1) {
        $("#ContactOtherJobTitleShow").css("display", "block");
    } else {
        $("#ContactOtherJobTitleShow").css("display", "none");
    }
}


function GetValueRadio(s, e) {
    e.customArgs['CorperateValue'] = CompanyTypeID.GetSelectedIndex();
}

function CountrySelectedIndexChanged(s, e) {

    RegCompanyAddress_State_Local.PerformCallback();
    RegCompanyAddress_State_Inter.PerformCallback();

    //RegCompanyAddress_SubDistrict.PerformCallback();
    //RegCompanyAddress_City.PerformCallback();
    //RegCompanyAddress_State.PerformCallback();

    //ChangedDocumentType(rdbBusinessTab1); 
}

function StateCompAddrLocalSelectedIndexChanged(s, e) {
    RegCompanyAddress_City_Local.PerformCallback();
}

function StateCompAddrInterSelectedIndexChanged(s, e) {
    RegCompanyAddress_City_Inter.PerformCallback();
}

function StateDelvAddrLocalSelectedIndexChanged(s, e) {
    RegDeliverAddress_City_Local.PerformCallback();
}

function StateDelvAddrInterSelectedIndexChanged(s, e) {
    RegDeliverAddress_City_Inter.PerformCallback();
}

function StatePersAddrLocalSelectedIndexChanged(s, e) {
    RegPersonalAddress_City_Local.PerformCallback();
}

function StatePersAddrInterSelectedIndexChanged(s, e) {
    RegPersonalAddress_City_Inter.PerformCallback();
}

function CityCompAddrLocalSelectedIndexChanged(s, e) {
    RegCompanyAddress_SubDistrict_Local.PerformCallback();
}

function CityCompAddrInterSelectedIndexChanged(s, e) {
    RegCompanyAddress_SubDistrict_Inter.PerformCallback();
}

function CityDelvAddrLocalSelectedIndexChanged(s, e) {
    RegDeliverAddress_SubDistrict_Local.PerformCallback();
}

function CityDelvAddrInterSelectedIndexChanged(s, e) {
    RegDeliverAddress_SubDistrict_Inter.PerformCallback();
}

function CityPersAddrLocalSelectedIndexChanged(s, e) {
    RegPersonalAddress_SubDistrict_Local.PerformCallback();
}

function CityPersAddrInterSelectedIndexChanged(s, e) {
    RegPersonalAddress_SubDistrict_Inter.PerformCallback();
}

function CountryDelvSelectedIndexChanged(s, e) {

    RegDeliverAddress_State_Local.PerformCallback();
    RegDeliverAddress_State_Inter.PerformCallback();

    //RegDeliverAddress_SubDistrict.PerformCallback();
    //RegDeliverAddress_City.PerformCallback();
    //RegDeliverAddress_State.PerformCallback();
}

function CountryPersSelectedIndexChanged(s, e) {

    RegPersonalAddress_State_Local.PerformCallback();
    RegPersonalAddress_State_Inter.PerformCallback();

}

function GetDocumentNameCallback(s, e) {
    e.customArgs['SendDelvAddrCountry'] = RegCompanyAddress_CountryCode.GetValue();
    e.customArgs['SendCompanyTypeID'] = CompanyTypeID.GetValue();
}
function GetValueCompAddrCountry(s, e) {
    e.customArgs['CompAddrCountry'] = RegCompanyAddress_CountryCode.GetValue();

}

function GetValueCompAddrLocalCountry(s, e) {
    e.customArgs['CompAddrCountry'] = RegCompanyAddress_CountryCode.GetValue();
    e.customArgs['CompAddrState'] = RegCompanyAddress_State_Local.GetValue();
    e.customArgs['CompAddrCity'] = RegCompanyAddress_City_Local.GetValue();
}

function GetValueCompAddrInterCountry(s, e) {
    e.customArgs['CompAddrCountry'] = RegCompanyAddress_CountryCode.GetValue();
    e.customArgs['CompAddrState'] = RegCompanyAddress_State_Inter.GetValue();
    e.customArgs['CompAddrCity'] = RegCompanyAddress_City_Inter.GetValue();
}

function GetValueDelvAddrCountry(s, e) {
    e.customArgs['DelvAddrCountry'] = RegDeliverAddress_CountryCode.GetValue();
}

function GetValueDelvAddrLocalCountry(s, e) {
    e.customArgs['DelvAddrCountry'] = RegDeliverAddress_CountryCode.GetValue();
    e.customArgs['DelvAddrState'] = RegDeliverAddress_State_Local.GetValue();
    e.customArgs['DelvAddrCity'] = RegDeliverAddress_City_Local.GetValue();
}

function GetValueDelvAddrInterCountry(s, e) {
    e.customArgs['DelvAddrCountry'] = RegDeliverAddress_CountryCode.GetValue();
    e.customArgs['DelvAddrState'] = RegDeliverAddress_State_Inter.GetValue();
    e.customArgs['DelvAddrCity'] = RegDeliverAddress_City_Inter.GetValue();
}

function GetValuePersAddrLocalCountry(s, e) {
    e.customArgs['PersAddrCountry'] = RegPersonalAddress_CountryCode.GetValue();
    e.customArgs['PersAddrState'] = RegPersonalAddress_State_Local.GetValue();
    e.customArgs['PersAddrCity'] = RegPersonalAddress_City_Local.GetValue();
}

function GetValuePersAddrInterCountry(s, e) {
    e.customArgs['PersAddrCountry'] = RegPersonalAddress_CountryCode.GetValue();
    e.customArgs['PersAddrState'] = RegPersonalAddress_State_Inter.GetValue();
    e.customArgs['PersAddrCity'] = RegPersonalAddress_City_Inter.GetValue();
}


function AddressCopySelectChanged(s, e) {

    if (s.GetValue() == 1)
    {
        CompanyAddressCopy();

      
    } else if (s.GetValue() == 2)
    {
        var checked = IsSameAddress.GetChecked();
        if (checked == true) {

            //console.log("Same True");
            CompanyAddressCopy();
        }
        else {
            //console.log("Same False");
            DeliverAddressCopy();
        }
    }

}

function CompanyAddressCopy()
{
    $("#RegPersonalAddress_HouseNo_Local").val($("#RegCompanyAddress_HouseNo_Local").val());
    $("#RegPersonalAddress_VillageNo_Local").val($("#RegCompanyAddress_VillageNo_Local").val());
    $("#RegPersonalAddress_Lane_Local").val($("#RegCompanyAddress_Lane_Local").val());
    $("#RegPersonalAddress_Road_Local").val($("#RegCompanyAddress_Road_Local").val());

    RegPersonalAddress_State_Local.SetValue(RegCompanyAddress_State_Local.GetText());
    RegPersonalAddress_City_Local.SetValue(RegCompanyAddress_City_Local.GetText());
    RegPersonalAddress_SubDistrict_Local.SetValue(RegCompanyAddress_SubDistrict_Local.GetText());

    $("#RegPersonalAddress_HouseNo_Inter").val($("#RegCompanyAddress_HouseNo_Inter").val());
    $("#RegPersonalAddress_VillageNo_Inter").val($("#RegCompanyAddress_VillageNo_Inter").val());
    $("#RegPersonalAddress_Lane_Inter").val($("#RegCompanyAddress_Lane_Inter").val());
    $("#RegPersonalAddress_Road_Inter").val($("#RegCompanyAddress_Road_Inter").val());

    RegPersonalAddress_State_Inter.SetValue(RegCompanyAddress_State_Inter.GetText());
    RegPersonalAddress_City_Inter.SetValue(RegCompanyAddress_City_Inter.GetText());
    RegPersonalAddress_SubDistrict_Inter.SetValue(RegCompanyAddress_SubDistrict_Inter.GetText());

    $("#RegPersonalAddress_PostalCode").val($("#RegCompanyAddress_PostalCode").val());

    RegPersonalAddress_CountryCode.SetSelectedItem(RegPersonalAddress_CountryCode.FindItemByValue(RegCompanyAddress_CountryCode.GetValue()));

    var countLang = $("#countLanguageLocalized").val();
    for (i = 0; i < countLang; i++) {
        $("#PerAddrHouseNo-" + i).val($("#ComAddrHouseNo-" + i).val());
        $("#PerAddrVillageNo-" + i).val($("#ComAddrVillageNo-" + i).val());
        $("#PerAddrLane-" + i).val($("#ComAddrLane-" + i).val());
        $("#PerAddrRoad-" + i).val($("#ComAddrRoad-" + i).val());
        $("#PerAddrSubDistrict-" + i).val($("#ComAddrSubDistrict-" + i).val());
        $("#PerAddrCity-" + i).val($("#ComAddrCity-" + i).val());
        $("#PerAddrState-" + i).val($("#ComAddrState-" + i).val());
    }
    
}

function DeliverAddressCopy() {

    $("#RegPersonalAddress_HouseNo_Local").val($("#RegDeliverAddress_HouseNo_Local").val());
    $("#RegPersonalAddress_VillageNo_Local").val($("#RegDeliverAddress_VillageNo_Local").val());
    $("#RegPersonalAddress_Lane_Local").val($("#RegDeliverAddress_Lane_Local").val());
    $("#RegPersonalAddress_Road_Local").val($("#RegDeliverAddress_Road_Local").val());

    RegPersonalAddress_State_Local.SetValue(RegDeliverAddress_State_Local.GetText());
    RegPersonalAddress_City_Local.SetValue(RegDeliverAddress_City_Local.GetText());
    RegPersonalAddress_SubDistrict_Local.SetValue(RegDeliverAddress_SubDistrict_Local.GetText());

    $("#RegPersonalAddress_HouseNo_Inter").val($("#RegDeliverAddress_HouseNo_Inter").val());
    $("#RegPersonalAddress_VillageNo_Inter").val($("#RegDeliverAddress_VillageNo_Inter").val());
    $("#RegPersonalAddress_Lane_Inter").val($("#RegDeliverAddress_Lane_Inter").val());
    $("#RegPersonalAddress_Road_Inter").val($("#RegDeliverAddress_Road_Inter").val());

    RegPersonalAddress_State_Inter.SetValue(RegDeliverAddress_State_Inter.GetText());
    RegPersonalAddress_City_Inter.SetValue(RegDeliverAddress_City_Inter.GetText());
    RegPersonalAddress_SubDistrict_Inter.SetValue(RegDeliverAddress_SubDistrict_Inter.GetText());

    $("#RegPersonalAddress_PostalCode").val($("#RegDeliverAddress_PostalCode").val());

    RegPersonalAddress_CountryCode.SetSelectedItem(RegPersonalAddress_CountryCode.FindItemByValue(RegDeliverAddress_CountryCode.GetValue()));

    var countLang = $("#countLanguageLocalized").val();
    for (i = 0; i < countLang; i++) {
        $("#PerAddrHouseNo-" + i).val($("#DelAddrHouseNo-" + i).val());
        $("#PerAddrVillageNo-" + i).val($("#DelAddrVillageNo-" + i).val());
        $("#PerAddrLane-" + i).val($("#DelAddrLane-" + i).val());
        $("#PerAddrRoad-" + i).val($("#DelAddrRoad-" + i).val());
        $("#PerAddrSubDistrict-" + i).val($("#DelAddrSubDistrict-" + i).val());
        $("#PerAddrCity-" + i).val($("#DelAddrCity-" + i).val());
        $("#PerAddrState-" + i).val($("#DelAddrState-" + i).val());
    }

}

var producttypeids;
var command;
function grdProdItemOnBeginCallback(s, e) {
    e.customArgs["Status"] = $("#hdfStatus").val();
    command = e.command;
}

function OnGvSampleLoansDestinationEndCallback(s, e) {
    if (command == "CUSTOMCALLBACK")
        s.UnselectRows();
}

function grdProdItemOnSelectionChanged(s, e) {
    if (e.isChangedOnServer) return;

    var key = s.GetRowKey(e.visibleIndex);
    if (e.isSelected){
        s.cpSelectedKeys.push(key);
    } 
    else
        s.cpSelectedKeys = RemoveElementFromArray(s.cpSelectedKeys, key);
    // do custom action with the s.cpSelectedKeys

    $("#hdfselectedProdCodes").val(s.cpSelectedKeys);
}

function RemoveElementFromArray(array, element) {
    var index = array.indexOf(element);
    if (index < 0) return array;
    array[index] = null;
    var result = [];
    for (var i = 0; i < array.length; i++) {
        if (array[i] === null)
            continue;
        result.push(array[i]);
    }
    return result;
}

function gridViewModelListOnBeginCallback(s, e) {
    //Pass all selected keys to GridView callback action
    e.customArgs["selectedProdCodes"] = $("#hdfselectedProdCodes").val();
    e.customArgs["ProductTypeID"] = ProductTypeID.GetValue();
    e.customArgs["deletedProdTypeID"] = $("#hdfdeletedProdTypeID").val();
    e.customArgs["deletedProdCode"] = $("#hdfdeletedProdCode").val();
}

