﻿function CompanyTypeChangedNew() {
    $('input[type=radio][name=CompanyTypeID]').change(function () {
        var companyTypeID = this.value;

        //console.log(companyTypeID);
        BusinessEntityCallback(companyTypeID);
        UpdateItemCompanyNew(companyTypeID);
        ChangedDocumentType(companyTypeID);
        UpdateLableTaxNoNew(companyTypeID);
    });
}

function BranchTypeChangedNew() {
    var countryCode = $("#dd-country option:selected").val();
    $('input[type=radio][name=BranchType]').change(function () {
        //console.log(this.value);
        if (this.value == "0") {
            $("#BranchNo").attr('readonly', true);
            $("#BranchNo").val("");
        } else if (this.value == "1") {
            $("#BranchNo").attr('readonly', true);
            $("#BranchNo").val("00000");
        } else {
            $("#BranchNo").attr('readonly', false);
            $("#BranchNo").val("");
        }
    });
}

function BusinessEntityCallback(companyTypeID) {
    //console.log("BusinessEntityCallback Register");
    $.ajax({
        url: '/Register/BusinessEntityPartialNew',
        dataType: 'html',
        data: { 'CorperateValue': companyTypeID },
        success: function (data) {
            $('#BusinessEntity').html(data);

            EndCallbackBusinessEntityNew(companyTypeID);
        },
        fail: function (event, data) {
        }
    });
}

function EndCallbackBusinessEntityNew(companyTypeID) {
    var companyType = companyTypeID
    if (companyType == 1) {
        //s.SetValue(4);
        $("#BusinessEntityID").val(4).change();;
        $("#txtOtherBusEnt").css("display", "none");
    } else if (companyType == 2) {
        //s.SetValue(13);
        $("#BusinessEntityID").val(13).change();;
        $("#txtOtherBusEnt").css("display", "none");
    }
}

function UpdateItemCompanyNew(companyTypeID) {
    var companyType = companyTypeID;
    var countryCode = $("#dd-country option:selected").val();
    //console.log(countryCode);

    if (companyType == 1) {
        if (countryCode == "TH") {
            $('input:radio[class=BranchType][id=BranchType1]').prop('checked', true);
            $("#BranchNo").attr('readonly', true);
            $("#BranchNo").val("00000");

            $('input:radio[class=BranchType][id=BranchType0]').prop('checked', false);
            $('input:radio[class=BranchType][id=BranchType0]').prop('disabled', true);
        } else {
            $('input:radio[class=BranchType][id=BranchType0]').prop('checked', true);
            $("#BranchNo").attr('readonly', true);
            $("#BranchNo").val("");
        }
    } else if (companyType == 2) {
        $('input:radio[class=BranchType][id=BranchType0]').prop('disabled', false);
        $('input:radio[class=BranchType][id=BranchType0]').prop('checked', true);

        $("#BranchNo").attr('readonly', true);
        $("#BranchNo").val("");
    }
}

function CheckBranchNoNew() {
    var companyType = $('input[name=CompanyTypeID]:checked').val();
    var countryCode = $("#dd-country option:selected").val();
    //console.log(companyType);
    if ($("#hidBranchNo").val() == "" || $("#hidBranchNo").val() == undefined) {
        $('input:radio[class=BranchType][id=BranchType0]').prop('checked', true);
    } else if ($("#hidBranchNo").val() == "00000") {
        $('input:radio[class=BranchType][id=BranchType1]').prop('checked', true);
    } else {
        $('input:radio[class=BranchType][id=BranchType2]').prop('checked', true);
    }
    if (companyType == 1) {
        if (countryCode == "TH") {
            $('input:radio[class=BranchType][id=BranchType0]').prop('disabled', true);
        } else {
            $('input:radio[class=BranchType][id=BranchType0]').prop('disabled', false);
        }
    } else if (companyType == 2) {
        $('input:radio[class=BranchType][id=BranchType0]').prop('disabled', true);
    }
}

function CountryChangedNew(countryCode) {
    var companyTypeID = $('input[name=CompanyTypeID]:checked').val();

    UpdateItemCompanyNew(companyTypeID);

    ChangedDocumentType(companyTypeID);

    UpdatePlaceholderCompany(countryCode);

    UpdatePlaceholderName(countryCode);

    UpdateLableTaxNoNew();
}

function DocumentNameChangedNew(docName, docValue) {
    //console.log(docName);
    //console.log(docValue);
    if (docName == "DocumentNameID_-1") {
        var docNameID = docValue;
        if (docNameID == -1) {
            $("#docTypeOther").css("display", "block");
        } else {
            $("#docTypeOther").css("display", "none");
        }
    }
}

function JobTitleChangedNew(jobTitle) {
    //alert(s.GetValue());
    if (jobTitle == -1) {
        $("#OtherJobTitleShow").css("display", "block");
    } else {
        $("#OtherJobTitleShow").css("display", "none");
    }
}

function BusinessEntityChangedNew(businessEntity) {
    if (businessEntity == -1) {
        $("#divOtherBusEnt").css("display", "block");
        $("#txtOtherBusEnt").css("display", "block");
    } else {
        $("#txtOtherBusEnt").css("display", "none");
        $("#divOtherBusEnt").css("display", "none");
        $("#txtOtherBusEnt").val("");
    }
}

//function ServiceTypeContactIndexChangedNew() {
//    $('input[type=checkbox][name=ServiceTypeContact]').each(function () {
//        //console.log($(this).val());
//        //console.log(this.checked);
//        if ($(this).val() == 1) {
//            if (this.checked == true) {
//                $("#hidService1").val("1");
//            } else {
//                $("#hidService1").val("");
//            }
//        } else if ($(this).val() == 2) {
//            if (this.checked == true) {
//                $("#hidService2").val("2");
//                $(".ContactInvitationCodeShow").css("display", "block");
//            } else {
//                $("#hidService2").val("");
//                $(".ContactInvitationCodeShow").css("display", "none");
//                $("#ContactInvitationCode").val("");
//                $("#errorContactInvitationCode").css("display", "none");
//            }
//        }
//    });
//}

//function GetValueServiceTypeNew() {
//    //var indicesToSelect = new Array();

//    $('input[type=checkbox][name=ServiceTypeList]').each(function () {
//        if ($(this).val() == 2) {
//            if (this.checked == true) {
//                $(".InvitationCodeShow").css("display", "block");
//            } else {
//                $(".InvitationCodeShow").css("display", "none");
//            }
//        }
//    });
//    var indicesToSelect = $('input[type=checkbox][name=ServiceTypeList]:checked').length;
//    if (indicesToSelect > 0) {
//        $("#errorServiceType").css("display", "none");
//    }
//}


function GetValueServiceType(s, e) {
    var indicesToSelect = new Array();
    for (var i = 0; i < s.GetItemCount(); i++) {
        var item = s.GetItem(i);
        if (item != null) {
            //console.log(item.value + " ++++++++++ " + item.selected);
            if (item.value == 2 && item.selected == true) {
                $("#InvetationCode").css("display", "block");
            }
            else {
                $("#InvetationCode").css("display", "none");
            }
        }
    }
}

function ReadAgreementCheckEventNew() {
    var checked = $('input[type=checkbox][name=chkReadAgreement]:checked').length > 0;
    //console.log(checked);
    $("#btnSubmit").attr('disabled', !checked);
}