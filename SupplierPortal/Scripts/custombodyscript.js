﻿$(".btnSubmit").click(function (evt) {
    
    var countryCode = $("#hidCountryCode").val();

    if (countryCode == "TH" && (rdbBusinessTab2.GetSelectedIndex() == 1)) {
        //console.log("In if");
        var brNo = $("#ddlBrNo").val();
        //console.log(brNo);
        if (brNo == "") {
            //console.log("ddlBrNo Null");
            evt.preventDefault();
            $('a[href="#tabs-1"]').click();
            $("#ddlBrNo").focus();
            $("#BranchNoValidation").css("display", "block");

        }
    }

    $("#isSubmit").val(true);

});

$(".btnSave").click(function (evt) {
  
    var countryCode = $("#hidCountryCode").val();
    
    if (countryCode == "TH" && (rdbBusinessTab2.GetSelectedIndex() == 1))
    {
        //console.log("In if");
        var brNo = $("#ddlBrNo").val();
        //console.log(brNo);
        if (brNo == "")
        {
            //console.log("ddlBrNo Null");
            evt.preventDefault();
            $('a[href="#tabs-1"]').click();
            $("#ddlBrNo").focus();
            $("#BranchNoValidation").css("display", "block");
        }
    }

    $("#isSubmit").val(false);
    //$(this).trigger('click');
    //return true;
});

$.validator.unobtrusive.adapters.addSingleVal("requireddeliver", "errormessage");

$.validator.addMethod("requireddeliver", function (value, element, issameaddr) {
    if ((errormessage != null) || ((errormessage != ""))) {
        return false;
    }
    return true;
});

$.validator.unobtrusive.adapters.addSingleVal("exclude", "chars");
$.validator.addMethod("exclude", function (value, element, exclude) {
    if (value) {
        for (var i = 0; i < exclude.length; i++) {
            if (jQuery.inArray(exclude[i], value) != -1) {
                return false;
            }
        }
    }
    return true;
});

$.validator.unobtrusive.adapters.addSingleVal("duns", "errormessage");
$.validator.addMethod("duns", function (value, element, errormessage) {
    if ((errormessage != null) || ((errormessage != ""))) {
        return false;
    }
    return true;
});

$.validator.unobtrusive.adapters.addSingleVal("taxid", "errormessage");
$.validator.addMethod("taxid", function (value, element, errormessage) {
    if ((errormessage != null) || ((errormessage != ""))) {
        return false;
    }
    return true;
});

$.validator.unobtrusive.adapters.addSingleVal("myrequired", "errormessage");
$.validator.addMethod("myrequired", function (value, element, errormessage) {
    if ((errormessage != null) || ((errormessage != ""))) {
        return false;
    }
    return true;
});

$.validator.unobtrusive.adapters.addSingleVal("requiredsubmit", "errormessage");
$.validator.addMethod("requiredsubmit", function (value, element, errormessage) {
    if ((errormessage != null) || ((errormessage != ""))) {
        return false;
    }
    return true;
});

$.validator.unobtrusive.adapters.addSingleVal("requireddeliversubmit", "errormessage");
$.validator.addMethod("requireddeliversubmit", function (value, element, errormessage) {
    if ((errormessage != null) || ((errormessage != ""))) {
        return false;
    }
    return true;
});

$.validator.unobtrusive.adapters.addSingleVal("postalcode", "errormessage");
$.validator.addMethod("postalcode", function (value, element, errormessage) {
    if ((errormessage != null) || ((errormessage != ""))) {
        return false;
    }
    return true;
});

$.validator.unobtrusive.adapters.addSingleVal("taxidinfos", "errormessage");
$.validator.addMethod("taxidinfos", function (value, element, errormessage) {
    if ((errormessage != null) || ((errormessage != ""))) {
        return false;
    }
    return true;
});

$.validator.unobtrusive.adapters.addSingleVal("taxidinfosedit", "errormessage");
$.validator.addMethod("taxidinfosedit", function (value, element, errormessage) {
    if ((errormessage != null) || ((errormessage != ""))) {
        return false;
    }
    return true;
});

$(document).ready(function () {

    //----------------For change css class-----------
    $("#lnkEdtEmail").click(function () {
        //$("#EdtEmail").removeClass("textBoxDisStyle");
        //$("#EdtEmail").addClass("textBoxStyle");
        $("#EdtEmail").attr("readonly", false);
    });

    $("#lnkEdtName").click(function () {
        //$("#EdtFName").removeClass("textBoxDisStyle");
        //$("#EdtFName").addClass("textBoxStyle");
        $("#EdtFName").attr("readonly", false);

        //$("#EdtLName").removeClass("textBoxDisStyle");
        //$("#EdtLName").addClass("textBoxStyle");
        $("#EdtLName").attr("readonly", false);
    });

    $("#LnkEditCompName_local").click(function () {
        $("#txtComp_local").attr("readonly", false);
    });

    $("#LnkEditCompName_inter").click(function () {
        $("#txtComp_inter").attr("readonly", false);
    });

    $("#lnkEdtJobTitle").click(function () {
        //$("#txtJobTitle").removeClass("textBoxDisStyle");
        //$("#txtJobTitle").addClass("textBoxStyle");
        $("#txtJobTitle").attr("readonly", false);
    });

    $("#lnkEdtDepartment").click(function () {
        //$("#txtDepartment").removeClass("textBoxDisStyle");
        //$("#txtDepartment").addClass("textBoxStyle");
        $("#txtDepartment").attr("readonly", false);
    });

    $("#lnkEdContactName").click(function () {
        //$("#txtContactFName").removeClass("textBoxDisStyle-inline");
        //$("#txtContactFName").addClass("textBoxStyle-inline");
        $("#txtContactFName").attr("readonly", false);

        //$("#txtContactLName").removeClass("textBoxDisStyle-inline");
        //$("#txtContactLName").addClass("textBoxStyle-inline");
        $("#txtContactLName").attr("readonly", false);
    });

    $("#lnkEdtEmail").click(function () {
        //$("#txtContactEmail").removeClass("textBoxDisStyle");
        //$("#txtContactEmail").addClass("textBoxStyle");
        $("#txtContactEmail").attr("readonly", false);
    });
    //----------------------End----------------------

    //----------------For tab control----------------
    $(function () {
        $("#TestTabs").tabs();
    });
    $(function () {
        $("#BusinessTabs").tabs();
    });

    $(function () {
        $("#CompanyAddrTabs").tabs();
    });

    $(function () {
        $("#DeliverAddrTabs").tabs();
    });

    $(function () {
        $("#MainConTactTabs").tabs();
    });

    $(function () {
        $("#PersonalAddrTabs").tabs();
    });
    //----------------------End----------------------
    //----------------------For check box event----------------------- 
    if ($("#IsSameAddress_S").val() == "C") {
        $("#isSameAddress").val(true);
        $('.DvAddr').hide();
    }
    else {
        $("#isSameAddress").val(false);
        $('.DvAddr').show();
    }

    if ($("#CompanyTypeID_RB0_I").val() == "C") {
        $("#isCorperate").val(true);
    }
    else {
        $("#isCorperate").val(false);
    }

    if ($("#rdbBusinessTab2_RB0_I").val() == "C") {
        $("#isHead").val(true);
    }
    else {
        $("#isHead").val(false);
    }

    $("#btnAgree").attr('disabled', true);

    /*$("#btnDontAgree").click(function () {
        window.location.href = '/Landing/WhyVerification/';
    });*/

    
    $("#btnOK").click(function () {
        gridView.Refresh();
    });
    //------------------------------End-------------------------------   

});




