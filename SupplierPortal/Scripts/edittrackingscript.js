﻿
function keyThaiOnly(evt, str) {
    //createAutoClosingAlert(".alert-message", 500);
    var countryCode = $("#CountryCode option:selected").val();
    if (countryCode == "TH") {
        var k;
        if (window.event) k = window.event.keyCode; //  IE 
        else if (evt) k = evt.which; //  Firefox 

        if ((k >= 161 && k <= 255) || (k >= 3585 && k <= 3675)) {
            // THAI
            return true;
        } else {
            // Number
            if (k >= 48 && k <= 57) {
                return true;
            } else {
                if (k != 8 && k != 46 && k != 188
                    && k != 190 && k != 40 && k != 41
                    && k != 47 && k != 42 && k != 43 && k != 44
                    && k != 45 && k != 32) {
                    showAlertLocal();
                    return false;
                }
            }
        }
        // if ((k>=65 && k<=90) || (k>=97 && k<=122)) { return allowedEng; } 
    }

}

function keyEngOnly(evt, str) {

    var k;
    if (window.event) k = window.event.keyCode; //  IE 
    else if (evt) k = evt.which; //  Firefox 
    // English
    if ((k >= 65 && k <= 90) || (k >= 97 && k <= 122)) {
        return true;
    } else {
        // Number
        if (k >= 48 && k <= 57) {

            return true;
        } else {
            if (k != 8 && k != 46 && k != 188
				&& k != 190 && k != 40 && k != 41
				&& k != 47 && k != 42 && k != 43 && k != 44
				&& k != 45 && k != 32 && k != 38) {
                showAlertInter();
                return false;
            }
        }
    }
}

function keyBranchNoNumberOnly(evt, str) {
    //console.log(str);
    var countryCode = $("#CountryCode option:selected").val();
    if (countryCode == "TH") {
        var k;
        if (window.event) k = window.event.keyCode; //  IE 
        else if (evt) {
            k = evt.which; //  Firefox 
            if (k == 8) {
                return true;
            }

        }
        // Number

        if (str.length >= 5) {
            return false;
        }

        if (k >= 48 && k <= 57) {
            return true;
        } else {
            showAlertNumber();
            return false;
        }
    }

}

function showAlertLocal() {
    $("#myAlertLocal").css('z-index', '2000');
    $("#myAlertLocal").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertLocal").css('z-index', '-1');
    });
}

function showAlertInter() {
    $("#myAlertInter").css('z-index', '2000');
    $("#myAlertInter").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertInter").css('z-index', '-1');
    });
}

function showAlertNumber() {
    $("#myAlertNumber").css('z-index', '2000');
    $("#myAlertNumber").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertNumber").css('z-index', '-1');
    });
}