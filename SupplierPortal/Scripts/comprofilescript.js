﻿

function keyThaiOnly(evt, str) {
    //createAutoClosingAlert(".alert-message", 500);
    var countryCode = $("#CountryCode option:selected").val();

    if (countryCode == "TH") {
        var k;
        if (window.event) k = window.event.keyCode; //  IE 
        else if (evt) k = evt.which; //  Firefox 

        if ((k >= 161 && k <= 255) || (k >= 3585 && k <= 3675)) {
            // THAI
            return true;
        } else {
            // Number
            if (k >= 48 && k <= 57) {
                return true;
            } else {
                if (k != 8 && k != 46 && k != 188
                    && k != 190 && k != 40 && k != 41
                    && k != 47 && k != 42 && k != 43 && k != 44
                    && k != 45 && k != 32) {
                    showAlertLocal();
                    return false;
                }
            }
        }
        // if ((k>=65 && k<=90) || (k>=97 && k<=122)) { return allowedEng; } 
    }

}

function keyEngOnly(evt, str) {

    var k;
    if (window.event) k = window.event.keyCode; //  IE 
    else if (evt) k = evt.which; //  Firefox 
    // English
    if ((k >= 65 && k <= 90) || (k >= 97 && k <= 122)) {
        return true;
    } else {
        // Number
        if (k >= 48 && k <= 57) {

            return true;
        } else {
            if (k != 8 && k != 46 && k != 188
				&& k != 190 && k != 40 && k != 41
				&& k != 47 && k != 42 && k != 43 && k != 44
				&& k != 45 && k != 32 && k != 38) {
                showAlertInter();
                return false;
            }
        }
    }
}

function keyBranchNoNumberOnly(evt, str) {
    //console.log(str);
    var countryCode = $("#CountryCode option:selected").val();
    if (countryCode == "TH") {
        var k;
        if (window.event) k = window.event.keyCode; //  IE 
        else if (evt) {
            k = evt.which; //  Firefox 
            if (k == 8) {
                return true;
            }

        }
        // Number

        if (str.length >= 5) {
            return false;
        }

        if (k >= 48 && k <= 57) {
            return true;
        } else {
            showAlertNumber();
            return false;
        }
    }

}

function showAlertLocal() {
    $("#myAlertLocal").css('z-index', '2000');
    $("#myAlertLocal").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertLocal").css('z-index', '-1');
    });
}

function showAlertInter() {
    $("#myAlertInter").css('z-index', '2000');
    $("#myAlertInter").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertInter").css('z-index', '-1');
    });
}

function showAlertNumber() {
    $("#myAlertNumber").css('z-index', '2000');
    $("#myAlertNumber").fadeTo(3000, 500).slideUp(500, function () {
        $("#myAlertNumber").css('z-index', '-1');
    });
}

function ValidatRequire(evt) {
    var check = true;

    //var branchType = BranchType.GetSelectedIndex();
    var branchType = $('input:radio[class=BranchType][id=BranchType2]').is(':checked');

    var textBranchNo = $("#BranchNo").val();
    var textTaxID = $("#TaxID").val();
    var textCompanyName_Local = $("#CompanyName_Local").val();
    var textOtherBusiness = $("#txtOtherBusEnt").val();

    var businessEntityID = $("#BusinessEntityID option:selected").val();
    var textCompanyName_Inter = $("#CompanyName_Inter").val();

    var elementTaxID = $("#TaxID").hasClass("input-validation-error");
    //console.log(elementTaxID);

    if (branchType) {
        if (textBranchNo == "" || textBranchNo == undefined) {
            evt.preventDefault();
            $("#errorBranchNo").css("display", "block");
            $("#BranchNo").focus();
            check = false;
        } else {
            $("#errorBranchNo").css("display", "none");
        }
    } else {
        $("#errorBranchNo").css("display", "none");
    }

    if (textCompanyName_Inter == "" || textCompanyName_Inter == undefined) {
        evt.preventDefault();
        $("#errorCompanyName_Inter").css("display", "block");
        $("#CompanyName_Inter").focus();
        check = false;
    } else {
        $("#errorCompanyName_Inter").css("display", "none");
    }

    if (textCompanyName_Local == "" || textCompanyName_Local == undefined) {
        evt.preventDefault();
        $("#errorCompanyName_Local").css("display", "block");
        $("#CompanyName_Local").focus();
        check = false;
    } else {
        $("#errorCompanyName_Local").css("display", "none");
    }

    if (businessEntityID == -1) {
        if (textOtherBusiness == "" || textOtherBusiness == undefined) {
            evt.preventDefault();
            $("#errorOtherBusiness").css("display", "block");
            $("#txtOtherBusEnt").focus();
            check = false;
        } else {
            $("#errorOtherBusiness").css("display", "none");
        }
    }

    if (textTaxID == "" || textTaxID == undefined) {
        evt.preventDefault();
        var companyTypeID = $('input[name=CompanyTypeID]:checked').val();
        var countryCode = $("#CountryCode option:selected").val();

        if (companyTypeID == 1) {
            $("#errorTaxIDCorp").css("display", "block");
            $("#errorTaxIDOrdinary").css("display", "none");
        } else if (companyTypeID == 2) {
            if (countryCode == "TH") {
                $("#errorTaxIDOrdinary").css("display", "block");
                $("#errorTaxIDCorp").css("display", "none");
            } else {
                $("#errorTaxIDCorp").css("display", "block");
                $("#errorTaxIDOrdinary").css("display", "none");
            }
        }
        //$("#errorTaxID").css("display", "block");
        $("#TaxID").focus();
        check = false;
    } else {
        $("#errorTaxIDCorp").css("display", "none");
        $("#errorTaxIDOrdinary").css("display", "none");
        //$("#errorTaxID").css("display", "none");
    }

    if (elementTaxID == true) {
        evt.preventDefault();
        $("#errorTaxID").css("display", "none");
        $("#TaxID").focus();
        check = false;
    }

    //console.log(branchType);

    return check;
}

function ValidatTaxIDAsync(evt) {
    var check = true;

    var companyTypeID = $('input[name=CompanyTypeID]:checked').val();
    var countryCode = $("#CountryCode option:selected").val();
    var textTaxID = $("#TaxID").val();
    //console.log(textTaxID);
    if (textTaxID == "" || textTaxID == undefined) {
        console.log("textTaxID undefined");
        $("#errorFormatTaxIDCorp").css("display", "none");
        $("#errorFormatTaxIDOrdinary").css("display", "none");

        evt.preventDefault();

        if (companyTypeID == 1) {
            $("#errorTaxIDCorp").css("display", "block");
            $("#errorTaxIDOrdinary").css("display", "none");
        } else if (companyTypeID == 2) {
            if (countryCode == "TH") {
                $("#errorTaxIDOrdinary").css("display", "block");
                $("#errorTaxIDCorp").css("display", "none");
            } else {
                $("#errorTaxIDCorp").css("display", "block");
                $("#errorTaxIDOrdinary").css("display", "none");
            }
        }
        $("#TaxID").focus();
        check = false;
    } else {
        $("#errorTaxIDCorp").css("display", "none");
        $("#errorTaxIDOrdinary").css("display", "none");
    }
    if (check == true) {
        $.ajax({
            url: '/GeneralInformation/ValidateTaxID',
            type: 'GET',
            data: {
                TaxID: textTaxID,
                CountryCode_VI: countryCode,
                CompanyTypeID: companyTypeID
            },
            async: false,
            contentType: 'application/json',
            success: function (data) {
                //console.log(data);
                if (data != true) {
                    evt.preventDefault();
                    if (companyTypeID == 1) {
                        $("#errorFormatTaxIDCorp").css("display", "block");
                        $("#errorFormatTaxIDOrdinary").css("display", "none");
                        $("#errorTaxIDOrdinary").css("display", "none");
                        $("#errorTaxIDCorp").css("display", "none");
                    } else if (companyTypeID == 2) {
                        if (countryCode == "TH") {
                            $("#errorFormatTaxIDOrdinary").css("display", "block");
                            $("#errorFormatTaxIDCorp").css("display", "none");
                            $("#errorTaxIDOrdinary").css("display", "none");
                            $("#errorTaxIDCorp").css("display", "none");
                        } else {
                            $("#errorFormatTaxIDCorp").css("display", "block");
                            $("#errorFormatTaxIDOrdinary").css("display", "none");
                            $("#errorTaxIDOrdinary").css("display", "none");
                            $("#errorTaxIDCorp").css("display", "none");
                        }
                    }
                    $("#TaxID").focus();
                    check = false;
                } else {
                    $("#errorFormatTaxIDCorp").css("display", "none");
                    $("#errorFormatTaxIDOrdinary").css("display", "none");
                }
            },
            fail: function (event, data) {
                check = false;
            }
        });
    }

    return check;
}

function ValidatDuplicateTexID(evt)
{
    var textTaxID = $("#TaxID").val();
    var oldTaxID = $("#hidTaxID").val();
    var branchNo = $("#BranchNo").val();

    var check = true;

    $.ajax({
        url: '/GeneralInformation/ValidatDuplicateTexID',
        type: 'GET',
        data: {
            OldTaxID: oldTaxID,
            TaxID: textTaxID,
            BranchNo: branchNo
        },
        async: false,
        contentType: 'application/json',
        success: function (data) {
            console.log(data);
            if (data != true) {
                evt.preventDefault();
                $("#errorDuplicateTexID").css("display", "block");
                $("#TaxID").focus();
                check = false;
            } else {
                $("#errorDuplicateTexID").css("display", "none");
            }
        },
        fail: function (event, data) {
            check = false;
        }
    });

    return check;
}


function ValidatDUNSNumberAsync(evt) {
    var check = true;

    var dUNSNumber = $("#DUNSNumber").val();

    if (dUNSNumber.trim() != "") {
        console.log("DUNSNumber");
        $.ajax({
            url: '/GeneralInformation/ValidateDUNSNumber',
            type: 'GET',
            data: {
                DUNSNumber: dUNSNumber
            },
            async: false,
            contentType: 'application/json',
            success: function (data) {
                //console.log(data);
                if (data != true) {
                    evt.preventDefault();
                    check = false;
                    $("#DUNSNumber").focus();
                }
            },
            fail: function (event, data) {
                check = false;
            }
        });
    }
    return check;
}
function onTokensChanged(s, e) {
    hideSelectedTokenBox(s);
}
function Buyer_Init(s, e) {
    hideSelectedTokenBox(s);
}

function onEndCallback_Buyer(s, e) {
    hideSelectedTokenBox(s);
}

var hideSelectedTokenBox = function (s) {
    var tr = $('#Address_BuyerList_DDD_L_LBT').find('tr');
    tr.show();
    var selectedTokenCollection = s.GetTokenCollection();
    selectedTokenCollection.forEach(function (text) {
        var item = s.FindItemByText(text);

        if (item) {
            tr.eq(item.index).hide();
        }

    });
}
var selectedIDs;
function selectionChanged(s, e) {
    s.GetSelectedFieldValues("PurGroupId;PurGroupName", GetSelectedFieldValuesCallback);
}

function buyerInit(s, e) {
    if (s.cpCheckedKeys != null) {
        var keys = [s.cpCheckedKeys.split(',')]
        keys.forEach(function (item) {
            s.SelectRowsByKey(item, true);
        });
    }
}
function GetSelectedFieldValuesCallback(values) {
    selectedIDs = [];
    SelectedRows.BeginUpdate();
    try {
        SelectedRows.ClearItems();
        for (var i = 0; i < values.length; i++) {
            selectedIDs.push(values[i][0]);
            SelectedRows.AddItem(values[i][1]);
        }
    } finally {
        SelectedRows.EndUpdate();
    }
    $("#count").html(BuyerList.GetSelectedRowCount());

    // selectedIDs = values[0].join(',');
    //alert(selectedIDs);
}

function markMatch(text, term) {
    //console.log(text);
    //console.log(term);
    // Find where the match is
    var match = text.toUpperCase().indexOf(term.toUpperCase());

    var $result = $('<span></span>');

    // If there is no match, move on
    if (match < 0) {
        return $result.text(text);
    }

    // Put in whatever text is before the match
    $result.text(text.substring(0, match));

    // Mark the match
    var $match = $('<span class="select2-rendered__match"></span>');
    $match.text(text.substring(match, match + term.length));

    // Append the matching text
    $result.append($match);

    // Put in whatever is after the match
    $result.append(text.substring(match + term.length));

    return $result;
}

function matchStart(term, text) {
    if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
        return true;
    }

    return false;
}

var checkbox_val = [];

$(document).ready(function () {

    //check box
    if ($("#CheckB").is(':checked')) {
        $("#DeliveredAdd").hide();
    }

    $('#CheckB').change(function () {
        if (!this.checked)
            $('#DeliveredAdd').fadeIn();
        else
            $('#DeliveredAdd').fadeOut();

        if (this.checked) {
            $("#DeliverAddress_HouseNo_Inter").validate({
                ignore: ".ignore"
            })
        }

    });

    //Validate btn
    $("#btnSubmitAddress").click(function (evt) {
        //evt.preventDefault();


        var CompcountryCode = $("#CompanyAddress_CountryCode").val();
        var DelvcountryCode = $("#DeliverAddress_CountryCode").val();

        var compSubDistrict = $("#CompanyAddress_SubDistrict_Local").val();
        var compCity = $("#CompanyAddress_City_Local").val();
        var compState = $("#CompanyAddress_State_Local").val();
        var compSubDistrict2 = $("#CompanyAddress_SubDistrict_Inter").val();
        var compCity2 = $("#CompanyAddress_City_Inter").val();
        var compState2 = $("#CompanyAddress_State_Inter").val();

        var deliSubDistrict = $("#DeliverAddress_SubDistrict_Local").val();
        var deliCity = $("#DeliverAddress_City_Local").val();
        var deliState = $("#DeliverAddress_State_Local").val();
        var deliSubDistrict2 = $("#DeliverAddress_SubDistrict_Inter").val();
        var deliCity2 = $("#DeliverAddress_City_Inter").val();
        var deliState2 = $("#DeliverAddress_State_Inter").val();

        var chk = true;
        var chkData = false;

        if (CompcountryCode == "TH") {
            //Sub companyAddress
            if (compSubDistrict != null) {
                compSubDistrict = $.trim(compSubDistrict);
            }
            if (compSubDistrict == "" || compSubDistrict == undefined || compSubDistrict == null) {
                evt.preventDefault();
                $('#CompanyAddress_SubDistrict_Local').val("");
                $('#CompanyAddress_SubDistrict_Local').focus();
                $("#errorCompSubDistrict").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#CompanyAddress_SubDistrict_Local').val(compSubDistrict);
                $("#errorCompSubDistrict").css("display", "none");
                chk = true;
            }
            if (compSubDistrict2 != null) {
                compSubDistrict2 = $.trim(compSubDistrict2);
            }
            if (compSubDistrict2 == "" || compSubDistrict2 == undefined || compSubDistrict2 == null) {
                evt.preventDefault();
                $('#CompanyAddress_SubDistrict_Inter').val("");
                $('#CompanyAddress_SubDistrict_Inter').focus();
                $("#errorCompSubDistrict2").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#CompanyAddress_SubDistrict_Inter').val(compSubDistrict2);
                $("#errorCompSubDistrict2").css("display", "none");
                chk = true;
            }
        }
        //City companyAddress
        if (compCity != null) {
            compCity = $.trim(compCity);
        }
        if (compCity == "" || compCity == undefined || compCity == null) {
            evt.preventDefault();
            $('#CompanyAddress_City_Local').val("");
            $('#CompanyAddress_City_Local').focus();
            $("#errorCompCity").css("display", "block");
            chk = false;
            return false;
        } else {
            $('#CompanyAddress_City_Local').val(compCity);
            $("#errorCompCity").css("display", "none");
            chk = true;
        }
        if (compCity2 != null) {
            compCity2 = $.trim(compCity2);
        }
        if (compCity2 == "" || compCity2 == undefined || compCity2 == null) {
            evt.preventDefault();
            $('#CompanyAddress_City_Inter').val("");
            $('#CompanyAddress_City_Inter').focus();
            $("#errorCompCity2").css("display", "block");
            chk = false;
            return false;
        } else {
            $('#CompanyAddress_City_Inter').val(compCity2);
            $("#errorCompCity2").css("display", "none");
            chk = true;
        }
        //State companyAddress
        if (compState != null) {
            compState = $.trim(compState);
        }
        if (compState == "" || compState == undefined || compState == null) {
            evt.preventDefault();
            $('#CompanyAddress_State_Local').val("");
            $('#CompanyAddress_State_Local').focus();
            $("#errorCompState").css("display", "block");
            chk = false;
            return false;
        } else {
            $('#CompanyAddress_State_Local').val(compState);
            $("#errorCompState").css("display", "none");
            chk = true;
        }
        if (compState2 != null) {
            compState2 = $.trim(compState2);
        }
        if (compState2 == "" || compState2 == undefined || compState2 == null) {
            evt.preventDefault();
            $('#CompanyAddress_State_Inter').val("");
            $('#CompanyAddress_State_Inter').focus();
            $("#errorCompState2").css("display", "block");
            chk = false;
            return false;
        } else {
            $('#CompanyAddress_State_Inter').val(compState2);
            $("#errorCompState2").css("display", "none");
            chk = true;
        }


        if ($("#CheckB").prop("checked") == false) {
            if (DelvcountryCode == "TH") {
                //Sub DeliverAddress
                if (deliSubDistrict != null) {
                    deliSubDistrict = $.trim(deliSubDistrict);
                }
                if (deliSubDistrict == "" || deliSubDistrict == undefined || deliSubDistrict == null) {
                    evt.preventDefault();
                    $('#DeliverAddress_SubDistrict_Local').val("");
                    $('#DeliverAddress_SubDistrict_Local').focus();
                    $("#errorDelvSubDistrict").css("display", "block");
                    chk = false;
                    return false;
                } else {
                    $('#DeliverAddress_SubDistrict_Local').val(deliSubDistrict);
                    $("#errorDelvSubDistrict").css("display", "none");
                    chk = true;
                }
                if (deliSubDistrict2 != null) {
                    deliSubDistrict2 = $.trim(deliSubDistrict2);
                }
                if (deliSubDistrict2 == "" || deliSubDistrict2 == undefined || deliSubDistrict2 == null) {
                    evt.preventDefault();
                    $('#DeliverAddress_SubDistrict_Inter').val("");
                    $('#DeliverAddress_SubDistrict_Inter').focus();
                    $("#errorDelvSubDistrict2").css("display", "block");
                    chk = false;
                    return false;
                } else {
                    $('#DeliverAddress_SubDistrict_Inter').val(deliSubDistrict2);
                    $("#errorDelvSubDistrict2").css("display", "none");
                    chk = true;
                }
            }
            //City deliberAddress
            if (deliCity != null) {
                deliCity = $.trim(deliCity);
            }
            if (deliCity == "" || deliCity == undefined || deliCity == null) {
                evt.preventDefault();
                $('#DeliverAddress_City_Local').val("");
                $('#DeliverAddress_City_Local').focus();
                $("#errorDelvCity").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#DeliverAddress_City_Local').val(deliCity);
                $("#errorDelvCity").css("display", "none");
                chk = true;
            }
            if (deliCity2 != null) {
                deliCity2 = $.trim(deliCity2);
            }
            if (deliCity2 == "" || deliCity2 == undefined || deliCity2 == null) {
                evt.preventDefault();
                $('#DeliverAddress_City_Inter').val("");
                $('#DeliverAddress_City_Inter').focus();
                $("#errorDelvCity2").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#DeliverAddress_City_Inter').val(deliCity2);
                $("#errorDelvCity2").css("display", "none");
                chk = true;
            }
            //State deliverAddress
            if (deliState != null) {
                deliState = $.trim(deliState);
            }
            if (deliState == "" || deliState == undefined || deliState == null) {
                evt.preventDefault();
                $('#DeliverAddress_State_Local').val("");
                $('#DeliverAddress_State_Local').focus();
                $("#errorDelvState").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#DeliverAddress_State_Local').val(deliState);
                $("#errorDelvState").css("display", "none");
                chk = true;
            }
            if (deliState2 != null) {
                deliState2 = $.trim(deliState2);
            }
            if (deliState2 == "" || deliState2 == undefined || deliState2 == null) {
                evt.preventDefault();
                $('#DeliverAddress_State_Inter').val("");
                $('#DeliverAddress_State_Inter').focus();
                $("#errorDelvState2").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#DeliverAddress_State_Inter').val(deliState2);
                $("#errorDelvState2").css("display", "none");
                chk = true;
            }
        }



        //trackingAddress
        var flag = false;
        $("input[data-tracking=1]").each(function (index) {
            var idtracking = $(this).attr('id');

            if (idtracking != undefined) {
                var name = document.getElementById(idtracking);

                if (name.value != name.defaultValue) {
                    flag = true;
                    chkData = true;
                    return false;
                }
            }

        });


        if (flag == false) {
            $("select[data-tracking=1]").each(function (index) {
                var idTracking = $(this).attr('id');

                //var comboBox = ASPxClientControl.GetControlCollection().GetByName(idTracking);
                //var name = document.getElementById(comboBox.name + "_I");

                var name = document.getElementById(idTracking);
                var defaultVal = $(this).data("defaultselected");

                //var oldVal = name.options[name.selectedIndex].value != name.options[0].value;
                //var newVal = name.value;
                //console.log("name ==> " + name.value + ":" + defaultVal);
                if (name.value != defaultVal) {
                    flag = true;
                    chkData = true;
                    return false;
                }
            });
            if (flag == false) {
                $("input[data-check=1]").each(function (index) {
                    var idTracking = $(this).attr('id');
                    var value1 = $("#CheckB").is(':checked');
                    var value2 = $("#CheckBool").val();
                    var bool1 = value1.toString().toLowerCase();
                    var bool2 = value2.toString().toLowerCase();

                    if (bool2 != bool1) {
                        flag = true;
                        return false;
                    }
                });
            }
        }

        $("#chkDataChange").val(chkData);

        var $form = $('#AddressID');
        if ($form.valid() && chk == true && flag) {
            //CompanyTypeID.SetEnabled(true);
            //BusinessEntityID.SetEnabled(true);

            //CompanyAddress_CountryCode.SetEnabled(true);
            //DeliverAddress_CountryCode.SetEnabled(true);

            if ($("#CheckB").prop("checked") == false) {
                //DeliverAddress_SubDistrict_Local.SetEnabled(true);
                //DeliverAddress_City_Local.SetEnabled(true);
                //DeliverAddress_State_Local.SetEnabled(true);
                //DeliverAddress_SubDistrict_Inter.SetEnabled(true);
                //DeliverAddress_City_Inter.SetEnabled(true);
                //DeliverAddress_State_Inter.SetEnabled(true);
            }
            //console.log("submit");
            disablePage();
            //$form.submit();
            document.getElementById("AddressID").submit();
        }
        else {
            $form.submit(function (event) {
                event.preventDefault();
                return false;
            });
        }

    });

    $("#btnSubmitAddress1").click(function (evt) {
        //evt.preventDefault();

        var CompcountryCode = $("#CompanyAddress_CountryCode").val();
        var DelvcountryCode = $("#DeliverAddress_CountryCode").val();


        var compSubDistrict = $("#CompanyAddress_SubDistrict_Local").val();
        var compCity = $("#CompanyAddress_City_Local").val();
        var compState = $("#CompanyAddress_State_Local").val();
        var compSubDistrict2 = $("#CompanyAddress_SubDistrict_Inter").val();
        var compCity2 = $("#CompanyAddress_City_Inter").val();
        var compState2 = $("#CompanyAddress_State_Inter").val();

        var deliSubDistrict = $("#DeliverAddress_SubDistrict_Local").val();
        var deliCity = $("#DeliverAddress_City_Local").val();
        var deliState = $("#DeliverAddress_State_Local").val();
        var deliSubDistrict2 = $("#DeliverAddress_SubDistrict_Inter").val();
        var deliCity2 = $("#DeliverAddress_City_Inter").val();
        var deliState2 = $("#DeliverAddress_State_Inter").val();

        var chk = true;
        var chkData = false;

        if (CompcountryCode == "TH") {
            //Sub companyAddress
            if (compSubDistrict != null) {
                compSubDistrict = $.trim(compSubDistrict);
            }
            if (compSubDistrict == "" || compSubDistrict == undefined || compSubDistrict == null) {
                evt.preventDefault();
                $('#CompanyAddress_SubDistrict_Local').val("");
                $('#CompanyAddress_SubDistrict_Local').focus();
                $("#errorCompSubDistrict").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#CompanyAddress_SubDistrict_Local').val(compSubDistrict);
                $("#errorCompSubDistrict").css("display", "none");
                chk = true;
            }
            if (compSubDistrict2 != null) {
                compSubDistrict2 = $.trim(compSubDistrict2);
            }
            if (compSubDistrict2 == "" || compSubDistrict2 == undefined || compSubDistrict2 == null) {
                evt.preventDefault();
                $('#CompanyAddress_SubDistrict_Inter').val("");
                $('#CompanyAddress_SubDistrict_Inter').focus();
                $("#errorCompSubDistrict2").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#CompanyAddress_SubDistrict_Inter').val(compSubDistrict2);
                $("#errorCompSubDistrict2").css("display", "none");
                chk = true;
            }
        }
        //City companyAddress
        if (compCity != null) {
            compCity = $.trim(compCity);
        }
        if (compCity == "" || compCity == undefined || compCity == null) {
            evt.preventDefault();
            $('#CompanyAddress_City_Local').val("");
            $('#CompanyAddress_City_Local').focus();
            $("#errorCompCity").css("display", "block");
            chk = false;
            return false;
        } else {
            $('#CompanyAddress_City_Local').val(compCity);
            $("#errorCompCity").css("display", "none");
            chk = true;
        }
        if (compCity2 != null) {
            compCity2 = $.trim(compCity2);
        }
        if (compCity2 == "" || compCity2 == undefined || compCity2 == null) {
            evt.preventDefault();
            $('#CompanyAddress_City_Inter').val("");
            $('#CompanyAddress_City_Inter').focus();
            $("#errorCompCity2").css("display", "block");
            chk = false;
            return false;
        } else {
            $('#CompanyAddress_City_Inter').val(compCity2);
            $("#errorCompCity2").css("display", "none");
            chk = true;
        }
        //State companyAddress
        if (compState != null) {
            compState = $.trim(compState);
        }
        if (compState == "" || compState == undefined || compState == null) {
            evt.preventDefault();
            $('#CompanyAddress_State_Local').val("");
            $('#CompanyAddress_State_Local').focus();
            $("#errorCompState").css("display", "block");
            chk = false;
            return false;
        } else {
            $('#CompanyAddress_State_Local').val(compState);
            $("#errorCompState").css("display", "none");
            chk = true;
        }
        if (compState2 != null) {
            compState2 = $.trim(compState2);
        }
        if (compState2 == "" || compState2 == undefined || compState2 == null) {
            evt.preventDefault();
            $('#CompanyAddress_State_Inter').val("");
            $('#CompanyAddress_State_Inter').focus();
            $("#errorCompState2").css("display", "block");
            chk = false;
            return false;
        } else {
            $('#CompanyAddress_State_Inter').val(compState2);
            $("#errorCompState2").css("display", "none");
            chk = true;
        }


        if ($("#CheckB").prop("checked") == false) {
            if (DelvcountryCode == "TH") {
                //Sub DeliverAddress
                if (deliSubDistrict != null) {
                    deliSubDistrict = $.trim(deliSubDistrict);
                }
                if (deliSubDistrict == "" || deliSubDistrict == undefined || deliSubDistrict == null) {
                    evt.preventDefault();
                    $('#DeliverAddress_SubDistrict_Local').val("");
                    $('#DeliverAddress_SubDistrict_Local').focus();
                    $("#errorDelvSubDistrict").css("display", "block");
                    chk = false;
                    return false;
                } else {
                    $('#DeliverAddress_SubDistrict_Local').val(deliSubDistrict);
                    $("#errorDelvSubDistrict").css("display", "none");
                    chk = true;
                }
                if (deliSubDistrict2 != null) {
                    deliSubDistrict2 = $.trim(deliSubDistrict2);
                }
                if (deliSubDistrict2 == "" || deliSubDistrict2 == undefined || deliSubDistrict2 == null) {
                    evt.preventDefault();
                    $('#DeliverAddress_SubDistrict_Inter').val("");
                    $('#DeliverAddress_SubDistrict_Inter').focus();
                    $("#errorDelvSubDistrict2").css("display", "block");
                    chk = false;
                    return false;
                } else {
                    $('#DeliverAddress_SubDistrict_Inter').val(deliSubDistrict2);
                    $("#errorDelvSubDistrict2").css("display", "none");
                    chk = true;
                }
            }
            //City deliberAddress
            if (deliCity != null) {
                deliCity = $.trim(deliCity);
            }
            if (deliCity == "" || deliCity == undefined || deliCity == null) {
                evt.preventDefault();
                $('#DeliverAddress_City_Local').val("");
                $('#DeliverAddress_City_Local').focus();
                $("#errorDelvCity").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#DeliverAddress_City_Local').val(deliCity);
                $("#errorDelvCity").css("display", "none");
                chk = true;
            }
            if (deliCity2 != null) {
                deliCity2 = $.trim(deliCity2);
            }
            if (deliCity2 == "" || deliCity2 == undefined || deliCity2 == null) {
                evt.preventDefault();
                $('#DeliverAddress_City_Inter').val("");
                $('#DeliverAddress_City_Inter').focus();
                $("#errorDelvCity2").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#DeliverAddress_City_Inter').val(deliCity2);
                $("#errorDelvCity2").css("display", "none");
                chk = true;
            }
            //State deliverAddress
            if (deliState != null) {
                deliState = $.trim(deliState);
            }
            if (deliState == "" || deliState == undefined || deliState == null) {
                evt.preventDefault();
                $('#DeliverAddress_State_Local').val("");
                $('#DeliverAddress_State_Local').focus();
                $("#errorDelvState").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#DeliverAddress_State_Local').val(deliState);
                $("#errorDelvState").css("display", "none");
                chk = true;
            }
            if (deliState2 != null) {
                deliState2 = $.trim(deliState2);
            }
            if (deliState2 == "" || deliState2 == undefined || deliState2 == null) {
                evt.preventDefault();
                $('#DeliverAddress_State_Inter').val("");
                $('#DeliverAddress_State_Inter').focus();
                $("#errorDelvState2").css("display", "block");
                chk = false;
                return false;
            } else {
                $('#DeliverAddress_State_Inter').val(deliState2);
                $("#errorDelvState2").css("display", "none");
                chk = true;
            }
        }



        //trackingAddress
        var flag = false;
        $("input[data-tracking=1]").each(function (index) {
            var idtracking = $(this).attr('id');

            if (idtracking != undefined) {
                var name = document.getElementById(idtracking);

                if (name.value != name.defaultValue) {
                    flag = true;
                    chkData = true;
                    return false;
                }
            }

        });


        if (flag == false) {
            $("select[data-tracking=1]").each(function (index) {
                var idTracking = $(this).attr('id');

                //var comboBox = ASPxClientControl.GetControlCollection().GetByName(idTracking);
                //var name = document.getElementById(comboBox.name + "_I");

                var name = document.getElementById(idTracking);
                var defaultVal = $(this).data("defaultselected");

                if (name.value != defaultVal) {
                    flag = true;
                    chkData = true;
                    return false;
                }
            });
            if (flag == false) {
                $("input[data-check=1]").each(function (index) {
                    var idTracking = $(this).attr('id');
                    var value1 = $("#CheckB").is(':checked');
                    var value2 = $("#CheckBool").val();
                    var bool1 = value1.toString().toLowerCase();
                    var bool2 = value2.toString().toLowerCase();

                    if (bool2 != bool1) {
                        flag = true;
                        return false;
                    }
                });
            }
        }

        $("#chkDataChange").val(chkData);

        var $form = $('#AddressID');
        if ($form.valid() && chk == true && flag) {
            //CompanyTypeID.SetEnabled(true);
            //BusinessEntityID.SetEnabled(true);

            // CompanyAddress_CountryCode.SetEnabled(true);
            //DeliverAddress_CountryCode.SetEnabled(true);

            //CompcountryCode.SetEnabled(true);
            //DelvcountryCode.SetEnabled(true);
            if ($("#CheckB").prop("checked") == false) {
                //DeliverAddress_SubDistrict_Local.SetEnabled(true);
                //DeliverAddress_City_Local.SetEnabled(true);
                //DeliverAddress_State_Local.SetEnabled(true);
                //DeliverAddress_SubDistrict_Inter.SetEnabled(true);
                //DeliverAddress_City_Inter.SetEnabled(true);
                //DeliverAddress_State_Inter.SetEnabled(true);
            }

            disablePage();
            //$form.submit();

            document.getElementById("AddressID").submit();
        }
        else {
            $form.submit(function (event) {
                event.preventDefault();
                return false;
            });
        }

    });

    $("#CompanyAddress_SubDistrict_Local").focusout(function () {

        var CompcountryCode = $("#CompanyAddress_CountryCode").val();

        var compSubDistrict = $("#CompanyAddress_SubDistrict_Local").val();
        if (CompcountryCode == "TH") {
            if (compSubDistrict == "" || compSubDistrict == undefined) {
                $("#errorCompSubDistrict").css("display", "block");
            } else {
                $("#errorCompSubDistrict").css("display", "none");
            }
        }
        else {
            $("#errorCompSubDistrict").css("display", "none");
        }
    });

    $("#CompanyAddress_City_Local").focusout(function () {
        var compCity = $("#CompanyAddress_City_Local").val();
        if (compCity == "" || compCity == undefined) {
            $("#errorCompCity").css("display", "block");
        } else {
            $("#errorCompCity").css("display", "none");
        }
    });

    $("#CompanyAddress_State_Local").focusout(function () {
        var compState = $("#CompanyAddress_State_Local").val();
        if (compState == "" || compState == undefined) {
            $("#errorCompState").css("display", "block");
        } else {
            $("#errorCompState").css("display", "none");
        }
    });

    $("#CompanyAddress_SubDistrict_Inter").focusout(function () {

        var CompcountryCode = $("#CompanyAddress_CountryCode").val();
        var compSubDistrict2 = $("#CompanyAddress_SubDistrict_Inter").val();
        if (CompcountryCode == "TH") {
            if (compSubDistrict2 == "" || compSubDistrict2 == undefined) {
                $("#errorCompSubDistrict2").css("display", "block");
            } else {
                $("#errorCompSubDistrict2").css("display", "none");
            }
        }
        else {
            $("#errorCompSubDistrict2").css("display", "none");
        }
    });

    $("#CompanyAddress_City_Inter").focusout(function () {
        var compCity2 = $("#CompanyAddress_City_Inter").val();
        if (compCity2 == "" || compCity2 == undefined) {
            $("#errorCompCity2").css("display", "block");
        } else {
            $("#errorCompCity2").css("display", "none");
        }
    });

    $("#CompanyAddress_State_Inter").focusout(function () {
        var compState2 = $("#CompanyAddress_State_Inter").val();
        if (compState2 == "" || compState2 == undefined) {
            $("#errorCompState2").css("display", "block");
        } else {
            $("#errorCompState2").css("display", "none");
        }
    });

    //delivarAddress
    $("#DeliverAddress_SubDistrict_Local").focusout(function () {
        var DelvcountryCode = $("#DeliverAddress_CountryCode").val();
        var delvSubDistrict = $("#DeliverAddress_SubDistrict_Local").val();
        if (DelvcountryCode == "TH") {
            if (delvSubDistrict == "" || delvSubDistrict == undefined) {
                $("#errorDelvSubDistrict").css("display", "block");
            } else {
                $("#errorDelvSubDistrict").css("display", "none");
            }
        }
        else {
            $("#errorDelvSubDistrict").css("display", "none");
        }
    });

    $("#DeliverAddress_City_Local").focusout(function () {
        var delvCity = $("#DeliverAddress_City_Local").val();
        if (delvCity == "" || delvCity == undefined) {
            $("#errorDelvCity").css("display", "block");
        } else {
            $("#errorDelvCity").css("display", "none");
        }
    });

    $("#DeliverAddress_State_Local").focusout(function () {
        var delvState = $("#DeliverAddress_State_Local").val();
        if (delvState == "" || delvState == undefined) {
            $("#errorDelvState").css("display", "block");
        } else {
            $("#errorDelvState").css("display", "none");
        }
    });

    $("#DeliverAddress_SubDistrict_Inter").focusout(function () {
        var DelvcountryCode = $("#DeliverAddress_CountryCode").val();
        var delvSubDistrict2 = $("#DeliverAddress_SubDistrict_Inter").val();
        if (DelvcountryCode == "TH") {
            if (delvSubDistrict2 == "" || delvSubDistrict2 == undefined) {
                $("#errorDelvSubDistrict2").css("display", "block");
            } else {
                $("#errorDelvSubDistrict2").css("display", "none");
            }
        }
        else {
            $("#errorDelvSubDistrict2").css("display", "none");
        }
    });

    $("#DeliverAddress_City_Inter").focusout(function () {
        var delvCity2 = $("#DeliverAddress_City_Inter").val();
        if (delvCity2 == "" || delvCity2 == undefined) {
            $("#errorDelvCity2").css("display", "block");
        } else {
            $("#errorDelvCity2").css("display", "none");
        }
    });

    $("#DeliverAddress_State_Inter").focusout(function () {
        var delvState2 = $("#DeliverAddress_State_Inter").val();
        if (delvState2 == "" || delvState2 == undefined) {
            $("#errorDelvState2").css("display", "block");
        } else {
            $("#errorDelvState2").css("display", "none");
        }
    });

    String.prototype.format = function () {
        a = this;
        for (k in arguments) {
            a = a.replace("{" + k + "}", arguments[k])
        }
        return a
    }
});
