﻿using Newtonsoft.Json.Linq;
using SupplierPortal.Data.Models.MappingTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace SupplierPortal.ServiceClients
{
    public class TermAndConditionService : APIConnectionService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public async Task<JObject> GetTermAndCondition(string username)
        {
            JObject result = null;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = string.Format("api/term/get/{0}", username);
                httpResponse = await this.GetApiConnection(url);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseBody = httpResponse.Content.ReadAsStringAsync().Result;
                    result = JObject.Parse(responseBody);
                }
            }
            catch (Exception ex)
            {
                logger.Error("GetTermAndCondition : " + ex.InnerException);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }

        public async Task<JObject> GetCOC(string username)
        {
            JObject result = null;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = string.Format("api/coc/get/{0}", username);
                httpResponse = await this.GetApiConnection(url);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseBody = httpResponse.Content.ReadAsStringAsync().Result;
                    result = JObject.Parse(responseBody);
                }
            }
            catch (Exception ex)
            {
                logger.Error("GetCOC : " + ex.InnerException);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return result;
        }
    }
}