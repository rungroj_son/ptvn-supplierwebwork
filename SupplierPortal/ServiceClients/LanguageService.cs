﻿using Newtonsoft.Json.Linq;
using SupplierPortal.Data.Models.MappingTable;
using SupplierPortal.Models.Dto.DataContract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace SupplierPortal.ServiceClients
{
    public class LanguageService : APIConnectionService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public async Task<List<Tbl_Language>> GetLanguage()
        {
            List<Tbl_Language> language = null;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string url = "api/Language/GetLanguage";
                httpResponse = await this.GetApiConnection(url);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Stream responseString = httpResponse.Content.ReadAsStreamAsync().Result;
                    StreamReader r = new StreamReader(responseString);
                    string jsonResult = r.ReadToEnd();
                    JavaScriptSerializer JsonConvert = new JavaScriptSerializer();
                    language = JsonConvert.Deserialize<List<Tbl_Language>>(jsonResult);
                }
            }
            catch (Exception ex)
            {
                logger.Error("GetLanguage Service(WebSite) : " + ex);
            }
            return language;
        }

        public async Task<string> GetLanguageIdByCode(string languageCode)
        {
            string languageId = string.Empty;
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                string parameter = string.Format("?languageCode={0}", languageCode);
                string url = string.Format("api/Language/GetLanguageIdByCode{0}", parameter);
                httpResponse = await this.GetApiConnection(url);
                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseBody = httpResponse.Content.ReadAsStringAsync().Result;
                    JObject _response = JObject.Parse(responseBody);

                    languageId = _response["LanguageID"].ToString();

                }
            }
            catch (Exception ex)
            {
                logger.Error("GetLanguageIdByCode Service(WebSite) : " + ex);
            }
            finally
            {
                httpResponse.Dispose();
            }
            return languageId;
        }
    }
}