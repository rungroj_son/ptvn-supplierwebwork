﻿using Newtonsoft.Json;
using SupplierPortal.Core.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace SupplierPortal.ServiceClients
{
    public class APIConnectionService
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public async Task<HttpResponseMessage> PostApiConnection(string url, Object model)
        {
            HttpClient client = new HttpClient();
            try
            {

                string languageURL = string.Empty;
                string baseURL = WebConfigurationManager.AppSettings["baseURL"];

                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string Jsonstr = JsonConvert.SerializeObject(model);
                HttpContent content = new StringContent(Jsonstr, Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                string urlAll = string.Format(url, baseURL);
                //string urlAll = string.Format("{0}api/AccountPortal/ResetPassword{1}", baseURL, languageURL);

                HttpResponseMessage response = await client.PostAsync(urlAll, content);

                return response;
            }
            catch (Exception ex)
            {
                logger.Error("APIConnectionService Method Post :" + ex);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }

        }

        public async Task<HttpResponseMessage> GetApiConnection(string url, string strUsername = "", string strPassword = "")
        {
            HttpClient client = new HttpClient();
            try
            {

                if (!string.IsNullOrEmpty(strUsername) && !string.IsNullOrEmpty(strPassword))
                {
                    client = SetHeaderLoginPortal(strUsername, strPassword);
                }

                string languageURL = string.Empty;
                string baseURL = WebConfigurationManager.AppSettings["baseURL"];

                client.BaseAddress = new Uri(baseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync(url);

                return response;
            }
            catch (Exception ex)
            {
                logger.Error("APIConnectionService Method Get :" + ex);
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        private HttpClient SetHeaderLoginPortal(string strUserName, string userPassword)
        {
            //string publicKey = "89844013dc5f506455e8dd3a48e5b4a9";
            string publicKey = WebConfigurationManager.AppSettings["publicKey"];
            //string strUserName = "portal@pantavanij.com";
            //string secretKey = "769fa783018bf7ff350697f5d7a218be";
            string secretKey = WebConfigurationManager.AppSettings["secretKey"];
            //string baseURL = "http://localhost:3867/";
            string baseURL = WebConfigurationManager.AppSettings["baseURL"];
            string signature = CryptoExtension.Signature(publicKey, secretKey);

            string authInfo = strUserName + ":" + signature;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authInfo);
            client.DefaultRequestHeaders.Add("User-Agent", "WebApiForPortal");
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            client.DefaultRequestHeaders.Add("Accept-Charset", "UTF-8");
            client.DefaultRequestHeaders.Add("AuthInfo", authInfo);
            client.DefaultRequestHeaders.Add("PublicKey", publicKey);
            //client.DefaultRequestHeaders.Add("baseURL", baseURL);
            client.DefaultRequestHeaders.Add("Userpassword", userPassword);

            return client;
        }
    }
}