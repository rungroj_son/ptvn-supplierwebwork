﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SupplierPortal.Models
{
    public class DashboardModels
    {
        //public int UserID { get; set; }
        public string SuppShortName { get; set; }
        public string UserName { get; set; }
        public string RptLastUpdate { get; set; }

        public string caption_NumPO { get; set; }
        public string caption_POVolume { get; set; }
        public string caption_POStatus { get; set; }

        public string SID { get; set; }
        //-----------------------------------------------------------

        // Year
        public int selectedYear { get; set; }
        public IEnumerable<YearFilter> filter_Years { get; set; }

        // Buyer
        public string selectedBuyerGrp { get; set; }
        public IEnumerable<BuyerGrpFilter> filter_BuyerGrps { get; set; }

        public IEnumerable<NumPO> data_NumPO { get; set; }
        public IEnumerable<POVolume> data_POVolume { get; set; }
        public IEnumerable<POStatus> data_POStatus { get; set; }
        //-----------------------------------------------------------

        public string filterCondition { get; set; }

        public string OrgID { get; set; }
        public string RptID { get; set; }
        public string UserID { get; set; }
    }

    // Chart NumPO Pie Chart
    public class ChartsModels_NumPO
    {
        public string caption_Chart { get; set; }
        public string json_NumPO { get; set; }
        public IEnumerable<NumPO> data_NumPO { get; set; }
    }

    // Chart POVolume Pie Chart
    public class ChartsModels_POVolume
    {
        public string caption_Chart { get; set; }
        public string json_POVolume { get; set; }
        public IEnumerable<POVolume> data_POVolume { get; set; }
    }

    // Chart POStatus Pie Chart
    public class ChartsModels_POStatus
    {
        public string caption_Chart { get; set; }
        public string json_POStatus { get; set; }
        public IEnumerable<POStatus> data_POStatus { get; set; }
    }
    //-----------------------------------------------------------

    // NumPO
    public class NumPO
    {
        //public string buyerGroup { get; set; }
        //public string buyerName { get; set; }
        public string buyer { get; set; }
        public string numPO { get; set; }
        public decimal percentNumPO { get; set; }
    }

    // POVolume
    public class POVolume
    {
        //public string buyerShortName { get; set; }
        public string buyer { get; set; }
        public string volume { get; set; }
        public decimal percentVolume { get; set; }
    }

    // POStatus
    public class POStatus
    {
        public string status { get; set; }
        public string volume { get; set; }
        public string numPO { get; set; }
        public decimal percentNumPO { get; set; }
    }

    // Year
    public class YearFilter
    {
        public int yearID { get; set; }
        public string yearName { get; set; }
    }

    // Buyer
    public class BuyerGrpFilter
    {
        //public string buyerShortName { get; set; }
        //public string buyerName { get; set; }
        //public string buyerGrpID { get; set; }
        public string buyerGrpName { get; set; }
    }
}