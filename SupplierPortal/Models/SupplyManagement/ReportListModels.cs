﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SupplierPortal.Models.SupplyManagement
{
    public class ReportListModels
    {

        public string ReportName { get; set; }
        public string ReportDetails { get; set; }
        public string UrlLink { get; set; }
        public string SID { get; set; }
        public string RptID { get; set; }
    }
}